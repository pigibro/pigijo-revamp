import React, { Component } from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight, map } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from "../components/page-header-icons";
import withAuth from '../_hoc/withAuth';
import numberWithComma from '../utils/numberWithComma';
import { FormControl, FormGroup, InputGroup, Col, Panel, PanelGroup, ListGroup, ListGroupItem } from "react-bootstrap";
import { getStationList, getTrainList, createToken, trainAddtoCart, setDepart, setReturn, sortFlightList, getAirlineList, setErrorMessage } from '../stores/actions';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import { SingleDatePicker } from "react-dates";
import 'react-dates/initialize';
import moment, { now } from 'moment';
import 'moment/locale/id'
import Loading from '../components/shared/loading';
import axios from "axios";
import { withRouter } from 'next/router';
import Router from 'next/router';
import { ConsoleView } from 'react-device-detect';
import getConfig from "next/config"
const { publicRuntimeConfig } = getConfig()
import * as gtag from '../utils/gtag';
moment.locale('id')
// Router.events.on('routeChangeComplete', url => gtag.pageview(url))

class Train extends Component {
    constructor(props) {
        super(props);
        this.state = { 
			station: [],
			destination: null,
			depart: null,
			destinationDefault: '',
			departDefault: '',
			trainclass: 0,
			trainclassDefault: 'All Class',
			date: moment().add(1, 'd'),
            dateDeparture: moment().add(1, 'd').format("YYYY-MM-DD"),
            focused: false,
            return_date: moment().add(2, 'd'),
            dateReturn: moment().add(2, 'd').format("YYYY-MM-DD"),
			return_focused: false,
			adult: 1,
			adultDefault: 1,
			infant: 0,
			infantDefault: '0',
			loadTrain: false,
			isRoundTrip: false,
			emptyField: [],
			departTrain: [],
			returnTrain: [],
			stationDetails: [],
			departure_train: null,
			train_section: 'departure'
		};
	}
	
	componentDidMount() {
		let { dispatch } = this.props
		dispatch(getStationList()).then(res => {
			
			let data = get(res, 'data')
			for(let i = 0; i < data.length; i++) {
				data[i].name = data[i].StationName;
                data[i].type = data[i].StationCode;
			}
			this.setState({station: data})
			if(this.props.router.query.dep) {
				let query = this.props.router.query
				let detail_depart = data.find(station => station.type == query.dep)
                let detail_destination = data.find(station => station.type == query.des)
				this.setState({
					destination: query.des ? query.des : null,
					depart: query.dep ? query.dep : null,
					destinationDefault: detail_destination.name,
					departDefault: detail_depart.name,
					trainclass: query.tc ? +query.tc : null,
					trainclassDefault: query.tcd ? query.tcd : null,
					date: query.depDate ? moment(query.depDate) : null,
					dateDeparture: query.depDate ? moment(query.depDate).format("YYYY-MM-DD") : null,
					return_date: query.retDate ? moment(query.retDate) : null,
					dateReturn: query.retDate ? moment(query.retDate).format("YYYY-MM-DD") : null,
					isRoundTrip: query.rt == 'true' ? true : false,
					adult: query.ad ? +query.ad : null,
					adultDefault: query.ad ? query.ad : null,
					infant: query.if ? +query.if : null,
					infantDefault: query.if ? query.if : null,
				}, () => this.handleSubmit())
			}
			
		})
	}

	handleSubmit = async (e) => {
		try {
			e ? e.preventDefault() : null
			let { dispatch } = this.props
			let { destination, depart, dateDeparture, dateReturn, trainclass, adult, infant, isRoundTrip, destinationDefault, departDefault } = this.state
			let emptyField = []
			if(depart === null) { emptyField.push('depart')}
            if(destination === null) { emptyField.push('destination')}
            if(dateDeparture === null) { emptyField.push('dateDeparture')}
            if(isRoundTrip) { if(dateReturn === null) { emptyField.push('dateReturn')}}
            if(adult === 0 ) { emptyField.push('adult') }
            if(trainclass === null ) { emptyField.push("trainclass")}
            if(emptyField.length > 0 ) {
                this.setState({emptyField})
            } else {
				this.setState({ loadTrain: true })
				let query = {
					des: destination,
					dep: depart,
					tc: trainclass,
					tcd: (trainclass == 0 ? 'All Class' : (trainclass == 1 ? 'Executive' : (trainclass == 2 ? 'Bussiness' : (trainclass == 3 ? 'Economy' : '')))),
					depDate: dateDeparture,
					retDate: dateReturn,
					rt: isRoundTrip,
					ad: adult,
					if: infant,
					destination: destinationDefault,
					departure: departDefault
                }

                e ? Router.replace({pathname:"/train", query}) : null
				const Routes = [{
                    'Origin': depart,
                    'Destination': destination,
                    'DepartDate': dateDeparture
                    }]
                isRoundTrip ? Routes.push({
                    'Origin': destination,
                    'Destination': depart,
                    'DepartDate': dateReturn
				}) : null
				
				let Params = {
                    'Route': Routes[0],
					"Adult": adult, 
					"Child": 0, 
					"Infant": infant,
					"Class": trainclass
				}

				let getDepart = await dispatch(getTrainList(Params))
				if(get(getDepart, 'meta.code') == 200) {
					this.setState({departTrain: get(getDepart, 'data.Schedules[0].Journeys'), stationDetails: get(getDepart, 'data.StationDetails')})
					if(isRoundTrip) {
						Params.Route = Routes[1]
						let getReturn = await dispatch(getTrainList(Params))
						if(get(getReturn, 'meta.code') == 200) {
							this.setState({returnTrain: get(getReturn, 'data.Schedules[0].Journeys'), loadTrain: false})
						}
					} else { this.setState({loadTrain: false})}
				}
			}
			
		} catch (error) {
			
		}
		
	}

	chooseTrain = (train, segment, trainIdx) => {
		let { returnTrain } = this.state
		let tempSeg = [segment]
		train.Segments = tempSeg
		if(returnTrain.length === 0 || trainIdx === 1 ) {
			
			const {router, dispatch, } = this.props;
			let { stationDetails, adult, infant, isRoundTrip, departure_train} = this.state

			dispatch(createToken())
			.then(res => {
				let Segments = []
				if(isRoundTrip){
					Segments.push({
						"TrainName": departure_train.TrainName,
						"Origin": departure_train.Origin,
						"ArrivalTime": departure_train.ArrivalTime,
						"ArrivalDate": departure_train.ArrivalDate,
						"Destination": departure_train.Destination,
						"DepartureTime": departure_train.DepartureTime,
						"DepartureDate": departure_train.DepartureDate,
						"JourneyCode": departure_train.JourneyCode,
						"CarrierNumber": departure_train.CarrierNumber,
						"ClassKey": departure_train.Segments[0].ClassKey,
						"SubClass": departure_train.Segments[0].SubClass,
						"Class": departure_train.Segments[0].Class,
						"Fare": departure_train.Segments[0].Fare,
						"FareBasisCode": departure_train.Segments[0].FareBasisCode,
						"Adult":adult,
						"Infant":infant
					})
				}
				Segments.push({
					"TrainName": train.TrainName,
					"Origin": train.Origin,
					"ArrivalTime": train.ArrivalTime,
					"ArrivalDate": train.ArrivalDate,
					"Destination": train.Destination,
					"DepartureTime": train.DepartureTime,
					"DepartureDate": train.DepartureDate,
					"JourneyCode": train.JourneyCode,
					"CarrierNumber": train.CarrierNumber,
					"ClassKey": train.Segments[0].ClassKey,
					"SubClass": train.Segments[0].SubClass,
					"Class": train.Segments[0].Class,
					"Fare": train.Segments[0].Fare,
					"FareBasisCode": train.Segments[0].FareBasisCode,
					"Adult":adult,
					"Infant":infant
				})

				let params = {
                    token: res.data.token,
                    Segments,
				}
				
				dispatch(trainAddtoCart(params))
				.then(result => {
					router.push({
						pathname: '/manifest-train',
						query: {
						  id: result.data.token,
						  adult: this.state.adult,
						  infant: this.state.infant,
						}
					});
				})
			})
		} else {
			this.setState({
				departure_train: train,
				train_section: 'return'
			})
		}
	}

    render() {
		let { 
			station, 
			destination, 
			destinationDefault, 
			depart, 
			departDefault, 
			trainclass, 
			trainclassDefault, 
			date, 
			dateDeparture, 
			return_date, 
			dateReturn, 
			focused, 
			return_focused,
			adult, 
			adultDefault,
			infant,
			infantDefault,
			loadTrain,
			isRoundTrip,
			emptyField,
			departTrain,
			returnTrain,
			stationDetails,
			train_section,
			departure_train
		} = this.state
		const minimumDate = this.state.date;
		let query = this.props.router.query		
		return (
            <>
                <Head title={"Pigijo - Find Affordable Train Ticket"} description={"Find Affordable Train Ticket and Travel Refference Here"} url={process.env.SITE_ROOT + '/train'} />
                <PageHeader section='transportation' background={"/static/images/img05.jpg"} title={'Train'} caption={'abcdefghijka'} category="accommodation" type={'train'} slug={''}/>
                <section className="body-content" style={{backgroundColor: '#F7F7F7'}} id="search-bar">
					<div className="container">
						<div className="row flex-center" style={{display: 'flex', justifyContent: 'center'}}>
						<div className="col-md-8 cat-header" style={{padding: '3em'}}>
							<form onSubmit={this.handleSubmit}>
							<div className="row" style={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
								<div className="col-lg-4 col-md-4 col-sm-4">
                                    <span className='text-label'>Origin</span>
                                    <SearchAutoComplete
                                        id="destination"
                                        key={"select-single"}
                                        index={0}
                                        data={station || []}
                                        placeholder="Select Station"
                                        labelKey="name"
                                        onChange={result => this.setState({depart: get(result, '0.type') || "", departDefault: get(result, '0.name') || ""})}
                                        bsSize="large"
                                        defaultInputValue={query.dep ? `${query.departure} (${query.dep})` : departDefault}
                                    />
									{
                                        emptyField.includes('depart') ? <span style={{color: 'red'}}>Please enter your departure station</span> : null
                                    }
								</div>

								<div className="col-lg-4 col-md-4 col-sm-4">
									<span className='text-label'>Destination</span>
									<SearchAutoComplete
                                        id="destination"
                                        key={"select-single"}
                                        index={0}
                                        data={station || []}
                                        placeholder="Select Station"
                                        labelKey="name"
                                        onChange={result => this.setState({destination: get(result, '0.type') || "", destinationDefault: get(result, '0.name') || ""})}
                                        bsSize="large"
                                        defaultInputValue={query.des ? `${query.destination} (${query.des})` : destinationDefault}
                                    />
									{
                                        emptyField.includes('destination') ? <span style={{color: 'red'}}>Please enter your destination station</span> : null
                                    }							
								</div>

								<div className="col-lg-4 col-md-4 col-sm-4">
									<span className='text-label'>Train Class</span>
									<SearchAutoComplete
                                        id="destination"
                                        key={"select-single"}
                                        index={0}
                                        data={[{ name: 'All Class', type: '', value: 0 }, { name: 'Executive', type: '', value: 1 }, { name: 'Bussiness', type: '', value: 2 }, { name: 'Economy', type: '', value: 3 }]}
                                        placeholder="Select Class"
                                        labelKey="name"
                                        onChange={result => this.setState({destination: get(result, '0.value') || ""})}
                                        bsSize="large"
                                        defaultInputValue={trainclassDefault}
                                    />
									{
                                        emptyField.includes('trainclass') ? <span style={{color: 'red'}}>Please enter your train class</span> : null
                                    }
								</div>

								<div className="col-lg-3 col-md-3 col-sm-3" style={{zIndex: 50}}>
									<span className='text-label'>Departure</span>
									<SingleDatePicker
										numberOfMonths={1}
										date={date}
										onDateChange={date => 
											this.setState({
												date: date,
												dateDeparture: moment(date).format("YYYY-MM-DD"),
												return_date: moment(date).add(1, 'd'),
												dateReturn: moment(date).add(1, 'd').format("YYYY-MM-DD")
											})
										}
										focused={focused}
										onFocusChange={({ focused }) => this.setState({ focused })}
										id="departdate"
										placeholder="Depature Date"
									/>
									{
                                        emptyField.includes('dateDeparture') ? <span style={{color: 'red'}}>Please enter your departure date</span> : null
                                    }
								</div>

                                <div className="col-lg-3 col-md-3 col-sm-3" style={{zIndex: 50}}>
									<span className='text-label'><input type="checkbox" className="styled-checkbox" checked={isRoundTrip} onChange={() => this.setState({isRoundTrip: !isRoundTrip})} style={{width: 'auto'}}/> Return</span>
									{
										isRoundTrip ? (
											<SingleDatePicker
												numberOfMonths={1}
												isOutsideRange={(day) => day.isBefore(minimumDate)}
												date={return_date}
												onDateChange={return_date =>
													this.setState({
													return_date: return_date,
													dateReturn: moment(return_date).format("YYYY-MM-DD")
													})
												}
												focused={return_focused}
												onFocusChange={({ focused }) => this.setState({ return_focused: focused })}
												id="return"
												placeholder="Return Date"
											/>
										) : null
									}
									{
                                        emptyField.includes('dateReturn') ? <span style={{color: 'red'}}>Please enter your return date</span> : null
                                    }
									
								</div>
                                <div className="col-lg-2 col-md-2 col-sm-2">
									<span className='text-label'>Adult</span>
									<SearchAutoComplete
                                        id="destination"
                                        key={"select-single"}
                                        index={0}
                                        data={[{ name: '0', type: ''}, {name: '1', type: ''}, {name: '2', type: ''}, {name: '3', type: ''}]}
                                        placeholder="Adult"
                                        labelKey="name"
                                        onChange={result => this.setState({adult: get(result, '0.name') || ""})}
                                        bsSize="large"
                                        defaultInputValue={adultDefault}
                                    />
									{
                                        emptyField.includes('adult') ? <span style={{color: 'red'}}>Please enter total ault passenger</span> : null
                                    }
								</div>
                                <div className="col-lg-2 col-md-2 col-sm-2">
									<span className='text-label'>Infant</span>
									<SearchAutoComplete
                                        id="destination"
                                        key={"select-single"}
                                        index={0}
                                        data={[{ name: '0', type: ''}, {name: '1', type: ''}, {name: '2', type: ''}, {name: '3', type: ''}]}
                                        placeholder="infant"
                                        labelKey="name"
                                        onChange={result => this.setState({infant: get(result, '0.name') || ""})}
                                        bsSize="large"
                                        defaultInputValue={infantDefault}
                                    />
								</div>
                                <div className="col-lg-2 col-md-2 col-sm-12" style={{paddingTop: '1.75em'}}>
                                    <button
                                        type='submit'
										className='btn btn-block btn-primary btn-fix btn-blue btn-flight-search'
										style={{borderRadius: '24px'}}
                                    >
                                        Search
                                    </button>
                                </div>
							</div>
							</form>
						</div>
						</div>
					</div>
				</section>
				{
					(isRoundTrip && departTrain.length > 0 ) && (
						<section className={`body-content ${train_section === 'departure' ? 'section_inactive' : 'section_active'}`}>
							<div className="container" style={{margin: 0, padding: 0, width: '100%'}}>
								<div className="row flex-center" style={{margin: 0, padding: 0, fontFamily: 'poppins'}}>
									<div className={`${train_section === 'departure' ? 'col-xs-6' : 'col-xs-3'} ${train_section === 'departure' ? 'section_active' : 'section_inactive'}`} style={{padding: '1em 2em 2em 7%', zIndex: 10}}>
										<h4 style={{color: (train_section === 'departure' ? '#FF5B00' : '#494747'), fontWeight: 'bold'}}>{train_section === 'departure' ? 'Pilih Kereta Pergi' : `${stationDetails[1].Code} - ${stationDetails[0].Code}`}</h4>
										{
											train_section === 'departure' ? (
												<span style={{verticalAlign: 'center'}}>{stationDetails[1].StationName} ({stationDetails[1].Code}) <img src={require('../static/icons/arrow.png')} style={{width: '7%'}}/> {stationDetails[0].StationName} ({stationDetails[0].Code})</span>
											) : null
										}
										<span style={{marginLeft: '1.5em'}}>{moment(dateDeparture).format('LL')} | {adult+infant} Penumpang</span>
										{
											train_section !== 'departure' ? (
												<p style={{marginLeft: '1.5em', color: '#FF5B00'}}>IDR {departure_train.Segments[0].PaxFares.map(fare => { return fare.PaxType == 'ADT' ? (numberWithComma(fare.Charges.length > 0 ? fare.Charges[0].Amount : 0)) : 0})}</p>
											) : null
										}
										
									</div>
									<div className={`${train_section === 'departure' ? 'col-xs-3' : 'col-xs-6'} ${train_section === 'departure' ? 'section_inactive' : 'section_active'}`} style={{padding: '2em'}}>
										<h4 style={{ fontWeight: 'bold', color: (train_section === 'departure' ? '#494747' : '#FF5B00')}}>Perjalanan Pulang</h4>
										{
											train_section !== 'departure' ? (
												<span style={{verticalAlign: 'center'}}>{stationDetails[0].StationName} ({stationDetails[0].Code}) <img src={require('../static/icons/arrow.png')} style={{width: '7%'}}/> {stationDetails[1].StationName} ({stationDetails[1].Code})</span>
											) : null
										}
										{
											train_section !== 'departure' ? (
												<span style={{marginLeft: '1.5em'}}>{moment(dateDeparture).format('LL')} | {adult+infant} Penumpang</span>
											) : null
										}
									</div>
									<div className={`col-xs-3 ${train_section === 'departure' ? 'section_inactive' : 'section_active'}`} style={{padding: '2.5em'}}>
										<div style={{width: '60%'}}>
											<button
												type='submit'
												className='btn btn-block btn-primary btn-fix btn-blue btn-flight-search'
												style={{borderRadius: '24px'}}
												onClick={() => {location.href = '#search-bar'}}
											>
												Ubah Pencarian
											</button>
										</div>
											
									</div>
								</div>
							</div>
						</section>
					)
				}
				
				{
					(departTrain.length > 0 && !isRoundTrip) && (
						<section className="body-content" style={{backgroundColor: '#fff', boxShadow: '15px 20px 20px #0000000D'}}>
							<div className="container" style={{margin: 0, padding: 0, width: '100%'}}>
								<div className="row flex-center" style={{margin: 0, padding: 0, fontFamily: 'poppins'}}>
									<div className='col-xs-6' style={{backgroundColor: '#fff', padding: '1em 2em 2em 7%', zIndex: 10}}>
										<h4 style={{color: '#FF5B00', fontWeight: 'bold'}}>Pilih Kereta Pergi</h4>
										<span style={{verticalAlign: 'center'}}>{stationDetails[1].StationName} ({stationDetails[1].Code}) <img src={require('../static/icons/arrow.png')} style={{width: '7%'}}/> {stationDetails[0].StationName} ({stationDetails[0].Code})</span>
										<span style={{marginLeft: '1.5em'}}>{moment(dateDeparture).format('LL')} | {adult+infant} Penumpang</span>
									</div>
									<div className='col-xs-3' style={{ padding: '2em'}}>
										
									</div>
									<div className='col-xs-3' style={{ padding: '2.5em'}}>
										<div style={{width: '60%'}}>
											<button
												type='submit'
												className='btn btn-block btn-primary btn-fix btn-blue btn-flight-search'
												style={{borderRadius: '24px'}}
												onClick={() => {location.href = '#search-bar'}}
											>
												Ubah Pencarian
											</button>
										</div>
											
									</div>
								</div>
							</div>
						</section>
					)
				}

				<section className="body-content" style={{backgroundColor: '#F7F7F7', fontFamily: 'poppins'}}>
                    <div className="container">
                        <div className="row" style={{padding: '3em 0'}}>
							{
								loadTrain ? (<Loading/>) : (
									map(train_section == 'departure' ? departTrain : returnTrain, (train, key) => (
									<React.Fragment key={key}>
									{
										train.Segments.map((trainSegment, keys) => (
											<div className='col-xs-12 col-md-12 col-lg-12' style={{borderRadius: '23px', backgroundColor: '#fff', boxShadow: '15px 20px 20px #0000000D', marginBottom: '2em'}} key={keys}>
												<div className='row'>
													<div className='col-xs-4 col-md-4 col-lg-4' style={{ padding: '2em'}}>
														<span style={{color: '#636060', fontWeight: 'bold', fontFamily: 'poppins'}}>{train.TrainName}</span>
														<p style={{color: '#636060', fontFamily: 'poppins'}}>{trainSegment.ClassName} (Subclass {trainSegment.SubClass})</p>
													</div>
													<div className='col-xs-4 col-md-4 col-lg-4' style={{padding: '2em', display: 'flex', justifyContent: 'center'}}>
														<div style={{width: '15%', backgroundColor: 'white'}}>
															<span style={{color: '#636060', fontWeight: 'bold', fontFamily: 'poppins'}}>{train.DepartureTime}</span>
															<p style={{color: '#636060', fontFamily: 'poppins'}}>{train.Origin}</p>
														</div>
														<div style={{width: '25%', backgroundColor: 'white', textAlign: 'center'}}>
															<span style={{color: '#636060', fontFamily: 'poppins', padding: '0 0.8em', borderBottom: '1px solid #707070'}}>{train.Duration}</span>
															<p style={{color: '#636060', fontFamily: 'poppins'}}>Langsung</p>
														</div>
														<div style={{width: '15%', backgroundColor: 'white', paddingLeft: '5%'}}>
															<span style={{color: '#636060', fontWeight: 'bold', fontFamily: 'poppins'}}>{train.ArrivalTime}</span>
															<p style={{color: '#636060', fontFamily: 'poppins'}}>{train.Destination}</p>
														</div>
													</div>

													<div className='col-xs-4 col-md-4 col-lg-4' style={{ padding: '2em', display: 'flex', justifyContent: 'flex-end'}}>
														<h4 style={{color: '#FF5B00', fontWeight: 'bold'}}>IDR {trainSegment.PaxFares.map(fare => { return fare.PaxType == 'ADT' ? (numberWithComma(fare.Charges.length > 0 ? fare.Charges[0].Amount : 0)) : 0})}</h4>
														<div style={{width: '40%', marginLeft: '10%', paddingTop: '3%'}}>
															<button
																onClick={() => this.chooseTrain(train, trainSegment, train_section == 'departure' ? 0 : 1)}
																className='btn btn-block btn-primary btn-fix btn-blue btn-flight-search'
																style={{borderRadius: '24px', lineHeight: '2.5em'}}
															>
																Pilih
															</button>
														</div>
													</div>
												</div>
											</div>
										))
									}
									</React.Fragment>
									
									))
								)
							}

							

							{/* <div className='col-xs-12 col-md-12 col-lg-12' style={{borderRadius: '23px', backgroundColor: '#fff', boxShadow: '15px 20px 20px #0000000D', marginBottom: '2em'}}>
								<div className='row'>
									<div className='col-xs-4 col-md-4 col-lg-4' style={{ padding: '2em'}}>
										<span style={{color: '#636060', fontWeight: 'bold', fontFamily: 'poppins'}}>Malabar 108Malabar 108</span>
										<p style={{color: '#636060', fontFamily: 'poppins'}}>Ekonomi (Subclass P)</p>
									</div>
									<div className='col-xs-4 col-md-4 col-lg-4' style={{padding: '2em', display: 'flex', justifyContent: 'center'}}>
										<div style={{width: '15%', backgroundColor: 'white'}}>
											<span style={{color: '#636060', fontWeight: 'bold', fontFamily: 'poppins'}}>16:10</span>
											<p style={{color: '#636060', fontFamily: 'poppins'}}>PSE</p>
										</div>
										<div style={{width: '25%', backgroundColor: 'white', textAlign: 'center'}}>
											<span style={{color: '#636060', fontFamily: 'poppins', padding: '0 0.8em', borderBottom: '1px solid #707070'}}>3j 24m</span>
											<p style={{color: '#636060', fontFamily: 'poppins'}}>Langsung</p>
										</div>
										<div style={{width: '15%', backgroundColor: 'white', paddingLeft: '5%'}}>
											<span style={{color: '#636060', fontWeight: 'bold', fontFamily: 'poppins'}}>19:34</span>
											<p style={{color: '#636060', fontFamily: 'poppins'}}>BD</p>
										</div>
									</div>

									<div className='col-xs-4 col-md-4 col-lg-4' style={{ padding: '2em', display: 'flex', justifyContent: 'flex-end'}}>
										<h4 style={{color: '#FF5B00', fontWeight: 'bold'}}>IDR 90.000</h4>
										<div style={{width: '40%', marginLeft: '10%', paddingTop: '3%'}}>
											<button
												type='submit'
												className='btn btn-block btn-primary btn-fix btn-blue btn-flight-search'
												style={{borderRadius: '24px', lineHeight: '2.5em'}}
											>
												Pilih
											</button>
										</div>
									</div>
								</div>
							</div> */}

						</div>
					</div>
				</section>

                <section className="mt2">
                    <div className="container">
                        <div className="row" style={{padding: '3em 0'}}>
                            {/* <div className='col-xs-12 col-md-12 col-lg-12'>
                                <h4 style={{color: '#636060', fontFamily: 'poppins', fontSize: '1.7em', fontWeight: 'bold'}}>Promo</h4>
                            </div> */}
                            {/* <div className='col-xs-12 col-md-6 col-lg-6'>
                                <a><img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/road_trip.jpg' style={{width: '100%', borderRadius: '23px', boxShadow: '5px 15px 20px #0000000D'}}/></a>
                            </div>

                            <div className='col-xs-12 col-md-6 col-lg-6'>
                                <a><img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/road_trip.jpg' style={{width: '100%', borderRadius: '23px', boxShadow: '5px 15px 20px #0000000D'}}/></a>
                            </div> */}

                            <div className='col-xs-12 col-md-12 col-lg-12 mt2'>
                                <h4 style={{color: '#636060', fontFamily: 'poppins', fontSize: '1.7em', fontWeight: 'bold'}}>Kota yang dilewati kereta api</h4>
                            </div>

                            <div className='col-xs-12 col-md-6 col-lg-6 mb2'>
                                <div style={{
                                    width: '100%',
                                    borderRadius: '23px',
                                    boxShadow: '5px 15px 20px #0000000D'
                                }}>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/road_trip.jpg' style={{width: '100%', borderTopLeftRadius: '23px', borderTopRightRadius: '23px', boxShadow: '5px 15px 20px #0000000D'}}/>
									<div style={{padding: '1em'}}>
										<h4 style={{color: '#636060', fontFamily: 'poppins', fontSize: '1.2em', fontWeight: 'bold', margin: 0}}>Jakarta</h4>
										<span style={{color: '#636060', fontFamily: 'poppins', fontSize: '1em', lineHeight: '2em'}}>Start from <span style={{color: '#FF5B00', fontWeight: 'bold', fontSize: '1.1em'}}>Rp 75.000</span></span>
										<p style={{color: '#636060', fontFamily: 'poppins', fontSize: '0.85em', lineHeight: '1.2em'}}>Bandung sudah dikenal luas sebagai salah satu tujuan wisata belanja dan kuliner bagi wisatawan lokal maupun mancanegara. Namun, ibukota Jawa Barat ini juga</p>
									</div>
                                </div>
                            </div>
							<div className='col-xs-12 col-md-6 col-lg-6 mb2'>
                                <div style={{
                                    width: '100%',
                                    borderRadius: '23px',
                                    boxShadow: '5px 15px 20px #0000000D'
                                }}>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/road_trip.jpg' style={{width: '100%', borderTopLeftRadius: '23px', borderTopRightRadius: '23px', boxShadow: '5px 15px 20px #0000000D'}}/>
									<div style={{padding: '1em'}}>
										<h4 style={{color: '#636060', fontFamily: 'poppins', fontSize: '1.2em', fontWeight: 'bold', margin: 0}}>Jakarta</h4>
										<span style={{color: '#636060', fontFamily: 'poppins', fontSize: '1em', lineHeight: '2em'}}>Start from <span style={{color: '#FF5B00', fontWeight: 'bold', fontSize: '1.1em'}}>Rp 75.000</span></span>
										<p style={{color: '#636060', fontFamily: 'poppins', fontSize: '0.85em', lineHeight: '1.2em'}}>Bandung sudah dikenal luas sebagai salah satu tujuan wisata belanja dan kuliner bagi wisatawan lokal maupun mancanegara. Namun, ibukota Jawa Barat ini juga</p>
									</div>
                                </div>
                            </div>
							<div className='col-xs-12 col-md-6 col-lg-6 mb2'>
                                <div style={{
                                    width: '100%',
                                    borderRadius: '23px',
                                    boxShadow: '5px 15px 20px #0000000D'
                                }}>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/road_trip.jpg' style={{width: '100%', borderTopLeftRadius: '23px', borderTopRightRadius: '23px', boxShadow: '5px 15px 20px #0000000D'}}/>
									<div style={{padding: '1em'}}>
										<h4 style={{color: '#636060', fontFamily: 'poppins', fontSize: '1.2em', fontWeight: 'bold', margin: 0}}>Jakarta</h4>
										<span style={{color: '#636060', fontFamily: 'poppins', fontSize: '1em', lineHeight: '2em'}}>Start from <span style={{color: '#FF5B00', fontWeight: 'bold', fontSize: '1.1em'}}>Rp 75.000</span></span>
										<p style={{color: '#636060', fontFamily: 'poppins', fontSize: '0.85em', lineHeight: '1.2em'}}>Bandung sudah dikenal luas sebagai salah satu tujuan wisata belanja dan kuliner bagi wisatawan lokal maupun mancanegara. Namun, ibukota Jawa Barat ini juga</p>
									</div>
                                </div>
                            </div>
							<div className='col-xs-12 col-md-6 col-lg-6 mb2'>
                                <div style={{
                                    width: '100%',
                                    borderRadius: '23px',
                                    boxShadow: '5px 15px 20px #0000000D'
                                }}>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/road_trip.jpg' style={{width: '100%', borderTopLeftRadius: '23px', borderTopRightRadius: '23px', boxShadow: '5px 15px 20px #0000000D'}}/>
									<div style={{padding: '1em'}}>
										<h4 style={{color: '#636060', fontFamily: 'poppins', fontSize: '1.2em', fontWeight: 'bold', margin: 0}}>Jakarta</h4>
										<span style={{color: '#636060', fontFamily: 'poppins', fontSize: '1em', lineHeight: '2em'}}>Start from <span style={{color: '#FF5B00', fontWeight: 'bold', fontSize: '1.1em'}}>Rp 75.000</span></span>
										<p style={{color: '#636060', fontFamily: 'poppins', fontSize: '0.85em', lineHeight: '1.2em'}}>Bandung sudah dikenal luas sebagai salah satu tujuan wisata belanja dan kuliner bagi wisatawan lokal maupun mancanegara. Namun, ibukota Jawa Barat ini juga</p>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <style jsx global>{`
                    .guest-filter {
						padding: 11px 10px 11px 10px;
						border: 1px solid #D8DDE6;
						border-radius: 0;
						background-color: #fff
					}

					.type-text {
						color: #b4b4b4;
						font-size: 10pt;
					}

					.type-number {
						color: #e17306;
					}

					.dropdown-icon {
						width: 13px;
						height: 13px;
						position: absolute;
						outline: none;
						border: none;
						border-radius: 50%;
						color: #e17306;
						background: transparent;
						font-size: 8pt;
						font-weight: 800;
						right: 15%
					}

					.option-ul {
						margin: 10px 0 0 -10px;
						padding: 10px 15px;
						position: absolute;
						z-index: 100;
						background: #fff;
						width: 90%;
						border: 1px solid #e9eef3;
						border-top: none;
					}

					.option-box {
						padding: 8px 0;
						list-style-type: none;
						border-bottom: 1px solid #cfcfcf;
						display: flex;
    					justify-content: space-between;
					}

					.option-label {
						font-weight: normal;
					}

					.amount-box {
						padding: 5px;
						border: 1px solid #e17306;
						border-radius: 20px;
					}

					.amount-number {
						padding: 0 5px 0 5px;
					}

					.amount-btn {
						width: 23px;
						border: none;
						border-radius: 50%;
						outline: none;
						font: inherit;
						font-size: 17px;
					}

					.SingleDatePicker_1 .SingleDatePickerInput .DateInput_1 .DateInput_input {
						font-size: 1em !important;
                    } 
                    
                    .DateInput_input {
                        font-size: 1em !important;
					}
					
					.section_active {
						background-color: #fff; 
					}

					.section_inactive {
						background-color: #ffcbb7;
					}
                `}</style>
            </>
        );
    }
}

export default compose(
    withRouter,
    connect(),
    withAuth(["PUBLIC"])
  )(Train);
