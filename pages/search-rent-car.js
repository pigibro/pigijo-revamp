import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight, map } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import Router from 'next/router'
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';
import Pagination from "react-js-pagination";
import { FormControl, FormGroup, InputGroup, Col, Panel, PanelGroup } from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import { DateRangePicker } from "react-dates";
import 'react-dates/initialize';
import numberWithComma from '../utils/numberWithComma';
import { slugify, urlImage, convertToRp } from '../utils/helpers';
import { getEventDetail, getLocations } from '../stores/actions';
import moment from 'moment';
import 'moment/locale/id'
moment.locale('id');
import ReactDOM from 'react-dom'


class SearchRentCar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            endDate: null,
            date : null,
            focusedInput: null,
            startDaterent: null,
            endDaterent: null,
            location: [],
            location_type: '',
            selectlocation: '',
            totalCar: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        // this.handleChange = this.handleChange.bind(this);

    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
        const { dispatch } = this.props;
        dispatch(getLocations()).then(result => {
          if (get(result, 'meta.code') === 200){
            this.setState({
              location: get(result, 'data')
            });
          }
        }); 
      }

    handleDateChange = ({ startDate, endDate}) =>
    this.setState({ 
        startDate : startDate,
        startDaterent : moment(startDate).format("YYYY-MM-DD"),
        endDate : endDate,
        endDaterent : moment(endDate).format("YYYY-MM-DD")
    });

    onFocusChange = (focusedInput) => {
		this.setState({ focusedInput });
    };

    getSelectValue = () => {

        const myValue = event.target.value;
        this.setState({
            totalCar : myValue
        })
    }
    
    async handleSubmit(e) {
      try {
          e.preventDefault();
          let {totalCar, startDaterent, endDaterent, selectlocation } = this.state
          const data = {
            'locationId' : selectlocation.id,
            'locationName' : selectlocation.name,
            'locationType' : selectlocation.type,
            'startDaterent' : startDaterent,
            'endDaterent' : endDaterent,
            'TotalCar' : totalCar
          }
          Router.push({
              pathname : '/rent-car',
              query: data
          })
      }catch (error) {
      }
    }

    isOutsideRange = () => false;
    render() {
        const { startDaterent, endDaterent, focusedInput, location, totalCar, selectlocation, startDate, endDate} = this.state;
        return (
            <React.Fragment>
                <Head 
                    title={"Eu nostrud amet esse enim est esse commodo reprehenderit."} 
                    description={"Culpa tempor eu elit duis eu occaecat culpa culpa. Commodo anim commodo occaecat do consequat consequat Lorem laborum qui duis reprehenderit nisi Lorem ullamco. Dolore cupidatat proident esse irure cillum deserunt. Sit et tempor sint duis occaecat ea culpa."} url={process.env.SITE_ROOT + '/p/1180-showcase-indonesia-full-pass-early-bird'} />
                <PageHeader
                background={"/static/images/img05.jpg"}
                title="Find Rent Car"
                caption="Culpa ad amet non aute do sunt velit veniam et." />

                <section className="filter-rentcar" style={{background: 'white'}}>
                    <div className="container">
                        <form onSubmit={this.handleSubmit}>
                        <div className="col-md-4 card">
                            <div className="col-md-12">
                            <span className="label-form">City</span>
                            <FormGroup style={{ top: "0.5em" }}>
                                <InputGroup>
                                <InputGroup.Addon>
                                    <a style={{ cursor: "pointer" }}>
                                    <img src={require('../static/icons/ic_city.png')} style={{ width: "15px" }} />
                                    </a>
                                </InputGroup.Addon>
                                <SearchAutoComplete
                                    id="city"
                                    key={"select-single"}
                                    index={0}
                                    data={location || []}
                                    placeholder="Select City"
                                    labelKey="name"
                                    onChange={result => this.setState({ selectlocation: get(result, '0') || "" })}
                                    bsSize="large"
                                    className="typeahead search-box-adv single-search"
                                />
                                </InputGroup>
                            </FormGroup>
                            </div>
                        </div>
                        <div className="col-md-4 card">
                            <div className="col-md-12">
                            <span className="label-form">Dates</span>
                            <FormGroup style={{ top: "0.5em" }}>
                                <InputGroup>
                                <InputGroup.Addon>
                                    <a style={{ cursor: "pointer" }}>
                                    <img src={require('../static/icons/ic_date.png')} style={{ width: "15px" }} />
                                    </a>
                                </InputGroup.Addon>
                                <DateRangePicker
                                    block
                                    startDatePlaceholderText="Start"
                                    endDatePlaceholderText="End"
                                    startDate={startDate}
                                    startDateId="startDateId"
                                    endDate={endDate}
                                    endDateId="endDateId"
                                    onDatesChange={this.handleDateChange}
                                    focusedInput={focusedInput}
                                    onFocusChange={this.onFocusChange}
                                    displayFormat="DD MMM YYYY"
                                    hideKeyboardShortcutsPanel
                                    numberOfMonths={1}
                                    withPortal={false}
                                    minimumNights={0}
                                />
                                </InputGroup>
                            </FormGroup>
                            </div>
                        </div>
                        <div className="col-md-4 card">
                            <div className="col-md-12">
                            <span className="label-form">Car</span>
                            <FormGroup style={{ top: "0.5em" }}>
                            <InputGroup className="inptCar">
                            <InputGroup.Addon>
                                <a style={{ cursor: "pointer" }}>
                                <img src={require('../static/icons/ic_car.png')} style={{ width: "17px" }} />
                                </a>
                            </InputGroup.Addon>
                            {/* <SearchAutoComplete
                                id="carTotal"
                                key={"select-single"}
                                index={0}
                                data={[
                                    { name: '1', type: '' },
                                    { name: '2', type: '' },
                                    { name: '3', type: '' },
                                    { name: '4', type: '' },
                                    { name: '5', type: '' }
                                ]}
                                placeholder="Total Car"
                                labelKey="name"
                                onChange={result => this.setState({ totalCar: get(result)})}
                                bsSize="large"
                                className="typeahead search-box-adv single-search"
                            /> */}
                            <FormControl
                            className="typeahead search-box-adv single-search inptCar"
                            id="CarTotal"
                            name="totalcar"
                            onChange={this.getSelectValue}
                            placeholder={"1"} 
                            componentClass="select"
                            ref={select => { this.select = select }}
                            >
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </FormControl>
                            </InputGroup>
                        </FormGroup>
                            </div>
                        </div>
                        <div className="col-md-12 button-rentcar"><button type="submit" className="btn btn-primary btn-sm btn-rentc-search"><span className="ti-search"></span> Search</button></div>
                        </form>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}


export default compose(
    connect(),
    withAuth(["PUBLIC"])
  )(SearchRentCar);
