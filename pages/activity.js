import { compose } from 'redux'
import { connect } from 'react-redux'
import Error from './_error'
import { get, isEmpty, merge, map, range, takeRight } from 'lodash'
import moment from "moment";
import Head from '../components/head'
import { slugify, urlImage, replaceEnter } from '../utils/helpers';
import { Modal } from 'react-bootstrap';
import React from 'react'
import withAuth from '../_hoc/withAuth'
import { getActivity } from '../stores/actions'
import PageHeader from '../components/page-header';
import Content from '../components/activity/content';
import Router from 'next/router';
import { Link } from "../routes";
import 'react-dates/initialize';
import Maps from "../components/shared/map";
import { DayPickerRangeController } from 'react-dates';
import { getSchedules, storeCart, getCartList, createToken, praCheckout, setSuccessMessage, setErrorMessage } from '../stores/actions';
import numeral from '../utils/numeral';
import { setCookie } from '../utils/cookies';
import { isMobileOnly } from "react-device-detect";
import * as gtag from '../utils/gtag';

// Router.events.on('routeChangeComplete', url => gtag.pageview(url))

const Summary = ({ activity, day, startDate, endDate, person, price, price_before, handleClose, bookNow, addToCart }) => (
  <div id={isMobileOnly ? 'mobile-bar' : 'sticky-bar'} className="sticky-bar fade-in">
    <div className="price">
      <p>{activity.name}</p>
      <p>
        <span className="ti-user" title="Person"></span>  <span> x {person} unit</span>
      </p>
      {
        get(activity, 'product_category_slug') === 'local-assistants' &&
        <p>
          <span className="ti-calendar" title="Day"></span>  <span> x {day} {day > 0 ? 'days' : 'day'}</span>
        </p>
      }
      <p>
        <span className="ti-timer" title="Schedule"></span> <span>{startDate !== endDate && `${moment(startDate).format("DD MMMM YYYY")} - ${moment(endDate).format("DD MMMM YYYY")}`}{startDate === endDate && `${moment(startDate).format("DD MMMM YYYY")}`}</span>
      </p>
      <div className="amount h5 text-primary">
        {/* Rp {numeral(parseInt(activity.price_after,10)).format('0,0')} */}
          Rp {numeral(parseInt(price, 10)).format('0,0')}
        {price !== price_before &&
          <span className="last-price">
            Rp {numeral(parseInt(price_before, 10)).format('0,0')}
          </span>
        }
      </div>
    </div>
    <div className="side-bar-footer">
      <div className="act-rating">
        {/* <span className="ti-shopping-cart-full"></span>
        <span>20 kali dipesan</span> */}
        Total
        <div className="amount h4 text-primary float-r" style={{ fontWeight: 600 }}>
          {
            `Rp ${numeral(parseInt(price * (get(activity, 'product_category_slug') === 'local-assistants' ? day : person), 10)).format('0,0')}`
          }
        </div>
      </div>
      <div className="act-rating">
        <button className="btn btn-primary btn-sm btn-block mt1" onClick={() => bookNow(startDate, endDate)}>Checkout</button>
        <button className="btn btn-check btn-sm btn-block mt1" onClick={() => addToCart(startDate, endDate)}>Add to Plan</button>
      </div>
    </div>
  </div>
);

class Activity extends React.Component {
  static async getInitialProps({ query, store }) {
    const { slug } = query;
    const currentState = store.getState();
    const activity = get(currentState, 'activity.single');
    let errorCode = false;
    if (isEmpty(activity) || (!isEmpty(activity) && get(activity, 'slug') != slug)) {
      const resProduct = await store.dispatch(getActivity(slug));
      errorCode = get(resProduct, 'meta.code') > 200 ? 404 : false;
    }
    return { errorCode, slug: slug }
  }

  constructor(props) {
    super(props);
    this.state = {
      stickyWidth: 0,
      startDate: null,
      endDate: null,
      focusedInput: 'startDate',
      showCalendar: false,
      person: 0,
      error: "",
      isLoading: false,
      isSummary: false,
      isModal: true,
      selected: {},
      day: 1,
    }
  }
  bookNow = (startDate, endDate) => {
    const { carts, dispatch, activity } = this.props;
    let person = this.state.person || this.props.activity.min_person;
    const day = this.state.day;
    if (get(activity, 'product_category_slug') === 'local-assistants') {
      person = day;
      endDate = moment(endDate).add(day, 'days').format('YYYY-MM-DD');
    }

    dispatch(createToken()).then(result => {
      if (get(result, 'meta.code') === 200) {
        dispatch(storeCart({ token: get(result, 'data.token'), category: get(activity, 'product_category_slug'), start_date: startDate, end_date: endDate, qty: person, product_id: activity.id })).then(res => {
          if (get(res, 'meta.code') === 200) {
            dispatch(praCheckout(get(result, 'data.token'))).then(booking => {
              if (get(booking, 'data.status') === 'verified') {
                Router.push('/checkout');
              }
            });
            // dispatch(getCartList(get(result,'data.token')));
            // dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
          } else {
            dispatch(setErrorMessage(get(res, 'data')));
          }
        })
      }
    });

    // if(carts.length === 0){
    //   dispatch(createToken()).then(result => {
    //     if (get(result, 'meta.code') === 200) {
    //       setCookie('__joctkn', get(result,'data.token'));
    //       setCookie('__joclimit', get(result,'data.timelimit'));
    //       dispatch(storeCart({token: get(result,'data.token'), start_date: startDate, end_date: endDate, qty: person,product_id: activity.id})).then(res => {
    //         if (get(res, 'meta.code') === 200) {
    //           dispatch(getCartList(get(result,'data.token')));
    //           // dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
    //         }else{
    //           dispatch(setErrorMessage(get(res,'data')));
    //         }
    //       })
    //     }
    //   });
    // }else{
    //   dispatch(storeCart({token: carts.token, start_date: startDate, end_date: endDate, qty: person,product_id: activity.id})).then(res => {
    //     if (get(res, 'meta.code') === 200) {
    //       dispatch(getCartList(carts.token));
    //       // dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
    //     }else{
    //       dispatch(setErrorMessage(get(res,'data')));
    //     }
    //   })
    // }
  }

  addToCart = (startDate, endDate) => {
    const { carts, dispatch, activity } = this.props;
    let person = this.state.person || this.props.activity.min_person;
    const day = this.state.day;
    if (get(activity, 'product_category_slug') === 'local-assistants') {
      person = day;
      endDate = moment(endDate).add(day, 'days').format('YYYY-MM-DD');
    }
    if (isEmpty(carts)) {
      dispatch(createToken()).then(result => {
        if (get(result, 'meta.code') === 200) {
          dispatch(storeCart({ token: get(result, 'data.token'), category: get(activity, 'product_category_slug'), start_date: startDate, end_date: endDate, qty: person, product_id: activity.id })).then(res => {
            if (get(res, 'meta.code') === 200) {
              dispatch(getCartList(get(result, 'data.token'), 'schedule|ASC'));
              dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
              // Router.back()
            } else {
              dispatch(setErrorMessage(get(res, 'data')));
            }
          })
        }
      });
    } else {
      dispatch(storeCart({ token: carts.token, category: get(activity, 'product_category_slug'), start_date: startDate, end_date: endDate, qty: person, product_id: activity.id })).then(res => {
        if (get(res, 'meta.code') === 200) {
          dispatch(getCartList(carts.token, 'schedule|ASC'));
          dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
          // Router.back()
        } else {
          dispatch(setErrorMessage(get(res, 'data')));
        }
      })
    }
  }

  onScroll() {
    const scroll = document.scrollingElement.scrollTop;
    const content = document.getElementById('content-activity');
    const content1 = document.getElementById('description');
    const content2 = document.getElementById('choose-package');
    const content3 = document.getElementById('wywg');
    const content4 = document.getElementById('activity-information');
    const content5 = document.getElementById('term-n-condition');
    const stickyBar = document.getElementById('sticky-bar');
    const sideLeft = document.getElementById('side-left');
    const { stickyWidth } = this.state;
    if (content !== null &&
      content1 !== null &&
      content2 !== null &&
      content3 !== null &&
      content4 !== null &&
      content5 !== null &&
      stickyBar !== null &&
      sideLeft !== null) {
      const header = document.getElementById('header-tabs');
      if (content2.offsetTop <= scroll + 44 && content3.offsetTop > scroll + 44) {
        header.childNodes.forEach(function (el) {
          el.classList.remove('active');
        })
        header.childNodes[0].classList.add('active');
      }
      else if (content3.offsetTop <= scroll + 44 && content4.offsetTop > scroll + 44) {
        header.childNodes.forEach(function (el) {
          el.classList.remove('active');
        })
        header.childNodes[1].classList.add('active');
      } else if (content4.offsetTop <= scroll + 44 && content5.offsetTop > scroll + 44) {
        header.childNodes.forEach(function (el) {
          el.classList.remove('active');
        })
        header.childNodes[2].classList.add('active');
      }
      else if (content4.offsetTop <= scroll + 44) {
        header.childNodes.forEach(function (el) {
          el.classList.remove('active');
        })
        header.childNodes[3].classList.add('active');
      }
      if (stickyWidth === 0) {
        this.setState({ 'stickyWidth': stickyBar.offsetWidth });
      }
      if (scroll >= content1.offsetTop - 44 && scroll < (content.offsetHeight + (content.offsetTop - 44)) - (stickyBar.offsetHeight + 64)) {
        stickyBar.style.position = 'fixed';
        stickyBar.style.top = '76px';
        stickyBar.style.left = (((document.body.clientWidth - (sideLeft.offsetWidth + stickyWidth)) / 2) + sideLeft.offsetWidth) + 'px';
        stickyBar.style.width = (stickyWidth < 216 ? 216 : stickyWidth) + 'px';
      }
      else if (scroll >= (content.offsetHeight + (content.offsetTop - 44)) - (stickyBar.offsetHeight + 64)) {
        stickyBar.removeAttribute('style');
        stickyBar.style.position = 'absolute';
        stickyBar.style.top = content.offsetHeight - (stickyBar.offsetHeight + 64) + 'px';
        stickyBar.style.width = (stickyWidth < 216 ? 216 : stickyWidth) + 'px';
      } else {
        stickyBar.removeAttribute('style');
      }
    }
  }
  componentDidMount() {
    // $('.DayPicker').removeClass('DayPicker__hidden');
    const { activity, dispatch, slug } = this.props

    if (!isEmpty(activity)) {
      dispatch(getSchedules({ start_date: activity.upcomming_schedule, end_date: moment(activity.upcomming_schedule).add(8, 'days').format('YYYY-MM-DD'), qty: activity.min_person, id: activity.slug }));
    }
    document.addEventListener('scroll', this.onScroll.bind(this))
  }
  componentWillUnmount() {
    document.removeEventListener('scroll', this.onScroll.bind(this));
  }

  checkAvailable = async () => {
    const { activity, dispatch } = this.props;
    let { startDate, endDate, person } = this.state;
    // if(!isEmpty(startDate) && !isEmpty(endDate)){
    startDate = isEmpty(startDate) ? moment(activity.upcomming_schedule).format('YYYY-MM-DD') : startDate.format('YYYY-MM-DD');
    endDate = isEmpty(endDate) ? moment(activity.upcomming_schedule).add(5, 'days').format('YYYY-MM-DD') : endDate.format('YYYY-MM-DD');
    if (endDate.valueOf() >= startDate.valueOf()) {
      this.setState({ error: "" });
      const params = {
        id: activity.slug,
        qty: person <= activity.min_person ? activity.min_person : person,
        start_date: startDate,
        end_date: endDate,
      };
      this.setState({ isLoading: true });
      await dispatch(getSchedules(params)).then(result => {
        this.setState({ isLoading: false });
      });
    }
    // }else{
    //   this.setState({error:"Please Choose date"});
    // }
  }

  handleClose = () => {
    this.setState({ isModal: false });
  }

  render() {
    const { activity, schedules, isScheduleLoading, image_others } = this.props;

    const { startDate, endDate, focusedInput, showCalendar, error, isLoading, person, isSummary, selected, isModal, day } = this.state;
    return (
      <>
        <Head title={activity.meta_title} description={activity.meta_description} keyword={activity.meta_keyword} ogImage={urlImage(activity.image_path + '/large/' + activity.image_filename)} url={process.env.SITE_ROOT + '/p/1180-showcase-indonesia-full-pass-early-bird'} />
        <PageHeader type={"activity-detail"} image_others={image_others} isOpen={this.state.isPreview} handleOpen={this.handleOpen} background={urlImage(activity.image_path + '/large/' + activity.image_filename)} title={activity.name} caption={activity.short_description} />
        <section className="main-box sticky">
          <div className="container">
            <ul id="header-tabs">
              <li className="tabs-item"><a href="#choose-package">Choose Package</a></li>
              <li className="tabs-item"><a href="#wywg">What You'll Get</a></li>
              <li className="tabs-item"><a href="#activity-information">Activity Information</a></li>
              <li className="tabs-item"><a href="#term-n-condition">Terms and Conditions</a></li>
            </ul>
          </div>
        </section>
        <section id="content-activity" className="content-activity">
          <section id="description" className="bg-content-home">
            <div className="container">
              <section id="side-left" className="side-left information">
                <nav className="act-breadcrumb" aria-label="breadcrumb">
                  <ol className="breadcrumb">
                    <li className="breadcrumb-item" onClick={() => Router.push('/')}><a>Home</a></li>
                    {
                      activity.province &&
                      <li className="breadcrumb-item" onClick={() => Router.push(`/l/province/${activity.province.slug}`)}><a href="#">{activity.province.name}</a></li>
                    }
                    <li className="breadcrumb-item active" aria-current="page">{activity.name.length <= 76 ? activity.name : activity.name.substring(1, 76) + '...'}</li>
                  </ol>
                </nav>
                <ul className="act-icons list-unstyled">
                  <li>
                    <span className="ti-info-alt"></span>
                    <span>{activity.cancellation_policy.name}</span>
                  </li>
                  <li>
                    <span className="ti-flag"></span>
                    <span>{activity.product_type.replace(activity.product_type[0], activity.product_type[0].toUpperCase())} Group</span>
                  </li>
                  <li>
                    <span className="ti-pin"></span>
                    <span>{`${get(activity, 'province.name')}, ${get(activity, 'city.name')}`}</span>
                  </li>
                  <li>
                    <span className="ti-user"></span>
                    <span>Min {activity.min_person} Unit{activity.min_person > 1 && 's'}</span>
                  </li>
                </ul>
                <div className="act-short-description" dangerouslySetInnerHTML={{ __html: (activity.description ? activity.description : activity.short_description) }}>

                </div>
              </section>
              <section className="side-right">
                {
                  isSummary ?
                    !isMobileOnly && process.browser ? <Summary {...selected} /> :
                      <Modal show={isModal} onHide={() => this.handleClose()} backdrop dialogClassName="modal-dialog-centered modal-sm">
                        <a className="close" onClick={(e) => { e.preventDefault; this.handleClose(); }}><i className="ti-close" /></a>
                        <div className="modal-header">
                          <span className="text-title m0 h4">Booking Details</span>
                        </div>
                        <div className="modal-panel">
                          <div className="col-sm-12 pb1 pt1">
                            <Summary {...selected} />
                          </div>
                        </div>
                      </Modal>
                    :
                    <div id="sticky-bar" className="sticky-bar">
                      <div className="price">
                        <p>Start from: </p>
                        <div className="amount h4 text-primary">
                          {/* Rp {numeral(parseInt(activity.price_after,10)).format('0,0')} */}
                          Rp {numeral(parseInt(activity.price, 10)).format('0,0')}
                          {activity.price !== activity.price_before &&
                            <span className="last-price">
                              Rp {numeral(parseInt(activity.price_before, 10)).format('0,0')}
                            </span>
                          } <span className="text-person">/{activity.product_category_slug === 'local-assistants' ? 'day' : 'unit'}</span>

                        </div>
                      </div>
                      <a href="#choose-package" className="btn btn-primary btn-sm btn-block mt1">Choose Package</a>
                      <div className="side-bar-footer">
                        <div className="icon">
                          <span className="ti-timer"></span>
                          <span className={moment(activity.upcomming_schedule).format("YYYY-MM-DD") === moment().format('YYYY-MM-DD') || moment(activity.upcomming_schedule).format("YYYY-MM-DD") === moment().add(1, 'days').format('YYYY-MM-DD') ? 'text-success' : 'text-info'}>
                            {moment(activity.upcomming_schedule).format("YYYY-MM-DD") === moment().format('YYYY-MM-DD') &&
                              'Available Today'
                            }
                            {moment(activity.upcomming_schedule).format("YYYY-MM-DD") === moment().add(1, 'days').format('YYYY-MM-DD') &&
                              'Available Tommorow'
                            }
                            {moment().add(1, 'days').valueOf() < moment(activity.upcomming_schedule).valueOf() &&
                              `Available from ${moment(activity.upcomming_schedule).format("DD MMMM YYYY")}`
                            }
                          </span>
                        </div>
                        <div className="act-rating">
                          <span className="ti-shopping-cart-full"></span>
                          <span>20 kali dipesan</span>
                        </div>
                      </div>
                    </div>
                }
              </section>
            </div>
          </section>
          <section id="choose-package" className="bg-content-home">
            <div className="container">
              <section className="side-left choose-package">
                <h2 className="text-title">Choose Packages</h2>
                <div className="row">
                  <div className="col-md-4 col-xs-7 mb1">
                    <button className="btn btn-o btn-block btn-sm btn-primary" onClick={() => this.setState({ showCalendar: !showCalendar })}><span className="ti-calendar"></span> Choose Date</button>
                    {!isEmpty(error) &&
                      <div className='text-danger error-text'>
                        {error}
                      </div>
                    }
                    <div className="mt1" style={{ position: "absolute", zIndex: "1" }}>
                      {showCalendar && <DayPickerRangeController
                        isOutsideRange={date => date.isBefore(moment(activity.upcomming_schedule))}
                        startDate={startDate}
                        endDate={endDate}
                        numberOfMonths={1}
                        onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate }, () => { this.checkAvailable() })}
                        onFocusChange={focusedInput => this.setState({ focusedInput: focusedInput || 'startDate' })}
                        focusedInput={focusedInput}
                        onOutsideClick={() => this.setState({ showCalendar: false })}
                      />
                      }
                    </div>
                  </div>
                  <div className="col-md-4 col-xs-5 mb1">
                    <div className="select-person">
                      <select className="form-control" onChange={(e) => { this.setState({ person: e.target.value }, () => { this.checkAvailable() }) }}>
                        {map(range(activity.min_person, activity.max_person), (value, index) => (
                          <option className="ti-user" key={`person-${index}`} value={value}>{value} Unit{value > 1 && 's'}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  {/* {
                    get(activity,'product_category_slug') === 'local-assistants' &&
                    <div className="col-md-4 col-xs-12 mb1">
                      <div className="select-day">
                        <select className="form-control" onChange={(e) => {this.setState({day: e.target.value})}}>
                          { map(range(1,2), (value, index) => (
                            <option className="ti-calendar" key={`calendar-${index}`} value={value}>{value} Day{value > 1 && 's'}</option>
                          ))}
                        </select>
                      </div>
                    </div>
                  } */}

                  {/* <div className="col-md-4 col-xs-12 mb1">
                    <button className="btn btn-block btn-primary" onClick={this.checkAvailable}>Check Available</button>
                  </div> */}
                </div>
                <div className="panel">
                  {
                    isLoading && <div className="panel panel-bordered panel-info sr-btm mt1">
                      <div className="panel-body">
                        <p>Loading...</p>
                      </div>
                    </div>
                  }
                  {!isEmpty(schedules) && !isLoading &&
                    map(schedules, (item, index) => (
                      <div className="panel-body shadow" key={`schedule-${index}`}>
                        <div className="float-l">
                          <label className="w100">
                            {item.start_date === item.start_date ? moment(item.start_date).format("dddd, DD MMMM YYYY") : moment(item.start_date).format("dddd, DD MMMM YYYY") + ' - ' + moment(item.end_date).format("dddd, DD MMMM YYYY")}
                          </label>
                          <span className="s--activity-price amount h5 text-primary">Rp {numeral(parseInt(item.price, 10)).format('0,0')} {item.price !== item.price_before && <strike>Rp {numeral(parseInt(item.price_before, 10)).format('0,0')}</strike>}<span className="text-person">/{activity.product_category_slug === 'local-assistants' ? 'day' : 'unit'}</span></span>
                        </div>
                        <div className="float-r s--cart">
                          {get(item, 'available') == 0 ?
                            (<button className="s--cart-btn btn btn-md btn-primary" disabled>Sold</button>)
                            :
                            (<button className="s--cart-btn btn btn-md btn-primary" onClick={() => this.setState({ selected: { day, activity, startDate: item.start_date, endDate: get(activity, 'product_category_slug') === 'local-assistants' ? moment(item.end_date).add(day, 'days') : item.end_date, person: person > 0 ? person : activity.min_person, price: item.price, price_before: item.price_before, isMobileOnly, handleClose: this.handleClose, bookNow: this.bookNow, addToCart: this.addToCart }, isModal: true, isSummary: true })} >Select</button>)
                          }
                        </div>
                        {/* <div className="float-r s--cart btn-group">
                        (parseInt(get(item,'sold'),10) || 0) >= (parseInt(get(item,'available'),10) || 0)
                            <button className="s--cart-btn btn btn-md btn-primary">Book Now</button>
                            <button className="s--cart-btn btn btn-o btn-md btn-primary ml1" onClick={() => this.addToCart(item.start_date,item.end_date)}>Add Bucket List</button>
                        </div> */}
                      </div>
                    ))
                  }
                  {isEmpty(schedules) &&
                    <div className="panel panel-bordered panel-info sr-btm mt1">
                      <div className="panel-body">
                        <p>{isScheduleLoading ? "Loading..." : "Product schedule is not available"}</p>
                      </div>
                    </div>
                  }
                </div>
              </section>
            </div>
          </section>
          <section id="wywg" className="bg-content-home">
            <div className="container">
              <section className="side-left">
                <h2 className="text-title">What You'll Get</h2>
                <ul className="list-unstyled check-list">
                  {
                    map(activity.facilities.includes, (item, index) =>
                      (
                        <li key={`includes-${index}`}><i className="ti-check text-success" />{item}</li>
                      ))
                  }
                  {
                    map(activity.facilities.excludes, (item, index) =>
                      (
                        <li key={`excludes-${index}`}><i className="ti-close text-danger" />{item}</li>
                      ))
                  }
                  {/* {
                    map(facilities, (item, index) => 
                      <li key={item.id}><i className="ti-check text-success" />{item.name}</li>
                    )
                  }
                  {isEmpty(facilities) &&
                    <li>None</li>
                  } */}
                </ul>
              </section>
            </div>
          </section>
          <section id="activity-information" className="bg-content-home">
            <div className="container">
              <section className="side-left">
                <h2 className="text-title">Activity Information</h2>
                <div>
                  <ul className="list-unstyled list-activities">
                    {
                      map(activity.itineraries, (item, index) =>
                        (
                          <li key={`itinerary-${index}`}>
                            <div className="row no-gutter">
                              <div className="col-lg-12 col-md-12 col-sm-12">Day {item.day}</div>
                              <div className="col-lg-4 col-md-4 col-sm-6">{moment(item.start_time, "HH:mm:ss").format("HH:mm")} - {moment(item.end_time, "HH:mm:ss").format("HH:mm")}</div>
                              <div className="col-lg-12 col-md-12">
                                <div className="text-dark" dangerouslySetInnerHTML={{ __html: replaceEnter(item.description) }} />
                              </div>
                            </div>
                          </li>
                        ))
                    }
                  </ul>
                  <h2 className="text-title">Meeting Point</h2>
                  <p>{activity.meeting_point_address} {activity.meeting_point_note}</p>
                  <Maps isMarkerShown lat={activity.meeting_point_map.latitude} lng={activity.meeting_point_map.longitude} />
                </div>
              </section>
            </div>
          </section>
          <section id="term-n-condition" className="bg-content-home">
            <div className="container">
              <section className="side-left">
                <h2 className="text-title">Terms and Conditions</h2>
                <div dangerouslySetInnerHTML={{ __html: activity.terms_n_conditions }} />
              </section>
            </div>
          </section>
        </section>
      </>
    );
  }
}
const mapStateToProps = state => ({
  activity: state.activity.single,
  isPreview: state.activity.isPreview,
  image_others: state.activity.image_others,
  schedules: state.schedules.data,
  carts: state.cart.data,
  isScheduleLoading: state.schedules.loading,
})

export default compose(
  connect(mapStateToProps),
  withAuth(["PUBLIC"])
)(Activity);