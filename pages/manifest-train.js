import React, { Component, Fragment } from "react";
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field as ReduxField, reduxForm, change } from 'redux-form';
import { get, map, isEmpty } from 'lodash';
import Error from './_error';
import Head from '../components/head';
import { Link } from '../routes';
import validate from '../components/contact-detail/validate';
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';
import { getCountries } from '../stores/actions';
import { SingleDatePicker } from "react-dates";
import DatePicker from 'react-datepicker'
import Router from "next/router";
import numberWithComma from '../utils/numberWithComma';
import ModalContainer from '../components/modals/container'
import openSocket from 'socket.io-client'
import { modalToggle, trainCart, getFareDetail, checkoutDetails, deleteCart, setErrorMessage } from '../stores/actions';
import { 
  Grid,
  Row, 
  Col, 
  Panel, 
  FormGroup, 
  ControlLabel, 
  Form, 
  FormControl,
  InputGroup,
  Modal
} from "react-bootstrap";
import 'react-dates/initialize';
import moment from 'moment';
import 'moment/locale/id'
import Loading from '../components/shared/loading';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import Axios from "axios";
import "react-datepicker/dist/react-datepicker-cssmodules.css";
import "react-datepicker/dist/react-datepicker-cssmodules.min.css";
import "react-datepicker/dist/react-datepicker.css";
import "react-datepicker/dist/react-datepicker.min.css";
import getConfig from "next/config"
import { logEvent } from "../utils/analytics.js";
import { isMobile } from "react-device-detect";
const { publicRuntimeConfig } = getConfig()

const { SOCKET_URL } = publicRuntimeConfig
moment.locale('id');

const renderField = ({
    input, placeholder, type, meta: { touched, error }, title, caption
  }) => (
      <div className="form-group">
        {!isEmpty(title) && <span className="text-label" style={{color: '#636060', textTransform: 'none',fontSize: '0.9em' , fontFamily: 'Poppins Medium', fontWeight: 100}}>{title}</span>}
        <input className="form-control" {...input} placeholder={placeholder} type={type} style={{
          backgroundColor: 'transparent',
          border: '1px solid #E5E5E5',
          borderRadius: '6px',
          fontFamily: 'Poppins Medium', 
          fontWeight: 100
          }}/>
        {touched && <span className="text-danger">{error}</span>}
        {caption && <span style={{color: '#C4C4C4', fontSize: '.715em'}}>{caption}</span>}
      </div>
  );
  
  const renderSelectSalutation = ({
    input, placeholder, meta: { touched, error }, title
  }) => (
      <div className="form-group">
        {!isEmpty(title) && <span className="text-label" style={{color: '#636060', textTransform: 'none',fontSize: '0.9em' , fontFamily: 'Poppins Medium', fontWeight: 100}}>{title}</span>}
        <select className='form-control' {...input} 
        style={{
          backgroundColor: 'transparent',
          border: '1px solid #E5E5E5',
          borderRadius: '6px',
          fontFamily: 'Poppins Medium', 
          fontWeight: 100
          }}>
          <option>Title</option>
          <option value='Mr'>Mr.</option>
          <option value='Mrs'>Mrs.</option>
          <option value='Ms'>Miss.</option>
        </select>
        {touched && <span className="text-danger">{error}</span>}
      </div>
  );

  const renderSelectIDType = ({
    input, placeholder, meta: { touched, error }, title
  }) => (
      <div className="form-group">
        {!isEmpty(title) && <span className="text-label" style={{color: '#636060', textTransform: 'none',fontSize: '0.9em' , fontFamily: 'Poppins Medium', fontWeight: 100}}>{title}</span>}
        <select className='form-control' {...input} 
        style={{
          backgroundColor: 'transparent',
          border: '1px solid #E5E5E5',
          borderRadius: '6px',
          fontFamily: 'Poppins Medium', 
          fontWeight: 100
          }}>
          <option>Title</option>
          <option value='KTP'>Ktp</option>
          <option value='SIM'>Sim</option>
          <option value='PASSPORT'>Passport</option>
        </select>
        {touched && <span className="text-danger">{error}</span>}
      </div>
  );

  const renderSelectExt = ({
    input, placeholder, meta: { touched, error }, title, option, defaultValue
  }) => (
      <div className="form-group">
        {!isEmpty(title) && <span className="text-label" style={{color: '#636060', textTransform: 'none',fontSize: '0.9em' , fontFamily: 'Poppins Medium', fontWeight: 100}}>{title}</span>}
        <select className='form-control' {...input} style={{
          backgroundColor: 'transparent',
          border: '1px solid #E5E5E5',
          borderRadius: '6px',
          fontFamily: 'Poppins Medium', 
          fontWeight: 100
          }}>
          {
            map(option, (item,idx) => (
              <option key={`ext-${idx}`} value={item.area_code}>{item.area_code}</option>
            ))
          }
          
        </select>
        {touched && <span className="text-danger">{error}</span>}
      </div>
  );
  
class ManifesTrain extends Component {
    static async getInitialProps(context) {
		const { req, store, query } = context;
    await store.dispatch( getCountries([]) );
    
    return { query }
    }
    
    constructor(props) {
        super(props);
        this.state = { 
          Adult: 0,
          Infant: 0,
          token: null,
          customer: {},
          passengers: {},
          isLoading: false,
          adultValDOB: [],
          infantValDOB: [],
          formVal: false,
          emptyField: false,
          totalFare: 0,
          detail: []
         };
    }

    componentDidMount() {
      const { dispatch, form, change, profile} = this.props
      dispatch(change(form, 'emailUser', 'abcd'));
      if(profile) {
        
        dispatch(change(form, 'nameUser',profile.firstname + profile.lastname));
        dispatch(change(form, 'emailUser', profile.email));
        if(profile.phone){
          const phone = profile.phone.substring(3,profile.phone.length);
          dispatch(change(form, 'extUser', profile.phone.substring(0,3)));
          dispatch(change(form, 'phoneUser', phone));
        }
      }
      const { id, adult, infant } = this.props.query;
      let customer= {
        "salutation":"Mr",
        "email":"",
        "name":"",
        "phone":"",
        "country_code":"id"
      }
      let trains = []
      this.setState({isLoading: true})

      dispatch(trainCart(id))
      .then(res => {
        let pass = this.setPassengers(adult, infant)

        let eco = ['C', 'P', 'Q','S','Z']
        let buss = ['B', 'K', 'N', 'O', 'Y']
        let exe = ['A', 'H', 'I', 'J', 'X']
        for(let i = 0; i < res.data.details.length; i++) {
          let classEco = eco.find(cl => (cl == res.data.details[i].additionals.SubClass))
          let classBuss = buss.find(cl => (cl == res.data.details[i].additionals.SubClass))
          let classExe = exe.find(cl => (cl == res.data.details[i].additionals.SubClass))

          if(classEco) {
            res.data.details[i].additionals.ClassType = 'Economy'
          } else if(classBuss) {
            res.data.details[i].additionals.ClassType = 'Bussiness'
          } else if(classExe) {
            res.data.details[i].additionals.ClassType = 'Executive'
          }
          let name = res.data.details[i].product_name.split('|')
          
          res.data.details[i].TrainName = name[0]
        }
        
        this.setState({
          id,
          Adult: +adult,
          Infant: +infant,
          token: id,
          train: res.data,
          customer,
          isLoading: false,
          totalFare: res.data.total_paid,
          detail: res.data.details
        })
      }).catch(err => console.log(err))
    }

    setPassengers = async (adult, infant) => {
      let { dispatch, form } = this.props
      let passengers= {}
      let adultPass = []
      let infantPass = []
      let adultValDOB = []
      let infantValDOB = []

      for(let i = 0; i < adult; i ++) {
        await dispatch(change(form, 'ext_Adt_'+i,'+62'));
        let tempAdt = {
          "IdentityType":"",
          "IdentityNumber": "",
          "Title": "",
          "FirstName": "",
          "LastName": "",
          "BirthDate": "",
          "MobilePhone": "",
          "PaxType":"ADT",
        }
        adultValDOB.push('')
        adultPass.push(tempAdt)
      }

      for(let i = 0; i < infant; i ++) {  
        await dispatch(change(form, 'ext_Chd_'+i,'+62'));
        let tempInf = {
          "IdentityType":"Birth date",
          "IdentityNumber": "",
          "Title": "INF",
          "FirstName": "",
          "LastName": "",
          "BirthDate": "",
          "MobilePhone": "",
          "PaxType":"INF",
          "Sequence": 0
        }
        childValDOB.push('')
        childPass.push(tempChd)
      }

      passengers.Adt = adultPass
      passengers.Inf = infantPass

      this.setState ({
        passengers,
        adultValDOB,
        infantValDOB
      })
    }

    render() {
      const { country, handleSubmit } = this.props;
      const{ Adult, Infant, passengers, adultValDOB, infantValDOB, formVal, train, detail} =this.state;
        return (
           <React.Fragment>
               <Head 
                    title={"Manifest"} 
                    description={"Manifest Flight pigijo."} 
                    url={process.env.SITE_ROOT + '/manifest'} 
                />
                <PageHeader
                    background={"/static/images/rail_img.png"}
                    title="Your Booking"
                    caption="Fill in your details and review your booking" 
                />
                <Grid>
                  <Form >
                    <Row>
                      <Col xs={12} md={8}>
                        <section className="manifest-card" style={{padding: '3em 0 .5em 0', margin: 0}}>
                          <h4 className="text-title" style={{fontFamily: 'Poppins Bold', color: '#636060'}}>Detail Pemesan</h4>
                          <section className="card" style={{backgroundColor: '#F7F7F7',boxShadow: '15px 17px 20px #0000000D' ,borderRadius: '23px'}}>
                            <div className="manifest-form">
                              <Row>
                                <Col md={12}>
                                  <div className="row">
                                    <div className='col-lg-2 col-md-2 col-sm-2'>
                                      <ReduxField
                                        className="form-control input-lg"
                                        title="Title"
                                        component={renderSelectSalutation}
                                        name="salutationUser"
                                        placeholder="Title"
                                        
                                      />
                                    </div>
                                    <div className='col-lg-10 col-md-10 col-sm-10'>
                                      <ReduxField
                                      className="form-control"
                                      component={renderField}
                                      name="nameUser"
                                      type="text"
                                      title="Name"
                                      placeholder="Name"
                                      caption='Seperti di KTP/Paspor/SIM (tanpa tanda baca dan gelar).'
                                      />
                                    </div>
                                    <div className='col-lg-6 col-md-6 col-sm-6'>
                                      <ReduxField
                                        className="form-control"
                                        component={renderField}
                                        name="emailUser"
                                        type="email"
                                        title="Email"
                                        placeholder="Email"
                                        caption='Detail pemesanan akan kami kirim ke email ini.'/>
                                    </div>
                                    <div className='col-lg-6 col-md-6 col-sm-6'>
                                      <span className='text-label' style={{color: '#636060', textTransform: 'none',fontSize: '0.9em' , fontFamily: 'Poppins Medium', fontWeight: 100}}>Phone Number</span>
                                      <div className='row sm-gutter form-groups'>
                                        <div className='col-lg-3 col-md-3 col-sm-4 col-xs-4'>
                                          <ReduxField
                                          className="form-control"
                                          component={renderSelectExt}
                                          option={country}
                                          type="text"
                                          name="extUser"
                                          placeholder="ex. +62"/>
                                        </div>
                                        <div className='col-lg-9 col-md-9 col-sm-8 col-xs-8'>
                                          <ReduxField
                                            className="form-control"
                                            component={renderField}
                                            name="phoneUser"
                                            type="text"
                                            placeholder="ex. 8123xxx"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </Col>
                              </Row>
                            </div>
                          </section>

                          <h4 className="text-title" style={{fontFamily: 'Poppins Bold', color: '#636060', marginTop: '2em'}}>Detail Penumpang</h4>
                          <section className="card" style={{backgroundColor: '#F7F7F7',boxShadow: '15px 17px 20px #0000000D' ,borderRadius: '23px'}}>
                            <div className="manifest-form">
                              <Row>
                                {
                                   !passengers.Adt ? null :( passengers.Adt.map((adult, id) => (
                                      <Col md={12} key={id}>
                                        <div className="row">
                                          <div className='col-lg-12 col-md-12 col-sm-12' style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '1.5em 1em'}}>
                                            <span style={{fontFamily: 'Poppins Bold', color: '#636060'}}>Penumpang {id+1} : Dewasa</span>
                                            {
                                              id == 0 ? (
                                                <div>
                                                  <span style={{fontSize:'.8em', color: '#636060'}}>Sama dengan pemesan  <input type="checkbox" className="styled-checkbox" style={{width: 'auto'}}/></span>
                                                </div>
                                              ) : null
                                            }
                                            
                                          </div>

                                          <div className='col-lg-2 col-md-2 col-sm-2'>
                                            <ReduxField
                                              className="form-control input-lg"
                                              title="Title"
                                              component={renderSelectSalutation}
                                              name={`Title_Adt_${id}`}
                                              placeholder="Title"
                                            />
                                          </div>
                                          <div className='col-lg-6 col-md-6 col-sm-6'>
                                            <ReduxField
                                            className="form-control"
                                            component={renderField}
                                            name={`FirstName_Adt_${id}`}
                                            type="text"
                                            title="Firstname"
                                            placeholder="Firstname"
                                            caption='Seperti di KTP/Paspor/SIM (tanpa tanda baca dan gelar).'
                                            />
                                          </div>
                                          <div className='col-lg-4 col-md-4 col-sm-4'>
                                            <ReduxField
                                            className="form-control"
                                            component={renderField}
                                            name={`LastName_Adt_${id}`}
                                            name="contactName"
                                            type="text"
                                            title="Lastname"
                                            placeholder="Lastname"
                                            />
                                          </div>
                                          <div className='col-lg-6 col-md-6 col-sm-6'>
                                            <span className='text-label'>Phone Number</span>
                                            <div className='row sm-gutter form-groups'>
                                              <div className='col-lg-3 col-md-3 col-sm-4 col-xs-4'>
                                                <ReduxField
                                                  className="form-control"
                                                  component={renderSelectExt}
                                                  option={country}
                                                  type="text"
                                                  placeholder="ex. +62"
                                                  name={`ext_Adt_${id}`}/>
                                              </div>
                                              <div className='col-lg-9 col-md-9 col-sm-8 col-xs-8'>
                                                <ReduxField
                                                  className="form-control"
                                                  component={renderField}
                                                  name={`phone_Adt_${id}`}
                                                  type="text"
                                                  placeholder="ex. 8123xxx"/>
                                              </div>
                                            </div>
                                          </div>
                                          <Col lg={6} md={6} sm={6}>
                                            <div>
                                              <span className="text-label">Date Of Birth</span>
                                            </div>
                                            <input 
                                              className='dob-form'
                                              type="date" 
                                              name="dob"
                                              // value={adult.BirthDate}
                                              // onChange={e => this.onChangeDOB('Adt', id,e.target.value)}
                                              />
                                              <span style={{color: 'red'}}><small>{adultValDOB[id]}</small></span>
                                          </Col>
                                          <div className='col-lg-6 col-md-6 col-sm-6'>
                                            <ReduxField
                                              className="form-control input-lg"
                                              title="ID Type"
                                              component={renderSelectIDType}
                                              name={`IdType_Adt_${id}`}
                                              name="salutationUser"
                                              placeholder="ID Type"
                                            />
                                          </div>
                                          <div className='col-lg-6 col-md-6 col-sm-6'>
                                            <ReduxField
                                            className="form-control"
                                            component={renderField}
                                            name={`IdNumber_Adt_${id}`}
                                            title="ID Number"
                                            placeholder="ID Number"
                                            caption='For passenger under 18 years old, please fill with your birthdate (ddmmyyyy).'
                                            />
                                          </div>
                                        </div>
                                      </Col>
                                    ))
                                  )
                                }
                                
                              </Row>
                            </div>
                          </section>
                        </section>
                      </Col>
                    
                      <Col xs={12} md={4}>
                        <section className="manifest-card" style={{padding: '3em 0', margin: 0}}>
                          <h4 className="text-title" style={{fontFamily: 'Poppins Bold', color: '#636060'}}>Rincian Harga</h4>
                          <section className="card" style={{backgroundColor: '#F7F7F7',boxShadow: '15px 17px 20px #0000000D' ,borderRadius: '23px', padding: '0 0 1em 0'}}>
                            <div style={{width: '100%', borderTopLeftRadius: '23px', borderTopRightRadius: '23px', backgroundColor: '#FF5B00', padding: ' 0 2em'}}>
                              <h5 className="text-title " style={{fontFamily: 'Poppins Bold', color: 'white', fontWeight: 100, textTransform: 'uppercase', margin: '5px 0'}}>Pergi</h5>
                            </div>
                            <div style={{
                              width: '100%',
                              padding: '1em 2em'
                            }}>
                              <span style={{color: '#636060', fontSize: '1.2em'}}>{ detail.length > 0 ? detail[0].additionals.Origin : ''} - {detail.length > 0 ? detail[0].additionals.Destination : ''}</span>
                              <p style={{color: '#636060', fontSize: '.8em', lineHeight: '.85em'}}>{moment(detail.length > 0 ? detail[0].additionals.DepartureDate : '').format('LL')} | {detail.length > 0 ? detail[0].additionals.DepartureTime : ''}</p>

                              <span style={{color: '#636060', margin: '2px'}}>{detail.length > 0 ? detail[0].TrainName : ''}</span>
                              <p style={{color: '#636060', margin: '2px'}}>{ detail.length > 0 ? detail[0].additionals.ClassType : ''}  (Subclass { detail.length > 0 ? detail[0].additionals.SubClass : ''})</p>
                              {
                                Infant > 0 ? (<p style={{color: '#636060', margin: '2px'}}>Infant (x{Infant})</p>) : null
                              }
                              
                              <span style={{color: '#636060', margin: '2px'}}>Dewasa (x{Adult})</span>
                              <span style={{color: '#636060', position: 'absolute', right: 30}}>Rp {numberWithComma(detail.length > 0 ? detail[0].total_paid : 0)}</span>
                            </div>
                            
                            {
                              detail.length > 1 ? (
                                <React.Fragment>
                                  <div style={{width: '100%', backgroundColor: '#FF5B00', padding: ' 0 2em'}}>
                                    <h5 className="text-title " style={{fontFamily: 'Poppins Bold', color: 'white', fontWeight: 100, textTransform: 'uppercase', margin: '5px 0'}}>Pulang</h5>
                                  </div>
                                  <div style={{
                                    margin: '1em 2em',
                                    paddingBottom: '1.5em'
                                  }}>
                                    <span style={{color: '#636060', fontSize: '1.2em'}}>{ detail.length > 0 ? detail[1].additionals.Origin : ''} - {detail.length > 0 ? detail[1].additionals.Destination : ''}</span>
                                    <p style={{color: '#636060', fontSize: '.8em', lineHeight: '.85em'}}>{moment(detail.length > 0 ? detail[1].additionals.DepartureDate : '').format('LL')} | {detail.length > 0 ? detail[1].additionals.DepartureTime : ''}</p>

                                    <span style={{color: '#636060', margin: '2px'}}>{detail.length > 0 ? detail[1].TrainName : ''}</span>
                                    <p style={{color: '#636060', margin: '2px'}}>{ detail.length > 0 ? detail[1].additionals.ClassType : ''}  (Subclass { detail.length > 0 ? detail[1].additionals.SubClass : ''})</p>
                                    {
                                      Infant > 0 ? (<p style={{color: '#636060', margin: '2px'}}>Infant (x{Infant})</p>) : null
                                    }
                                    <span style={{color: '#636060', margin: '2px'}}>Dewasa (x{Adult})</span>
                                    <span style={{color: '#636060', position: 'absolute', right: 30}}>Rp {numberWithComma(detail.length > 0 ? detail[1].total_paid : 0)}</span>
                                  </div>
                                </React.Fragment>
                              ) : null
                            }
                            

                            <div style={{
                              margin: '1em 2em',
                              display: 'flex',
                              justifyContent: 'space-between',
                              borderTop: '2px dashed #FF5B00',
                            }}>
                              <h5 className="text-title " style={{fontFamily: 'Poppins Bold', color: '#FF5B00', fontWeight: 100, textTransform: 'uppercase', margin: '5px 0'}}>Total</h5>
                              <h5 className="text-title " style={{fontFamily: 'Poppins Bold', color: '#FF5B00', fontWeight: 100, textTransform: 'uppercase', margin: '5px 0'}}>Rp 180.000</h5>
                            </div>
                          </section>
                        </section>
                      </Col>

                      <Col xs={12} md={8}>
                      <section className="manifest-card" style={{padding: '0 0 3em 0', margin: 0}}>
                        <Row style={{display: 'flex', justifyContent: 'flex-end'}}>
                          <div className="col-lg-4 col-md-4 col-sm-12" style={{paddingTop: '1.75em'}}>
                            <button
                                type='submit'
                                className='btn btn-primary btn-o btn-fix btn-blue btn-block btn-flight-search'
                                style={{borderRadius: '24px'}}
                            >
                                Pilih Kursi
                            </button>
                          </div>
                          <div className="col-lg-4 col-md-4 col-sm-12" style={{paddingTop: '1.75em'}}>
                            <button
                                type='submit'
                                className='btn btn-block btn-primary btn-fix btn-blue btn-flight-search'
                                style={{borderRadius: '24px'}}
                            >
                                Lanjutkan
                            </button>
                          </div>
                        </Row>
                      </section>
                            
                      </Col>
                    </Row>
                  </Form>
                </Grid>
                <style jsx>{`
                  .dob-form {
                    background-color: transparent;
                    border: 1px solid rgb(229, 229, 229);
                    border-radius: 6px;
                    padding: 0 1.2em;
                    line-height: 1.4em;
                    margin: 0 0 1em;
                    height: 34px;
                  }
                `}</style>
           </React.Fragment> 
        );
    }
}

const mapStateToProps = state => ({
  profile: state.auth.profile,
  type: state.modal.type,
  isOpen: state.modal.isOpen,
  message: state.modal.message,
  country: state.country.data,
})

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

ManifesTrain = reduxForm({
  form: "manifest_train",
  initialValues: {
    salutationUser: 'Mr',
    extUser: "+62",
  }
})(ManifesTrain);

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withAuth(["PUBLIC"])
)(ManifesTrain);