import React, { Component } from 'react';
import Head from '../components/head';
import PageHeader from '../components/page-header';
import { compose } from "redux";
import withAuth from '../_hoc/withAuth';
import ActivityCategory from '../components/home/activity-category';
import { getActivities, getActivityPromos,getDetailCategory, getPlaces } from '../stores/actions';


class LocalExperiences extends Component {
    render() {
        return (
            <React.Fragment>
                <Head title={"Local Experiences"} description={"Local Experiences"} url={process.env.SITE_ROOT+'/local-experiences'}/>
                <PageHeader background={"/static/images/img02.jpg"} title={`Local Experiences`} caption=""/>
                <div className='container'>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <p className='text-center'>Under Construction, thank you for your interest, we are currently working on this page. </p>
                        {/* <ActivityCategory loopTo={[1,1,1]} classProps={[4,6,12]} title="Local Experiences You'll Love" moreLoading={recommended.isMoreLoading} isLoading={recommended.isLoading} isReload={recommended.isReload} data={recommended.data} link={"recommended"} model={1} moreBtn moreData={this.moreCategory} total={recommended.total} /> */}
                        </div>
                    </div>
                    <style jsx>{`
                        ol.text-ol li{
                            line-height: 30px;
                        }
                    `}</style>
                </div>
            </React.Fragment>
        );
    }
}

export default compose(
    withAuth(["PUBLIC"])
  )(LocalExperiences);