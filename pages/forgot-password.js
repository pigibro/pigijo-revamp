import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import Loading from '../components/shared/loading';
import React from 'react';
import withAuth from '../_hoc/withAuth';
import { submitNewPass, resendVerification } from '../stores/actions';

class RegisterVerification extends React.Component {
    static async getInitialProps({ query }) {
      // const { verifyToken } = query;
      return { query }
    }
    constructor(props) {
        super(props)
        this.state = {
            success: '',
            password: null,
            password_confirmation: null,
            error: '',
            isLoading: false,
        }
        this.handleOnSubmit = this.handleOnSubmit.bind(this)
    }
    handleOnSubmit = (e) => {
        e.preventDefault();
        const {query} = this.props;
        this.setState({isLoading: true});
        const params = {
          token: query.verifyToken,
          password: this.state.password,
          confirm_password: this.state.password_confirmation,
          email: null,
        };
        this.props.dispatch(submitNewPass(params)).then((result) => {
          if(get(result,'meta.code') === 200){
            this.setState({success: get(result,'meta.message')});
            this.setState({error: ''});
          }else{
            this.setState({error: get(result,'meta.message')});
            this.setState({success: ''});
          }
          this.setState({isLoading: false});
        });
    }

    render(){
      const {isLoading, error, success} = this.state;
      return (
        <>
          <Head title={"Forgot Password"}/>
          <PageHeader background={"/static/images/img05.jpg"} title={`Forgot Password`}/>
          <section className="content-wrap">
              <div className="container">
                <div className="row">
                  <div className="col-md-6 col-md-offset-3">
                    <p>Please verify your email address to improve our service for use this website.</p>
                    {
                      !isLoading &&
                      <div className='panel panel-bordered panel-primary sr-btm mt2'>
                        <div className='panel-body'>
                          {error !== '' &&
                            <div className='text-danger error-text'>
                              {error}
                            </div>
                          }
                          {success !== '' &&
                            <div className='text-success success-text'>
                              {success}
                            </div>
                          }
                           <form onSubmit={this.handleOnSubmit}>
                            <div className="form-group">
                              <span className="text-label">Password</span>
                              <input
                                className="form-control"
                                type="password"
                                placeholder="New password"
                                value={this.state.password}
                                onChange={e =>
                                  this.setState({ password: e.target.value })
                                }
                                required
                              />
                            </div>
                            <div className="form-group">
                              <span className="text-label">
                                Password Confirmation
                              </span>
                              <input
                                className="form-control"
                                type="password"
                                placeholder="Password confirmation"
                                value={this.state.password_confirmation}
                                onChange={e =>
                                  this.setState({
                                    password_confirmation: e.target.value
                                  })
                                }
                                required
                              />
                            </div>
                            <button
                              className="btn btn-block btn-primary"
                              type="submit"
                            >
                              Reset Password
                            </button>
                          </form>
                        </div>
                      </div>
                    }
                  </div>
                </div>
              </div>
          </section>
          {
            isLoading && <Loading/> 
          }
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(RegisterVerification);