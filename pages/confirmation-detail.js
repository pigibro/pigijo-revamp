import React, { Component } from "react";
import { connect } from 'react-redux'
import { compose } from "redux";
import Content from "../components/contact-detail/content";
import SideBar from "../components/shared/sidebar";
import Head from "../components/head";
import PageHeader from "../components/page-header";
import withAuth from "../_hoc/withAuth";
import {isMobile} from 'react-device-detect'

 class ConfirmationDetail extends Component {
	state={
		chkbox:false,
		coupon:'',
		passenger:[]
	}

	componentWillReceiveProps(props){
    // let contact = descriptFunc('contact_detail','contact_detail');
    // let traveller = this.props.cusData.length !== 0 ? props.cusData : contact.detail;
    // this.setState({passenger:traveller})
  }

  toggleModal=(e)=>(nani)=>{
  //   e.preventDefault();
  //   const history = createBrowserHistory();
  //   // Get the current location.
  //   const location = history.location;
  //   history.push(location.pathname, { some: "state" });

    // const bool = !this.props.isOpen
    // this.props.dispatch(ModalToggle(bool))
    // this.props.dispatch(ModalType(nani))
  }
  handleChangeChk=(e)=>{
    this.setState({chkbox:!this.state.chkbox})
  }
  handleChangeCoupon=(e)=>{
    this.setState({
      coupon: e.target.value
    })
  }
  handleSubmitCoupon=(e)=>{
    e.preventDefault()
    this.props.dispatch(sendCoupon(this.state.coupon))
  }

	render() {
		let travellerDetail=[]

    // event start
      for (let i = 0; i < 3;i++) {
        travellerDetail.push(
          <div key={i}>
            <div className="col-lg-6 col-md-6 col-sm-6">
              <span className="text-label">First Name</span>
              <div className="form-group">
                <div className="text-dark">{'a.firstname[i]'}</div>
              </div>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6">
              <span className="text-label">Last Name</span>
              <div className="form-group">
                <div className="text-dark">{'a.lastname[i]'}</div>
              </div>
            </div>
          </div>
        )
    }

		return (
			<>
				<Head
					title={"Eu nostrud amet esse enim est esse commodo reprehenderit."}
					description={
						"Culpa tempor eu elit duis eu occaecat culpa culpa. Commodo anim commodo occaecat do consequat consequat Lorem laborum qui duis reprehenderit nisi Lorem ullamco. Dolore cupidatat proident esse irure cillum deserunt. Sit et tempor sint duis occaecat ea culpa."
					}
					url={process.env.SITE_ROOT + "/p/1180-showcase-indonesia-full-pass-early-bird"}
				/>
				<PageHeader
					background={"/static/images/img05.jpg"}
					title="sfsdfsdfsdfsdf"
					caption="Culpa ad amet non aute do sunt velit veniam et."
				/>
				<section className="content-wrap">
					<div className="container">
						<div className="row">
							<div className="col-lg-8 col-md-8 col-sm-8">
							<div className="form-wrapper">
                  {/* <Link className="btn btn-sm btn-primary pull-right" to="/finalize-itinerary/booking">Edit</Link> */}
                  <div className="main-title">
                    <h3 className="text-title">Contact Person</h3>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-label">First Name</span>
                      <div className="form-group">
                        <div className="text-dark">{'userData===null?"":userData.contact_firstname'}</div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-label">Last Name</span>
                      <div className="form-group">
                        <div className="text-dark">{'userData===null?"":userData.contact_lastname'}</div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-label">Email Address</span>
                      <div className="form-group">
                        <div className="text-dark">{'userData===null?"":userData.contact_email'}</div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-label">Phone Number</span>
                      <div className="form-group">
                        <div className="text-dark">{'userData===null?"":userData.contact_phone'}</div>
                      </div>
                    </div>
                  </div>{/* row */}

                  <div className="row">
                    <div className="col-lg-8 col-md-8 col-sm-8">
                      <div className="form-wrapper">
                        {/* <Link className="btn btn-sm btn-primary pull-right" to="/finalize-itinerary/booking">Edit</Link> */}
                        <div className="main-title">
                          <h3 className="text-title">Participant Information</h3>
                        </div>
                        {
                          travellerDetail.length !== 0 ? (travellerDetail):(<Loader />)
                        }
                      </div>
                    </div>
                  </div>{/* row */}

                </div>{/* form-wrapper */}

                <div className="panel" style={{marginTop:"20px"}}>
                  <div className="panel-body">
                    <div className="flex-row flex-center">
                      <div className="h5">Add Promo Code</div>
                      {/* <div className="switch-checkbox mla add-promo">
                        <input type="checkbox" id="c1" defaultChecked={this.state.chkbox} onChange={this.handleChangeChk}/>
                        <label htmlFor="c1" />
                      </div> */}
                    </div>
                    <div className="row sm-gutter mt2" id="promo">
                    {/* <div className="row sm-gutter mt2" id="promo" style={this.state.chkbox ? {display: 'block'} : {display: 'none'} }> */}
                      <div className="col-md-9 col-xs-12">
                        <input className="form-control" type="text" placeholder="ex. BERANITRAVELING" value={this.state.coupon} onChange={this.handleChangeCoupon}/>
                      </div>
                     
                      <div className="col-md-3 col-xs-12">
                        <button className="btn btn-block btn-default" type="submit" onClick={this.handleSubmitCoupon}>Apply Promo</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row mt2">
                  <div className="col-lg-7 col-md-7 col-sm-12">
                    By Clicking this button, you acknowledge that you have read and agreed to the <a href="" onClick={e=>this.toggleModal(e)("terms")}>Terms &amp; Condition</a> &amp; <a href="" onClick={e=>this.toggleModal(e)("policy")}>Privacy Policy</a> of Pigijo
                  </div>
                  <div className="col-lg-5 col-md-5 col-sm-12">
                    <a className="btn btn-block btn-primary  btn-fix btn-blue" href="" onClick={e=>this.toggleModal(e)("bookProceed")}>Continue to payment</a>
                  </div>
                </div>
							</div>
							<SideBar
								title="Trip Details"
								destination="Jakarta"
								startDate="19 May 2019"
								endDate="23 May 2019"
								person={2}
							/>
						</div>
					</div>
				</section>
			</>
		);
	}
}
export default compose(connect(), withAuth(["PUBLIC"]))(ConfirmationDetail);