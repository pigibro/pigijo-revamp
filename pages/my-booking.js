import React from "react";
import { compose } from "redux";
import withAuth from "../_hoc/withAuth";
import Headers from "../components/profile/_component/header";
import Content from "../components/profile/mybooking/index";
import { connect } from "react-redux";
import { getBooking } from "../stores/actions/myBookingAction";
import { IMAGE_URL } from "../stores/actionTypes";

class MyBooking extends React.Component {
	componentWillMount() {
		this.props.dispatch(getBooking({page: 1},this.props.token));
	}
	render() {
		const { token, asPath, list, item, details, pagination, isLoading, joToken } = this.props;

		return (
			<div>
				<Headers />
				<Content 
					joToken={joToken}
					token={token}
					path={asPath}
					list={list}
					item={item}
					details={details}
					pagination={pagination}
					url={IMAGE_URL}
					isLoading={isLoading}
				/>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	list: state.myBooking.list,
	details: state.myBooking.details,
	item: state.myBooking.item,
	pagination: state.myBooking.pagination,
	isLoading: state.myBooking.isLoading
});

export default compose(
	connect(mapStateToProps),
	withAuth(["PRIVATE"]),
)(MyBooking);
