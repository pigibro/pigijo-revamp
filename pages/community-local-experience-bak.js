import { compose } from 'redux'
import { connect } from 'react-redux'
import React from 'react'
import { get, isEqual, isEmpty } from 'lodash';
// import { SingleDatePicker } from 'react-dates';
// import InputRange from "react-input-range";

import Loading from '../components/home/loading-flashsale';
import InfiniteScroll from 'react-infinite-scroller';
import "react-input-range/lib/css/index.css";

import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';

import { httpReqGetDetailCommunity } from "../stores/actions";
import withAuth from '../_hoc/withAuth'
import Head from '../components/head';
import PageHeader from '../components/page-header';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import { FormControl, FormGroup, InputGroup, Glyphicon } from "react-bootstrap";
import { Link } from "../routes";
import { getLocalExp } from "../stores/actions";
import { getLocations } from '../stores/actions';
import HeaderDetail from "../components/community/header-detail";

class CommunityLocalExperience extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          filter: "",
          location: [],
          params: {
            page: 1,
            city: null,
            province: null,
            area: null,
            search: null,
            category: null
          },
          hasMore: true,
          page: 1,
          localExpState: [],
          communities: {
            isLoading: false,
            data: {},
            page: 1,
            isReload: false
          },
        };
    }

    static async getInitialProps({ query }) {
      const { slug } = query;
      return { slug: slug }
    }

    async componentDidMount() {
      const { page, communities} = this.state;

      if (isEmpty(communities.data)) {
        this.setState({
          communities: {
            isLoading: true,
            data: [],
            isReload: false
          }
        });

        this.props
          .dispatch(httpReqGetDetailCommunity(this.props.slug))
          .then(result => {
            if (get(result, "meta.code") === 200) {
              this.setState({
                communities: {
                  isLoading: false,
                  data: get(result, "data"),
                  isReload: false
                }
              });
            } else {
              this.setState({
                communities: {
                  isLoading: false,
                  data: {},
                  isReload: true
                }
              });
            }
          });
      }
      
      await this.props.dispatch(getLocalExp(this.props.slug, { per_page: 8, page }))
      .then(result => {
        this.setState(prevState => {
          const { localExpState } = prevState;
          const newData = !isEqual(localExpState, result.data) ? result.data : [];
          return {
            localExpState: [...localExpState, ...newData]
          }
        })
      });
      this.props.dispatch(getLocations()).then(result => {
        if (get(result, "meta.code") === 200) {
          this.setState({
            location: get(result, "data")
          });
        }
      });;
    }

    handleLocation = value => {
      if (value.length > 0) {
        let params = this.state.params;
  
        if (value[0].type === "Area") {
          const area = { area: value[0].id, city: null, province: null };
          params = { ...params, ...area };
        } else if (value[0].type === "Kabupaten" || value[0].type === "Kota") {
          const city = { city: value[0].id, area: null, province: null };
          params = { ...params, ...city };
        } else {
          const province = { province: value[0].id, area: null, city: null };
          params = { ...params, ...province };
        }
  
        this.setState({ params }, () => {
          this.getLocalExp(this.props.slug, params);
        });
      }
    };

    handleSearch = e => {
      let params = this.state.params;
      const search = { search: e.target.value };
      params = { ...params, ...search };
  
      this.setState({ params });
    };
  
    searchFilter = () => {
      let params = this.state.params;
      this.getLocalExp(this.props.slug, params);
    };

    loadMore = async () => {
      const { loading } = this.props;
      if (!loading) {
        const { page } = this.state;
        await this.props.dispatch(getLocalExp(this.props.slug, { per_page: 8, page: page+1 }))
        .then(async result => {
          await this.setState(prevState => {
            const { localExpState, page } = prevState;
            const newData = localExpState.length > 0 && !isEqual(localExpState, result.data) ? result.data : [];
            return {
              page: page+1,
              localExpState: [...localExpState, ...newData]
            }
          })
          if(page === get(result,'pagination.last_page')){
            this.setState({hasMore: false});
          }
        });
      }
    }

    render(){
      const { filter, location, hasMore, localExpState, communities } = this.state;
      const { cityReducer } = this.props;
      const { data } = cityReducer;
      const detailComm = communities.data.detail_community
        ? communities.data.detail_community
        : "";
      return (
        <>
          <Head
            title={"Community Local Experiences"}
            description={"Hubungi Pusat Bantuan Pigijo untuk mendapatkan informasi lengkap dan jelas mengenai rencana perjalanan anda, status pembayaran, dan cara bergabung menjadi partner pigijo."}
            url={process.env.SITE_ROOT+'/community/local-experiences'}
          />
          <PageHeader
            background={"/static/images/img05.jpg"}
            title={`Community Local Experiences`}
          />
          <HeaderDetail
            comm={communities.data}
            slug={this.props.slug}
            detailComm={detailComm}
          />
          {/* <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-lg-4 col-md-4">
                  <img
                    className='img-responsive img-circle'
                    alt=''
                    src="https://www.pigijo.com/static/media/step1.342121fc.png"
                    style={{
                      height: '200px',
                      width: '200px',
                      margin: '0 auto',
                    }}
                  />
                </div>
                <div className="col-lg-5 col-md-5">
                  <h2 className="h4 mb1">
                    Traveller's / Community's ID
                  </h2>
                  <p>
                    Category
                  </p>
                  <p>
                    # Member
                  </p>
                </div>
                <div className="col-lg-3 col-md-3">
                  <div>
                    <button type="button" className="btn btn-info">
                      Join +
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section> */}
          {/* Menu */}
          {/* <section>
            <div className="container">
              <div className="row flex-row flex-center text-center">
                <div className="col-lg-3 col-md-3">
                  <Link to="#">Home</Link>
                </div>
                <div className="col-lg-3 col-md-3">
                  <Link to="#">Events</Link>
                </div>
                <div className="col-lg-3 col-md-3">
                  <Link to="#">Local Experiences</Link>
                </div>
                <div className="col-lg-3 col-md-3">
                  <Link to="#">Places</Link>
                </div>
              </div>
              <hr />
            </div>
          </section> */}
          {/* <section>
            <div className="container">
              <div className="row flex-row flex-center" style={{ margin: '20px' }}>
                <div className="col-lg-5 col-lg-offset-5 input-group-lg">
                  <input type="text" className="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2" />
                </div>
                <div className="col-lg-2">
                  <button className="btn btn-outline-info" type="button" id="button-addon2">
                    Search
                  </button>
                </div>
              </div>
            </div>
          </section> */}
          <section>
            <div className="container">
              <div className="row" style={{ margin: '20px' }}>
              {/* fiter start */}
              <div className="col-lg-2 col-md-2 col-sm-12 hidden-xs">
                <div className="community-filter">
                  <ul
                    id="header-tabs"
                    className="list-unstyled"
                    style={{ padding: "10px" }}
                  >
                    <li className="tabs-item">
                      <button
                        className="btn btn-sm btn-o btn-light"
                        style={{ width: "100%" }}
                        onClick={() => this.setState({ filter: "location" })}
                      >
                        Location
                      </button>
                      <div
                        ref="location"
                        className={`p__filter ${
                          filter === "location" ? "show" : "hide"
                        }`}
                      >
                        <SearchAutoComplete
                          id="destination"
                          key={"select-single"}
                          index={0}
                          data={location || []}
                          placeholder="Select City"
                          labelKey="name"
                          onChange={this.handleLocation}
                          bsSize="large"
                        />
                      </div>
                    </li>
                    <br />
                    <li className="tabs-item">
                      <button
                        className="btn btn-sm btn-o btn-light"
                        style={{ width: "100%" }}
                        onClick={() => this.setState({ filter: "search" })}
                      >
                        Search
                      </button>
                      <div
                        ref="search"
                        className={`p__filter ${
                          filter === "search" ? "show" : "hide"
                        }`}
                      >
                        <div className="cnt-filter">
                          <div className="cnt-body">
                            <FormGroup>
                              <InputGroup>
                                <FormControl
                                  bsSize="small"
                                  type="text"
                                  onChange={this.handleSearch}
                                  placeholder="Search ..."
                                />
                                <InputGroup.Addon>
                                  <a onClick={this.searchFilter}>
                                    <Glyphicon glyph="search" />
                                  </a>
                                </InputGroup.Addon>
                              </InputGroup>
                            </FormGroup>
                          </div>
                        </div>
                      </div>
                    </li>
                    <br />
                  </ul>
                </div>
              </div>
              {/* filter end */}
              
                <div className="col-lg-10 col-md-10 col-sm-12">
                  <div
                    className="card-columns"
                    // style={{ height: '1500px', overflowY: 'auto' }}
                    >
                    <InfiniteScroll
                      pageStart={0}
                      initialLoad
                      loader={<div className="loader" style={{clear: 'both'}} key={0}>Loading ...</div>}
                      useWindow={false}
                      loadMore={this.loadMore}
                      hasMore={hasMore}
                      threshold={1000}
                    >
                      {
                        localExpState && localExpState.map((dataLocalExp, index) => (
                          <div key={`${dataLocalExp.id}-${index}`} className="card">
                            <img
                              src={
                                "https://s3-ap-southeast-1.amazonaws.com/pigijo/" +
                                dataLocalExp.image_path +
                                "/" +
                                dataLocalExp.image_filename
                              }
                              className="card-img-top"
                              alt="..."
                              style={{
                                height: '200px',
                                width: '100%',
                                margin: '0 auto',
                              }}
                            />
                            <div className="card-body">
                              <h5 className="card-title">{ dataLocalExp.name }</h5>
                              <div className="card-text">
                                <span>
                                min. { dataLocalExp.min_person } persons
                                </span>
                                <p>
                                  IDR { dataLocalExp.price }
                                </p>
                                <p>
                                  Available from 18 May 2019
                                </p>
                              </div>
                            </div>
                          </div>
                        ))
                      }
                    </InfiniteScroll>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
      )
    }
}

const mapStateToProps = state => ({
  localExp: state.localExperiences.data,
  loading: state.localExperiences.loading,
  cityReducer: state.city,
});

export default compose(
  connect(mapStateToProps),
  withAuth(["PUBLIC"])
)(CommunityLocalExperience);