import React from "react";
import { compose } from "redux";
import withAuth from "../_hoc/withAuth";
import Headers from '../components/profile/_component/header';
import { getBookTourList } from '../stores/actions/eticketActions'
import { connect } from 'react-redux'
import Home from '../components/profile/eTicket/index'

class Eticket extends React.Component {
  static async getInitialProps({ query }) {
  }

  componentWillMount() {
    this.props.dispatch(getBookTourList('', this.props.token))
  }

  render() {
    return (
      <div>
        <Headers />
        <Home path={this.props.asPath} token={this.props.token} />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  eticket: state.eticket,
})


export default compose(connect(mapStateToProps), withAuth(["PRIVATE"]))(Eticket);