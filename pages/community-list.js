import React from "react";
import ReactDOM from "react-dom";
import { get, isEmpty, sortBy } from "lodash";
import { compose } from "redux";
import { connect } from "react-redux";
import withAuth from "../_hoc/withAuth";
import Head from "../components/head";
import PageHeader from "../components/page-header";
import { getCommunityList, getLocations } from "../stores/actions";
import ActivityCategory from "../components/community/card";
import SearchAutoComplete from "../components/shared/searchautocomplete";
import { FormControl, FormGroup, InputGroup, Glyphicon } from "react-bootstrap";

class CommunityList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      communities: {
        isLoading: false,
        data: [],
        isReload: false
      },
      filter: "",
      location: [],
      params: {
        page: 1,
        city: null,
        province: null,
        area: null,
        search: null,
        category: null
      }
    };
  }

  componentDidMount() {
    document.addEventListener("click", this.handleClickOutside, true);
    const { communities } = this.state;

    if (isEmpty(communities.data)) {
      this.setState({
        communities: {
          isLoading: true,
          data: [],
          isReload: false
        }
      });
      this.getCommunity();
    }

    this.props.dispatch(getLocations()).then(result => {
      if (get(result, "meta.code") === 200) {
        this.setState({
          location: get(result, "data")
        });
      }
    });
  }

  getCommunity = async params => {
    this.props.dispatch(getCommunityList(params)).then(result => {
      if (get(result, "meta.code") === 200) {
        this.setState({
          communities: {
            isLoading: false,
            data: sortBy(get(result, "data"), ['created_at']),
            isReload: false
          }
        });
      } else {
        this.setState({
          communities: {
            isLoading: false,
            data: [],
            isReload: true
          }
        });
      }
    });
  };

  handleLocation = value => {
    if (value.length > 0) {
      let params = this.state.params;

      if (value[0].type === "Area") {
        const area = { area: value[0].id, city: null, province: null };
        params = { ...params, ...area };
      } else if (value[0].type === "Kabupaten" || value[0].type === "Kota") {
        const city = { city: value[0].id, area: null, province: null };
        params = { ...params, ...city };
      } else {
        const province = { province: value[0].id, area: null, city: null };
        params = { ...params, ...province };
      }

      this.setState({ params }, () => {
        this.getCommunity(params);
      });
    }
  };

  handleSearch = e => {
    let params = this.state.params;
    const search = { search: e.target.value };
    params = { ...params, ...search };

    this.setState({ params });
  };

  searchFilter = () => {
    let params = this.state.params;
    this.getCommunity(params);
  };

  handleClickOutside = event => {
    const location = ReactDOM.findDOMNode(this.refs.location);
    if (!location || !location.contains(event.target)) {
      const search = ReactDOM.findDOMNode(this.refs.search);
      if (!search || !search.contains(event.target)) {
        this.setState({ filter: '' });
      }
    }
  };

  render() {
    const { communities, filter, location } = this.state;

    return (
      <div>
        <Head
          title={"Community Corner"}
          description={"Community Corner"}
          url={process.env.SITE_ROOT + "/community-list"}
        />
        <PageHeader
          background={"/static/images/img05.jpg"}
          title={`Community Corner`}
        />
        <div className="content-wrap bg-content-home container">
          <div className="row">
            {/* fiter start */}
            <div className="col-lg-2 col-md-2 col-sm-12 hidden-xs">
              <div className="community-filter">
                <ul
                  id="header-tabs"
                  className="list-unstyled"
                  style={{ padding: "10px" }}
                >
                  <li className="tabs-item">
                    <button
                      className="btn btn-sm btn-o btn-light"
                      style={{ width: "100%" }}
                      onClick={() => this.setState({ filter: "location" })}
                    >
                      Location
                    </button>
                    <div
                      ref="location"
                      className={`p__filter ${
                        filter === "location" ? "show" : "hide"
                        }`}
                    >
                      <SearchAutoComplete
                        id="destination"
                        key={"select-single"}
                        index={0}
                        data={location || []}
                        placeholder="Select City"
                        labelKey="name"
                        onChange={this.handleLocation}
                        bsSize="large"
                      />
                    </div>
                  </li>
                  <br />
                  <li className="tabs-item">
                    <button
                      className="btn btn-sm btn-o btn-light"
                      style={{ width: "100%" }}
                      onClick={() => this.setState({ filter: "search" })}
                    >
                      Search
                    </button>
                    <div
                      ref="search"
                      className={`p__filter ${
                        filter === "search" ? "show" : "hide"
                        }`}
                    >
                      <div className="cnt-filter">
                        <div className="cnt-body">
                          <FormGroup>
                            <InputGroup>
                              <FormControl
                                bsSize="small"
                                type="text"
                                onChange={this.handleSearch}
                                placeholder="Search ..."
                              />
                              <InputGroup.Addon>
                                <a onClick={this.searchFilter}>
                                  <Glyphicon glyph="search" />
                                </a>
                              </InputGroup.Addon>
                            </InputGroup>
                          </FormGroup>
                        </div>
                      </div>
                    </div>
                  </li>
                  <br />
                </ul>
              </div>
            </div>
            {/* filter end */}
            <div className="col-md-10">
              <ActivityCategory
                loopTo={[1, 1, 1, 1]}
                classProps={[3, 6, 6]}
                title="All Communities"
                isLoading={communities.isLoading}
                isReload={communities.isReload}
                data={communities.data}
                link={"community-list"}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(CommunityList);
