import React from "react";
import { compose } from "redux";
import withAuth from "../_hoc/withAuth";
import Headers from "../components/profile/_component/header";
import FavoritesItem from "../components/profile/favorites/index";
import { connect } from "react-redux";
import { wishlistList } from "../stores/actions/favoritesActions";
import { IMAGE_URL } from "../stores/actionTypes";

class Favorites extends React.Component {
	componentWillMount() {
		this.props.dispatch(wishlistList(this.props.token, `?page=1&per_page=8`));
	}
	render() {
		const { token, asPath, list, temporary_list, temporary_remove, paging, isLoading } = this.props;

		return (
			<div>
				<Headers />
				<FavoritesItem
					token={token}
					path={asPath}
					list={list}
					temporary_list={temporary_list}
					temporary_remove={temporary_remove}
          paging={paging}
          url={IMAGE_URL}
          isLoading={isLoading}
				/>
			</div>
		);
	}
}
const mapStateToProps = (state) => ({
	list: state.favorites.list,
	temporary_list: state.favorites.temporary_list,
	temporary_remove: state.favorites.temporary_remove,
  paging: state.favorites.paging,
  isLoading: state.favorites.isLoading
});

export default compose(
	connect(mapStateToProps),
	withAuth(["PRIVATE"]),
)(Favorites);
