import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

class TravelAssistants extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Head title={"Travel Assistants"} description={""} url={process.env.SITE_ROOT + '/travel-assistants'} />
                <PageHeader background={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/shutterstock_1564028536.png"} title={`Travel Assistants`} caption=" " />
                <section className="content-wrap bg-content-home">
                    <div className="container">
                        <div className="col-md-12 section-one">
                            <p className="text-center">
                                Siapa saja bisa menjadi Travel Assitants (TA) Pigijo selama memenuhi kriteria*. Menjadi Travel Assistant
                                Pigijo adalah pekerjaan yang menyenangkan karena memiliki waktu kerja yang fleksibel. Anda dapat mengenalkan
                                kekayaan budaya, wisata, dan kuliner di sekitar lokasi Anda kepada wisatawan lokal maupun mancanegara. Anda
                                dapat membantu traveller menyusun itinerary, menemani traveller, dan mendampingi traveller saat melakukan
                                aktivitas sesuai kemampuan dan/atau keahlian, seperti diving, hiking, dll.
                </p>
                        </div>
                    </div>
                </section>

                <section className="content-wrap bg-content-home">
                    <div className="container section-two">
                        <div className="col-md-12">
                            <div className="col-md-offset-2 col-md-8">
                                <p>
                                    Persyaratan Umum
                    </p>
                                <ol className="sub-p section-three-p">
                                    <li>
                                        Memiliki KTP
                        </li>
                                    <li>
                                        Memiliki NPWP (Jika ada).
                        </li>
                                    <li>
                                        Usia minimal 19 tahun, usia maksimal tidak dibatasi, selama memenuhi syarat dan ketentuan sesuai
                                        yang
                                        tercantum.
                        </li>
                                    <li>
                                        Memahami & menguasasi daerah dimana Anda tingga , seperti akses jalan, kuliner, lokasi wisata, serta
                                        aktivitas yang dapat diikuti di daerah tersebut.
                        </li>
                                    <li>
                                        Menguasai setidaknya 1 bahasa asing, misalnya Inggris, Mandarin, Arab, Jepang, Korea.
                        </li>
                                    <li>
                                        Menguasai bahasa daerah di lokasi domisili.
                        </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="content-wrap bg-content-home">
                    <div className="container">
                        <div className="col-md-12 section-three">
                            <div className="col-md-offset-2 col-md-8">
                                <p>
                                    Keuntungan
                    </p>

                                <div className="">
                                    <div className="row">
                                        <div className="col-md-1 col-xs-1 text-right">
                                            >
                            </div>
                                        <div className="col-md-9 col-xs-9">
                                            Waktu kerja yang flexible
                            </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-1 col-xs-1 text-right">
                                            >
                            </div>
                                        <div className="col-md-9 col-xs-9">
                                            Anda tidak perlu repot mempromosikan jasa Anda, team promosi Pigijo akan mempromosikan nya untuk
                                            Anda.
                            </div>
                                    </div>
                                </div>

                                <p className="komponen-biaya-sewa">
                                    Komponen Service Free
                    </p>
                                <ol className="sub-p section-four-p">
                                    <li>
                                        Anda sepenuhnya menentukan nilai jasa yang ditampilkan resmi di website dan aplikasi Pigijo.
                        </li>
                                    <li>
                                        Komponen nilai jasa yang harus dipertimbangkan:
                            <ol type="a" className="sub-sub-p">
                                            <li>Skill</li>
                                            <li>Kemampuan Bahasa</li>
                                            <li>Panjang Perjalanan</li>
                                        </ol>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="content-wrap bg-content-home">
                    <div className="container">
                        <div className="col-md-4 col-md-offset-4 mb1">
                            <a href="https://partner.pigijo.com/login" rel='noopener noreferrer' target="_blank" className="btn btn-primary btn-block">Become a Partner Now</a>
                        </div>
                    </div>
                </section>

                <section className="content-wrap bg-custom">
                    <div>
                        <div className="col-md-12 section-five padding-none">
                            <div className="container-fluid">
                                <div className="row">
                                    <p className="section-five-title text-center">
                                        Travel Assistant Tier
                        </p>
                                </div>
                            </div>
                            <div className="container-fluid section-five md-none">
                                <div className="row">
                                    <div className="rectangle-green col-md-2">

                                    </div>
                                    <div className="circle-green col-md-2">
                                        <img className="img-freelance" src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/freelance.png' alt="" />
                                    </div>
                                    <div className="box-green col-md-6">
                                        <div className="box-white col-md-12">
                                            <div className="text-title-box col-md-12">
                                                Freelance Guide
                                </div>
                                            <div className="container-fluid">
                                                <div className="row">
                                                    <div className="col-md-3 text-right">
                                                        >
                                        </div>
                                                    <div className="col-md-9">
                                                        Memiliki ketertarikan terhadap pariwisata dan/atau bekerja paruh waktu di bidang
                                                        pariwsata.
                                        </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-3 text-right">
                                                        >
                                        </div>
                                                    <div className="col-md-9">
                                                        Memahami & mengenal secara baik daerah dimana Anda tinggal, seperti akses jalan,
                                                        kuliner, lokasi wisata, serta aktivitas yang dapat diikuti di daerah tersebut.
                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid section-five min-mt-1 md-none">
                            <div className="row">
                                <div className="rectangle-gold col-md-3">

                                </div>
                                <div className="circle-gold col-md-2">
                                    <img className="img-freelance" src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/profesional.png' alt="" />
                                </div>
                                <div className="box-gold col-md-6">
                                    <div className="box-white col-md-12">
                                        <div className="text-title-box col-md-12">
                                            Professional Guide
                            </div>
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-md-3 text-right">
                                                    <p>></p>
                                                </div>
                                                <div className="col-md-9">
                                                    <p>Memiliki dan/atau menjalankan profesi di bidang kepariwisataan.</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-3 text-right">
                                                    <p>></p>
                                                </div>
                                                <div className="col-md-9">
                                                    <p>Memiliki pengalaman membawa tamu wisatawan.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid section-five min-mt-1 md-none">
                            <div className="row">
                                <div className="rectangle-red col-md-4">

                                </div>
                                <div className="circle-red col-md-2">
                                    <img className="img-freelance" src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/certified.png' alt="" />
                                </div>
                                <div className="box-red col-md-6">
                                    <div className="box-white col-md-12">
                                        <div className="text-title-box col-md-12">
                                            Certified Guide
                            </div>
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-md-3 text-right">
                                                    <p>></p>
                                                </div>
                                                <div className="col-md-9">
                                                    <p>Dikhususkan bagi TA yang memiliki sertifikasi/lisensi, seperti yang dibutuhkan pada
                                                    aktivitas Diving, Hiking, Mountaineering, Crafting, Photography, Planting, Surfing,
                                        Rafting, dll.</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-3 text-right">
                                                    <p>></p>
                                                </div>
                                                <div className="col-md-9">
                                                    <p> Memiliki lisensi dari lembaga atau asosiasi/organisasi yang berkaitan dengan
                                        aktivitas.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* Card */}
                        <div className="row">
                            <div className="card col-sm-5 col-xs-offset-1 col-xs-10 xs-show">
                                <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/freelance.png" alt="Avatar" className="col-xs-8 col-xs-offset-2" />
                                <div className="container col-xs-12">
                                    <h4><b>Freelance Guide</b></h4>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-xs-1 text-right list-guide">
                                                <p>></p>
                                            </div>
                                            <div className="col-xs-9 list-guide">
                                                <p>Memiliki ketertarikan terhadap pariwisata dan/atau bekerja paruh waktu di bidang
                                    pariwsata.</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-1 text-right list-guide">
                                                <p>></p>
                                            </div>
                                            <div className="col-xs-9 list-guide">
                                                <p>Memahami & mengenal secara baik daerah dimana Anda tinggal, seperti akses jalan,
                                    kuliner, lokasi wisata, serta aktivitas yang dapat diikuti di daerah tersebut.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card col-sm-5 col-xs-offset-1 col-xs-10 xs-show">
                                <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/profesional.png" alt="Avatar" className="col-xs-8 col-xs-offset-2" />
                                <div className="container col-xs-12">
                                    <h4><b>Professional Guide</b></h4>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-xs-1 text-right list-guide">
                                                <p>></p>
                                            </div>
                                            <div className="col-xs-9 list-guide">
                                                <p>Memiliki dan/atau menjalankan profesi di bidang kepariwisataan.</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-1 text-right list-guide">
                                                <p>></p>
                                            </div>
                                            <div className="col-xs-9 list-guide">
                                                <p>Memiliki pengalaman membawa tamu wisatawan.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="card col-sm-5 col-xs-offset-1 col-xs-10 xs-show">
                                <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/certified.png" alt="Avatar" className="col-xs-8 col-xs-offset-2" />
                                <div className="col-xs-12">
                                    <h4><b>Certified Guide</b></h4>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-xs-1 text-right list-guide">
                                                <p>></p>
                                            </div>
                                            <div className="col-xs-9 list-guide">
                                                <p>Dikhususkan bagi TA yang memiliki sertifikasi/lisensi, seperti yang dibutuhkan pada
                                                 aktivitas Diving, Hiking, Mountaineering, Crafting, Photography, Planting, Surfing,
                                    Rafting, dll.</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-1 text-right list-guide">
                                                <p>></p>
                                            </div>
                                            <div className="col-xs-9 list-guide">
                                                <p>
                                                    Memiliki lisensi dari lembaga atau asosiasi/organisasi yang berkaitan dengan
                                                    aktivitas.
                                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <style jsx>{`
    .section-one{
        margin-top : 40px;
        margin-bottom : 20px;
    }

    .section-one p{
        font-family: roboto;
        font-size : 14px;
    }

    .container.section-two{
        width: 100%;
        background-color : #f2f0f0;
        padding-top : 30px;
        padding-bottom : 30px;
        margin-top : 20px;
        margin-bottom : 20px;
    }

    .section-two p{
        margin-bottom : 20px;
        font-family: roboto-bold;
        font-size : 18px;
    }

    .section-two ol li{
        font-family: roboto;
        font-size : 14px;
    }

    .section-three{
        margin-top:-60px;
        padding-bottom:100px;
    }

    .section-three p{
        margin-top : 20px;
        margin-bottom : 20px;
        font-family: roboto-bold;
        font-size : 18px;
    }

    .section-three ol li{
        font-family: roboto;
        font-size : 14px;
    }

    .bg-custom {
        background-image: linear-gradient(to right, #e5e5e5 10%, #fafafa);
    }

    .xs-show {
        display: none;
    }

    .list-guide {
        font-family: Roboto;
        font-size: 20px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: left;
        color: #969696;
    }

    .bg-custom {
        background-image: linear-gradient(to right, #e5e5e5 10%, #fafafa);
    }

    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        transition: 0.3s;
        border-radius: 5px;
        padding: 0px 0px 30px 0px;
        margin-top: 2vh;
    }

    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
    }

    .padding-none {
        padding: 0;
    }

    .img-freelance {
        background-position: center center;
        margin-top: -5px;
        margin-left: -27px;
    }

    .min-mt-1 {
        margin-top: -10vh;
    }

    /* .min-ml-1 {
        margin-left: -16% !important;
    } */

    .xs-show {
        display: none;
    }

    .section-two {
        padding-top: 5%;
        padding-bottom: 5%;
    }

    .section-three {
        padding-top: 5%;
        padding-bottom: 5%;
    }

    .section-four {
        padding-top: 5%;
        padding-bottom: 5%;
    }

    .section-five {
        padding-top: 5%;
        padding-bottom: 5%;
    }

    .section-five-title {
        font-family: Roboto;
        font-size: 50px;
        font-weight: 900;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.36;
        letter-spacing: normal;
    }

    .rectangle-green {
        position: relative;
        margin-top: 17vh;
        /* width: 364px; */
        height: 18px;
        background-color: #2add84;
    }

    .circle-green {
        position: relative;
        margin-left: 0px;
        /* padding: 15px 0px 0px 0px; */
        /* width: 364px; */
        height: 229px;
        /* background-color: #e5e5e5; */
        border-radius: 110%;
        border: 20px solid #2add84;
        z-index: 1;
    }

    .box-green {
        position: relative;
        padding: 15px 0px 0px 0px;
        /* width: 364px; */
        height: 229px;
        background-color: #2add84;
        border-top-right-radius: 115px;
        border-bottom-right-radius: 115px;
        margin-left: -100px;
    }

    .rectangle-gold {
        /* width: 506px; */
        margin-top: 17vh;
        height: 18px;
        background-color: #ddbb2a;
    }

    .box-gold {
        position: relative;
        padding: 15px 0px 0px 0px;
        /* width: 364px; */
        height: 229px;
        background-color: #ddbb2a;
        border-top-right-radius: 115px;
        border-bottom-right-radius: 115px;
        margin-left: -100px;
    }

    .circle-gold {
        position: relative;
        margin-left: 0px;
        /* padding: 15px 0px 0px 0px; */
        /* width: 364px; */
        height: 229px;
        /* background-color: #e5e5e5; */
        border-radius: 110%;
        border: 20px solid #ddbb2a;
        z-index: 1;
    }

    .rectangle-red {
        /* width: 662px; */
        margin-top: 17vh;
        height: 18px;
        background-color: #dd2a5a;
    }

    .circle-red {
        position: relative;
        margin-left: 0px;
        /* padding: 15px 0px 0px 0px; */
        /* width: 364px; */
        height: 229px;
        /* background-color: #e5e5e5; */
        border-radius: 110%;
        border: 20px solid #dd2a5a;
        z-index: 1;
    }

    .box-red {
        position: relative;
        padding: 15px 0px 0px 0px;
        /* width: 364px; */
        height: 229px;
        background-color: #dd2a5a;
        border-top-right-radius: 115px;
        border-bottom-right-radius: 115px;
        margin-left: -100px;
    }

    .box-white {
        height: 200px;
        /* background-color: #e5e5e5; */
        background-image: linear-gradient(to right, #e5e5e5 10%, #fafafa);
        border-top-right-radius: 115px;
        border-bottom-right-radius: 115px;
        margin-left: -20px;
    }

    .text-title-box {
        font-family: Roboto;
        font-size: 30px;
        font-weight: 900;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.37;
        letter-spacing: normal;
        text-align: left;
        margin-top: 2.5%;
        margin-bottom: 2.5%;
        margin-left: 18%;
        color: #595a5a;
    }

    .text-content-box {
        font-family: Roboto;
        font-size: 20px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: left;
        margin-left: 20%;
        color: #969696;
    }

    @media (max-width: 575.98px) {
        .list-guide p {
            font-family: Roboto;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.5;
            letter-spacing: normal;
            text-align: left;
            color: #969696;
        }

        .header {
            margin-top:100px;
            margin-left:10%;
            line-height:1;
        }

        .card {
            padding-top: 20px;
            
        }

        .bg-custom {
            height: 1700px;
        }

        .xs-show {
            display: block;
        }

        .md-none {
            display: none;
        }

        .section-five-title {
            font-family: Roboto;
            font-size: 32px;
            font-weight: 900;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.36;
            letter-spacing: normal;
            text-align: left;
            margin-left: 5%;
        }

        .img-section-transportation {
            background-image: url('../img/shutterstock_101145013.png');
            background-position: center center;
            background-size: cover;
            height: 40vh;
        }

        .img-section-travelassistants {
            background-position: center center;
            background-size: cover;
            height: 40vh;
        }

        .p-title {
            font-family: Roboto;
            font-size: 40px;
            color: white;
            margin-top: 32%;
            margin-left: 5%;
        }

        .p-title-group {
            font-family: Roboto;
            font-size: 40px;
            color: white;
            margin-top: 32%;
            margin-left: 5%;
        }

        .p-title-thin,
        .p-title-group-thin {
            font-size: 40px;
        }

        .sub-p {
            margin-left: -8%;
        }

        .sub-sub-p {
            margin-left: -8%;
        }

        .sub-sub-p-none {
            margin-left: -4%;
        }

        .section-two-p {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.5;
            letter-spacing: normal;
            text-align: center;
            color: #969696;
        }

        .persyaratan-umum {
            font-size: 20px;
        }

        .section-three-p {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.5;
            letter-spacing: normal;
            text-align: left;
            color: #969696;
        }

        .keuntungan {
            font-size: 20px;
        }

        .komponen-biaya-sewa {
            font-size: 20px;
        }

        .section-four-p {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.5;
            letter-spacing: normal;
            text-align: left;
            color: #969696;
        }
    }

    @media (min-width: 576px) and (max-width: 767.98px) {
        .xs-show {
            display: block;
        }

        .md-none {
            display: none;
        }
        .p-title {
            font-family: Roboto;
            font-size: 50px;
            color: white;
            margin-top: 12%;
            margin-left: 15%;
        }

        .p-title-group {
            font-family: Roboto;
            font-size: 50px;
            color: white;
            margin-top: 20%;
        }
    }

    @media (min-width: 768px) and (max-width: 991.98px) {
        .card {
            margin-left: 40px;
        }
        .md-none {
            display: none;
        }

        .xs-show {
            display: block;
        }

        .img-section-travelassistants {
            background-image: url('../img/shutterstock_1564028536.png');
            background-position: center center;
            background-size: cover;
            height: 30vh;
        }

        .p-title {
            font-family: Roboto;
            font-size: 50px;
            color: white;
            margin-top: 20%;
        }

        .p-title-group {
            font-family: Roboto;
            font-size: 50px;
            color: white;
            margin-top: 20%;
        }

        .sub-p {
            margin-left: -8.5%;
        }

        .section-two {
            padding-top: 6%;
            padding-bottom: 6%;
        }

        .section-two-p {
            font-family: Roboto;
            font-size: 16px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.5;
            letter-spacing: normal;
            text-align: center;
            color: #969696;
        }

        .persyaratan-umum {
            font-size: 20px;
        }

        .section-three-p {
            font-family: Roboto;
            font-size: 12px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.5;
            letter-spacing: normal;
            text-align: left;
            color: #969696;
            margin-left: -26px;
        }

        .keuntungan {
            font-size: 20px;
        }

        .komponen-biaya-sewa {
            font-size: 20px;
        }

        .section-four-p {
            font-family: Roboto;
            font-size: 12px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.5;
            letter-spacing: normal;
            text-align: left;
            color: #969696;
            margin-left: -26px;
        }

        .sub-sub-p {
            margin-left: -26px;
        }

        .list-guide {
            font-family: Roboto;
            font-size: 10px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.5;
            letter-spacing: normal;
            text-align: left;
            color: #969696;
        }
    }

    @media (min-width: 992px) and (max-width: 1199.98px) {}

    @media (min-width: 1200px) {}
    `}</style>
            </React.Fragment>
        )
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
)(TravelAssistants);
