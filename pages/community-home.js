import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import withAuth from "../_hoc/withAuth";
import HeaderDetail from "../components/community/header-detail";
import PageHeader from "../components/page-header";
import Home from "../components/community/Home";
import { httpReqGetDetailCommunity } from "../stores/actions";
import { get, isEmpty } from "lodash";

class CommunityHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      communities: {
        isLoading: false,
        data: {},
        page: 1,
        isReload: false,
        longDesc: false
      },
      slug: this.props.asPath.substring(this.props.asPath.lastIndexOf("/") + 1)
    };
  }

  componentDidMount() {
    const { communities } = this.state;

    if (isEmpty(communities.data)) {
      this.setState({
        communities: {
          isLoading: true,
          data: [],
          isReload: false
        }
      });

      this.props
        .dispatch(httpReqGetDetailCommunity(this.state.slug))
        .then(result => {
          if (get(result, "meta.code") === 200) {
            this.setState({
              communities: {
                isLoading: false,
                data: get(result, "data"),
                isReload: false
              }
            });
          } else {
            this.setState({
              communities: {
                isLoading: false,
                data: {},
                isReload: true
              }
            });
          }
        });
    }
  }

  render() {
    const styles = {
      containerTabItem: {
        marginTop: "8px",
        marginBottom: "8px"
      }
    };

    const { communities } = this.state;
    const detailComm = communities.data.detail_community
      ? communities.data.detail_community
      : "";

    return (
      <>
        <PageHeader
          background={"/static/images/img05.jpg"}
          title={communities.data.company_name}
        />
        <HeaderDetail
          comm={communities.data}
          slug={this.state.slug}
          detailComm={detailComm}
          longDesc={this.state.longDesc}
          clickShow={() => this.setState({ longDesc: !this.state.longDesc })}
        />
        <section>
          <div className="container" style={styles.containerTabItem}>
            <Home slug={this.state.slug} />
          </div>
        </section>
      </>
    );
  }
}
const mapStateToProps = state => ({
  community: state.community
});

export default compose(
  connect(mapStateToProps),
  withAuth(["PUBLIC"])
)(CommunityHome);
