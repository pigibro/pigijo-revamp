import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: 'cover',
    backgroundPosition: ' center center',
    opacity: 0.8,
  }
}

class AboutUs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: false
    }
  }

  componentDidMount() {

    if (window.innerWidth < 767) {
      this.setState({ isMobile: true })
    }
  }

  render() {
    let { isMobile } = this.state
    return (
      <React.Fragment>
        <Head title={"About Us"} description={""} url={process.env.SITE_ROOT + '/about-us'} />
        <PageHeader background={"/static/images/beach.jpg"} title={` `} caption=" " />
        <section className="content-wrap bg-content-home" style={{ padding: '2em 0' }}>
          <div className="container">
            <ul className="nav nav-tabs">
              <li className="active" style={{ marginRight: '3em' }}><a data-toggle="tab" href="#tab1">Who We Are</a></li>
              <li style={{ marginRight: '3em' }}><a data-toggle="tab" href="#tab2">Organization Structure</a></li>
              {/* <li><a data-toggle="tab" href="#tab3">Management</a></li> */}
            </ul>
            <div className="tab-content">
              <div id="tab1" className="tab-pane fade in active">
                <section className="content-wrap bg-content-home" style={{ padding: '2em 0' }}>
                  <div className="container">
                    <div className="main-title">
                      <h2 className="text-title" style={{ fontFamily: 'roboto-bold', fontSize: '2em', margin: 0, color: '#333' }}>WHO WE ARE</h2>
                    </div>
                    <p>
                      “Pigijo exist to spread the love for Indonesia, to share what Indonesia has to offers, and to show you life experiences which you have never seen before. We offer countless travel experiences that you won’t find anywhere else online, as well as other things you may need while you travel and make your unforgettable experiences in Indonesia. Plan your next adventure at Pigijo.com.”
                  </p>
                  </div>
                </section>
                <section className="content-wrap bg-content-home" style={{ marginLeft: '-30%', marginRight: '-30%', padding: '2% 30%', backgroundColor: '#eaeaea' }}>
                  <div className="container">
                    <div className="main-title">
                      <h2 className="text-title" style={{ fontFamily: 'roboto-bold', fontSize: '2em', margin: 0, color: '#333' }}>WHY PIGIJO EXIST</h2>
                    </div>
                    <p>
                      We love Indonesia and we believe that our country is more than meets the eye and has bountiful offer than what you already know. Indonesia has a diverse culture, hospitable peoples, and don’t forget about it’s amazing nature & scenery. If you are seeking to get an unforgettable experience, Indonesia is the perfect country for you. We are here to show you that paradise exists on earth, and it is here in Indonesia.
                  </p>
                  </div>
                </section>
                <section className="content-wrap bg-content-home" style={{ padding: '2em 0' }}>
                  <div className="container">
                    <div className="main-title">
                      <h2 className="text-title" style={{ fontFamily: 'roboto-bold', fontSize: '2em', margin: 0, color: '#333' }}>HOW PIGIJO SPREAD THE LOVE FOR INDONESIA</h2>
                    </div>

                    <div className="row" style={{ display: 'flex', alignItems: 'center', marginBottom: '1.5em', flexWrap: 'wrap' }}>
                      <div className="col-md-6 col-sm-12">
                        <img src="/static/images/pigijo_reason_ilustration.png" width="100%" alt="How Pigijo Spread the Love for Indonesia" />
                      </div>
                      <div className="col-sm-6">
                        <p>
                          Established in February 2017 in Jakarta, under PT. Tourindo Guide Indonesia, Pigijo is a digital tourism marketplace, enabling tourism product providers and travelers interact real time online to get the best, newest, safe and competitive price of the product. In September 2018 the website go live, and almost a year later SF Capital also empowering Pigijo as its subsidiary.
                      </p>
                      </div>
                    </div>

                    <p>
                      Traveler has a freedom to find and choose their own itinerary using Pigijo apps – just book all the items needed and process it in a single checkout. We brings you
                  </p>

                    <div className="row" style={{ display: 'flex', alignItems: 'center', marginBottom: '1.5em', flexWrap: 'wrap', justifyContent: 'center' }}>
                      <div className="col-md-3 col-sm-6" style={{ display: 'flex', alignItems: 'center', paddingTop: '2em', flexWrap: 'wrap', flexDirection: 'column' }}>
                        <img src="/static/images/localExp_aboutUs.png" width={isMobile ? "30%" : '20%'} alt="Local Experience" />
                        <div style={{ height: '100px' }}>
                          <p style={{ marginTop: '2em', textAlign: 'center' }}>3000+ Local Experiences</p>
                        </div>
                      </div>
                      <div className="col-md-2 col-sm-6" style={{ display: 'flex', alignItems: 'center', paddingTop: '2em', flexWrap: 'wrap', flexDirection: 'column' }}>
                        <img src="/static/images/ta_aboutUs.png" width="30%" alt="Travel assistant" />
                        <div style={{ height: '100px' }}>
                          <p style={{ marginTop: '2em', textAlign: 'center' }}>Travel assistant all over Indonesia</p>
                        </div>
                      </div>
                      <div className="col-md-2 col-sm-6" style={{ display: 'flex', alignItems: 'center', paddingTop: '2em', flexWrap: 'wrap', flexDirection: 'column' }}>
                        <img src="/static/images/places_aboutUs.png" width="30%" alt="Places" />
                        <div style={{ height: '100px' }}>
                          <p style={{ marginTop: '2em', textAlign: 'center' }}>4,800+ places</p>
                        </div>
                      </div>
                      <div className="col-md-2 col-sm-6" style={{ display: 'flex', alignItems: 'center', paddingTop: '2em', flexWrap: 'wrap', flexDirection: 'column' }}>
                        <img src="/static/images/rentCar_aboutUs.png" width="30%" alt="Rent Car" />
                        <div style={{ height: '100px' }}>
                          <p style={{ marginTop: '2em', textAlign: 'center' }}>1,200+ car rentals</p>
                        </div>
                      </div>
                      <div className="col-md-3 col-sm-12" style={{ display: 'flex', alignItems: 'center', paddingTop: '2em', flexWrap: 'wrap', flexDirection: 'column' }}>
                        <img src="/static/images/homestay_aboutUs.png" width={isMobile ? "30%" : '20%'} alt="Homestay" />
                        <div style={{ height: '100px' }}>
                          <p style={{ marginTop: '2em', textAlign: 'center' }}>400+ homestays</p>
                        </div>
                      </div>
                    </div>

                    <p>
                      We partnered with countless local tourism agent &amp; businesses, participating in helping the overall local economy. Our platform offers seamless &amp; convenient payments for you in a single
                      platform.
                  </p>

                    <div className="main-title" style={{ margin: '0 0 1em' }}>
                      <h2 className="text-title" style={{ fontFamily: 'roboto-bold', fontSize: '2em', marginTop: '3em', color: '#333' }}>OUR FEATURES</h2>
                    </div>

                    <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%', padding: '1em 0', flexWrap: 'wrap' }}>
                      <div style={{ width: isMobile ? '48%' : '22%', marginBottom: '15px', padding: '1em', borderRadius: '20px', minHeight: '350px', boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)' }}>
                        <img src='/static/images/clipboard.png' width='20%' alt="Create Your Own Itinerary" style={{ paddingTop: '2em' }} />
                        <h5 style={{ fontFamily: 'roboto-bold', color: '#fa9f42', margin: '1em 0' }}>CREATE YOUR OWN ITINERARY</h5>
                        <p>Find the best suitable travel preferences and tailor your own trip itinerary. Explore available destinations from our directory; find activities, Accommodations and transportations to book. It's all about your preference!
                      </p>
                      </div>
                      <div style={{ width: isMobile ? '48%' : '22%', marginBottom: '15px', padding: '1em', borderRadius: '20px', minHeight: '350px', boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)' }}>
                        <img src='/static/images/receipt.png' width='20%' alt="Single Checkout" style={{ paddingTop: '2em' }} />
                        <h5 style={{ fontFamily: 'roboto-bold', color: '#fa9f42', margin: '1em 0' }}>SINGLE CHECKOUT</h5>
                        <p>
                          We save you the hustle and help you to book all necessary items in a single checkout process.
                      </p>
                      </div>
                      <div style={{ width: isMobile ? '48%' : '22%', marginBottom: '15px', padding: '1em', borderRadius: '20px', minHeight: '350px', boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)' }}>
                        <img src='/static/images/receptionist.png' width='20%' alt="Travel Information" style={{ paddingTop: '2em' }} />
                        <h5 style={{ fontFamily: 'roboto-bold', color: '#fa9f42', margin: '1em 0' }}>TRAVEL INFORMATION FOR EVERYTHING YOU NEED</h5>
                        <p>
                          Often wonders where to visit next? Find your next destination here from our directory. We have information of thousands of Destination, Activity, Accommodations and Transportation
                      </p>
                      </div>
                      <div style={{ width: isMobile ? '48%' : '22%', marginBottom: '15px', padding: '1em', borderRadius: '20px', minHeight: '350px', boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)' }}>
                        <img src='/static/images/invoice.png' width='20%' alt="Budget Estimation" style={{ paddingTop: '2em' }} />
                        <h5 style={{ fontFamily: 'roboto-bold', color: '#fa9f42', margin: '1em 0' }}>BUDGET ESTIMATION</h5>
                        <p>
                          Concern about wanting too many things during your travel? Don't fret! Pigijo will help you manage your spending by showing you real time fee that will be spent while you're building your itinerary.
                      </p>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
              <div id="tab2" className="tab-pane fade in">
                <section className="content-wrap bg-content-home" style={{ padding: '2em 0' }}>
                  <div className="container">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/org-structure.jpg" alt="Organizational Structure" width="90%" />
                  </div>
                </section>
              </div>
              <div id="tab3" className="tab-pane fade in">
                <section className="content-wrap bg-content-home" style={{ padding: '2em 0' }}>
                  <div className="container">
                    <div className="row">
                      <div children="col-12">
                        <div className="main-title">
                          <h2 className="text-title" style={{ fontFamily: 'roboto-bold', margin: 0, color: '#FF5B00' }}>Board of Commisioners</h2>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-4">
                        <img className="photo-img" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/management/Claudia.jpg" alt="Claudia Ingkiriwang" />
                        <h4 style={{ fontFamily: 'roboto-bold', marginTop: 15, marginBottom: -2, color: '#FF5B00' }}>Claudia Ingkiriwang</h4>
                        <p>President Commissioner</p>
                      </div>
                      <div className="col-lg-4">
                        <img className="photo-img" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/management/Linawati.jpg" alt="Linawati" />
                        <h4 style={{ fontFamily: 'roboto-bold', marginTop: 15, marginBottom: -2, color: '#FF5B00' }}>Linawati</h4>
                        <p>Commissioner</p>
                      </div>
                      <div className="col-lg-4">
                        <img className="photo-img" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/management/Darren.jpg" alt="Darren Arthur Philip Setiawan" />
                        <h4 style={{ fontFamily: 'roboto-bold', marginTop: 15, marginBottom: -2, color: '#FF5B00' }}>Darren Arthur Philip Setiawan</h4>
                        <p>Commissioner</p>
                      </div>
                    </div>

                    <div className="row">
                      <div children="col-12">
                        <div className="main-title">
                          <h2 className="text-title" style={{ fontFamily: 'roboto-bold', margin: 0, color: '#FF5B00' }}>Board of Directors</h2>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-4">
                        <img className="photo-img" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/img-not-available.jpg" alt="Adi Putera Widjaja" />
                        <h4 style={{ fontFamily: 'roboto-bold', marginTop: 15, marginBottom: -2, color: '#FF5B00' }}>Adi Putera Widjaja</h4>
                        <p>President Director</p>
                      </div>
                      <div className="col-lg-4">
                        <img className="photo-img" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/img-not-available.jpg" alt="Lilik Takahashi" />
                        <h4 style={{ fontFamily: 'roboto-bold', marginTop: 15, marginBottom: -2, color: '#FF5B00' }}>Lilik Takahashi</h4>
                        <p>Director</p>
                      </div>
                      <div className="col-lg-4">
                        <img className="photo-img" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/management/Evie.jpg" alt="Evie Feniyanti" />
                        <h4 style={{ fontFamily: 'roboto-bold', marginTop: 15, marginBottom: -2, color: '#FF5B00' }}>Evie Feniyanti</h4>
                        <p>Director</p>
                      </div>
                    </div>

                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
        <style jsx>{`
            .photo-img{
              border-radius: 20px;
              width: 300px;
              box-shadow : 0 10px 16px 0 rgba(0,0,0,0.2);
            }

            .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
              border: 0;
              border-bottom: 3px solid #FF5B00;
              font-weight: bold;
            }

            .tab-content{
              border: 0;
            }

            .box-shadow{
                padding: 1.5em;
                border-radius: 20px;
                min-height: 350px;
                // box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
                // width: 23%;
            }

            .box-shadow h5{
              text-align: center;
              color: #fa9f42;
            }

            .title-feature{
              marginTop: 2em; 
              text-align: center;
            }

            .icon-wrapper{
              display: flex; 
              align-items: center; 
              marginBottom: 1.5em;
            }

            .main-about p{
              font: 16px/24px Helvetica;
            }

            .blockOneFive{
              width: 20%;
              display: inline-block;
              padding: 10px;
              vertical-align: top;
            }

            .text-dark{
              color: #333;
            }

            .text-center img{
                width: 100px;
            }

            .main-title{
              margin: 0 0 1em
            }
            
            @media (max-width: 767px) {
              .blockOneFive{
                  width: 100%;
              }
              
              .text-center img{
                  width: 50px;
              }

              .box-shadow{
                width: 100%;
                margin-bottom: 20px;
              }
          }
                  
        `}</style>
      </React.Fragment >
    )
  }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(AboutUs);