import React, { Component, Fragment } from "react";
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field as ReduxField, reduxForm, change } from 'redux-form';
import { get, map, isEmpty } from 'lodash';
import Error from './_error';
import Head from '../components/head';
import { Link } from '../routes';
import validate from '../components/contact-detail/validate';
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';
import { getCountries } from '../stores/actions';
import { SingleDatePicker } from "react-dates";
import DatePicker from 'react-datepicker'
import Router from "next/router";
import numberWithComma from '../utils/numberWithComma';
import ModalContainer from '../components/modals/container'
import openSocket from 'socket.io-client'
import { modalToggle, flightCart, getFareDetail, checkoutDetails, deleteCart, setErrorMessage } from '../stores/actions';
import { 
  Grid,
  Row, 
  Col, 
  Panel, 
  FormGroup, 
  ControlLabel, 
  Form, 
  FormControl,
  InputGroup,
  Modal
} from "react-bootstrap";
import 'react-dates/initialize';
import moment from 'moment';
import 'moment/locale/id'
import Loading from '../components/shared/loading';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import Axios from "axios";
import "react-datepicker/dist/react-datepicker-cssmodules.css";
import "react-datepicker/dist/react-datepicker-cssmodules.min.css";
import "react-datepicker/dist/react-datepicker.css";
import "react-datepicker/dist/react-datepicker.min.css";
import getConfig from "next/config"
import { logEvent } from "../utils/analytics.js";
import { isMobile } from "react-device-detect";
const { publicRuntimeConfig } = getConfig()

const { SOCKET_URL } = publicRuntimeConfig
moment.locale('id');

class PickSeat extends Component {
    static async getInitialProps(context) {
		const { req, store, query } = context;
    await store.dispatch( getCountries([]) );
    
    return { query }
    }

    constructor(props) {
        super(props);
        this.state = { 
            number: [1,2,3,4,5,6,7,8,9,10,11,12,13]
         };
    }

    render() {
        const { country, handleSubmit } = this.props;
        return (
            <React.Fragment>
                <Head 
                    title={"Manifest"} 
                    description={"Manifest Flight pigijo."} 
                    url={process.env.SITE_ROOT + '/manifest'} 
                />
                <Grid>
                    <Row>
                        <Col xs={12} md={8}>
                            <section className="manifest-card" style={{padding: '3em 0 .5em 0', margin: 0}}>
                                <h4 className="text-title" style={{fontFamily: 'Poppins Bold', color: '#636060'}}>Pilih Kursi</h4>
                                <section className="card" style={{backgroundColor: '#F7F7F7',boxShadow: '15px 17px 20px #0000000D' ,borderRadius: '23px', border: '1px solid #D1D1D1', padding: 0}}>
                                    <div className="row" style={{padding: 0, margin: 0, display: 'flex', alignItems: 'center', flexWrap: 'wrap'}}>
                                        <div className='col-lg-5 col-md-5 col-sm-5' style={{padding: '1em 2em'}}>
                                            <p style={{fontFamily: 'Poppins Bold', color: '#636060', fontSize: '1.1em' }}>1. Tuan John Doe</p>
                                            
                                            <span style={{fontFamily: 'Poppins Bold', color: '#636060', fontSize: '.9em'}}>Pergi</span>
                                            <p style={{color: '#636060', fontSize: '.9em'}}>Ekonomi (Subclass P)</p>

                                            <span style={{fontFamily: 'Poppins Bold', color: '#636060', fontSize: '.9em'}}>Pulang</span>
                                            <p style={{color: '#636060', fontSize: '.9em'}}>Ekonomi (Subclass P)</p>
                                        </div>
                                        <div className='col-lg-7 col-md-7 col-sm-7' style={{padding: '1em 2em', backgroundColor: 'white', borderLeft: '1px solid #D1D1D1', borderBottom: '1px solid #D1D1D1', borderTopRightRadius: '23px'}}>
                                            <p style={{fontFamily: 'Poppins Bold', color: '#636060', fontSize: '1.1em' }}>2. Nyonya Jane Doe</p>
                                            
                                            <span style={{fontFamily: 'Poppins Bold', color: '#636060', fontSize: '.9em'}}>Pergi</span>
                                            <p style={{color: '#636060', fontSize: '.9em'}}>Ekonomi (Subclass P)</p>

                                            <span style={{fontFamily: 'Poppins Bold', color: '#636060', fontSize: '.9em'}}>Pulang</span>
                                            <p style={{color: '#636060', fontSize: '.9em'}}>Ekonomi (Subclass P)</p>
                                        </div>

                                        <div className='col-lg-12 col-md-12 col-sm-12' style={{padding: '1em 2em', display: 'flex', flexDirection: 'column', alignItems: 'flex-end'}}>
                                            <div>
                                                <span style={{color: '#636060', fontSize: '.75em'}}>Time left to choose seat :</span>
                                                <p style={{fontFamily: 'Poppins Bold', color: '#FF5B00', fontSize: '.75em', lineHeight: '.8em' }}>00:04:45</p>
                                            </div>
                                        </div>

                                        <div className='col-lg-6 col-md-6 col-sm-6' style={{padding: '1.5em 3.5em'}}>
                                            <p style={{fontFamily: 'Poppins Bold', color: '#FF5B00', fontSize: '1.1em', marginBottom: 0, lineHeight: '1.2em' }}>PERGI</p>
                                            <span style={{color: '#FF5B00'}}>Jakarta - bandung</span>
                                            <p style={{color: '#FF5B00', fontSize: '.8em'}}>26 Mar 2020 | 17:45 - 20.45</p>
                                        </div>
                                        <div className='col-lg-6 col-md-6 col-sm-6' style={{padding: '1.5em 3.5em', backgroundColor: 'white', borderTopLeftRadius: '50px', borderBottomLeftRadius: '50px', border: '1px solid #D1D1D1', borderRight: 0}}>
                                            <p style={{fontFamily: 'Poppins Bold', color: '#636060', fontSize: '1.1em', marginBottom: 0, lineHeight: '1.2em' }}>PULANG</p>
                                            <span style={{color: '#636060'}}>Jakarta - bandung</span>
                                            <p style={{color: '#636060', fontSize: '.8em'}}>26 Mar 2020 | 17:45 - 20.45</p>
                                        </div>

                                        <div className='col-lg-12 col-md-12 col-sm-12' style={{padding: '2em'}}>
                                            <p style={{fontFamily: 'Poppins Bold', color: '#636060', fontSize: '1.1em' }}>Argo Parahyangan</p>
                                            <div style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                                alignItems: 'center',
                                                padding: '1.5em 0'
                                            }}>
                                                <div style={{
                                                    width: '22%',
                                                    border: '1px solid #FF5B00',
                                                    borderRadius: '50px',
                                                    padding: '.5em 0',
                                                    textAlign: 'center',
                                                    color: '#FF5B00',
                                                    backgroundColor: 'white'
                                                }}>
                                                    <p style={{fontFamily: 'Poppins Bold', lineHeight: '1.05em', marginBottom: 0}}>Kereta 1</p>
                                                    <span style={{fontSize: '.7em'}}>58 kursi tersedia</span>
                                                </div>

                                                <div style={{
                                                    width: '22%',
                                                    border: '1px solid #707070',
                                                    borderRadius: '50px',
                                                    padding: '.5em 0',
                                                    textAlign: 'center',
                                                    color: '#707070',
                                                    backgroundColor: 'white'
                                                }}>
                                                    <p style={{fontFamily: 'Poppins Bold', lineHeight: '1.05em', marginBottom: 0}}>Kereta 2</p>
                                                    <span style={{fontSize: '.7em'}}>58 kursi tersedia</span>
                                                </div>

                                                <div style={{
                                                    width: '22%',
                                                    border: '1px solid #707070',
                                                    borderRadius: '50px',
                                                    padding: '.5em 0',
                                                    textAlign: 'center',
                                                    color: '#707070',
                                                    backgroundColor: 'white'
                                                }}>
                                                    <p style={{fontFamily: 'Poppins Bold', lineHeight: '1.05em', marginBottom: 0}}>Kereta 2</p>
                                                    <span style={{fontSize: '.7em'}}>58 kursi tersedia</span>
                                                </div>

                                                <div style={{
                                                    width: '22%',
                                                    border: '1px solid #707070',
                                                    borderRadius: '50px',
                                                    padding: '.5em 0',
                                                    textAlign: 'center',
                                                    color: '#707070',
                                                    backgroundColor: 'white'
                                                }}>
                                                    <p style={{fontFamily: 'Poppins Bold', lineHeight: '1.05em', marginBottom: 0}}>Kereta 2</p>
                                                    <span style={{fontSize: '.7em'}}>58 kursi tersedia</span>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div className='col-lg-12 col-md-12 col-sm-12' style={{padding: '2em', display: 'flex', justifyContent: 'center'}}>
                                            <div style={{
                                                width: '90%',
                                                border: '3px solid #FF5B00',
                                                backgroundColor: 'white',
                                                borderRadius: '30px',
                                                padding: '2.5em 1.5em 2em 3em'
                                            }}>
                                                <div style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    marginBottom: '10px'
                                                }}>
                                                    <span style={{color: '#636060'}}>D</span>
                                                    {
                                                        this.state.number.map(num => (
                                                            <div style={{
                                                                width: '35px',
                                                                height: '35px',
                                                                border: '1px solid #E3E3E3',
                                                                borderRadius: '5px',
                                                                marginLeft: '10px',
                                                                display: 'flex',
                                                                justifyContent:'center',
                                                                alignItems: 'center',
                                                                backgroundColor: 'white'
                                                            }}></div>
                                                        ))
                                                    }
                                                </div>

                                                <div style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    marginBottom: '10px'
                                                }}>
                                                    <span style={{color: '#636060'}}>C</span>
                                                    {
                                                        this.state.number.map(num => (
                                                            <div style={{
                                                                width: '35px',
                                                                height: '35px',
                                                                border: '1px solid #E3E3E3',
                                                                borderRadius: '5px',
                                                                marginLeft: '10px',
                                                                display: 'flex',
                                                                justifyContent:'center',
                                                                alignItems: 'center',
                                                                backgroundColor: '#E3E3E3',
                                                            }}>
                                                                <span style={{color: 'white', fontSize: '1.8em', fontFamily: 'eina'}}>x</span>
                                                            </div>
                                                        ))
                                                    }
                                                </div>

                                                <div style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    marginBottom: '10px'
                                                }}>
                                                    <span style={{color: 'transparent'}}>U</span>
                                                    {
                                                        this.state.number.map(num => (
                                                            <div style={{
                                                                width: '35px',
                                                                height: '35px',
                                                                border: '1px solid transparent',
                                                                borderRadius: '5px',
                                                                marginLeft: '10px',
                                                                display: 'flex',
                                                                justifyContent:'center',
                                                                alignItems: 'center',
                                                            }}>
                                                                <span style={{color: '#636060'}}>{num}</span>
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                                <div style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    marginBottom: '10px'
                                                }}>
                                                    <span style={{color: '#636060'}}>D</span>
                                                    {
                                                        this.state.number.map(num => (
                                                            <div style={{
                                                                width: '35px',
                                                                height: '35px',
                                                                border: '1px solid #E3E3E3',
                                                                borderRadius: '5px',
                                                                marginLeft: '10px',
                                                                display: 'flex',
                                                                justifyContent:'center',
                                                                alignItems: 'center',
                                                                backgroundColor: 'white'
                                                            }}></div>
                                                        ))
                                                    }
                                                </div>
                                                <div style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    marginBottom: '10px'
                                                }}>
                                                    <span style={{color: '#636060'}}>D</span>
                                                    {
                                                        this.state.number.map(num => (
                                                            <div style={{
                                                                width: '35px',
                                                                height: '35px',
                                                                border: '1px solid #FF5B00',
                                                                borderRadius: '5px',
                                                                marginLeft: '10px',
                                                                display: 'flex',
                                                                justifyContent:'center',
                                                                alignItems: 'center',
                                                                backgroundColor: '#FF5B00',
                                                                color: 'white'
                                                            }}>
                                                                <span>1</span>
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </section>
                        </Col>

                        <Col xs={12} md={4}>
                        <section className="manifest-card" style={{padding: '3em 0', margin: 0}}>
                          <h4 className="text-title" style={{fontFamily: 'Poppins Bold', color: '#636060'}}>Rincian Harga</h4>
                          <section className="card" style={{backgroundColor: '#F7F7F7',boxShadow: '15px 17px 20px #0000000D' ,borderRadius: '23px', padding: '0 0 1em 0'}}>
                            <div style={{width: '100%', borderTopLeftRadius: '23px', borderTopRightRadius: '23px', backgroundColor: '#FF5B00', padding: ' 0 2em'}}>
                              <h5 className="text-title " style={{fontFamily: 'Poppins Bold', color: 'white', fontWeight: 100, textTransform: 'uppercase', margin: '5px 0'}}>Pergi</h5>
                            </div>
                            <div style={{
                              width: '100%',
                              padding: '1em 2em'
                            }}>
                              <span style={{color: '#636060', fontSize: '1.2em'}}>PSE - BSD</span>
                              <p style={{color: '#636060', fontSize: '.8em', lineHeight: '.85em'}}>Sab, 29 Feb 2020 | 16:10</p>

                              <span style={{color: '#636060', margin: '2px'}}>Malabar 108</span>
                              <p style={{color: '#636060', margin: '2px'}}>Ekonomi (Subclass P)</p>
                              <span style={{color: '#636060', margin: '2px'}}>Dewasa (x1)</span>
                              <span style={{color: '#636060', position: 'absolute', right: 30}}>Rp 90.000</span>
                            </div>

                            <div style={{width: '100%', backgroundColor: '#FF5B00', padding: ' 0 2em'}}>
                              <h5 className="text-title " style={{fontFamily: 'Poppins Bold', color: 'white', fontWeight: 100, textTransform: 'uppercase', margin: '5px 0'}}>Pulang</h5>
                            </div>
                            <div style={{
                              margin: '1em 2em',
                              borderBottom: '2px dashed #FF5B00',
                              paddingBottom: '1.5em'
                            }}>
                              <span style={{color: '#636060', fontSize: '1.2em'}}>PSE - BSD</span>
                              <p style={{color: '#636060', fontSize: '.8em', lineHeight: '.85em'}}>Sab, 29 Feb 2020 | 16:10</p>

                              <span style={{color: '#636060', margin: '2px'}}>Malabar 108</span>
                              <p style={{color: '#636060', margin: '2px'}}>Ekonomi (Subclass P)</p>
                              <span style={{color: '#636060', margin: '2px'}}>Dewasa (x1)</span>
                              <span style={{color: '#636060', position: 'absolute', right: 30}}>Rp 90.000</span>
                            </div>

                            <div style={{
                              padding: '0 2em',
                              display: 'flex',
                              justifyContent: 'space-between'
                            }}>
                              <h5 className="text-title " style={{fontFamily: 'Poppins Bold', color: '#FF5B00', fontWeight: 100, textTransform: 'uppercase', margin: '5px 0'}}>Total</h5>
                              <h5 className="text-title " style={{fontFamily: 'Poppins Bold', color: '#FF5B00', fontWeight: 100, textTransform: 'uppercase', margin: '5px 0'}}>Rp 180.000</h5>
                            </div>
                          </section>
                        </section>
                      </Col>
                    </Row>
                </Grid>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    profile: state.auth.profile,
    type: state.modal.type,
    isOpen: state.modal.isOpen,
    message: state.modal.message,
    country: state.country.data,
  })
  
const mapDispatchToProps = dispatch => ({
    dispatch: dispatch
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withAuth(["PUBLIC"])
  )(PickSeat);