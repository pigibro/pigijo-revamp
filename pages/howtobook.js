import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: 'cover',
    backgroundPosition: ' center center',
    opacity: 0.8,
  }
}

class HowToBook extends React.Component {
  render() {
    return (
      <>
        <Head title={"How To Book"} description={"Hubungi Pusat Bantuan Pigijo untuk mendapatkan informasi lengkap dan jelas mengenai rencana perjalanan anda, status pembayaran, dan cara bergabung menjadi partner pigijo."} url={process.env.SITE_ROOT + '/how-to-book'} />
        <PageHeader background={"/static/images/img06.png"} title={`How to Book with Pigijo`} caption="" />
        <section className="content-wrap bg-content-home">
          <div className="container">
            <div className="row header-icon">
              <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                <a href="#search">
                  <img src="/static/images/component1.svg" alt="Search" />
                </a>
                <p>Search</p>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                <a href="#select">
                  <img src="/static/images/component2.svg" alt="Select" />
                </a>
                <p>Select</p>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                <a href="#choose">
                  <img src="/static/images/component3.svg" alt="Choose Product" />
                </a>
                <p>Choose Packages</p>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                <a href="#checkout">
                  <img src="/static/images/component4.svg" alt="Checkout or Add to Plan" />
                </a>
                <p>Checkout or Arrange Itinerary</p>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                <a href="#fill">
                  <img src="/static/images/component5.svg" alt="Fill in Contact Person" />
                </a>
                <p>Fill In Contact Person</p>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                <a href="#payment">
                  <img src="/static/images/component6.svg" alt="Payment" />
                </a>
                <p>Payment</p>
              </div>
            </div>
          </div>
        </section>

        <section className="content-wrap bg-content-home">
          <div className="container">
            <hr />
          </div>
        </section>

        <section className="content-wrap bg-content-home" style={{ marginBottom: '5em' }}>
          <div className="container">
            <div className="row content-row-1" id="search">
              <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                <img width="30em" src="/static/images/1.png" alt="1" />
              </div>
              <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                <p className="icon-content-header-odd">SEARCH</p>
                <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Start your search on
                the Pigijo Home Page. You can type anything related to your journey, we will show all everything we
                got on it.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                <img src="/static/images/new_search.png" />
              </div>
            </div>

            <div className="row content-row-1" id="select">
              <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                <img width="30em" src="/static/images/2.png" alt="2" />
              </div>
              <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                <p className="icon-content-header-odd">SELECT</p>
                <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Tour, Assistances, Places, Homestay,
                and Vehicle will be shown on the search results page. You can filter the product categories that you want.
                 Click on the product’s card for more detailed info.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                <img src="/static/images/new_select1.png" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 pic-content" style={{ textAlign: 'center' }}>
                <img src="/static/images/new_select2.png" />
              </div>
            </div>

            <div className="row content-row-1" id="choose">
              <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                <img width="30em" src="/static/images/3.png" alt="3" />
              </div>
              <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                <p className="icon-content-header-odd">CHOOSE PACKAGES</p>
                <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Choose the schedule
                and number of persons going with you, make sure that all are good as your planned, then you
                can click select.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                <img src="/static/images/new_choose.png" />
              </div>
            </div>

            <div className="row content-row-1" id="checkout">
              <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                <img width="30em" src="/static/images/4.png" alt="4" />
              </div>
              <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                <p className="icon-content-header-odd">CHECKOUT OR ARRANGE ITINERARY</p>
                <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>After select product you
                can click Checkout for booking the package or click Add to Plan if you want arrange itinerary.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                <img src="/static/images/new_checkout.png" />
              </div>
            </div>

            <div className="row content-row-1" id="fill">
              <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                <img width="30em" src="/static/images/5.png" alt="5" />
              </div>
              <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                <p className="icon-content-header-odd">FILL IN CONTACT PERSON</p>
                <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Fill in the contact person detail for this
                booking. This contact person will be in charge for this booking, so please make sure
                the contact person detail is reachable.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                <img src="/static/images/new_fill1.png" />
              </div>
            </div>

            <div className="row content-row-1" id="payment">
              <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                <img width="30em" src="/static/images/6.png" alt="6" />
              </div>
              <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                <p className="icon-content-header-odd">PAYMENT</p>
                <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>There are variety of payment choices you can select
                to meet your preference.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                <img src="/static/images/new_payment.png" />
              </div>
            </div>
          </div>
        </section>

        <style jsx global>{`
          .header-icon {
            padding: 3% 10% 0% 10%;
            margin-right: 0%;
            text-align: center;
          }
          
          .header-icon p {
            font-family: Poppins Medium;
            font-size: 15px;
            color: black;
            margin-top: 10px;
          }
          
          hr {
            border-top: 2px solid whitesmoke;
            margin-top: 50px;
          }

          .pic-content img{
            width: 70%;
          }

          .btn-primary-outline {
            background-color: transparent;
            border-color: transparent;
          }

          @media (max-width: 351px) {
            .navbar {
                margin: -20px 0 0 50px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 100px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-list img {
                width: 50%;
                margin-bottom: 10px;
                margin-left: 50px;
            }
        
            /* odd */
            .icon-number-odd {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        
            /* even */
            .icon-number-even {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        }
        
        @media (min-width: 352px) and (max-width: 575px) {
            .navbar {
                margin: -20px 0 0 50px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 80px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-list img {
                width: 50%;
                margin-bottom: 10px;
                margin-left: 50px;
            }
        
            /* odd */
            .icon-number-odd {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-number-odd img {
                width: 10%;
            }
        
            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        
            /* even */
            .icon-number-even {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        }
        
        @media (min-width: 576px) and (max-width: 767px) {
            .navbar {
                margin: -20px 0 0 80px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 110px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-list img {
                width: 50%;
                margin-bottom: 10px;
                margin-left: 50px;
            }
        
            .icon-number-odd {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        
            .icon-number-even {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        }
        
        @media (min-width: 768px) and (max-width: 991px) {
            .navbar {
                margin: -45px 0 0 100px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 120px;
            }
        
            .icon-list img {
                width: 60%;
                margin-bottom: 10px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-number-odd {
                text-align: left;
            }
        
            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
            }
        
            .icon-number-even {
                text-align: left;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
            }
        }
        
          @media (min-width: 992px) and (max-width: 1199px) {
            .navbar {
                margin: -45px 0 0 100px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 120px;
            }
        
            .icon-list img {
                width: 60%;
                margin-bottom: 10px;
                margin-left: 50px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-number-odd {
                text-align: left;
                margin-left: 50px;
            }
        
            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
          }

          @media (min-width: 1200px) {
            .navbar {
                margin: -45px 0 0 150px;
            }
        
            .header {
                font-size: 60px;
            }
        
            .header-icon img {
                width: 120px;
            }
        
            .icon-list img {
                width: 80%;
            }
        
            .contents-md {
                display: none;
            }
        
            .contents-lg {
                display: block;
            }
        
            .contents-lg .row {
                margin-bottom: 0px;
            }
        
            .dash-1 {
                text-align: right;
            }
        
            .dash-2 {
                margin-left: -133px;
            }
        
            .icon-number-odd {
                text-align: right;
            }
        
            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
            }
        
            .icon-number-even {
                text-align: right;
                margin-left: -12%;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: -12%;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: -12%;
            }
          }
      `}</style>
      </>
    )
  }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(HowToBook);