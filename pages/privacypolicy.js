import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

class PrivacyPolicy extends React.Component {
    render(){
      return (
        <>
          <Head title={"Privacy Policy"} description={"Hubungi Pusat Bantuan Pigijo untuk mendapatkan informasi lengkap dan jelas mengenai rencana perjalanan anda, status pembayaran, dan cara bergabung menjadi partner pigijo."} url={process.env.SITE_ROOT+'/how-to-book'}/>
          <PageHeader background={"/static/images/img05.jpg"} title={`Privacy Policy`} caption=""/>
          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-12">
                    <h2 className="h4 mb1">
                        PENDAHULUAN
                    </h2>
                    <p className="term-text">
                    Selamat datang di Website pigijo.com. Website ini dimiliki, dioperasikan, diselenggarakan oleh PT. Tourindo Guide Indonesia (“Kami” atau “pigijo.com”), suatu perseroan terbatas yang didirikan berdasarkan hukum Republik Indonesia dengan Ijin Tetap Usaha Pariwisata Nomor AHU-10.AH.02.02 tertanggal 09 Februari 2010; Kami menyediakan website dan layanan yang tersedia secara online melalui website: www.pigijo.com atau situs web mobile : m.pigijo.com atau aplikasi mobile : pigijo untuk iOS dan pigijo untuk Android, dan berbagai akses, media, perangkat dan platform lainnya (“Layanan Online”), baik yang sudah atau akan tersedia di kemudian hari.
                    </p>
                </div>
              </div>
            </div>
          </section>
          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-12">
                    <h2 className="h4 mb1">
                        APA ITU ATURAN PRIVASI PIGIJO.COM
                    </h2>
                    <p className="term-text">
                    Kebijakan Privasi ini mengatur dan atau menjelaskan seluruh layanan yang sudah Kami sediakan untuk Anda (“Pengguna”) gunakan, baik layanan yang Kami operasikan sendiri maupun yang dioperasikan melalui afiliasi dan/atau rekanan Kami. Untuk menjaga kepercayaan Anda kepada Kami, maka Kami senantiasa akan menjaga segala kerahasiaan yang terkandung dalam data pribadi Anda, karena Kami menganggap privasi Anda sangat penting bagi Kami. Dalam aturan privasi ini Kami sertakan penjelasan mengenai tata cara Kami mengumpulkan, menggunakan, mengungkapkan, memproses dan melindungi informasi dan data pribadi Anda yang Kami identifikasi (“Data Pribadi”). Pada saat Anda membuat pemesanan dan atau akun pribadi pada website pigijo.com, maka Kami akan mencatat dan menyimpan informasi dan data pribadi Anda. Dimana pada prinsipnya, setiap data yang Anda berikan dari waktu ke waktu akan Kami simpan dan Kami gunakan untuk kepentingan penyediaan produk dan layanan Kami kepada Anda, yaitu antara lain untuk keperluan tokenisasi, akuntansi, tagihan, audit, verifikasi kredit atau pembayaran, newsletter, serta keperluan keamanan, administrasi dan hukum, bonus poin atau bentuk sejenisnya, pengujian, pemeliharaan dan pengembangan sistem, hubungan pelanggan, promosi dan membantu Kami di kemudian hari dalam memberikan pelayanan kepada Anda. Sehubungan dengan itu, Kami dapat mengungkapkan data Anda kepada grup perusahaan dimana pigijo.com bergabung didalamnya, mitra penyedia produk, perusahaan lain yang tercatat sebagai rekanan dari pigijo.com, perusahaan yang ditunjuk untuk melakukan proses data yang terikat kontrak dengan Kami, agen perjalanan, badan pemerintah dan badan peradilan yang berwenang di jurisdiksi manapun. Oleh karena pentingnya aturan privasi ini dan ketentuan lainnya dalam penggunaan website Kami, maka untuk menjaga keamanan data pribadi Anda, maka mohon untuk dibaca secara seksama seluruh ketentuan dalam aturan privasi ini dan ketentuan lainnya dalam website Kami.
                    </p>
                </div>
              </div>
            </div>
          </section>
          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-12">
                    <h2 className="h4 mb1">
                        INFORMASI APA SAJA YANG KAMI KUMPULKAN DAN GUNAKAN ?
                    </h2>
                    <p className="term-text">
                        Pada saat Anda mengakses website dan atau layanan online yang Kami sediakan, maka semua informasi dan data pribadi Anda akan Kami kumpulkan dengan ketentuan sebagai berikut :
                    </p>
                    <ul>
                        <li>
                            <p className="term-text">
                                Kami akan mengumpulkan informasi mengenai komputer atau pun media apapun yang Anda gunakan, termasuk IP address, sistem operasi, browser yang digunakan, URL, halaman, lokasi geografis dan waktu akses serta data lainnya terkait dengan penggunaan komputer Anda (“Detail IP”).
                            </p>
                        </li>
                        <li>
                            <p className="term-text">
                                Kami akan meminta Anda untuk mengisi data-data pribadi Anda secara benar, jelas, lengkap, akurat dan tidak menyesatkan, seperti nama, alamat email, nomor telepon, alamat lengkap, informasi yang digunakan untuk pembayaran, informasi kartu kredit (nomor kartu kredit dan masa berlaku kartu kredit) dan data-data lain yang Kami perlukan guna melakukan transaksi melalui website dan layanan online lainnya yang Kami sediakan agar Anda dapat memanfaatkan layanan yang Anda butuhkan. Kami tidak bertanggung jawab atas segala kerugian yang mungkin terjadi karena informasi dan atau data yang tidak benar, jelas, lengkap, akurat dan menyesatkan yang Anda berikan;
                            </p>
                        </li>
                        <li>
                            <p className="term-text">
                                Kami dapat menggunakan data pribadi Anda dan informasi lainnya yang dikumpulkan dengan tujuan pemasaran Media Sosial menggunakan tehnik grafik langsung dan terbuka dan untuk tujuan pemasaran digital konvensional, seperti mengirimkan Anda newsletter secara otomatis melalui surat elektronik untuk memberitahukan informasi produk baru, penawaran khusus atau informasi lainnya yang menurut Kami akan menarik bagi Anda.
                            </p>
                        </li>
                        <li>
                            <p className="term-text">
                                Dalam menggunakan layanan Kami, informasi-informasi yang Anda berikan dapat Kami gunakan dan berikan kepada pihak ketiga yang bekerjasama dengan Kami, sejauh untuk kepentingan transaksi dan penggunaan layanan Kami.
                            </p>
                        </li>
                        <li>
                            <p className="term-text">
                                Segala informasi yang Kami terima dapat Kami gunakan untuk melindungi diri Kami terhadap segala tuntutan dan hukum yang berlaku terkait dengan penggunaan layanan dan pelanggaran yang Anda lakukan pada website Kami atas segala ketentuan sebagaimana diatur dalam persyaratan layanan pigijo.com dan pedoman penggunaan produk dan layanan Kami, termasuk dan tidak terbatas apabila dibutuhkan atas perintah Pengadilan dalam proses hukum;
                            </p>
                        </li>
                        <li>
                            <p className="term-text">
                                Anda bertanggung jawab atas kerahasiaan informasi dan data pribadi Anda, termasuk bertanggung jawab atas semua akses dan penggunaan website yang menggunakan kata sandi dan akun serta token yang Anda miliki yang digunakan oleh siapa saja, baik atas seijin maupun tidak seijin Anda. Demi keamanan data  rahasia Anda, Kami sangat menyarankan agar Anda menyimpan akun dan kata sandi yang Anda miliki dengan sebaik-baiknya dan atau melakukan perubahan kata sandi secara berkala. Setiap penggunaan yang tidak sah dan tanpa sepengetahuan dan izin Anda menjadi tanggung jawab Anda sendiri dan Kami tidak bertanggung-jawab atas segala kerugian yang ditimbulkan sebagai akibat dari kelalaian yang Anda lakukan.
                            </p>
                        </li>
                        <li>
                            <p className="term-text">
                                Anda harus segera memberitahukan kepada Kami mengenai adanya penggunaan sandi atau akun tanpa izin Anda atau semua bentuk pelanggaran atau ancaman pelanggaran keamanan dalam website ini.
                            </p>
                        </li>
                        <li>
                            <p className="term-text">
                            Anda berhak untuk merubah dan atau menghapus data alamat yang telah Anda berikan dan telah tersimpan dalam sistem Kami dengan cara menghubungi  Customer Service kami.
                            </p>
                        </li>
                    </ul>         
                </div>
              </div>
            </div>
          </section>
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(PrivacyPolicy );