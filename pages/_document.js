import Document, { Head, Main, NextScript } from "next/document";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
const { GOOGLE_API_KEY, NODE_ENV, GA_ID } = publicRuntimeConfig;
// require('jquery/dist/jquery.min');
export default class MyDocument extends Document {

  render() {
    return (
      <html lang="id">
        <Head>
          <link rel="apple-touch-icon" sizes="57x57" href="/static/app-icon/57.png" />
          <link rel="apple-touch-icon" sizes="60x60" href="/static/app-icon/60.png" />
          <link rel="apple-touch-icon" sizes="72x72" href="/static/app-icon/72.png" />
          <link rel="apple-touch-icon" sizes="76x76" href="/static/app-icon/76.png" />
          <link rel="apple-touch-icon" sizes="114x114" href="/static/app-icon/114.png" />
          <link rel="apple-touch-icon" sizes="120x120" href="/static/app-icon/120.png" />
          <link rel="apple-touch-icon" sizes="144x144" href="/static/app-icon/144.png" />
          <link rel="apple-touch-icon" sizes="152x152" href="/static/app-icon/152.png" />
          <link rel="apple-touch-icon" sizes="180x180" href="/static/app-icon/180.png" />
          <link rel="icon" type="image/png" sizes="256x256"  href="/static/app-icon/256.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="96x96" href="/static/favicon-96x96.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16x16.png" />
          <meta name="msapplication-TileColor" content="#FF6700" />
          <meta name="msapplication-TileImage" content="/static/app-icon/144.png" />
          <link rel="manifest" href="/static/manifest.json" />
          <meta name="theme-color" content="#FF6700" />
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
          />
          <meta name="apple-mobile-web-app-title" content="PIGIJO" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="default"
          />
          {/* <link href="https://s3.ap-southeast-1.amazonaws.com/pigijo/assets/fonts/socicons/socicons.css" rel="stylesheet" /> */}
          <link href="/static/fonts/socicons/socicons.css" rel="stylesheet" />
          {/* <link href="https://s3.ap-southeast-1.amazonaws.com/pigijo/assets/fonts/themify-icons/themify-icons.css" rel="stylesheet" /> */}
          <link href="/static/fonts/themify-icons/themify-icons.css" rel="stylesheet" />
          {/* <link href="https://s3.ap-southeast-1.amazonaws.com/pigijo/assets/css/swiper.min.css" rel="stylesheet" /> */}
          <link href="/static/swiper.min.css" rel="stylesheet" />
          <script src="https://code.jquery.com/jquery-1.9.1.min.js" integrity="sha256-wS9gmOZBqsqWxgIVgA8Y9WcQOa7PgSIX+rPA0VL2rbQ=" crossOrigin="anonymous"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js" crossOrigin="anomymous"></script>
          {/* <script async type="text/javascript" dangerouslySetInnerHTML={
            {
              __html: "var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();(function(){var s1=document.createElement('script'),s0=document.getElementsByTagName('script')[0];s1.async=true;s1.src='https://embed.tawk.to/5b90ae85afc2c34e96e842da/default';s1.charset='UTF-8';s1.setAttribute('crossorigin','*');s0.parentNode.insertBefore(s1,s0);})();"
            }
          }></script> */}
          <script async type="text/javascript" dangerouslySetInnerHTML={
            {
              __html: `var options = { facebook: "pigijoid", line: "https://lin.ee/sDV6Tm3", call: "+6282246598802", whatsapp: "+6282246598802", call_to_action: "Reach us", button_color: "#FF6550", position: "right", order: "facebook,whatsapp, line, call", }; var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host; var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js'; s.onload = function () { WhWidgetSendButton.init(host, proto, options); }; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);`
            }
          }></script>
          <script dangerouslySetInnerHTML={
            {
              __html: `!function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window,document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '352679415365121'); 
              fbq('track', 'PageView');`
            }
          }></script>
          {/* <script type="text/javascript" dangerouslySetInnerHTML={{
            __html: `(function(b,r,a,n,c,h,_,s,d,k){if(!b[n]||!b[n]._q){for(;s<_.length;)c(h,_[s++]);d=r.createElement(a);d.async=1;d.src="https://cdn.branch.io/branch-latest.min.js";k=r.getElementsByTagName(a)[0];k.parentNode.insertBefore(d,k);b[n]=h}})(window,document,"script","branch",function(b,r){b[r]=function(){b._q.push([r,arguments])}},{_q:[],_v:1},"addListener applyCode autoAppIndex banner closeBanner closeJourney creditHistory credits data deepview deepviewCta first getCode init link logout redeem referrals removeListener sendSMS setBranchViewData setIdentity track validateCode trackCommerceEvent logEvent disableTracking".split(" "), 0);
            branch.init('key_live_YOUR_KEY_GOES_HERE');`
          }}></script> */}
          <noscript dangerouslySetInnerHTML={{ __html: `<img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=352679415365121&ev=PageView
            &noscript=1" />` }}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
