import React from "react";
import { Provider } from "react-redux";
import App from "next/app";
// import { ThemeProvider } from "styled-components";
import withRedux from "next-redux-wrapper";
import { initStore } from "../stores";
// import theme from "../theme";
import "normalize.css";
import 'react-perfect-scrollbar/dist/css/styles.css';
import '../styles/style.scss';
// import 'bootstrap/dist/css/bootstrap.min.css';

// import "../css/global.css";
import Router from "next/router";
import NProgress from "nprogress";
import Alert from 'react-s-alert';

NProgress.configure({ showSpinner: true });
Router.events.on("routeChangeStart", url => {
  NProgress.start();
});
Router.events.on("routeChangeComplete", url => {
  NProgress.done();
  window.scrollTo(0, 0);
});
Router.events.on("routeChangeError", () => NProgress.done());


export default withRedux(initStore)(
  class Pigijo extends App {
    static async getInitialProps({ Component, ctx }) {
      return {
        pageProps: Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {}
      };
    }

    render() {
      const { Component, pageProps, store } = this.props;
      return (
        <Provider store={store}>
            <Component {...pageProps} />
            <Alert stack={{limit: 3}} />
        </Provider>
      );
    }
  }
);
