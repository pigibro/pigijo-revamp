import React from "react";
import { compose } from "redux";
import withAuth from "../_hoc/withAuth";
import Test from '../components/profile/index'
import Headers from '../components/profile/_component/header'

class Profile extends React.Component {
  render() {
    return (
      <div>
        <Headers />
        <Test path={this.props.asPath} token={this.props.joToken}/>
      </div>
    );
  }
}

export default compose(withAuth(["PRIVATE"]))(Profile);