import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';
import moment from 'moment';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
      opacity: 0.8,
  }
}

class paymentFailed extends React.Component {

    render(){
      return (
        <>
          <Head title={"Payment Failed"} description={"Hubungi Pusat Bantuan Pigijo untuk mendapatkan informasi lengkap dan jelas mengenai rencana perjalanan anda, status pembayaran, dan cara bergabung menjadi partner pigijo."} url={process.env.SITE_ROOT+'/how-to-book'}/>
          <PageHeader background={"/static/images/img05.jpg"} title={`Payment Failed`} caption=""/>
          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-8 col-lg-push-2 col-md-8 col-md-push-2 thankyou">
                  <h2 className="h3 mb1">
                    <center>Whoops! Payment Failed.</center>
                  </h2> 
                  <p className="thankyou-content">
                    Something wrong happened when we tried to process your payment. <br/>please repeat your booking.
                  </p>
                  <Link to="/"><a className="btn btn-block btn-primary thankyou-button">Continue to Shopping</a></Link>
                </div>
              </div>
            </div>
          </section>

          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-6 col-md-offset-3">
                  <h2 className="h4 mt1 mb1 text-center">
                   Need Help ?
                  </h2>
                  <p>
                  Have problem when booking our products ? our customer service are ready to help you find your way around by click our chat
                  </p>
                </div>
              </div>
            </div>
          </section>
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(paymentFailed);