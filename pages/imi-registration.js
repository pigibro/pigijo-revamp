import React, { Component } from 'react';
import { get, isEmpty, concat } from 'lodash';
import { Link, Router } from '../routes';
import { compose } from "redux";
import withAuth from '../_hoc/withAuth';
import Head from '../components/head';
import { getActivities, getActivityPromos, getDetailCategory, getPlaces } from '../stores/actions';
import { Modal, Dropdown, DropdownButton, MenuItem } from "react-bootstrap";
import PageHeader from '../components/page-header';

class imiRegistration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMobile: false
        };
    }

    componentDidMount() {
        if (window.innerWidth < 767) {
            this.setState({ isMobile: true })
        }
    }
    render() {
        let { isMobile } = this.state
        return (
            <>
                <Head />
                <PageHeader background={'/static/images/img01.jpg'} title="" />
                <div className="jumbotron" style={{ backgroundColor: 'transparent', padding: '5em' }}>
                    <div className="container" style={{
                        width: "100%",
                        height: "100%",
                        textAlign: 'center',
                        zIndex: 50, position: 'relative'
                    }}>
                        <img src={isMobile ? require('../static/images/backgorund_imi_mobile.png') : require('../static/images/imi_desktop.png')} alt="Cinque Terre" width="100%" style={{ zIndex: -1 }} />
                        <div className="row button-wrapper" style={{ position: 'absolute', width: '100%', top: '62%' }}>
                            <div className="col-md-4 mb-2 _btn1" style={{ display: 'flex', justifyContent: 'center' }}>
                                <DropdownButton id="dropdown-basic-button-imi" title="IMI" style={{ minWidth: '240px', backgroundImage: 'linear-gradient(#1671cf, #00008b)', border: '1px solid white', borderRadius: '30px' }}>
                                    <MenuItem><Link route="/imi-registration/imi/Pengawas">Pengawas</Link></MenuItem>
                                    <MenuItem><Link route="/imi-registration/imi/Pusat">IMI Pusat</Link></MenuItem>
                                    <MenuItem><Link route="/imi-registration/imi/Propinsi">IMI Propinsi</Link></MenuItem>
                                    <MenuItem><Link route="/imi-registration/imi/Asosiasi">Asosiasi</Link></MenuItem>
                                </DropdownButton>
                            </div>
                            <div className="col-md-4 mb-2 _btn2">
                                <button style={{ minWidth: '240px', backgroundImage: 'linear-gradient(#1671cf, #00008b)', border: '1px solid white', borderRadius: '30px' }} className="btn text-white" type="button" onClick={() => Router.push('/imi-registration/atlet-nasional')}>
                                    Atlet Nasional
                            </button>
                            </div>
                            <div className="col-md-4 mb-2 _btn3" style={{ display: 'flex', justifyContent: 'center' }}>
                                <DropdownButton id="dropdown-basic-button-imi" title="Undangan" style={{ minWidth: '240px', backgroundImage: 'linear-gradient(#1671cf, #00008b)', border: '1px solid white', borderRadius: '30px' }}>
                                    <MenuItem><Link route="/imi-registration/undangan/Tokoh-Otomotif">Tokoh Otomotif</Link></MenuItem>
                                    <MenuItem><Link route="/imi-registration/undangan/Life-Time-Achievement">Life Time Achievement</Link></MenuItem>
                                    <MenuItem><Link route="/imi-registration/undangan/Media">Media</Link></MenuItem>
                                    <MenuItem><Link route="/imi-registration/undangan/Sponsor">Sponsor</Link></MenuItem>
                                </DropdownButton>
                            </div>
                        </div>
                    </div>
                    <div className="container px-3" style={{
                        backgroundImage: 'url(../static/images/pict1.png)',
                        backgroundSize: 'cover',
                        backgroundPosition: "center center",
                        width: "100%",
                        height: "100%",
                        textAlign: 'center'
                    }}>
                        {/* <div className="row" style={{marginTop: '5em', marginBottom: this.state.isMobile ? '22em' : '8em', display: 'flex'}}>
                        <div className="col-md-6 col-sm-6 round1" style={{padding: 0}}>
                            <img src={require('../static/images/round-pict1.png')} className="img-fluid" alt="IMI"  style={this.state.isMobile ? {width: '50%'} : {}}/>
                        </div>
                        <div className="col-md-6 col-sm-6 round2" style={{padding: 0}}>
                            <img src={require("../static/images/round-pict2.png")} className="img-fluid" alt="IMI" style={this.state.isMobile ? {width: '50%'} : {}}/>
                        </div>
                    </div>
                    <div className="row button-wrapper" style={{paddingBottom: '5em', paddingTop: '10em'}}>
                        <div className="col-md-4 mb-2 _btn1" style={{display: 'flex', justifyContent: 'center'}}>
                            <DropdownButton id="dropdown-basic-button" title="IMI" style={{backgroundColor: 'darkblue', minWidth: '240px'}}>
                                <MenuItem><Link route="/imi-registration/imi/pengawas">Pengawas</Link></MenuItem>
                                <MenuItem><Link route="/imi-registration/imi/pusat">IMI Pusat</Link></MenuItem>
                                <MenuItem><Link route="/imi-registration/imi/propinsi">IMI Propinsi</Link></MenuItem>
                                <MenuItem><Link route="/imi-registration/imi/asosiasi">Asosiasi</Link></MenuItem>
                            </DropdownButton>
                        </div>
                        <div className="col-md-4 mb-2 _btn2">
                            <button style={{backgroundColor:'darkblue', minWidth: '240px'}} className="btn text-white" type="button" onClick={() => Router.push('/imi-registration/atlet-nasional')}>
                                Atlet Nasional
                            </button>
                        </div>
                        <div className="col-md-4 mb-2 _btn3" style={{display: 'flex', justifyContent: 'center'}}>
                            <DropdownButton id="dropdown-basic-button" title="Undangan" style={{backgroundColor: 'darkblue', minWidth: '240px'}}>
                                <MenuItem><Link route="/imi-registration/undangan/tokoh-otomotif">Tokoh Otomotif</Link></MenuItem>
                                <MenuItem><Link route="/imi-registration/undangan/life-time-achievement">Life Time Achievement</Link></MenuItem>
                                <MenuItem><Link route="/imi-registration/undangan/media">Media</Link></MenuItem>
                                <MenuItem><Link route="/imi-registration/undangan/sponsor">Sponsor</Link></MenuItem>
                            </DropdownButton>
                        </div>
                    </div> */}
                    </div>
                    <style jsx global>{`
                    @media (max-width: 767px) {
                        .jumbotron{
                            padding: 10px !important;
                        }

                        .display-4{
                            font-size: 24px;
                        }

                        .round1 img, .round2 img{
                            width: 140px;
                        }

                        .button-wrapper ._btn1, .button-wrapper ._btn2, .button-wrapper ._btn3{
                            margin-bottom: 10px;
                            width: 100%;
                            display: block;
                            margin-top: 0 !important
                            
                        }

                        .button-wrapper {
                            margin-top: 1em !important
                        }

                        .btn{
                            line-height: 2em !important;
                            min-width: 200px !important
                        }

                        #dropdown-basic-button-imi{
                            line-height: 2em !important;
                            min-width: 200px !important
                        }
                    }
                `}</style>
                </div>
            </>
        );
    }
}

export default compose(
    withAuth(["PUBLIC"])
)(imiRegistration);