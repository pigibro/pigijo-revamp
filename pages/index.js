import React from 'react';
import { get, isEmpty, concat } from 'lodash';
import { compose } from "redux";
import Router from "next/router";
import withAuth from '../_hoc/withAuth';
import Head from '../components/head';
import Banner from '../components/home/banner';
import FlashSale from '../components/home/flashsale';
import NextGetaway from '../components/home/nextgateway';
import HowToBook from '../components/home/howtobook';
import TaCategory from '../components/home/ta-category';
import ActivityCategory from '../components/home/activity-category';
import PlaceCategory from '../components/home/place-category';
import SectionCategory from '../components/home/section-category';
import Xtremesale from '../components/home/Xtremesale'
import Blog from '../components/home/blog';
import Event from '../components/home/event';
import { modalToggle } from '../stores/actions'
import { getActivities, getActivityPromos, getDetailCategory, getPlaces } from '../stores/actions';
import { Modal } from "react-bootstrap";
import IconWrapper from '../components/home/icons-wrapper';
import BannersContainer from '../components/home/BannersContainer';
import Statistik from '../components/home/Statistik';
import SliderMobile from '../components/home/sliderMobile'
import AutoComplete from '../components/home/autocomplete-search'
import Partners from '../components/home/Partners';
import * as gtag from '../utils/gtag';
import getConfig from "next/config"
const { publicRuntimeConfig } = getConfig()

const { SEASON_TAG } = publicRuntimeConfig

// Router.events.on('routeChangeComplete', url => gtag.pageview(url))

class Index extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      recommended: {
        isLoading: false,
        data: [],
        page: 1,
        total: 0,
        isReload: false,
        isMoreLoading: false,
      },
      localExperiences: {
        isLoading: false,
        data: [],
        page: 1,
        isReload: false,
      },
      ourAssistance: {
        isLoading: false,
        data: [],
        page: 1,
        total: 0,
        isReload: false,
        isMoreLoading: false,
      },
      places: {
        isLoading: false,
        data: [],
        page: 1,
        total: 0,
        isReload: false,
        isMoreLoading: false,
      },
      specialOffer: {
        isLoading: false,
        data: [],
        page: 1,
        isReload: false,
        none: false
      },
      events: {
        isLoading: false,
        data: [],
        page: 1,
        isReload: false,
      },
      seasonal: {
        isLoading: false,
        data: [],
        page: 1,
        isReload: false,
      },
      seasonTag: 31,
      viewPopup: true,
      isMobile: false
    }
  }

  static async getInitialProps({ query, store }) {
    const { slug } = query;
    let errorCode = false;
    const resCategory = await store.dispatch(getDetailCategory('local-experiences'));
    const resCategory1 = await store.dispatch(getDetailCategory('local-assistants'));
    const resCategory2 = await store.dispatch(getDetailCategory('events'));
    errorCode = get(resCategory, 'meta.code') !== 200 ? 404 : false;
    errorCode = get(resCategory1, 'meta.code') !== 200 ? 404 : false;
    errorCode = get(resCategory2, 'meta.code') !== 200 ? 404 : false;
    const localExperiencesCategory = get(resCategory, 'data');
    const ourAssistanceCategory = get(resCategory1, 'data');
    const eventsCategory = get(resCategory2, 'data');
    return { errorCode, slug, localExperiencesCategory, ourAssistanceCategory, eventsCategory }
  }

  componentDidMount() {
    // window.innerWidth > 480 ? this.setState({isMobile: false}) : this.setState({isMobile: true})
    // this.props.dispatch(modalToggle(true, 'xtremesale'))
    const { localExperiences, ourAssistance, recommended, specialOffer, places, events, seasonal, seasonTag } = this.state;
    const { localExperiencesCategory, ourAssistanceCategory, eventsCategory } = this.props;
    // let visited = localStorage["alreadyVisited"];
    //     if(visited) {
    //          this.setState({ viewPopup: false })
    //          //do not view Popup
    //     } else {
    //          //this is the first time
    //          localStorage["alreadyVisited"] = true;
    //          this.setState({ viewPopup: true});
    //     }
    let recommendSum = 6
    if (isEmpty(seasonal.data) && SEASON_TAG) {
      this.setState({
        seasonal: {
          isLoading: true,
          data: [],
          isReload: false,
        }
      });
      const params = {
        category_id: localExperiencesCategory.id,
        tags: SEASON_TAG
      };
      this.props.dispatch(getActivities(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          recommendSum = 6 - result.data.length
          this.setState({
            seasonal: {
              isLoading: false,
              data: get(result, 'data'),
              isReload: false,
            }
          });
        } else {
          this.setState({
            seasonal: {
              isLoading: false,
              data: [],
              isReload: true,
            }
          });
        }
      });
    }
    if (isEmpty(localExperiences.data)) {
      this.setState({
        localExperiences: {
          isLoading: true,
          data: [],
          isReload: false,
        }
      });
      const params = {
        per_page: 8,
        category_id: localExperiencesCategory.id,
      };
      this.props.dispatch(getActivities(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            localExperiences: {
              isLoading: false,
              data: get(result, 'data'),
              isReload: false,
            }
          });
        } else {
          this.setState({
            localExperiences: {
              isLoading: false,
              data: [],
              isReload: true,
            }
          });
        }
      });
    }
    if (isEmpty(ourAssistance.data)) {
      this.setState({
        ourAssistance: {
          isLoading: true,
          data: [],
          isReload: false,
          isMoreLoading: false,
        }
      });
      const params = {
        per_page: 6,
        category_id: ourAssistanceCategory.id
      };
      this.props.dispatch(getActivities(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            ourAssistance: {
              isLoading: false,
              total: get(result, 'pagination.total'),
              data: get(result, 'data'),
              isReload: false,
              isMoreLoading: false,
            }
          });
        } else {
          this.setState({
            ourAssistance: {
              isLoading: false,
              data: [],
              isReload: true,
              isMoreLoading: false,
            }
          });
        }
      });
    }
    if (isEmpty(places.data)) {
      this.setState({
        places: {
          isLoading: true,
          data: [],
          isReload: false,
          isMoreLoading: false,
        }
      });
      const params = {
        per_page: 8,
      };
      this.props.dispatch(getPlaces(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            places: {
              isLoading: false,
              total: get(result, 'pagination.total'),
              data: get(result, 'data'),
              isReload: false,
              isMoreLoading: false,
            }
          });
        } else {
          this.setState({
            places: {
              isLoading: false,
              data: [],
              isReload: true,
              isMoreLoading: false,
            }
          });
        }
      });
    }
    if (isEmpty(recommended.data)) {
      this.setState({
        recommended: {
          isLoading: true,
          data: [],
          isReload: false,
          isMoreLoading: false,
        }
      });
      const params = {
        per_page: recommendSum,
        category_id: localExperiencesCategory.id,
        sort_by: "recommended|DESC",

      };
      this.props.dispatch(getActivities(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            recommended: {
              isLoading: false,
              total: get(result, 'pagination.total'),
              data: get(result, 'data'),
              isReload: false,
              isMoreLoading: false,
            }
          });
        } else {
          this.setState({
            recommended: {
              isLoading: false,
              data: [],
              isReload: true,
              isMoreLoading: false,
            }
          });
        }
      });
    }
    if (isEmpty(specialOffer.data)) {
      this.setState({
        specialOffer: {
          isLoading: true,
          data: [],
          isReload: false,
          none: true
        }
      });
      const params = {
        per_page: 10,
        promo: 1
      };
      this.props.dispatch(getActivities(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            specialOffer: {
              isLoading: false,
              data: get(result, 'data'),
              isReload: false,
            }
          });
        } else {
          this.setState({
            specialOffer: {
              isLoading: false,
              data: [],
              isReload: true,
              none: true
            }
          });
        }
      });
    }
    if (isEmpty(events.data)) {
      this.setState({
        events: {
          isLoading: true,
          data: [],
          isReload: false,
        }
      });
      const params = {
        per_page: 6,
        category_id: eventsCategory.id
      };
      this.props.dispatch(getActivities(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            events: {
              isLoading: false,
              data: get(result, 'data'),
              isReload: false,
            }
          });
        } else {
          this.setState({
            events: {
              isLoading: false,
              data: [],
              isReload: true,
            }
          });
        }
      });
    }
  }

  moreCategory = (e) => {
    const { recommended, ourAssistance, places, events } = this.state;
    const { ourAssistanceCategory, localExperiencesCategory, eventsCategory } = this.props;
    const category = e.target.id;
    if (category === 'local-assistants') {
      if (ourAssistance.data.length <= ourAssistance.total) {
        const params = {
          page: (ourAssistance.page || 1) + 1,
          per_page: 6,
          category_id: ourAssistanceCategory.id
        };
        this.setState({
          ourAssistance: {
            isLoading: false,
            data: ourAssistance.data,
            page: ourAssistance.page,
            isReload: false,
            total: ourAssistance.total,
            isMoreLoading: true,
          }
        });

        this.props.dispatch(getActivities(params)).then((result) => {
          if (get(result, 'meta.code') === 200) {
            this.setState({
              ourAssistance: {
                isLoading: false,
                data: ourAssistance.data.concat(get(result, 'data')),
                page: (ourAssistance.page || 1) + 1,
                isReload: false,
                total: get(result, 'pagination.total'),
                isMoreLoading: false,
              }
            });
          }
        });
      }
    }
    if (category === 'recommended') {
      if (recommended.data.length <= recommended.total) {
        const params = {
          page: (recommended.page || 1) + 1,
          per_page: 6,
          category_id: localExperiencesCategory.id,
          sort_by: "recommended|DESC",
        };
        this.setState({
          recommended: {
            isLoading: false,
            data: recommended.data,
            page: recommended.page,
            isReload: false,
            total: recommended.total,
            isMoreLoading: true,
          }
        });

        this.props.dispatch(getActivities(params)).then((result) => {
          if (get(result, 'meta.code') === 200) {
            this.setState({
              recommended: {
                isLoading: false,
                data: recommended.data.concat(get(result, 'data')),
                page: (recommended.page || 1) + 1,
                isReload: false,
                total: get(result, 'pagination.total'),
                isMoreLoading: false,
              }
            });
          }
        });
      }
    }
    if (category === 'places') {
      if (places.data.length <= places.total) {
        const params = {
          page: (places.page || 1) + 1,
          per_page: 8,
        };
        this.setState({
          places: {
            isLoading: false,
            data: places.data,
            page: places.page,
            isReload: false,
            total: places.total,
            isMoreLoading: true,
          }
        });

        this.props.dispatch(getPlaces(params)).then((result) => {
          if (get(result, 'meta.code') === 200) {
            this.setState({
              places: {
                isLoading: false,
                data: places.data.concat(get(result, 'data')),
                page: (places.page || 1) + 1,
                isReload: false,
                total: get(result, 'pagination.total'),
                isMoreLoading: false,
              }
            });
          }
        });
      }
    }
    if (category === 'events') {
      if (events.data.length <= events.total) {
        const params = {
          page: (events.page || 1) + 1,
          per_page: 6,
          category_id: eventsCategory.id,
          sort_by: "created_at|DESC",
        };
        this.setState({
          events: {
            isLoading: false,
            data: events.data,
            page: events.page,
            isReload: false,
            total: events.total,
            isMoreLoading: true,
          }
        });

        this.props.dispatch(getActivities(params)).then((result) => {
          if (get(result, 'meta.code') === 200) {
            this.setState({
              events: {
                isLoading: false,
                data: events.data.concat(get(result, 'data')),
                page: (events.page || 1) + 1,
                isReload: false,
                total: get(result, 'pagination.total'),
                isMoreLoading: false,
              }
            });
          }
        });
      }
    }
  }
  // static async getInitialProps({store}) {
  //   const currentState = store.getState();

  //   const blog = get(currentState, 'blog.data');
  //   const event = get(currentState, 'event.data');
  //   const flashsale = get(currentState, 'activity.data');

  //   if (isEmpty(blog))
  //     await store.dispatch( getLatestBlog() );
  //   if (isEmpty(event))
  //     await store.dispatch( getEvent() );
  //   if (isEmpty(flashsale))
  //     await store.dispatch( getActivityPromos() );
  // }
  // onHide = () => this.setState({ viewPopup: false });
  render() {
    const { localExperiences, ourAssistance, specialOffer, recommended, places, events, seasonal } = this.state;
    const { dispatch } = this.props;
    return (
      <div>
        <Head />
        <Banner dispatch={dispatch} />
        {/* <Modal
            show={this.state.viewPopup}
            onHide={this.onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
          <Modal.Body closeButton>
            <a href="#" style={{position: 'relative'}}>
            <img src="/static/images/bannerapps.png" style={{ width: '100%'}}/>
            </a>
          </Modal.Body>
        </Modal> */}

        {/* <SliderMobile /> */}

        {/* <BannersContainer /> */}

        {/* <IconWrapper /> */}

        <div className='container' style={{ 'zIndex': '-2' }}>
          <div className='row' style={{ display: 'block', justifyContent: 'center', paddingTop: '2em' }}>
            <div className='col-sm-12 banner-container'>
              <a href='/p/1564-diving-in-amed-bali-certified-divers'><img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/11/diving-in-amed-bali-certified-divers.jpg' width='100%' /></a>
            </div>
          </div>
        </div>

        <div className='container' style={{ 'zIndex': '-2' }}>
          <div className='row' style={{ display: 'block', justifyContent: 'center', paddingTop: '2em' }}>
            <div className='col-sm-12 banner-container'>
              <a href='/p/1861-newnormal-lombok-3d2n-montana-premier-3'><img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/11/newnormal-lombok-3d2n-montana-premier-3.jpg' width='100%' /></a>
            </div>
          </div>
        </div>

        <div className='container' style={{ 'zIndex': '-2' }}>
          <div className='row' style={{ display: 'block', justifyContent: 'center', paddingTop: '2em' }}>
            <div className='col-sm-12 banner-container'>
              <a href='/p/2072-diving-in-nusa-penida-certified-divers'><img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/11/diving-in-nusa-penida-certified-divers.jpg' width='100%' /></a>
            </div>
          </div>
        </div>

        {/* <AutoComplete dispatch={dispatch}/> */}

        {/* <Statistik /> */}

        <TaCategory loopTo={[1, 1, 1]} classProps={[4, 6, 12]} title="Travel Assistance Experiences" subtitle="Can’t make it to your planned destination? Take one of our online experiences with a passionate local in your own home." moreLoading={ourAssistance.isMoreLoading} isLoading={ourAssistance.isLoading} isReload={ourAssistance.isReload} data={ourAssistance.data} link={"local-assistants"} model={1} moreBtn moreData={this.moreCategory} total={ourAssistance.total} />

        {
          (isEmpty(specialOffer.data)) ? null : (
            <FlashSale data={specialOffer.data} isReload={specialOffer.isReload} isLoading={specialOffer.isLoading} />
          )
        }



        {/* <Xtremesale isMobile={this.state.isMobile} loopTo={[1,1,1]} classProps={[4,6,12]} isLoading={specialOffer.isLoading} isReload={specialOffer.isReload} data={specialOffer.data} link={"recommended"} model={1}/> */}
        {/* <ActivityCategory loopTo={[1,1,1,1]} classProps={[3,6,6]} title="Local Experiences" isLoading={localExperiences.isLoading} isReload={localExperiences.isReload} data={localExperiences.data} link={"local-experiences"} /> */}
        {/* <ActivityCategory loopTo={[1,1,1]} classProps={[4,6,12]} title="Events" moreLoading={events.isMoreLoading} isLoading={events.isLoading} isReload={events.isReload} data={events.data} link={"events"} model={1} moreBtn moreData={this.moreCategory} total={events.total} /> */}
        <ActivityCategory loopTo={[1, 1, 1]} classProps={[4, 6, 12]} title="Local Experiences You’ll Love" subtitle="Love simplicity. Traveling packages made for you, provided by local agents" moreLoading={recommended.isMoreLoading} isLoading={recommended.isLoading} isReload={recommended.isReload} data={[...seasonal.data, ...recommended.data]} link={"recommended"} model={1} moreBtn moreData={this.moreCategory} total={recommended.total} />
        {/* <SectionCategory title="Meet Our Assistants" description="Wanna go someplace for traveling but never been there? And you wonder, who can help me getting around to see the good places this place have to offer? or simply to taste the local dishes?
Fret not, we got you covered! check out our local travel assistant below, they will give you the best recommendations and help you need to create your awesome next adventure"/> */}
        {/*<ActivityCategory loopTo={[1,1,1]} classProps={[4,6,12]} title="Meet Our Assistants" subtitle="They will help you create your unforgettable experiences in Indonesia" moreLoading={ourAssistance.isMoreLoading} isLoading={ourAssistance.isLoading} isReload={ourAssistance.isReload} data={ourAssistance.data} link={"local-assistants"} model={1} moreBtn moreData={this.moreCategory} total={ourAssistance.total} />*/}
        <PlaceCategory loopTo={[1, 1, 1, 1]} classProps={[3, 6, 6]} title="Places" subtitle="Discover thousands of amazing places to visit in Indonesia" isLoading={places.isLoading} isReload={places.isReload} data={places.data} link={"places"} moreBtn moreData={this.moreCategory} total={places.total} />
        {/* <FlashSale/>
        <NextGetaway/>
        <Event/>
        <HowToBook/> */}
        <Blog />
        <Partners />
        <style jsx>{`
          .wh-widget-send-button-get-button{
            display: none;
          }

          

          .cat-header{
            z-index: -2
          }

          .planning-area{
            z-index: 9999 !important;
          }

          #desk-advance-search{
            z-index: 9999;
          }
          
          
          @media (max-width: 767px) {
            .pigijo-middle-nav li.nav-item{
              display: inline;
            }
            
            .pigijo-middle-nav li{
              width: 25%;
              float: left;
              margin-bottom: 10px;
            }
            
            .pigijo-middle-nav li.nav-item a img{
              width: 48px;
            }
            
            .img-banner1, .img-banner2, .img-banner3, .img-banner4{
              width: 100%;
              height: 360px;
              object-fit: cover;
            }
            
            .bg-content-home .container-fluid{
              padding: 0 1em !important;
            } 
          }
          
          
        `}</style>


      </div>
    )
  }
}
export default compose(
  withAuth(["PUBLIC"])
)(Index);
