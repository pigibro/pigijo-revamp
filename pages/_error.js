import { compose } from "redux"
import {Link} from '../routes'
import React from 'react'
import withAuth from '../_hoc/withAuth'
import Head from '../components/head'

class Error extends React.Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

render() {
  const { statusCode } = this.props;
  return (
    <main className="body-auth">
      <div className="boxed-auth">
        <Head />
        {(statusCode == 404) &&
        <>
        <Link route="index" >
            <a className="box-logo">
                <img className="logo" src="/static/images/404.svg" alt="page not found" />
            </a>
        </Link>
        <div className="not-found">
          <h3>O.. Oh.. Page Not Found!</h3>
          <span>
            <Link route="index"><a className="btn btn-primary btn-sm btn-notfound">Back to Home</a></Link>
          </span>
        </div>
        </>
        }
        {(statusCode != 404) &&
        <>
        <Link route="index" >
            <a className="box-logo">
                <img className="logo" src="/static/images/maintenance.svg" alt="page error 500" />
            </a>
        </Link>
            <div className="not-found"> 
              <h3>Sorry, something went wrong and we are working on it.</h3>
              <p>We'll be up and running shortly</p>
            </div>
        </>
        }
        <br />
        <br />
        <br />
      </div>
    </main>
  )
}
}

export default Error;