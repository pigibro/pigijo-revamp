import { compose } from "redux";
import { connect } from "react-redux";
import { get, isEmpty } from "lodash";
import Head from "../components/head";
import { Link } from "../routes";
import PageHeader from "../components/page-header-icons";
import React from "react";
import withAuth from "../_hoc/withAuth";
import { getCarListDefault, getCarList } from "../stores/actions/rentCarActions";
import { getLocations } from "../stores/actions/cityActions";
import { storeCartRentCar, getCartList, createToken, praCheckout, setSuccessMessage, setErrorMessage, getDetailCategory } from '../stores/actions';
import RentCarIndex from "../components/rentCar";
import SideSearch from "../components/rentCar/src/searchSide";
import FilterBy from "../components/rentCar/src/filterBy";
import moment from "moment";
import Router from 'next/router';
import { convertToRp, hitungSelisih } from "../utils/helpers";
import IconLink from "../components/shared/iconLink";
import * as gtag from '../utils/gtag';

// Router.events.on('routeChangeComplete', url => gtag.pageview(url))

class RentCar extends React.Component {
	static async getInitialProps({ query, store}) {
			const resCategory = await store.dispatch(getDetailCategory('rent-car'));
			const category = get(resCategory,'data');
        return { query, category}
    }
	constructor(props) {
		super(props);
		this.state = {
			duration: 1,
			startDate: moment(),
			endDate: moment().add(1, 'days'),
			focusedInput: null,
			time: null,

			destination: null,

			type: "",
			transmission: "",
			brand: "",

			page: 1,

			minMax: {
				min: 0,
				max: 1500000,
			},

			stick: false,
		};
	}



	componentDidMount() {
		const {
			locationName,
			locationType,
			startDaterent,
			endDaterent,
			TotalCar,
			type,
			slug
		} = this.props.query;
		
		this.props.dispatch(getCarList(
				this.props.token,
				locationName || slug ?(locationName ? locationName : slug):'',
				'',
				'',
				'',
				'',
				'',
				'',
				1,
				startDaterent?startDaterent:'',
				locationType || type ? (locationType ? locationType : type):'')
			);
		this.props.dispatch(getLocations(""));
	}

	//============search=============
	handleDestination = (city) => {
		let getObj = city[0];
		this.setState({ destination: getObj }, () => {
			if (getObj !== undefined) {
				this.findRentCar();
			}
		});
	};

	onDatesChange = ({ startDate, endDate }) => {
		this.setState({ startDate, endDate });
		if (endDate !== null) {
			let day = hitungSelisih(startDate, endDate);
			this.setState({ duration: day + 1 });
			this.findRentCar();
		}
	};

	onFocusChange = (focusedInput) => {
		this.setState({ focusedInput });
	};

	handleTime = (e) => {
		this.setState({
			time: moment(e, "HH:mm").format("HH:mm"),
		});
	};

	//=====================================

	//===========filter===================
	handleOnChange = (e) => {
		const { name, value } = e.target;
		this.setState({ [name]: value }, () => this.findRentCar(e));
	};

	findRentCar = (e) => {
		if (!isEmpty(e)) {
			e.preventDefault();
		}
		const { startDate, minMax, type, transmission, brand, destination, page, duration } = this.state;
		const { paramDetail, token, dispatch, query } = this.props;
		let price = minMax.max !== 0 ? `${minMax.min}-${minMax.max}` : "";
		let cekDesti = destination !== null ? destination : paramDetail;
		dispatch(
			getCarList(
				token,
				destination !== null ? cekDesti.slug : query.slug,
				transmission,
				brand,
				type,
				paramDetail.agency,
				price,
				page,
				moment(startDate).format("YYYY-MM-DD"),
				duration,
				destination !== null ? cekDesti.type.toLowerCase() : (query.type ? query.type.toLowerCase() : null),
			),
		);
	};

	handlePrice = () => {
		this.findRentCar();
	};

	handlePriceRange = (val) => {
		this.setState({ minMax: val });
	};

	handlePageChange = (curPage) => {
		this.setState({ page: curPage }, () => {
			return this.findRentCar();
		});
	};

	addToCart = (id) => {
		event.preventDefault()
		let { startDate, endDate } = this.state
		const {carts, dispatch} = this.props;
		if(isEmpty(carts)){
			dispatch(createToken()).then(result => {
				if (get(result, 'meta.code') === 200) {
				dispatch(storeCartRentCar({token: get(result,'data.token'), start_date: moment(startDate).format('YYYY-MM-DD'), end_date: moment(endDate).format('YYYY-MM-DD'), product_id: id})).then(res => {
					if (get(res, 'meta.code') === 200) {
					// dispatch(praCheckout(get(result,'data.token'))).then(booking => {
					// 	if(get(booking,'data.status') === 'verified'){
					// 	Router.push('/checkout');
					// 	}
					// });
					// Router.back()
					}else{
					dispatch(setErrorMessage(get(res,'data')));
					}
				})
				}
			});
		}else{
			dispatch(storeCartRentCar({token: carts.token, start_date: moment(startDate).format('YYYY-MM-DD'), end_date: moment(endDate).format('YYYY-MM-DD'), product_id: id})).then(res => {
				if (get(res, 'meta.code') === 200) {
					dispatch(getCartList(carts.token, 'schedule|ASC'));
					dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
					// Router.back()
				}else{
				dispatch(setErrorMessage(get(res,'data')));
				}
			})
		}
	}

	bookNow = (id) => {
		event.preventDefault()
		
		let { startDate, endDate } = this.state
		const {carts, dispatch} = this.props;
	
		dispatch(createToken()).then(result => {
		  if (get(result, 'meta.code') === 200) {
			dispatch(storeCartRentCar({token: get(result,'data.token'), start_date: moment(startDate).format('YYYY-MM-DD'), end_date: moment(endDate).format('YYYY-MM-DD'), product_id: id})).then(res => {
			  if (get(res, 'meta.code') === 200) {
				dispatch(praCheckout(get(result,'data.token'))).then(booking => {
				  if(get(booking,'data.status') === 'verified'){
					Router.push('/checkout');
				  }
				});
			  }else{
				dispatch(setErrorMessage(get(res,'data')));
			  }
			})
		  }
		});
	}

	render() {
		const {
			slug,
			child,
			list_car,
			dataType,
			dataBrand,
			dataPrice,
			dataTrans,
			paramDetail,
			cartDetail,
			pagination,
			isLoading,
			locations,
			category
		} = this.props;
		const{
			locationName,
			locationId,
			startDaterent,
			endDaterent,
		} = this.props.query;

		const { startDate, endDate, focusedInput, minMax, type, transmission, brand, destination, duration } = this.state;

		return (
			<>
				<Head title={category.meta_title} description={category.meta_description} url={process.env.SITE_ROOT+'/rent-car'}/>
        		<PageHeader section='main' background={"/static/images/img05.jpg"} title={category.name} caption={category.short_description} category="rent-car" type={this.props.query.type} slug={this.props.query.slug}/>
				<section className="body-content mt2">
					<div className="container">
						{/* <IconLink route="rent-car" type={this.props.query.type} slug={this.props.query.slug}/> */}
						<div className="row">
							<div className="">
								<aside className="col-lg-3 col-md-3 col-sm-12 sidebar">
									<div className="panel">
										<SideSearch
											defaultValue={locationName?[{id:locationId, name:locationName}]:[]}
											locations={locations}
											startDate={startDate}
											endDate={endDate}
											focusedInput={focusedInput}
											handleDestination={this.handleDestination}
											onDatesChange={this.onDatesChange}
											onFocusChange={this.onFocusChange}
											handleTime={this.handleTime}
											findRentCar={(e) => this.findRentCar(e)}
											destination={destination}
											validation={isEmpty(destination) || isEmpty(endDate)}
											query={this.props.query}
										/>
									</div>
									<div className="panel">
										<FilterBy
											convertToRp={convertToRp}
											minMax={minMax}
											dataType={dataType}
											dataBrand={dataBrand}
											dataPrice={dataPrice}
											dataTrans={dataTrans}
											stateFilter={{
												type,
												transmission,
												brand,
											}}
											handlePrice={this.handlePrice}
											handlePriceRange={this.handlePriceRange}
											handleOnChange={this.handleOnChange}
										/>
									</div>
								</aside>
							</div>
							<div className="col-lg-9 col-md-3 col-sm-12">
								<section className="header-content">
									<RentCarIndex
										path={slug}
										child={child}
										listCar={list_car}
										paramDetail={paramDetail}
										cartDetail={cartDetail}
										pagination={pagination}
										handlePageChange={this.handlePageChange}
										isLoading={isLoading}
										handleOnClickBook={this.bookNow}
										handleOnClickAdd={this.addToCart}
									/>
								</section>
							</div>
						</div>
					</div>
				</section>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	list_car: state.rentCar.list_car,
	dataType: state.rentCar.dataType,
	dataBrand: state.rentCar.dataBrand,
	dataPrice: state.rentCar.dataPrice,
	dataTrans: state.rentCar.dataTrans,
	paramDetail: state.rentCar.paramDetail,
	cartDetail: state.rentCar.cartDetail,
	pagination: state.rentCar.pagination,
	isLoading: state.rentCar.isLoading,
	locations: state.city.data,
	carts: state.cart.data,
});

export default compose(
	connect(mapStateToProps),
	withAuth(["PUBLIC"]),
)(RentCar);
