import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';
import { sendContact, modalToggle } from "../stores/actions";
import Loading from "../components/shared/loading";

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
      opacity: 0.8,
  }
}

class ContactUs extends React.Component {
  constructor(props){
      super(props)
      this.state = {
          bookingid:'',
          subject:'',
          email:'',
          fullname:'',
          message:'',
          phone:'',
          isLoading:false,
      }
      this.handleOnChange = this.handleOnChange.bind(this)
      this.handleOnSubmit = this.handleOnSubmit.bind(this)
  }
  handleOnChange(e) {
      let name = e.target.name
      this.setState({
          [name]: e.target.value
      })
  }
  handleOnSubmit(e){
      e.preventDefault()
      const {dispatch} = this.props;
      // this.props.dispatch(sendContact(
      //     this.state.bookingid,
      //     this.state.email,
      //     this.state.subject,
      //     this.state.fullname,
      //     this.state.message,
      //     this.state.phone
      // ))
      this.setState({isLoading: true});
      dispatch(sendContact({
        bookingid: this.state.bookingid,
        email: this.state.email,
        subject: this.state.subject,
        fullname: this.state.fullname,
        message: this.state.message,
        phone: this.state.phone,
      })).then( result => {
        if (get(result, 'meta.code') === 200){
          dispatch(modalToggle(true, 'message', get(result, 'meta.message')));
        }
        this.setState({isLoading: false});
      });
  }
    render(){
      const {isLoading} = this.state;
      return (
        <>
          <Head title={"About Us"} description={"Contact Pigijo call center to get more information about your itinerary, payment status, and how to join become Pigijo's partner."} url={process.env.SITE_ROOT+'/about-us'}/>
          <PageHeader background={"/static/images/img05.jpg"} title={`Contact Us`} caption="Contact Pigijo help center to get more information about your itinerary, payment status, and how to join become Pigijo's partner."/>
          <div className="main-contact">
            <section className="content-wrap">
              <div className="container">
                  <div className="row flex-row flex-center">
                      <div className="col-lg-6 col-md-6 col-sm-6 contact-form">
                          <h5>Reach us anytime and we will get back to you as soon as possible.</h5>
                          <form onSubmit={this.handleOnSubmit}>
                              <div className="form-group">
                                  <span className="text-label">Booking Number (optional)</span>
                                  <input className="form-control" type="text" name="bookingid" onChange={this.handleOnChange} placeholder="Booking Number"/>
                              </div>
                              <div className="form-group">
                                  <span className="text-label">Full Name</span>
                                  <input className="form-control" type="text" name="fullname"  required onChange={this.handleOnChange} placeholder="Full Name"/>
                              </div>
                              <div className="form-group">
                                  <span className="text-label">Phone</span>
                                  <input className="form-control" type="number" name="phone"  required onChange={this.handleOnChange} placeholder="Phone Number" />
                              </div>
                              <div className="form-group">
                                  <span className="text-label">Subject</span>
                                  <input className="form-control" type="text" name="subject"  required onChange={this.handleOnChange} placeholder="Subject"/>
                              </div>
                              <div className="form-group">
                                  <span className="text-label">Email Address</span>
                                  <input className="form-control" type="email" name="email"  required onChange={this.handleOnChange} pattern='[^@]+@[^@]+\.[a-zA-Z]{2,6}' placeholder="email"/>
                              </div>
                              <div className="form-group">
                                  <span className="text-label">Your Message</span>
                                  <textarea className="js-textarea form-control" rows="4" name="message" defaultValue={""} required  onChange={this.handleOnChange} placeholder="Message"/>
                              </div>
                              <button className="btn btn-block btn-primary" type="submit">Submit</button>
                          </form>
                      </div>{/* col */}
                      <div className="col-lg-6 col-md-6 col-sm-6 text-center">
                          <img src={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/mascot01.svg"} width={300} className="mb2" alt="" />
                          <h4>Pigijo Call Center</h4>
                          <p>Please prepare your Pigijo booking number before contacting us.</p>
                          <a className="btn btn-lg btn-default mt1 mb1" href="tel:+6282246598802">(+62) 082246598802</a>
                          <p>or you can always reach us via <a href="mailto:info@pigijo.com">info@pigijo.com</a></p>
                      </div>{/* col */}
                  </div>{/* row */}
              </div>{/* container */}
            </section>
            {isLoading && 
              <Loading/>
            }
          </div>
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(ContactUs);