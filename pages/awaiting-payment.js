import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';
import moment from 'moment';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
      opacity: 0.8,
  }
}

class awaitingPayment extends React.Component {

    render(){
      return (
        <>
          <Head title={"Complete Your Payment"} description={"Hubungi Pusat Bantuan Pigijo untuk mendapatkan informasi lengkap dan jelas mengenai rencana perjalanan anda, status pembayaran, dan cara bergabung menjadi partner pigijo."} url={process.env.SITE_ROOT+'/how-to-book'}/>
          <PageHeader background={"/static/images/img05.jpg"} title={`Awaiting Payment`} caption=""/>
          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-8 col-lg-push-2 col-md-8 col-md-push-2 thankyou">
                  <h2 className="h3 mb1">
                    <center>Awaiting Payment</center>
                  </h2>
                  <p className="thankyou-content">
                    We have sent a booking invoice to your email.<br/>Please check your inbox and complete your payment before:
                    <p className="h5">{moment().add(6,'hours').format("LLLL")}</p>
                    </p>
                  <Link to="/"><a className="btn btn-block btn-primary thankyou-button">Continue to Homepages</a></Link>
                </div>
              </div>
            </div>
          </section>

          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-6 col-md-offset-3">
                  <h2 className="h4 mt1 mb1 text-center">
                   Need Help ?
                  </h2>
                  <p>
                  Don't receive booking invoice from pigijo ? our customer service are ready to help you find your way around by click our chat
                  </p>
                </div>
              </div>
            </div>
          </section>
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(awaitingPayment);