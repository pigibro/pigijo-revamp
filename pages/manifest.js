import React, { Component, Fragment } from "react";
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field as ReduxField, reduxForm, change } from 'redux-form';
import { get, map, isEmpty } from 'lodash';
import Error from './_error';
import Head from '../components/head';
import { Link } from '../routes';
import validate from '../components/contact-detail/validate';
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';
import { getCountries } from '../stores/actions';
import { SingleDatePicker } from "react-dates";
import DatePicker from 'react-datepicker'
import Router from "next/router";
import numberWithComma from '../utils/numberWithComma';
import ModalContainer from '../components/modals/container'
import openSocket from 'socket.io-client'
import { modalToggle, flightCart, getFareDetail, checkoutDetails, deleteCart, setErrorMessage } from '../stores/actions';
import { 
  Grid,
  Row, 
  Col, 
  Panel, 
  FormGroup, 
  ControlLabel, 
  Form, 
  FormControl,
  InputGroup,
  Modal
} from "react-bootstrap";
import 'react-dates/initialize';
import moment from 'moment';
import 'moment/locale/id'
import Loading from '../components/shared/loading';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import Axios from "axios";
import "react-datepicker/dist/react-datepicker-cssmodules.css";
import "react-datepicker/dist/react-datepicker-cssmodules.min.css";
import "react-datepicker/dist/react-datepicker.css";
import "react-datepicker/dist/react-datepicker.min.css";
import getConfig from "next/config"
import { logEvent } from "../utils/analytics.js";
import { isMobile } from "react-device-detect";
const { publicRuntimeConfig } = getConfig()

const { SOCKET_URL } = publicRuntimeConfig
moment.locale('id');

const renderField = ({
  input, placeholder, type, meta: { touched, error }, title
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <input className="form-control" {...input} placeholder={placeholder} type={type}/>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

const renderSelect = ({
  input, placeholder, meta: { touched, error }, title, option, defaultValue
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <select className='form-control' {...input} >
        <option>Country code</option>
        {
          map(option, (item,idx) => (
            <option key={`ext-${idx}`}>{item.area_code}</option>
          ))
        }
        
      </select>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

const renderSelectSalutation = ({
  input, placeholder, meta: { touched, error }, title
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <select className='form-control' {...input} >
        <option>Title</option>
        <option value='Mr'>Mr.</option>
        <option value='Mrs'>Mrs.</option>
        <option value='Ms'>Miss.</option>
      </select>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

const renderSelectNational = ({
  input, placeholder, meta: { touched, error }, title, option, defaultValue
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <select className='form-control' {...input} >
        <option>Select your nationality</option>
        {
          map(option, (item,idx) => (
            <option key={`national-${idx}`} value={item.code.toUpperCase()}>{item.name}</option>
          ))
        }
        
      </select>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

const renderSelectExt = ({
  input, placeholder, meta: { touched, error }, title, option, defaultValue
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <select className='form-control' {...input} >
        {
          map(option, (item,idx) => (
            <option key={`ext-${idx}`} value={item.area_code}>{item.area_code}</option>
          ))
        }
        
      </select>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

const renderSelectAdult = ({
  input, placeholder, meta: { touched, error }, title, option, defaultValue
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <select className='form-control' {...input} >
        <option>Select adult association</option>
        {
          map(option, (item,idx) => (
            <option key={`adult-${idx}`} value={idx+1}>Adult {idx+1}</option>
          ))
        }
        
      </select>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);



class Manifest extends Component {
  static async getInitialProps(context) {
		const { req, store, query } = context;
    await store.dispatch( getCountries([]) );
    
    return { query }
	}
  state={
    contact_title: "",
    Adult: 0,
    Child: 0,
    Infant: 0,
    focused: false,
    date: null,
    baggage: "0 kg - Rp 0",
    flight: [],
    token: null,
    key: null,
    adultFare: 0,
    childFare: 0,
    infantFare: 0,
    totalFare: 0,
    customer: {},
    passengers: {},
    departFare: {},
    returnFare: {},
    isInternational: false,
    isModal: false,
    params: {},
    TotalChanging: false,
    newTotal: 0,
    isLoading: false,
    depart_flight_cart: {},
    return_flight_cart: {},
    flightUnavailable: false,
    adultValDOB: [],
    childValDOB: [],
    infantValDOB: [],
    formVal: false,
    emptyField: false,
    paymentUrl: ''
  };

  async componentDidMount() {
    
    const { departure_flight, return_flight, dispatch, form, change, profile} = this.props
    if(profile){
      dispatch(change(form, 'firstnameUser', profile.firstname));
      dispatch(change(form, 'lastnameUser', profile.lastname));
      dispatch(change(form, 'emailUser', profile.email));
      if(profile.phone){
        const phone = profile.phone.substring(3,profile.phone.length);
        dispatch(change(form, 'extUser', profile.phone.substring(0,3)));
        dispatch(change(form, 'phoneUser', phone));
      }
    }
    const { id, adult, child, infant } = this.props.query;
    let isInternational =  false 
    let customer= {
      "salutation":"Mr",
      "email":"",
      "firstname":"",
      "lastname":"",
      "phone":"",
      "country_code":"id"
    }
    let flights = []
    let passengers= {}
    let adultValDOB = []
    let childValDOB = []
    let infantValDOB = []
    this.setState({isLoading: true})
    
    // if(!departure_flight.Id) {
      dispatch(flightCart(id)).then(result => {
        let data = result.data.miscellaneous.Segments
        flights = data
        
        if(data.length > 1) {
          if(data[0].IsInternational || data[1].IsInternational) {
            isInternational = true
          }
        } else { isInternational = data[0].IsInternational }

        let pass = this.setPassengers(adult, child, infant, isInternational)

        this.setState({
          id,
          Adult: +adult,
          Child: +child,
          Infant: +infant,
          token: id,
          flight: flights,
          isRoundTrip: return_flight.Id ? true : false,
          isInternational : return_flight.Id ? ( (departure_flight || return_flight) ? true : false ) : departure_flight.IsInternational,
          customer,
          isLoading: false,
          totalFare: result.data.total_paid
        })
      }).catch(err => console.log(err))
    // } 
    // else {
    //   if(return_flight.Id) {
    //     if(departure_flight.isInternational || return_flight.isInternational) {
    //       isInternational = true
    //     }
    //     flights = [departure_flight, return_flight]
    //   } else { 
    //     isInternational = departure_flight.isInternational 
    //     flights = [departure_flight]
    //   }

    //   let pass = this.setPassengers(adult, child, infant, isInternational)
    //   if(departure_flight.FareBreakdowns === null || return_flight.FareBreakdowns === null){
    //     let departDataReq = {
    //       "Airline": departure_flight.Airline,
    //       "Adult": +adult,
    //       "Child": +child,
    //       "Infant": +infant,
    //       "ClassId": departure_flight.ClassObjects[0].Id,
    //       "FlightId": departure_flight.Id,
    //       "Fare": departure_flight.Fare,
    //       "Tax": departure_flight.ClassObjects[0].Tax
    //     }      
    //     dispatch(getFareDetail(departDataReq)).then(resfareDepart => {
    //       this.setState({departFare: resfareDepart.data})
    //       if(return_flight.Id) {
    //         let returnDataReq = {
    //           "Airline": return_flight.Airline,
    //           "Adult": adult,
    //           "Child": child,
    //           "Infant": infant,
    //           "ClassId": return_flight.ClassObjects[0].Id,
    //           "FlightId": return_flight.Id,
    //           "Fare": return_flight.Fare,
    //           "Tax": return_flight.ClassObjects[0].Tax
    //         }
    //         dispatch(getFareDetail(returnDataReq)).then(resfareReturn => {
    //           this.setState({returnFare: resfareReturn.data})
    //         })
    //       } else {
    //         this.countFare()
    //       }
    //     })
    //   } else {
    //     this.countFare()
    //   }

    //   this.setState({
    //     id,
    //     Adult: +adult,
    //     Child: +child,
    //     Infant: +infant,
    //     token: id,
    //     flight: flights,
    //     isRoundTrip: return_flight.Id ? true : false,
    //     isInternational : return_flight.Id ? ( (departure_flight || return_flight) ? true : false ) : departure_flight.IsInternational,
    //     customer,
    //     isLoading: false,
    //   })
    // }          
  }

  setPassengers = async (adult, child, infant, isInternational) => {
    let { dispatch, form } = this.props
    let passengers= {}
    let adultPass = []
    let childPass = []
    let infantPass = []
    let adultValDOB = []
    let childValDOB = []
    let infantValDOB = []
    for(let i = 0; i < adult; i ++) {
      await dispatch(change(form, 'ext_Adt_'+i,'+62'));
      await dispatch(change(form, 'Nationality_Adt_'+i,'ID'));
      let tempAdt = {
        "Type": 1,
        "Title": "Mr",
        "FirstName": "",
        "LastName": "",
        "BirthDate": new Date(),
        "Email": "",
        "MobilePhone": "",
        "IdNumber": "",
        "Nationality": "id", 
      }
      if (isInternational) {
        tempAdt.PassportNumber = ""
        tempAdt.PassportExpire = ""
        tempAdt.PassportOrigin = "id"
      } 
      adultValDOB.push('')
      adultPass.push(tempAdt)
    }

    for(let i = 0; i < child; i ++) {  
      await dispatch(change(form, 'ext_Chd_'+i,'+62'));
      await dispatch(change(form, 'Nationality_Chd_'+i,'ID'));
      let tempChd = {
        "Type": 2,
        "Title": "Mr",
        "FirstName": "",
        "LastName": "",
        "BirthDate": new Date(),
        "Email": "",
        "MobilePhone": "",
        "IdNumber": "",
        "Nationality": "id", 
      }
      if (isInternational) {
        tempChd.PassportNumber = ""
        tempChd.PassportExpire = ""
        tempChd.PassportOrigin = "id"
      } 
      childValDOB.push('')
      childPass.push(tempChd)
    }
    
    for(let i = 0; i < infant; i ++) {  
      await dispatch(change(form, 'ext_Inf_'+i,'+62'));
      await dispatch(change(form, 'Nationality_Inf_'+i,'ID'));
      
      let tempInf = {
        "Type": 3,
        "Title": "Mr",
        "FirstName": "",
        "LastName": "",
        "BirthDate": new Date(),
        "Email": "",
        "MobilePhone": "",
        "IdNumber": "",
        "Nationality": "id", 
        "AdultAssoc": 0
      }
      if (isInternational) {
        tempInf.PassportNumber = ""
        tempInf.PassportExpire = ""
        tempInf.PassportOrigin = "id"
      } 
      infantValDOB.push('')
      infantPass.push(tempInf)
    }
    passengers.Adt = adultPass
    passengers.Chd = childPass
    passengers.Inf = infantPass

    this.setState ({
      passengers,
      adultValDOB,
      childValDOB,
      infantValDOB
    })
  }

  countFare() {
    const { id, adult, child, infant } = this.props.query;
    const { departure_flight, return_flight } = this.props
    const { departFare, returnFare,isRoundTrip } = this.state
    let adultFare = 0;
    let childFare = 0;
    let infantFare = 0;
    let totalFare = 0;
    if(departure_flight.FareBreakdowns === null || return_flight.FareBreakdowns === null) {
      for(let i=0; i < departFare.Details.length; i++) {
        if(departFare.Details[i].Code == 'ADT') {
          adultFare = Math.round(departFare.Details[i].Amount)
        } else if ( departFare.Details[i].Code == 'CHD') {
          childFare = Math.round(departFare.Details[i].Amount)
        } else if( departFare.Details[i].Code == 'INF') {
          infantFare = Math.round(departFare.Details[i].Amount)
        }
      }
      if(isRoundTrip) {
        for(let j=0; j < returnFare.Details.length; j++) {
          if(returnFare.Details[j].Code == 'ADT') {
            adultFare = Math.round(adultFare + returnFare.Details[j].Amount)
          } else if ( returnFare.Details[j].Code == 'CHD') {
            childFare = Math.round(childFare + returnFare.Details[j].Amount)
          } else if( returnFare.Details[j].Code == 'INF') {
            infantFare = Math.round(infantFare + returnFare.Details[j].Amount)
          }
        }
      }
      totalFare = isRoundTrip ? departFare.Total + returnFare.Total : departFare.Total
    } else if(departure_flight.FareBreakdowns || return_flight.FareBreakdowns){
      let AdtPax = 0;
      let ChdPax = 0;
      let InfPax = 0;
      for(let k=0; k < departure_flight.FareBreakdowns.length; k++ ) {
        let data = departure_flight.FareBreakdowns[k]
        if(data.PaxType === 'ADT') {
          for(let l=0; l < data.Charges.length; l++) {
            AdtPax = Math.round(AdtPax + data.Charges[l].Amount)
          }
        }
        if(data.PaxType === 'CHD') {
          for(let l=0; l < data.Charges.length; l++) {
            ChdPax = Math.round(ChdPax + data.Charges[l].Amount)
          }
        }
        if(data.PaxType === 'ADT') {
          for(let l=0; l < data.Charges.length; l++) {
            InfPax = Math.round(InfPax + data.Charges[l].Amount)
          }
        }
      }
      if(isRoundTrip) {
        for(let k=0; k < return_flight.FareBreakdowns.length; k++ ) {
          let data = return_flight.FareBreakdowns[k]
          if(data.PaxType === 'ADT') {
            for(let l=0; l < data.Charges.length; l++) {
              AdtPax = Math.round(AdtPax + data.Charges[l].Amount)
            }
          }
          if(data.PaxType === 'CHD') {
            for(let l=0; l < data.Charges.length; l++) {
              ChdPax = Math.round(ChdPax + data.Charges[l].Amount)
            }
          }
          if(data.PaxType === 'ADT') {
            for(let l=0; l < data.Charges.length; l++) {
              InfPax = Math.round(InfPax + data.Charges[l].Amount)
            }
          }
        }
      }

      adultFare = adult * AdtPax
      childFare = child * ChdPax
      infantFare = infant * InfPax
      totalFare = adultFare + childFare + infantFare
    }
    
    
    this.setState({
      adultFare,
      childFare,
      infantFare,
      totalFare
    })
  }

  onCheckout(value) {
    this.setState({isLoading: true})
    let {id, adult, child, infant} = this.props.query
    let {passengers, customer, adultValDOB, childValDOB, infantValDOB} = this.state
    let temPass = passengers
    let emptyField = false
    for(let i = 0; i < adult; i++) {
      if(
        !value[`Title_Adt_${i}`] ||
        !value[`FirstName_Adt_${i}`] ||
        !value[`LastName_Adt_${i}`] ||
        !value[`Email_Adt_${i}`] ||
        !value[`phone_Adt_${i}`] ||
        !value[`Nationality_Adt_${i}`]
      ){
        emptyField = true
      }
      value[`MobilePhone_Adt_${i}`] = value[`ext_Adt_${i}`] + value[`phone_Adt_${i}`]
    }
    for(let i = 0; i < child; i++) {
      if(
        !value[`Title_Chd_${i}`] ||
        !value[`FirstName_Chd_${i}`] ||
        !value[`LastName_Chd_${i}`] ||
        !value[`Email_Chd_${i}`] ||
        !value[`phone_Chd_${i}`] ||
        !value[`Nationality_Chd_${i}`]
      ){
        
        emptyField = true
      }
      value[`MobilePhone_Chd_${i}`] = value[`ext_Chd_${i}`] + value[`phone_Chd_${i}`]
    }
    for(let i = 0; i < infant; i++) {
      if(
        !value[`Title_Inf_${i}`] ||
        !value[`FirstName_Inf_${i}`] ||
        !value[`LastName_Inf_${i}`] ||
        !value[`Email_Inf_${i}`] ||
        !value[`phone_Inf_${i}`] ||
        !value[`Nationality_Inf_${i}`] ||
        !value[`AdultAssoc_Inf_${i}`]
      ){
        emptyField = true
      }
      value[`MobilePhone_Inf_${i}`] = value[`ext_Inf_${i}`] + value[`phone_Inf_${i}`]
    }
    for(let key in value) {
      let keys = key.split("_")      
      if(keys.length > 1 && key !== 'contact_details') {
        let type = temPass[keys[1]]
        if(keys[0] != 'ext' || keys[0] != 'phone') {          
          if(keys[0] == 'Nationality') {
            if(value[key] !== 'ID') {
              type[keys[2]].PassportNumber = value[`IdNumber_${keys[1]}_${keys[2]}`]
            }
          }
          type[keys[2]][keys[0]] = value[key]
        }
        
      }
    }
    if(!value.salutationUser || !value.emailUser || !value.firstnameUser || !value.lastnameUser || !value.extUser || !value.phoneUser) {
      emptyField = true
    }
    let params = {
      token: id,
      user_account: {
        "salutation":value.salutationUser,
        "email":value.emailUser,
        "firstname":value.firstnameUser,
        "lastname":value.lastnameUser,
        "phone":value.extUser + value.phoneUser,
        "country_code":"ID"
      },
      Passengers: [...temPass.Adt, ...temPass.Chd, ...temPass.Inf]
    }

    for(let k = 0; k < params.Passengers.length; k++) {
      params.Passengers[k].Index = k+1
      params.Passengers[k].BirthDate = moment(params.Passengers[k].BirthDate).format("YYYY-MM-DD")
    }

    let cekAdultDOB = adultValDOB.includes("The date entered is not older than 12 years old")
    let cekChildDOB = childValDOB.includes("The date entered is not between 2 and 12 years old")
    let cekInfantDOB = infantValDOB.includes("The date entered is older than 2 years old")

    if( cekAdultDOB || cekChildDOB || cekInfantDOB ) {
      this.props.dispatch(setErrorMessage('Isikan tanggal kelahiran yg sesuai'))
    } else {
      this.setState({isModal: true, params: params, isLoading: false, emptyField})
    }
    
  }

  checkout = (params) => {   
     
    if(this.state.emptyField === false) {
      this.setState({isModal: false, isLoading: true})
      this.props.dispatch(checkoutDetails(params)).then(result => {
        if(get(result, 'meta.code') === 200) {
          const socket = openSocket(SOCKET_URL)
          socket.on(get(result, 'data.PnrId'), (message) => {
            if(message.Progress >= 100) {
              if(message.Text == 'Reserved' || message.data.Status == 'Reserved') {
                let newTotal = 0
                for(let i=0; i < message.data.Payments.length; i++) {
                  if(message.data.Payments[i].Code == "TOTAL") {
                    newTotal = message.data.Payments[i].Amount
                    if(this.state.totalFare !== message.data.Payments[i].Amount) {
                      this.setState({TotalChanging: true, newTotal: message.data.Payments[i].Amount, isLoading: false, paymentUrl: result.data.url})
                    } else {
                      this.setState({isLoading: false, isModal: false, paymentUrl: result.data.url})
                      logEvent('Checkout Payment', 'Checkout Payment');
                      this.props.dispatch(deleteCart({token: this.state.id, delete: true}))
                      .then(res => {
                        window.location = result.data.url
                      })
                      
                      
                      // this.props.dispatch(modalToggle(true, 'checkout-payment'));
                    }
                  }
                  
                }
              } else {
                this.setState({
                  flightUnavailable: true,
                  isLoading: false
                })
              }

            }
          })
        }
      })
    } else {
      this.setState({formVal: true, isModal: false})
    }
    
  }

  onChangeDOB(type, id, value) {
    let tempPass = this.state.passengers
    tempPass[type][id].BirthDate = value
    
    let age = moment().diff(moment(value, "YYYY-MM-DD"), 'years')
    let adultValDOB = this.state.adultValDOB
    let childValDOB = this.state.childValDOB
    let infantValDOB = this.state.infantValDOB
    if(type == 'Adt' && age <= 12) {
      adultValDOB[id] = 'The date entered is not older than 12 years old'
    } else if( type == 'Chd' && (age <= 2 || age > 12)) {
      childValDOB[id] = 'The date entered is not between 2 and 12 years old'
    } else if( type == 'Inf' && age > 2) {
      infantValDOB[id] = 'The date entered is older than 2 years old'
    } else {
      adultValDOB[id] = ''
      childValDOB[id] = ''
      infantValDOB[id] = ''
    }
    this.setState({
      passengers: tempPass,
      adultValDOB,
      childValDOB,
      infantValDOB
    })
  }

  onChangePE(type, id, value) {
    let tempPass = this.state.passengers
    tempPass[type][id].PassportExpire = value
    this.setState({
      passengers: tempPass
    })
  }

  deleteCart = () => {
    this.setState({isLoading: true})
    this.props.dispatch(deleteCart({token: this.state.id, delete: true})).then(res => {
      this.setState({isLoading: false})
      this.props.router.push({
        pathname: '/flight',
      });
    })
  }

  render() {
    const { country, handleSubmit } = this.props;
    const{ Adult, Child, Infant, passengers, isModal, TotalChanging, adultValDOB, childValDOB, infantValDOB, formVal} =this.state;
    return (
      <Fragment>
        <Head 
          title={"Manifest"} 
          description={"Manifest Flight pigijo."} 
          url={process.env.SITE_ROOT + '/manifest'} 
        />
        <PageHeader
          background={"/static/images/img05.jpg"}
          title="Your Booking"
          caption="Fill in your details and review your booking" 
        />
          <Grid>
            <Form onSubmit={handleSubmit(value => { this.onCheckout(value)
             })}>
              <Row>
                <Col xs={12} md={8}>
                  <section className="manifest-card">
                    <section className="card">
                      <h4 className="text-title bold"> Contact Detail</h4>
                      <div className="manifest-form">
                        <Row>
                          <Col md={12}>
                            <div className="row">
                              <div className='col-lg-2 col-md-2 col-sm-2'>
                                <ReduxField
                                  className="form-control input-lg"
                                  title="Title"
                                  component={renderSelectSalutation}
                                  name="salutationUser"
                                  placeholder="Title"
                                  
                                />
                              </div>
                              <div className='col-lg-5 col-md-5 col-sm-5'>
                                <ReduxField
                                className="form-control"
                                component={renderField}
                                name="firstnameUser"
                                type="text"
                                title="FirstName"
                                placeholder="First Name"/>
                              </div>
                              <div className='col-lg-5 col-md-5 col-sm-5'>
                                <ReduxField
                                className="form-control"
                                component={renderField}
                                name="lastnameUser"
                                type="text"
                                title="LastName"
                                placeholder="Last Name"/>
                              </div>

                            </div>
                          </Col>
                          <Col lg={6} md={6} sm={6}>
                            <ReduxField
                              className="form-control"
                              component={renderField}
                              name="emailUser"
                              type="email"
                              title="Email Address"
                              placeholder="Email"/>
                          </Col>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-label'>Phone Number</span>
                            <div className='row sm-gutter form-groups'>
                              <div className='col-lg-3 col-md-3 col-sm-4 col-xs-4'>
                                <ReduxField
                                className="form-control"
                                component={renderSelectExt}
                                option={country}
                                type="text"
                                name="extUser"
                                placeholder="ex. +62"/>
                              </div>
                              <div className='col-lg-9 col-md-9 col-sm-8 col-xs-8'>
                                <ReduxField
                                  className="form-control"
                                  component={renderField}
                                  name="phoneUser"
                                  type="text"
                                  placeholder="ex. 8123xxx"/>
                              </div>
                            </div>
                          </div>
                        </Row>
                      </div>
                    </section>

                    <section className="card">
                      <h4 className="text-title bold"> Traveler Details</h4>
                      <section className="traveler-detail-box">
                          {
                            !passengers.Adt ? null :( passengers.Adt.map((adult, id) => (
                              <Fragment key={id}>
                                <section className="main-box" style={{margin: "10px 0 20px", display: 'flex'}}>
                                  <div className="container">
                                    <ul id="header-tabs">
                                      <h4 className="text-title" style={{margin: "12px 0 !important"}}>
                                        Adult {id + 1}
                                      </h4>
                                    </ul>
                                  </div>
                                </section>
                                  <div className="manifest-form">
                                    <Row>
                                      <Col md={12}>
                                        <div className="row">
                                          <div className='col-lg-2 col-md-2 col-sm-2'>
                                            <ReduxField
                                              className="form-control input-lg"
                                              title="Title"
                                              component={renderSelectSalutation}
                                              name={`Title_Adt_${id}`}
                                            />
                                          </div>
                                          <div className='col-lg-5 col-md-5 col-sm-5'>
                                            <ReduxField
                                            className="form-control"
                                            component={renderField}
                                            name={`FirstName_Adt_${id}`}
                                            type="text"
                                            title="FirstName"
                                            placeholder="First Name"/>
                                          </div>
                                          <div className='col-lg-5 col-md-5 col-sm-5'>
                                            <ReduxField
                                            className="form-control"
                                            component={renderField}
                                            name={`LastName_Adt_${id}`}
                                            type="text"
                                            title="LastName"
                                            placeholder="Last Name"/>
                                          </div>
                                        </div>
                                      </Col>
                                      <Col lg={6} md={6} sm={6}>
                                        <ReduxField
                                          className="form-control"
                                          component={renderField}
                                          name={`Email_Adt_${id}`}
                                          type="email"
                                          title="Email Address"
                                          placeholder="Email"/>
                                      </Col>
                                      <div className='col-lg-6 col-md-6 col-sm-6'>
                                        <span className='text-label'>Phone Number</span>
                                        <div className='row sm-gutter form-groups'>
                                          <div className='col-lg-3 col-md-3 col-sm-4 col-xs-4'>
                                            <ReduxField
                                              className="form-control"
                                              component={renderSelectExt}
                                              option={country}
                                              type="text"
                                              placeholder="ex. +62"
                                              name={`ext_Adt_${id}`}/>
                                          </div>
                                          <div className='col-lg-9 col-md-9 col-sm-8 col-xs-8'>
                                            <ReduxField
                                              className="form-control"
                                              component={renderField}
                                              name={`phone_Adt_${id}`}
                                              type="text"
                                              placeholder="ex. 8123xxx"/>
                                          </div>
                                        </div>
                                      </div>
                                      <Col lg={6} md={6} sm={6}>
                                        <ReduxField
                                          className="form-control"
                                          component={renderField}
                                          name={`IdNumber_Adt_${id}`}
                                          title="Nomor Identitas"
                                          placeholder="Nomor Identitas"/>
                                      </Col>
                                      <Col lg={6} md={6} sm={6}>
                                        <ReduxField
                                            className="form-control"
                                            component={renderSelectNational}
                                            option={country}
                                            name={`Nationality_Adt_${id}`}
                                            title="Nationality"/>
                                      </Col>
                                      {
                                        adult.PassportNumber ? (
                                          <Col lg={6} md={6} sm={6}>
                                            <ReduxField
                                              className="form-control"
                                              component={renderField}
                                              name={`PassportNumber_Adt_${id}`}
                                              title="Nomor Passport"
                                              placeholder="Nomor Passport"/>
                                          </Col>
                                        ) : null
                                      }
                                      {
                                        adult.PassportExpire ? (
                                          <Col lg={6} md={6} sm={6}>
                                            <div>
                                              <span className="text-label">Masa Berlaku Passport</span>
                                            </div>
                                            <input 
                                              type="date" 
                                              name="dob"
                                              value={adult.PassportExpire}
                                              onChange={e => this.onChangePE('Adt', id,e.target.value)}
                                              />
                                          </Col>
                                        ) : null
                                      }
                                      {
                                        adult.PassportOrigin ? (
                                          <Col lg={6} md={6} sm={6}>
                                            <ReduxField
                                                className="form-control"
                                                component={renderSelectNational}
                                                option={country}
                                                name={`PassportOrigin_Adt_${id}`}
                                                title="Negara Penerbit Passport"/>
                                          </Col>
                                        ) : null
                                      }
                                      <Col lg={6} md={6} sm={6}>
                                        <div>
                                          <span className="text-label">Date Of Birth</span>
                                        </div>
                                        <input 
                                          type="date" 
                                          name="dob"
                                          value={adult.BirthDate}
                                          onChange={e => this.onChangeDOB('Adt', id,e.target.value)}
                                          />
                                          <span style={{color: 'red'}}><small>{adultValDOB[id]}</small></span>
                                      </Col>
                                      
                                    </Row>
                                  </div>
                              </Fragment>
                            )))
                          }
                          
                      </section>
                      
                      <section className="traveler-detail-box">
                        { !passengers.Chd ? null : passengers.Chd.map((child, id) => (
                          <Fragment key={id}>
                            <section className="main-box" style={{margin: "10px 0 20px", display: 'flex'}}>
                              <div className="container">
                                <ul id="header-tabs">
                                  <h4 className="text-title" style={{margin: "12px 0 !important"}}>
                                    Child {id+1}
                                  </h4>
                                </ul>
                              </div>
                            </section>
                            <div className="manifest-form">
                              <Row>
                                <Col md={12}>
                                  <div className="row">
                                    <div className='col-lg-2 col-md-2 col-sm-2'>
                                      <ReduxField
                                        className="form-control input-lg"
                                        title="Title"
                                        component={renderSelectSalutation}
                                        name={`Title_Chd_${id}`}
                                      />
                                    </div>
                                    <div className='col-lg-5 col-md-5 col-sm-5'>
                                      <ReduxField
                                      className="form-control"
                                      component={renderField}
                                      name={`FirstName_Chd_${id}`}
                                      type="text"
                                      title="FirstName"
                                      placeholder="First Name"/>
                                    </div>
                                    <div className='col-lg-5 col-md-5 col-sm-5'>
                                      <ReduxField
                                      className="form-control"
                                      component={renderField}
                                      name={`LastName_Chd_${id}`}
                                      type="text"
                                      title="LastName"
                                      placeholder="Last Name"/>
                                    </div>
                                  </div>
                                </Col>
                                <Col lg={6} md={6} sm={6}>
                                  <ReduxField
                                    className="form-control"
                                    component={renderField}
                                    name={`Email_Chd_${id}`}
                                    type="email"
                                    title="Email Address"
                                    placeholder="Email"/>
                                </Col>
                                <div className='col-lg-6 col-md-6 col-sm-6'>
                                  <span className='text-label'>Phone Number</span>
                                  <div className='row sm-gutter form-groups'>
                                    <div className='col-lg-3 col-md-3 col-sm-4 col-xs-4'>
                                      <ReduxField
                                        className="form-control"
                                        component={renderSelectExt}
                                        option={country}
                                        type="text"
                                        placeholder="ex. +62"
                                        name={`ext_Chd_${id}`}/>
                                    </div>
                                    <div className='col-lg-9 col-md-9 col-sm-8 col-xs-8'>
                                      <ReduxField
                                        className="form-control"
                                        component={renderField}
                                        name={`phone_Chd_${id}`}
                                        type="text"
                                        placeholder="ex. 8123xxx"/>
                                    </div>
                                  </div>
                                </div>
                                <Col lg={6} md={6} sm={6}>
                                  <ReduxField
                                    className="form-control"
                                    component={renderField}
                                    name={`IdNumber_Chd_${id}`}
                                    title="Nomor Identitas"
                                    placeholder="Nomor Identitas"/>
                                </Col>
                                <Col lg={6} md={6} sm={6}>
                                  <ReduxField
                                      className="form-control"
                                      component={renderSelectNational}
                                      option={country}
                                      name={`Nationality_Chd_${id}`}
                                      title="Nationality"/>
                                </Col>
                                {
                                  child.PassportNumber ? (
                                    <Col lg={6} md={6} sm={6}>
                                      <ReduxField
                                        className="form-control"
                                        component={renderField}
                                        name={`PassportNumber_Chd_${id}`}
                                        title="Nomor Passport"
                                        placeholder="Nomor Passport"/>
                                    </Col>
                                  ) : null
                                }
                                {
                                  child.PassportExpire ? (
                                    <Col lg={6} md={6} sm={6}>
                                      <div>
                                        <span className="text-label">Masa Berlaku Passport</span>
                                      </div>
                                      <input 
                                          type="date" 
                                          name="dob"
                                          value={child.PassportExpire}
                                          onChange={e => this.onChangePE('Chd', id,e.target.value)}
                                          />
                                    </Col>
                                  ) : null
                                }
                                {
                                  child.PassportOrigin ? (
                                    <Col lg={6} md={6} sm={6}>
                                      <ReduxField
                                          className="form-control"
                                          component={renderSelectNational}
                                          option={country}
                                          name={`PassportOrigin_Chd_${id}`}
                                          title="Negara Penerbit Passport"/>
                                    </Col>
                                  ) : null
                                }
                                <Col lg={6} md={6} sm={6}>
                                  <div>
                                    <span className="text-label">Date Of Birth</span>
                                  </div>
                                  <input 
                                    type="date" 
                                    name="dob"
                                    value={child.BirthDate}
                                    onChange={e => this.onChangeDOB('Chd', id,e.target.value)}
                                    />
                                    <span style={{color: 'red'}}><small>{childValDOB[id]}</small></span>
                                </Col>
                                
                              </Row>
                            </div>
                          </Fragment>
                        ))}
                      </section>
                      
                      <section className="traveler-detail-box">
                        {!passengers.Inf ? null : passengers.Inf.map((infant, id) => (
                          <Fragment>
                            <section className="main-box" style={{margin: "10px 0 20px", display: 'flex'}}>
                              <div className="container">
                                <ul id="header-tabs">
                                  <h4 className="text-title" style={{margin: "12px 0 !important"}}>
                                    Infant {id+1}
                                  </h4>
                                </ul>
                              </div>
                            </section>
                            <div className="manifest-form">
                              <Row>
                                <Col md={12}>
                                  <div className="row">
                                    <div className='col-lg-2 col-md-2 col-sm-2'>
                                      <ReduxField
                                        className="form-control input-lg"
                                        title="Title"
                                        component={renderSelectSalutation}
                                        name={`Title_Inf_${id}`}
                                      />
                                    </div>
                                    <div className='col-lg-5 col-md-5 col-sm-5'>
                                      <ReduxField
                                      className="form-control"
                                      component={renderField}
                                      name={`FirstName_Inf_${id}`}
                                      type="text"
                                      title="FirstName"
                                      placeholder="First Name"/>
                                    </div>
                                    <div className='col-lg-5 col-md-5 col-sm-5'>
                                      <ReduxField
                                      className="form-control"
                                      component={renderField}
                                      name={`LastName_Inf_${id}`}
                                      type="text"
                                      title="LastName"
                                      placeholder="Last Name"/>
                                    </div>
                                  </div>
                                </Col>
                                <Col lg={6} md={6} sm={6}>
                                  <ReduxField
                                    className="form-control"
                                    component={renderField}
                                    name={`Email_Inf_${id}`}
                                    type="email"
                                    title="Email Address"
                                    placeholder="Email"/>
                                </Col>
                                <div className='col-lg-6 col-md-6 col-sm-6'>
                                  <span className='text-label'>Phone Number</span>
                                  <div className='row sm-gutter form-groups'>
                                    <div className='col-lg-3 col-md-3 col-sm-4 col-xs-4'>
                                    <ReduxField
                                      className="form-control"
                                      component={renderSelectExt}
                                      option={country}
                                      type="text"
                                      placeholder="ex. +62"
                                      name={`ext_Inf_${id}`}/>
                                    </div>
                                    <div className='col-lg-9 col-md-9 col-sm-8 col-xs-8'>
                                      <ReduxField
                                        className="form-control"
                                        component={renderField}
                                        name={`phone_Inf_${id}`}
                                        type="text"
                                        placeholder="ex. 8123xxx"/>
                                    </div>
                                  </div>
                                </div>
                                <Col lg={6} md={6} sm={6}>
                                  <ReduxField
                                    className="form-control"
                                    component={renderField}
                                    name={`IdNumber_Inf_${id}`}
                                    title="Nomor Identitas"
                                    placeholder="Nomor Identitas"/>
                                </Col>
                                <Col lg={6} md={6} sm={6}>
                                  <ReduxField
                                      className="form-control"
                                      component={renderSelectNational}
                                      option={country}
                                      name={`Nationality_Inf_${id}`}
                                      title="Nationality"/>
                                </Col>
                                <Col lg={6} md={6} sm={6}>
                                  <ReduxField
                                      className="form-control"
                                      component={renderSelectAdult}
                                      option={passengers.Adt}
                                      name={`AdultAssoc_Inf_${id}`}
                                      title="Orang Dewasa Pendamping"/>
                                </Col>
                                {
                                  infant.PassportNumber ? (
                                    <Col lg={6} md={6} sm={6}>
                                      <ReduxField
                                        className="form-control"
                                        component={renderField}
                                        name={`PassportNumber_Inf_${id}`}
                                        title="Nomor Passport"
                                        placeholder="Nomor Passport"/>
                                    </Col>
                                  ) : null
                                }
                                {
                                  infant.PassportExpire ? (
                                    <Col lg={6} md={6} sm={6}>
                                      <div>
                                        <span className="text-label">Masa Berlaku Passport</span>
                                      </div>
                                        <input 
                                          type="date" 
                                          name="dob"
                                          value={infant.PassportExpire}
                                          onChange={e => this.onChangePE('Inf', id,e.target.value)}
                                          />
                                    </Col>
                                  ) : null
                                }
                                {
                                  infant.PassportOrigin ? (
                                    <Col lg={6} md={6} sm={6}>
                                      <ReduxField
                                          className="form-control"
                                          component={renderSelectNational}
                                          option={country}
                                          name={`PassportOrigin_Inf_${id}`}
                                          title="Negara Penerbit Passport"/>
                                    </Col>
                                  ) : null
                                }
                                <Col lg={6} md={6} sm={6}>
                                  <div>
                                    <span className="text-label">Date Of Birth</span>
                                  </div>
                                  <input 
                                    type="date" 
                                    name="dob"
                                    value={infant.BirthDate}
                                    onChange={e => this.onChangeDOB('Inf', id,e.target.value)}
                                    />
                                    <span style={{color: 'red'}}><small>{infantValDOB[id]}</small></span>
                                </Col>
                                
                              </Row>
                            </div>
                          </Fragment>
                        ))}
                      </section>
                      
                    </section>

                    {/* <section className="card">
                      <h3 className="text-title"> Flight Facilities </h3>
                      <section className="main-box" style={{margin: "10px 0 20px"}}>
                        <div className="container">
                          <ul id="header-tabs">
                            <h4 className="text-title" style={{margin: "12px 0 !important"}}>
                              Baggage
                            </h4>
                          </ul>
                        </div>
                      </section>
                      {Array.from(Array(Adult+Child+Infant),(item, idx) => (
                        <div style={{marginBottom: 30}}>
                          <h5 className='text-title'>Mr. Louis</h5>
                          <div className="manifest-form">
                            <ControlLabel className="label-form">CGK - BPN</ControlLabel>
                            <SearchAutoComplete
                              id="cabin"
                              key={"select-single" + {idx}}
                              index={0}
                              data={[
                                { name: '0 kg - Rp 0', type: '' }, 
                                { name: '5 kg - Rp 160.000', type: '' }, 
                                { name: '10 kg - Rp 310.000', type: '' },
                                { name: '15 kg - Rp 460.000', type: '' }, 
                                { name: '20 kg - Rp 610.000', type: '' }, 
                                { name: '25 kg - Rp 760.000', type: '' },
                                { name: '30 kg - Rp 910.000', type: '' }, 
                              ]}
                              labelKey="name"
                              onChange={result => this.setState({ baggage: get(result, '0.name') || "" })}
                              bsSize="large"
                            />

                            <ControlLabel className="label-form">BPN - CGK</ControlLabel>
                            <SearchAutoComplete
                              id="cabin"
                              key={"select-single" + {idx} +1}
                              index={0}
                              data={[
                                { name: '0 kg - Rp 0', type: '' }, 
                                { name: '5 kg - Rp 160.000', type: '' }, 
                                { name: '10 kg - Rp 310.000', type: '' },
                                { name: '15 kg - Rp 460.000', type: '' }, 
                                { name: '20 kg - Rp 610.000', type: '' }, 
                                { name: '25 kg - Rp 760.000', type: '' },
                                { name: '30 kg - Rp 910.000', type: '' }, 
                              ]}
                              labelKey="name"
                              onChange={result => this.setState({ baggage: get(result, '0.name') || "" })}
                              bsSize="large"
                            />
                          </div>
                        </div>
                      ))}
                    </section> */}
                  </section>
                </Col>
                <Col xs={12} md={4} style={isMobile ? {marginTop: '2em'} : {}} >
                  <section className="manifest-card">
                    {/* <section className="card">
                      <h4 className="text-title bold"> Additional Information</h4>
                      <div className="manifest-form">
                        {
                          this.state.flight.map((flight, idx) => (
                            <div className="flight-info">
                              <div className="list-horizontal_middle" style={{margin:"0 6px"}}>
                                <img src={flight.AirlineImageUrl ? flight.AirlineImageUrl : ''} width="30"/>
                              </div>
                              <div className="list-horizontal_middle ">{flight.Origin ? flight.Origin : ''} - {flight.Destination ? flight.Destination : ''}</div>
                              <div className="list-horizontal_middle fdb-dot">•</div>
                              <div className="list-horizontal_middle ">{moment(flight.DepartDate ? flight.DepartDate : '').format("ll")}</div>
                              <div className="list-horizontal_middle fdb-dot">•</div>
                              <div className="list-horizontal_middle">{flight.DepartTime ? flight.DepartTime : ''}</div>
                            </div>
                          ))
                        }
                      </div>
                    </section> */}

                    <section className="card">
                      <h4 className="text-title bold"> Additional Information</h4>
                        <div className="manifest-form">
                          {
                            this.state.flight.map((flight, idx) => (
                              <div className="flight-info">
                                <Row style={{display: 'flex', alignItems: 'center'}}>
                                  <Col xs={3}>
                                    <img src={flight.AirlineImageUrl ? flight.AirlineImageUrl : ''} width="90%"/>
                                  </Col>
                                  <Col xs={9}>
                                    <div className="list-horizontal_middle ">{flight.Origin ? flight.Origin : ''} - {flight.Destination ? flight.Destination : ''}</div>
                                    <div className="list-horizontal_middle fdb-dot">•</div>
                                    <div className="list-horizontal_middle ">{flight.Number}</div>
                                    <br/>
                                    <div className="list-horizontal_middle ">{flight.DepartDate}</div>
                                    <div className="list-horizontal_middle fdb-dot">•</div>
                                    <div className="list-horizontal_middle">{flight.DepartTime ? flight.DepartTime : ''}</div>
                                  </Col>
                                </Row>
                                {/* <div className="list-horizontal_middle" style={{margin:"0 6px"}}>

                                </div> */}
                                
                              </div>
                            ))
                          }
                        </div>
                      
                      <div className="manifest-form">
                        {/* <Row>
                          <Col xs={6}>
                            <div>Adult (x{Adult})</div>
                            {Child > 0 ? <div>Child (x{Child})</div> : null}
                            {Infant > 0 ? <div>Infant (x{Infant})</div> : null}
                          </Col>

                          <Col xs={6}>
                            <div style={{textAlign: "right"}}>Rp. {numberWithComma(this.state.adultFare)}</div>
                            {Child > 0 ? <div style={{textAlign: "right"}}>Rp. {numberWithComma(this.state.childFare)}</div> : null}
                            {Infant > 0 ? <div style={{textAlign: "right"}}>Rp. {numberWithComma(this.state.infantFare)}</div> : null}
                          </Col>
                        </Row> */}
                        
                        <div style={{borderTop:"solid 1px #D8DDE6", marginTop: 20}}>
                          <h4 className="text-title bold"> Total Price</h4>
                          <Row>
                          <Col xs={5}>
                            <h5 className="text-title bold" style={{margin: "12px 0"}}>Price you pay</h5>
                          </Col>

                          <Col xs={7}>
                            <h4 style={{ textAlign: "right", color:"#e17306"}}>Rp. {numberWithComma(this.state.totalFare)}</h4>
                          </Col>
                          </Row>
                        </div>
                      </div>
                    </section>
                  
                      <button className="btn btn-primary btn-lg btn-flight-book" type="submit" style={{width: "100%", fontSize: ".9em"}}>Continue to Payment</button>
                  </section>
                </Col>
              </Row>
            </Form>
          </Grid>
    <Modal show={isModal} onHide={() => this.handleClose()} backdrop dialogClassName="modal-dialog-centered modal-md">
      <a className="close" onClick={(e) => this.setState({isModal: !this.state.isModal})}><i className="ti-close"/></a>
      <div className="modal-header">
        <span className="text-title m0 h4">Continue to payment ?</span>
      </div>
      <div className="modal-panel">
        <div className="col-sm-12 pb1 pt1">
          <p>Make sure your data is correct. You can’t change your booking details once you proceed to payment.</p>
        </div>
        <div className="col-sm-6 pb1 pt1">
          <button className="btn btn-check btn-sm btn-block mt1" onClick={(e) => this.setState({isModal: !this.state.isModal})}style={{color: "#000",backgroundColor: "white",border: "1px solid #e17306"}} >No, Check Again</button>
        </div>
        <div className="col-sm-6 pb1 pt1">
          <button className="btn btn-primary btn-sm btn-block mt1" onClick={(e) => this.checkout(this.state.params)}>Yes, Continue</button>
        </div>
      </div>
    </Modal>
    <Modal show={TotalChanging} onHide={() => this.handleClose()} backdrop dialogClassName="modal-dialog-centered modal-md">
      <a className="close" onClick={(e) => this.setState({TotalChanging: !this.state.TotalChanging})}><i className="ti-close"/></a>
      <div className="modal-header">
        <span className="text-title m0 h4">Fare Changed!</span>
        <div className="modal-panel">
          <div className="col-sm-12 pb1 pt1">
            <p>Fare has changed. From Rp {numberWithComma(this.state.totalFare)} to Rp {numberWithComma(this.state.newTotal)}. Do you want to proceed ?</p>
          </div>
          <div className="col-sm-6 pb1 pt1">
            <button className="btn btn-check btn-sm btn-block mt1" onClick={(e) => this.setState({TotalChanging: !this.state.TotalChanging}, () => Router.push({pathname: '/flight'}))}style={{color: "#000",backgroundColor: "white",border: "1px solid #e17306"}} >Find another flight</button>
          </div>
          <div className="col-sm-6 pb1 pt1">
            <button className="btn btn-primary btn-sm btn-block mt1" onClick={(e) => this.setState({isLoading: false}, () => {
              this.props.dispatch(deleteCart({token: this.state.id, delete: true}))
              .then(res => {
                window.location = this.state.paymentUrl
              })
              
            }) }>Yes, Continue</button>
          </div>
        </div>
      </div>
    </Modal>
    <Modal show={this.state.flightUnavailable} onHide={() => this.setState({flightUnavailable: false}, () => this.deleteCart({delete: true}))} backdrop dialogClassName="modal-dialog-centered modal-sm">
      <a className="close" onClick={(e) => this.setState({flightUnavailable: !this.state.flightUnavailable}, () => this.deleteCart({delete: true}))}><i className="ti-close"/></a>
      <div className="modal-header">
        <span className="text-title m0 h4">Flight Unavailable!</span>
        <div className="modal-panel">
          <div className="col-sm-12 pb1 pt1">
            <p>Sorry your chosen flight already sold out. Let's find another flight!</p>
          </div>
          <div className="col-sm-12 pb1 pt1">
            <button className="btn btn-primary btn-sm btn-block mt1" onClick={(e) => this.setState({flightUnavailable: !this.state.flightUnavailable}, () => this.deleteCart({delete: true}))}>Yes, Continue</button>
          </div>
        </div>
      </div>
    </Modal>
    <Modal show={formVal} onHide={() => this.setState({formVal: false})} backdrop dialogClassName="modal-dialog-centered modal-sm">
      <a className="close" onClick={(e) => this.setState({formVal: !formVal})}><i className="ti-close"/></a>
      <div className="modal-header">
        <span className="text-title m0 h4">Please fill your data!</span>
        <div className="modal-panel">
          <div className="col-sm-12 pb1 pt1">
            <p>All data is required for booking process!</p>
          </div>
          <div className="col-sm-12 pb1 pt1">
            <button className="btn btn-primary btn-sm btn-block mt1" onClick={(e) => this.setState({formVal: !this.state.formVal})}>Yes, Of Course</button>
          </div>
        </div>
      </div>
    </Modal>
    <ModalContainer/>
    {this.state.isLoading ? (<Loading/>) : null}
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  profile: state.auth.profile,
  type: state.modal.type,
  isOpen: state.modal.isOpen,
  message: state.modal.message,
  country: state.country.data,
  departure_flight: state.flights.departure_flight,
  return_flight: state.flights.return_flight
})

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

Manifest = reduxForm({
  form: "contact_details",
  initialValues: {
    salutationUser: 'Mr',
    extUser: "+62"
  },
  validate
})(Manifest);

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withAuth(["PUBLIC"])
)(Manifest);