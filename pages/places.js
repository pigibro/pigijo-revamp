import { compose } from 'redux';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import Error from './_error';
import { map, get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import numeral from '../utils/numeral';
import { Link } from '../routes';
import PageHeader from '../components/page-header-icons';
import Loading from '../components/home/loading-flashsale';
import InfiniteScroll from 'react-infinite-scroller';
import React from 'react';
import { Glyphicon } from 'react-bootstrap';
import withAuth from '../_hoc/withAuth';
import { getPlaces, getLocations, getDetailCategory, getTagList } from '../stores/actions';
import { slugify, urlImage, convertToRp } from '../utils/helpers';
import moment from 'moment';
import IconLink from "../components/shared/iconLink";
import SearchAutoComplete from "../components/shared/searchautocomplete";
import InputRange from "react-input-range";
import 'react-dates/initialize';
import { DayPickerRangeController, DateRangePicker } from 'react-dates';
import { MobileView } from "react-device-detect";
import * as gtag from '../utils/gtag';
import Router from 'next/router';

// Router.events.on('routeChangeComplete', url => gtag.pageview(url))

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      // backgroundRepeat: 'no-repeat'
      backgroundPosition: ' center center',
  }
}

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          data: [],
          total: 0,
          page: 1,
          isLoading: false,
          product_type:[],
          more: true,
          filter: '',
          sort_by: 'created_at|DESC',
          location: [],
          duration: [],
          startDate: null,
          endDate: null,
          city: null,
          province: null,
          area: null,
          category_selected: '',
          schedule_type: [],
          minMax:{min: 0 , max: 50000000},
          quantity: 2,
          focusedInput: '',
          btnMobileActive: '',
          category_list: [{
            name: 'All',
            type: ''
          }, {
            name: 'Events',
            type: 'events'
          }, {
            name: 'Culinary',
            type: 'culinary'
          }, {
            name: 'Places',
            type: 'places'
          }],
          tag: null,
          tags: []
        }
    }
    static async getInitialProps({ query, store}) {
        const { slug } = query;
        let paramsSlug = "places";
        let errorCode = false;
        const resCategory = await store.dispatch(getDetailCategory(paramsSlug));
        errorCode = get(resCategory,'meta.code') !== 200 ? 404 : false;
        const category = get(resCategory,'data');
        return { errorCode, slug, category, query }
    }

    componentDidMount() {
      document.addEventListener('click', this.handleClickOutside, true);
      
      const { dispatch } = this.props;
      dispatch(getLocations()).then(result => {
        if (get(result, 'meta.code') === 200){
          this.setState({
            location: get(result, 'data')
          });
        }
      }); 
      dispatch(getTagList()).then(res => {
        if(get(res, 'meta.code') === 200) {
          let data = get(res, 'data') || []
          for(let i=0; i < data.length; i++){
            data[i].type = 'Tag'
          }
          this.setState({
            tags: data
          })
        }
        
      })
    }
    
    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
    }

    handleTypeFilter = (e) => {
      const { product_type } = this.state;
      let newState = product_type;
      if(product_type.indexOf(e.target.value) < 0){
        newState.push(e.target.value);
      }else{
        newState.splice(product_type.indexOf(e.target.value), 1);
      }
      this.setState({
        product_type: newState,
      });
    }
    setParams = (isRerender) => {
      const {slug, query} = this.props;
      const { page,startDate,endDate, quantity, city, area, province,minMax, schedule_type, category_selected, tag } = this.state;
      let { sort_by } = this.state;
      if(sort_by === 'created_at|DESC' && slug === 'recommended'){
        sort_by = 'recommended|DESC';
      }
      let req = {
        'per_page': 16,
        'page':isRerender ? 1 : page,
        'sort_by': sort_by,
        'category': category_selected
      };
      if(isRerender == false) {
        this.setState({
          tag: query.tags ? +query.tags[0] : tag
        })
        if (query.tags) {
          req = { ...req, ...{ tags: +query.tags[0] }}
        }
      }
      if(city === null || query.type) {
        req[query.type] = query.slug
      }
      if(!isEmpty(startDate) && !isEmpty(endDate)){
        req = {...req, ...{start_date:moment(startDate).format('YYYY-MM-DD'), end_date:moment(endDate).format('YYYY-MM-DD')}}
      }
      if(city !== null){
        req = {...req, ...{city}}
      }
      if(province !== null){
        req = {...req, ...{province}}
      }
      if(area !== null){
        req = {...req, ...{area}}
      }
      if(schedule_type.length > 0){
        req = {...req, ...{schedule_type}}
      }

      let queries = {}
      if (tag !== null) {
        req = { ...req, ...{ tags: tag }}
        let det_Tag = this.state.tags.find(tags => tags.id === tag)
        queries.tags = `${det_Tag.id}-${det_Tag.name}`
      }


      isRerender ? Router.push({pathname: `/places`, query: queries}) : null

      return req;
    }

    getActivity = async(isRerender = false) => {
      if (this.state.isLoading) {
        return 'isLoading...';
      }
      const params = this.setParams(isRerender);
      const { dispatch } = this.props;
      const page = isRerender ? 0 : this.state.page;
      let dataTmp = !isRerender ? this.state.data : [];
      if(page > 0){
        this.setState({ isLoading: true, btnMobileActive: '' })
        await dispatch(getPlaces(params)).then(result => {
          if (get(result, 'meta.code') === 200){
            this.setState({data: dataTmp.concat(get(result,'data')),page: page+1});
            this.setState({total: get(result,'pagination.total')});
            if(page === get(result,'pagination.last_page')){
              this.setState({more: false});
            }
          }
          this.setState({ isLoading: false })
        });
      }
    }

    loadMore = () => {
      this.getActivity(false);
    }
    
    changeShow = (filter) => {
      this.setState({filter});
    }

    handleClickOutside = event => {
      const location = ReactDOM.findDOMNode(this.refs.location);
      if (!location || !location.contains(event.target)) {
        const dates = ReactDOM.findDOMNode(this.refs.dates);
        if (!dates || !dates.contains(event.target)) {
          const sort = ReactDOM.findDOMNode(this.refs.sort);
          if (!sort || !sort.contains(event.target)) {
            const tag = ReactDOM.findDOMNode(this.refs.tag);
            if (!tag || !tag.contains(event.target)) {
              this.setState({ filter: '' });
            }
          }
        }
      }
    }

    handleLocation = (value) => {
      if(value.length > 0){
        if(value[0].type === 'Area'){
          this.setState({area: value[0].slug, city: null, province: null, data: [], page: 1, more: true}, () => {
          });
        }else if (value[0].type === 'Kabupaten' || value[0].type === 'Kota'){
          this.setState({area: null, city: value[0].slug, province: null, data: [], page: 1, more: true}, () => {
          });
        }else{
          this.setState({area: null, city: null, province: value[0].slug, data: [], page: 1, more: true}, () => {
          });
        }

        this.getActivity(true)
      }
    }

    handlePriceRange = (value) => {
      this.setState({ minPrice: value.min, maxPrice: value.max });
    }

    handleCategory = (value) => {
      this.setState({category_selected: value, data: [], page:1, more: true })
      this.getActivity(true)
    }

    handleDuration = (e) => {
      const { schedule_type } = this.state;
      let newState = schedule_type;
      if(schedule_type.indexOf(e.target.value) < 0){
        newState.push(e.target.value);
      }else{
        newState.splice(schedule_type.indexOf(e.target.value), 1);
      }
      this.setState({
        schedule_type: newState,
        data: [], page:1, more: true 
      }, () => {
        this.getActivity(true);
      });
    }
    
    
    render(){
      const {slug, errorCode, category, query} = this.props;
      const { product_type,
         page,btnMobileActive,
         category_list,
         schedule_type, 
         total, 
         quantity, 
         minMax, 
         data, 
         pagination, 
         more, 
         filter, 
         location, 
         startDate, 
         endDate, 
         focusedInput,
         category_selected,
         tags,
         tag } = this.state;
      const loopTo = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
      if(errorCode > 200){
        return (<Error statusCode={errorCode}/>);
      }
      return (
        <>
          <Head title={category.meta_title} description={category.meta_description} url={process.env.SITE_ROOT+'/c/'+slug}/>
          <PageHeader section='main' background={"/static/images/img05.jpg"} title={category.name} caption={category.short_description} type={query.type ? query.type : ''} slug={query.slug ? query.slug : ''} category="places"/>
          <section className="main-box sticky">
            <div className="container">
              <ul id="header-tabs">
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'location'})}>Location</button>
                  <div ref='location' className={`p__filter ${filter === 'location' ? 'show' : 'hide'}`} >
                    <SearchAutoComplete
                      id="destination"
                      key={"select-single"}
                      index={0}
                      data={location || []}
                      placeholder="Select City"
                      labelKey="name"
                      onChange={this.handleLocation}
                      bsSize="large"
                    />
                  </div>
                </li>
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'tag'})}>Tags</button>
                  <div ref='tag' className={`p__filter ${filter === 'tag' ? 'show' : 'hide'}`} >
                    <SearchAutoComplete
                      id="tag"
                      key={"select-single-1"}
                      index={1}
                      data={tags || []}
                      placeholder="Select Tag"
                      labelKey="name"
                      onChange={e => this.setState({tag: e[0].id}, () => this.getActivity(true))}
                      bsSize="large"
                    />
                  </div>
                </li>
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'sort'})}>Sort By</button>
                  <div ref='sort' className={`p__filter ${filter === 'sort' ? 'show' : 'hide'}`}>
                    <div className="sort-desktop" onClick={() => {this.setState({sort_by:'created_at|DESC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                      Newly listed
                    </div>
                    <div className="sort-desktop" onClick={() => {this.setState({sort_by:'recommended|DESC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                      Recommended
                    </div>
                  </div>
                </li>
                <li className="tabs-item" style={{float: 'right'}}>
                  {
                    category_list.length > 0 ? (
                      category_list.map((item, index) => (
                        <button 
                        onClick={() => this.setState({category_selected: item.type, data: [], page:1, more: true }, () => this.getActivity(true))}
                        className="btn btn-sm btn-o btn-light" 
                        style={category_selected === item.type ? {backgroundColor: '#e17306', color: '#fff'} : {}}>
                        {item.name}</button>
                      ))
                    ) : null
                  }
                  
                  {/* <button className="btn btn-sm btn-o btn-light">Events</button>
                  <button className="btn btn-sm btn-o btn-light">Culinary</button>
                  <button className="btn btn-sm btn-o btn-light">Places</button> */}
                </li>
              </ul>
            </div>
          </section>
          {
            btnMobileActive !== '' &&
            <MobileView>
            <div className="p__fitler--mobile-content">
              <a className="btn-close" onClick={e => {e.preventDefault; this.setState({btnMobileActive:''})}}><i className="ti-close"/></a>
              {
                btnMobileActive === 'filter' &&
                <>
                  <div className="form-group">
                    <span className="text-label">Location</span>
                    <SearchAutoComplete
                      id="destination-mobile"
                      key={"select-single"}
                      index={0}
                      data={location || []}
                      placeholder="Select City"
                      labelKey="name"
                      onChange={this.handleLocation}
                      bsSize="large"
                    />
                  </div>
                  <div className="form-group">
                    <span className="text-label">Category</span>
                    <SearchAutoComplete
                      id="category"
                      key={"select-single"}
                      index={0}
                      data={category_list || []}
                      placeholder="Select Category"
                      labelKey="name"
                      onChange={this.handleCategory}
                      bsSize="large"
                    />
                  </div>
                </>
              }
              {
                btnMobileActive === 'sort' &&
                <>
                  <div className="sort-mobile mt2" onClick={() => {this.setState({sort_by:'created_at|DESC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                    Newly listed
                  </div>
                  <div className="sort-mobile" onClick={() => {this.setState({sort_by:'recommended|ASC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                    Recommended
                  </div>
                </>
              }
            </div>
            </MobileView>
          }
          {
            process.browser &&
            <MobileView>
            <section className="p__filter--mobile">
              <div className="btn-group">
                <button className="btn btn-sm btn-primary" onClick={() => this.setState({btnMobileActive: 'filter'})}>Filter</button>
                <button className="btn btn-sm btn-primary btn-o bg-white" onClick={() => this.setState({btnMobileActive: 'sort'})}>Sort</button>
              </div>
            </section>  
            </MobileView>
          }

          {/* <section className="body-content mt2 ">
          <div className="container">
            <IconLink type={query.type ? query.type : ''} slug={query.slug ? query.slug : ''} category="places" />
          </div> 
        </section> */}

          <section className="body-content mt2">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <h4 className="text-title">Found {total} in {category.name}</h4>
                </div>
                <div className="product-list">
                <InfiniteScroll pageStart={page} initialLoad loader={<div style={{padding:"0 .99em"}} key={'loading'}><Loading /></div>} useWindow={true} loadMore={this.loadMore} hasMore={more} threshold={1000}>
                {
                  map(data, (item, index) => (
                    <div className="col-md-3 col-sm-6 col-xs-6 p-item" key={`p-${index}`}>
                      <Link route="place" params={{slug: item.slug}}>
                      <a>
                      <div className="plan-item sr-btm">
                        <div className="box-img plan-img">
                          <div className="thumb" style={_imgStyle(urlImage(item.image_path+'/'+item.image_filename))}/>
                          <div className="box-img-top">
                            {!isEmpty(item.city) && <span><i className="ti-pin"/> {item.city.name}</span> }
                          </div>
                          <div className="box-img-bottom">
                          <p className="p_discount">FREE</p>
                          </div>
                        </div>
                        <div className="plan-info">
                          <ul>
                            <li className="title"><h3 title={item.name}>{item.name}</h3></li>
                            <li className="min-person">
                              <p className="text-label">
                              {
                                map(item.tags, (tag, idx) => {
                                  if(idx < 2) {
                                    return ( 
                                      idx === 0 ? `${tag}` : `, ${tag}`
                                    )
                                  }})
                              }
                              </p> 
                            </li>
                            <li className="price">
                              {/* {item.discount > 0 && <p className="price-before"><del>Rp {numeral(parseInt(item.price_before,10)).format('0,0')}</del></p>} */}
                              <p>
                                <span  className="price-latest">
                                  <b><span className={`badge ${item.status === "closed" ? "badge-error" : "badge-success"} capitalized`}>{item.status}</span></b>
                                </span>
                                <span className='notice text-info' title="Arange Visit Time">
                                {item.arange_visit_time}
                                </span>
                              </p>
                            </li>
                          </ul>
                        </div>
                      </div>{/*plan-item-end*/}
                      </a>
                      </Link>
                    </div>
                  ))
                }
                </InfiniteScroll>
                </div>
              </div>
            </div>
          </section>
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(Category);