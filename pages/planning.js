import { compose } from 'redux'
import { connect } from 'react-redux'
import Error from './_error'
import { get, isEmpty, takeRight } from 'lodash'
import { Tabs, Tab } from 'react-bootstrap';
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import Head from '../components/head'
import PageHeader from '../components/page-header';
import SideBar from '../components/shared/sidebar';
import React from 'react'
import withAuth from '../_hoc/withAuth'

const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: 'cover',
    backgroundPosition: ' center center',
  }
}
class PlanningItinerary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          activeKey : 0,
          startDate: null,
          endDate: null,
          focusedInput: null,
        };
    }

    static async getInitialProps({ query, store}) {
        
    }

    render(){
      const {startDate,endDate, focusedInput} = this.state;
      const {activeKey} = this.props;
      return (
        <>
          <Head title={"Eu nostrud amet esse enim est esse commodo reprehenderit."} description={"Culpa tempor eu elit duis eu occaecat culpa culpa. Commodo anim commodo occaecat do consequat consequat Lorem laborum qui duis reprehenderit nisi Lorem ullamco. Dolore cupidatat proident esse irure cillum deserunt. Sit et tempor sint duis occaecat ea culpa."} url={process.env.SITE_ROOT+'/p/1180-showcase-indonesia-full-pass-early-bird'}/>
          <PageHeader background={"/static/images/img05.jpg"} title="Confirm your trip’s route and date." caption="Check again your trip’s route and date. You can customise your trip duration in each destination as you wish by changing the arrival date on each destination."/>
          <section className='content-wrap'>
            <div className='container'>
              <div className='row timeline-steps'>
                <div className='col-xs-4 step-item active'>
                  <span className='step bg-ongoing'>1</span>
                </div>
                <div className='col-xs-4 step-item'>
                  <span className='step bg-todo'>2</span>
                </div>
                <div className='col-xs-4 step-item'>
                  <span className='step bg-todo'>3</span>
                </div>
              </div>
              <div className="row"> 
                <div className="col-md-8">
                  <Tabs activeKey={activeKey} onSelect={(key) => this.setState({ activeKey: key})} id='tab-event-route' className="tab-route-trip">
                    <Tab eventKey={0} title={"Jakarta"}>
                      <div className="row">
                        <div className="col-md-4">
                          <div className='box-img'>
                            <div className='thumb' style={_imgStyle("https://s3-ap-southeast-1.amazonaws.com/pigijo/53aad639aca4b5c010927cf610c3ff9c/2018/08/30/1535631387DKI-Jakarta.jpg")}/>
                          </div>
                        </div>
                        <div className="col-md-8">
                          <h5 className="text-title">How long you will be in Jakarta ?</h5>
                          <DateRangePicker
                            startDatePlaceholderText='Start'
                            endDatePlaceholderText='End'
                            startDate={startDate}
                            startDateId='startDateId'
                            endDate={endDate}
                            endDateId='endDateId'
                            onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })}
                            focusedInput={focusedInput}
                            onFocusChange={focusedInput => this.setState({ focusedInput })}
                            displayFormat='DD MMM YYYY'
                            hideKeyboardShortcutsPanel
                            numberOfMonths={1}
                            withPortal
                            minimumNights={0}
                          />
                        </div>
                      </div>
                    </Tab>
                    <Tab eventKey={1} title={"Bali"}>
                      asdasdasd
                    </Tab>
                    <Tab eventKey={2} title={"Lombok"}>
                      asdasdas
                    </Tab>
                    <Tab eventKey={3} title={"Surabaya"}>
                      asdasdas
                    </Tab>
                    <Tab eventKey={4} title={"Yogyakarta"}>
                      asdasdas
                    </Tab>
                    <Tab eventKey={5} title={"Nangroe Aceh Darusalam"}>
                      asdasdas
                    </Tab>
                    <Tab eventKey={6} title={"Kalimantan Barat"}>
                      asdasdas
                    </Tab>
                  </Tabs>
                </div>
                <div className="col-md-4">
                  <button className="btn btn-block btn-primary btn-submit mb1" type="submit">Go To Planning</button>
                  <SideBar title="Trip Details" destination="Jakarta" startDate="19 May 2019" endDate="23 May 2019" person={2} />
                </div>
              </div>
            </div>
          </section>
         
        </> 
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(PlanningItinerary);