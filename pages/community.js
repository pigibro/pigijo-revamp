import React from "react";
import { compose } from "redux";
import { connect } from 'react-redux'
import withAuth from '../_hoc/withAuth';
import PageHeader from '../components/community/page-header';
import {  urlImage  } from '../utils/helpers';
import Home from '../components/community/Home';
import Activity from '../components/community/Activity';
import Place from '../components/community/Place';
import Event from '../components/community/Event';
import { httpReqGetDetailCommunity } from '../stores/actions'
 
class Community extends React.Component {
  static async getInitialProps({ query , store}) {
    const { slug } = query
    const community = await store.dispatch( httpReqGetDetailCommunity(slug));
    return {community}
  }

  constructor(props){
    super(props)
    this.state = {
      activeTab : 'home'
    }
  }

  componentWillMount() {
  }

  renderContentTab(){
    switch (this.state.activeTab) {
      case 'home' : 
        return <Home/>;
      break;
      case 'activity' : 
        return <Activity/>;
      break;
      case 'place' : 
        return <Place/>;
      break;
      case 'event' : 
        return <Event/>;
      break;
      default:
        return <h1>Loading</h1>;
    }
  }

  render() {

    const styles = {
      containerTabItem : {
        marginTop : '8px',
        marginBottom : '8px'
      }
    }
    
    const community = this.props.community 
    return (
      <>
        <PageHeader
          imageAvatar={urlImage(community.detail.detail_community.logo.path+'/small/'+community.detail.detail_community.logo.filename)}
          background={urlImage(community.detail.detail_community.banner.path+'/small/'+community.detail.detail_community.banner.filename)} 
          title={community.detail.company_name} 
          caption={community.detail.detail_community.category_community.name}/>
        <section className="main-box sticky">
          <div className="container">
            <ul id="header-tabs">
              <li onClick = {()=> this.setState({activeTab : 'home'})} className={this.state.activeTab === 'home' ? 'active' : ''}><a href="#community-home">Home</a></li>
              <li onClick = {()=> this.setState({activeTab : 'event'})} className={this.state.activeTab === 'event' ? 'active' : ''}><a href="#community-event">Event</a></li>
              <li onClick = {()=> this.setState({activeTab : 'activity'})} className={this.state.activeTab === 'activity' ? 'active' : ''}><a href="#community-activity">Activities</a></li>
              <li onClick = {()=> this.setState({activeTab : 'place'})} className={this.state.activeTab === 'place' ? 'active' : ''}><a href="#community-place">Place</a></li> 
            </ul>
          </div>
        </section>
        <section>
          <div className="container" style={styles.containerTabItem}>
            {this.renderContentTab()}
          </div>
        </section>
      </>
    );
  }
}
const mapStateToProps = state => ({
  community: state.community,
})
 

export default compose(connect(mapStateToProps), withAuth(["PUBLIC"]))(Community);