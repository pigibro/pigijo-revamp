import { compose } from "redux";
import { connect } from "react-redux";
import Error from "./_error";
import Head from "../components/head"
import numeral from "../utils/numeral";
import { Link } from "../routes";
import PageHeader from "../components/page-header-icons";
import moment from "moment";
import { isEmpty, get } from "lodash";
import Pagination from "react-js-pagination";
import SearchAutoComplete from "../components/shared/searchautocomplete";
import { SingleDatePicker } from "react-dates";
import Loading from '../components/shared/loading';

import { getGuestHouse, getLocations, getCities, getDetailCategory, getaCitiesHotel, getHotelFirst, getHotelPage } from "../stores/actions";
import React from "react";
import withAuth from "../_hoc/withAuth";
import { slugify, urlImage, convertToRp, hitungSelisih } from "../utils/helpers";
import { FormControl, FormGroup, InputGroup, Glyphicon, Col, Panel, PanelGroup } from "react-bootstrap";
import FilterAcc from "../components/accommodation/filterAcc";
import IconLink from "../components/shared/iconLink";
import ContenAcc from "../components/accommodation/content";
import { dummy } from "../components/accommodation/tes";
import * as gtag from '../utils/gtag';
import Router from 'next/router';

// Router.events.on('routeChangeComplete', url => gtag.pageview(url))

class Accommodation extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			room: 1,
			data: [],
			page: 1,
			hotelpage: 1,
			isLoading: false,
			product_type: [],
			hasMore: true,
			isShow: null,
			focusedInput: null,
			startDate: moment(),
			endDate: moment().add(1, 'days'),
			location_detail: {},
			locations: [],
			locationsHotel: [],
			locationHotel_detail: {},
			adult: 1,
			infant: 0,
			child: 0,
			guest: 1,
			city: null,
      		province: null,
      		area: null,
			guestBox: false,
            Checkin: moment(),
            check_in_focused: false,
            Checkout: moment().add(1, 'days'),
            check_out_focused: false,
			destination: null,
			node: null,
			acc_type: 'hotel',
			hotels: {},
			hotelPagination: {
				current_page: 1,
				total_found: 0,
				offset: 25,
				lastPage: 1,
				displaying: 25,
				total: 0
			},
			minMax: {
				min: 0,
				max: 1500000,
			},
			rating: 1,
		};
	}

	static async getInitialProps({ query, store, req, res }) {
		const { slug, child } = query;
		
		const resCategory = await store.dispatch(getDetailCategory('accommodations'));
		const category = get(resCategory,'data');
		return { slug: slug, child: child, query, category };
	}

	getListRoom = async (params) => {
		this.setState({isLoading: true})
		const { 
			token, locationUser, dispatch} = this.props;
		const {
			enddates,
			startdates,
			location,
			locationtype,
			guest,
			room,
			type,
			slug
		} = this.props.query;
		const a = moment();
		const b = moment()
		.add(1, "days")
		
		let night = (startdates && enddates) ? moment(enddates).diff( moment(startdates), 'days') : 1
		let paramsTemp = {
			startdate: (startdates?startdates:moment().format("YYYY-MM-DD")),
			enddate: (enddates?enddates:moment()
			.add(1, "days")
			.format("YYYY-MM-DD")),
			night,
			room: (room ? room : 1),
			adult: ( guest ? guest : 1),
			infant: 0,
			child: 0,
			minprice: "",
			maxprice: "",
			page: 1,
		};
		if(locationtype === 'Kabupaten' || locationtype === 'Kota') {paramsTemp.city = location}
		if(locationtype === 'Provinsi') { paramsTemp.province = location }
		if(locationtype === 'Area'){ paramsTemp.area = location }
		if(type) { paramsTemp[type] = slug}
		let getGH = await dispatch(getGuestHouse('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAwLCJsb2dpbiI6ZmFsc2V9.TxpSsxxz6VHPQ8F86B1fvQx54118rP_ssc8T2qlZPd8', params ? params : paramsTemp));
		if(get(getGH, 'meta.code') === 200) {
			this.setState({isLoading: false})
		}
	};

	getHotelList = (e) => {
		this.setState({isLoading: true})
		let temp = this.state.hotelPagination
		if (!isEmpty(e)) {
			e.preventDefault();
		}
		const {dispatch} = this.props;
		const {enddates,startdates,location,locationtype, guest, room, type,slug} = this.props.query;
		const {startDate, endDate, locationsHotel} = this.state
		let locArr = slug.split('-')
		let locStr = ''
		if(locArr.length === 1) {
			locStr = slug
		} else if(locArr[0].toLowerCase == 'kota' || locArr[0].toLowerCase == 'area' || locArr[0].toLowerCase == 'kabupaten' || locArr[0].toLowerCase == 'provinsi') {
			let locTemp = locArr.slice(1, locArr.length)
			locStr = locTemp.join(' ')
		} else {
			locStr = locArr.join(' ')
		}
		let loc = locationsHotel.find(loc => locStr.toLowerCase() === loc.CityName.toLowerCase())
		
		let param = {
			GuestPassport: 'ID',
			DestinationKey: loc ? loc.CityKey : '',
			CheckInDate: startdates ? startdates : moment().format('YYYY-MM-DD'),
			CheckOutDate: enddates ? enddates : moment().add(1, 'days').format('YYYY-MM-DD'),
			CountRoom: (room ? room : 1),
			CountGuest: (guest ? guest : 1)
		}

		dispatch(getHotelFirst(param))
		.then(res => {
			
			let data = res.data
			temp.lastPage = data.MaxPage
			temp.total_found = data.Count
			if( data.Count <= temp.displaying) {
				temp.displaying = data.Count
			}
			temp.total = data.Count
			let hotelList = []
			for(let i = 0; i < data.Hotels.length; i++) {
				if(data.Hotels[i].Rooms[0].Status !== 2) {
					data.Hotels[i].business_uri = data.Hotels[i].HotelName
					data.Hotels[i].name = data.Hotels[i].HotelName
					data.Hotels[i].photo_primary = data.Hotels[i].ImageUri
					data.Hotels[i].province_name = data.Hotels[i].Area
					data.Hotels[i].price = data.Hotels[i].AveragePrice
					hotelList.push(data.Hotels[i])
				}
				
				if(i === data.Hotels.length-1) {
					data.Hotels = hotelList
				}
				
			}

			this.setState({hotels: data, hotelPagination: temp, isLoading: false})
		})
		.catch(err => {
			console.log('====================================');
			console.log(err);
			console.log('====================================');
		})
	}

	getHotelPagination = (e) => {
		let { hotels, hotelpage } = this.state
		let temp = this.state.hotelPagination
		if (!isEmpty(e)) {
			e.preventDefault();
		}
		const {dispatch} = this.props;
		// const {enddates,startdates,location,locationtype,type,slug} = this.props.query;
		// const {startDate, endDate, room, guest, locationsHotel} = this.state
		// let loc = locationsHotel.find(loc => slug === loc.CityName.toLowerCase())
		let param = {
			CorrelationId: hotels.CorrelationId,
			Page: hotelpage,
		}
		temp.current_page = hotelpage
		temp.displaying = temp.displaying + 25
		dispatch(getHotelPage(param))
		.then(res => {
			let data = res.data
			for(let i = 0; i < data.Hotels.length; i++) {
				data.Hotels[i].business_uri = data.Hotels[i].HotelName
				data.Hotels[i].name = data.Hotels[i].HotelName
				data.Hotels[i].photo_primary = data.Hotels[i].ImageUri
				data.Hotels[i].province_name = data.Hotels[i].Area
				data.Hotels[i].price = data.Hotels[i].AveragePrice
			}
			let tempHotel = hotels
			tempHotel.Hotels = [...data.Hotels]
			this.setState({hotels: tempHotel, hotelPagination: temp})
		})
		.catch(err => {
			console.log('====================================');
			console.log(err);
			console.log('====================================');
		})
	}

	findAccomodation = async (e, custparams) => {
		if (!isEmpty(e)) {
			e.preventDefault();
		}
		const { startDate, endDate, adult, child, infant, destination, page, duration, room, minMax } = this.state;
		const { dispatch, query } = this.props;
		let price = minMax.max !== 0 ? `${minMax.min}-${minMax.max}` : "";
		const night = (endDate ? endDate : moment()).diff(startDate ? startDate : 1, 'days')
		let params = {
			startdate: moment(startDate).format("YYYY-MM-DD"),
			enddate: moment(endDate).format("YYYY-MM-DD"),
			night: night,
			room: room,
			adult: adult,
			infant: infant,
			child: child,
			price: price,
			page: page,
		};
		if(destination !== null) {
			params[destination.type.toLowerCase()] = destination.slug
		} else if(destination === null && query.type && query.slug) {
			params[query.type] = query.slug
		}
		await dispatch(getGuestHouse('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAwLCJsb2dpbiI6ZmFsc2V9.TxpSsxxz6VHPQ8F86B1fvQx54118rP_ssc8T2qlZPd8', custparams ? custparams : params));
		
	};

	onStarClick = (nextValue, prevValue, name) => {
		this.setState({ rating: nextValue });
	};

	componentDidMount() {
		const {
			enddates,
			startdates,
			location,
			locationtype,
			guest,
			room,
			type,
			slug
		} = this.props.query;
		let locArr = slug.split('-')
		let locStr = (locArr.length > 1 ? locArr.join(' ') : slug)
		this.setState({
			endDate: (enddates ? moment(enddates) : moment().add(1, 'days')),
			startDate: (startdates ? moment(startdates) : moment()),
			Checkout: (enddates ? moment(enddates) : moment().add(1, 'days')),
			Checkin: (startdates ? moment(startdates) : moment()),
			guest: (guest ? guest : 1),
			room: (room ? room : 1),
			area: (type == 'area' ? locStr : null),
			city: (type == 'kota' ? locStr : null),
			province: (type == 'province' ? locStr : null)
		})
		this.props.dispatch(getCities())
		.then(res => {
			this.setState({locations: get(res, 'data')})
		})
		.catch(err => {
			console.log(err);
		})

		this.props.dispatch(getaCitiesHotel())
		.then(res => {
			this.setState({locationsHotel: get(res, 'data')}, () => this.getHotelList())
		})
		.catch(err => {
			console.log(err);
		})
		this.getListRoom();
		document.addEventListener("mousedown", this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener("mousedown", this.handleClickOutside);
	}

	onDatesChange = ({ startDate, endDate }) => {
		this.setState({ startDate, endDate, page: 1 }, () => this.findAccomodation());
		if (endDate !== null) {
			let day = hitungSelisih(startDate, endDate);
			this.setState({ duration: day + 1 });
		}
	};

	onFocusChange = (focusedInput) => {
		this.setState({ focusedInput });
	};

	changeShow = (e, name) => {
		e.preventDefault();
		this.setState({ isShow: name });
	};

	setWrapperRef = (node) => {
		this.wrapperRef = node;
	};

	setWrapperRefPrice = (node) => {
		this.wrapperRef = node;
	};

	handleClickOutside = (event) => {
		if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
			this.setState({ isShow: null });
		}
		// if (this.setWrapperRefPrice && !this.setWrapperRefPrice.contains(event.target)) {
		// 	this.setState({ isShow: null });
		// }
	};

	handleTypeFilter = (e) => {
		const { product_type } = this.state;
		let newState = product_type;
		if (product_type.indexOf(e.target.value) < 0) {
			newState.push(e.target.value);
		} else {
			newState.splice(product_type.indexOf(e.target.value), 1);
		}
		this.setState({
			product_type: newState,
		});
	};

	anyClick = (e) => {

		return this.setState({ isShow: null });
	};

	handleRoom = (val) => {
		this.setState({room: val, page: 1}, () => this.findAccomodation())
	}

	handlePrice = () => {
		this.setState({page: 1}, () => this.findAccomodation()) 
	};

	handlePriceRange = (val) => {
		this.setState({ minMax: val });
	};

	plus = (e, name) => {
		event.preventDefault();
		switch (name) {
			case "Adult":
				if (this.state.adult < 8) {
					this.setState({ adult: this.state.adult + 1, page: 1 }, () => this.findAccomodation());
				}
				break;
			case "Child":
				if (this.state.child < 7) {
					this.setState({ child: this.state.child + 1, page: 1 }, () => this.findAccomodation());
				}
				break;
			default:
				if (this.state.infant < 4) {
					this.setState({ infant: this.state.infant + 1, page: 1 }, () => this.findAccomodation());
				}
				break;
		}
	};

	handleLocation = (value) => {
		if(value.length > 0){
		  if(value[0].type === 'Area'){
			this.setState({area: value[0].name, city: null, province: null, more: true}, () => {
			});
		  }else if (value[0].type === 'Kabupaten' || value[0].type === 'Kota'){
			this.setState({area: null, city: value[0].name, province: null, more: true}, () => {
			});
		  }else{
			this.setState({area: null, city: null, province: value[0].name, more: true}, () => {
			});
		  }
  		}
	  }

	minus = (e, name) => {
		event.preventDefault();
		switch (name) {
			case "Adult":
				if (this.state.adult > 1) {
					this.setState({ adult: this.state.adult - 1, page: 1 }, () => this.findAccomodation());
				}
				break;
			case "Child":
				if (this.state.child > 0) {
					this.setState({ child: this.state.child - 1, page: 1 }, () => this.findAccomodation());
				}
				break;
			default:
				if (this.state.infant > 0) {
					this.setState({ infant: this.state.infant - 1, page: 1 }, () => this.findAccomodation());
				}
				break;
		}
	};

	handleDestination = (city) => {
		let getObj = city[0];
		this.setState({ destination: getObj, page: 1 }, () => {
			if (getObj !== undefined) {
				this.findAccomodation();
			}
		});
	};
	
	IncrementGuest = () => {
        this.setState({ guest: this.state.guest + 1 });
    }
    DecreaseGuest = () => {
        this.setState({ guest: this.state.guest - 1 });
    }

    IncrementRoom = () => {
        this.setState({ room: this.state.room + 1 });
    }
    DecreaseRoom = () => {
        this.setState({ room: this.state.room - 1 });
    }

	searchHomestay = (e) => {
		e.preventDefault()		
		let { Checkin, Checkout , room, guest, area, city, province } = this.state
		let location = (area === null && city === null && province === null) ? false : true				
		if( Checkin !== null && Checkout !== null && guest !== 0 && room !== 0 && location) {
			let night = Checkout.diff(Checkin, 'days')
			let locationType = area !== null ? 'area' : (city !== null ? 'kota' : (province ? 'province' : ''))
			let loc = area !== null ? area.toLowerCase() : (city !== null ? city.toLowerCase() : (province ? province.toLowerCase() : ''))
			let locArr = loc.split(' ')
			let locSlug = locArr.join('-')
			let query = {
				startdates: moment(Checkin).format("YYYY-MM-DD"),
				enddates: moment(Checkout).format("YYYY-MM-DD"),
				room: room,
				guest,
			};

			Router.push({pathname: `/accommodation/l/${locationType}/${locSlug}`, query})
		}
		
	}

	render() {
		const { slug, locations, locationUser, list_GHouse, pagination,
			province,
			startDates,
			endDates,
			category
		} = this.props;
		const {
			room,
			adult,
			infant,
			child,
			data,
			isShow,
			showPass,
			location,
			hasMore,
			showCalendar,
			startDate,
			endDate,
			focusedInput,
			minMax,
			rating,
			page,
			hotelPagination
		} = this.state;

		let pass = [
			{ name: "Adult", label: "(> 12 Years)", value: this.state.adult },
			{ name: "Child", label: "(2-11 Years)", value: this.state.child },
			{ name: "Infant", label: "(< 2 Years)", value: this.state.infant },
		];
		let arrAdult = [];
		for (let i = 1; i <= 9; i++) {
			arrAdult.push(i);
		}

		let arrInfant = [];
		for (let i = 0; i <= 8; i++) {
			arrInfant.push(i);
		}

		let arrChild = [];
		for (let i = 0; i <= 5; i++) {
			arrChild.push(i);
		}
		const minimumDate = this.state.startDate;
		let locTemp = this.props.query.slug.split('-')
		let defLoc = locTemp.join(' ')
		return (
			<>
				<Head title={category.meta_title} description={category.meta_description} url={process.env.SITE_ROOT+'/accommodations'}/>
        		<PageHeader section='main' background={"/static/images/img05.jpg"} title={category.name} caption={category.short_description} category="accommodation" type={this.props.query.type} slug={this.props.query.slug}/>
				<section className="body-content" style={{backgroundColor: '#F7F7F7'}}>
					<div className="container">
						<div className="row flex-center" style={{display: 'flex', justifyContent: 'center'}}>
						<div className="col-md-8 cat-header" style={{padding: '1em'}}>
							<form onSubmit={this.searchHomestay}>
							<div className="row" style={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
								<div className="col-lg-5 col-md-5 col-sm-5">
								<span className='text-label'>Location</span>
								<SearchAutoComplete
									id="destination"
									key={"select-single"}
									index={0}
									data={this.state.locations || []}
									placeholder="Select City"
									labelKey="name"
									onChange={this.handleLocation}
									bsSize="large"
									defaultInputValue={defLoc}
								/>
								</div>

								<div className="col-lg-5 col-md-5 col-sm-5">
									<span className='text-label'>Guests and Rooms</span>
									<div className="passengers-form guest-filter" style={{marginBottom: '0.5em'}}>
                                        <span className="pass-pep-number type-number">{this.state.guest}</span><span className="pass-pep type-text">Guest </span> 
                                        <span className="pass-pep-number type-number">{this.state.room}</span><span className="pass-pep type-text">Room </span> 
                                        <button className="booking-form__toggle-btn dropdown-icon" onClick={() => this.setState({ guestBox: !this.state.guestBox })} type="button">
                                            <span className="ti-angle-down"></span>
                                        </button>
                                        <ul className="booking-form__passengers passengers-list option-ul" style={{ display: (this.state.guestBox ? 'block' : 'none') }}>
                                            <li className="passengers-list__row passengers-chooser option-box">
                                                <label className="passengers-chooser__title option-label" >Guest</label>
                                                <div className="passengers-chooser__amount js-amount-input-container amount-box">
                                                    <button 
                                                        className="passengers-chooser__amount-btn amount-btn" 
                                                        type="button" 
                                                        onClick={this.DecreaseGuest}
                                                        disabled={this.state.guest <= 1} 
                                                    >-</button>
                                                    <span className="passengers-chooser__amount-field"> {this.state.guest} </span>
                                                    <button 
                                                        className="passengers-chooser__amount-btn amount-btn" 
                                                        type="button" 
                                                        onClick={this.IncrementGuest}
                                                        disabled={this.state.guest >= this.state.room*4}
                                                    >+</button>
                                                </div>
                                            </li>
                                            <li className="passengers-list__row passengers-chooser option-box">
                                                <label className="passengers-chooser__title option-label" >Room</label>
                                                <div className="passengers-chooser__amount js-amount-input-container amount-box">
                                                    <button 
                                                        className="passengers-chooser__amount-btn amount-btn" 
                                                        type="button" 
                                                        onClick={this.DecreaseRoom}
                                                        disabled={this.state.room < 1}
                                                    >-</button>
                                                    <span className="passengers-chooser__amount-field"> {this.state.room} </span>
                                                    <button 
                                                        className="passengers-chooser__amount-btn amount-btn" 
                                                        type="button" 
                                                        onClick={this.IncrementRoom}
                                                        disabled={this.state.room >= this.state.guest}
                                                    >+</button>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>								
								</div>

								<div className="col-lg-5 col-md-5 col-sm-5">
									<span className='text-label'>Check-in</span>
									<SingleDatePicker
										numberOfMonths={1}
										isOutsideRange={(day) => day.isBefore(minimumDate)}
										date={this.state.startDate}
										onDateChange={date =>
											this.setState({
											startDate: date,
											Checkin: moment(date)
											})
										}
										focused={this.state.check_in_focused}
										onFocusChange={({ focused }) => this.setState({ check_in_focused: focused })}
										id="startdate"
										placeholder="Return Date"
										style={{fontSize: '14px'}}
									/>
								</div>

								<div className="col-lg-5 col-md-5 col-sm-5">
									<span className='text-label'>Check-out</span>
									<SingleDatePicker
										numberOfMonths={1}
										isOutsideRange={(day) => day.isBefore(minimumDate)}
										date={this.state.endDate}
										onDateChange={date =>
											this.setState({
											endDate: date,
											Checkout: moment(date)
											})
										}
										focused={this.state.check_out_focused}
										onFocusChange={({ focused }) => this.setState({ check_out_focused: focused })}
										id="enddate"
										placeholder="Return Date"
									/>
								</div>

								<div className="col-lg-10 col-md-10 col-sm-10 mt1 mb1" >
								<div className="row" style={{display: 'flex', justifyContent: 'flex-end'}}>
									<div className="col-lg-4 col-md-4 col-sm-12 mt1 mb1" >
									<button
										type='submit'
										className='btn btn-block btn-primary btn-fix btn-blue btn-flight-search'
									>
										Search
									</button>
									</div>
								</div>
								
								</div>
							</div>
							</form>
						</div>
						</div>
					</div>
					</section>
				{/* <section className="filter-acc sticky" style={{paddingTop: '1.2em', height: '100%', backgroundColor: '#fff'}}>
					<div className="container">
						<div className="col-md-9 ml0">
							<FilterAcc
								adult={adult}
								infant={infant}
								child={child}
								data={data}
								isShow={isShow}
								showPass={showPass}
								startDate={startDate}
								endDate={endDate}
								focusedInput={focusedInput}
								pass={pass}
								locations={locations}
								handleRoom={this.handleRoom}
								minus={this.minus}
								plus={this.plus}
								handleDestination={this.handleDestination}
								anyClick={this.anyClick}
								handleRoom={this.handleRoom}
								handlePrice={this.handlePrice}
								handlePriceRange={this.handlePriceRange}
								minMax={minMax}
								onDatesChange={this.onDatesChange}
								onFocusChange={this.onFocusChange}
								changeShow={this.changeShow}
								convertToRp={convertToRp}
								setWrapperRef={this.setWrapperRef}
								setWrapperRefPrice={this.setWrapperRefPrice}
								query={this.props.query}
							/>
						</div>
						<Col md={3} align="right">
							
						</Col>
					</div>
				</section> */}

				<section className="body-content mt2 " style={{backgroundColor: 'white'}}>
					<div className="container">
						<div className="row">
							<div className="col-xs-12" style={{marginBottom: '2em'}}>
								<div className="row">
									<div className="col-xs-12 col-md-6" style={{display: 'flex'}}>
										<div className={`menu-box ${this.state.acc_type === 'hotel' ? 'menu-box-active' : ''}`} onClick={() => this.setState({acc_type: 'hotel'})} >
											<p className={`menu ${this.state.acc_type === 'hotel' ? 'menu-active' : ''}`} style={{margin: 0}}>Hotel</p>
										</div>
										<div className={`menu-box ${this.state.acc_type === 'homestay' ? 'menu-box-active' : ''}`} onClick={() => this.setState({acc_type: 'homestay'})} >
											<p className={`menu ${this.state.acc_type === 'homestay' ? 'menu-active' : ''}`} style={{margin: 0}}>Homestay</p>
										</div>
									</div>
								</div>
							</div>
							{/* <h3 className="text-title title-x">Pick Your Accommodation</h3> */}
							{
								this.state.isLoading ? (<Loading/>) : (
									<ContenAcc data={this.state.acc_type === 'hotel' ? this.state.hotels.Hotels : list_GHouse} rating={rating} onStarClick={this.onStarClick} query={{
										type: this.state.acc_type,
										CorrelationId: this.state.hotels.CorrelationId ? this.state.hotels.CorrelationId : '',
										startdate: moment(startDate).format("YYYY-MM-DD"),
										enddate: moment(endDate).format("YYYY-MM-DD"),
										night: (endDate ? endDate : moment()).diff(startDate !== null ? startDate : 1, 'days'),
										room: room,
										adult: adult,
										child: child,
										infant: infant
									}}/>
								)
							}
							
							<div className="col-xs-12" style={{ height: "150px" }}>
								{!isEmpty(pagination) &&
									(!isEmpty(list_GHouse) && (
										<nav className="pagination-nav">
											<span className="text-label">
												Displaying {isEmpty(pagination) ? 0 : pagination.displaying} of{" "}
												{isEmpty(pagination) ? 0 : pagination.total}
											</span>
											<Pagination
												activePage={page}
												itemsCountPerPage={pagination.offset}
												pageCount={pagination.last_page}
												totalItemsCount={pagination.total_found}
												pageRangeDisplayed={3}
												onChange={(nextPage) => this.setState({page: nextPage}, () => this.findAccomodation())}
											/>
										</nav>
									))}
							</div>
							{
								this.state.acc_type === 'hotel' && (
									<div className="col-xs-12" style={{ height: "150px" }}>
										{!isEmpty(hotelPagination) &&
											(!isEmpty(this.state.hotels) && (
												<nav className="pagination-nav">
													<span className="text-label">
														Displaying {isEmpty(hotelPagination) ? 0 : hotelPagination.displaying} of{" "}
														{isEmpty(hotelPagination) ? 0 : hotelPagination.total}
													</span>
													<Pagination
														activePage={this.state.hotelpage}
														itemsCountPerPage={hotelPagination.offset}
														pageCount={hotelPagination.last_page}
														totalItemsCount={hotelPagination.total_found}
														pageRangeDisplayed={3}
														onChange={(nextPage) => this.setState({hotelpage: nextPage}, () => this.getHotelPagination())}
													/>
												</nav>
											))}
									</div>
								)
							}
							
						</div>
					</div>
				</section>
				<style jsx global>{`
					.guest-filter {
						padding: 11px 10px 11px 10px;
						border: 1px solid #D8DDE6;
						border-radius: 0;
						background-color: #fff
					}

					.type-text {
						color: #b4b4b4;
						font-size: 10pt;
					}

					.type-number {
						color: #e17306;
					}

					.dropdown-icon {
						width: 13px;
						height: 13px;
						position: absolute;
						outline: none;
						border: none;
						border-radius: 50%;
						color: #e17306;
						background: transparent;
						font-size: 8pt;
						font-weight: 800;
						right: 15%
					}

					.option-ul {
						margin: 10px 0 0 -10px;
						padding: 10px 15px;
						position: absolute;
						z-index: 100;
						background: #fff;
						width: 90%;
						border: 1px solid #e9eef3;
						border-top: none;
					}

					.option-box {
						padding: 8px 0;
						list-style-type: none;
						border-bottom: 1px solid #cfcfcf;
						display: flex;
    					justify-content: space-between;
					}

					.option-label {
						font-weight: normal;
					}

					.amount-box {
						padding: 5px;
						border: 1px solid #e17306;
						border-radius: 20px;
					}

					.amount-number {
						padding: 0 5px 0 5px;
					}

					.amount-btn {
						width: 23px;
						border: none;
						border-radius: 50%;
						outline: none;
						font: inherit;
						font-size: 17px;
					}

					.SingleDatePicker_1 .SingleDatePickerInput .DateInput_1 .DateInput_input {
						font-size: 14px;
					}

					.menu {
						color: #000
					}

					.menu-active {
						color: #FF5B00
					}

					.menu-box {
						width: 150px;
						border-bottom: 2px solid #E8E8E8;
						padding: 15px 0;
						text-align: center;
						cursor: pointer
					}

					.menu-box-active {
						border-bottom: 2px solid #FF5B00 !important
					}

					@media (max-width: 1220px){

					}
				`}</style>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	locations: state.city.data,
	list_GHouse: state.guestHoust.list_GHouse,
	pagination: state.guestHoust.pagination
});

export default compose(
	connect(mapStateToProps),
	withAuth(["PUBLIC"]),
)(Accommodation);
