import React, { Component } from 'react';
import Head from '../components/head';
import PageHeader from '../components/page-header';
import { compose } from "redux";
import withAuth from '../_hoc/withAuth';


class HowtoUse extends Component {
    render() {
        return (
            <React.Fragment>
                <Head title={"How to Use"} description={"How to Use"} url={process.env.SITE_ROOT + '/media-release'} />
                <PageHeader background={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners.jpg"} title={`How to Use Pigijo`} caption="" />
                <div className='container'>
                    <div className='row'>
                        {/* <div className='col-sm-12'>
                            <div className='nav-tab'>
                                <a href='#' className='text-green'>How to Use</a> */}
                        {/* <a href='/how-to-book' className='text-dark'>How to Book</a> */}
                        {/* </div>
                        </div> */}

                        <div className='col-sm-12 tab-content'>
                            <p>
                                We are here to be your traveling reference in Indonesia. As the world largest island country with more than 17.000 islands, we will do our best to provide you with everything you need for your time in Indonesia.
                            </p>
                            <p>There is a lot of Information in our database, and we know it can be a bit overwhelming for you. Hence, here are our Tips for you to get the best out of Pigijo!</p>
                        </div>
                    </div>
                    <p className='text-dark'>Looking for Reference</p>
                    <div className='row'>
                        <div className='col-sm-2'>
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/places_icon.png' />
                        </div>
                        <div className='col-sm-9'>
                            <p className='text-dark'>Places</p>
                            <p>You can search on the Pigijo Home Page. You can type anything related to your journey. Choose Places, we got plenty of places all around Indonesia! And you do not need to pay us anything for those “places” Reference</p>
                        </div>
                    </div>

                    <div className='clearfix' style={{ 'paddingTop': '50px' }}></div>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <p className='text-dark'>Looking For Tour, Assistance, Homestay, and Vehicle</p>
                            <p>We are Partnering with local businesses provider, to offers you services that you might need during your vacations.</p>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-sm-2'>
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/ta_icon.png' />
                        </div>
                        <div className='col-sm-9'>
                            <p className='text-dark'>Assistances</p>
                            <p>Do you want to know the local culture, history, and have the best experience? Great! Our partners (travel assistant) are here to help you</p>
                        </div>
                    </div>
                    <div className='clearfix' style={{ 'paddingTop': '30px' }}></div>

                    <div className='row'>
                        <div className='col-sm-2'>
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/homestay.png' />
                        </div>
                        <div className='col-sm-9'>
                            <p className='text-dark'>Homestay</p>
                            <p>Are you bored of staying in the “branded” hotels, and wanted to try to live as locals do? No worries! Our Partners (Homestay) are ready to share that experience</p>
                        </div>
                    </div>
                    <div className='clearfix' style={{ 'paddingTop': '30px' }}></div>

                    <div className='row'>
                        <div className='col-sm-2'>
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/experience_icon.png' />
                        </div>
                        <div className='col-sm-9'>
                            <p className='text-dark'>Local Experience</p>
                            <p>Do you want to just purchase Tour packages provided by local agent? Guess what? Pigijo got that covered for you</p>
                        </div>
                    </div>
                    <div className='clearfix' style={{ 'paddingTop': '30px' }}></div>


                    <div className='row'>
                        <div className='col-sm-2'>
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/rent_car.png' />
                        </div>
                        <div className='col-sm-9'>
                            <p className='text-dark'>Rent Car</p>
                            <p>Need to rent a vehicle during your vacation? Our local partner can provide you that</p>
                        </div>
                    </div>
                    <div className='clearfix' style={{ 'paddingTop': '30px' }}></div>
                    <style jsx>{`
                        .nav-tab{
                            margin: 30px 0;
                        }

                        .well-padding{
                            padding: 25px;
                        }

                        .tab-content{
                            border: none;
                        }

                        .nav-tab a{
                            margin-right: 30px;
                        }

                        .text-green{
                            color: #49A9A7;
                            font: 18px Helvetica;
                            font-weight: bold;
                        }  
                        
                        .text-dark{
                            color: #333;
                            font: 18px Helvetica;
                            font-weight: bold;
                        } 
                        
                        .tab-content p{
                            font: 16px/24px Helvetica;
                        }

                        .tab-content h4{
                            color: #333;
                            font-weight: bold;
                        }

                        .col-sm-2 img{
                            width: 80px;
                            margin-bottom: 15px;
                        }
                    `}</style>
                </div>
            </React.Fragment>
        );
    }
}

export default compose(
    withAuth(["PUBLIC"])
)(HowtoUse);