import React from "react";
import { compose } from "redux";
import withAuth from "../_hoc/withAuth";
import Headers from '../components/profile/_component/header';
import { getTraveller } from '../stores/actions/travellerActions'
import { connect } from 'react-redux'
import Index from '../components/profile/travellerList/index'

class Traveller extends React.Component {

  componentWillMount() {
    this.props.dispatch(getTraveller(this.props.token))
  }

  render() {
    return (
      <div>
        <Headers />
        <Index path={this.props.asPath} token={this.props.token} />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  traveller: state.traveller,
})


export default compose(connect(mapStateToProps), withAuth(["PRIVATE"]))(Traveller);