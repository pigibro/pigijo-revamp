import { compose } from 'redux'
import { connect } from 'react-redux'
import Error from './_error'
import { get, isEmpty, takeRight } from 'lodash'
import { TabList, Tabs, Tab, TabPanel, resetIdCounter } from 'react-tabs';
import moment from 'moment';
import Head from '../components/head';
import PageHeader from '../components/page-header';
import SideBar from '../components/shared/sidebar';
import ActivityCart from '../components/activity/carts';
import FlightPlanning from '../components/flight/flightPlanning';
import ActivityPlanning from '../components/activity/activityPlanning';
import AccommodationPlanning from '../components/accommodation/accommodationPlanning';
import RentPlanning from '../components/rent/rentPlanning';
import React from 'react'
import withAuth from '../_hoc/withAuth'

class PlanningDay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          activeKey : 0,
          startDate: null,
          endDate: null,
          focusedInput: null,
        };
    }

    static async getInitialProps({ query, store}) {
      resetIdCounter();
    }

    render(){
      const {startDate,endDate, focusedInput, activeKey} = this.state;
      return (
        <>
          <Head title={"Eu nostrud amet esse enim est esse commodo reprehenderit."} description={"Culpa tempor eu elit duis eu occaecat culpa culpa. Commodo anim commodo occaecat do consequat consequat Lorem laborum qui duis reprehenderit nisi Lorem ullamco. Dolore cupidatat proident esse irure cillum deserunt. Sit et tempor sint duis occaecat ea culpa."} url={process.env.SITE_ROOT+'/p/1180-showcase-indonesia-full-pass-early-bird'}/>
          <PageHeader background={"/static/images/img05.jpg"} title="Confirm your trip’s route and date." caption="Check again your trip’s route and date. You can customise your trip duration in each destination as you wish by changing the arrival date on each destination."/>
          <section className='content-wrap'>
            <div className='container'>
              <div className='row timeline-steps'>
                <div className='col-xs-4 step-item active'>
                  <span className='step bg-ongoing'>1</span>
                </div>
                <div className='col-xs-4 step-item active'>
                  <span className='step bg-todo'>2</span>
                </div>
                <div className='col-xs-4 step-item'>
                  <span className='step bg-todo'>3</span>
                </div>
              </div>
              <div className="row"> 
                <div className="col-md-8">
                  <Tabs defaultIndex={activeKey}
                    onSelect={(id) => this.setState({ activeKey: parseInt(id,10)})}
                  >
                    <nav className="tab-control timeline-days">
                      <TabList className="nav nav-tabs">
                        <Tab className="text-dark" style={{ cursor: 'pointer' }}>
                          day 1
                        </Tab>
                        <Tab className="text-dark" style={{ cursor: 'pointer' }}>
                          day 2
                        </Tab>
                        <Tab className="text-dark" style={{ cursor: 'pointer' }}>
                          day 3
                        </Tab>
                      </TabList>
                    </nav>
                    <TabPanel>
                      <div className="text-wrapper flex-row flex-center mb1">
                        <div className="title-label">
                          <h5 className="text-title m0">DAY 1</h5>
                          <div className="text-dark">
                            {moment('2019-04-01').format('ddd, DD MMM YYYY')}
                          </div>
                        </div>
                        <div className="location mla">
                          <h5 className="text-title m0">
                            Jakarta
                          </h5>
                        </div>
                      </div>
                      <div className="act-explore">
                        <ActivityPlanning/>
                        <ActivityCart/>
                        <FlightPlanning/>
                        <AccommodationPlanning/>
                        <RentPlanning/>
                      </div>
                    </TabPanel>
                    <TabPanel>
                      day 2
                    </TabPanel>
                    <TabPanel>
                      day 3
                    </TabPanel>
                  </Tabs>
                  
                </div>
                <div className="col-md-4">
                  <div className="action-btn btn-fix-wrapper mb1">
                    <button className="btn btn-block btn-o btn-primary" href="">Save as Draft</button>
                    <button className="btn btn-block btn-primary" href="" >Finish Planning</button>
                  </div>
                  <SideBar title="Trip Details" destination="Jakarta" startDate="19 May 2019" endDate="23 May 2019" person={2} />
                  <SideBar title="Pricing Details" totalPrice="200000" cartList={{test:['asdasd']}} />
                </div>
              </div>
            </div>
          </section>
        </> 
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(PlanningDay);