import React, { Component } from 'react'
import { get, isEmpty, concat } from 'lodash';
import { compose } from "redux";
import { connect } from 'react-redux';
import { Link, Router } from '../routes';
import { setErrorMessage, setSuccessMessage } from '../stores/actions'
import withAuth from '../_hoc/withAuth';
import Head from '../components/head';
import { apiCall, apiUrl } from '../services/request'
import { Modal, Dropdown, DropdownButton, MenuItem } from "react-bootstrap";
import PageHeader from '../components/page-header';
import { SingleDatePicker } from "react-dates";
import 'react-dates/initialize';

class Imitokohotomotif extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstnama: '',
            lastnama: '',
            company: '',
            email: '',
            phone: '',
            gender: 'Mr',
            position: '',
            typeString: ''
        };
    }

    static async getInitialProps({ query, store }) {
        const { type } = query;
        return { type }
    }

    componentDidMount() {
        // let typeJoin = this.props.type.split("-")
        // let typeString = typeJoin.join(' ')
        // this.setState({ position: typeString })
    }

    Regist = async () => {
        let { dispatch, type } = this.props
        let { firstname, lastname, company, email, phone, gender, position } = this.state
        if (firstname == '' || lastname == '' || phone == '' || company == '' || email == '' || position == '' || gender == null) {
            let res = await dispatch(setErrorMessage('Semua field wajib diisi'))
        } else {
            this.sendForm()
        }
    }

    sendForm = async () => {
        let { dispatch, type } = this.props
        let { firstname, lastname, company, email, phone, gender, position } = this.state
        let data = {
            type: 'ipo',
            firstname,
            lastname,
            phone,
            email,
            gender,
            company,
            position
        }

        const dataReq = {
            method: "POST",
            url: `https://api-registration.pigijo.com/user/register`,
            data: {
                data
            }
        };

        const res = await dispatch(apiCall(dataReq));
        if (get(res, 'meta.code') === 200) {
            let res = await dispatch(setSuccessMessage('Registrasi berhasil, silahkan periksa email untuk verifikasi'))
            Router.push({ pathname: '/ipo-registration/thankyou', })

        } else if (get(res, 'meta.code') === 400) {
            await dispatch(setErrorMessage(get(res, 'meta.message')))
        }
    }

    changeEmail = (val) => {
        let pattern = /[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g;
        let result = pattern.test(val)
        if (result) {
            this.setState({ email: val, emailerror: '' })
        } else {
            this.setState({ email: val, emailerror: 'Email tidak valid' })
        }
    }

    changePhone = (val) => {
        let patternNumber = /^[0-9\b]+$/;
        let NumCheck = patternNumber.test(+val)
        let string = `${val}`
        if (string.length < 8 || string.length > 13 || string[0] !== '0' || !NumCheck) {
            this.setState({ phone: val, phonerror: 'Nomor telfon tidak valid' })
        } else {
            this.setState({ phone: val, phonerror: '' })
        }
    }

    render() {
        let { firstname, lastname, company, email, phone, gender, position } = this.state

        return (
            <>
                <div>
                    <Head />
                    <div className="row" style={{ paddingLeft: '3em', paddingRight: '3em' }}>
                        <div className="col-lg-5">
                            <div className="logo row">
                                <div className="logo-idx col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <img alt="IDX" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/Logo_BEI_member_of_WFE_hi_res.png" />
                                </div>
                                <div className="logo-pigijo col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <img alt="Background" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/pigijo_logo-01.svg" />
                                </div>
                                <div className="logo-sf col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <img alt="Sf" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/sf_logo.png" />
                                </div>
                            </div>
                            <div className="header">
                                <span>Register</span>
                                <p>Please fill your details to get a barcode to enter PT Tourindo Guide Indonesia Tbk's
                        Initial Public Offering, 8 January 2020.</p>
                            </div>
                            <div className="forms">
                                <div className="row" style={{ marginBottom: '1em' }}>
                                    <div className="col-lg-12">
                                        <input type="text" className="form-control" placeholder="Email" value={email} onChange={e => this.setState({ email: e.target.value })} />
                                    </div>
                                </div>
                                <div className="row" style={{ marginBottom: '1em' }}>
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <select className="form-control" value={gender} onChange={e => this.setState({ gender: e.target.value })}>
                                            <option>Mr</option>
                                            <option>Mrs</option>
                                            <option>Ms</option>
                                        </select>
                                    </div>
                                    <div className="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" className="form-control" placeholder="First Name" value={firstname} onChange={e => this.setState({ firstname: e.target.value })} />
                                    </div>
                                </div>
                                <div className="row" style={{ marginBottom: '1em' }}>
                                    <div className="col-lg-12">
                                        <input type="text" className="form-control" placeholder="Last Name" value={lastname} onChange={e => this.setState({ lastname: e.target.value })} />
                                    </div>
                                </div>
                                <div className="row" style={{ marginBottom: '1em' }}>
                                    <div className="col-lg-12">
                                        <input type="text" className="form-control" placeholder="Phone" value={phone} onChange={e => this.changePhone(e.target.value)} />
                                    </div>
                                </div>
                                <div className="row" style={{ marginBottom: '1em' }}>
                                    <div className="col-lg-12">
                                        <input type="text" className="form-control" placeholder="Company" value={company} onChange={e => this.setState({ company: e.target.value })} />
                                    </div>
                                </div>
                                <div className="row" style={{ marginBottom: '1em' }}>
                                    <div className="col-lg-12">
                                        <input type="text" className="form-control" placeholder="Job Title" value={position} onChange={e => this.setState({ position: e.target.value })} />
                                    </div>
                                </div>
                                <div className="submit-button row">
                                    <div className="col-lg-12">
                                        <button type="button" className="btn btn-submit" onClick={this.Regist}>Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-7 img-background">
                            <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/background.jpg" alt="Background" />
                        </div>
                    </div>
                </div>


                <style jsx global>{`
                    .logo {
                        margin-top: 20px;
                        margin-bottom: 80px;
                    }
                    
                    .img-background img{
                        width: auto;
                        height: 100%;
                    }
                    
                    .logo-pigijo {
                        text-align: center;
                    }
                    
                    .logo-pigijo img {
                        width: 80%;
                    }
                    
                    .logo-idx {
                        text-align: center;
                    }
                    
                    .logo-idx img {
                        width: 60%;
                    }
                    
                    .logo-sf {
                        text-align: right;
                    }
                    
                    .logo-sf img {
                        width: 40%;
                    }
                    
                    .header span {
                        font-family: roboto-bold;
                        font-size: 27px;
                        color: #707070;
                    }
                    
                    .header p {
                        font-family: roboto;
                        font-size: 12px;
                        color: #898989;
                        margin-top: 10px;
                    }
                    
                    .forms {
                        margin-top: 30px;
                    }
                    
                    .forms .row {
                        margin-top: 10px;
                    }
                    
                    .submit-button {
                        text-align: right;
                    }
                    
                    .btn-submit {
                        background-color: #ff7e10 !important;
                        border-radius: 24px !important;
                        color: white;
                        font-size: small;
                        width: 120px;
                        margin-bottom : 20px;
                    }

                    @media (max-width: 351px) {
                        .img-background {
                           display: none;
                        }
                    }

                    @media (min-width: 352px) and (max-width: 575px) {
                        .img-background {
                            display: none;
                         }
                    }

                    @media (min-width: 576px) and (max-width: 767px) {
                        .img-background {
                            display: none;
                         }
                    }

                    @media (min-width: 768px) and (max-width: 991px) {
                        .img-background {
                            display: none;
                         }
                    }

                    @media (min-width: 992px) and (max-width: 1199px) {
                        .img-background {
                            display: none;
                         }
                    }
                `}</style>
            </>
        );
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
)(Imitokohotomotif);