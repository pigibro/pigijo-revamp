import { compose } from 'redux';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import Error from './_error';
import { map, get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import numeral from '../utils/numeral';
import { Link, Router } from '../routes';
import PageHeader from '../components/page-header';
import Loading from '../components/home/loading-flashsale';
import InfiniteScroll from 'react-infinite-scroller';
import IconLink from "../components/shared/iconLink";
import React from 'react';
import { Glyphicon } from 'react-bootstrap';
import withAuth from '../_hoc/withAuth';
import { getActivities, getLocations, getDetailCategory, getTagList } from '../stores/actions';
import { slugify, urlImage, convertToRp } from '../utils/helpers';
import moment from 'moment';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import InputRange from "react-input-range";
import 'react-dates/initialize';
import { DayPickerRangeController, DateRangePicker } from 'react-dates';
import { MobileView } from "react-device-detect";

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      // backgroundRepeat: 'no-repeat'
      backgroundPosition: ' center center',
  }
}

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          data: [],
          total: 0,
          page: 1,
          isLoading: false,
          product_type:[],
          more: true,
          filter: '',
          sort_by: 'created_at|DESC',
          location: [],
          duration: [],
          startDate: null,
          endDate: null,
          city: null,
          province: null,
          area: null,
          schedule_type: [],
          minMax:{min: 0 , max: 25000000},
          quantity: 2,
          focusedInput: '',
          btnMobileActive: '',
          tags: [],
          tag: null
        }
    }
    static async getInitialProps({ query, store}) {
        const { slug } = query;
        let paramsSlug = slug;
        let errorCode = false;
        if(slug === "recommended"){
          paramsSlug = "local-experiences"
        }
        const resCategory = await store.dispatch(getDetailCategory(paramsSlug));
        errorCode = get(resCategory,'meta.code') !== 200 ? 404 : false;
        const category = get(resCategory,'data');
        return { errorCode, slug, category, query }
    }

    componentDidMount() {
      document.addEventListener('click', this.handleClickOutside, true);
      const { dispatch } = this.props;
      dispatch(getLocations()).then(result => {
        if (get(result, 'meta.code') === 200){
          this.setState({
            location: get(result, 'data')
          });
        }
      }); 
      dispatch(getTagList()).then(res => {
        if(get(res, 'meta.code') === 200) {
          let data = get(res, 'data') || []
          for(let i=0; i < data.length; i++){
            data[i].type = 'Tag'
          }
          this.setState({
            tags: data
          })
        }
        
      })
    }
    
    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
    }

    handleTypeFilter = (e) => {
      const { product_type } = this.state;
      let newState = product_type;
      if(product_type.indexOf(e.target.value) < 0){
        newState.push(e.target.value);
      }else{
        newState.splice(product_type.indexOf(e.target.value), 1);
      }
      this.setState({
        product_type: newState,
      });
    }
    setParams = (isRerender) => {
      const {category, slug, query} = this.props;
      const { page,startDate,endDate, quantity, city, area, province,minMax, schedule_type, tag } = this.state;
      let { sort_by } = this.state;

      if(sort_by === 'created_at|DESC' && slug === 'recommended'){
        sort_by = 'recommended|DESC';
      }
      let req = {
        'per_page': 16,
        'page':isRerender ? 1 : page,
        'person': quantity,
        'min_price': minMax.min,
        'max_price': minMax.max,
        'category_id': category.id,
        'sort_by': sort_by,
      };
      if(isRerender == false) {
        this.setState({
          tag: query.tags ? +query.tags[0] : tag
        })
        if (query.tags) {
          req = { ...req, ...{ tags: +query.tags[0] }}
        }
      }
      if(!isEmpty(startDate) && !isEmpty(endDate)){
        req = {...req, ...{start_date:moment(startDate).format('YYYY-MM-DD'), end_date:moment(endDate).format('YYYY-MM-DD')}}
      }
      if(city !== null){
        req = {...req, ...{city}}
      }
      if(province !== null){
        req = {...req, ...{province}}
      }
      if(area !== null){
        req = {...req, ...{area}}
      }
      if(schedule_type.length > 0){
        req = {...req, ...{schedule_type}}
      }

      let queries = {}
      if (tag !== null) {
        req = { ...req, ...{ tags: tag }}
        let det_Tag = this.state.tags.find(tags => tags.id === tag)
        queries.tags = `${det_Tag.id}-${det_Tag.name}`
      }
  
      isRerender ? Router.push({pathname: `/c/${slug}`, query: queries}) : null  
      return req;
    }

    getActivity = async(isRerender = false) => {
      if (this.state.isLoading) {
        return 'isLoading...';
      }
      const params = this.setParams(isRerender);
      const { dispatch } = this.props;
      const page = isRerender ? 0 : this.state.page;
      let dataTmp = !isRerender ? this.state.data : [];
      if(page > 0){
        this.setState({ isLoading: true, btnMobileActive: '' })
        await dispatch(getActivities(params)).then(result => {
          if (get(result, 'meta.code') === 200){
            this.setState({data: dataTmp.concat(get(result,'data')),page: page+1});
            this.setState({total: get(result,'pagination.total')});
            if(page === get(result,'pagination.last_page')){
              this.setState({more: false});
            }
          }
          this.setState({ isLoading: false })
        });
      }
    }

    loadMore = () => {
      this.getActivity(false);
    }
    
    changeShow = (filter) => {
      this.setState({filter});
    }

    handleClickOutside = event => {
      const location = ReactDOM.findDOMNode(this.refs.location);
      if (!location || !location.contains(event.target)) {
        const dates = ReactDOM.findDOMNode(this.refs.dates);
        if (!dates || !dates.contains(event.target)) {
          const guests = ReactDOM.findDOMNode(this.refs.guests);
          if (!guests || !guests.contains(event.target)) {
            const price = ReactDOM.findDOMNode(this.refs.price);
            if (!price || !price.contains(event.target)) {
              const duration = ReactDOM.findDOMNode(this.refs.duration);
              if (!duration || !duration.contains(event.target)) {
                const sort = ReactDOM.findDOMNode(this.refs.sort);
                if (!sort || !sort.contains(event.target)) {
                  const tag = ReactDOM.findDOMNode(this.refs.tag);
                  if (!tag || !tag.contains(event.target)) {
                    this.setState({ filter: '' });
                  }
                }
              }
            }
          }
        }
      }
    }

    handleLocation = (value) => {
      if(value.length > 0){
        if(value[0].type === 'Area'){
          this.setState({area: value[0].id, city: null, province: null, data: [], page: 1, more: true}, () => {
          });
        }else if (value[0].type === 'Kabupaten' || value[0].type === 'Kota'){
          this.setState({area: null, city: value[0].id, province: null, data: [], page: 1, more: true}, () => {
          });
        }else{
          this.setState({area: null, city: null, province: value[0].id, data: [], page: 1, more: true}, () => {
          });
        }
      }
    }

    handlePriceRange = (value) => {
      this.setState({ minPrice: value.min, maxPrice: value.max });
    }

    handleDuration = (e) => {
      const { schedule_type } = this.state;
      let newState = schedule_type;
      if(schedule_type.indexOf(e.target.value) < 0){
        newState.push(e.target.value);
      }else{
        newState.splice(schedule_type.indexOf(e.target.value), 1);
      }
      this.setState({
        schedule_type: newState,
        data: [], page:1, more: true 
      }, () => {
        this.getActivity(true);
      });
    }
    
    
    render(){
      const {slug, errorCode, category} = this.props;
      const { product_type, page,btnMobileActive,schedule_type, total, quantity, minMax, data, pagination, more, filter, location, startDate, endDate, focusedInput, tags } = this.state;
      const loopTo = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
      if(errorCode > 200){
        return (<Error statusCode={errorCode}/>);
      }
      return (
        <>
          <Head title={category.meta_title} description={category.meta_description} url={process.env.SITE_ROOT+'/c/'+slug}/>
          <PageHeader background={"/static/images/img05.jpg"} title={`Find the best ${category.name} on your next trip`} caption={category.short_description}/>
          <section className="main-box sticky">
            <div className="container">
              <ul id="header-tabs">
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'location'})}>Location</button>
                  <div ref='location' className={`p__filter ${filter === 'location' ? 'show' : 'hide'}`} >
                    <SearchAutoComplete
                      id="destination"
                      key={"select-single"}
                      index={0}
                      data={location || []}
                      placeholder="Select City"
                      labelKey="name"
                      onChange={this.handleLocation}
                      bsSize="large"
                    />
                  </div>
                </li>
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'tag'})}>Tags</button>
                  <div ref='tag' className={`p__filter ${filter === 'tag' ? 'show' : 'hide'}`} >
                    <SearchAutoComplete
                      id="tag"
                      key={"select-single-1"}
                      index={1}
                      data={tags || []}
                      placeholder="Select Tag"
                      labelKey="name"
                      onChange={e => this.setState({tag: e[0].id}, () => this.getActivity(true))}
                      bsSize="large"
                    />
                  </div>
                </li>
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'dates'})}>Dates</button>
                  <div ref='dates' className={`p__filter ${filter === 'dates' ? 'show' : 'hide'}`}>
                  {filter === 'dates' &&
                    <DayPickerRangeController
                      isOutsideRange={date => date.isBefore(moment())}
                      startDate={startDate}
                      endDate={endDate}
                      numberOfMonths={1}
                      onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate, data: [], page:1, more: true })}
                      onFocusChange={focusedInput => this.setState({ focusedInput: focusedInput || 'startDate'})}
                      focusedInput={focusedInput}
                    />
                  }
                  </div>
                </li>
                <li className="tabs-item">
                <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'guests'})}>Guests</button>
                  <div ref='guests' className={`p__filter ${filter === 'guests' ? 'show' : 'hide'}`}>
                    <ul className="ev-box-qty">
                      <li className={(quantity < 1 ? 'disabled-add-min-events ev-min' : 'ev-min')} onClick={() => this.setState({quantity: (quantity-1 < 1) ? 1 : quantity-1,data: [], page:1, more: true})}>
                        <a>
                          <Glyphicon glyph="minus" />
                        </a>
                      </li>
                      <li className="ev-quantity">
                        <input type="text" defaultValue={quantity} value={quantity} maxLength="3"></input>
                      </li>
                      <li className={(quantity > 7 ? 'disabled-add-min-events ev-add' : 'ev-add')}  onClick={() => this.setState({quantity: (quantity+1 > 7) ? 7 : quantity+1,data: [], page:1, more: true})}>
                      <a>
                        <Glyphicon glyph="plus" />
                      </a>
                      </li>
                    </ul>
                  </div>
                </li>
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'price'})}>Price</button>
                  <div ref='price' className={`p__filter ${filter === 'price' ? 'show' : 'hide'}`}>
                    <p>{convertToRp(minMax.min)} - {convertToRp(minMax.max)}</p>
                    <InputRange
                      formatLabel={(value) => convertToRp(value)}
                      draggableTrack
                      maxValue={25000000}
                      minValue={0}
                      value={minMax}
                      onChange={value => this.setState({ minMax: value, data: [], page:1, more: true })}
                      onChangeComplete={(e) => this.getActivity(true)} 
                    />
                  </div>
                </li>
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'duration'})}>Duration</button>
                  <div ref='duration' className={`p__filter ${filter === 'duration' ? 'show' : 'hide'}`}>
                    <div className="cnt-filter">
                      <div className="cnt-body">
                        <div className="form-group">
                          <div className="checkbox">
                            <input type="checkbox" id="s0112" name='type[]' defaultValue={3} checked={false} onChange={this.handleDuration} checked={schedule_type.indexOf("3") !== -1 ? true : false } />
                            <label htmlFor="s0112">Full Day</label>
                          </div>
                        </div>
                        <div className="form-group">
                          <div className="checkbox">
                            <input type="checkbox" id="s0113" name='type[]' defaultValue={2} checked={false} onChange={this.handleDuration} checked={schedule_type.indexOf("2") !== -1 ? true : false }  />
                            <label htmlFor="s0113">A Couple Of Hours</label>
                          </div>
                        </div>
                        <div className="form-group">
                          <div className="checkbox">
                            <input type="checkbox" id="s0114" name='type[]' defaultValue={1} checked={false} onChange={this.handleDuration} checked={schedule_type.indexOf("1") !== -1 ? true : false }  />
                            <label htmlFor="s0114">Multiple Days</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="tabs-item">
                  <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'sort'})}>Sort By</button>
                  <div ref='sort' className={`p__filter ${filter === 'sort' ? 'show' : 'hide'}`}>
                    <div className="sort-desktop" onClick={() => {this.setState({sort_by:'created_at|DESC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                      Newly listed
                    </div>
                    <div className="sort-desktop" onClick={() => {this.setState({sort_by:'price|DESC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                      Price: high to low
                    </div>
                    <div className="sort-desktop" onClick={() => {this.setState({sort_by:'price|ASC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                      Price: low to high
                    </div>
                    <div className="sort-desktop" onClick={() => {this.setState({sort_by:'schedule|ASC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                      Upcoming schedule
                    </div>
                    <div className="sort-desktop" onClick={() => {this.setState({sort_by:'recommended|DESC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                      Recommended
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </section>
          {
            btnMobileActive !== '' &&
            <MobileView>
            <div className="p__fitler--mobile-content">
              <a className="btn-close" onClick={e => {e.preventDefault; this.setState({btnMobileActive:''})}}><i className="ti-close"/></a>
              {
                btnMobileActive === 'filter' &&
                <>
                  <div className="form-group">
                    <span className="text-label">Location</span>
                    <SearchAutoComplete
                      id="destination-mobile"
                      key={"select-single"}
                      index={0}
                      data={location || []}
                      placeholder="Select City"
                      labelKey="name"
                      onChange={this.handleLocation}
                      bsSize="large"
                    />
                  </div>
                  <div className="form-group">
                    <span className="text-label">Dates</span>
                    <DateRangePicker
                      startDatePlaceholderText='Start'
                      endDatePlaceholderText='End'
                      startDate={startDate}
                      startDateId='startDateId'
                      endDate={endDate}
                      endDateId='endDateId'
                      onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate, data: [], page:1, more: true })}
                      focusedInput={focusedInput}
                      onFocusChange={focusedInput => this.setState({ focusedInput })}
                      displayFormat='DD MMM YYYY'
                      hideKeyboardShortcutsPanel
                      numberOfMonths={1}
                      withPortal={false}
                      minimumNights={0}
                    />
                  </div>
                  <div className="form-group">
                    <span className="text-label">Person</span>
                    <select
                      defaultValue={quantity}
                      onChange={e => this.setState({ quantity: e.target.value, data: [], page:1, more: true  },() => { this.getActivity(true)})}
                      className='form-control mb1'
                    >
                      <option value='1'>1 Person</option>
                      <option value='2'>2 Persons</option>
                      <option value='3'>3 Persons</option>
                      <option value='4'>4 Persons</option>
                      <option value='5'>5 Persons</option>
                      <option value='6'>6 Persons</option>
                      <option value='7'>7 Persons</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <span className="text-label">Price</span>
                    <p>{convertToRp(minMax.min)} - {convertToRp(minMax.max)}</p>
                    <div style={{padding: "0 3em"}}>
                    <InputRange
                      formatLabel={(value) => convertToRp(value)}
                      draggableTrack
                      maxValue={25000000}
                      minValue={0}
                      value={minMax}
                      onChange={value => this.setState({ minMax: value })}
                      onChangeComplete={(e) => this.getActivity(true)} 
                    />
                    </div>
                  </div>
                  <div className="form-group">
                    <span className="text-label">Durations</span>
                    <div className="checkbox">
                      <input type="checkbox" id="s0112" name='type[]' defaultValue={3} checked={false} onChange={this.handleDuration} checked={schedule_type.indexOf("3") !== -1 ? true : false } />
                      <label htmlFor="s0112">Full Day</label>
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="checkbox">
                      <input type="checkbox" id="s0113" name='type[]' defaultValue={2} checked={false} onChange={this.handleDuration} checked={schedule_type.indexOf("2") !== -1 ? true : false }  />
                      <label htmlFor="s0113">A Couple Of Hours</label>
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="checkbox">
                      <input type="checkbox" id="s0114" name='type[]' defaultValue={1} checked={false} onChange={this.handleDuration} checked={schedule_type.indexOf("1") !== -1 ? true : false }  />
                      <label htmlFor="s0114">Multiple Days</label>
                    </div>
                  </div>
                </>
              }
              {
                btnMobileActive === 'sort' &&
                <>
                  <div className="sort-mobile mt2" onClick={() => {this.setState({sort_by:'created_at|DESC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                    Newly listed
                  </div>
                  <div className="sort-mobile" onClick={() => {this.setState({sort_by:'price|DESC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                    Price: high to low
                  </div>
                  <div className="sort-mobile" onClick={() => {this.setState({sort_by:'price|ASC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                    Price: low to high
                  </div>
                  <div className="sort-mobile" onClick={() => {this.setState({sort_by:'schedule|ASC',data: [], page:1, more: true }, () => {this.getActivity(true)})}}>
                    Upcoming schedule
                  </div>
                </>
              }
            </div>
            </MobileView>
          }
          <section className="body-content mt2 ">
          <div className="container">
            <IconLink/>
          </div> 
        </section>
          {
            process.browser &&
            <MobileView>
            <section className="p__filter--mobile">
              <div className="btn-group">
                <button className="btn btn-sm btn-primary" onClick={() => this.setState({btnMobileActive: 'filter'})}>Filter</button>
                <button className="btn btn-sm btn-primary btn-o bg-white" onClick={() => this.setState({btnMobileActive: 'sort'})}>Sort</button>
              </div>
            </section>  
            </MobileView>
          }
          <section className="body-content mt2">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <h4 className="text-title">Found {total} in {category.name}</h4>
                </div>
                <div className="product-list">
                <InfiniteScroll pageStart={page} initialLoad loader={<div style={{padding:"0 .99em"}} key={'loading'}><Loading /></div>} useWindow={true} loadMore={this.loadMore} hasMore={more} threshold={1000}>
                {
                  map(data, (item, index) => (
                    <div className="col-md-3 col-sm-6 col-xs-6 p-item" key={`p-${index}`}>
                      <Link route="activity" params={{slug: item.slug}}>
                      <a>
                      <div className="plan-item sr-btm">
                        <div className="box-img plan-img">
                          <div className="thumb" style={_imgStyle(urlImage(item.image_path+'/'+item.image_filename))}/>
                          <div className="box-img-top">
                            {!isEmpty(item.city) && <span><i className="ti-pin"/> {item.city.name}</span> }
                          </div>
                          <div className="box-img-bottom">
                            {item.discount > 0 && <p className="p_discount">{Math.round(item.discount)}% OFF</p>}
                          </div>
                        </div>
                        <div className="plan-info">
                          <ul>
                            <li className="title"><h3 title={item.name}>{item.name}</h3></li>
                            <li className="min-person">
                              <p className="text-label">Min {item.min_person} person{item.min_person === 1? '' : 's'}</p>
                            </li>
                            <li className="price">
                              {item.discount > 0 && <p className="price-before"><del>Rp {numeral(parseInt(item.price_before,10)).format('0,0')}</del></p>}
                              <p>
                                <span  className="price-latest">
                                { item.product_category !== 'Local Assistants'?
                                  <><b className="text-primary">Rp {numeral(parseInt(item.price,10)).format('0,0')}</b><span className="text-person">/person</span></>:
                                  <><b className="text-primary">Rp {numeral(parseInt(item.price,10)).format('0,0')}</b><span className="text-person">/day</span></>
                                }
                                </span>
                                <span className={ moment(item.upcomming_schedule).format("YYYY-MM-DD") === moment().format('YYYY-MM-DD') || moment(item.upcomming_schedule).format("YYYY-MM-DD") === moment().add(1, 'days').format('YYYY-MM-DD') ? 'notice text-success' : 'notice text-info'}>
                                  { moment(item.upcomming_schedule).format("YYYY-MM-DD") === moment().format('YYYY-MM-DD') &&
                                    'Available Today'
                                  }
                                  { moment(item.upcomming_schedule).format("YYYY-MM-DD") === moment().add(1, 'days').format('YYYY-MM-DD') &&
                                    'Available Tommorow'
                                  }
                                  { moment().add(1,'days').valueOf() < moment(item.upcomming_schedule).valueOf() &&
                                    `Available from ${moment(item.upcomming_schedule).format("DD MMMM YYYY")}`
                                  }
                                </span>
                              </p>
                            </li>
                          </ul>
                        </div>
                      </div>{/*plan-item-end*/}
                      </a>
                      </Link>
                    </div>
                  ))
                }
                </InfiniteScroll>
                </div>
              </div>
            </div>
          </section>
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(Category);