import React, { Component } from 'react'
import { get, isEmpty, concat } from 'lodash';
import { compose } from "redux";
import { connect } from 'react-redux';
import { Link, Router } from '../routes';
import { setErrorMessage, setSuccessMessage } from '../stores/actions'
import withAuth from '../_hoc/withAuth';
import Head from '../components/head';
import {apiCall, apiUrl} from '../services/request'
import { Modal, Dropdown, DropdownButton, MenuItem} from "react-bootstrap";
import PageHeader from '../components/page-header';
import { SingleDatePicker } from "react-dates";
import 'react-dates/initialize';
import axios from 'axios'

class Imipusatform extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            check_in: null,
            check_in_focused: false,
            check_out: null,
            check_out_focused: false,
            mandat_number: '',
            kta: '',
            nama: '',
            phone: '',
            prov: 1,
            jabatan: '',
            email: '',
            gender: 'Mr',
            provinces: [],
            asosiasi: ': / /SM-Rakernas/Munaslub IMI 2019/XII/2019',
            imi: ': /IMI - /SM-Rakernas/Munaslub IMI 2019/A/XII/2019',
            mandaterror: '',
            emailerror: '',
            phonerror: '',
            ktaerror: '',
            status: 'ACTIVE'
         };
    }

    static async getInitialProps({ query, store }) {
        const { type } = query;
        return { type }
    }

    componentDidMount() {
        this.getProvinces()
        
    }

    getProvinces = async () => {
        const dataReq = {
            method: "GET",
            url: apiUrl + `/v2/location/province`,
        };
        const res = await this.props.dispatch(apiCall(dataReq));
        
        if (get(res, 'meta.code') === 200) {
            this.setState({provinces: get(res, 'data')})
        }
    }

    Regist = async () => {
        let { dispatch, type } = this.props
        let {mandat_number, kta, nama, phone, prov, jabatan, email, check_in, check_out, gender, mandaterror, emailerror, phonerror} = this.state    
        if ( mandat_number == ''||
            kta == ''||
            nama == ''|| 
            phone == ''|| 
            prov == ''|| 
            jabatan == ''|| 
            email == '' || 
            gender == '' || 
            check_in == null || 
            check_out, gender == null || 
            phonerror !== '') {
            let res = await dispatch(setErrorMessage('Semua field wajib diisi'))            
        } else {
            this.sendForm()
        }
    }

    sendForm = async () => {
        let { dispatch, type } = this.props
        let {mandat_number, kta, nama, phone, prov, jabatan, email, check_in, check_out, gender} = this.state
        let names = nama.split(" ")
        let last = names.pop()        
        let data = {
            type: 'imi',
            no_mandat: mandat_number,
            no_kta: kta,
            firstname: `${names.length >= 1 ? names.join(" ") : nama}`,
            lastname: last,
            phone,
            province: prov,
            position: jabatan,
            email,
            check_in,
            check_out,
            gender
        }

        const dataReq = {
            method: "POST",
            url: `https://api-registration.pigijo.com/user/register`,
            data: {
                data
            }
        };

        const res = await dispatch(apiCall(dataReq));
        if(get(res, 'meta.code') === 200) {
            let res = await dispatch(setSuccessMessage('Registrasi berhasil, silahkan periksa email untuk verifikasi')) 
            Router.push({pathname: '/imi-registration/thankyou',})
            
        } else if(get(res, 'meta.code') === 400){
            await dispatch(setErrorMessage(get(res, 'meta.message'))) 
        }
    }

    changeMandat = (val) => {
        let { imi, asosiasi } = this.state
        let {type} = this.props
        if(val.length > 3 ) {
            let value = val.slice(3)
            let valSplit = value.split(" ")
            let valJoin = valSplit.join("")
            let valLower = valJoin.toLowerCase()

            if(type === 'asosiasi') {
                let asSplit = asosiasi.split(" ")
                let asJoin = asSplit.join("")
                let asLower = asJoin.toLowerCase()
                
                
                if(valLower !== asLower) {
                    this.setState({mandat_number : val, mandaterror: 'Format nomor surat mandat tidak valid'})
                } else {
                    this.setState({mandat_number: val, mandaterror: ''})
                }
            } else {
                let imiSplit = imi.split(" ")
                let imiJoin = imiSplit.join("")
                let imiLower = imiJoin.toLowerCase()
                if(valLower !== imiLower) {
                    this.setState({mandat_number : val, mandaterror: 'Format nomor surat mandat tidak valid'})
                } else {
                    this.setState({mandat_number: val, mandaterror: ''})
                }
            }
        } else {
            this.setState({mandat_number: val, mandaterror: ''})
        }
    }

    changeEmail = (val) => {
        let pattern = /[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g;
        let result = pattern.test(val)
        if(result) {
            this.setState({ email: val, emailerror: ''})
        } else {
            this.setState({ email: val, emailerror: 'Email tidak valid'})
        }
    }

    changePhone = (val) => {
        let patternNumber = /^[0-9\b]+$/;
        let NumCheck = patternNumber.test(+val)
        let string = `${val}`
        if(string.length < 8 || string.length > 13  || string[0] !== '0' || !NumCheck) {
            this.setState({phone: val, phonerror: 'Nomor telfon tidak valid'})
        } else {
            this.setState({phone: val, phonerror: ''})
        }
    }

    changekta = (val) => {
        this.setState({kta: val})
        axios.post('https://crm.imi.id/switching/crisa/imi/customer/inquiry/status/membership', {
            memberCardId: val
        }, {
            headers: {
                'content-type': 'application/json',
                Authorization: 'DA01 apiKeyImi:R3qzIsOIp1KkSNkn/3jbzVLl1l8=',
            }
        })
        .then((res) => {
            this.setState({ktaerror: '', nama: res.customerName, email: customerEmail, status: res.customerMembershipStatus})            
        })
        .catch(err => {
            if(err.status === 400) {
                this.setState({ktaerror: 'Nomor KTA tidak valid'})
            } 
            
        })
    }

    render() {
        let { mandat_number, kta, nama, phone, provinces, jabatan, email, prov, gender, mandaterror, emailerror, phonerror, ktaerror} = this.state
        return (
            <div>
                <Head/>
                <PageHeader background={'/static/images/img01.jpg'} title={`IMI ${this.props.type}`} caption="Data Pribadi"/>
                <div className="container" style={{paddingTop: '3em'}}>
                    <div className="row align-items-start" style={{paddingBottom: '2em'}}>
                        <p className='text-center'><img src={require("../static/images/logo.png")} className="img-fluid" width="30%"/></p>
                    </div>

                    <div className="row align-items-center mb-2" style={{marginBottom: '1em'}}>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>No Surat Mandat :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={mandat_number} onChange={e => this.setState({mandat_number: e.target.value})}/>
                            <small style={{color: 'red'}}>{mandaterror}</small>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{marginBottom: '1em'}}>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>No. KTA :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={kta} onChange={e => this.changekta(e.target.value)}/>
                            <small style={{color: 'red'}}>{ktaerror}</small>
                        </div>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>Status :</span>
                        </div>
                        <div className="col-md-3">
                            <span className="text-uppercase font-weight-bold" style={{fontSize: '13px'}}>{this.state.status}</span>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{marginBottom: '1em'}}>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>Nama Lengkap :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={nama} onChange={e => this.setState({nama: e.target.value})}/>
                        </div>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>Jabatan :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={jabatan} onChange={e => this.setState({jabatan: e.target.value})}/>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{marginBottom: '1em'}}>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>No Ponsel :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" placeholder="08xxxxxxxxxx" className="form-control form-control-sm" value={phone} onChange={e => this.changePhone(e.target.value)}/>
                            <small style={{color: 'red'}}>{phonerror}</small>
                        </div>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>Email :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={email} onChange={e => this.setState({email: e.target.value})}/>
                            <small style={{color: 'red'}}>{emailerror}</small>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{marginBottom: '1em'}}>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>Propinsi :</span>
                        </div>
                        <div className="col-md-3">
                            <select  className="form-control form-control-sm" value={prov} onChange={e => this.setState({prov: e.target.value})}>
                                {
                                    provinces.length > 0 ? provinces.map((province, id) => (
                                    <option value={province.id} key={id}>{province.name}</option>
                                    )) : <option>Provinsi</option>
                                }
                            </select>                        
                        </div>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>Gender :</span>
                        </div>
                        <div className="col-md-3">
                            <select  className="form-control form-control-sm" value={gender} onChange={e => this.setState({gender: e.target.value})}>
                                <option value="Mr">Mr.</option>
                                <option value="Mrs">Mrs.</option>
                                <option value="Ms">Miss</option>
                            </select>                        
                        </div>
                    </div>
                    <div className="row align-items-center mb-3" style={{marginBottom: '1.5em'}}>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>Jadwal tiba/Check in :</span>
                        </div>
                        <div className="col-md-3">
                            <SingleDatePicker
                                numberOfMonths={1}
                                date={this.state.check_in}
                                onDateChange={date =>
                                this.setState({
                                    check_in: date,
                                })
                                }
                                focused={this.state.check_in_focused}
                                onFocusChange={({ focused }) => this.setState({ check_in_focused: focused })}
                                id="test"
                                placeholder=""
                                small
                            />
                        </div>
                        <div className="col-md-2">
                            <span style={{fontSize: '13px'}}>Jadwal pulang/Check out :</span>
                        </div>
                        <div className="col-md-3">
                            <SingleDatePicker
                                numberOfMonths={1}
                                date={this.state.check_out}
                                onDateChange={date =>
                                this.setState({
                                    check_out: date,
                                })
                                }
                                focused={this.state.check_out_focused}
                                onFocusChange={({ focused }) => this.setState({ check_out_focused: focused })}
                                id="test"
                                placeholder=""
                                small
                            />
                        </div>
                    </div>
                    <div className="row align-items-center mb-3" style={{marginBottom: '1.5em'}}>
                        <div className="col-md-10 text-right">
                            <button style={{width: '150px'}} type="button"
                                className="btn btn-primary text-uppercase font-weight-bold text-white" onClick={this.Regist}>daftar</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
  )(Imipusatform);