import React from "react";
import { compose } from "redux";
import withAuth from "../_hoc/withAuth";
import Headers from "../components/profile/_component/header";
import Content from "../components/profile/mybooking/details/content";
import { connect } from "react-redux";
import { getBookingDetail } from "../stores/actions/myBookingAction";
import { getCarListDefault } from "../stores/actions/rentCarActions";
import { IMAGE_URL } from "../stores/actionTypes";

class MyBooking extends React.Component {

	static getInitialProps({query}) {
    return {query}
	}
	
	componentWillMount() {
		this.props.dispatch(getBookingDetail(this.props.query.slug, this.props.token));
		this.props.dispatch(getCarListDefault(this.props.token));
	}
	render() {
		const { token, asPath, list, item, details, paging, isLoading, joToken } = this.props;

		return (
			<div>
				<Headers />
				<Content 
					joToken={joToken}
					token={token}
					path={asPath}
					list={list}
					item={item}
					details={details}
					paging={paging}
					url={IMAGE_URL}
					isLoading={isLoading}
				/>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	list: state.myBooking.list,
	details: state.myBooking.details,
	item: state.myBooking.item,
  paging: state.myBooking.paging,
  isLoading: state.myBooking.isLoading
});

export default compose(
	connect(mapStateToProps),
	withAuth(["PRIVATE"]),
)(MyBooking);
