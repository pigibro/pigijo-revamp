import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

class BecomePartner extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Head title={"Become a Partner"} description={""} url={process.env.SITE_ROOT + '/about-us'} />
                <PageHeader background={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/jumbotron.png"} title={`Mengapa Bermitra dengan Pigijo`} caption="Kami hadir untuk menyebarkan keindahan dan rasa cinta untuk Indonesia, kepada dunia." />

                <section className="content-wrap bg-content-home">
                    <div className="container display1">
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12" style={{ marginTop: "5%" }}>
                                <p className="d1-header">Nilai Kami</p>
                                <p className="d1-text">Kami bermitra dengan pelaku bisnis lokal di bidang travelling,
                                    untuk menyediakan segala kebutuhan para wisatawan local
                                    dan mancanegara.
                                </p>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <img style={{ width: '90%' }} alt="Pigijo" src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/display1.jpg' />
                            </div>
                        </div>
                    </div>
                </section>

                <section className="content-wrap bg-content-home">
                    <div className="container display2">
                        <div className="row">
                            <div className="col-lg-12">
                                <p className="d2-header">Mengapa Bermitra dengan Pigijo?</p>
                            </div>
                        </div>
                        <div className="row d2-icon">
                            <div className="col-lg-1 col-md-1">
                            </div>
                            <div className="col-lg-2 col-md-2 col-sm-5">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/free.svg' />
                                <p className="d2-icon-header">Gratis</p>
                                <p className="d2-icon-text">Tidak ada biaya untuk
                                    memulai bisnis di market
                                    place Pigijo.
                                </p>
                            </div>
                            <div className="col-lg-2 col-md-2 col-sm-5">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/photographer.svg' />
                                <p className="d2-icon-header">Wisatawan Lokal Dan Mancanegara</p>
                                <p className="d2-icon-text">Pigijo adalah sumber referensi wisatawan lokal ataupun
                                    mancanegara untuk merencanakan perjalanan mereka.</p>
                            </div>
                            <div className="col-lg-2 col-md-2 col-sm-5">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/suitcase.svg' />
                                <p className="d2-icon-header">Promosi yang cerdas, Hemat dan tepat</p>
                                <p className="d2-icon-text">Pigijo akan mempromosikan
                                    produk Anda kepada wisatawan yang membutuhkan,
                                    dengan sistem komisi yang ringan.
                                </p>
                            </div>
                            <div className="col-lg-2 col-md-2 col-sm-5">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/flyers.svg' />
                                <p className="d2-icon-header">Menyediakan Berbagai Kebutuhan Traveling</p>
                                <p className="d2-icon-text">Anda dapat menjadi supllier
                                    Pigijo sebagai penyedia kebutuhan traveling bagi wisatawan
                                    domestik dan mancanegara.
                                </p>
                            </div>
                            <div className="col-lg-2 col-md-2 col-sm-5">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/fitur pendukung.svg' />
                                <p className="d2-icon-header">Banyak Fitur Pendukung</p>
                                <p className="d2-icon-text">Terdapat banyak fitur pendukung yang dapat memudahkan
                                    pengembangan bisnis Anda, seperti pilihan pembayaran yang beragam,
                                    dashboard supplier yang mudah.
                                </p>
                            </div>
                            <div className="col-lg-1 col-md-1">
                            </div>
                        </div>
                    </div>
                </section>

                <div className="display3">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <p className="d3-header">Mitra Pigijo</p>
                            </div>
                        </div>
                        <div className="row d3-icon">
                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/homepage.svg' />
                                <p className="d3-icon-header">Buka Home Page Pigijo</p>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/become.svg' />
                                <p className="d3-icon-header">Klik Become a Partner</p>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/daftar akun.svg' />
                                <p className="d3-icon-header">Daftar Akun Sesuai Jenis Partner</p>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/tampilkan.svg' />
                                <p className="d3-icon-header">Tampilkan produk terbaikmu!</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="display4">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <p className="d4-header">Daftar Sebagai Partner Pigijo</p>
                            </div>
                        </div>
                    </div>
                    <div className="row d4-icon">
                        <div className="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        </div>
                        <div className="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/loca_exp.svg' />
                            <p className="d4-icon-header"><Link to="/localexperiences"><a> Local Experience </a></Link></p>
                        </div>
                        <div className="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/rent_car.svg' />
                            <p className="d4-icon-header"><Link to="/transportation"><a> Rent Car </a></Link></p>
                        </div>
                        <div className="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/hotel.svg' />
                            <p className="d4-icon-header"><Link to="/homestay"><a> Homestay </a></Link></p>
                        </div>
                        <div className="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/travel_ass.svg' />
                            <p className="d4-icon-header"><Link to="/travel-assistants"><a> Travel Assistant </a></Link></p>
                        </div>
                        <div className="col-lg-2 col-md-2 col-sm-12">
                        </div>
                    </div>
                </div>
                <section className="content-wrap bg-content-home">
                    <div className="container">
                        <div className="col-md-4 col-md-offset-4 mt2 mb2">
                            <a href="https://partner.pigijo.com/login" rel='noopener noreferrer' target="_blank" className="btn btn-primary btn-block">Become a Partner Now</a>
                        </div>
                    </div>
                </section>
                <style jsx>{`
                    .container.display1 {
                        padding: 50px 100px;
                        margin-top: -5px;
                        background-color: #fcfcfc;
                        width: 100%;
                    }
                                    
                    .display1 p {
                        text-align: right;
                    }
                                    
                    .d1-header {
                        font-family: roboto-bold;
                        color: #2add84;
                        font-size: 20px;
                    }
                                    
                    .d1-text {
                        font-family: roboto;
                        font-size: 16px;
                    }
                                    
                    .container.display2 {
                        width: 100%;
                        background-color: #2add84;
                        margin: -12px 0px;
                        padding: 50px 100px;
                    }
                                    
                    .d2-header {
                        text-align: center;
                        font-family: roboto;
                        color: white;
                        font-size: 30px;
                        margin-bottom: 40px;
                    }
                                    
                    .d2-icon {
                        text-align: left;
                    }
                                    
                    .d2-icon img {
                        width: 30%;
                    }
                                    
                    .d2-icon-header {
                        font-family: roboto-bold;
                        color: white;
                        font-size: 15px;
                        margin-top: 10px;
                    }
                                    
                    .d2-icon-text {
                        font-family: roboto;
                        color: white;
                        font-size: 15px;
                    }
                                    
                    .display3 {
                        background-color: #fcfcfc;
                        padding: 50px 100px;
                    }
                                    
                    .d3-header {
                        text-align: center;
                        font-family: roboto;
                        color: #2add84;
                        font-size: 30px;
                        margin-bottom: 40px;
                    }
                                    
                    .d3-icon {
                        text-align: center;
                    }
                                    
                    .d3-icon img {
                        width: 80%;
                    }
                                    
                    .d3-icon-header {
                        font-family: roboto;
                        color: #2add84;
                        font-size: 15px;
                        margin-top: 10px;
                    }
                                    
                    .display4 {
                        background-color: #f3f3f3;
                        padding: 50px 100px;
                    }
                                    
                    .d4-header {
                        text-align: center;
                        font-family: roboto;
                        color: #595a5a;
                        font-size: 30px;
                        margin-bottom: 40px;
                    }
                                    
                    .d4-icon {
                        text-align: center;
                    }
                                    
                    .d4-icon img {
                        width: 60%;
                    }
                                    
                    .d4-icon-header {
                        font-family: roboto;
                        color: #595a5a;
                        font-size: 15px;
                        margin-top: 10px;
                    }

                    @media (min-width: 0px) and (max-width: 575px) {                                   
                        .display1 p {
                            text-align: left;
                        }
                                    
                        .d1-header {
                            font-family: roboto-bold;
                            color: #2add84;
                            font-size: 18px;
                        }
                                    
                        .d1-text {
                            font-family: roboto;
                            font-size: 14px;
                        }
                                   
                        .d2-header {
                            font-size: 23px;
                        }

                        .d2-icon {
                            text-align: center;
                        }
                                    
                        .d2-icon img {
                            width: 30%;
                        }
                                
                        .d3-header {
                            font-size: 23px;
                        }

                        .d3-icon img {
                            margin-top:20px;
                            width: 100%;
                        }
                               
                        .d4-header {
                            font-size: 23px;
                        }

                        .d4-icon img {
                            margin-top:20px;
                            width: 40%;
                        }
                    }

                    @media(min-width: 576px) and (max-width: 767px) {
                        .display1 p {
                            text-align: left;
                        }
                                    
                        .d1-header {
                            font-family: roboto-bold;
                            color: #2add84;
                            font-size: 18px;
                        }
                                    
                        .d1-text {
                            font-family: roboto;
                            font-size: 14px;
                        }
                                   
                        .d2-header {
                            font-size: 23px;
                        }

                        .d2-icon {
                            text-align: center;
                        }
                                    
                        .d2-icon img {
                            width: 10%;
                        }
                              
                        .d3-header {
                            font-size: 23px;
                        }

                        .d3-icon img {
                            width: 20%;
                        }
                            
                        .d4-header {
                            font-size: 23px;
                        }

                        .d4-icon img {
                            width: 20%;
                        }
                    }

                    @media(min-width: 768px) and (max-width: 1024px) {
                        .header1 {
                            margin-left: 10%;
                            font-size: 45px;
                        }
                                    
                        .header2 {
                            margin-left: 10%;
                            font-size: 45px;
                        }
                                    
                        .header3 {
                            margin-left: 10.5%;
                            font-size: 15px;
                        }
                                    
                        .d1-header {
                            font-size: 17px;
                        }
                                    
                        .d1-text {
                            font-size: 14px;
                        }
                                
                        .d2-header {
                            font-size: 23px;
                        }

                        .d2-icon {
                            text-align: center;
                        }
                                    
                        .d2-icon img {
                            width: 20%;
                        }
                        
                        .d3-header {
                            font-size: 23px;
                        }
                        
                        .d3-icon img {
                            width: 40%;
                        }
                                
                        .d4-header {
                            font-size: 23px;
                        }

                        .d4-icon img {
                            width: 40%;
                        }
                    }
                `}</style>
            </React.Fragment >
        )
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
)(BecomePartner);
