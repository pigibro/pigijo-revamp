import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
      opacity: 0.8,
  }
}

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          product_type:[]
        }
    }

    static async getInitialProps({ query, store}) {
        const { slug, child } = query;
        return {slug: slug, child: child }
    }
    
    handleTypeFilter = (e) => {
      const { product_type } = this.state;
      let newState = product_type;
      if(product_type.indexOf(e.target.value) < 0){
        newState.push(e.target.value);
      }else{
        newState.splice(product_type.indexOf(e.target.value), 1);
      }
      this.setState({
        product_type: newState,
      });
    }

    
    
    render(){
      const {slug, child} = this.props;
      const { product_type } = this.state;
      return (
        <>
          <Head title={"Eu nostrud amet esse enim est esse commodo reprehenderit."} description={"Culpa tempor eu elit duis eu occaecat culpa culpa. Commodo anim commodo occaecat do consequat consequat Lorem laborum qui duis reprehenderit nisi Lorem ullamco. Dolore cupidatat proident esse irure cillum deserunt. Sit et tempor sint duis occaecat ea culpa."} url={process.env.SITE_ROOT+'/p/1180-showcase-indonesia-full-pass-early-bird'}/>
          <PageHeader background={"/static/images/img05.jpg"} title={isEmpty(child) ? slug : `${slug} ${child}`} caption="Culpa ad amet non aute do sunt velit veniam et."/>
          <section className="body-content mt2">
            <div className="container">
              <div className="row">
                  <aside className="col-lg-3 col-md-3 col-sm-12 sidebar">
                      <div className="panel">
                        <div className="panel-body">
                          <div className="cnt-filter">
                            <p className="text-title">Type</p>
                            <div className="cnt-body">
                              <div className="form-group">
                                <div className="checkbox">
                                  <input type="checkbox" id="s0112" name='type[]' value={'open'} checked={false} onChange={this.handleTypeFilter} checked={product_type.indexOf('open') !== -1 ? true : false } />
                                  <label htmlFor="s0112">Open</label>
                                </div>
                              </div>
                              <div className="form-group">
                                <div className="checkbox">
                                  <input type="checkbox" id="s0113" name='type[]' value={'private'} checked={false} onChange={this.handleTypeFilter}  checked={product_type.indexOf('private') !== -1 ? true : false } />
                                  <label htmlFor="s0113">Private</label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="cnt-filter">
                            <p className="text-title">Location</p>
                            <div className="cnt-body"></div>
                          </div>
                          <div className="cnt-filter">
                            <p className="text-title">Location</p>
                            <div className="cnt-body"></div>
                          </div>
                        </div>
                      </div>
                  </aside>
                  <div className="col-lg-9 col-md-3 col-sm-12">
                  <section className="header-content">
                      <nav className="nav-list-header">
                        <ul className="list-unstyled scrollmenu-x">
                          <li><Link to={`/c/${slug}/expo`} ><a style={_imgStyle("https://michaeltay.files.wordpress.com/2010/04/encounter-button.jpg")}>Expo</a></Link></li>
                          <li><Link to={`/c/${slug}/concert`} ><a style={_imgStyle("https://michaeltay.files.wordpress.com/2010/04/encounter-button.jpg")}>Concert</a></Link></li>
                          <li><Link to={`/c/${slug}/festival`} ><a style={_imgStyle("https://michaeltay.files.wordpress.com/2010/04/encounter-button.jpg")}>Festival</a></Link></li>
                          <li><Link to={`/c/${slug}/work-shop`} ><a style={_imgStyle("https://michaeltay.files.wordpress.com/2010/04/encounter-button.jpg")}>Work shop</a></Link></li>
                          <li><Link to={`/c/${slug}/competition`} ><a style={_imgStyle("https://michaeltay.files.wordpress.com/2010/04/encounter-button.jpg")}>Competition</a></Link></li>
                          <li><Link to={`/c/${slug}/pop-culture`} ><a style={_imgStyle("https://michaeltay.files.wordpress.com/2010/04/encounter-button.jpg")}>Pop Culture</a></Link></li>
                          <li><Link to={`/c/${slug}/shopping`} ><a style={_imgStyle("https://michaeltay.files.wordpress.com/2010/04/encounter-button.jpg")}>Shopping</a></Link></li>
                        </ul>
                      </nav>
                  </section>
                  </div>
              </div>
            </div>
          </section>
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(Category);