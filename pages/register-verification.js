import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import Loading from '../components/shared/loading';
import React from 'react';
import withAuth from '../_hoc/withAuth';
import { verify, resendVerification } from '../stores/actions';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
      opacity: 0.8,
  }
}

class RegisterVerification extends React.Component {
    static async getInitialProps({ query }) {
      // const { verifyToken } = query;
      return { query }
    }
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            error: '',
            success: '',
            isLoading: true,
            isVerified: false,
        }
        this.handleOnSubmit = this.handleOnSubmit.bind(this)
    }
    componentDidMount(){
      const {query} = this.props;
      this.props.dispatch(verify(query.verifyToken)).then((result) => {
        if(get(result,'meta.code') === 200){
          this.setState({success: get(result,'meta.message')});
          this.setState({error: ''});
        }else{
          this.setState({error: get(result,'meta.message')});
          this.setState({success: ''});
        }
        this.setState({isLoading: false});
      });
    }
    handleOnSubmit = (e) => {
        e.preventDefault()
        this.setState({isLoading: true});
        this.props.dispatch(resendVerification({email: this.state.email})).then((result) => {
          if(get(result,'meta.code') === 200){
            this.setState({success: get(result,'meta.message')});
            this.setState({error: ''});
          }else{
            this.setState({error: get(result,'meta.message')});
            this.setState({success: ''});
          }
          this.setState({isLoading: false});
        });
    }

    render(){
      const {isLoading, isVerified, error, success} = this.state;
      return (
        <>
          <Head title={"Verification Your Email"}/>
          <PageHeader background={"/static/images/img05.jpg"} title={`Verification Your Email`}/>
          <section className="content-wrap">
              <div className="container">
                <div className="row">
                  <div className="col-md-6 col-md-offset-3">
                    <p>Please verify your email address to improve our service for use this website.</p>
                    {
                      !isLoading &&
                      <div className='panel panel-bordered panel-primary sr-btm mt2'>
                        <div className='panel-body'>
                          {error !== '' &&
                            <div className='text-danger error-text' style={{marginTop: '1em', marginBottom: '1em', fontSize: '1.8em', textAlign: 'center'}}>
                              {error}
                            </div>
                          }
                          {success !== '' &&
                            <div className='text-success success-text' style={{marginTop: '1em', marginBottom: '1em', fontSize: '1.8em', textAlign: 'center'}}>
                              {success}
                            </div>
                          }
                          {
                            error !== '' && 
                            <form onSubmit={this.handleOnSubmit}>
                              <div className="form-group">
                                  <span className="text-label">Email Address</span>
                                  <input className="form-control" type="email" pattern='[^@]+@[^@]+\.[a-zA-Z]{2,6}' placeholder="Your email address" value={this.state.email} onChange={e => this.setState({email:e.target.value})} required/>
                              </div>
                              <button className="btn btn-block btn-primary" type="submit">Resend Email Verification</button>
                            </form>
                          }
                          
                        </div>
                      </div>
                    }
                  </div>
                </div>
              </div>
          </section>
          {
            isLoading && <Loading/> 
          }
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(RegisterVerification);