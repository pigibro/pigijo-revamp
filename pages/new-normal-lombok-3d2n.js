import React, { Component } from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';

class Preorder extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <Head title={"Explore Belitung 3D2N"} description={""} url={process.env.SITE_ROOT + '/wisata-sehat-pulau-harapan'} />
                {/* <PageHeader
                    background={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/banner/preorder-book.jpg"}
                    title="Virtual Tour Borobudur"
                    caption="Registrasi sekarang dan ikuti virtual tour borobudur" /> */}
                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/10/explore-belitung-3d2n.jpg' width='100%' />
                <>
                    <div style={{ paddingTop: '2em', marginBottom: '2em' }}>
                        <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfSx2GR-1sN_g4_1UJs-CJdaEsEB3i31f1FdC7FmSFkmJeNTA/viewform?embedded=true" width="100%" height="1352" frameBorder="0" marginHeight="0" marginWidth="0">Loading…</iframe>
                    </div>
                </>
            </React.Fragment>
        );
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
)(Preorder);