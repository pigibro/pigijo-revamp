import { compose } from 'redux'
import { connect } from 'react-redux'
import Error from './_error'
import { get, isEmpty, merge, map, range, takeRight } from 'lodash'
import moment from "moment";
import Head from '../components/head'
import { slugify, urlImage, replaceEnter } from '../utils/helpers';
import { FormControl, FormGroup, InputGroup, Col, Panel, PanelGroup } from "react-bootstrap";
import React from 'react'
import withAuth from '../_hoc/withAuth'
import { getActivity } from '../stores/actions'
import Loading from "../components/shared/loading";
import PageHeader from '../components/page-header';
import Content from '../components/activity/content';
import Router from 'next/router';
import { Link } from '../routes'
import 'react-dates/initialize';
import { DayPickerRangeController } from 'react-dates';
import { getSchedules, storeCart, getCartList, createToken, praCheckout, setSuccessMessage, setErrorMessage, deleteCart } from '../stores/actions';
import numeral from '../utils/numeral';
import { setCookie } from '../utils/cookies';
import { isMobileOnly } from "react-device-detect";

const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: `100%`,
    backgroundRepeat: `no-repeat`
  }
}

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      selectedId: [],
      AllSelect: false
    }
  }
  checkout = () => {
    const { carts, dispatch } = this.props;
    if (!isEmpty(carts)) {
      this.setState({
        isLoading: true,
      });
      dispatch(praCheckout(carts.token)).then(booking => {

        if (get(booking, 'meta.code') === 200) {
          if (get(booking, 'data.status') === 'verified') {
            Router.push('/checkout');
          }
        }
        else {
          dispatch(setErrorMessage(get(booking, 'data')));
        }
        this.setState({
          isLoading: false,
        });

      });
    }
  }

  deleteCart = (id) => {
    const { carts, dispatch } = this.props;
    if (!isEmpty(carts)) {
      let tempId = this.state.selectedId
      if (id) { tempId.push(id) }
      this.setState({
        isLoading: true,
        selectedId: this.state.selectedId
      })
      let dataReq = {
        token: carts.token,
        cart_detail_id: tempId
      }
      dispatch(deleteCart(dataReq)).then(res => {
        if (get(res, 'meta.code') === 200) {
          dispatch(getCartList(carts.token, 'schedule|ASC'))
        } else {
          dispatch(setErrorMessage(get(res, 'data')));
        }
        this.setState({
          isLoading: false,
        });
      })
    }
  }

  _handleSelect = (id) => {
    event.preventDefault()
    let tempId = []
    if (this.state.selectedId.includes(id)) {
      map(this.state.selectedId, (item, index) => {
        item !== id ? tempId.push(item) : null
      })
    } else {
      tempId = [...this.state.selectedId, id]
    }


    this.setState({
      selectedId: tempId
    })
  }

  selectAll = () => {
    let tempId = []
    const { carts } = this.props;
    let selectStatus = true
    if (!this.state.AllSelect) {
      map(carts.details, (item, index) => {
        !this.state.selectedId.includes(item._id) ? tempId.push(item._id) : null
      })
    } else {
      selectStatus = false
    }
    this.setState({
      selectedId: tempId,
      AllSelect: selectStatus
    })
  }

  deleteAll = () => {
    event.preventDefault()
    if (this.state.AllSelect) { this.deleteCart({ delete: true }) }
  }

  _handleGoToProduct = (item) => {
    switch (item.category_slug) {
      case "places":
        Router.push(`/place/${slugify(`${item.product_id}-${item.product_name}`)}`);
        break;
      case "activity":
        Router.push(`/p/${slugify(`${item.product_id}-${item.product_name}`)}`);
        break;
      case "local-experiences":
        Router.push(`/p/${slugify(`${item.product_id}-${item.product_name}`)}`);
        break;
      case "local-assistants":
        Router.push(`/p/${slugify(`${item.product_id}-${item.product_name}`)}`);
        break;
      case "rent-car":
        Router.push(`/rent-car`);
        break;
      case "accommodations":
        Router.push(`/accommodation`);
        break;
      case "homestay":
        Router.push(`/accommodation`);
        break;
      default:
        Router.push('/')
    }
  }

  render() {
    const { carts } = this.props;
    const { isLoading, selectedId, AllSelect } = this.state;
    const { details } = carts;
    return (
      <>
        <Head title={"Plan"} description={"Checkout all your plan"} keyword={"Plan"} url={'nice'} />
        <PageHeader background={"/static/images/img05.jpg"} type={"Plan"} title={'Plan'} caption={'Checkout all your plan'} />
        <section id="content-activity" className="content-activity">
          <section id="description" className="bg-content-home">
            <div className="container">

            </div>
          </section>
          <section id="choose-package" className="bg-content-home">
            <div className="container">
              {
                !isEmpty(details) &&
                <section className="side-left choose-package side-mobile">
                  <div className="panel">
                    {
                      !isEmpty(details) &&
                      <div className="panel-body shadow cart-body mb1">
                        <div className="head-content">
                          <div className="head-all">
                            <input type="checkbox" name="checkboxG3" id="checkboxG3" checked={AllSelect} onChange={e => this.selectAll()} />
                            <label htmlFor="checkboxG3" className="css-label">Select All Plan</label>
                          </div>
                          <div className="head-delete">
                            <a href="" className="text-delete" onClick={this.deleteAll}>Delete</a>
                          </div>
                        </div>
                      </div>
                    }

                    {/* start cart */}
                    <div className="panel panel-info sr-btm">
                      {
                        map(details, (item, index) => (
                          <div key={`cart-${index}`}>
                            <div className="body-listitem">
                              <div className="cekitem">
                                <input type="checkbox" checked={selectedId.includes(item._id)} onChange={e => this._handleSelect(item._id)} name="checkboxG3" id={`item-${index}`} />
                                <label htmlFor={`item-${index}`} className="css-label"></label>
                                <div className="box-img">
                                  <div className="thumb" onClick={() => this._handleGoToProduct(item)} style={_imgStyle(urlImage(item.product_image, item.category_slug))}>
                                  </div>
                                </div>
                                <div className="product-detailcart">
                                  <h4 className="text-title">{item.product_name}</h4>
                                  <p className="text-label">{moment(item.start_date).format("lll")}
                                    {item.category_slug !== 'homestay' ?
                                      '' :
                                      <><span>, {item.qty_optional} room</span></>
                                    }
                                  </p>
                                  <h4 className="price">{item.total_paid > 0 ? `Rp ${numeral(parseInt(item.total_paid, 10)).format('0,0')}` : 'FREE'}</h4>
                                </div>
                              </div>
                              <div className="item-list">
                                <span className="ti-trash" onClick={() => this.deleteCart(item._id)}></span>
                                <div className="cart-chooser">
                                  {/* <button className="cart-chooser__amount-btn-min" type="button" ></button> */}
                                  <span className="passengers-chooser__amount-field">{item.qty}</span>
                                  {/* <button className="cart-chooser__amount-btn-plus" type="button" ></button> */}
                                  <div className="cart-text">{item.unit}</div>
                                </div>
                              </div>
                            </div>
                            <hr />
                          </div>
                        ))
                      }
                    </div>
                  </div>
                </section>
              }
              {
                isEmpty(details) &&
                <section className="choose-package">
                  <div className="panel1">
                    {/* start cart */}
                    <div className="panel panel-info sr-btm">
                      <div className="cart-null">
                        <img src="/static/images/addtocart.svg" className="img-null-cart" />
                        <div className="row">
                          <div className="col-md-12 ">
                            <Link route="/">
                              <a className="btn btn-block btn-o btn-primary text-center mb2 btn-cart">
                                Go to Homepage
                                  </a>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              }
              {
                !isEmpty(details) &&
                <section className="side-right1">
                  <div className="panel-body shadow summary-cart">
                    <div className="head-summary">
                      <label className="title-summary">Summary</label>
                    </div>
                    <div className="body-summary">
                      <div className="detail-summary">
                        <div className="title-summary">Total Price</div>
                        <div className="price-summary">Rp {numeral(parseInt(carts.total_paid, 10)).format('0,0')}</div>
                      </div>
                    </div>
                    {/* <div className="col-md-12 col-xs-7 cuppon-summary">
                        <div className="card-cuppon">
                          <div className="form-group">
                            <span className="text-label">Use Promo Code</span>
                            <input className="form-control" name="promo"  type="text"/>
                         </div>
                        </div>
                      </div> */}
                    {/* <a href="#choose-package" className="btn btn-primary btn-sm btn-block mt1">Make Itinerary</a> */}
                    {/* <a href="#choose-package" className="btn btn-check btn-sm btn-block mt1">Expl\ore More</a> */}
                    <button onClick={this.checkout} className="btn btn-check btn-sm btn-block mt1" disabled={isLoading || isEmpty(details) ? true : false}>Checkout</button>
                  </div>
                </section>
              }
            </div>
          </section>
          {
            isLoading &&
            <Loading />
          }
        </section>
      </>
    );
  }
}

const mapStateToProps = state => ({
  carts: state.cart.data,
})

export default compose(
  connect(mapStateToProps),
  withAuth(["PUBLIC"])
)(Cart);