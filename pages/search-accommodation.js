import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight, map } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import Router from 'next/router'
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';
import Pagination from "react-js-pagination";
import { FormControl, FormGroup, InputGroup, Col, Panel, PanelGroup } from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import { DateRangePicker } from "react-dates";
import 'react-dates/initialize';
import numberWithComma from '../utils/numberWithComma';
import { slugify, urlImage, convertToRp } from '../utils/helpers';
import { getEventDetail, getLocations } from '../stores/actions';
import moment from 'moment';
import 'moment/locale/id'
moment.locale('id');

class SearchAccommodation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            endDate: null,
            date : null,
            focusedInput: null,
            adult: 1,
            child: 0,
            infant: 0,
            passengerBoxes: false,
            location: [],
            location_type: '',
            selectlocation: '',
            startDateren: null,
            endDateren: null,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
        const { dispatch } = this.props;
        dispatch(getLocations()).then(result => {
          if (get(result, 'meta.code') === 200){
            this.setState({
              location: get(result, 'data')
            });
          }
        }); 
      }
    IncrementAdult = () => {
        this.setState({ adult: this.state.adult + 1 });
      }
      DecreaseAdult = () => {
        this.setState({ adult: this.state.adult - 1 });
      }
    
      IncrementChild = () => {
        this.setState({ child: this.state.child + 1 });
      }
      DecreaseChild = () => {
        this.setState({ child: this.state.child - 1 });
      }
    
      IncrementInfant = () => {
        this.setState({ infant: this.state.infant + 1 });
      }
      DecreaseInfant = () => {
        this.setState({ infant: this.state.infant - 1 });
      }

    handleDateChange = ({ startDate, endDate }) =>
    this.setState({ 
        startDate : startDate,
        startDateren : moment(startDate).format("YYYY-MM-DD"),
        endDate : endDate,
        endDateren : moment(endDate).format("YYYY-MM-DD")
    });

    onFocusChange = (focusedInput) => {
		this.setState({ focusedInput });
    };
    
    async handleSubmit(e) {
        try {
            e.preventDefault();
            let {startDateren, endDateren, selectlocation,  } = this.state
            const data = {
              // 'locationId' : selectlocation.id,
              // 'locationName' : selectlocation.name,
              'location' : selectlocation.name,
              'locationtype' : selectlocation.type,
              'startdates' : startDateren,
              'enddates' : endDateren,
            }
            Router.push({
                pathname : '/accommodation',
                query: data
            })
        }catch (error) {
        }
      }

    isOutsideRange = () => false;
    render() {
        const { startDaterent, endDaterent, location, focusedInput, passengerBoxes, selectlocation} = this.state;

        return (
            <React.Fragment>
                <Head 
                    title={"Eu nostrud amet esse enim est esse commodo reprehenderit."} 
                    description={"Culpa tempor eu elit duis eu occaecat culpa culpa. Commodo anim commodo occaecat do consequat consequat Lorem laborum qui duis reprehenderit nisi Lorem ullamco. Dolore cupidatat proident esse irure cillum deserunt. Sit et tempor sint duis occaecat ea culpa."} url={process.env.SITE_ROOT + '/p/1180-showcase-indonesia-full-pass-early-bird'} />
                <PageHeader
                background={"/static/images/img05.jpg"}
                title="Find comfort hotel"
                caption="Culpa ad amet non aute do sunt velit veniam et." />

                <section className="filter-rentcar" style={{background: 'white'}}>
                    <div className="container">
                        <form onSubmit={this.handleSubmit}>
                        <div className="col-md-6 card">
                            <div className="col-md-12">
                            <span className="label-form">City</span>
                            <FormGroup style={{ top: "0.5em" }}>
                                <InputGroup>
                                <InputGroup.Addon>
                                    <a style={{ cursor: "pointer" }}>
                                    <img src={require('../static/icons/ic_city.png')} style={{ width: "15px" }} />
                                    </a>
                                </InputGroup.Addon>
                                <SearchAutoComplete
                                    id="city"
                                    key={"select-single"}
                                    index={0}
                                    data={location || []}
                                    placeholder="Select City"
                                    labelKey="name"
                                    onChange={result => this.setState({ selectlocation: get(result, '0') || "" })}
                                    bsSize="large"
                                    className="typeahead search-box-adv single-search"
                                />
                                </InputGroup>
                            </FormGroup>
                            </div>
                        </div>
                        <div className="col-md-6 card">
                            <div className="col-md-12">
                            <span className="label-form">Dates</span>
                            <FormGroup style={{ top: "0.5em" }}>
                                <InputGroup>
                                <InputGroup.Addon>
                                    <a style={{ cursor: "pointer" }}>
                                    <img src={require('../static/icons/ic_date.png')} style={{ width: "15px" }} />
                                    </a>
                                </InputGroup.Addon>
                                <DateRangePicker
                                    block
                                    startDatePlaceholderText="Start"
                                    endDatePlaceholderText="End"
                                    startDate={this.state.startDate}
                                    startDateId="startDateId"
                                    endDate={this.state.endDate}
                                    endDateId="endDateId"
                                    onDatesChange={this.handleDateChange}
                                    focusedInput={focusedInput}
                                    onFocusChange={this.onFocusChange}
                                    displayFormat="DD MMM YYYY"
                                    hideKeyboardShortcutsPanel
                                    numberOfMonths={1}
                                    withPortal={false}
                                    minimumNights={0}
                                />
                                </InputGroup>
                            </FormGroup>
                            </div>
                        </div>
                        {/* <div className="col-md-4 card">
                            <div className="col-md-12">
                            <span className="label-form">Guest</span>
                            <div className="passengers-form">
                                <a className="img-pass-form">
                                <img src={require('../static/icons/ic_passengers.png')} style={{ width: "17px" }} />
                                </a>
                                <span className="pass-pep">Adult </span> <span className="pass-pep-number">{this.state.adult}</span>
                                <span className="pass-pep">Child </span> <span className="pass-pep-number">{this.state.child}</span>
                                <span className="pass-pep">Infant </span> <span className="pass-pep-number">{this.state.infant}</span>
                                <button className="booking-form__toggle-btn" onClick={() => this.setState({ passengerBoxes: !passengerBoxes })} type="button"><span className="ti-angle-down"></span></button>
                                <ul className="booking-form__passengers passengers-list" style={{ display: (passengerBoxes ? 'block' : 'none') }}>
                                <li className="passengers-list__row passengers-chooser">
                                    <label className="passengers-chooser__title" >Adult</label>
                                    <div className="passengers-chooser__amount js-amount-input-container">
                                    <button className="passengers-chooser__amount-btn" type="button" onClick={this.DecreaseAdult}>-</button>
                                    <span className="passengers-chooser__amount-field"> {this.state.adult} </span>
                                    <button className="passengers-chooser__amount-btn" type="button" onClick={this.IncrementAdult}>+</button>
                                    </div>
                                </li>
                                <li className="passengers-list__row passengers-chooser">
                                    <label className="passengers-chooser__title" >Child</label>
                                    <div className="passengers-chooser__amount js-amount-input-container">
                                    <button className="passengers-chooser__amount-btn" type="button" onClick={this.DecreaseChild}>-</button>
                                    <span className="passengers-chooser__amount-field"> {this.state.child} </span>
                                    <button className="passengers-chooser__amount-btn" type="button" onClick={this.IncrementChild}>+</button>
                                    </div>
                                </li>
                                <li className="passengers-list__row passengers-chooser">
                                    <label className="passengers-chooser__title" >Infant</label>
                                    <div className="passengers-chooser__amount js-amount-input-container">
                                    <button className="passengers-chooser__amount-btn" type="button" onClick={this.DecreaseInfant}>-</button>
                                    <span className="passengers-chooser__amount-field"> {this.state.infant} </span>
                                    <button className="passengers-chooser__amount-btn" type="button" onClick={this.IncrementInfant}>+</button>
                                    </div>
                                </li>
                                </ul>
                            </div>
                            </div>
                        </div> */}
                        <div className="col-md-12 button-rentcar"><button type="submit" className="btn btn-primary btn-sm btn-rentc-search"><span className="ti-search"></span> Search</button></div>
                        </form>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}


export default compose(
    connect(),
    withAuth(["PUBLIC"])
  )(SearchAccommodation);
