import React, { Component } from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight, map } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';
import numberWithComma from '../utils/numberWithComma';
import { FormControl, FormGroup, InputGroup, Col, Panel, PanelGroup, ListGroup, ListGroupItem } from "react-bootstrap";
import { getAirportList, postAirportList, createToken, flighAddtoCart, setDepart, setReturn, sortFlightList, getAirlineList, setErrorMessage, getFareDetail } from '../stores/actions';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import FlightListComponent from "../components/flight/flightList";
import { SingleDatePicker } from "react-dates";
import 'react-dates/initialize';
import moment, { now } from 'moment';
import 'moment/locale/id'
import Loading from '../components/shared/loading';
import axios from "axios";
import { withRouter } from 'next/router';
import Router from 'next/router';
import { ConsoleView } from 'react-device-detect';
import getConfig from "next/config"
const { publicRuntimeConfig } = getConfig()
import * as gtag from '../utils/gtag';

// Router.events.on('routeChangeComplete', url => gtag.pageview(url))

const { PREFERRED_AIRLINE } = publicRuntimeConfig
moment.locale('id')
class Flightlist extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            product_type: [],
            isRoundTrip: false,
            flightKey: null,
            activeKey: "0",
            data: [],
            airportlist: [],
            defaultOrigin: "",
            defaultDestination: "",
            origin: "",
            destination: "",
            date: moment().add(1, 'd'),
            dateDeparture: null,
            focused: false,
            return_date: moment().add(2, 'd'),
            dateReturn: null,
            return_focused: false,
            cabinType: "",
            passengerBoxes: false,
            adult: 1,
            child: 0,
            infant: 0,
            detail_origin: {},
            detail_destination: {},
            flight_list: [],
            PreferredAirlines: [11],
            loadFlights: false,
            showReturnFlights: false,
            departure_flight: null,
            return_flight: null,
            showRoundTripBar: false,
            flight_times: ["00:00 - 06:00", "06:00 - 12:00", "12:00 - 18:00", "18:00 - 24:00"],
            query_flight:{},
            dSortBy : [
                {key: "lowest_price", label: "Lowest Price", active: false },
                {key: "highest_price", label: "Highest Price", active: false },
                // {key: "earliest_d_time", label: "Earliest Departure Time", active: false },
                // {key: "latest_d_time", label: "Latest Departure Time", active: false },
                // {key: "earliest_a_time", label: "Earliest Arrival Time", active: false },
                // {key: "latest_a_time", label: "Latest Arrival Time", active: false },
        
              ],
            rSortBy : [
            {key: "lowest_price", label: "Lowest Price", active: false },
            {key: "highest_price", label: "Highest Price", active: false },
            // {key: "earliest_d_time", label: "Earliest Departure Time", active: false },
            // {key: "latest_d_time", label: "Latest Departure Time", active: false },
            // {key: "earliest_a_time", label: "Earliest Arrival Time", active: false },
            // {key: "latest_a_time", label: "Latest Arrival Time", active: false },
    
            ],
            departure_isInternational: false,
            return_isInternational: false,
            emptyField: []
         };
    }

    componentDidMount() {
        this.getAirline()

        this.setState((state) => { 
            return {
                dateDeparture: moment(state.date).format("YYYY-MM-DD"),
                dateReturn: moment(state.return_date).format("YYYY-MM-DD")
            }
        })

        this.props.dispatch(getAirportList()).then(result => {
            let airportlist = result.data
                map(airportlist, (item) => {
                item.name = `${item.CityName} (${item.Code})`;
                item.type = item.Code;
                });
            this.setState({ airportlist });
            if(this.props.router.query.d) {
                // console.log(this.props);
                let query = this.props.router.query
                let detail_origin = airportlist.find(airport => airport.Code == query.o)
                let detail_destination = airportlist.find(airport => airport.Code == query.d)
                this.setState({
                    infant: +query.infant,
                    child: +query.child,
                    adult: +query.adult,
                    origin: query.o,
                    destination: query.d,
                    date:  moment(query.d_date),
                    return_date: moment(query.r_date),
                    dateDeparture: query.d_date,
                    dateReturn: query.r_date,
                    isRoundTrip: query.type == "depart" ? true : false,
                    cabinType: query.cabin,
                    defaultOrigin: detail_origin.CityName,
                    defaultDestination: detail_destination.CityName,
                }, () => this.handleSubmit())
                
            }
        })
        .catch(err => console.log(err))
    }
    
    getAirline = () => {
        this.props.dispatch(getAirlineList())
        .then(res => {
            let airlines = res.data.airLines
            let temp = []
            for(let i = 0; i < airlines.length; i++) {
                temp.push(airlines[i].key)
            }
            this.setState({
                PreferredAirlines: temp
            })
        })
        .catch(err => {
            let prefAir = PREFERRED_AIRLINE.split(',')
            this.setState({PreferredAirlines: prefAir})
            console.log('====================================');
            console.log(err);
            console.log('====================================');
        })
    }

    handleChange = () => {
        this.setState({
          isRoundTrip: !this.state.isRoundTrip
        })
    }

    IncrementAdult = () => {
        this.setState({ adult: this.state.adult + 1 }, () => this.cekField("passenger"));
    }
    DecreaseAdult = () => {
        this.setState({ adult: this.state.adult - 1 },() => this.cekField("passenger"));
    }

    IncrementChild = () => {
        this.setState({ child: this.state.child + 1 },() => this.cekField("passenger"));
    }
    DecreaseChild = () => {
        this.setState({ child: this.state.child - 1 },() => this.cekField("passenger"));
    }

    IncrementInfant = () => {
        this.setState({ infant: this.state.infant + 1 },() => this.cekField("passenger"));
    }
    DecreaseInfant = () => {
        this.setState({ infant: this.state.infant - 1 },() => this.cekField("passenger"));
    }

    handleSelect = (activeKey) => {
        this.setState({activeKey: this.state.activeKey === activeKey ? 0 : activeKey})
    }

    handleSubmit = async (e) => {
        try {
            e ? e.preventDefault() : null
            let { dispatch } = this.props
            let {origin, destination, dateDeparture, dateReturn, adult, child, infant, PreferredAirlines, cabinType, airportlist, isRoundTrip} = this.state
            let emptyField = []
            if(origin === "") { emptyField.push('origin')}
            if(destination === "") { emptyField.push('destination')}
            if(dateDeparture === null) { emptyField.push('dateDeparture')}
            if(isRoundTrip) { if(dateReturn === null) { emptyField.push('dateReturn')}}
            if(adult === 0 && child === 0 && infant === 0) { emptyField.push('passenger') }
            if(cabinType === "") { emptyField.push("cabinType")}
            if(emptyField.length > 0 ) {
                this.setState({emptyField})
            } else {
                this.setState({ loadFlights: true })
                let query = {
                    infant: infant,
                    adult: adult,
                    child: child,
                    o: origin,
                    d: destination,
                    d_date: dateDeparture,
                    r_date: isRoundTrip ? dateReturn : "",
                    origin: this.state.defaultOrigin,
                    destination: this.state.defaultDestination,
                    type: isRoundTrip ? "depart" : "one-way",
                    cabin: cabinType
                }

                e ? Router.replace({pathname:"/flight", query}) : null
                localStorage.setItem("_flight_recently_searched", JSON.stringify(query));

                const Routes = [{
                    'Origin': origin,
                    'Destination': destination,
                    'DepartDate': dateDeparture
                    }]
                isRoundTrip ? Routes.push({
                    'Origin': destination,
                    'Destination': origin,
                    'DepartDate': dateReturn
                }) : null
                let Params = {
                    'Routes': Routes,
                    'Adult': adult,
                    'Child': child,
                    'Infant': infant,
                    'FareType': 'LowestFare',
                    'PreferredAirlines': [],
                    'CabinClass': cabinType
                }
                let getFlight = []
                let detail_origin = airportlist.find(airport => airport.Code == this.state.origin)
                let detail_destination = airportlist.find(airport => airport.Code == this.state.destination)
                
                for(let i=0; i < PreferredAirlines.length; i++) {
                    Params.PreferredAirlines = [+PreferredAirlines[i]]
                    getFlight.push(await dispatch(postAirportList(Params)))
                }

                for(let j=0; j < getFlight.length; j++) {

                    if(getFlight[j].meta.code === 200) {
                        let flightAvailability= getFlight[j].data.Schedules
                        let departureFlights = flightAvailability.length > 0 ? flightAvailability[0].Flights : []
                        let returnFlights = flightAvailability.length > 1 ? flightAvailability[1].Flights : []
                        let flight_list = this.state.flight_list
                        if(this.state.flight_list.length > 0) {
                            for(let i=0; i < departureFlights.length; i++) {
                                if(departureFlights[i].IsConnecting !== true) {
                                    departureFlights[i].MultiAirline = false
                                    departureFlights[i].detail_origin = airportlist.find(airport => airport.Code == departureFlights[i].Origin)
                                    departureFlights[i].detail_destination = airportlist.find(airport => airport.Code == departureFlights[i].Destination)
                                    departureFlights[i].TotalFare = departureFlights[i].ClassObjects[0].Fare + departureFlights[i].ClassObjects[0].Tax
                                    flight_list[0].push(departureFlights[i])
                                } else {
                                    departureFlights[i].MultiAirline = departureFlights[i].ConnectingFlights.every( (val, i, arr) => val.AirlineName === arr[0].AirlineName ) 
                                    departureFlights[i].SameAirline = departureFlights[i].ConnectingFlights.every( (val, i, arr) => val.Airline === arr[0].Airline )
                                    let totalFare = 0
                                    let domestic = false
                                    for(let j=0; j < departureFlights[i].ConnectingFlights.length; j++) {
                                        departureFlights[i].ConnectingFlights[j].detail_origin = airportlist.find(airport => airport.Code == departureFlights[i].ConnectingFlights[j].Origin)
                                        departureFlights[i].ConnectingFlights[j].detail_destination = airportlist.find(airport => airport.Code == departureFlights[i].ConnectingFlights[j].Destination)
                                        totalFare = totalFare + departureFlights[i].ConnectingFlights[j].ClassObjects[0].Fare + departureFlights[i].ConnectingFlights[j].ClassObjects[0].Tax
                                        if(departureFlights[i].ConnectingFlights[j].detail_origin && departureFlights[i].ConnectingFlights[j].detail_destination) {
                                            domestic = true
                                        }
                                    }
                                    if(domestic === true) {
                                        departureFlights[i].TotalFare = totalFare
                                        flight_list[0].push(departureFlights[i])
                                    }
                                }
                            }
                            if(flight_list.length === 2) {
                                for(let i=0; i < returnFlights.length; i++) {
                                    if(!returnFlights[i].IsConnecting) {
                                        returnFlights[i].MultiAirline = false
                                        returnFlights[i].detail_origin = airportlist.find(airport => airport.Code == returnFlights[i].Origin)
                                        returnFlights[i].detail_destination = airportlist.find(airport => airport.Code == returnFlights[i].Destination)
                                        returnFlights[i].TotalFare = returnFlights[i].ClassObjects[0].Fare + returnFlights[i].ClassObjects[0].Tax
                                        flight_list[1].push(returnFlights[i])
                                    } else {
                                        returnFlights[i].MultiAirline = returnFlights[i].ConnectingFlights.every( (val, i, arr) => val.AirlineImageUrl === arr[0].AirlineImageUrl ) 
                                        returnFlights[i].SameAirline = returnFlights[i].ConnectingFlights.every( (val, i, arr) => val.Airline === arr[0].Airline )
                                        let totalFare = 0
                                        let domestic = false
                                        for(let j=0; j < returnFlights[i].ConnectingFlights.length; j++) {
                                            returnFlights[i].ConnectingFlights[j].detail_origin = airportlist.find(airport => airport.Code == returnFlights[i].ConnectingFlights[j].Origin)
                                            returnFlights[i].ConnectingFlights[j].detail_destination = airportlist.find(airport => airport.Code == returnFlights[i].ConnectingFlights[j].Destination)
                                            totalFare = totalFare + returnFlights[i].ConnectingFlights[j].ClassObjects[0].Fare + returnFlights[i].ConnectingFlights[j].ClassObjects[0].Tax
                                            if(returnFlights[i].ConnectingFlights[j].detail_origin && returnFlights[i].ConnectingFlights[j].detail_destination) {
                                                domestic = true
                                            }
                                        }
                                        if(domestic === true) {
                                            returnFlights[i].TotalFare = totalFare
                                            flight_list[1].push(returnFlights[i])
                                        }
                                    }
                                }
                            }
                        } else {
                            flight_list=[[], []]
                            for(let i=0; i < departureFlights.length; i++) {
                                if(departureFlights[i].IsConnecting !== true) {
                                    departureFlights[i].MultiAirline = false
                                    departureFlights[i].detail_origin = airportlist.find(airport => airport.Code == departureFlights[i].Origin)
                                    departureFlights[i].detail_destination = airportlist.find(airport => airport.Code == departureFlights[i].Destination)
                                    departureFlights[i].TotalFare = departureFlights[i].ClassObjects[0].Fare + departureFlights[i].ClassObjects[0].Tax
                                    flight_list[0].push(departureFlights[i])
                                } else {
                                    departureFlights[i].MultiAirline = departureFlights[i].ConnectingFlights.every( (val, i, arr) => val.AirlineImageUrl === arr[0].AirlineImageUrl ) 
                                    departureFlights[i].SameAirline = departureFlights[i].ConnectingFlights.every( (val, i, arr) => val.Airline === arr[0].Airline )
                                    let totalFare = 0
                                    let domestic = false
                                    for(let j=0; j < departureFlights[i].ConnectingFlights.length; j++) {
                                        departureFlights[i].ConnectingFlights[j].detail_origin = airportlist.find(airport => airport.Code == departureFlights[i].ConnectingFlights[j].Origin)
                                        departureFlights[i].ConnectingFlights[j].detail_destination = airportlist.find(airport => airport.Code == departureFlights[i].ConnectingFlights[j].Destination)
                                        totalFare = totalFare + departureFlights[i].ConnectingFlights[j].ClassObjects[0].Fare + departureFlights[i].ConnectingFlights[j].ClassObjects[0].Tax
                                        if(departureFlights[i].ConnectingFlights[j].detail_origin && departureFlights[i].ConnectingFlights[j].detail_destination) {
                                            domestic = true
                                        }
                                    }
                                    if(domestic === true) {
                                        departureFlights[i].TotalFare = totalFare
                                        flight_list[0].push(departureFlights[i])
                                    }
                                    
                                }
                            }
                            if(flightAvailability.length > 1) {
                                for(let i=0; i < returnFlights.length; i++) {
                                    if(!returnFlights[i].IsConnecting) {
                                        returnFlights[i].MultiAirline = false
                                        returnFlights[i].detail_origin = airportlist.find(airport => airport.Code == returnFlights[i].Origin)
                                        returnFlights[i].detail_destination = airportlist.find(airport => airport.Code == returnFlights[i].Destination)
                                        returnFlights[i].TotalFare = returnFlights[i].ClassObjects[0].Fare + returnFlights[i].ClassObjects[0].Tax
                                        flight_list[1].push(returnFlights[i])
                                    } else {
                                        returnFlights[i].MultiAirline = returnFlights[i].ConnectingFlights.every( (val, i, arr) => val.AirlineImageUrl === arr[0].AirlineImageUrl ) 
                                        returnFlights[i].SameAirline = returnFlights[i].ConnectingFlights.every( (val, i, arr) => val.Airline === arr[0].Airline )
                                        let totalFare = 0
                                        let domestic = false
                                        for(let j=0; j < returnFlights[i].ConnectingFlights.length; j++) {
                                            returnFlights[i].ConnectingFlights[j].detail_origin = airportlist.find(airport => airport.Code == returnFlights[i].ConnectingFlights[j].Origin)
                                            returnFlights[i].ConnectingFlights[j].detail_destination = airportlist.find(airport => airport.Code == returnFlights[i].ConnectingFlights[j].Destination)
                                            totalFare = totalFare + returnFlights[i].ConnectingFlights[j].ClassObjects[0].Fare + returnFlights[i].ConnectingFlights[j].ClassObjects[0].Tax
                                            if(returnFlights[i].ConnectingFlights[j].detail_origin && returnFlights[i].ConnectingFlights[j].detail_destination) {
                                                domestic = true
                                            }
                                        }
                                        if(domestic === true) {
                                            returnFlights[i].TotalFare = totalFare
                                            flight_list[1].push(returnFlights[i])
                                        }
                                        
                                    }
                                }
                            } 
                        }   
                        this.setState({
                        detail_destination,
                        detail_origin,
                        flight_list,
                        loadFlights: false,
                        showReturnFlights: false,
                        departure_flight: null,
                        return_flight: null,
                        departure_isInternational: flightAvailability.length > 0 ? flightAvailability[0].IsInternational : false,
                        return_isInternational: flightAvailability.length > 1 ? flightAvailability[1].IsInternational : false,
                        showRoundTripBar: isRoundTrip}, () => {
                            this.sortFlight(this.state.dSortBy[0], 0)
                            this.sortFlight(this.state.rSortBy[0], 1)
                        })
                    } else {
                        this.setState({ loadFlights: false })
                        dispatch(setErrorMessage(get(res, 'data')))
                    }
                }
            }
        } catch (error) {
            this.setState({loadFlights: false})
            this.props.dispatch(setErrorMessage("Oops, There's something wrong"))
            console.log('====================================');
            console.log(error);
            console.log('====================================');
        }
    }

    onClickDepartureFlightSummary =  () => {
        this.setState({
          showReturnFlights: false,
          departure_flight: null,
          return_flight: null,
        })
    }

    getFareDetail = (flight, flightIdx) => {
        let { detail_origin, detail_destination, adult, child, infant} = this.state
        if(!flight.IsConnecting) {
            let departDataReq = {
                    "Airline": flight.Airline,
                    "Adult": +adult,
                    "Child": +child,
                    "Infant": +infant,
                    "ClassId": flight.ClassObjects[0].Id,
                    "FlightId": flight.Id,
                    "Fare": flight.ClassObjects[0].Fare,
                    "Tax": flight.ClassObjects[0].Tax
                    }
            this.props.dispatch(getFareDetail(departDataReq)).then(resfareDepart => {
                let data = resfareDepart.data
                flight.Fare = data.Total                  
                this.chooseFlight(flight, flightIdx)
            })
        } else {
            let done = false
            let classId = ''
            let flightId = ''
            let totalfare = 0
            let totalTax = 0
            for(let i=0; i < flight.ConnectingFlights.length; i++) {
                if(i === 0 ) {
                    classId = flight.ConnectingFlights[i].ClassObjects[0].Id
                    flightId = flight.ConnectingFlights[i].Id
                    totalfare = totalfare + flight.ConnectingFlights[i].ClassObjects[0].Fare
                    totalTax = totalTax + flight.ConnectingFlights[i].ClassObjects[0].Tax
                } else {
                    classId = classId + '#' + flight.ConnectingFlights[i].ClassObjects[0].Id
                    flightId = flightId + '#' + flight.ConnectingFlights[i].Id
                    totalfare = totalfare + flight.ConnectingFlights[i].ClassObjects[0].Fare
                    totalTax = totalTax + flight.ConnectingFlights[i].ClassObjects[0].Tax
                }
                
            }
            let departDataReq = {
                "Airline": flight.Airline,
                "Adult": +adult,
                "Child": +child,
                "Infant": +infant,
                "ClassId": classId,
                "FlightId": flightId,
                "Fare": totalfare,
                "Tax": totalTax
            }
            this.props.dispatch(getFareDetail(departDataReq)).then(resfareDepart => {
                let data = resfareDepart.data
                flight.Fare = data.Total
                for(let i=0; i < flight.ConnectingFlights.length; i++) {
                    if(i === 0) {
                        flight.ConnectingFlights[i].Fare = data.Total
                        flight.ConnectingFlights[i].Tax = 0
                    } else {
                        flight.ConnectingFlights[i].Fare = 0
                        flight.ConnectingFlights[i].Tax = 0
                    }
                }
                this.chooseFlight(flight, flightIdx)
            })
        }
        
    }

    chooseFlight = (flight, flightIdx) => {
        if(this.state.flight_list[1].length === 0 || flightIdx === 1) {
            const {router, dispatch, } = this.props;
            let { detail_origin, detail_destination, adult, child, infant} = this.state
            let {isRoundTrip, departure_flight} = this.state

            dispatch(createToken())
            .then(result => {
                let Segments = []
                if(isRoundTrip) {
                    flight.IsInternational = this.state.return_isInternational
                    dispatch(setReturn(flight)).then(returnRes => {                      
                    }) 
                    if(departure_flight.IsConnecting) {
                        let depart_ConnectingFlights = departure_flight.ConnectingFlights
                        for(let i=0; i < depart_ConnectingFlights.length; i++) {
                            Segments.push({
                                ClassId: depart_ConnectingFlights[i].ClassObjects[0].Id,
                                Airline: depart_ConnectingFlights[i].Airline,
                                FlightNumber: depart_ConnectingFlights[i].Number,
                                Number: depart_ConnectingFlights[i].Number,
                                Origin: depart_ConnectingFlights[i].detail_origin.Code,
                                DepartDate: depart_ConnectingFlights[i].DepartDate,
                                DepartTime: depart_ConnectingFlights[i].DepartTime,
                                Destination: depart_ConnectingFlights[i].detail_destination.Code,
                                ArriveDate: depart_ConnectingFlights[i].ArriveDate,
                                Fare: depart_ConnectingFlights[i].Fare,
                                ArriveTime: depart_ConnectingFlights[i].ArriveTime,
                                ClassCode: depart_ConnectingFlights[i].ClassObjects[0].Code,
                                FlightId: depart_ConnectingFlights[i].Id,
                                Num: departure_flight.SameAirline ? 0 : i,
                                Seq: i,
                                Adult : adult,
                                Infant: infant,
                                Child: child,
                                Tax: depart_ConnectingFlights[i].Tax,
                                FareBreakdowns: depart_ConnectingFlights[i].FareBreakdowns,
                                IsInternational: this.state.return_isInternational,
                                AirlineImageUrl: depart_ConnectingFlights[i].AirlineImageUrl
                              })
                        }
                    } else {
                        Segments.push({
                            ClassId: departure_flight.ClassObjects[0].Id,
                            Airline: departure_flight.Airline,
                            FlightNumber: departure_flight.Number,
                            Number: departure_flight.Number,
                            Origin: detail_origin.Code,
                            DepartDate: departure_flight.DepartDate,
                            DepartTime: departure_flight.DepartTime,
                            Destination: detail_destination.Code,
                            ArriveDate: departure_flight.ArriveDate,
                            Fare: departure_flight.Fare,
                            ArriveTime: departure_flight.ArriveTime,
                            ClassCode: departure_flight.ClassObjects[0].Code,
                            FlightId: departure_flight.Id,
                            Num: 0,
                            Seq: 0,
                            Adult : adult,
                            Infant: infant,
                            Child: child,
                            Tax: departure_flight.ClassObjects[0].Tax,
                            FareBreakdowns: departure_flight.FareBreakdowns,
                            IsInternational: this.state.return_isInternational,
                            AirlineImageUrl: departure_flight.AirlineImageUrl
                        })
                    }
                    
                } else {
                    flight.IsInternational = this.state.departure_isInternational
                    dispatch(setDepart(flight)).then(returnDep => {
                      
                    }) 
                }
                if( flight.IsConnecting ) {                    
                    let ConnectingFlights = flight.ConnectingFlights                    
                    for(let i=0; i < ConnectingFlights.length; i++) {
                        Segments.push({
                            ClassId: ConnectingFlights[i].ClassObjects[0].Id,
                              Airline: ConnectingFlights[i].Airline,
                              FlightNumber: ConnectingFlights[i].Number,
                              Number: ConnectingFlights[i].Number,
                              Origin: ConnectingFlights[i].detail_origin.Code,
                              DepartDate: ConnectingFlights[i].DepartDate,
                              DepartTime: ConnectingFlights[i].DepartTime,
                              Destination: ConnectingFlights[i].detail_destination.Code,
                              ArriveDate: ConnectingFlights[i].ArriveDate,
                              Fare: ConnectingFlights[i].Fare,
                              ArriveTime: ConnectingFlights[i].ArriveTime,
                              ClassCode: ConnectingFlights[i].ClassObjects[0].Code,
                              FlightId: ConnectingFlights[i].Id,
                              Num: flight.SameAirline ? 0 : i,
                              Seq: i,
                              Adult : adult,
                              Infant: infant,
                              Child: child,
                              Tax: ConnectingFlights[i].Tax,
                              FareBreakdowns: ConnectingFlights[i].FareBreakdowns,
                              IsInternational: this.state.departure_isInternational,
                              AirlineImageUrl: ConnectingFlights[i].AirlineImageUrl
                        })
                    }
                } else {
                    Segments.push({
                        ClassId: flight.ClassObjects[0].Id,
                        Airline: flight.Airline,
                        FlightNumber: flight.Number,
                        Number: flight.Number,
                        Origin: isRoundTrip ? detail_destination.Code : detail_origin.Code,
                        DepartDate: flight.DepartDate,
                        DepartTime: flight.DepartTime,
                        Destination: isRoundTrip ? detail_origin.Code : detail_destination.Code,
                        ArriveDate: flight.ArriveDate,
                        Fare: flight.Fare,
                        ArriveTime: flight.ArriveTime,
                        ClassCode: flight.ClassObjects[0].Code,
                        FlightId: flight.Id,
                        Num: isRoundTrip ? (flight.Airline === departure_flight.Airline ? 1 : 0) : 0,
                        Seq: flight.TotalTransit > 0 ? 1 : 0,
                        Adult : adult,
                        Infant: infant,
                        Child: child,
                        Tax: flight.ClassObjects[0].Tax,
                        FareBreakdowns: flight.FareBreakdowns,
                        IsInternational: this.state.departure_isInternational,
                        AirlineImageUrl: flight.AirlineImageUrl
                    })
                }
                

                let params = {
                    token: result.data.token,
                    Segments,
                    FlightType: flight.FlightType,
                    qty: +adult + +child + +infant
                }
                dispatch(flighAddtoCart(params)).then(res => {          
                    router.push({
                      pathname: '/manifest',
                      query: {
                        id: result.data.token,
                        adult: this.state.adult,
                        child: this.state.child,
                        infant: this.state.infant,
                        cabinType: this.state.cabinType
                      }
                    });
                })
                .catch(err => console.log(err))
            })
            .catch(err => {
                console.log('====================================');
                console.log(err);
                console.log('====================================');
            })
        } else {
            const {router, dispatch, } = this.props;
            flight.IsInternational = this.state.departure_isInternational
            dispatch(setDepart(flight)).then(returnDep => {
                
            }) 
            this.setState({
                showReturnFlights: !this.state.showReturnFlights,
                departure_flight: flight
            })
            location.href = "#flight-top"
        }
    }

    sortFlight = async (sortItem, idx) => {
        try {
            this.setState({ loadFlights: true })
            let sortBy = idx === 0 ? "dSortBy" : "rSortBy";
            let { dispatch } = this.props;
            let sort = this.state[sortBy].map((item) => {
                item.key === sortItem.key ? item.active = true : item.active = false
                return item
            });
            let flight_list = await dispatch(sortFlightList(this.state.flight_list, sortItem.key, idx ))
            this.setState({
              [sortBy] : sort ,
              flight_list,
              loadFlights: false
            })
        } catch (error) {
            console.log('====================================');
            console.log(error);
            console.log('====================================');
        }
    }

    cekField = (field) => {
        let empties = this.state.emptyField
        let emptyField = []
        if(field !== "passenger") {
            if(this.state[field] !== null || this.state[field] !== "") {
                for(let i = 0; i < empties.length; i++) {
                    if(empties[i] !== field) {
                        emptyField.push(empties[i])
                    }
                }              
            }
        } else {
            if(this.state.adult > 0 && this.state.child > 0 && this.state.infant > 0) {
                for(let i = 0; i < empties.length; i++) {
                    if(empties[i] !== field) {
                        emptyField.push(empties[i])
                    }
                } 
            }
        }
            
        this.setState({emptyField})
        
    }

    render() {
        const hidden = this.state.isRoundTrip ? '' : 'hidden';
        const { 
            airportlist,
            passengerBoxes,
            detail_destination,
            detail_origin,
            adult,
            infant,
            child,
            dateDeparture,
            dateReturn,
            flight_list,
            cabinType,
            loadFlights,
            showReturnFlights,
            departure_flight,
            showRoundTripBar,
            flight_times,
            emptyField
            } = this.state;
        const minimumDate = this.state.date;
        let query = this.props.router.query        
        return (
            <React.Fragment>
                <Head title={"Pigijo - Find Affordable Flight Ticket"} description={"Find Affordable Flight Ticket and Travel Refference Here"} url={process.env.SITE_ROOT + '/flight'} />
                <PageHeader
                background={"/static/images/img05.jpg"}
                title="Flight Ticket"
                caption="" />
                <section className="filter-flight" style={{ backgroundColor: "white" }}>
                    <div className="container">
                        <form onSubmit={this.handleSubmit}>
                            <div className="col-md-4 card">
                                <div className="col-md-12">
                                    <span className="label-form">From</span>
                                    <FormGroup style={{ top: "0.5em", marginBottom: '0.5em' }}>
                                        <InputGroup>
                                        <InputGroup.Addon>
                                            <a style={{ cursor: "pointer" }}>
                                            <img src={require('../static/icons/ic_plane1.png')} style={{ width: "15px" }} />
                                            </a>
                                        </InputGroup.Addon>
                                        <SearchAutoComplete
                                            id="origin"
                                            key={"select-single"}
                                            index={0}
                                            data={ airportlist || []}
                                            placeholder="Select City or Airport"
                                            labelKey="name"
                                            onChange={result => this.setState({ origin: get(result, '0.Code') || "" , defaultOrigin: get(result, '0.CityName') || "" }, () => this.cekField("origin"))}
                                            bsSize="large"
                                            className="typeahead search-box-adv single-search"
                                            defaultInputValue={query.origin ? query.origin : this.state.defaultOrigin}
                                        />
                                        </InputGroup>
                                    </FormGroup>
                                    {
                                        emptyField.includes('origin') ? <span style={{color: 'red'}}>Please enter your origin airport</span> : null
                                    }
                                </div>
                                <div className="col-md-12 fmargin">
                                    <span className="label-form" >To</span>
                                    <FormGroup style={{ top: "0.5em", marginBottom: '0.5em' }}>
                                        <InputGroup>
                                        <InputGroup.Addon>
                                            <a style={{ cursor: "pointer" }}>
                                            <img src={require('../static/icons/ic_plane2.png')} style={{ width: "15px" }} />
                                            </a>
                                        </InputGroup.Addon>
                                        <SearchAutoComplete
                                            id="origin"
                                            key={"select-single"}
                                            index={0}
                                            data={airportlist || []}
                                            placeholder="Select City or Airport"
                                            labelKey="name"
                                            onChange={result => this.setState({ destination: get(result, '0.Code') || "" , defaultDestination: get(result, '0.CityName') || ""}, () => this.cekField("destination"))}
                                            bsSize="large"
                                            className="typeahead search-box-adv single-search"
                                            defaultInputValue={ query.destination ? query.destination :  this.state.defaultDestination}
                                        />

                                        </InputGroup>
                                    </FormGroup>
                                    {
                                        emptyField.includes('destination') ? <span style={{color: 'red'}}>Please enter your destination airport</span> : null
                                    }
                                </div>
                            </div>
                            <div className="col-md-4 card">
                                <div className="col-md-12">
                                    <span className="label-form">Departure</span>
                                    <FormGroup style={{ top: "0.5em", zIndex: 15, marginBottom: '0.5em' }}>
                                        <InputGroup className="date-custom-1">
                                        <InputGroup.Addon>
                                            <a style={{ cursor: "pointer" }}>
                                            <img src={require('../static/icons/ic_date.png')} style={{ width: "15px" }} />
                                            </a>
                                        </InputGroup.Addon>

                                        <SingleDatePicker
                                            numberOfMonths={1}
                                            date={this.state.date}
                                            onDateChange={date => 
                                                this.setState({
                                                    date: date,
                                                    dateDeparture: moment(date).format("YYYY-MM-DD"),
                                                    return_date: moment(date).add(1, 'd'),
                                                    dateReturn: moment(date).add(1, 'd').format("YYYY-MM-DD")
                                                }, () => this.cekField("dateDeparture"))
                                            }
                                            focused={this.state.focused}
                                            onFocusChange={({ focused }) => this.setState({ focused })}
                                            id="test"
                                            placeholder="Depature Date"
                                        />
                                        </InputGroup>
                                    </FormGroup>
                                    {
                                        emptyField.includes('dateDeparture') ? <span style={{color: 'red'}}>Please enter your depart airport</span> : null
                                    }
                                </div>
                                <div className="col-md-12 fmargin">
                                    <input type="checkbox" className="styled-checkbox" checked={this.state.isRoundTrip} onChange={this.handleChange} style={{width: 'auto'}}/>
                                    <span className="label-form">&nbsp;Return</span>
                                        {this.state.isRoundTrip &&
                                            <FormGroup className={hidden} style={{ top: "0.5em", zIndex: 10, marginBottom: '0.5em' }}>
                                                <InputGroup className="date-custom-1">
                                                    <InputGroup.Addon>
                                                    <a style={{ cursor: "pointer" }}>
                                                        <img src={require('../static/icons/ic_date.png')} style={{ width: "15px" }} />
                                                    </a>
                                                    </InputGroup.Addon>
                                                    <SingleDatePicker
                                                        numberOfMonths={1}
                                                        isOutsideRange={(day) => day.isBefore(minimumDate)}
                                                        date={this.state.return_date}
                                                        onDateChange={return_date =>
                                                            this.setState({
                                                            return_date: return_date,
                                                            dateReturn: moment(return_date).format("YYYY-MM-DD")
                                                            },() => this.cekField("dateReturn"))
                                                        }
                                                        focused={this.state.return_focused}
                                                        onFocusChange={({ focused }) => this.setState({ return_focused: focused })}
                                                        id="return"
                                                        placeholder="Return Date"
                                                    />

                                                </InputGroup>
                                            </FormGroup>
                                        }
                                        {
                                            (emptyField.includes('dateReturn') && this.state.isRoundTrip) ? <span style={{color: 'red'}}>Please enter your depart airport</span> : null
                                        }
                                </div>
                            </div>
                            <div className="col-md-4 card">
                                <div className="col-md-12">
                                    <span className="label-form" >No. of passengers</span>
                                    <div className="passengers-form" style={{marginBottom: '0.5em'}}>
                                        <a className="img-pass-form">
                                            <img src={require('../static/icons/ic_passengers.png')} style={{ width: "17px" }} />
                                        </a>
                                        <span className="pass-pep">Adult </span> <span className="pass-pep-number">{this.state.adult}</span>
                                        <span className="pass-pep">Child </span> <span className="pass-pep-number">{this.state.child}</span>
                                        <span className="pass-pep">Infant </span> <span className="pass-pep-number">{this.state.infant}</span>
                                        <button className="booking-form__toggle-btn" onClick={() => this.setState({ passengerBoxes: !passengerBoxes })} type="button">
                                            <span className="ti-angle-down"></span>
                                        </button>
                                        <ul className="booking-form__passengers passengers-list" style={{ display: (passengerBoxes ? 'block' : 'none') }}>
                                            <li className="passengers-list__row passengers-chooser">
                                                <label className="passengers-chooser__title" >Adult</label>
                                                <div className="passengers-chooser__amount js-amount-input-container">
                                                    <button 
                                                        className="passengers-chooser__amount-btn" 
                                                        type="button" 
                                                        onClick={this.DecreaseAdult}
                                                        disabled={this.state.adult <= 1} 
                                                    >-</button>
                                                    <span className="passengers-chooser__amount-field"> {this.state.adult} </span>
                                                    <button 
                                                        className="passengers-chooser__amount-btn" 
                                                        type="button" 
                                                        onClick={this.IncrementAdult}
                                                        disabled={this.state.adult + this.state.child >= 7}
                                                    >+</button>
                                                </div>
                                            </li>
                                            <li className="passengers-list__row passengers-chooser">
                                                <label className="passengers-chooser__title" >Child</label>
                                                <div className="passengers-chooser__amount js-amount-input-container">
                                                    <button 
                                                        className="passengers-chooser__amount-btn" 
                                                        type="button" 
                                                        onClick={this.DecreaseChild}
                                                        disabled={this.state.child < 1}
                                                    >-</button>
                                                    <span className="passengers-chooser__amount-field"> {this.state.child} </span>
                                                    <button 
                                                        className="passengers-chooser__amount-btn" 
                                                        type="button" 
                                                        onClick={this.IncrementChild}
                                                        disabled={this.state.adult + this.state.child >= 7}
                                                    >+</button>
                                                </div>
                                            </li>
                                            <li className="passengers-list__row passengers-chooser">
                                                <label className="passengers-chooser__title" >Infant</label>
                                                <div className="passengers-chooser__amount js-amount-input-container">
                                                    <button 
                                                        className="passengers-chooser__amount-btn" 
                                                        type="button" 
                                                        onClick={this.DecreaseInfant}
                                                        disabled={this.state.infant < 1}
                                                    >-</button>
                                                    <span className="passengers-chooser__amount-field"> {this.state.infant} </span>
                                                    <button 
                                                        className="passengers-chooser__amount-btn" 
                                                        type="button" 
                                                        onClick={this.IncrementInfant}
                                                        disabled={this.state.infant >= this.state.adult}
                                                    >+</button>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    {
                                        emptyField.includes('passenger') ? <span style={{color: 'red'}}>Please enter your depart airport</span> : null
                                    }
                                </div>

                                <div className="col-md-12 fmargin">
                                    <span className="label-form" >Cabin Class</span>
                                    <FormGroup style={{ top: "0.5em" }}>
                                        <InputGroup>
                                            <InputGroup.Addon>
                                                <a style={{ cursor: "pointer" }}>
                                                <img src={require('../static/icons/ic_cabin.png')} style={{ width: "17px" }} />
                                                </a>
                                            </InputGroup.Addon>
                                            <SearchAutoComplete
                                                id="cabin"
                                                key={"select-single"}
                                                index={0}
                                                data={[{ name: 'Economy', type: '' }, { name: 'Business', type: '' }, { name: 'FirstClass', type: '' }]}
                                                placeholder="Select Cabin"
                                                labelKey="name"
                                                onChange={result => this.setState({ cabinType: get(result, '0.name') || "" },() => this.cekField("cabinType"))}
                                                bsSize="large"
                                                className="typeahead search-box-adv single-search"
                                                defaultInputValue={query.cabin ? query.cabin : this.state.cabinType}
                                            />
                                        </InputGroup>
                                    </FormGroup>
                                    {
                                        emptyField.includes('cabinType') ? <span style={{color: 'red'}}>Please enter your cabin class</span> : null
                                    }
                                </div>
                            </div>
                            <div className="col-md-12 button-flight">
                                <button type="submit" className="btn btn-primary btn-sm btn-flight-search"><span className="ti-search"></span> Search</button>
                            </div>
                        </form>

                        
                    </div>
                </section>
                
                { showRoundTripBar ? (
                    <section id="flight-top" className="filter-flight flight-review" style={{marginTop: 20}}>
                        <div className="container">
                            <div className="row" style={{backgroundColor: "white"}}>
                                {this.state.departure_flight ? (
                                    <div className="col-md-4 flight-review-box inactive" onClick={() => this.onClickDepartureFlightSummary()} id="departure-review">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <div className="img-logo-flight" >
                                                    <img src={departure_flight.AirlineImageUrl}/>
                                                </div>
                                            </div>
                                            <div className="col-md-9">
                                                <p className="flight-review-title">{departure_flight.AirlineName}</p>
                                                <ul className="flight-review-list">
                                                    <li className="check-flight-rute">{departure_flight.origin} - {departure_flight.destination}</li>
                                                    <li className="check-flight-rute">{departure_flight.DepartDate}</li>
                                                    <li className="check-flight-rute">{departure_flight.DepartTime} - {departure_flight.ArriveTime}</li>
                                                    <li className="check-flight-rute" id="price">Rp. {numberWithComma(departure_flight.Fare)}</li>
                                                    <li><a onClick={() => this.onClickDepartureFlightSummary()}>Change</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <div className="col-md-8 flight-review-box">
                                        <p className="flight-review-title"> Departure Flight</p>
                                        <p className="check-flight-rute">{detail_origin.CityName} ({detail_origin.Code}) - {detail_destination.CityName} ({detail_destination.Code})</p>
                                    </div>
                                )}

                                {this.state.showReturnFlights ? (
                                    <div className="col-md-8 flight-review-box">
                                        <p className="flight-review-title"> Return Flight</p>
                                        <p className="check-flight-rute">{detail_destination.CityName} ({detail_destination.Code}) - {detail_origin.CityName} ({detail_origin.Code})</p>
                                    </div>
                                ) : (
                                    <div className="col-md-4 flight-review-box inactive">
                                        <p className="flight-review-title"> Return Flight</p>
                                    </div>
                                )}
                            </div>
                        </div>
                    </section>
                ) : null}

                <section className="flight-list-area">
                {loadFlights ? (<Loading/>) :
                flight_list.length > 0 ? (
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 flight-area">
                                {showReturnFlights ? (
                                    <FlightListComponent 
                                        index={1} 
                                        handleSelect={this.handleSelect} 
                                        activeKey={this.state.activeKey}
                                        detail_origin={detail_origin} 
                                        detail_destination={detail_destination} 
                                        dateDeparture ={dateDeparture} 
                                        dateReturn={dateReturn} 
                                        departure_flight={flight_list[1]} 
                                        chooseFlight={this.getFareDetail}
                                        adult={adult} child={child} infant={infant} cabinType={cabinType}
                                        sortBy={this.state.rSortBy}
                                        sortFlight={this.sortFlight}
                                    />
                                ) : (
                                    <FlightListComponent 
                                        index={0} 
                                        handleSelect={this.handleSelect} 
                                        activeKey={this.state.activeKey}
                                        detail_origin={detail_origin} 
                                        detail_destination={detail_destination} 
                                        dateDeparture ={dateDeparture} 
                                        dateReturn={dateReturn} 
                                        departure_flight={flight_list[0]} 
                                        chooseFlight={this.getFareDetail}
                                        adult={adult} child={child} infant={infant} cabinType={cabinType}
                                        sortBy={this.state.rSortBy}
                                        sortFlight={this.sortFlight}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                ) : null}
                </section>
            </React.Fragment>
        );
    }
}

export default compose(
    withRouter,
    connect(),
    withAuth(["PUBLIC"])
  )(Flightlist);