import React, { Component } from 'react'
import { get, isEmpty, concat } from 'lodash';
import { compose } from "redux";
import { connect } from 'react-redux';
import { Link, Router } from '../routes';
import { setErrorMessage, setSuccessMessage } from '../stores/actions'
import withAuth from '../_hoc/withAuth';
import Head from '../components/head';
import { apiCall, apiUrl } from '../services/request'
import { Modal, Dropdown, DropdownButton, MenuItem, FormGroup, InputGroup } from "react-bootstrap";
import PageHeader from '../components/page-header';
import { SingleDatePicker } from "react-dates";
import 'react-dates/initialize';

class Imiatletform extends Component {
    constructor(props) {
        super(props);
        this.state = {
            check_in: null,
            check_in_focused: false,
            check_out: null,
            check_out_focused: false,
            kta: '',
            atlet_type: '',
            atlet_subtype: '',
            nama: '',
            alamat: '',
            prov: 1,
            tim: '',
            juara: '',
            category: '',
            category_subtype: '',
            phone: '',
            email: '',
            gender: 'Mr',
            provinces: [],
            status: 'ACTIVE'
        };
    }

    componentDidMount() {
        this.getProvinces()
    }

    getProvinces = async () => {
        const dataReq = {
            method: "GET",
            url: apiUrl + `/v2/location/province`,
        };
        const res = await this.props.dispatch(apiCall(dataReq));

        if (get(res, 'meta.code') === 200) {
            this.setState({ provinces: get(res, 'data') })
        }
    }

    Regist = async () => {
        let { dispatch, type } = this.props
        let { kta, atlet_type, atlet_subtype, nama, alamat, prov, tim, juara, category, category_subtype, phone, email, check_in, check_out, gender } = this.state
        if (kta === '' || atlet_type === '' || atlet_subtype === '' || nama === '' || alamat === '' || prov === '' || tim === '' || juara === '' || category === '' || category_subtype === '' || phone === '' || email === '' || gender == '' || check_in == null || check_out, gender == null) {
            let res = await dispatch(setErrorMessage('Semua field wajib diisi'))
        } else {
            this.sendForm()
        }
    }

    sendForm = async () => {
        let { dispatch, type } = this.props
        let { kta, atlet_subtype, nama, alamat, prov, tim, juara, category_subtype, phone, email, check_in, check_out, gender } = this.state
        let names = nama.split(" ")
        let last = names.pop()
        let data = {
            type: 'atlet',
            no_kta: kta,
            firstname: `${names.length >= 1 ? names.join(" ") : nama}`,
            lastname: last,
            phone,
            province: prov,
            email,
            check_in,
            check_out,
            gender,
            team: tim,
            awards: juara,
            address: alamat,
            atlet_type: atlet_subtype,
            category: category_subtype
        }

        const dataReq = {
            method: "POST",
            url: `https://api-registration.pigijo.com/user/register`,
            data: {
                data
            }
        };

        const res = await dispatch(apiCall(dataReq));
        if (get(res, 'meta.code') === 200) {
            let res = await dispatch(setSuccessMessage('Registrasi berhasil, silahkan periksa email untuk verifikasi'))
            Router.push({ pathname: '/imi-registration/thankyou', })

        } else if (get(res, 'meta.code') === 400) {
            await dispatch(setErrorMessage(get(res, 'meta.message')))
        }
    }

    changeEmail = (val) => {
        let pattern = /[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g;
        let result = pattern.test(val)
        if (result) {
            this.setState({ email: val, emailerror: '' })
        } else {
            this.setState({ email: val, emailerror: 'Email tidak valid' })
        }
    }

    changePhone = (val) => {
        let patternNumber = /^[0-9\b]+$/;
        let NumCheck = patternNumber.test(+val)
        let string = `${val}`
        if (string.length < 8 || string.length > 13 || string[0] !== '0' || !NumCheck) {
            this.setState({ phone: val, phonerror: 'Nomor telfon tidak valid' })
        } else {
            this.setState({ phone: val, phonerror: '' })
        }
    }

    changekta = (val) => {
        this.setState({ kta: val })
        axios.post('https://crm.imi.id/switching/crisa/imi/customer/inquiry/status/membership', {
            memberCardId: val
        }, {
            headers: {
                'content-type': 'application/json',
                Authorization: 'DA01 apiKeyImi:R3qzIsOIp1KkSNkn/3jbzVLl1l8=',
            }
        })
            .then((res) => {
                this.setState({ ktaerror: '', nama: res.customerName, email: customerEmail, status: res.customerMembershipStatus })
            })
            .catch(err => {
                if (err.status === 400) {
                    this.setState({ ktaerror: 'Nomor KTA tidak valid' })
                }

            })
    }

    render() {
        let { kta, atlet_type, atlet_subtype, nama, alamat, prov, tim, juara, category, category_subtype, phone, email, gender, provinces } = this.state
        return (
            <div>
                <Head />
                <PageHeader background={'/static/images/img01.jpg'} title="Atlet Nasional" caption="Data Pribadi" />
                <div className="container" style={{ paddingTop: '3em' }}>
                    <div className="row align-items-start" style={{ paddingBottom: '2em' }}>
                        <p className='text-center'><img src={require("../static/images/logo.png")} className="img-fluid" width="30%" /></p>
                    </div>

                    <div className="row align-items-center mb-2" style={{ marginBottom: '1em' }}>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>No. KTA :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={kta} onChange={e => this.changekta(e.target.value)} />
                        </div>
                        <div className="col-md-1">
                            <span style={{ fontSize: '13px' }}>Partisipasi :</span>
                        </div>
                        <div className="col-md-3">
                            <div style={{
                                display: 'flex',
                                width: '100%',
                                border: '1px solid #eee',
                                backgroundColor: '#eee',
                                borderRadius: '5px',
                                alignItems: 'center',
                            }}>
                                <input type="checkbox" style={{ margin: '0.5em', marginRight: 0, width: '2em' }} aria-label="..." checked={atlet_type === "olahraga" ? true : false} onChange={() => this.setState({ atlet_type: atlet_type !== 'olahraga' ? 'olahraga' : '', atlet_subtype: atlet_type !== 'olahraga' ? 'Olahraga - FIA International' : '' })} />
                                <select className="form-control form-control-sm" style={{ borderRadius: '0px' }} value={atlet_subtype} onChange={e => this.setState({ atlet_subtype: e.target.value })} disabled={atlet_type !== 'olahraga' ? true : false}>
                                    <option >Olahraga</option>
                                    <option value="Olahraga - FIA International">FIA International</option>
                                    <option value="Olahraga - FIA Intenational Progress">FIA Intenational Progress</option>
                                    <option value="Olahraga - CIK International">CIK International</option>
                                    <option value="Olahraga - CIK Intenational Progre">CIK Intenational Progress</option>
                                    <option value="Olahraga - FIM International">FIM International</option>
                                    <option value="Olahraga - FIM Intenational Progress">FIM Intenational Progress</option>
                                    <option value="Olahraga - Women in Motorsport">Women in Motorsport</option>
                                </select>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div style={{
                                display: 'flex',
                                width: '100%',
                                border: '1px solid #eee',
                                backgroundColor: '#eee',
                                borderRadius: '5px',
                                alignItems: 'center',
                            }}>
                                <input type="checkbox" style={{ margin: '0.5em', marginRight: 0, width: '2em' }} aria-label="..." checked={atlet_type === 'achievement' ? true : false} onChange={e => this.setState({ atlet_type: atlet_type === 'achievement' ? '' : 'achievement', atlet_subtype: atlet_type !== 'achievemt' ? 'Achievement - FIA International' : '' })} />
                                <select className="form-control form-control-sm" value={atlet_subtype} style={{ borderRadius: '0px' }} onChange={e => this.setState({ atlet_subtype: e.target.value })} disabled={atlet_type !== 'achievement' ? true : false}>
                                    <option >Achievement</option>
                                    <option value="Achievement - FIA International">FIA International</option>
                                    <option value="Achievement - FIA Intenational Progress">FIA Intenational Progress</option>
                                    <option value="Achievement - CIK International">CIK International</option>
                                    <option value="Achievement - CIK Intenational Progre">CIK Intenational Progress</option>
                                    <option value="Achievement - FIM International">FIM International</option>
                                    <option value="Achievement - FIM Intenational Progress">FIM Intenational Progress</option>
                                    <option value="Achievement - Women in Motorsport">Women in Motorsport</option>
                                    <option value="Achievement - International">International</option>
                                    <option value="Achievement - National">National</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{ marginBottom: '1em' }}>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Nama Lengkap :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={nama} onChange={e => this.setState({ nama: e.target.value })} />
                        </div>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Status :</span>
                        </div>
                        <div className="col-md-3">
                            <span className="text-uppercase font-weight-bold" style={{ fontSize: '13px' }}>{this.state.status}</span>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{ marginBottom: '1em' }}>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Alamat :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={alamat} onChange={e => this.setState({ alamat: e.target.value })} />
                        </div>
                        <div className="col-md-1">
                            <span style={{ fontSize: '13px' }}>Gender :</span>
                        </div>
                        <div className="col-md-3">
                            <select className="form-control form-control-sm" value={gender} onChange={e => this.setState({ gender: e.target.value })}>
                                <option value="Mr">Mr.</option>
                                <option value="Mrs">Mrs.</option>
                                <option value="Ms">Miss</option>
                            </select>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{ marginBottom: '1em' }}>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Propinsi :</span>
                        </div>
                        <div className="col-md-3">
                            <select className="form-control form-control-sm" value={prov} onChange={e => this.setState({ prov: e.target.value })}>
                                {
                                    provinces.length > 0 ? provinces.map((province, id) => (
                                        <option value={province.id} key={id}>{province.name}</option>
                                    )) : <option>Provinsi</option>
                                }
                            </select>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{ marginBottom: '1em' }}>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Tim :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={tim} onChange={e => this.setState({ tim: e.target.value })} />
                        </div>
                    </div>

                    <div className="row align-items-center mb-2" style={{ marginBottom: '1em' }}>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Juara :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={juara} onChange={e => this.setState({ juara: e.target.value })} />
                        </div>
                        <div className="col-md-1">
                            <span style={{ fontSize: '13px' }}>Kategori :</span>
                        </div>
                        <div className="col-md-3">
                            <div style={{
                                display: 'flex',
                                width: '100%',
                                border: '1px solid #eee',
                                backgroundColor: '#eee',
                                borderRadius: '5px',
                                alignItems: 'center',
                            }}>
                                <input type="checkbox" style={{ margin: '0.5em', marginRight: 0, width: '2em' }} aria-label="..." checked={category === "Mobil"} onChange={e => this.setState({ category: category === 'Mobil' ? '' : 'Mobil', category_subtype: category !== "Mobil" ? "Mobil - Time rally" : '' })} />
                                <select className="form-control form-control-sm" style={{ borderRadius: '0px' }} value={category_subtype} onChange={e => this.setState({ category_subtype: e.target.value })} disabled={category !== "Mobil" ? true : false}>
                                    <option >Mobil</option>
                                    <option value="Mobil - Time rally">Time rally</option>
                                    <option value="Mobil - Adventure offroad team">Adventure offroad team</option>
                                    <option value="Mobil - Speed offroad">Speed offroad</option>
                                    <option value="Mobil - Adventure offroad individual">Adventure offroad individual</option>
                                    <option value="Mobil - Sprint rally">Sprint rally</option>
                                    <option value="Mobil - Rally">Rally</option>
                                    <option value="Mobil - Karting">Karting</option>
                                    <option value="Mobil - Slalom">Slalom</option>
                                    <option value="Mobil - Balap mobil">Balap mobil</option>
                                    <option value="Mobil - Drag race">Drag race</option>
                                    <option value="Mobil - Drifting">Drifting</option>
                                    <option value="Mobil - Digital motorsport">Digital motorsport</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div style={{
                                display: 'flex',
                                width: '100%',
                                border: '1px solid #eee',
                                backgroundColor: '#eee',
                                borderRadius: '5px',
                                alignItems: 'center',
                            }}>
                                <input type="checkbox" style={{ margin: '0.5em', marginRight: 0, width: '2em' }} aria-label="..." checked={category === "Motor"} onChange={e => this.setState({ category: category === "Motor" ? '' : 'Motor', category_subtype: category === 'Motor' ? '' : 'Motor - Balap motor/motorprix' })} />
                                <select className="form-control form-control-sm" style={{ borderRadius: '0px' }} value={category_subtype} onChange={e => this.setState({ category_subtype: e.target.value })} disabled={category !== "Motor" ? true : false}>
                                    <option >Motor</option>
                                    <option value="Motor - Balap motor/motorprix">Balap motor/motoprix</option>
                                    <option value="Motor - Drag bike">Drag bike</option>
                                    <option value="Motor - Grass track">Grass track</option>
                                    <option value="Motor - Power track">Power track</option>
                                    <option value="Motor - Motorcross">Motocross</option>
                                    <option value="Motor - Supermoto">Supermoto</option>
                                    <option value="Motor - Balap motor/oneprix">Balap motor/oneprix</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row align-items-center mb-2" style={{ marginBottom: '1em' }}>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>No Ponsel :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={phone} onChange={e => this.changePhone(e.target.value)} />
                        </div>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Email :</span>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control form-control-sm" value={email} onChange={e => this.setState({ email: e.target.value })} />
                        </div>
                    </div>
                    <div className="row align-items-center mb-3" style={{ marginBottom: '1em' }}>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Jadwal tiba/Check in :</span>
                        </div>
                        <div className="col-md-3">
                            <SingleDatePicker
                                numberOfMonths={1}
                                date={this.state.check_in}
                                onDateChange={date =>
                                    this.setState({
                                        check_in: date,
                                    })
                                }
                                focused={this.state.check_in_focused}
                                onFocusChange={({ focused }) => this.setState({ check_in_focused: focused })}
                                id="test"
                                placeholder=""
                                small
                            />
                        </div>
                        <div className="col-md-2">
                            <span style={{ fontSize: '13px' }}>Jadwal pulang/Check out :</span>
                        </div>
                        <div className="col-md-3">
                            <SingleDatePicker
                                numberOfMonths={1}
                                date={this.state.check_out}
                                onDateChange={date =>
                                    this.setState({
                                        check_out: date,
                                    })
                                }
                                focused={this.state.check_out_focused}
                                onFocusChange={({ focused }) => this.setState({ check_out_focused: focused })}
                                id="test"
                                placeholder=""
                                small
                            />
                        </div>
                    </div>
                    <div className="row align-items-center mb-3" style={{ marginBottom: '1em' }}>
                        <div className="col-md-10 text-right">
                            <button style={{ width: '150px' }} type="button"
                                className="btn btn-warning text-uppercase font-weight-bold text-white" onClick={this.Regist}>Daftar</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
)(Imiatletform);