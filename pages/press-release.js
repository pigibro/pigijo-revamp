import React, { Component } from 'react';
import Head from '../components/head';
import PageHeader from '../components/page-header';
import { compose } from "redux";
import withAuth from '../_hoc/withAuth';


class MediaRelease extends Component {
    render() {
        return (
            <React.Fragment>
                <Head title={"Press Release"} description={"Press Release"} url={process.env.SITE_ROOT+'/press-release'}/>
                <PageHeader background={"/static/images/img02.jpg"} title={`Press Release`} caption=""/>
                <div className='container'>
                    <div className='row'>

                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Tambahan Informasi atas Rencana Peningkatan Modal Dengan Hak Memesan Efek Terlebih Dahulu</h5>
                                    <p className="card-text">Additional Information on Increase in Capital with Pre-emptive Rights (Rights Issue)</p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/Tambahan+Informasi+atas+Rencana+PMHMETD+PGJO.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>

                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Pemanggilan Rapat Umum Pemegang Saham Luar Biasa ("RUPSLB")</h5>
                                    <p className="card-text">
                                        Pemanggilan RUPSLB PGJO <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/07/Pemanggilan+RUPSLB_PGJO.pdf">ID</a> | <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/07/Pemanggilan+RUPSLB_PGJO_ENG.pdf">EN</a><br/>
                                        Format Surat Kuasa RUPSLB PGJO <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/07/Surat+Kuasa+RUPSLB+PGJO+2020.pdf">ID</a> | <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/07/PoA+EGMS+PGJO+2020.pdf">EN</a><br/>
                                        <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/07/Bahan+RUPSLB+PT+Tourindo+Guide+Indonesia+Tbk.pdf">Bahan RUPSLB</a><br/>
                                        <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/07/CV+Harun+Pandapotan.pdf">CV Harun Pandapotan (Calon Pengawas Baru Perseroan)</a><br/>
                                        <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/07/CV+Supandi+Widi+Siswanto.pdf">CV Supandi Widi Siswanto (Calon Pengawas Baru Perseroan)</a><br/>
                                    </p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/07/Pemanggilan+RUPSLB_PGJO.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>
                        
                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Pengumuman Rencana Peningkatan Modal melalui Hak Memesan Efek Terlebih Dahulu ("HMETD")</h5>
                                    <p className="card-text">Direksi PT Tourindo Guide Indonesia Tbk. dengan ini memberitahukan kepada para pemegang saham bahwa Perseroan berencana untuk melakukan Peningkatkan Modal melalui HMETD dengan informasi sebagaimana terlampir.</p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/Surat+Rencana+Peningkatan+Modal+Dengan+Hak+Memesan+Efek+Terlebih+Dahulu.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>

                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Pengumuman Rapat Umum Pemegang Saham Luar Biasa ("RUPSLB")</h5>
                                    <p className="card-text">Direksi PT Tourindo Guide Indonesia Tbk. dengan ini memberitahukan kepada para pemegang saham bahwa Perseroan berencana untuk menyelenggarakan RUPSLB dengan informasi sebagaimana terlampir. <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/Pengumuman+RUPSLB+PGJO+(English).pdf">EN</a></p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/Pengumuman+RUPSLB+PGJO.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>

                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Laporan Hasil Pelaksanaan Public Expose Tahun 2020</h5>
                                    <p className="card-text">Sehubungan telah diselenggarakannya Public Expose PT Tourindo Guide Indonesia Tbk, berikut ini merupakan Laporan Hasil Pelaksanaan Public Expose Tahun 2020 sebagaimana terlampir. <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/Materi+Public+Expose+Tahun+2020.pdf">Materi Public Expose Tahun 2020</a></p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/Laporan+Hasil+Pelaksanaan+Public+Expose+Tahun+2020.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>

                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Materi Public Expose Tahun 2020</h5>
                                    <p className="card-text">Sehubungan dengan rencana pelaksanaan Public Expose PT Tourindo Guide Indonesia Tbk, berikut ini merupakan Materi Public Expose Tahun 2020 sebagaimana terlampir.</p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/markom/Paparan+Publik+PGJO_Final.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>

                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Pengumuman Rencana Penyelenggaraan Public Expose - Tahunan</h5>
                                    <p className="card-text">PT Tourindo Guide Indonesia Tbk (PGJO) berencana untuk melakukan Public Expose Tahun 2020 dengan informasi sebagaimana terlampir.</p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/markom/Rencana+Penyelenggaraan+Public+Expose+PGJO.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>
                        
                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Ringkasan Risalah RUPST dan RUPSLB Pigijo</h5>
                                    <p className="card-text">Sehubungan dengan penyelenggaraan RUPST dan RUPSLB Pigijo, maka berikut adalah ringkasan risalah Rapat tersebut.</p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/Ringkasan+Risalah+RUPST+dan+RUPSLB+PGJO_20200604.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>

                        <div className='col-sm-4'>
                            <div className="card">
                                <img src="/static/images/empty-image.png" className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Pemanggilan RUPST dan RUPSLB PGJO</h5>
                                    <p className="card-text">
                                        Pemanggilan RUPST & RUPSLB PGJO <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/Pemanggilan+RUPST+dan+RUPSLB+PGJO.pdf">ID</a> | <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/PGJO's+AGMS+and+EGMS+Invitations.pdf">EN</a><br/>
                                        Format Surat Kuasa RUPST & RUPSLB PGJO <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/Format+Surat+Kuasa+RUPST+dan+RUPSLB+PGJO.pdf">ID</a> | <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/Format+PoA+AGMS+and+EGMS+PGJO.pdf">EN</a><br/>
                                        <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/CV+Lilik+Takahashi+(Calon+Pengurus+atau+Pengawas+Baru+Perseroan).pdf">CV Lilik Takahashi (Calon Pengurus / Pengawas Baru Perseroan)</a><br/>
                                        <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/CV+Adi+Putera+(Calon+Pengurus+atau+Pengawas+Baru+Perseroan).pdf">CV Adi Putera (Calon Pengurus / Pengawas Baru Perseroan)</a><br/>
                                    </p>
                                    <a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/rups/Pemanggilan+RUPST+dan+RUPSLB+PGJO.pdf" className="btn btn-primary">See original article</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <style jsx>{`
                        .card img{
                            width: 100%;
                            height: 240px;
                            object-fit: cover;
                        }

                        .card {
                            min-height: 600px;
                        }
                    `}</style>
                </div>
            </React.Fragment>
        );
    }
}

export default compose(
    withAuth(["PUBLIC"])
  )(MediaRelease);