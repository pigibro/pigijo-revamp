import React, { Component } from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import Error from './_error';
import { map, get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import numeral from '../utils/numeral';
import { Link, Router } from '../routes';
import PageHeader from '../components/page-header';
import Loading from '../components/home/loading-flashsale';
import InfiniteScroll from 'react-infinite-scroller';
import { Glyphicon } from 'react-bootstrap';
import withAuth from '../_hoc/withAuth';
import { getActivities, getLocations, getLocationDetail, getDetailCategory } from '../stores/actions';
import { slugify, urlImage, convertToRp, ucFirst } from '../utils/helpers';
import moment from 'moment';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import InputRange from "react-input-range";
import 'react-dates/initialize';
import { DayPickerRangeController, DateRangePicker } from 'react-dates';
import { MobileView } from "react-device-detect";
import IconLink from "../components/shared/iconLink";
import * as gtag from '../utils/gtag';

// Router.events.on('routeChangeComplete', url => gtag.pageview(url))

const _imgStyle = (url) => {
    return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      // backgroundRepeat: 'no-repeat'
      backgroundPosition: ' center center',
    }
  }

class Promo extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          page: 1,
          isLoading: false,
          product_type: [],
          more: true,
          total: 0,
          filter: '',
          location: [],
          duration: [],
          startDate: null,
          sort_by: 'recommended|DESC',
          endDate: null,
          city: null,
          province: null,
          area: null,
          schedule_type: [],
          minMax: { min: 0, max: 25000000 },
          quantity: 2,
          focusedInput: 'startDate',
          btnMobileActive: '',
          promo: 1
        }
      }

      static async getInitialProps({ query, store }) {
        const { slug, type, category } = query;
        let resCategory = false;
        let resLocation = false;
        if (type) {
            resLocation = await store.dispatch(getLocationDetail(ucFirst(type), slug));
            
        }
        const location_detail = get(resLocation, 'data');
        let returnThing = {
            query, 
            slug, 
            type, 
            category, resCategory
        }
        type ? returnThing.location_detail = location_detail : null

        return returnThing
      }

      componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
        const { dispatch } = this.props;
        dispatch(getLocations()).then(result => {
          if (get(result, 'meta.code') === 200){
            this.setState({
              location: get(result, 'data')
            });
          }
        }); 
      }

      componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
      }

      handleTypeFilter = (e) => {
        const { product_type } = this.state;
        let newState = product_type;
        if (product_type.indexOf(e.target.value) < 0) {
          newState.push(e.target.value);
        } else {
          newState.splice(product_type.indexOf(e.target.value), 1);
        }
        this.setState({
          product_type: newState,
        });
      }

      setParams = (isRerender) => {
        const { location_detail, resCategory } = this.props;
        const { page, startDate, endDate, quantity, city, area, province, minMax, schedule_type, sort_by } = this.state;
        let req = {
          'per_page': 16,
          'page': isRerender ? 1 : page,
          'sort_by': sort_by,
          'promo': 1
        };
        if (!isEmpty(resCategory)) {
          req = { ...req, ...{ category_id: resCategory.id } };
        }
        
        if (location_detail) {
          if (location_detail.type === 'Area') {
            req = { ...req, ...{ area: location_detail.id } }
          } else if (location_detail.type === 'Province') {
            req = { ...req, ...{ province: location_detail.id } }
          } else {
            req = { ...req, ...{ city: location_detail.id } }
          }
        }
        if (resCategory) {
    
          req = { ...req, ...{ categoy_id: resCategory.id } }
        }
        if (city !== null) {
          req = { ...req, ...{ city } }
        }
        if (province !== null) {
          req = { ...req, ...{ province } }
        }
        if (area !== null) {
          req = { ...req, ...{ area } }
        }
        if (schedule_type.length > 0) {
          req = { ...req, ...{ schedule_type } }
        }
        return req;
      }

      getActivity = async (isRerender = false) => {
        if (this.state.isLoading) {
          return 'isLoading...';
        }
        const params = this.setParams(isRerender);
        const { dispatch } = this.props;
        const page = isRerender ? 0 : this.state.page;
        let dataTmp = !isRerender ? this.state.data : [];
        if (page > 0) {
          this.setState({ isLoading: true, btnMobileActive: '' })
          await dispatch(getActivities(params)).then(result => {
            if (get(result, 'meta.code') === 200) {
              this.setState({ data: dataTmp.concat(get(result, 'data')), page: page + 1 });
              this.setState({ total: get(result, 'pagination.total') });
              if (page === get(result, 'pagination.last_page')) {
                this.setState({ more: false });
              }
            }
            this.setState({ isLoading: false })
          });
        }
      }

      loadMore = () => {
        this.getActivity(false);
      }
    
      changeShow = (filter) => {
        this.setState({ filter });
      }

      handleClickOutside = event => {
        const location = ReactDOM.findDOMNode(this.refs.location);
        if (!location || !location.contains(event.target)) {
          const dates = ReactDOM.findDOMNode(this.refs.dates);
          if (!dates || !dates.contains(event.target)) {
            const guests = ReactDOM.findDOMNode(this.refs.guests);
            if (!guests || !guests.contains(event.target)) {
              const price = ReactDOM.findDOMNode(this.refs.price);
              if (!price || !price.contains(event.target)) {
                const duration = ReactDOM.findDOMNode(this.refs.duration);
                if (!duration || !duration.contains(event.target)) {
                  const sort = ReactDOM.findDOMNode(this.refs.sort);
                  if (!sort || !sort.contains(event.target)) {
                    this.setState({ filter: '' });
                  }
                }
              }
            }
          }
        }
      }

      handleLocation = (value) => {
        if (value.length > 0) {
          if (value[0].type === 'Area') {
            this.setState({ area: value[0].id, city: null, province: null, data: [], page: 1, more: true }, () => {
            });
            Router.push(`/promo/area/${value[0].slug}`);
          } else if (value[0].type === 'Kabupaten' || value[0].type === 'Kota') {
            this.setState({ area: null, city: value[0].id, province: null, data: [], page: 1, more: true }, () => {
            });
            Router.push(`/promo/kota/${value[0].slug}`);
          } else {
            this.setState({ area: null, city: null, province: value[0].id, data: [], page: 1, more: true }, () => {
            });
            Router.push(`/promo/province/${value[0].slug}`);
          }
        }
      }
    render() {
        const { slug, type, errorCode, location_detail, category } = this.props;
        const { product_type, btnMobileActive, schedule_type, quantity, minMax, data, total, pagination, more, filter, location, startDate, endDate, focusedInput, page } = this.state;
        const loopTo = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        if (errorCode > 200) {
            return (<Error statusCode={errorCode} />);
        }
        return (
            <div>
                {/* process.env.SITE_ROOT + '/promo/' + location_detail ?  `${location_detail.type}/${location_detail.slug}` : ''} */}
                <Head title='Pigijo - Promotion' url={`${process.env.SITE_ROOT}/promo/${location_detail ? `${location_detail.type}/${location_detail.slug}` : ''}`} ogImage={location_detail ? urlImage(location_detail.image_path + '/large/' + location_detail.image_filename) : "/static/images/img01.jpg"} />
                <PageHeader background={location_detail ? urlImage(location_detail.image_path + '/' + location_detail.image_filename) : "/static/images/img01.jpg"} title={`Find Your Activity With The Best Price`} />
                <section className="main-box sticky">
                    <div className="container">
                        <ul id="header-tabs">
                            <li className="tabs-item">
                                <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({filter:'location'})}>Location</button>
                                <div ref='location' className={`p__filter ${filter === 'location' ? 'show' : 'hide'}`} >
                                <SearchAutoComplete
                                    id="destination"
                                    key={"select-single"}
                                    index={0}
                                    data={location || []}
                                    placeholder="Select City"
                                    labelKey="name"
                                    onChange={this.handleLocation}
                                    bsSize="large"
                                />
                                </div>
                            </li>
                            <li className="tabs-item">
                                <button className="btn btn-sm btn-o btn-light" onClick={() => this.setState({ filter: 'sort' })}>Sort By</button>
                                <div ref='sort' className={`p__filter ${filter === 'sort' ? 'show' : 'hide'}`}>
                                    <div className="sort-desktop" onClick={() => { this.setState({ sort_by: 'created_at|DESC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                        Newly listed
                                        </div>
                                    <div className="sort-desktop" onClick={() => { this.setState({ sort_by: 'price|DESC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                        Price: high to low
                                        </div>
                                    <div className="sort-desktop" onClick={() => { this.setState({ sort_by: 'price|ASC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                        Price: low to high
                                        </div>
                                    <div className="sort-desktop" onClick={() => { this.setState({ sort_by: 'schedule|ASC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                        Upcoming schedule
                                        </div>
                                    <div className="sort-desktop" onClick={() => { this.setState({ sort_by: 'recommended|DESC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                        Recommended
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
                {
                    btnMobileActive !== '' &&
                    <MobileView>
                        <div className="p__fitler--mobile-content">
                            <a className="btn-close" onClick={e => { e.preventDefault; this.setState({ btnMobileActive: '' }) }}><i className="ti-close" /></a>
                            {
                                btnMobileActive === 'filter' &&
                                <>
                                    <div className="form-group">
                                        <span className="text-label">Location</span>
                                        <SearchAutoComplete
                                        id="destination-mobile"
                                        key={"select-single"}
                                        index={0}
                                        data={location || []}
                                        placeholder="Select City"
                                        labelKey="name"
                                        onChange={this.handleLocation}
                                        bsSize="large"
                                        />
                                    </div>
                                </>
                            }
                            {
                                btnMobileActive === 'sort' &&
                                <>
                                <div className="sort-mobile mt2" onClick={() => { this.setState({ sort_by: 'created_at|DESC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                    Newly listed
                                </div>
                                <div className="sort-mobile" onClick={() => { this.setState({ sort_by: 'price|DESC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                    Price: high to low
                                </div>
                                <div className="sort-mobile" onClick={() => { this.setState({ sort_by: 'price|ASC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                    Price: low to high
                                </div>
                                <div className="sort-mobile" onClick={() => { this.setState({ sort_by: 'schedule|ASC', data: [], page: 1, more: true }, () => { this.getActivity(true) }) }}>
                                    Upcoming schedule
                                </div>
                                </>
                            }
                        </div>
                    </MobileView>
                }
                {
                    process.browser &&
                    <MobileView>
                        <section className="p__filter--mobile">
                        <div className="btn-group">
                            <button className="btn btn-sm btn-primary" onClick={() => this.setState({ btnMobileActive: 'filter' })}>Filter</button>
                            <button className="btn btn-sm btn-primary btn-o bg-white" onClick={() => this.setState({ btnMobileActive: 'sort' })}>Sort</button>
                        </div>
                        </section>
                    </MobileView>
                }

                <section className="body-content mt2">
                    <div className="container">
                        <div className="row">
                            {
                                location_detail &&
                                <div className="col-md-12">
                                    <h4 className="text-title">Found {total} in {location_detail.name}</h4>
                                </div>
                            }
                            <div className="product-list">
                            <InfiniteScroll 
                                pageStart={page} 
                                initialLoad 
                                loader={<div style={{ padding: "0 .99em" }} 
                                key={'loading'}><Loading /></div>} 
                                useWindow={true} 
                                loadMore={this.loadMore} 
                                hasMore={more} 
                                threshold={1000}>
                                    {
                                        map(data, (item, index) => (
                                            <div key={`p-${index}`}>
                                                {
                                                    // item.discount > 0 ? (
                                                        <div className="col-md-3 col-sm-6 col-xs-6 p-item" >
                                                            <Link route="activity" params={{ slug: item.slug }}>
                                                                <a>
                                                                    <div className="plan-item sr-btm">
                                                                        <div className="box-img plan-img">
                                                                            <div className="thumb" style={_imgStyle(urlImage(item.image_path + '/' + item.image_filename))} />
                                                                            <div className="box-img-top">
                                                                            {!isEmpty(item.city) && <span><i className="ti-pin" /> {item.city.name}</span>}
                                                                            </div>
                                                                            <div className="box-img-bottom">
                                                                            {item.discount > 0 && <p className="p_discount">{Math.round(item.discount)}% OFF</p>}
                                                                            </div>
                                                                        </div>
                                                                        <div className="plan-info">
                                                                            <ul>
                                                                            <li className="title"><h3 title={item.name}>{item.name}</h3></li>
                                                                            <li className="min-person">
                                                                                <p className="text-label">Min {item.min_person} person{item.min_person === 1 ? '' : 's'}</p>
                                                                            </li>
                                                                            <li className="price">
                                                                                {item.discount > 0 && <p className="price-before"><del>Rp {numeral(parseInt(item.price_before, 10)).format('0,0')}</del></p>}
                                                                                <p>
                                                                                <span className="price-latest">
                                                                                { item.product_category !== 'Local Assistants'?
                                                                                    <><b className="text-primary">Rp {numeral(parseInt(item.price,10)).format('0,0')}</b><span className="text-person">/person</span></>:
                                                                                    <><b className="text-primary">Rp {numeral(parseInt(item.price,10)).format('0,0')}</b><span className="text-person">/day</span></>
                                                                                }
                                                                                </span>
                                                                                <span className={moment(item.upcomming_schedule).format("YYYY-MM-DD") === moment().format('YYYY-MM-DD') || moment(item.upcomming_schedule).format("YYYY-MM-DD") === moment().add(1, 'days').format('YYYY-MM-DD') ? 'notice text-success' : 'notice text-info'}>
                                                                                    {moment(item.upcomming_schedule).format("YYYY-MM-DD") === moment().format('YYYY-MM-DD') &&
                                                                                    'Available Today'
                                                                                    }
                                                                                    {moment(item.upcomming_schedule).format("YYYY-MM-DD") === moment().add(1, 'days').format('YYYY-MM-DD') &&
                                                                                    'Available Tommorow'
                                                                                    }
                                                                                    {moment().add(1, 'days').valueOf() < moment(item.upcomming_schedule).valueOf() &&
                                                                                    `Available from ${moment(item.upcomming_schedule).format("DD MMMM YYYY")}`
                                                                                    }
                                                                                </span>
                                                                                </p>
                                                                            </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </Link>
                                                        </div>
                                                    // ) : null
                                                }
                                            </div>  
                                        ))
                                    }
                            </InfiniteScroll>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
  )(Promo);