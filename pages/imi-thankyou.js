import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link, Router } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
      opacity: 0.8,
  }
}

class ThankYou extends React.Component {
  static async getInitialProps({ query, store}) {
      return { query}
  }
    componentDidMount(){
    //   const {query} = this.props;
    //   switch (query.status_code) {
    //       case '200': 
    //         break
    //       case '201':
    //           Router.push('/awaiting-payment');
    //           break;
    //       default:
    //           Router.push('/payment-failed');
    //           break;
    //   }
    }
    render(){
      return (
        <>
          <Head title={"Complete Payment"} description={"Hubungi Pusat Bantuan Pigijo untuk mendapatkan informasi lengkap dan jelas mengenai rencana perjalanan anda, status pembayaran, dan cara bergabung menjadi partner pigijo."} url={process.env.SITE_ROOT+'/how-to-book'}/>
          <PageHeader background={"/static/images/img05.jpg"} title={`Complete Payment`} caption=""/>
          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-8 col-lg-push-2 col-md-8 col-md-push-2 thankyou">
                  <h2 className="h3 mb1">
                    <center>THANK YOU</center>
                  </h2>
                  <p className="thankyou-content">Please check your activation code on your email for your QR code.</p>
                  <Link to="/"><a className="btn btn-primary thankyou-button">Find more activity</a></Link>
                </div>
              </div>
            </div>
          </section>

          <section className="content-wrap bg-content-home">
            <div className="container">
              <div className="row flex-row flex-center">
                <div className="col-md-6 col-md-offset-3">
                  <h2 className="h4 mt1 mb1 text-center">
                   Need Help ?
                  </h2>
                  <p>
                  Don't receive e-receipt from pigijo ? our customer service are ready to help you find your way around by click our chat
                  </p>
                </div>
              </div>
            </div>
          </section>
        </>
      )
    }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(ThankYou);