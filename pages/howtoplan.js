import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: 'cover',
    backgroundPosition: ' center center',
    opacity: 0.8,
  }
}

class HowToPlan extends React.Component {
  render() {
    return (
      <>
        <Head title={"How To Plan"} description={"Hubungi Pusat Bantuan Pigijo untuk mendapatkan informasi lengkap dan jelas mengenai rencana perjalanan anda, status pembayaran, dan cara bergabung menjadi partner pigijo."} url={process.env.SITE_ROOT + '/how-to-plan'} />
        <PageHeader background={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/how-to-plan.jpg"} title={``} caption="" />
        <section className="content-wrap bg-content-home">
          <div className="container">
            <ul className="nav nav-tabs">
              <li className="active" style={{ marginRight: '4em' }}><a data-toggle="tab" href="#tab1">How to Plan</a></li>
              <li><a data-toggle="tab" href="#tab2">How to Book</a></li>
            </ul>
            <div className="tab-content">
              <div id="tab1" className="tab-pane fade in active">
                <section className="content-wrap bg-content-home" style={{ marginBottom: '5em' }}>
                  <div className="container">
                    <p className="top-title">ADD TO PLAN</p>
                    <p className="top-text">We will guide you on how to create day-to-day itinerary for your
                    vacation in Indonesia from our places, local experiences, homestays, and vehicles. If you need a tour guide you can add travel assistants to your plan.</p>

                    <p className="top-title2">CREATE ITINERARY: 2 DAYS 1 NIGHT IN LOMBOK</p>
                    <p className="top-text2">Places &amp; activities: surfing, Pantai Tanjung Aan, Ashtori Lounge. Amenities: homestay, car rental. Add travel assistant as tour guide.</p>

                    <div className="row content-row-1">
                      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/1.1.png" alt="1" />
                      </div>
                      <div className="col-lg-11 col-md-11 col-sm-11 col-xs-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">SEARCH</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Let say, there are 2 people that will go on vacation (In reality, there could be less or more people). Start your search on the Pigijo Home Page using keyword “Lombok”</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/search_book.png" />
                      </div>
                    </div>

                    <div className="row content-row-1">
                      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.1.png" alt="2" />
                      </div>
                      <div className="col-lg-11 col-md-11 col-sm-11 col-xs-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">CHOOSE PRODUCT &amp; ADD TO PLAN</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>There are 5 option of products (Tour, Assistants, Places, Homestay, Vehicle) that you can choose for your itinerary plans.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1" style={{ marginTop: '1em', textAlign: 'right', fontSize: '18px' }}>
                        <p>2.1</p>
                      </div>
                      <div className="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                        <p className="icon-content-text-odd">“Tour” provides all-in activities in Lombok. Select
                        “Surfing Trip in Lombok” for the first day in Lombok or other activity do you want. In
                        “Surfing Trip in Lombok” you will visit Sade Village, Grupuk Beach, Seger Beach, Islamic Centre, and Malimbu Hill from 7.30 AM until 18.00 PM. In this activity already included Boat and Car for the trip,
                        you don’t need to rent a car.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.1.1.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">Set the date of activity on February 22 2020 and the number of reservations is two people.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.1.2.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">Click “select” on the date you choose and make sure the schedule and number of persons is right, and then “add to plan”. Click “Back” for choose another product.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.1.3.png" />
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1" style={{ marginTop: '1em', textAlign: 'right', fontSize: '18px' }}>
                        <p>2.2</p>
                      </div>
                      <div className="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                        <p className="icon-content-text-odd">In “Assistants” there are local people that can accompany you in Lombok. Select “Agus Sujono” as an example, he is very familiar with Lombok and its hidden treasure or select another Travel Assistant do you preferred.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.2.1.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">Set the date of activity on February 23, 2020 and the number of reservations is two people.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.2.2.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">Click “select” on the date you choose and make sure the schedule and number of persons is right, and then “add to plan”. Click “Back” for choose another product.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.2.3.png" />
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1" style={{ marginTop: '1em', textAlign: 'right', fontSize: '18px' }}>
                        <p>2.3</p>
                      </div>
                      <div className="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                        <p className="icon-content-text-odd">In “Homestay” you can select budget accommodations close to the location of your selected destinations. Set a schedule 22-23 February 2020 and select “Homestay Fass Inn” as an example.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.3.1.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">Choose room type by clicking “Select”. Make sure the plan summary is right, and then “add to plan”. Click “Back” for choose another product.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.3.2.png" />
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1" style={{ marginTop: '1em', textAlign: 'right', fontSize: '18px' }}>
                        <p>2.4</p>
                      </div>
                      <div className="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                        <p className="icon-content-text-odd">“Vehicle” will help you move around during your trip in Lombok. First set a schedule on 23 February 2020, then select “Avanza (DR 1645 MZ)” to take us to destination locations. Click the product to see details of the product and click “add to plan”.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.4.1.png" />
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1" style={{ marginTop: '1em', textAlign: 'right', fontSize: '18px' }}>
                        <p>2.5</p>
                      </div>
                      <div className="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                        <p className="icon-content-text-odd">Pigijo has “Places” menu that provide options of tourism interest and culinary locations to add on your itinerary plans. You can visit “Pantai Tanjung Aan” to see the beauty of the sunrise in your second day in Lombok. You must to visit the beach before the sun comesor you can select another place you want to visit.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.5.1.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">You can read the description, chose the interest spot. Click “Select” for set a schedule.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.5.2.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">Choose date &amp; time of activity on February 23 2020 and then “add to plan”. Click “Back” for choose another places for the second day in Lombok.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.5.3.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">After visit “Pantai Tanjung Aan”, you can visit another places for lunch, example “Ashtari Lounge and Kitchen”. Click “Select” for set a schedule.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.5.4.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ textAlign: 'center' }}>
                        <p className="icon-content-text-odd">Choose date &amp; time of activity on February 23 2020 and then “add to plan”. Click “Back” for choose another product.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/2.5.5.png" />
                      </div>
                    </div>

                    <div className="row content-row-1">
                      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/3.1.png" alt="3" />
                      </div>
                      <div className="col-lg-11 col-md-11 col-sm-11 col-xs-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">REVIEW AND FINALIZE YOUR PLAN</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>You can review all selected items in your plan summary. If you want to add another product in Lombok you can repeat the previous steps. Finalized your plan and make it come true with Pigijo by click Checkout.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/how-to-plan/review.png" />
                      </div>
                    </div>
                  </div>
                </section>
              </div>
              <div id="tab2" className="tab-pane fade">
                <section className="content-wrap bg-content-home">
                  <div className="row header-icon">
                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                      <a href="#search">
                        <img src="/static/images/component1.svg" alt="Search" />
                      </a>
                      <p>Search</p>
                    </div>
                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                      <a href="#select">
                        <img src="/static/images/component2.svg" alt="Select" />
                      </a>
                      <p>Select</p>
                    </div>
                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                      <a href="#choose">
                        <img src="/static/images/component3.svg" alt="Choose Product" />
                      </a>
                      <p>Choose Packages</p>
                    </div>
                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                      <a href="#checkout">
                        <img src="/static/images/component4.svg" alt="Checkout or Add to Plan" />
                      </a>
                      <p>Checkout or Arrange Itinerary</p>
                    </div>
                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                      <a href="#fill">
                        <img src="/static/images/component5.svg" alt="Fill in Contact Person" />
                      </a>
                      <p>Fill In Contact Person</p>
                    </div>
                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                      <a href="#payment">
                        <img src="/static/images/component6.svg" alt="Payment" />
                      </a>
                      <p>Payment</p>
                    </div>
                  </div>
                </section>

                <section className="content-wrap bg-content-home">
                  <div className="container">
                    <hr />
                  </div>
                </section>

                <section className="content-wrap bg-content-home" style={{ marginBottom: '5em' }}>
                  <div className="container">
                    <div className="row content-row-1" id="search">
                      <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="/static/images/1.png" alt="1" />
                      </div>
                      <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">SEARCH</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Start your search on
                        the Pigijo Home Page. You can type anything related to your journey, we will show all everything we
                got on it.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="/static/images/new_search.png" />
                      </div>
                    </div>

                    <div className="row content-row-1" id="select">
                      <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="/static/images/2.png" alt="2" />
                      </div>
                      <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">SELECT</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Tour, Assistances, Places, Homestay, and Vehicle will be shown on the search results page. You can filter the product categories that you want. Click on the product’s card for more detailed info.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="/static/images/new_select1.png" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ textAlign: 'center' }}>
                        <img src="/static/images/new_select2.png" />
                      </div>
                    </div>

                    <div className="row content-row-1" id="choose">
                      <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="/static/images/3.png" alt="3" />
                      </div>
                      <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">CHOOSE PACKAGES</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Choose the schedule
                        and number of persons going with you, make sure that all are good as your planned, then you
                can click select.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="/static/images/new_choose.png" />
                      </div>
                    </div>

                    <div className="row content-row-1" id="checkout">
                      <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="/static/images/4.png" alt="4" />
                      </div>
                      <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">CHECKOUT OR ARRANGE ITINERARY</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>After select product you can click Checkout for booking the package or click Add to Plan if you want arrange itinerary.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="/static/images/new_checkout.png" />
                      </div>
                    </div>

                    <div className="row content-row-1" id="fill">
                      <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="/static/images/5.png" alt="5" />
                      </div>
                      <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">FILL IN CONTACT PERSON</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>Fill in the contact person detail for this
                        booking. This contact person will be in charge for this booking, so please make sure
                the contact person detail is reachable.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="/static/images/new_fill1.png" />
                      </div>
                    </div>

                    <div className="row content-row-1" id="payment">
                      <div className="col-lg-1 icon-number-odd" style={{ marginTop: '3em' }}>
                        <img width="30em" src="/static/images/6.png" alt="6" />
                      </div>
                      <div className="col-lg-11" style={{ marginTop: '3em', padding: '0' }}>
                        <p className="icon-content-header-odd">PAYMENT</p>
                        <p className="icon-content-text-odd" style={{ paddingRight: '3em' }}>There are variety of payment choices you can select to meet your preference.</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 pic-content" style={{ marginTop: '3em', textAlign: 'center' }}>
                        <img src="/static/images/new_payment.png" />
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>

        <style jsx global>{`
          .header-icon {
            padding: 3% 10% 0% 10%;
            margin-right: 0%;
            text-align: center;
          }
          
          .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
            border: 0;
            border-bottom: 3px solid #FF5B00;
            font-weight: bold;
          }

          .tab-content{
            border: 0;
          }

          .header-icon p {
            font-family: Poppins Medium;
            font-size: 15px;
            color: black;
            margin-top: 10px;
          }
          
          hr {
            border-top: 2px solid whitesmoke;
            margin-top: 50px;
            width : 95%;
          }

          .pic-content img{
            width: 70%;
          }

          .btn-primary-outline {
            background-color: transparent;
            border-color: transparent;
          }

          .icon-content-text-odd-title{
            margin-top: 0;
            font-family: Poppins Medium;
            font-size: 15px;
            text-align: right;
          }

          @media (max-width: 351px) {
            .navbar {
                margin: -20px 0 0 50px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 100px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-list img {
                width: 50%;
                margin-bottom: 10px;
                margin-left: 50px;
            }
        
            /* odd */
            .icon-number-odd {
                margin-left: 30px;
                text-align: left;
            }

            .top-title {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 20px;
              text-align: center;
            }

            .top-text {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: center;
            }

            .top-title2 {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 60px;
              text-align: left;
            }

            .top-text2 {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: left;
            }
        
            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        
            /* even */
            .icon-number-even {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        }
        
        @media (min-width: 352px) and (max-width: 575px) {
            .navbar {
                margin: -20px 0 0 50px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 80px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-list img {
                width: 50%;
                margin-bottom: 10px;
                margin-left: 50px;
            }
        
            /* odd */
            .icon-number-odd {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-number-odd img {
                width: 10%;
            }
            
            .top-title {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 20px;
              text-align: center;
            }

            .top-text {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: center;
            }

            .top-title2 {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 60px;
              text-align: left;
            }

            .top-text2 {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: left;
            }

            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        
            /* even */
            .icon-number-even {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        }
        
        @media (min-width: 576px) and (max-width: 767px) {
            .navbar {
                margin: -20px 0 0 80px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 110px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-list img {
                width: 50%;
                margin-bottom: 10px;
                margin-left: 50px;
            }
        
            .icon-number-odd {
                margin-left: 30px;
                text-align: left;
            }
        
            .top-title {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 20px;
              text-align: center;
            }

            .top-text {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: center;
            }

            .top-title2 {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 60px;
              text-align: left;
            }

            .top-text2 {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: left;
            }

            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        
            .icon-number-even {
                margin-left: 30px;
                text-align: left;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
        }
        
        @media (min-width: 768px) and (max-width: 991px) {
            .navbar {
                margin: -45px 0 0 100px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 120px;
            }
        
            .icon-list img {
                width: 60%;
                margin-bottom: 10px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-number-odd {
                text-align: left;
            }
            
            .top-title {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 20px;
              text-align: center;
            }

            .top-text {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: center;
            }

            .top-title2 {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 60px;
              text-align: left;
            }

            .top-text2 {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: left;
            }

            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
            }
        
            .icon-number-even {
                text-align: left;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
            }
        }
        
          @media (min-width: 992px) and (max-width: 1199px) {
            .navbar {
                margin: -45px 0 0 100px;
            }
        
            .header {
                font-size: 40px;
            }
        
            .header-icon img {
                width: 120px;
            }
        
            .icon-list img {
                width: 60%;
                margin-bottom: 10px;
                margin-left: 50px;
            }
        
            .contents-lg {
                display: none;
            }
        
            .contents-md {
                display: block;
            }
        
            .icon-number-odd {
                text-align: left;
                margin-left: 50px;
            }

            .top-title {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 20px;
              text-align: center;
            }

            .top-text {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: center;
            }
        
            .top-title2 {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 60px;
              text-align: left;
            }

            .top-text2 {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: left;
            }

            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: 50px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: 50px;
            }
          }

          @media (min-width: 1200px) {
            .navbar {
                margin: -45px 0 0 150px;
            }
        
            .header {
                font-size: 60px;
            }
        
            .header-icon img {
                width: 120px;
            }
        
            .icon-list img {
                width: 80%;
            }
        
            .contents-md {
                display: none;
            }
        
            .contents-lg {
                display: block;
            }
        
            .contents-lg .row {
                margin-bottom: 0px;
            }
        
            .dash-1 {
                text-align: right;
            }
        
            .dash-2 {
                margin-left: -133px;
            }
        
            .icon-number-odd {
                text-align: right;
            }
            
            .top-title {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 20px;
              text-align: center;
            }

            .top-text {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: center;
            }

            .top-title2 {
              font-family: Poppins Bold;
              font-size: 24px;
              color: #939393;
              margin-top: 60px;
              text-align: left;
            }

            .top-text2 {
              font-family: Poppins Medium;
              font-size: 15px;
              color: #969696;
              margin-top: 10px;
              text-align: left;
            }

            .icon-content-header-odd {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #969696;
                margin-top: 10px;
            }
        
            .icon-content-text-odd {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
            }
        
            .icon-number-even {
                text-align: right;
                margin-left: -12%;
            }
        
            .icon-content-header-even {
                font-family: Poppins Bold;
                font-size: 24px;
                color: #939393;
                margin-top: 8px;
                margin-left: -12%;
            }
        
            .icon-content-text-even {
                font-family: Poppins Medium;
                font-size: 15px;
                color: #969696;
                margin-top: 10px;
                margin-left: -12%;
            }
          }
      `}</style>
      </>
    )
  }
}


export default compose(
  connect(),
  withAuth(["PUBLIC"])
)(HowToPlan);