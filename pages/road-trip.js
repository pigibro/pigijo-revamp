import React, { Component } from 'react';
import Head from '../components/head';
import PageHeader from '../components/page-header';
import { compose } from "redux";
import withAuth from '../_hoc/withAuth';
import { modalToggle } from '../stores/actions';
import { Link, Router } from "../routes";

class roadTrip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            auth: false
        }
    }

    getFile = () => {
        if(this.props.token !== null) {
            window.open('https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/road_trip_bandung.pdf', '_blank')
        } else {
            this.props.dispatch(modalToggle(true, 'auth'))
        }
    }

    // componentDidUpdate(prevProps) {
    //     if(prevProps.token !== this.props.token) {
    //         this.getFile()
    //     }
    // }
    render() {
        return (
            <React.Fragment>
                <Head title={"Promo Valentine"} description={"Promo Valentine"} url={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/banner_valentine.png"}/>
                <PageHeader background={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/banner_valentine.png"} title={`Promo Valentine`} caption="untuk semua destinasi local experience"/>
                <section className="content-wrap bg-content-home">
                        <div className='container'>
                        <div className='row'>
                            <div className='col-sm-1'/>
                            <div className='col-sm-10'>
                                <div className="row" style={{display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', textAlign: 'center'}}>
                                        <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/voucher_valentine.png" style={{width: '53%', marginTop: '3em', marginBottom: '2em', cursor: 'grab'}} onClick={() => Router.push('/promo')}/>
                                        <p style={{width: '100%'}}>Hi travelers! buat kamu yang ingin traveling merayakan hari kasih sayang di bulan Februari  bisa banget nih, karena Pigijo akan memberikan potongan Rp 500.000,- untuk semua local experience special untuk kamu di bulan penuh cinta ini.</p>
                                </div>
                                
                                
                                <h4>Terms and Condition</h4>
                                <ol className='text-ol'>
                                    <li>Voucher Potongan Rp 500.000 dapat digunakan dengan nilai minimum per transaksi Rp 1.000.000 melalui website atau apps Pigijo (tidak berlaku kelipatan)  </li>
                                    <li>Periode transasksi   : 3 – 29 Februari 2020</li>
                                    <li>Periode perjalanan : 10 Februari - 30 April 2020</li>
                                    <li>Voucher dapat di akses di website <Link route="/">Pigijo.com</Link></li>
                                    <li>Hubungi Pigijo Customer Service <a href="https://api.whatsapp.com/send?phone=6281284289434">081284289434</a> melalui DM/WA/Line</li>
                                    <li>Voucher promo tidak dapat digabungkan dengan produk diskon lainnya dan tidak dapat dipindahtangankan.</li>
                                    <li>Perjalanan tidak bisa dibatalkan dan dikembalikan dananya.</li>
                                </ol>
                                <div className='clearfix' style={{'padding': '10px'}}></div>

                                <Link route="/promo" style={{color: '#fff', fontWeight: 'bold'}}>
                                    <div 
                                    // onClick={this.getFile}
                                    style={{padding: '0.7em 3.7em', 
                                    display: 'inline-block', 
                                    marginLeft: '2em',
                                    marginBottom: '1em', 
                                    borderRadius: '5px', 
                                    cursor: 'grab',
                                    backgroundColor: '#efc59f',
                                    color: '#fff',
                                    borderRadius: '5px'}}>
                                        <p style={{margin: 0}}>Get Now</p>
                                    </div>
                                </Link>
                                

                                {/* <h5>Untuk pendaftaran Road Trip dapat menghubungi: <a href='tel:6282246598802'>0812-8428-9434</a></h5>
                                <p><strong>Format Pendaftaran: </strong></p>
                                <ul className='text-ol' style={{listStyleType: 'none'}}>
                                    <li><p>Nama: </p></li>
                                    <li><p>Jumlah anggota keluarga/grup: </p></li>
                                    <li><p>Asal Berangkat: Jakarta</p></li>
                                    <li><p>Tanggal Keberangkatan: </p></li>
                                    <li><p>No HP: </p></li>
                                    <li><p>Email: </p></li>
                                </ul> */}
                                
                                {/* {
                                    this.props.token !== null ? (
                                        <div 
                                        onClick={this.getFile}
                                        style={{padding: '0.5em 1em', 
                                        border: '1px solid #e17306', 
                                        display: 'inline-block', 
                                        marginBottom: '1em', 
                                        borderRadius: '5px', 
                                        cursor: 'grab'}}>
                                            <p><a href="https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/road_trip_bandung.pdf" target="_blank">Download the pdf here <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/pdf-icon.jpg' width='24'/></a></p>
                                        </div>
                                        
                                    ) : (
                                    <div 
                                    onClick={() => this.props.dispatch(modalToggle(true, 'auth'))} 
                                    style={{padding: '0.5em 1em', 
                                        border: '1px solid #e17306', 
                                        display: 'inline-block', 
                                        marginBottom: '1em', 
                                        borderRadius: '5px', 
                                        cursor: 'grab'}}>
                                        <p ><a>For Download road trip file please login</a></p>
                                    </div>
                                    ) 
                                } */}

                            </div>
                        </div>
                        <style jsx>{`
                            ol.text-ol li{
                                line-height: 30px;
                            }
                        `}</style>
                    </div>
                </section>
                
            </React.Fragment>
        );
    }
}

export default compose(
    withAuth(["PUBLIC"])
  )(roadTrip);