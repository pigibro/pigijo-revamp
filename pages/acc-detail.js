import { compose } from "redux";
import { connect } from "react-redux";
import Error from "./_error";
import { get, isEmpty, merge, map, range, first } from "lodash";
import moment from "moment";
import Head from "../components/head";
import { slugify, urlImage, convertToRp, hitungSelisih } from "../utils/helpers";
import React from "react";
import withAuth from "../_hoc/withAuth";
import { getActivity, modalToggle } from "../stores/actions";
import PageHeader from "../components/page-header";
import numeral from '../utils/numeral';
import Content from "../components/activity/content";
import "react-dates/initialize";
import { DayPickerRangeController } from "react-dates";
import { getSchedules,storeCartPlaces, getCartList, getDetailGuestHouse, getLocations, getLocationUser, storeCartHomestay, createToken, praCheckout, setSuccessMessage, setErrorMessage, getHotelPage, storeCartHotel, getCancelPolicyHotel } from "../stores/actions";

import Maps from "../components/shared/map";
import Router from 'next/router';

import { detail } from "../components/accommodation/tes";
import ListRoom from "../components/accommodation/detail/list";
import Comment from "../components/shared/comment";
import FilterAcc from "../components/accommodation/filterAcc";
import ShareSoc from "../components/shared/shareSosmed";
import SideRight from "../components/accommodation/detail/SideRight";
import Loading from '../components/shared/loading';

const _imgStyle = (url) => {
	return {
		backgroundImage: `url(${url})`,
		backgroundSize: "cover",
		backgroundPosition: " center center",
	};
};
class AccommodationDetail extends React.Component {
	static async getInitialProps({ query, store}) {
		const { slug, type } = query;
		if(type === 'homestay') {			
			const currentState = store.getState();
			const accommodation = get(currentState, 'guestHoust.rooms');
			let errorCode = false;
			if (isEmpty(accommodation)){
			const resProduct = await store.dispatch( getDetailGuestHouse('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAwLCJsb2dpbiI6ZmFsc2V9.TxpSsxxz6VHPQ8F86B1fvQx54118rP_ssc8T2qlZPd8', slug, query) );
			errorCode = get(resProduct,'meta.code') > 200 ? 404 : false;
			}
			return { errorCode,  query }
		}
	    
		return { query }
	}

	constructor(props) {
		super(props);
		this.state = {
			hotel: {},
			stickyWidth: 0,
			startDate: null,
			endDate: null,
			focusedInput: "startDate",
			showCalendar: false,

			isShow: null,

			destination: null,
			node: null,

			minMax: {
				min: 0,
				max: 1500000,
			},

			person: 1,
			error: "",
			selectedRoom: null,
			room: 1,
			isModal: false,
			rooms: [],
			HotelPolicy: [
				{
					"name": "All rooms",
					"tier_one": "Pembatalan maximum 1 Hari sebelum kedatangan",
					"tier_two": "Pembatalan maximum 1 Hari sebelum kedatangan"
				}
			],
			room_total : 0,
			start_from: 0,
			facilities: [],
			isLoading: false
		};
	}

	getHotelPagination = (e) => {
		this.setState({isLoading: true})
		if (!isEmpty(e)) {
			e.preventDefault();
		}
		const {dispatch, query} = this.props;
		let param = {
			CorrelationId: query.CorrelationId,
			Page: 1,
			HotelName: query.slug
		}
		let icon = {
			LDRY: {
				name: 'Laundry',
				icon: require('../static/icons/laundry.png')
			},
			PRK: {
				name: 'Park',
				icon: require('../static/icons/park.png')
			},
			RST: {
				name: 'Restaurant',
				icon: require('../static/icons/restaurant.png')
			},
			RSVC: {
				name: 'Room Service',
				icon: require('../static/icons/roomservice.png')
			},
			SPA: {
				name: 'Spa', 
				icon: require('../static/icons/spa.png')
			},
			POOL: {
				name: 'Swimming Pool',
				icon: require('../static/icons/swimming.png')
			},
			WIFI: {
				name: 'Wifi', 
				icon: require('../static/icons/wifi.png')
			},
		}
		dispatch(getHotelPage(param))
		.then(res => {
			let data = res.data
			let fac = []
			data.Hotels[0].business_uri = data.Hotels[0].HotelName
			data.Hotels[0].name = data.Hotels[0].HotelName
			data.Hotels[0].photo_primary = data.Hotels[0].ImageUri
			data.Hotels[0].province_name = data.Hotels[0].Area
			data.Hotels[0].price = data.Hotels[0].AveragePrice
			for(let j = 0; j < data.Hotels[0].Facilities.length; j++) {
				fac.push(icon[data.Hotels[0].Facilities[j]])
			}			
			
			data.Hotels[0].Facilities = fac
			let Rooms = data.Hotels[0].Rooms
			let dataReq = {
				CorrelationId: query.CorrelationId,
				HotelKey: data.Hotels[0].HotelKey
			}
			dispatch(getCancelPolicyHotel(dataReq))
			.then(result => {
				let policy = result.data.CancelPolicy
				if( policy !== null) {
					for(let i = 0; i < Rooms.length; i++) {
						let key = Object.keys(Rooms[i].PriceDetails[0].RoomTypePrice)
						let roomType = Rooms[i].PriceDetails[0].RoomTypePrice[key[0]].TypeCode
						let maxPeople = roomType === 'single' ? 1 : (
							roomType === 'double' ? 2 : (
								roomType === 'twin' ? 2 : (
									roomType === 'triple' ? 3 : (
										roomType === 'quad' ? 4 : 0
									)
								)
							))
						Rooms[i].photo_url = data.Hotels[0].ImageUri
						Rooms[i].minimum_stays = 1
						Rooms[i].maxGuest = maxPeople
						Rooms[i].price = Rooms[i].PriceDetails[0].RoomTypePrice[key[0]].PricePerRoom,
						Rooms[i].all_photo_room = [data.Hotels[0].ThumbUri],
						Rooms[i].room_available = Rooms[i].PriceDetails[0].TotalRoom
						Rooms[i].room_facility = data.Hotels[0].Facilities.slice(0, 5)
						Rooms[i].IsGuaranteedBooking = policy.Rooms[i].IsGuaranteedBooking
						Rooms[i].CancelLimit = policy.Rooms[i].CancelLimit
						Rooms[i].Message = policy.Rooms[i].Message
						Rooms[i].BreakfastType = (Rooms[i].IncludeBreakfast ? 'Include Breakfast' : 'No Breakfast')
						Rooms[i].Policy = (policy.Rooms[i].IsGuaranteedBooking ? ['Non Refundable'] : policy.Rooms[i].Summaries)
					}
				} else {
					for(let i = 0; i < Rooms.length; i++) {
						let key = Object.keys(Rooms[i].PriceDetails[0].RoomTypePrice)
						let roomType = Rooms[i].PriceDetails[0].RoomTypePrice[key[0]].TypeCode
						let maxPeople = roomType === 'single' ? 1 : (
							roomType === 'double' ? 2 : (
								roomType === 'twin' ? 2 : (
									roomType === 'triple' ? 3 : (
										roomType === 'quad' ? 4 : 0
									)
								)
							))
						Rooms[i].photo_url = data.Hotels[0].ImageUri
						Rooms[i].minimum_stays = 1
						Rooms[i].maxGuest = maxPeople
						Rooms[i].price = Rooms[i].PriceDetails[0].RoomTypePrice[key[0]].PricePerRoom,
						Rooms[i].all_photo_room = [data.Hotels[0].ThumbUri],
						Rooms[i].room_available = Rooms[i].PriceDetails[0].TotalRoom
						Rooms[i].room_facility = data.Hotels[0].Facilities.slice(0, 5)
					}
					data.Hotels[0].RoomNotAvail = true
				}				
				this.setState({hotel: data, start_from: (Rooms.length > 0 ? Rooms[0].price : 0), isLoading: false})
			})
		})
		.catch(err => {
			console.log('====================================');
			console.log(err);
			console.log('====================================');
		})
	}

	onStarClick = (nextValue, prevValue, name) => {
		this.setState({ rating: nextValue });
	};

	onDatesChange = ({ startDate, endDate }) => {
		this.setState({ startDate, endDate });
		if (endDate !== null) {
			let day = hitungSelisih(startDate, endDate);
			this.setState({ duration: day + 1 });
		}
	};

	handleClickOutside = (event) => {


		if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
			this.setState({ isShow: null });
		}
	};

	operation = (oprt, avail) => {

		let { room } = this.state
		if(oprt === 'plus') {
			if(room < avail) {
				this.setState({room: room+1})
			}
		} else if(oprt === 'minus'){
			if(room > 1) {
				this.setState({room: room-1})
			} else if(room === 1) {
				this.setState({room: 1, selectedRoom: null})
			} else {
				this.setState({room: 1})
			}
		}
	}

	componentDidMount() {
		let icon = {
			"room service": require('../static/icons/roomservice.png'),
			spa: require('../static/icons/spa.png'),
			pool: require('../static/icons/swimming.png'),
			"wireless internet": require('../static/icons/wifi.png'),
			"air conditioining": require('../static/icons/ac.png'),
			cafe: require('../static/icons/coffee.png'),
			tv: require('../static/icons/tv.png')
		}

		if(this.props.query.type == 'hotel') {
			this.getHotelPagination()
		}
		this.setState({isLoading: true})
		let iconKey = Object.keys(icon)
		let fac = []
		for(let i=0; i < this.props.facilities.length; i++) {
			fac.push({
				name: this.props.facilities[i],
				icon: icon[this.props.facilities[i].toLowerCase()]
			})
		}
		for(let j=0; j < this.props.rooms.length; j++) {
			let facs = []
			
			for(let k=0; k < this.props.rooms[j].room_facility.length; k++) {
				let iconStat = iconKey.find(el => el.name == this.props.rooms[j].room_facility[k].toLowerCase())
				this.props.rooms.FacilitiesIcon = (iconStat !== undefined ? true : false)
				facs.push({
					name: this.props.rooms[j].room_facility[k],
					icon: icon[this.props.rooms[j].room_facility[k].toLowerCase()]
				})
			}
			this.props.rooms[j].room_facility = facs
		}
		this.setState({facilities: fac, isLoading: false})
		
		
		
		this.props.dispatch(getLocations(""));
		const { activity, dispatch } = this.props;
		
	}

	modalOpen=(e)=>{
		e.preventDefault()
		this.props.dispatch(modalToggle(!this.props.isOpen, 'info-hotel'))
	}

	addToCart = () => {
		event.preventDefault()
		
		let { room, selectedRoom, hotel } = this.state
		const {query, dispatch} = this.props;
		dispatch(createToken()).then(result => {
		  if (get(result, 'meta.code') === 200) {
			if( query.type !== 'hotel') {
				let dataReq = {
					token: get(result, 'data.token'),
					start_date: query.startdate,
					end_date: query.enddate,
					night: query.night,
					room: room,
					adult: query.adult,
					child: query.child,
					infant: query.infant,
					product_id: selectedRoom.room_id
				}
				dispatch(storeCartHomestay(dataReq)).then(res => {
					if (get(res, 'meta.code') === 200) {
						dispatch(praCheckout(get(result,'data.token'))).then(booking => {
						if(get(booking,'data.status') === 'verified'){
							Router.push('/checkout');
						}
						});
					}else{
						dispatch(setErrorMessage(get(res,'data')));
					}
				})
			} else {
				let dataReq = {
					token: get(result, 'data.token'),
					CorrelationId: hotel.CorrelationId,
					HotelKey: hotel.Hotels[0].HotelKey,
					RoomKey: selectedRoom.RoomKey,
					ImageThumb: hotel.Hotels[0].ThumbUri
				}

				dispatch(storeCartHotel(dataReq)).then(res => {
					if (get(res, 'meta.code') === 200) {
						dispatch(praCheckout(get(result,'data.token'))).then(booking => {
							if(get(booking,'data.status') === 'verified'){
								Router.push('/checkout');
							}
						});
					}else{
						dispatch(setErrorMessage(get(res,'data')));
					}
				})
			}
			
		  }
		});
	}

	addingToCart = (startDate, endDate) => {
		const {carts, dispatch, query} = this.props;
		let { room, selectedRoom, hotel } = this.state
		if( query.type !== 'hotel') {
			if(isEmpty(carts)){
				dispatch(createToken()).then(result => {
					if (get(result, 'meta.code') === 200) {
						let dataReq = {
							token: get(result, 'data.token'),
							start_date: query.startdate,
							end_date: query.enddate,
							night: query.night,
							room: room,
							adult: query.adult,
							child: query.child,
							infant: query.infant,
							product_id: selectedRoom.room_id
						}
						dispatch(storeCartHomestay(dataReq)).then(res => {
							if (get(res, 'meta.code') === 200) {
								dispatch(getCartList(dataReq.token, 'schedule|ASC'));
								dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
								// Router.back()
							}else{
								dispatch(setErrorMessage('Something went wrong. Please contact our customer service.'));
							}
						})
					}
				});
			}else{
				let dataReq = {
					token: carts.token,
					start_date: query.startdate,
					end_date: query.enddate,
					night: query.night,
					room: room,
					adult: query.adult,
					child: query.child,
					infant: query.infant,
					product_id: selectedRoom.room_id
				}
				dispatch(storeCartHomestay(dataReq)).then(res => {
					if (get(res, 'meta.code') === 200) {
						dispatch(getCartList(carts.token, 'schedule|ASC'));
						dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
						// Router.back()
					}else{
						dispatch(setErrorMessage(get(res,'data')));
					}
				})
			}
		} else {
			if(isEmpty(carts)){
				dispatch(createToken()).then(result => {
					if (get(result, 'meta.code') === 200) {
						let dataReq = {
							token: get(result, 'data.token'),
							CorrelationId: hotel.CorrelationId,
							HotelKey: hotel.Hotels[0].HotelKey,
							RoomKey: selectedRoom.RoomKey,
							ImageThumb: hotel.Hotels[0].ThumbUri
						} 
						dispatch(storeCartHotel(dataReq)).then(res => {
							if (get(res, 'meta.code') === 200) {
								dispatch(getCartList(dataReq.token, 'schedule|ASC'));
								dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
								// Router.back()
							}else{
								dispatch(setErrorMessage('Something went wrong. Please contact our customer service.'));
							}
						})
					}
				});
			}else{
				let dataReq = {
					token: carts.token,
					CorrelationId: hotel.CorrelationId,
					HotelKey: hotel.Hotels[0].HotelKey,
					RoomKey: selectedRoom.RoomKey,
					ImageThumb: hotel.Hotels[0].ThumbUri
				}
				dispatch(storeCartHotel(dataReq)).then(res => {
					if (get(res, 'meta.code') === 200) {
						dispatch(getCartList(carts.token, 'schedule|ASC'));
						dispatch(setSuccessMessage(`${res.data.product_name} has been added on your Plan`));
						// Router.back()
					}else{
						dispatch(setErrorMessage(get(res,'data')));
					}
				})
			}
		}
		
    
    
  }

	handleClose = () => {
		this.setState({
			isModal: false
		})
	}


	render() {
		const { breadcrumb, all_photo, rooms, primaryPhotos, policy, query} = this.props;
		const {
			room,
			selectedRoom,
			isModal,
			hotel,
			facilities,
			isLoading
		} = this.state;

		return (
			<>
				{
					((hotel.Hotels || breadcrumb) && !isLoading ) ? (
						<>
							<Head
								title={query.type !== 'hotel' ? breadcrumb.name : hotel.Hotels[0].name}
								description={query.type !== 'hotel' ? breadcrumb.regional :hotel.Hotels[0].name }
								keyword={query.type !== 'hotel' ? breadcrumb.name : hotel.Hotels[0].name}
								ogImage={query.type !== 'hotel' ? primaryPhotos : hotel.Hotels[0].ImageUri}
								url={process.env.SITE_ROOT + `/accommodation/detail/${query.slug}`}
							/>

							{/* <PageHeader background={query.type !== 'hotel' ? primaryPhotos : hotel.Hotels[0].ImageUri} title={query.type !== 'hotel' ? breadcrumb.name : hotel.Hotels[0].name} caption={query.type !== 'hotel' ? breadcrumb.address : hotel.Hotels[0].Address} /> */}
							<div className='container' style={{padding: 0, margin: 0, width: '100%'}}>
								<div className='row' style={{padding: 0, margin: 0, width: '100%'}}>
									<div className='col-xs-6' style={{padding: '1em 2% 0 12%'}}>
										<span className="poppins-grey">Stay/elroyalehotel/detail</span>

										<div className="hotel-box">
											<div className='row'>
												<div className='col-xs-8'>
													<h4 className="acc-name">{query.type !== 'hotel' ? breadcrumb.name : hotel.Hotels[0].name}</h4>
													<span className="badge" style={{backgroundColor: (query.type !== 'hotel' ? '#43737F' : '#F0C65F'), marginBottom: '10px'}}>
														{ query.type !== 'hotel' ? 'HOMESTAY' : 'HOTEL'}
													</span><br/>
													<small className="poppins-orange">Start From :</small><br/>
													<div style={{display: 'flex', alignItems: 'flex-start'}} className='pricing'>
														<div><span className="poppins-orange" style={{fontSize: '1.1em', marginRight: '8px'}}>Rp</span></div>
														<div><span className="poppins-orange" style={{fontSize: '2em', fontWeight: 100}}>{numeral(parseInt( query.type !== 'hotel' ? breadcrumb.price : this.state.start_from,10)).format('0,0')}</span></div>
														<div><p className="poppins-orange" style={{fontSize: '1.1em'}}>/night</p></div>
													</div>													
												</div>
												<div className=" col-xs-4 address-box">
													<small>Address :</small>
													<small>{query.type !== 'hotel' ? breadcrumb.address : hotel.Hotels[0].Address}</small><br/>
													<span className="poppins-grey" style={{fontSize: '0.8em'}}>
														<img src={require('../static/icons/pin-maps.png')} style={{width: '8%'}}/> {query.type !== 'hotel' ? breadcrumb.province_name : `${hotel.Hotels[0].CityName}, ${hotel.Hotels[0].CountryName}`}
													</span>
													{/* <div className="disc-flag">
														<img src={require('../static/images/discount-flag.png')} style={{width: '100%'}}/>
														<h4 className="disc-numb">10%</h4>
														<h4 className="disc-numb" style={{right: '15%', top: '1em', textAlign: 'right'}}>OFF</h4>
													</div> */}

												</div>
											</div>
										</div>

										<h4 className="poppins-grey" style={{fontWeight: 'bold', marginBottom: '5px'}}>Facilities</h4>
										<div className="icon-box">
											{
												query.type !== 'hotel' ? (
													facilities.map((fac, key) => {
														if(fac.icon !== undefined) {
															return (
																<div className="icon" key={key}>
																	<img src={fac.icon !== undefined ? fac.icon : ''} style={{width: '55%'}}/>
																</div>
															)
														}
													} )
												) : (
													hotel.Hotels[0].Facilities.map((facility, key) => (
														<div className="icon" key={key}>
															<img src={facility.icon} style={{width: '55%'}}/>
														</div>
													))
												)
											}	
										</div>
									</div>
									<div className='col-xs-6' style={{padding: 0}}>
										<img src={query.type !== 'hotel' ? primaryPhotos : hotel.Hotels[0].ImageUri} style={{width: '100%', maxHeight: '40em'}}/>
									</div>
								</div>
							</div>

							<div className='container' style={{padding: 0, margin: 0, width: '100%', backgroundColor: '#f7f7f7'}}>
								<form>
									<div className='row' style={{display: 'flex', justifyContent: 'center', padding: '1.5em 0'}}>
										{/* <div className="col-md-2">
											<span className="poppins-grey">Check - in</span>
											<div className="group-input">
												<img src={require('../static/icons/calender.png')} style={{width: '10%'}}/>
												<input type="date" className="acc-filter-input"/>
											</div>
										</div>

										<div className="col-md-2">
											<span style={{color: '#636060', fontFamily: 'Poppins Medium'}}>Check - out</span>
											<div className="group-input">
												<img src={require('../static/icons/calender.png')} style={{width: '10%'}}/>
												<input type="date" className="acc-filter-input"/>
											</div>
										</div>
										<div className="col-md-2">
											<span style={{color: '#636060', fontFamily: 'Poppins Medium'}}>Guest and Room</span>
											<div className="group-input">
												<img src={require('../static/icons/calender.png')} style={{width: '10%'}}/>
												<input type="date" className="acc-filter-input"/>
											</div>
										</div>
										<div className="col-md-2" style={{paddingTop: '1.5em'}}>
											<div style={{display: 'flex'}}>
												<input type="checkbox" style={{width: 'auto'}}/>
												<span className="poppins-grey" style={{marginLeft: '10px'}}>Free Cancellation</span>
											</div>
											<div style={{display: 'flex'}}>
												<input type="checkbox" style={{width: 'auto'}}/>
												<span className="poppins-grey" style={{marginLeft: '10px'}}>Free Breakfast</span>
											</div>

										</div>
										<div className="col-md-2" style={{paddingTop: '1.2em'}}>
											<button type="submit" className="btn btn-primary btn-sm btn-flight-search" style={{backgroundColor: '#FF5B00', borderRadius: '5px'}}>
												<span className="ti-search"></span> Search Room
											</button>
										</div> */}
										
									</div>
								</form>
								
							</div>

							<section id="choose-package" className="content-activity">
								<section id="choose-package" className="bg-content-home">
									<div className="container">
										<section className="side-left choose-package">
											{
												query.type !== 'hotel' ? (
													<div className="panel">
														{rooms.map((item, key) => {
															return (
																<ListRoom
																	type='homestay'
																	key={key}
																	roomId={item.room_id}
																	detId={"R" + key}
																	thumb={item.photo_url}
																	title={item.room_name}
																	isBreakfasts={item.with_breakfasts}
																	label={"Minimum Stays " + (item.minimum_stays < 1 ? 1 : item.minimum_stays)}
																	label2={item.maximum_person + ' Guest'}
																	label3=''
																	amount={item.price}
																	desc={item.room_description}
																	facility={item.room_facility || []}
																	all_photo={item.all_photo_room || []}
																	iconStatus={item.iconStat}
																	isAvailable={item.room_available}
																	policy={policy}
																	onSelected={() => this.setState({selectedRoom: item, room: 1, isModal: true})}
																/>
															);
														})}
														{isEmpty(rooms) && (
															<div className="panel panel-bordered panel-info sr-btm mt1">
																<div className="panel-body">
																	<p>{true ? "Loading..." : "Product schedule is not available"}</p>
																</div>
															</div>
														)}
													</div>
												) : <div className="panel">
														{
															!hotel.Hotels[0].RoomNotAvail ? (
																hotel.Hotels[0].Rooms.map((item, key) => {
																	if(item.status !== 2) {
																		return (
																			<ListRoom
																				type='hotel'
																				key={key}
																				roomId={item.RoomKey}
																				detId={"R" + key}
																				thumb={item.photo_url}
																				title={item.RoomName}
																				isBreakfasts={item.IncludeBreakfast}
																				label={item.IsGuaranteedBooking ? 'Non Refundable' : 'refundable'}
																				label2={item.maxGuest + ' Guest'}
																				label3={item.BreakfastType}
																				amount={item.price}
																				desc={item.RoomSubText}
																				facility={item.room_facility || []}
																				all_photo={item.all_photo_room || []}
																				isAvailable={item.room_available}
																				iconStatus={item.room_facility.length > 0 ? true : false}
																				policy={item.Policy}
																				onSelected={() => this.setState({selectedRoom: item, room: 1, isModal: true, room_total: item.room_available})}
																			/>
																		);
																	}
																
																})): (
																	<div className="panel panel-bordered panel-info sr-btm mt1">
																		<div className="panel-body">
																			<p>Product schedule is not available</p>
																		</div>
																	</div>
																)
														}
														{isEmpty(hotel.Hotels[0].Rooms) && (
															<div className="panel panel-bordered panel-info sr-btm mt1">
																<div className="panel-body">
																	<p>{true ? "Loading..." : "Product schedule is not available"}</p>
																</div>
															</div>
														)}
													</div>
											}
											
										</section>
										{
											query.type !== 'hotel' ? (
												<SideRight 
												isSummary={!isEmpty(selectedRoom)} 
												room={!isEmpty(selectedRoom) ? selectedRoom : first(rooms)} 
												total_room={room} 
												handleRoomChange={this.operation} 
												handleCheckout={this.addToCart}
												night={query.night}
												isModal={isModal}
												handleClose={this.handleClose}
												handleAddToCart={this.addingToCart}
												/>
											) : (
												<SideRight 
												isSummary={!isEmpty(selectedRoom)} 
												room={!isEmpty(selectedRoom) ? selectedRoom : hotel.Hotels[0].Rooms[0]} 
												total_room={this.state.room_total} 
												handleRoomChange={this.operation} 
												handleCheckout={this.addToCart}
												night={query.night}
												isModal={isModal}
												handleClose={this.handleClose}
												handleAddToCart={this.addingToCart}
												/>
											)
										}
										
									</div>
								</section>
								<section id="wywg" className="bg-content-home">
									<div className="container">
										<section className="side-left width-maps">
											<h2 className="text-title">Location</h2>
											<Maps isMarkerShown lat={query.type !== 'hotel' ? breadcrumb.latitude : hotel.Hotels[0].Latitude} lng={query.type !== 'hotel' ? breadcrumb.longitude : hotel.Hotels[0].Longitude} />
										</section>
									</div>
								</section>
								<section id="activity-information" className="bg-content-home">
									<div className="container">
										<section className="side-left">
											<h2 className="text-title">Facilities</h2>
											<div>
												{
													query.type !== 'hotel' ? (
														<ul className="list-unstyled check-list">
															{map(facilities, (item, index) => (
																<li key={`includes-${index}`}>
																	<i className="ti-check text-success" />
																	{item.name}
																</li>
															))}
														</ul>
													) : (
														<ul className="list-unstyled check-list">
															{map(hotel.Hotels[0].Facilities, (item, index) => (
																<li key={`includes-${index}`}>
																	<i className="ti-check text-success" />
																	{item.name}
																</li>
															))}
														</ul>
													)
												}
												
											</div>
										</section>
									</div>
								</section>
								<section id="term-n-condition" className="bg-content-home">
									<div className="container">
										<section className="side-left">
											<h2 className="text-title">Terms and Conditions</h2>
											{  query.type !== 'hotel' ? (
												policy.map((item, i) => (
													<div key={i}>
														<p>- {item.name}</p>
														<p>- {item.tier_one}</p>
														<p>- {item.tier_two}</p>
													</div>
												))
											) : (
												this.state.HotelPolicy .map((item, i) => (
													<div key={i}>
														<p>- {item.name}</p>
														<p>- {item.tier_one}</p>
														<p>- {item.tier_two}</p>
													</div>
												))
											)}
										</section>
									</div>
								</section>
							</section>
						</>
						
					) : (<Loading/>)
				}
			<style jsx>{`
				.badge {
					padding: 0.2em 1.5em;
					border-radius: 50px;
					border: 1px solid white;
					font-family: Poppins Medium;
					font-size: 1em;
					font-weight: 100
				}

				.address-box {
					min-height: 150px;
					display: flex;
					flex-direction: column;
					justify-content: flex-end;
					text-align: right;
					color: #C6C6C6;
					line-height: initial;
					font-size: 1em
				}

				.disc-flag {
					position: absolute;
					top: -23%;
					width: 40%
				}
				
				.disc-numb {
					position: absolute;
					top: 0;
					left: 5px;
					color: white;
					font-family: Poppins Medium;
					margin: 0;
				}

				.hotel-box {
					width: 110%;
					background-color: #fff;
					min-height: 200px;
					border-radius: 15px;
					box-shadow: 5px 15px 20px #0000000D;
					border: 1px solid #D8D8D8;
					z-index: 2;
					padding: 2em;
					position: sticky;
					margin-left: 10%;
					margin-top: 3%;
					margin-bottom: 15%
				}

				.acc-name {
					font-family: Poppins Medium;
					font-weight: bold;
					color: #636060;
					margin-bottom: 5px
				}

				.poppins-grey {
					font-family: Poppins Medium;
					color: #636060
				}

				.poppins-orange {
					font-family: Poppins Medium;
					color: #FF5B00
				}

				.icon-box {
					display: flex;
					align-items: center;
					width: 100%;
					padding: 1em 0
				}

				.icon {
					width: 15%
				}

				.group-input {
					display: flex;
					padding: 8px;
					border: 1px solid #E5E5E5;
					border-radius: 6px
				}

				.acc-filter-input {
					border: 0;
					background-color: transparent;
					margin-left: 10px
				}

				@media (max-width: 990px) {
					.hotel-box {
						margin-left: 0;
						width: 100%;
						padding: 1em;
					}

					.acc-name {
						font-size: 1.2em
					}

					.badge {
						font-size: 0.8em
					}

					.side-left {
						width: 70% !important
					}

					.pricing {
						font-size: 0.8em;

					}
				}
			`}</style>	
				

				
			</>
		);
	}
}
const mapStateToProps = (state) => ({
	breadcrumb: state.guestHoust.breadcrumb,
	rooms: state.guestHoust.rooms,
	primaryPhotos: state.guestHoust.primaryPhotos,
	all_photo: state.guestHoust.all_photo,
	policy: state.guestHoust.policy,
	facilities: state.guestHoust.facilities,
	carts: state.cart.data,
});

export default compose(
	connect(mapStateToProps),
	withAuth(["PUBLIC"]),
)(AccommodationDetail);
