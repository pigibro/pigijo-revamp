import { compose } from "redux";
import { connect } from "react-redux";
import React from 'react';
import Error from './_error'
import { get, isEmpty, concat } from 'lodash';
import {  Modal  } from 'react-bootstrap';
import withAuth from '../_hoc/withAuth';
import Head from "../components/head";

import PageHeader from "../components/page-header";
import HowToBook from '../components/home/howtobook';
import ActivityCategory from '../components/home/activity-category';
import PlaceCategory from '../components/home/place-category';
import SectionCategory from '../components/home/section-category';
import AccommodationCategory from '../components/home/accommodation-category';
import RentCarCategory from '../components/home/transport-category';
import Event from '../components/home/event';
import moment from "moment";
import { getActivities, getDetailCategory, getPlaces, getGuestHouse } from '../stores/actions';



class Index extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      localExperiences:{
        isLoading: false,
        data: [],
        page: 1,
        isReload: false,
      },
      ourAssistance:{
        isLoading: false,
        data: [],
        page: 1,
        total: 0,
        isReload: false,
        isMoreLoading: false,
      },
      places:{
        isLoading: false,
        data: [],
        page: 1,
        total: 0,
        isReload: false,
        isMoreLoading: false,
      },
      accommodation:{
        isLoading: false,
        data: [],
        page: 1,
        isReload: false,
      },
      rentcar:{
        isLoading: false,
        data: [],
        page: 1,
        isReload: false,
      }
    }
  }

  static async getInitialProps({ query, store}) {
    const { slug } = query;
    let errorCode = false;
    const resCategory = await store.dispatch(getDetailCategory('local-experiences'));
    const resCategory1 = await store.dispatch(getDetailCategory('local-assistants'));
    errorCode = get(resCategory,'meta.code') !== 200 ? 404 : false;
    errorCode = get(resCategory1,'meta.code') !== 200 ? 404 : false;
    const localExperiencesCategory = get(resCategory,'data');
    const ourAssistanceCategory = get(resCategory1,'data');
    return { errorCode, slug, localExperiencesCategory, ourAssistanceCategory }
  }
  
  componentDidMount(){
    const {localExperiences, ourAssistance, places, accommodation, rentcar} = this.state;
    const {localExperiencesCategory, ourAssistanceCategory} = this.props;
    if (isEmpty(localExperiences.data)){
      this.setState({
        localExperiences:{
          isLoading: true,
          data: [],
          isReload: false,
        }
      });
      const params = {
        per_page: 8,
        category_id: localExperiencesCategory.id
      };
      this.props.dispatch(getActivities(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            localExperiences:{
              isLoading: false,
              data: get(result, 'data'),
              isReload: false,
            }
          });
        }else{
          this.setState({
            localExperiences:{
              isLoading: false,
              data: [],
              isReload: true,
            }
          });
        }
      });
    }
    if (isEmpty(ourAssistance.data)){
      this.setState({
        ourAssistance:{
          isLoading: true,
          data: [],
          isReload: false,
          isMoreLoading: false,
        }
      });
      const params = {
        per_page: 6,
        category_id: ourAssistanceCategory.id
      };
      this.props.dispatch(getActivities(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            ourAssistance:{
              isLoading: false,
              total: get(result, 'pagination.total'),
              data: get(result, 'data'),
              isReload: false,
              isMoreLoading: false,
            }
          });
        }else{
          this.setState({
            ourAssistance:{
              isLoading: false,
              data: [],
              isReload: true,
              isMoreLoading: false,
            }
          });
        }
      });
    }
    if (isEmpty(places.data)){
      this.setState({
        places:{
          isLoading: true,
          data: [],
          isReload: false,
          isMoreLoading: false,
        }
      });
      const params = {
        per_page: 8,
      };
      this.props.dispatch(getPlaces(params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            places:{
              isLoading: false,
              total: get(result, 'pagination.total'),
              data: get(result, 'data'),
              isReload: false,
              isMoreLoading: false,
            }
          });
        }else{
          this.setState({
            places:{
              isLoading: false,
              data: [],
              isReload: true,
              isMoreLoading: false,
            }
          });
        }
      });
    }
    if (isEmpty(accommodation.data)){
      this.setState({
        Accommodation:{
          isLoading: true,
          data: [],
          isReload: false,
          isMoreLoading: false,
        }
      });
     const { token, locationUser, dispatch } = this.props;
    
      // if (dispatch(getLocationUser())) {
      let params = {
        startdate: moment().format("YYYY-MM-DD"),
        enddate: moment()
          .add(1, "days")
          .format("YYYY-MM-DD"),
        night: 1,
        room: 1,
        adult: 1,
        person: 0,
        child: 0,
        minprice: "",
        maxprice: "",
        longitude: get(locationUser, 'longitude') || "-6.21462",
        latitude: get(locationUser, 'latitude') || "106.84513",
        page: 1,
        search: "",
      };

      this.props.dispatch(getGuestHouse(token,params)).then((result) => {
        if (get(result, 'meta.code') === 200) {
          this.setState({
            Accommodation:{
              isLoading: false,
              total: get(result, 'pagination.total'),
              data: get(result, 'data'),
              isReload: false,
              isMoreLoading: false,
            }
          });
        }else{
          this.setState({
            accommodation:{
              isLoading: false,
              data: [],
              isReload: true,
              isMoreLoading: false,
            }
          });
        }
      });
    }
    if (isEmpty(rentcar.data)){
      this.setState({
        rentcar:{
          isLoading: true,
          data: [],
          isReload: false,
        }
      });
      const params = {
        per_page: 8,
        promo:1
      };
      // this.props.dispatch(getAccommodation(params)).then((result) => {
      //   if (get(result, 'meta.code') === 200) {
      //     this.setState({
      //       rentcar:{
      //         isLoading: false,
      //         data: get(result, 'data'),
      //         isReload: false,
      //       }
      //     });
      //   }else{
      //     this.setState({
      //       rentcar:{
      //         isLoading: false,
      //         data: [],
      //         isReload: true,
      //       }
      //     });
      //   }
      // });
    }
  }

  moreCategory = (e) => 
  {
    const {accommodation, rentcar, ourAssistance, places} = this.state;
    const {ourAssistanceCategory,localExperiencesCategory} = this.props;
    const category = e.target.id;
    if(category === 'local-assistants'){
      if(ourAssistance.data.length <= ourAssistance.total){
        const params = {
          page: (ourAssistance.page || 1)+1,
          per_page: 6,
          category_id: ourAssistanceCategory.id
        };
        this.setState({
        ourAssistance:{
          isLoading: false,
          data: ourAssistance.data,
          page: ourAssistance.page,
          isReload: false,
          total: ourAssistance.total,
          isMoreLoading: true,
        }});
        
        this.props.dispatch(getActivities(params)).then((result) => {
          if (get(result, 'meta.code') === 200) {
            this.setState({
              ourAssistance:{
                isLoading: false,
                data: ourAssistance.data.concat(get(result,'data')),
                page: (ourAssistance.page || 1)+1,
                isReload: false,
                total: get(result,'pagination.total'),
                isMoreLoading: false,
              }
            });
          }
        });
      }
    }
    if(category === 'accommodation'){
      if(recommended.data.length <= recommended.total){
        const params = {
          page: (recommended.page || 1)+1,
          per_page: 6,
          category_id: localExperiencesCategory.id,
          sort_by: "recommended|DESC",
        };
        this.setState({
        accommodation:{
          isLoading: false,
          data: accommodation.data,
          page: accommodation.page,
          isReload: false,
          total: accommodation.total,
          isMoreLoading: true,
        }});
        
        this.props.dispatch(getAccommodation(params)).then((result) => {
          if (get(result, 'meta.code') === 200) {
            this.setState({
              accommodation:{
                isLoading: false,
                data: accommodation.data.concat(get(result,'data')),
                page: (accommodation.page || 1)+1,
                isReload: false,
                total: get(result,'pagination.total'),
                isMoreLoading: false,
              }
            });
          }
        });
      }
    }
    if(category === 'places'){
      if(places.data.length <= places.total){
        const params = {
          page: (places.page || 1)+1,
          per_page: 8,
        };
        this.setState({
        places:{
          isLoading: false,
          data: places.data,
          page: places.page,
          isReload: false,
          total: places.total,
          isMoreLoading: true,
        }});
        
        this.props.dispatch(getPlaces(params)).then((result) => {
          if (get(result, 'meta.code') === 200) {
            this.setState({
              places:{
                isLoading: false,
                data: places.data.concat(get(result,'data')),
                page: (places.page || 1)+1,
                isReload: false,
                total: get(result,'pagination.total'),
                isMoreLoading: false,
              }
            });
          }
        });
      }
    }
  }
  // static async getInitialProps({store}) {
  //   const currentState = store.getState();
    
  //   const blog = get(currentState, 'blog.data');
  //   const event = get(currentState, 'event.data');
  //   const flashsale = get(currentState, 'activity.data');
    
  //   if (isEmpty(blog))
  //     await store.dispatch( getLatestBlog() );
  //   if (isEmpty(event))
  //     await store.dispatch( getEvent() );
  //   if (isEmpty(flashsale))
  //     await store.dispatch( getActivityPromos() );
  // }
  render() {
    const {localExperiences, ourAssistance, specialOffer, accommodation, rentcar, places} = this.state;
    const {dispatch} = this.props;
  return (
      <div>
        <Head 
             title={"Destination Search"}
             description={
              "Jika anda mencari tempat yang tepat untuk melarikan diri dari sibuknya kota, Bali adalah destinasi yang cocok untuk anda."
             }
             url={process.env.SITE_ROOT + "/p/1180-showcase-indonesia-full-pass-early-bird"}
          />
          <PageHeader
             background={"/static/images/img05.jpg"}
             title="Explore More "
             caption="Find Your Holidays Needs at this destinations"
          />   
         
        <AccommodationCategory data={accommodation.data} isReload={accommodation.isReload} isLoading={accommodation.isLoading}/>
        <AccommodationCategory loopTo={[1,1,1]} classProps={[4,6,12]} title="Trending Now" moreLoading={accommodation.isMoreLoading} isLoading={accommodation.isLoading} isReload={accommodation.isReload} data={accommodation.data} link={"accommodation"} model={1} moreBtn moreData={this.moreCategory} total={accommodation.total} />
        <ActivityCategory loopTo={[1,1,1,1]} classProps={[3,6,6]} title="Local Experiences" isLoading={localExperiences.isLoading} isReload={localExperiences.isReload} data={localExperiences.data} link={"local-experiences"} />
        <ActivityCategory loopTo={[1,1,1]} classProps={[4,6,12]} title="Meet Our Assistants" moreLoading={ourAssistance.isMoreLoading} isLoading={ourAssistance.isLoading} isReload={ourAssistance.isReload} data={ourAssistance.data} link={"local-assistants"} model={1} moreBtn moreData={this.moreCategory} total={ourAssistance.total} />
        <PlaceCategory loopTo={[1,1,1,1]} classProps={[3,6,6]} title="Places" isLoading={places.isLoading} isReload={places.isReload} data={places.data} link={"places"} moreBtn moreData={this.moreCategory} total={places.total} />
        <rentcarCategory LoopTo={[1,1,1,1]} classProps={[4,6,12]} title="Local Transport" moreLoading={rentcar.isMoreLoading} isLoading={rentcar.isLoading} isReload={rentcar.isReload} data={rentcar.data} link={"rentcar"} model={1} moreBtn moreData={this.moreCategory} total={rentcar.total} />
      </div>
    )
  }
}
export default compose(
  withAuth(["PUBLIC"])
)(Index);