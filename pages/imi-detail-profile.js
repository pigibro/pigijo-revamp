import React, { Component } from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import Loading from '../components/shared/loading';
import withAuth from '../_hoc/withAuth';
import {apiCall, apiUrl} from '../services/request'
import { verify, resendVerification, setErrorMessage } from '../stores/actions';

class Imidetailprofile extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            email: '',
            error: '',
            success: '',
            isLoading: true,
            isVerified: false,
            profile: {}
         };
    }

    static async getInitialProps({ query }) {
        const { uid } = query;
        return { uid }
      }

    componentDidMount() {
        let { dispatch, uid } = this.props;
        const dataReq = {
            method: "GET",
            url: `https://api-registration.pigijo.com/user/profile?uid=${uid}`,
        };

        dispatch(apiCall(dataReq)).then((result) => {
            if(get(result,'meta.code') === 200){
                this.setState({profile: get(result, 'data')});
                this.setState({error: ''});
            }else{
                this.setState({error: get(result,'meta.message')});
                this.setState({success: ''});
                this.errorAlert(get(result,'meta.message'))
            }
            this.setState({isLoading: false});
        })
    }

    errorAlert = async (error) => {
        await this.props.dispatch(setErrorMessage(error))
    }
    render() {
        const {isLoading, error, success, profile} = this.state;
        let keys = Object.keys(profile)
        return (
            <div>
                <Head title={"Rakernas Profile"}/>
                <PageHeader background={"/static/images/img05.jpg"} title={`Profile`}/>
                <section className="content-wrap">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-md-offset-3">
                                <div className='panel panel-bordered panel-primary sr-btm mt2'>
                                    <div className='panel-body'>
                                        <div className='text-success success-text' style={{marginTop: '1em', marginBottom: '1em', fontSize: '1.8em', textAlign: 'center'}}>
                                        Profile
                                        </div>
                                        {
                                            profile.id &&
                                            <div style={{display: 'flex', flexDirection: 'column', flexWrap: 'wrap'}}>
                                                <div style={{marginLeft: 'auto', marginRight: 'auto', marginBottom: '0.5em'}}>
                                                    <p>Name : {profile.salutation} {profile.firstname} {profile.lastname}</p>
                                                    <p>Email : {profile.email}</p>
                                                    <p>Phone : {profile.phone}</p>
                                                </div>
                                                
                                                <p style={{textAlign: 'center', fontSize: '1.5em', color: '#000'}}>QR Code</p>
                                                <img src={`data:image/png;base64,${profile.qrcode}`} width="50%" style={{marginLeft: 'auto', marginRight: 'auto'}}/>
                                            </div>
                                        }
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
  )(Imidetailprofile);