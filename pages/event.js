import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight, map } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';
import Pagination from "react-js-pagination";
import { FormControl, FormGroup, InputGroup, Col, Panel, PanelGroup } from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import { SingleDatePicker } from "react-dates";
import 'react-dates/initialize';
import numberWithComma from '../utils/numberWithComma';
import { slugify, urlImage, convertToRp } from '../utils/helpers';
import { getEventDetail } from '../stores/actions';
import moment from 'moment';
import 'moment/locale/id'
moment.locale('id');

class Event extends Component {
    static async getInitialProps({ query, store}) {
        const { slug } = query;
        // const currentState = store.getState();
        // const activity = get(currentState, 'activity.single');
        let errorCode = false;
        // if (isEmpty(activity) || (!isEmpty(activity) &&  get(activity,'slug') != slug)){
        //   const resProduct = await store.dispatch( getActivity(slug) );
        //   errorCode = get(resProduct,'meta.code') > 200 ? 404 : false;
        // }
        const data = await store.dispatch( getEventDetail(slug));
        errorCode = get(data,'meta.code') > 200 ? 404 : false;
        const event = get(data,'data');
        return { errorCode, slug: slug, event:event}
    }
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <Head 
                    title={"Eu nostrud amet esse enim est esse commodo reprehenderit."} 
                    description={"Culpa tempor eu elit duis eu occaecat culpa culpa. Commodo anim commodo occaecat do consequat consequat Lorem laborum qui duis reprehenderit nisi Lorem ullamco. Dolore cupidatat proident esse irure cillum deserunt. Sit et tempor sint duis occaecat ea culpa."} url={process.env.SITE_ROOT + '/p/1180-showcase-indonesia-full-pass-early-bird'} />
                <PageHeader
                background={"/static/images/img05.jpg"}
                title="Find all your Entertainment needs"
                caption="Culpa ad amet non aute do sunt velit veniam et." />
                <section id="content-activity" className="content-activity"> 
                    <div className="container">
                        <div className="col-md-12 ev-header">
                            <div className="row">
                                <div className="col-md-8 col-sm-8 col-xs-12 ev-head-content">
                                    <h3 className="ev-title">{name}</h3>
                                    <h5 className="ev-location">
                                        <span className="glyphicon glyphicon-map-marker"></span> Bali, Denpasar 
                                    </h5>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12 ev-head-content">
                                    <ul>
                                        <li>
                                            <label>Share :</label>
                                        </li>
                                        <li>
                                            <div className="media-social-icon1">
                                                <span className="ti-facebook"></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="media-social-icon2">
                                                <span className="ti-google"></span>
                                            </div>
                                        </li> 
                                        <li>
                                            <div className="media-social-icon3">
                                                <span className="ti-twitter"></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="media-social-icon4">
                                                <span className="ti-linkedin"></span>
                                            </div>
                                        </li>    
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 ev-content">
                           <div className="row">
                               <div className="col-md-8">
                                   <div className="image-gallery-image">
                                       <img className="img-content" src={"https://s3-ap-southeast-1.amazonaws.com/loket-production-sg/images/banner/20190624024219.jpg"} />
                                   </div>
                                   <div className="row">
                                       <div className="col-md-12 ev-desc">
                                           <h3 className="text-title">Description</h3>
                                       </div>
                                       <div className="col-md-12 ev-body">
                                           <p className="text-content">
                                            Cillum sunt dolor reprehenderit incididunt ut aliquip adipisicing amet aliquip
                                            adipisicing anim labore ut exercitation in in amet enim esse deserunt est nisi excepteur
                                            laborum laboris tempor id dolore ea pariatur est tempor adipisicing dolore anim esse
                                            quis cupidatat incididunt proident veniam non occaecat et nisi amet incididunt qui id
                                            nisi in ex labore magna sunt velit labore reprehenderit esse fugiat consequat
                                            exercitation culpa dolor qui quis sunt exercitation ofcia commodo eu pariatur non sint
                                            anim veniam incididunt elit et dolore sunt ut sed veniam eiusmod cupidatat adipisicing
                                            esse mollit minim dolor aliqua ut in anim in consectetur non irure ut velit dolor ut aute
                                            sed eiusmod ut ut labore ea voluptate proident in ut consectetur dolor pariatur sed ea
                                            ut anim deserunt mollit ea dolore ofcia irure in deserunt aliquip in dolore esse pariatur
                                            ut minim minim magna irure sint dolore ut dolor do amet incididunt minim est veniam
                                            ofcia est adipisicing sunt anim mollit fugiat tempor dolor cillum aliqua adipisicing quis
                                            ofcia in ex non reprehenderit minim exercitation ofcia et dolore laboris exercitation
                                            ex sit voluptate anim ea fugiat ut laboris exercitation cupidatat in irure eu incididunt
                                            nostrud in tempor consequat et sunt enim nulla sunt non aliquip ex amet laboris ut
                                            nulla sunt commodo incididunt anim qui elit ad et culpa.
                                           </p>
                                       </div>
                                   </div>
                               </div>
                               <div className="col-md-4">
                                   <div className="row">
                                       <div className="col-md-12 ev-text-d">
                                        <div className="ev-date">
                                            <h5 className="text-date">28 - 29 October 2019</h5>
                                            <span>20:00 - 24:00</span>
                                        </div>
                                       </div>
                                       <div className="col-md-12 ev-ticket">
                                           <div className="ev-panel">
                                                <div className="ev-panel-header">
                                                   <h5 className="text-title-tix">
                                                       TICKET CATEGORY
                                                   </h5>
                                                </div>
                                                <div className="ev-panel-body">
                                                    <PanelGroup
                                                        accordion
                                                        id="accordion-controlled-example"
                                                        className=""
                                                    >
                                                        <Panel style={{ background : "#f5f7fa"}}>
                                                            <Panel.Heading className="ev-b-header">
                                                                <Panel.Title toggle className="ev-bg" >
                                                                    <span className="text-pre">PreSale</span>
                                                                    <span className="text-pre1">Rp. 1500.000</span>
                                                                </Panel.Title>
                                                            </Panel.Heading>
                                                            <Panel.Body collapsible className="ev-b-header">
                                                                <div className="box-pre">
                                                                    <div className="tix-chooser">
                                                                        <button className="tix-chooser__amount-btn" type="button" >-</button>
                                                                        <span className="passengers-chooser__amount-field">0</span>
                                                                        <button className="tix-chooser__amount-btn" type="button" >+</button>
                                                                    </div>
                                                                </div>
                                                                <div className="row ev-bg1">
                                                                    <div className="col-md-6">
                                                                        <p className="text-pre">Sub Total</p>
                                                                    </div>
                                                                    <div className="col-md-6">
                                                                        <p className="text-pre1">Rp. 1500.000</p>
                                                                    </div>
                                                                </div>
                                                            </Panel.Body>
                                                        </Panel>
                                                    </PanelGroup>
                                                </div>
                                                <div className="ev-panel-footer">
                                                   <div className="row">
                                                       <div className="col-md-6">
                                                            <h5 className="text-footer">Total</h5>
                                                       </div>
                                                       <div className="col-md-6">
                                                            <h5 className="text-footer1">Rp. 123.000</h5>
                                                       </div>
                                                   </div>
                                                </div>
                                           </div>
                                       </div>
                                       <div className="col-md-12 ev-text-d">
                                         <a href="#choose-package" className="btn btn-primary btn-buy">Buy Ticket</a>
                                       </div>
                                       <div className="col-md-12 ev-text-d">
                                            <div className="ev-date">
                                                <h5 className="text-date">Term & Condition</h5>   
                                            </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                         </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}


export default compose(
    connect(),
    withAuth(["PUBLIC"])
  )(Event);
