import React from "react";
import { compose } from "redux";
import withAuth from "../_hoc/withAuth";
import Headers from "../components/profile/_component/header";
import Content from "../components/profile/myItinerary/content";
import { connect } from "react-redux";
import { getItinerary } from "../stores/actions/listItineraryAction";
import { IMAGE_URL } from "../stores/actionTypes";

class MyItinerary extends React.Component {
	componentWillMount() {
		this.props.dispatch(getItinerary(`?q=&latest&page=1&per_page=10`,this.props.token));
	}
	render() {
		const { token, asPath, list, timeline, hotel, tour, rentcar, details, paging, isLoading } = this.props;

		return (
			<div>
				<Headers />
				<Content 
					token={token}
					path={asPath}
					list={list}
					timeline={timeline}
					hotel={hotel}
					rentcar={rentcar}
					tour={tour}
					details={details}
          paging={paging}
          url={IMAGE_URL}
          isLoading={isLoading}
				/>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	list  :state.listItinerary.list,
  paging  :state.listItinerary.paging,
  isLoading:state.listItinerary.isLoading,
  hotel:state.listItinerary.hotel,
  tour:state.listItinerary.tour,  
  rentcar:state.listItinerary.rentcar,
  timeline:state.listItinerary.timeline,
  details:state.listItinerary.details
});

export default compose(
	connect(mapStateToProps),
	withAuth(["PRIVATE"]),
)(MyItinerary);
