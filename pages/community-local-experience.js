import "react-dates/initialize";
import React from "react";
import InfiniteScroll from "react-infinite-scroller";
import InputRange from "react-input-range";
import NumberFormat from "react-number-format";
import moment from "moment";
// import Router from 'next/router'
import { Badge } from 'react-bootstrap'
import { SingleDatePicker } from "react-dates";
import QueryString from "query-string";
import { compose } from "redux";
import { connect } from "react-redux";
import { httpReqGetDetailCommunity } from "../stores/actions";
import { get, isEmpty } from "lodash";
import PageHeader from "../components/page-header";
import Head from "../components/head";
import withAuth from "../_hoc/withAuth";
import HeaderDetail from "../components/community/header-detail";
import { Link, Router } from "../routes";
import { getLocalExp } from "../stores/actions";
import { getLocations } from "../stores/actions";
import { urlImage } from "../utils/helpers";
import SearchAutoComplete from "../components/shared/searchautocomplete";

class CommunityEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: 10,
      searchState: "",
      focused: false,
      date: null,
      communities: {
        isLoading: false,
        data: {},
        page: 1,
        isReload: false
      },
      slug: this.props.asPath.substring(this.props.asPath.lastIndexOf("/") + 1)
    };
  }

  static async getInitialProps({ query }) {
    const { slug, product_name, city, max_price, start_date } = query;
    return {
      slug: slug,
      query: {
        product_name: product_name,
        city: city,
        max_price: max_price,
        start_date: start_date
      }
    };
  }

  componentDidUpdate(prevProps) {
    const { slug, query, dispatch } = this.props;
    if (prevProps.query != this.props.query) {
      dispatch(getLocalExp(slug, query));
    }
  }

  componentDidMount() {
    const { communities } = this.state;
    
    if (isEmpty(communities.data)) {
      this.setState({
        communities: {
          isLoading: true,
          data: [],
          isReload: false
        }
      });

      this.props
        .dispatch(httpReqGetDetailCommunity(this.state.slug))
        .then(result => {
          if (get(result, "meta.code") === 200) {
            this.setState({
              communities: {
                isLoading: false,
                data: get(result, "data"),
                isReload: false
              }
            });
          } else {
            this.setState({
              communities: {
                isLoading: false,
                data: {},
                isReload: true
              }
            });
          }
        });
    }

    const { slug, dispatch, query } = this.props;    
    if (query["max_price"] !== "" && query["max_price"] !== undefined) {
      this.setState({ price: Number(query["max_price"]) });
    }

    dispatch(getLocalExp(slug, query))
    dispatch(getLocations());
  }

  render() {
    const { slug, cityReducer, query, localExp } = this.props;
    const { data } = cityReducer;

    const { communities } = this.state;
    const detailComm = communities.data.detail_community
      ? communities.data.detail_community
      : "";      
    return (
      // <InfiniteScroll>
      <div className="community-events">
        <Head
          title="Community Event | Pigijo"
          description="Halaman Event Berbagai Komunitas Pigijo"
          keyword="Komunitas - Event - Pigijo"
          url={process.env.SITE_ROOT + `/community/local-experiences/${slug}`}
        />
        <PageHeader
          background={"/static/images/img05.jpg"}
          title={`Community Events`}
        />
        <HeaderDetail
          comm={communities.data}
          slug={this.state.slug}
          detailComm={detailComm}
        />
        <section>
          <div className="container mb">
            <div className="row flex-row">
              <div className="col-lg-offset-6 col-lg-6 col-md-12 search input-group-lg col-xs-12 col-sm-12">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search"
                  aria-label="Search"
                  aria-describedby="button-addon2"
                  onChange={e => this.setState({ searchState: e.target.value })}
                />
                <button
                  className="btn btn-outline-secondary"
                  type="button"
                  id="button-addon2"
                  onClick={() => {
                    const newQuery = query;
                    newQuery["product_name"] = this.state.searchState;
                    Router.pushRoute(
                      `/community/local-experiences/${slug}?${QueryString.stringify(
                        newQuery
                      )}`
                    );
                  }}
                >
                  Search
                </button>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <div
              className="row flex-row"
              style={{
                alignItems: "flex-start"
              }}
            >
              <div className="col-lg-2 data-filter col-xs-12">
                <div className="cell">
                  <p>Location</p>
                  <div className="cell-row">
                    {/* <input type="checkbox" aria-label="Checkbox for following text input"/>
                    <label>Jakarta</label> */}
                    <SearchAutoComplete
                      id="event-destination"
                      key={"select-single"}
                      index={0}
                      data={data || []}
                      placeholder="Select City"
                      labelKey="name"
                      onChange={e => {
                        const newQuery = query;
                        newQuery.city = e.length > 0 ? e[0].id : "";
                        // Router.pushRoute(
                        //   `/community/local-experiences/${slug}?${QueryString.stringify(
                        //     newQuery
                        //   )}`
                        // );
                      }}
                      bsSize="large"
                    />
                  </div>
                  {/* <div className="cell-row">
                    <input type="checkbox" aria-label="Checkbox for following text input"/>
                    <label>Bandung</label>
                  </div>
                  <div className="cell-row">
                    <input type="checkbox" aria-label="Checkbox for following text input"/>
                    <label>Surabaya</label>
                  </div>
                  <div className="cell-row">
                    <input type="checkbox" aria-label="Checkbox for following text input"/>
                    <label>Bogor</label>
                  </div>
                  <a href="#">Lihat Selengkapnya >></a> */}
                </div>
                {/* <div className="cell">
                  <p>Kategori</p>
                  <div className="cell-row">
                    <input type="checkbox" aria-label="Checkbox for following text input"/>
                    <label>Sport</label>
                  </div>
                  <div className="cell-row">
                    <input type="checkbox" aria-label="Checkbox for following text input"/>
                    <label>Concert</label>
                  </div>
                  <a href="#">Lihat Selengkapnya >></a>
                </div> */}
                <div className="cell">
                  <p>Price</p>
                  <div className="cell-row">
                    <InputRange
                      maxValue={10000000}
                      minValue={0}
                      value={this.state.price}
                      onChange={e => this.setState({ price: e })}
                      onChangeComplete={e => {
                        const newQuery = query;
                        newQuery["max_price"] = e;
                        // Router.pushRoute(
                        //   `/community/local-experiences/${slug}?${QueryString.stringify(
                        //     newQuery
                        //   )}`
                        // );
                      }}
                    />
                  </div>
                </div>
                <div className="cell">
                  <p>Date</p>
                  <div className="cell-row">
                    <SingleDatePicker
                      date={this.state.date}
                      onDateChange={date => {
                        this.setState({ date });
                        const newQuery = query;
                        newQuery["start_date"] = date.format("YYYY-MM-DD");
                        // Router.pushRoute(
                        //   `/community/local-experiences/${slug}?${QueryString.stringify(
                        //     newQuery
                        //   )}`
                        // );
                      }}
                      small
                      block
                      focused={this.state.focused}
                      onFocusChange={({ focused }) =>
                        this.setState({ focused })
                      }
                      id="BOD"
                      placeholder="Pilih Tanggal"
                      withPortal
                    />
                  </div>
                </div>
              </div>
              <div className="col-lg-10 data-table">
                <div className="row flex-row">
                  {localExp.id ? localExp.community_tours.map(item => (
                    <div className="col-lg-4 mb" key={item.code}>
                      <Link route="activity" params={{slug: item.slug}}>
                        <a>
                          <div className="card">
                            <img
                              src={urlImage(
                                item.cover_path + "/" + item.cover_filename
                              )}
                              className="card-img-top"
                              alt="card-img"
                            />
                            <div className="card-body">
                              <h5 className="card-title" style={{textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap'}}>{item.product_name}</h5>
                              <p className="venue">
                                {item.meeting_point_address}
                              </p>
                              <p>
                                {
                                  item.meta_keyword.map((keyword, id) => id > 3 ? null : <Badge>{keyword}</Badge>)
                                }
                              </p>
                              {/* <p className="card-text">
                                <NumberFormat
                                  value={parseInt(item.price, 10)}
                                  displayType={"text"}
                                  thousandSeparator={true}
                                  prefix={"Rp "}
                                />
                              </p>
                              <p className="card-text">
                                {moment(
                                  item.upcomming_schedule,
                                  "YYYY-MM-DD"
                                ).format("DD MMMM YYYY")}
                              </p> */}
                            </div>
                          </div>
                        </a>
                      </Link>
                    </div>
                  )) : null}
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      // </InfiniteScroll>
    );
  }
}

function mapStateToProps(state) {
  return {
    communityReducer: state.community,
    cityReducer: state.city,
    localExp: state.localExperiences.data
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withConnect,
  withAuth(["PUBLIC"])
)(CommunityEvent);
