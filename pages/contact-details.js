import React, { Component } from "react";
import { connect } from 'react-redux'
import { compose } from "redux";
import FormContact from "../components/contact-detail/content";
import SideBar from "../components/shared/sidebar";
import Loading from "../components/shared/loading";
import { get, isEmpty} from 'lodash'
import Head from "../components/head";
import PageHeader from "../components/page-header";
import withAuth from "../_hoc/withAuth";
import { getCookie } from '../utils/cookies';
import { getCartList, checkoutDetails, modalToggle, checkoutPayment, getCountries, checkVoucher, deleteCart } from '../stores/actions';
import { isMobileOnly } from "react-device-detect";
import { logEvent } from "../utils/analytics.js";
import { Modal } from 'react-bootstrap';

 class ContactDetails extends Component {
	static async getInitialProps(context) {
		const { req, store } = context;
		let errorCode = false;
		const ctoken = getCookie("__jocrtkn", req) || null;
		const resCartList = await store.dispatch( getCartList(ctoken, 'schedule|ASC') );
		await store.dispatch( getCountries([]) );
		errorCode = get(resCartList,'meta.code') > 200 ? 404 : false;
		return { errorCode, cart: get(resCartList,'data') }
	}
	componentDidMount(){
		if(this.props.profile) {
			this.setState({ isVoucher: true})
		}
		if(!isEmpty(this.props.cart.coupon_code)) {
			this.setState({
				voucherSuc: true
			})
		}
		
	}

	componentDidUpdate(prevProps) {
		if(prevProps.profile !== this.props.profile) {
			if(!isEmpty(this.props.profile)) {
				this.setState({isVoucher: true})
			} else {
				this.setState({isVoucher: false})
			}
		}
	}
	constructor(props){
		super(props);
		this.state = {
				isModal: false,
				isLoading: false,
				voucher: '',
				voucherErr: '',
				voucherSuc: false,
				isVoucher: false
		}
	}
	handleClose = () => {
    	this.setState({isModal: false});
	}
	detailBooking = () => {
		
    	this.setState({isModal: true});
	}
	submitVoucher = async (e) => {
		try {
			e.preventDefault()
			let { profile, cart, dispatch, gtoken } = this.props
			let { voucher } = this.state
			if(voucher === '') {
				this.setState({voucherErr: "No Voucher is Inputed"})
			} else {
				let params = {
					"guest_detail": {
						"id": profile.id,
						"login": profile.email
					},
					"code": voucher,
					"cart_token": cart.token
				}

				let res = await dispatch(checkVoucher(params))
				if(get(res, 'meta.code') === 200) {
					let data = res.data.original
					if(get(data, 'meta.code') === 200) {
						location.reload()
						this.setState({
							voucherSuc: true
						}, () => dispatch( getCartList(this.props.cart.token, 'schedule|ASC') ))
						
					} else {
						this.setState({
							voucherSuc: false,
							voucherErr: get(data, 'meta.message')
						})
					}
					
				}
			}
		} catch (error) {
			console.log(error);
			
		}
		
	}
	handleSubmitForm = (value, guest) => {
		const {cart, dispatch} = this.props;
		if(this.state.isLoading === false) {
			if(cart.details[0].category_slug == "places"){
				this.setState({isLoading: true})
				let params = {
					token: cart.token,
					user_account: {
						salutation: value.salutation,
						email: value.email,
						firstname: value.firstname,
						lastname: value.lastname,
						phone: value.ext+value.phone,
						country_code: 'id',
						as_member: !guest
					},
				};
				this.setState({isLoading: true});
				dispatch(checkoutDetails(params)).then(result => {
					if (get(result, 'meta.code') === 200) {
						dispatch(checkoutPayment({
							token: cart.token,
							payment_method_id: 1
						})).then(result => {
							this.props.dispatch(deleteCart({token: cart.token, delete: true}))
							.then(res => {
								window.location = result.data.original.data.url
							})
							
						})
					}
					this.setState({isLoading: false});
				});
			} else {
				let params = {
					token: cart.token,
					user_account: {
						salutation: value.salutation,
						email: value.email,
						firstname: value.firstname,
						lastname: value.lastname,
						phone: value.ext+value.phone,
						country_code: 'id',
						as_member: !guest
					},
				};
				this.setState({isLoading: true}); 
				dispatch(checkoutDetails(params)).then(result => {
					if (get(result, 'meta.code') === 200) {
						logEvent('Checkout Payment', 'Checkout Payment');
						this.props.dispatch(deleteCart({token: cart.token, delete: true}))
						.then(res => {
							window.location = result.data.url
						})
						
						// dispatch(modalToggle(true, 'checkout-payment'));
					}
					this.setState({isLoading: false});
				});
			}
		}
		
		
	} 

	render() {
		const {cart, profile} = this.props;
		const {isModal, isLoading} = this.state;
		return (
			<>
				<Head/>
				<PageHeader
					background={"/static/images/img02.jpg"}
					title="Checkout"
				/>
				<section className="content-wrap">
					<div className="container">
						<div className="row">
							<div className="col-lg-8 col-md-8 col-sm-8">
								<FormContact user={profile} handleSubmitForm={this.handleSubmitForm}/>
							</div>
							{
								!isMobileOnly ?
								<SideBar
									title="Trip Details"
									cartList={cart}
									totalPrice={cart.total_paid}
									cartDetails={cart.details}
									isVoucher={this.state.isVoucher}
									voucher={this.state.voucher}
									voucherErr={this.state.voucherErr}
									voucherSuc={this.state.voucherSuc}
									onChangeVoucher={e => this.setState({voucher: e.target.value})}
									onSubmitVoucher={this.submitVoucher}
								/>
								:
								<div className="col-sm-12">
									<button className="btn btn-o mt1 btn-primary btn-block btn-sm" onClick={() => this.detailBooking()}>Review Booking</button>
								</div>
							}
							<Modal show={isModal} onHide={() => this.handleClose()} backdrop dialogClassName="modal-dialog-centered modal-sm">
								<a className="close" onClick={(e) => {e.preventDefault; this.handleClose();}}><i className="ti-close"/></a>
								<div className="modal-header">
									<span className="h4 text-title">Trip Details</span>
								</div>
								<div className="modal-panel">
										<SideBar
											cartList={cart}
											totalPrice={cart.total_paid}
											cartDetails={cart.details}
										/>
								</div>
							</Modal> 
						</div>
					</div>
				</section>
				{isLoading &&
					<Loading/>
				}
			</>
		);
	}
}
const mapStateToProps = state => ({
  profile: state.auth.profile,
})
const mapDispatchToProps = dispatch => ({
});

export default compose(connect(mapStateToProps,mapDispatchToProps), withAuth(["PUBLIC"]))(ContactDetails);