import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight, map } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import withAuth from '../_hoc/withAuth';
import Pagination from "react-js-pagination";
import { FormControl, FormGroup, InputGroup, Col, Panel, PanelGroup } from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import SearchAutoComplete from "../components/shared/searchautocomplete";
import { SingleDatePicker } from "react-dates";
import IconLink from "../components/shared/iconLink";
import 'react-dates/initialize';
import numberWithComma from '../utils/numberWithComma';
import { slugify, urlImage, convertToRp } from '../utils/helpers';
import { getEventList } from '../stores/actions';
import moment from 'moment';
import 'moment/locale/id'
moment.locale('id');
 
const _imgStyle = (url) => {
    return {
        backgroundImage: `url(${url})`,
        backgroundSize: 'cover',
        // backgroundRepeat: 'no-repeat'
        backgroundPosition: ' center center',
    }
  }

class Events extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            temp_search: ['Concert', 'Festival', 'Jakarta Fair'],
            keyword: '',
            data:[],
            page: 1,
            isLoading: false,
            total : 0,
            activePage: 1,
            pagination: null,
         };
    }

    static async getInitialProps({ query, store}) {
        const { slug } = query;
        let paramsSlug = slug;
        let errorCode = false;
        if(slug === "event"){
          paramsSlug = "Events"
        }
    }

    componentDidMount() {
        this.props.dispatch(getEventList({per_page:8})).then(result => {
          const data = result.data;
          map(data, (item, index) => {
            item.name = item.name;
            item.type = item.id;
          });
          this.setState({ data });
          this.setState({total : get(result,'paging.total')});
          this.setState({pagination : get(result, 'paging')});
          
        });
      }
    getEvents = (isRerender) => {
        const { page} = this.state;
        let req = {
          'per_page': 8,
          'page':isRerender ? 1 : page
        };
        this.props.dispatch(getEventList(req)).then(result => {
            const data = result.data;
            map(data, (item, index) => {
              item.name = item.name;
              item.type = item.id;
            });
            this.setState({ data });
            this.setState({total : get(result,'paging.total')});
            this.setState({pagination : get(result, 'paging')});
            
          });
        return req;
      }
    getActivity = async(isRerender = false) => {
        if (this.state.isLoading) {
          return 'isLoading...';
        }
        const params = this.setParams(isRerender);
        const {dispatch } = this.props;
        const page = isRerender ? 0 : this.state.page;
        let dataTmp = !isRerender ? this.state.data : [];
        if(page > 0){
          this.setState({ isLoading: true, btnMobileActive: '' })
          await dispatch(getPlaces(params)).then(result => {
            if (get(result, 'meta.code') === 200){
              this.setState({data: dataTmp.concat(get(result,'data')),page: page+1});
              this.setState({total: get(result,'pagination.total')});
              if(page === get(result,'pagination.last_page')){
                this.setState({more: false});
              }
            }
            this.setState({ isLoading: false })
          });
        }
      }
    handlePageChange(curPage) {
        this.setState({page : curPage}, () =>{
            this.getEvents(false);
        });
        
    }
    render() {
        const {
            data,
            page,
            btnMobileActive,
            total,
            pagination,
        } = this.state;

        return (
            <React.Fragment>
                <Head 
                    title={"Eu nostrud amet esse enim est esse commodo reprehenderit."} 
                    description={"Culpa tempor eu elit duis eu occaecat culpa culpa. Commodo anim commodo occaecat do consequat consequat Lorem laborum qui duis reprehenderit nisi Lorem ullamco. Dolore cupidatat proident esse irure cillum deserunt. Sit et tempor sint duis occaecat ea culpa."} url={process.env.SITE_ROOT + '/p/1180-showcase-indonesia-full-pass-early-bird'} />
                <PageHeader
                background={"/static/images/img05.jpg"}
                title="Find all your Entertainment needs"
                caption="Culpa ad amet non aute do sunt velit veniam et." />
                <section className="main-box sticky events-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-6">
                                        <SearchAutoComplete
                                            id="search"
                                            key={"select-single"}
                                            index={0}
                                            data={this.state.temp_search || []}
                                            placeholder="Start Typing to Search Activity"
                                            labelKey="search"
                                            onChange={result => this.setState({ keyword: get(result, '0.Code') || "" })}
                                            bsSize="large"
                                            className="typeahead search-box-adv single-search"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row" style={{display: 'flex', justifyContent: 'flex-end'}}>
                                    <div className="col-md-4">
                                        <SearchAutoComplete
                                            id="search"
                                            key={"select-single"}
                                            index={0}
                                            data={this.state.temp_search || []}
                                            placeholder="New Event"
                                            labelKey="search"
                                            onChange={result => this.setState({ keyword: get(result, '0.Code') || "" })}
                                            bsSize="large"
                                            className="typeahead search-box-adv single-search"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="body-content mt2 ">
                    <div className="container">
                        <IconLink/>
                    </div> 
               </section>

                <section className="body-content mt2">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <h4 className="text-title">Found {total} in All Category</h4>
                            </div>
                        </div>
                        <ul className="scrollmenu scroll-style">
                            <li>
                                <a href="#">
                                    <div className="category-event-selected select-event-catt" >
                                        All
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#festival-general">
                                    <div className="select-event-catt" >
                                        Festival General
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#concert">
                                    <div className="select-event-catt" >
                                        Concert
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Attraction
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Workshop
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Sport
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Conference
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Conference
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Festival Electronic Dance Music
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Exhibition
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Theater & Drama Musical
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#attraction">
                                    <div className="select-event-catt" >
                                        Transportaion
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div className="product-list">
                            {
                            map(data, (item, index) => (
                            <div className="col-md-3 col-sm-6 col-xs-12 p-item" key={`p-${index}`} >
                            <Link route="event" params={{slug: item.slug}}>
                            <a>
                                <div className="plan-item sr-btm">
                                    <div className="box-img plan-img">
                                        <div className="thumb" style={_imgStyle(item.cover_path+''+item.cover_filename)}>
                                        </div>
                                    </div>
                                    <div className="plan-info padd-info-events">
                                        <ul className="ul-events">
                                            <li className="title">
                                                <h1 className="plan-title event-titlep-card" title={item.name}>
                                                    {item.name}
                                                </h1>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="plan-footer flex-row flex-center">
                                        <div className="price p-event">
                                            <div className="icon-event">
                                                <span className="glyphicon glyphicon-glyphicon glyphicon-map-marker"></span><span>{item.city}</span>
                                            </div>
                                            <div className="icon-event">
                                                <span className="glyphicon glyphicon-glyphicon glyphicon-calendar"></span><span className="text-primary">{moment(item.start_date).format("DD MMMM YYYY")}</span>
                                            </div>
                                            <div className="notice text-info pad-event">Price starts from:</div>
                                            <div className="amount h4 text-primary">
                                                <span>Rp. {numberWithComma(item.start_price)}</span>
                                            </div>
                                                <div className="text-muted" ></div>
                                                <div className="notice text-info" >per person</div>
                                        </div>
                                    </div>
                                </div>{/*plan-item-end*/}
                            </a>
                            </Link>
                            </div>
                                ))
                            }
                            <div className="col-xs-12" style={{ height: "150px" }}>
                                <nav className="pagination-nav">
                                    <span className="text-label">
                                        Displaying {isEmpty(pagination) ? 0 : pagination.displaying} of{" "}
                                        {isEmpty(pagination) ? 0 : pagination.total}
                                    </span>
                                    {!isEmpty (pagination) && 
                                         <Pagination
                                            activePage={page}
                                            itemsCountPerPage={pagination.per_page}
                                            pageCount={pagination.last_page}
                                            totalItemsCount={pagination.total}
                                            pageRangeDisplayed={pagination.displaying }
                                            onChange={(curPage)=>this.handlePageChange(curPage)}
                                        />
                                    }
                                    
                                </nav>
                            </div>
                        </div>
                        
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
  )(Events);