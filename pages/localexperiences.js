import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from './_error';
import { get, isEmpty, takeRight } from 'lodash';
import Head from '../components/head';
import { Link } from '../routes';
import PageHeader from '../components/page-header';
import React from 'react';
import withAuth from '../_hoc/withAuth';

class Localexperience extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Head title={"Local Experience"} description={""} url={process.env.SITE_ROOT + '/about-us'} />
                <PageHeader background={"https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/becomeapartner/shutterstock_1092692273.png"} title={`Local Experiences`} caption=" " />
                <section className="content-wrap bg-content-home">
                    <div className="container">
                        <div className="col-md-12 section-one">
                            <p className="text-center">
                                Local Experiences di Pigijo adalah aktivitas yang memberikan pengalaman luar biasa, menyenangkan dan
                                memorable bagi para wisatawan domestik maupun mancanegara. Local experiences atau umumnya disebut dnegan
                                paket wisata ini, dapat berupa open trip, private trip, maupun aktivitas yang identik dengan daerah
                                tersebut, misalnya daerah laut, maka local experience yang ditawarkan adalah menyelam. Penyelenggara local
                                experiences/paket wisata dapat menyediakan itinerary serta tambahan fasilitas seperti akomodasi,
                                transportasi, spot wisata, travel assistant, dan lain-lain. Pigijo akan menampilkan dan mempromosikan
                                localexperience/paket wisata Anda di website dan aplikasi Pigijo
                            </p>
                        </div>
                    </div>
                </section>
                <section className="content-wrap bg-content-home">
                    <div className="container section-two">
                        <div className="col-md-12">
                            <div className="col-md-offset-2 col-md-8">
                                <p>Persyaratan Umum</p>
                                <ol>
                                    <li>Penyedia local experience memilik KTP.</li>
                                    <li>Penyedia local experience memiliki NPWP (jika ada).</li>
                                    <li>Bagi penyelenggara wisata yang berbadan hukum, dapat mencantumkan legal formal dan sertifikat yang dimiliki/SIUP.</li>
                                    <li>Local Experience/paket wisata melibatkan destinasi lokal atau aktivitas lokal.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="content-wrap bg-content-home">
                    <div className="container">
                        <div className="col-md-12 section-three">
                            <div className="col-md-offset-2 col-md-8">
                                <p>Keuntungan</p>
                                <ol>
                                    <li>Pigijo membantu mempromosikan local experience yang dimiliki.</li>
                                    <li>Transaksi pembayaran antara pengguna local experience dan pihak pengelola dijamin aman dan terpercaya.</li>
                                    <li><b style={{ color: '#aa0f0f' }}>(Proses penerimaan dana kepada mitra TBD by Finance).</b></li>
                                    <li>Besarnya biaya local experience ditentukan sepenuhnya oleh pengelola.</li>
                                </ol>
                                <p>Komponen biaya sewa</p>
                                <ol>
                                    <li><b style={{ color: '#aa0f0f' }}>Biaya transaksi sebesar 5%.</b></li>
                                    <li>Nilai local experience yang ditampilkan resmi di website dan aplikasi Pigijo adalah sebagai berikut:
                                <br />(Harga dasar pengelola + 5 % biaya transaksi + komisi Pigijo) + 10 % PPN
                                <br />TBD by Finance.
                            </li>
                                    <li>Komponen nilai local experience yang harus dipertimbangkan:
                                <ol type="A">
                                            <li>Biaya transaksi 5%</li>
                                            <li>PPN 10%</li>
                                            <li>Komisi Pigijo</li>
                                        </ol>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="content-wrap bg-content-home">
                    <div className="container">
                        <div className="col-md-4 col-md-offset-4 mb1">
                            <a href="https://partner.pigijo.com/login" rel='noopener noreferrer' target="_blank" className="btn btn-primary btn-block">Become a Partner Now</a>
                        </div>
                    </div>
                </section>

                <style jsx>{`
                    .section-one{
                        margin-top : 40px;
                        margin-bottom : 20px;
                    }

                    .section-one p{
                        font-family: roboto;
                        font-size : 14px;
                    }

                    .container.section-two{
                        width: 100%;
                        background-color : #f2f0f0;
                        padding-top : 30px;
                        padding-bottom : 30px;
                        margin-top : 20px;
                        margin-bottom : 20px;
                    }

                    .section-two p{
                        margin-bottom : 20px;
                        font-family: roboto-bold;
                        font-size : 18px;
                    }

                    .section-two ol li{
                        font-family: roboto;
                        font-size : 14px;
                    }

                    .section-three{
                        padding-bottom:50px;
                    }

                    .section-three p{
                        margin-top : 20px;
                        margin-bottom : 20px;
                        font-family: roboto-bold;
                        font-size : 18px;
                    }

                    .section-three ol li{
                        font-family: roboto;
                        font-size : 14px;
                    }

                    @media (max-width: 351px){
                        .header {
                            font-size : 25px;
                            margin-top: 60px;
                            margin-left: 10%;
                        }
                    }

                    @media (min-width: 352px) and (max-width: 575px){
                        .header {
                            font-size : 40px;
                            margin-top: 60px;
                            margin-left: 10%;
                        }
                    }

                    @media (min-width: 576px) and (max-width: 767px){

                    }

                    @media (min-width: 768px) and (max-width: 991px){

                    }

                    @media (min-width: 992px) and (max-width: 1199px){
                        
                    }

                `}</style>
            </React.Fragment >
        )
    }
}

export default compose(
    connect(),
    withAuth(["PUBLIC"])
)(Localexperience);