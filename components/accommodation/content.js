import React from "react";
import moment from "moment";
import { Link } from "../../routes";
import { map, isEmpty } from "lodash";
import Loading from "../home/loading-flashsale";
import { convertToRp } from "../../utils/helpers";
import StarRatingComponent from "react-star-rating-component";

const _imgStyle = (url) => {
	return {
		backgroundImage: `url(${url})`,
		backgroundSize: "cover",
		backgroundPosition: " center center",
		opacity: 0.8,
	};
};
 
// const loopTo = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

const ContentAcc = ({ data, rating, onStarClick, query }) => {
	return (
		<div>
			{/* {loopTo.map((item,key)=>(
        <div key={key}>
          <Loading />
        </div>
      ))} */}
			<div className="product-list">
				{map(data, (item, index) => (
					<div className="col-md-3 col-sm-6 col-xs-12 p-item p-item-o" key={`p-${index}`}>
						<Link href={{pathname: `/accommodation/detail/${item.business_uri}`, query: query}} >
							<a target="">
								<div className="plan-item sr-btm">
									<div className="box-img plan-img" style={{borderBottomLeftRadius: 0, borderBottomRightRadius: 0}}>
										<div className="thumb" style={_imgStyle(item.photo_primary)} />
										{
											item.HotelKey ? (
												<div className="box-img-top" style={{padding: '1em', backgroundImage: 'linear-gradient(to right, transparent, transparent)'}}>
													<span className="badge" style={{backgroundColor: '#F0C65F'}}>
														HOTEL
													</span>
												</div>
											) : (
												<div className="box-img-top" style={{padding: '1em', backgroundImage: 'linear-gradient(to right, transparent, transparent)'}}>
													<span className="badge" style={{backgroundColor: '#43737F'}}>
														HOMESTAY
													</span>
												</div>
											)
										}
										
									</div>
									<div className="plan-info" style={{position: 'initial', paddingBottom: '0.8em', minHeight: '7.5em'}}>
										{/* <div className="disc-flag">
											<img src={require('../../static/images/discount-flag.png')} style={{width: '100%'}}/>
											<h4 className="disc-numb">10%</h4>
											<h4 className="disc-numb" style={{right: '15%', top: '1em', textAlign: 'right'}}>OFF</h4>
										</div> */}
										<ul>
											<li className="title title-max-weight" >
												<p title={item.name} style={{color: '#636060', fontFamily: 'Poppins Medium', fontWeight: 'bold'}}>{item.name.slice(0, 25)}</p>
											</li>
											{/* <StarRatingComponent name="rate1" starCount={5} value={rating} onStarClick={onStarClick} /> */}
											<li className="price prices-normargin">
												<p>
													<span className="price-latest" style={{color: '#e17306', fontSize: '1.1em', fontFamily: 'Poppins Medium', marginTop: '0.5em'}}>
														{convertToRp(parseInt(item.price, 10))} / night
													</span>
												</p>
											</li>
											{
												item.province_name ? (
													<li className="title title-max-weight" >
														<p title={item.province_name} style={{color: '#636060', fontFamily: 'Poppins Medium'}}><img src={require('../../static/icons/pin-maps.png')} style={{width: '4%', marginRight: '2%'}}/> {item.province_name.slice(0, 30)}</p>
													</li>
												) : null
											}
											
										</ul>
									</div>
								</div>
							</a>
						</Link>
					</div>
				))}
			</div>
			<style jsx>{`
				.badge {
					padding: 0.2em 1.5em;
					border-radius: 50px;
					border: 1px solid white;
					font-family: Poppins Medium;
					font-size: 1em;
					font-weight: 100
				}

				.disc-flag {
					position: absolute;
					top: 64%;
					right: 0;
					width: 20%
				}
				
				.disc-numb {
					position: absolute;
					top: 0;
					left: 5px;
					color: white;
					font-family: Poppins Medium;
					margin: 0;
				}
			`}</style>
		</div>
	);
};
export default ContentAcc;
