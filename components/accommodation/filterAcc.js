import React from "react";
import SearchAutoComplete from "../shared/searchautocomplete";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
// import OutsideClickHandler from "react-outside-click-handler";

import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import moment from "moment";
import { Col } from "react-bootstrap";

const FilterAcc = ({
	room,
	adult,
	infant,
	child,
	isShow,
	showPass,
	startDate,
	endDate,
	focusedInput,
	pass,
	locations,
	minus,
	plus,
	handleDestination,
	anyClick,
	handleRoom,
	handlePrice,
	handlePriceRange,
	minMax,
	onDatesChange,
	onFocusChange,
	changeShow,
  convertToRp,
  setWrapperRef,
	setWrapperRefPrice,
	query
}) => {
	return (
		<ul>
			{
				!query.type && !query.slug &&
				<li>
					{/* {isShow === "location" ? (
										<div ref={setWrapperRef}> */}
					<SearchAutoComplete
						id="destination"
						key={"select-single"}
						index={0}
						data={locations || []}
						placeholder="Select City"
						labelKey="name"
						onChange={handleDestination}
						bsSize="small"
						className="search-box-acc single-search"
					/>
					{/* </div>
									) : (
										<button className="btn btn-sm btn-o btn-light" onClick={() => setState({ isShow: "location" })}>
											<span className="ti-location-pin" /> Location
										</button>
									)} */}
				</li>
			}
			
			<li>
				{/* <OutsideClickHandler
											onOutsideClick={() => {
												setState({ isShow: 'null' });
											}}
										> */}
				<div onClick={(e) => changeShow(e, "date")} style={{width: '21em'}}>
					{/* <span className="ti-calendar" />{" "} */}
					<DateRangePicker
							// style={{marginBottom: '0px !important'}}
							// small
							startDatePlaceholderText="Start"
							endDatePlaceholderText="End"
							startDate={startDate}
							startDateId="startDateId"
							endDate={endDate}
							endDateId="endDateId"
							onDatesChange={onDatesChange}
							focusedInput={focusedInput}
							onFocusChange={onFocusChange}
							displayFormat="DD MMM YYYY"
							hideKeyboardShortcutsPanel
							numberOfMonths={1}
							withPortal={false}
							minimumNights={0}							
						/>
				</div>
				{/* </OutsideClickHandler> */}
			</li>
			{/* <li>
				<div className="dropdown-passenger open">
					<button className="btn btn-sm btn-o btn-light" onClick={(e) => changeShow(e, "pass")}>
						{adult > 1 || child > 0 || infant > 0 ? (
							adult + " Adult" + (child > 0 ? `, ${child} Child` : "") + (infant > 0 ? `, ${infant} Infant` : "")
						) : (
							<div>
								<span className="ti-user" /> Guests
							</div>
						)}
					</button>
					<div id='pass' ref={setWrapperRef}>
						<OutsideClickHandler onOutsideClick={anyClick}>
						<div className={isShow === "pass" ? "dropdown-passenger-menu open shadow " : "dropdown-passenger-menu"} id='pass'>
							{pass.map((item, key) => (
								<div key={"pass" + key} className="row panel-body ">
									<Col xs={6} md={5}>
										<p className="text-label" onClick={minus}>{item.name}</p>
										<p className="pass-title">{item.label}</p>
									</Col>
									<Col xs={6} md={7}>
										<ul className="pass-box-qty" align="right">
											<li >
												<button className="btn-guest" onClick={minus}>
													<span className="ti-minus" />
												</button>
											</li>
											<li>
												<span>{item.value}</span>
											</li>
											<li>
												<button className="btn-guest" onClick={plus}>
													<span className="ti-plus" />
												</button>
											</li>
										</ul>
									</Col>
									<hr/>
								</div>
							))}
						</div>
						</OutsideClickHandler>
					</div>
				</div>
			</li> */}
			{/* <li>
				<div className="dropdown-passenger open">
					<button className="btn btn-sm btn-o btn-light" onClick={(e) => changeShow(e, "price")}>
						<span className="ti-money" /> Price
					</button>
          			<div id='price' ref={setWrapperRefPrice}>
						<div className={isShow === "price" ? "dropdown-passenger-menu open shadow " : "dropdown-passenger-menu"}>
							<div className="panel-body">
									<InputRange
										formatLabel={(value) => convertToRp(value)}
										maxValue={2000000}
										minValue={0}
										value={minMax}
										onChange={(value) => handlePriceRange(value)}
										onChangeComplete={(e) => handlePrice(e)} 
									/>
									<p align='center' style={{border:'1px dashed #e17306', padding:'0.8em'}}>{convertToRp(minMax.min) + " - " + convertToRp(minMax.max)}</p>
							</div>
						</div>
					</div>
				</div>
			</li> */}
			<li>
				<div className="person-btn">
					<button
						className="btn btn-sm btn-o btn-light"
						style={{padding: '0.6em 1em'}}
					>
						<select className="form-control" name="person" value={room} onChange={(e) => handleRoom(e.target.value)}>
							<option value="1">1 ROOM</option>
							<option value="2">2 ROOMS</option>
							<option value="3">3 ROOMS</option>
							<option value="4">4 ROOMS</option>
							<option value="5">5 ROOMS</option>
							<option value="6">6 ROOMS</option>
							<option value="7">7 ROOMS</option>
						</select>
						{/* <span className="ti-money" /> Price */}
					</button>
				</div>
			</li>
		</ul>
	);
};

export default FilterAcc;
