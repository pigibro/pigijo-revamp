import React, { Component } from "react";
// import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Tab, Tabs } from "react-bootstrap";
import NumberFormat from "react-number-format";
import { ic_cutlery_24 } from "../../../utils/icons";
import Slider from "../../shared/slider";
import StarRatingComponent from "react-star-rating-component";
import { isMobile } from 'react-device-detect'

class ListRoom extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		let images;
		const {onSelected} = this.props;
		if (this.props.all_photo.length === 1) {
			images = [
				{
					original: this.props.all_photo[0],
					thumbnail: this.props.all_photo[0],
				},
				{
					original: this.props.all_photo[0],
					thumbnail: this.props.all_photo[0],
				},
			];
		} else {
			images = this.props.all_photo.map((item) => {
				return {
					original: item,
					thumbnail: item,
				};
			});
		}
		const _imgStyle = (url) => {
			return {
				backgroundImage: `url(${url})`,
				backgroundSize: "cover",
				backgroundPosition: " center center",
				borderTopLeftRadius: '22px',
				borderBottomLeftRadius: '22px'
			};
		};
		return (
			<div className="list-transport shadow" style={{borderRadius: '22px', boxShadow: '5px 15px 20px #00000029'}}>
				<div className="trip-destination flex-row flex-center">
					<div className="box-img" style={{width: '23%', margin: 0}}>
						<div className="thumb" style={_imgStyle(this.props.thumb)} />
					</div>
					<div className="panel-body" style={{padding: '0 1em'}}>
						<div className="row flex-row flex-center">
							<div className="col-lg-6 col-md-6 col-sm-6">
								<h4 className="text-title acc-name">
									{this.props.title}
								</h4>
								<p className="text-label poppins" style={{marginBottom: 0}}>{this.props.label}</p>
								<p className="text-label poppins"style={{marginBottom: 0}}>{this.props.label2}</p>
								<p className="text-label poppins"style={{marginBottom: 0}}>{this.props.label3}</p>
								{
									this.props.iconStatus ? (
										<p className="text-label poppins" style={{marginBottom: 0, color: '#636060', fontSize: '0.8em'}}>Facilities</p>
									) : null
								}
								
								<div className='fac-box'>
									{
										this.props.facility.length > 0 ? (
											this.props.facility.map((fac, key) => {
												if(fac.icon !== undefined) {
													return (
														<div className="icon-box" key={key}>
															<img src={fac.icon} style={{width: '70%'}}/>
														</div>
													)
												}
											} )
										) : null
									}
								</div>
								{/* <StarRatingComponent name="rate1" starCount={5} value={3} /> */}
							</div>
							<div className="col-lg-6 col-md-6 col-sm-6">
								<div className="price price-box">
									<div className="amount h4 poppins" style={{fontSize: '1.7rem', fontWeight: 100, color: '#FF5B00'}}>
										<NumberFormat
											value={parseInt(this.props.amount, 10)}
											displayType={"text"}
											thousandSeparator={true}
											prefix={"Rp "}
										/>
									</div>
									<span className="text-label text-primary poppins" style={{color: '#FF5B00', fontWeight: 100}}>/room /night</span>
									
								</div>
								<div className="button-box">
									<a
										className="btn btn-primary btn-o"
										data-toggle="collapse"
										data-target={"#" + this.props.detId}
										style={{ width: "115px" }}
									>
										View
									</a>
									{this.props.isAvailable !== "" ? (
										<a onClick={() => onSelected()} className="btn btn-primary choose-room" style={{ width: "115px" }}>
											Select
										</a>
									) : (
										<a className="btn btn-primary choose-room" style={{ width: "115px" }}>
											Select
										</a>
									)}
									</div>
							</div>
						</div>
					</div>
				</div>
				<div className="collapse collapse-wrapper" id={this.props.detId}>
					<div className="panel-body">
						<div className="row">
							<div className="col-lg-5 col-md-5 col-sm-6">
								<Slider images={images} />
							</div>
							<div className="col-lg-7 col-md-7 col-sm-6">
								<Tabs defaultActiveKey={1} id="tab-acc">
									<Tab eventKey={1} title="Room Info">
										<div className="swiper-slide">
											<h4 className="text-title">Room Overview</h4>
											<div className="plan-content">
												<div dangerouslySetInnerHTML={{ __html: this.props.desc }} />
											</div>
										</div>
									</Tab>
									<Tab eventKey={2} title="Room Facilities">
										<div className="swiper-slide" >
											<h4 className="text-title">Room Facilities</h4>
											<div className="plan-content">
												<ul className="facility-icon" style={{display: 'flex', flexWrap: 'wrap'}}>
													{this.props.facility.map((item, key) => (
														<li key={key} style={{width: '32%', lineHeight: 'normal', marginBottom: '0.5em'}}>{item.name}</li>
													))}
												</ul>
											</div>
										</div>
									</Tab>
									<Tab eventKey={3} title="Cancellation Policy">
										<div className="swiper-slide">
											<h4 className="text-title">Cancellation Policy</h4>
											{
												this.props.type !== 'hotel' ? (
													<div className="plan-content">
														{this.props.policy.map((item, key) => (
															<div key={key}>
																{this.props.title === item.name || item.name === "All rooms" ? (
																	<div>
																		
																		{
																			item.tier_one !== '' && (
																				<p>1. {item.tier_one}</p>
																			)
																		}
																		{
																			item.tier_two !== '' && (
																				<p>2. {item.tier_two}</p>
																			)
																		}
																	</div>
																) : (
																	""
																)}
															</div>
														))}
													</div>
												) : (
													<div className="plan-content">
														{this.props.policy.map((item, key) => (
															<div key={key}>
																<div>
																<p>{key+1}. {item}</p>
																</div>
															</div>
														))}
													</div>
												)
											}
											
										</div>
									</Tab>
								</Tabs>
							</div>
						</div>
					</div>
				</div>
				<style jsx global>{`
				.acc-name {
					font-size: 1.2em !important;
					font-family: poppins;
					font-weight: bold;
					color: #636060
				}

				.poppins {
					font-family: Poppins Medium
				}

				.fac-box {
					display: flex;
					align-items: center;
					width: 100%;
					flex-wrap: wrap;
					padding: 0.2em 0 1em 0;
					position: absolute
				}
				
				.icon-box {
					width: 15%
				}

				.price-box {
					display: flex;
					width: 100%;
					flex-direction: column;
					align-items: flex-end;
				}

				.button-box {
					width: 90%;
					position: absolute;
					display:flex;
					flex-wrap: wrap;
					justify-content: space-between
				}

				.nav-tabs li {
					width: auto !important;
					margin-right: 17px;
				}

				#tab-acc ul li a{
					padding: 0 !important
				}

				@media (max-width: 1220px){
					.btn-primary {
						justify-content: center !important;
						width: 45% !important;
						line-height: 0 !important;
						height: 2.5em !important;
						display: flex !important;
						align-items: center !important;
					}
				}

				@media (max-width: 990px) {
					.nav-tabs li {
						width: 30% !important;
						text-align: center;
					}

					.facility-icon {
						flex-direction: column
					}
				}

				.text-title {
					font-family: 'Poppins Medium' !important
				}
				`}</style>
			</div>
		);
	}
}
export default ListRoom;
