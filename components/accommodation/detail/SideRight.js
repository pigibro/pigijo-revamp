import React, { Component } from "react";
import ModalInfo from "./modalInfo";
import numeral from '../../../utils/numeral';
import numberWithComma from '../../../utils/numberWithComma'
import { isMobileOnly } from "react-device-detect";
import { Modal } from 'react-bootstrap'

const SideRight =({ room, isSummary, total_room, handleRoomChange, handleCheckout, night, isModal, handleClose, handleAddToCart})=> {
		if(isSummary) {
			if(isMobileOnly) {
				return (
					<Modal show={isModal} onHide={() => handleClose()} backdrop dialogClassName="modal-dialog-centered modal-sm">
						<a className="close" onClick={(e) => {e.preventDefault; handleClose();}}><i className="ti-close"/></a>
						<div className="modal-header">
						<span className="text-title m0 h4">Booking Details</span>
						</div>
						<div className="modal-panel">
						<div className="col-sm-12 pb1 pt1">
							<section className="side-right">
								<div id="sticky-bar" className="sticky-bar ">
									<div className="panel mb0">
										<h5 className="text-title">Plan Summary</h5>
										<hr />
										<div className="row no-gutters">
											<div className="col-md-3 col-xs-12 mr0">
												<img src={room.all_photo_room[0]} style={{ maxWidth: "70px", maxHeight: "70px" }} />
											</div>
											<div className="col-md-9 col-xs-12 ml0">
												<div className="card-body">
													<h5 className="card-title m0">{room.room_name}</h5>
													<p className="card-text">
														<small className="text-muted">Only {room.room_available} {`room${room.room_available > 1 ? 's' : ""}`} left!</small>
													</p>
													<div className="row">
														<div className="col-md-6 col-xs-12">
															<ul className="pass-box-qty" align="right">
																<li>
																	<button type="button" className="btn-guest" onClick={() => handleRoomChange('minus', room.room_available)}>
																		<span className="ti-minus" />
																	</button>
																</li>
																<li>
																	<span style={{ margin: "1em" }}>{total_room}</span>
																</li>
																<li>
																	<button type="button" className="btn-guest" onClick={() => handleRoomChange('plus', room.room_available)}>
																		<span className="ti-plus" />
																	</button>
																</li>
															</ul>
														</div>
														<div className="col-md-6 col-xs-12">
															<div className="price  text-right">
															<a onClick={() => {}}>Info</a>
																<div className="amount text-primary">
																	<span>Rp {numeral(parseInt(room.price, 10)).format('0,0')}</span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									{/* <a href="#choose-package" className="btn btn-primary btn-o btn-sm btn-block mt1">
										See My Cart List
									</a> */}
									<hr />
									<div className="row no-gutters">
										<div className="col-md-6 col-xs-12" >
											<h5 className="text-title">SUBTOTAL</h5>
										</div>
										<div className="col-md-6 col-xs-12">
											<div className="price text-right">
											
												<div className="amount text-primary">
													<span>Rp {numeral((parseInt(room.price, 10)*parseInt(night, 10)*total_room)).format('0,0')}</span>
												</div>
											</div>
										</div>
										<div className="col-md-12" align="center">
											{/* <p>Already done with your bucket-list?</p> */}
											<a href="#choose-package" className="btn btn-primary btn-md" onClick={handleCheckout} style={{width: '100%'}}>
												Checkout
											</a>
											<button href="#choose-package" className="btn btn-check btn-sm btn-block mt1" onClick={handleAddToCart}>Add to Plan</button>											
										</div>
									</div>
								</div>
							</section>
						</div>
						</div>
					</Modal> 
				)
			} else {
				return (
				<section className="side-right">
					<div id="sticky-bar" className="sticky-bar ">
						<div className="panel mb0">
							<h5 className="text-title">Plan Summary</h5>
							<hr />
							<div className="row no-gutters">
								<div className="col-md-3 col-xs-12 mr0">
									<img src={room.all_photo_room[0]} style={{ maxWidth: "70px", maxHeight: "70px" }} />
								</div>
								<div className="col-md-9 col-xs-12 ml0">
									<div className="card-body">
										<h5 className="card-title m0">{room.room_name ? room.room_name : room.RoomName}</h5>
										<p className="card-text">
											<small className="text-muted">Only {room.room_available} {`room${room.room_available > 1 ? 's' : ""}`} left!</small>
										</p>
										<div className="row">
											<div className="col-md-6 col-xs-12">
												<ul className="pass-box-qty" align="right">
													<li>
														<button type="button" className="btn-guest" onClick={() => handleRoomChange('minus', room.room_available)}>
															<span className="ti-minus" />
														</button>
													</li>
													<li>
														<span style={{ margin: "1em" }}>{total_room}</span>
													</li>
													<li>
														<button type="button" className="btn-guest" onClick={() => handleRoomChange('plus', room.room_available)}>
															<span className="ti-plus" />
														</button>
													</li>
												</ul>
											</div>
											<div className="col-md-6 col-xs-12">
												<div className="price  text-right">
												<a onClick={() => {}}>Info</a>
													<div className="amount text-primary">
														<span>Rp {numeral(parseInt(room.price, 10)).format('0,0')}</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						{/* <a href="#choose-package" className="btn btn-primary btn-o btn-sm btn-block mt1">
							See My Cart List
						</a> */}
						<hr />
						<div className="row no-gutters">
							<div className="col-md-6 col-xs-12" >
								<h5 className="text-title">SUBTOTAL</h5>
							</div>
							<div className="col-md-6 col-xs-12">
								<div className="price text-right">
								
									<div className="amount text-primary">
										<span>Rp {numeral((parseInt(room.price, 10)*parseInt(night, 10)*total_room)).format('0,0')}</span>
									</div>
								</div>
							</div>
							<div className="col-md-12" align="center">
								{/* <p>Already done with your bucket-list?</p> */}
								<a href="#choose-package" className="btn btn-primary btn-block btn-sm" onClick={handleCheckout}>
									Checkout
								</a>
								<button href="#choose-package" className="btn btn-check btn-sm btn-block mt1" onClick={handleAddToCart}>Add to Plan</button>
							</div>
						</div>
					</div>
				</section>
				)
			}
			
		}
		else{
			return (
				<section className="side-right">
				<div id="sticky-bar" className="sticky-bar ">
					<div className="price">
                      <p>Start from: </p>
					  <div className="amount h4 text-primary">
                          Rp {numeral(parseInt(room.price,10)).format('0,0')}
                        {room.price !== room.oldprice && 
                          <span className="last-price">
                            Rp {numeral(parseInt(room.price,10)).format('0,0')}
                          </span>
                        }
                      </div>
					</div>
					<a href="#choose-package" className="btn btn-primary btn-sm btn-block mt1">Choose Room</a>
				</div>
				</section>

			)
		}
		
	}


	export default SideRight;