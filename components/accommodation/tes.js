export const detail =  {
    "primaryPhotos": "http://hotel.pigijo.id/images/hotels/hotel_-1_ol78hatklwtc0w7t5w0f.jpg",
    "breadcrumb": {
        "name": "Flamingo Homestay ",
        "province_name": "Jawa Tengah - Banjarnegara",
        "address": " Jl. Raya Dieng No. 42, RT 01 RW 01, 53456 Dieng, Indonesia.\r\nrumah bpk. Hanafi Aditama)",
        "regional": "Jawa Tengah - Banjarnegara",
        "hotel_id": 233,
        "room_facility_name": "a:7:{i:0;s:1:\"7\";i:1;s:2:\"13\";i:2;s:2:\"15\";i:3;s:2:\"39\";i:4;s:2:\"26\";i:5;s:2:\"37\";i:6;s:2:\"38\";}",
        "oldprice": "250000.00",
        "price": "250000.00",
        "longitude": "109.8947266",
        "latitude": "-7.2153199"
    },
    "policy": [
        {
            "name": "All rooms",
            "tier_one": "Pembatalan maximum 3 Hari sebelum kedatangan",
            "tier_two": "Pembatalan maximum 3 Hari sebelum kedatangan"
        }
    ],
    "avail_facilities": {
        "avail_facilities": [
            "TV",
            "Parking Included",
            "Wireless Internet",
            "Terrace",
            "Bahasa & English Languages",
            "Receptionist 24 Hours",
            "Wake-up services "
        ]
    },
    "all_photo": {
        "photo": [
            "http://hotel.pigijo.id/images/hotels/tfuj0s5h88eh9n8e6ngg.jpg",
            "http://hotel.pigijo.id/images/hotels/ia0pok4obd0jhvgp9c9s.jpg"
        ]
    },
    "results": [
        {
            "room_id": 337,
            "room_available": 4,
            "currency": "idr",
            "minimum_stays": 0,
            "maximum_person": 2,
            "with_breakfasts": "",
            "room_description": "<p>Double room kamar 1, 2 ,3, 5</p>\r\n<p>kamar 5 terdapat TV</p>\r\n<p>kamar mandi dalam</p>",
            "all_photo_room": [
                "http://hotel.pigijo.id/images/rooms/233_icon_gz20f0fdn5yeniy8vktg.jpg",
                "http://hotel.pigijo.id/images/rooms/233_icon_gz20f0fdn5yeniy8vktg_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view1_hjbpk61udzjmykwdtbrh.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view1_hjbpk61udzjmykwdtbrh_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view2_lpw58vb4f5jqnvg92g4e.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view2_lpw58vb4f5jqnvg92g4e_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view3_lm2e7xr10ihp8etf05ip.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view3_lm2e7xr10ihp8etf05ip_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view4_g3fxxv6yc9druf61cx1m.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view4_g3fxxv6yc9druf61cx1m_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/",
                "http://hotel.pigijo.id/images/rooms/"
            ],
            "photo_url": "http://hotel.pigijo.id/images/rooms/233_icon_gz20f0fdn5yeniy8vktg.jpg",
            "room_name": "Double Room",
            "oldprice": "250000.00",
            "price": "250000.00",
            "room_facility": [
                "TV",
                "Cabinet"
            ]
        },
        {
            "room_id": 338,
            "room_available": 2,
            "currency": "idr",
            "minimum_stays": 0,
            "maximum_person": 4,
            "with_breakfasts": "",
            "room_description": "<p>Double room kamar 4 dan 6</p>\r\n<p>kamar 6 terdapat TV</p>\r\n<p>kamar mandi dalam</p>",
            "all_photo_room": [
                "http://hotel.pigijo.id/images/rooms/233_icon_eee4lbw2u18962ggdrgp.jpg",
                "http://hotel.pigijo.id/images/rooms/233_icon_eee4lbw2u18962ggdrgp_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view1_bhovr9r38greu6hfhehc.JPG",
                "http://hotel.pigijo.id/images/rooms/233_view1_bhovr9r38greu6hfhehc_thumb.JPG",
                "http://hotel.pigijo.id/images/rooms/233_view2_kpkkq214thsuyhqpqhsx.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view2_kpkkq214thsuyhqpqhsx_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view3_yjbrot868ohld375578x.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view3_yjbrot868ohld375578x_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view4_r1slhib8z46wmgob0vgg.jpg",
                "http://hotel.pigijo.id/images/rooms/233_view4_r1slhib8z46wmgob0vgg_thumb.jpg",
                "http://hotel.pigijo.id/images/rooms/",
                "http://hotel.pigijo.id/images/rooms/"
            ],
            "photo_url": "http://hotel.pigijo.id/images/rooms/233_icon_eee4lbw2u18962ggdrgp.jpg",
            "room_name": "Quadruple",
            "oldprice": "450000.00",
            "price": "450000.00",
            "room_facility": [
                "TV",
                "Cabinet"
            ]
        }
    ]
}

export const dummy = [
  {
      "name": "Homestay Kelayang Wisata",
      "business_uri": "34-homestay-kelayang-wisata",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_vnclxe10ju8cgmo90kfg.jpg",
      "address": "Jl. Pantai Tanjung Kelayang Tanjung Pandan, Bangka Belitung, 33451",
      "regional": "Belitung",
      "hotel_id": 34,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "TV",
          "Kitchen",
          "Parking Included",
          "Wireless Internet",
          "Terrace",
          "Breakfast",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:9:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:2:\"11\";i:3;s:2:\"13\";i:4;s:2:\"15\";i:5;s:2:\"39\";i:6;s:2:\"22\";i:7;s:2:\"37\";i:8;s:2:\"38\";}",
      "oldprice": "150000.00",
      "price": "150000.00"
  },
  {
      "name": "Homestay Panji & Vadila",
      "business_uri": "37-homestay-panji-vadila",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_r5gwyvo2exkzzqwomgn2.jpg",
      "address": "Jl Ishak Deramad RT.008 RW.002 Desa Tanjong Tinggi Kecamatan Sijuk, Belitung, 33451",
      "regional": "Belitung",
      "hotel_id": 37,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Terrace",
          "Breakfast",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:6:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"39\";i:3;s:2:\"22\";i:4;s:2:\"37\";i:5;s:2:\"38\";}",
      "oldprice": "300000.00",
      "price": "300000.00"
  },
  {
      "name": "Homestay Desa Keciput",
      "business_uri": "56-homestay-desa-keciput",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_jdo257387z11her95yb3.jpg",
      "address": "Jalan tanjung Tinggi Daam RT07/03 Keciput tanjung bandan, Belitung, 33414",
      "regional": "Belitung",
      "hotel_id": 56,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:4:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"37\";i:3;s:2:\"38\";}",
      "oldprice": "125000.00",
      "price": "125000.00"
  },
  {
      "name": "Homestay Maknoi",
      "business_uri": "59-homestay-maknoi",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_keo5rkcfu0r6nf7fzpqx.jpg",
      "address": "Gatot Subroto RT 005 RW 003 Air Saga, Tanjung Pandan, 33415",
      "regional": "Belitung",
      "hotel_id": 59,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:4:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"37\";i:3;s:2:\"38\";}",
      "oldprice": "300000.00",
      "price": "300000.00"
  },
  {
      "name": "Aquatro Homestay (Kreatif Terong)",
      "business_uri": "87-aquatro-homestay-kreatif-terong",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_g9czoc7le2k4i46yyweg.jpg",
      "address": "Jl Tanjung Kelayang Desa Terong RT 005/002 Kecamatan Sijuk Belitung, Tanjung Pandan\r\nKode pos:33451\r\n",
      "regional": "Belitung",
      "hotel_id": 87,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Terrace",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 1,
      "room_facility_name": "a:6:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"39\";i:3;s:2:\"26\";i:4;s:2:\"37\";i:5;s:2:\"38\";}",
      "oldprice": "200000.00",
      "price": "200000.00"
  },
  {
      "name": "Azzahra Homestay (Kreatif Terong)",
      "business_uri": "88-azzahra-homestay-kreatif-terong",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_88_haf9t04ryrmjji5ic307.jpg",
      "address": "Jl Tanjung Kelayang Desa Terong RT 005/002 Kecamatan Sijuk Belitung, Tanjung Pandan\r\nKode pos:33451\r\nIndonesia\r\n",
      "regional": "Belitung",
      "hotel_id": 88,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Terrace",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 1,
      "room_facility_name": "a:6:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"39\";i:3;s:2:\"26\";i:4;s:2:\"37\";i:5;s:2:\"38\";}",
      "oldprice": "200000.00",
      "price": "200000.00"
  },
  {
      "name": "Fajar Homestay (Kreatif Terong)",
      "business_uri": "89-fajar-homestay-kreatif-terong",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_b07p0f7ihubm2sd2ztah.jpg",
      "address": "Jl Tanjung Kelayang Desa Terong RT 005/002 Kecamatan Sijuk Belitung, Tanjung Pandan\r\nKode pos:33451\r\nIndonesia\r\n",
      "regional": "Belitung",
      "hotel_id": 89,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [],
      "cancel_reservation_day": 1,
      "room_facility_name": "",
      "oldprice": "200000.00",
      "price": "200000.00"
  },
  {
      "name": "Mawar Homestay (Kreatif Terong)",
      "business_uri": "90-mawar-homestay-kreatif-terong",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_dx0itczhd9sy2lncrgw4.jpg",
      "address": "",
      "regional": "Belitung",
      "hotel_id": 90,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Terrace",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 1,
      "room_facility_name": "a:6:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"39\";i:3;s:2:\"26\";i:4;s:2:\"37\";i:5;s:2:\"38\";}",
      "oldprice": "200000.00",
      "price": "200000.00"
  },
  {
      "name": "Yaya Homestay (Kreatif Terong)",
      "business_uri": "91-yaya-homestay-kreatif-terong",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_ua84t7mmf686pss3rerg.jpg",
      "address": "Jl Tanjung Kelayang Desa Terong RT 005/002 Kecamatan Sijuk Belitung, Tanjung Pandan\r\nKode pos:33451\r\nIndonesia\r\n",
      "regional": "Belitung",
      "hotel_id": 91,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Terrace",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 1,
      "room_facility_name": "a:6:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"39\";i:3;s:2:\"26\";i:4;s:2:\"37\";i:5;s:2:\"38\";}",
      "oldprice": "200000.00",
      "price": "200000.00"
  },
  {
      "name": "Batu Itam Homestay (Pagal Piling)",
      "business_uri": "117-batu-itam-homestay-pagal-piling",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_dux311w0pnwhoump896k.jpg",
      "address": "Jalan Masjid, RT.003/RW.001, Suak Gual, Selat Nasik, Kabupaten Belitung, Kepulauan Bangka Belitung 33481\r\n",
      "regional": "Belitung",
      "hotel_id": 117,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:4:{i:0;s:1:\"5\";i:1;s:2:\"26\";i:2;s:2:\"37\";i:3;s:2:\"38\";}",
      "oldprice": "120000.00",
      "price": "120000.00"
  },
  {
      "name": "Kakin Homestay (Pagal Piling)",
      "business_uri": "118-kakin-homestay-pagal-piling",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_hbvamqnwtzrgf64onrfj.jpg",
      "address": "Jalan Mawar, RT.004/RW.001, Suak Gual, Selat Nasik, Kabupaten Belitung, Kepulauan Bangka Belitung 33481",
      "regional": "Belitung",
      "hotel_id": 118,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Kitchen",
          "Parking Included",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:6:{i:0;s:1:\"5\";i:1;s:2:\"11\";i:2;s:2:\"13\";i:3;s:2:\"26\";i:4;s:2:\"37\";i:5;s:2:\"38\";}",
      "oldprice": "120000.00",
      "price": "120000.00"
  },
  {
      "name": "Lancor Homestay (Pagal Piling)",
      "business_uri": "119-lancor-homestay-pagal-piling",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_119_mqid4zkx3hk6snmzpydp.jpg",
      "address": "Jalan Masjid RT 003 RW 001 Desa, Suak Gual, Selat Nasik, Kabupaten Belitung, Kepulauan Bangka Belitung 33481",
      "regional": "Belitung",
      "hotel_id": 119,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "TV",
          "Parking Included",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:6:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:2:\"13\";i:3;s:2:\"26\";i:4;s:2:\"37\";i:5;s:2:\"38\";}",
      "oldprice": "120000.00",
      "price": "120000.00"
  },
  {
      "name": "Suak Saer Homestay (Pagal Piling)",
      "business_uri": "120-suak-saer-homestay-pagal-piling",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_nfrttynhc466eiuetdkb.jpg",
      "address": "Suak Gual, Selat Nasik, Kabupaten Belitung, Kepulauan Bangka Belitung 33481",
      "regional": "Belitung",
      "hotel_id": 120,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "TV",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:5:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:2:\"26\";i:3;s:2:\"37\";i:4;s:2:\"38\";}",
      "oldprice": "120000.00",
      "price": "120000.00"
  },
  {
      "name": "Pengkalan Rasit Homestay (Pagal Piling)",
      "business_uri": "121-pengkalan-rasit-homestay-pagal-piling",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_121_kkhv86iniw0ne5jsu7vi.jpg",
      "address": "Jalan Rahman RT 04 RW 02, 33481 Gual, Indonesia , Belitung, 33481\r\nIndonesia\r\n",
      "regional": "Belitung",
      "hotel_id": 121,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:4:{i:0;s:1:\"5\";i:1;s:2:\"26\";i:2;s:2:\"37\";i:3;s:2:\"38\";}",
      "oldprice": "120000.00",
      "price": "120000.00"
  },
  {
      "name": "Belitung Joss Homestay",
      "business_uri": "168-belitung-joss-homestay",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_syfnwyjmd22y31d05qs8.jpeg",
      "address": "Jl. Sehan Umar, Keciput, Sijuk\r\nBelitung, Kepulauan Bangka Belitung, 33414\r\nIndonesia\r\n\r\n",
      "regional": "Belitung",
      "hotel_id": 168,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:5:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"26\";i:3;s:2:\"37\";i:4;s:2:\"38\";}",
      "oldprice": "175000.00",
      "price": "175000.00"
  },
  {
      "name": "Denni & Denis Homestay",
      "business_uri": "170-denni-denis-homestay",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_xpmtb8ka2dxgcpb10oxp.jpeg",
      "address": "Dusun Tanjung Kelayang RT:05 RW:02, Desa Keciput, Kec. Sijuk, Keciput, Tanjungbinga, Kabupaten Belitung, Kepulauan Bangka Belitung, 33414, Indonesia\r\n",
      "regional": "Belitung",
      "hotel_id": 170,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Wireless Internet",
          "Bahasa & English Languages",
          "Receptionist 24 Hours"
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:5:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"15\";i:3;s:2:\"26\";i:4;s:2:\"37\";}",
      "oldprice": "250000.00",
      "price": "250000.00"
  },
  {
      "name": "Yumie Homestay",
      "business_uri": "213-yumie-homestay",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_yv6c0z4whwkr08tazqjy.jpg",
      "address": "Kebun Jeruk Residence Jl.Padat Karya Tanjungpandan,Belitung (Sebelah kiri sebrang kantor, Perawas, Tj. Pandan. Kabupaten Belitung, Kepulauan Bangka Belitung\r\n",
      "regional": "Belitung",
      "hotel_id": 213,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "TV",
          "Kitchen",
          "Parking Included",
          "Bahasa & English Languages",
          "Fan",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:8:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:2:\"11\";i:3;s:2:\"13\";i:4;s:2:\"26\";i:5;s:2:\"29\";i:6;s:2:\"37\";i:7;s:2:\"38\";}",
      "oldprice": "450000.00",
      "price": "450000.00"
  },
  {
      "name": "Batu Penjuru Microcity Belitung",
      "business_uri": "256-batu-penjuru-microcity-belitung",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_fhdn41mpvlatfowx4g13.jpeg",
      "address": "Jl. Tlk. Gembira, Padang Kandis, Membalong, Kabupaten Belitung, Kepulauan Bangka Belitung 33452",
      "regional": "Belitung",
      "hotel_id": 256,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Parking Included",
          "Terrace",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:6:{i:0;s:1:\"5\";i:1;s:2:\"13\";i:2;s:2:\"39\";i:3;s:2:\"26\";i:4;s:2:\"37\";i:5;s:2:\"38\";}",
      "oldprice": "1300000.00",
      "price": "1300000.00"
  },
  {
      "name": "Arumdalu Privat Resorts",
      "business_uri": "260-arumdalu-privat-resorts",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_260_ql2xb7pvr0fxm3ilmjjv.jpg",
      "address": "Jalan Batu Lubang, Membalong, Kabupaten Belitung, 33452, Indonesia.",
      "regional": "Belitung",
      "hotel_id": 260,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "Pool",
          "Parking Included",
          "Wireless Internet",
          "Terrace",
          "Breakfast",
          "Bahasa & English Languages",
          "Coffee/Tea",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:10:{i:0;s:1:\"5\";i:1;s:1:\"8\";i:2;s:2:\"13\";i:3;s:2:\"15\";i:4;s:2:\"39\";i:5;s:2:\"22\";i:6;s:2:\"26\";i:7;s:2:\"32\";i:8;s:2:\"37\";i:9;s:2:\"38\";}",
      "oldprice": "7500000.00",
      "price": "7500000.00"
  },
  {
      "name": "Homestay Belitung",
      "business_uri": "277-homestay-belitung",
      "province_name": "Bangka Belitung",
      "photo_primary": "http://hotel.pigijo.id/images/hotels/hotel_-1_o5xls1n112kxiamjcwts.jpeg",
      "address": "Paktahau no 31 Desa Air Saga, Tanjung pandan Belitung, 33415",
      "regional": "Belitung",
      "hotel_id": 277,
      "longitude": "107.9531836",
      "latitude": "-2.8708938",
      "facilities": [
          "Room Service",
          "TV",
          "Parking Included",
          "Terrace",
          "Bahasa & English Languages",
          "Receptionist 24 Hours",
          "Wake-up services "
      ],
      "cancel_reservation_day": 3,
      "room_facility_name": "a:7:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:2:\"13\";i:3;s:2:\"39\";i:4;s:2:\"26\";i:5;s:2:\"37\";i:6;s:2:\"38\";}",
      "oldprice": "150000.00",
      "price": "150000.00"
  }
]