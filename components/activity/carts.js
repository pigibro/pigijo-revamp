import moment from 'moment';
import { Glyphicon } from 'react-bootstrap';

const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: 'cover',
    backgroundPosition: ' center center',
  }
}

const ActivityCart = () => (
  <>
    <div className="panel panel-info mb1">
      <div className="panel-heading flex-row flex-center">
        <h5 className="text-title">Activities &amp; Places to Explore</h5>
        <span className="text-label mla">Selected 1 Tour(s) </span>
      </div>
    </div>
    <ul className="list-unstyled list-activities">
      <li>
        <div className="row no-gutter">
          <div className="col-lg-2 col-md-2 col-sm-2">
            <div className="act-time">{moment("10:00:00","HH:mm:ss").format("HH:mm")}</div>
            <div className="text-sm">Day 1</div>
          </div>
          <div className="col-lg-10 col-md-10 col-sm-10">
            <div className="panel-trip panel sr-btm">
              <div className="trip-destination">
                <a className="flex-row flex-center" data-toggle="collapse" href="#cart1">
                  <div style={{top:'1em', right:'1em', position:'absolute'}}>
                    <Glyphicon glyph='menu-down '/> 
                  </div>
                  <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-3">
                      <div className="box-img">
                        <div className="thumb" style={_imgStyle("https://s3-ap-southeast-1.amazonaws.com/pigijo/86024cad1e83101d97359d7351051156/2019/03/14/e9fd8ca7702fa847816dd1492b1d3706.jpg")}>
                          {/* <img src="images/img12.jpg" /> */}
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-9">
                      <div className="panel-body panel-pl">
                        <div className="info">
                          <div className="text-label text-primary">Category</div>
                          <h4 className="text-title" style={{fontSize:'1em'}}>1 Day Trip Bidadari Island</h4>
                          <div className="text-label">Beach Family Outdoor</div>
                        </div>{/* info */}
                        <div className="footer">
                          <div className="row flex-row">
                            <div className="col-lg-6 col-md-6 col-sm-6">
                              <div className="text-label">Location</div>
                              <div className="text-dark">Kepulauan Seribu, DKI Jakarta</div>
                            </div>{/* col */}
                            <div className="col-lg-6 col-md-6 col-sm-6">
                              <div className="text-label">Person</div>
                              <div className="text-dark">2 person(s)</div>
                            </div>{/* col */}
                          </div>{/* row */}
                        </div>{/* footer */}
                      </div>{/* panel-body */}
                    </div>
                  </div>
                </a>
              </div>
              <div className="collapse collapse-wrapper" id="cart1">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-label">Meeting Point</div>
                      <div className="text-dark">Jalan Marina Raya Ancol Barat, Kota Tua, Ancol, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta, Indonesia</div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-label">Total Spending</div>
                      <div className="text-dark">
                        <p>2 persons x Rp 380.000</p>
                        <b className="text-primary">Rp 760.000</b>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel-footer">
                  <ul className="footer-nav">
                    <li>See Nearby Places</li>
                    <li>View on Map</li>
                    <li>Remove</li>
                  </ul>
                </div>
              </div>{/* collapse */}
            </div>{/* col-10 */}
          </div>
        </div>
      </li>
    </ul>
  </>
);

export default ActivityCart;