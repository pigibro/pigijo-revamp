import React, { Component } from "react";
import { connect } from "react-redux";
import Swiper from "react-id-swiper/lib";
import numeral from '../../utils/numeral';
import { Link, Router } from '../../routes';
import { map, get, isEmpty } from 'lodash';
// import ImageLoader from '../../utils/ImageLoader';
import { getActivityPromos } from '../../stores/actions';
import { slugify, urlImage } from '../../utils/helpers';
import { Glyphicon } from 'react-bootstrap';
import moment from 'moment';


class Content extends Component {
  constructor(props){
    super(props);
    this.state = {
      stickyWidth: 0
    }
  }
  onScroll(){
    const scroll = document.scrollingElement.scrollTop;
    const stickyBar = document.getElementById('sticky-bar');
    const sideLeft = document.getElementById('side-left');
    const content = document.getElementById('content-activity');
    const { stickyWidth } = this.state; 
    if(stickyWidth === 0){
      this.setState({'stickyWidth': stickyBar.offsetWidth});
    }
    if(scroll >= 270 && scroll <= content.offsetTop+content.offsetHeight-stickyBar.offsetHeight-76-36){
      stickyBar.style.position = 'fixed';
      stickyBar.style.top = '76px';
      stickyBar.style.left = (((document.body.clientWidth-(sideLeft.offsetWidth+stickyWidth))/2)+sideLeft.offsetWidth)+'px';
      stickyBar.style.width = stickyWidth+'px';
    }
    else if(scroll >= content.offsetTop+content.offsetHeight-stickyBar.offsetHeight-76-36){
      stickyBar.removeAttribute('style');
      stickyBar.style.position = 'absolute';
      stickyBar.style.top = (content.offsetHeight-stickyBar.offsetHeight-36)+'px';
      stickyBar.style.width = stickyWidth+'px';
    }else{
      stickyBar.removeAttribute('style');
    }
  }
  componentDidMount(){
    document.addEventListener('scroll', this.onScroll.bind(this))
  }
  componentWillUnmount () {
    document.removeEventListener('scroll', this.onScroll.bind(this));
  }
  render() {
    return (
      <>
        <section className="main-box sticky">
          <div className="container">
            <ul>
              <li>Pilihan Paket</li>
              <li>Deskripsi</li>
              <li>Informasi Activity</li>
              <li>Cara Penggunaan</li>
              <li>Kebijakan Pembatalan</li>
              <li>FAQS</li>
            </ul>
          </div>
        </section>
        <section id="content-activity" className="content-activity">
          <div className="container">
            <section id="side-left" className="side-left">
              <div id="pilihan-paket">
                <h1>Pilihan Paket</h1>
              </div>
              <div id="deskripsi">
                <h1>Deskripsi</h1>
              </div>
              <div id="informasi-activity">
                <h1>Informasi</h1>
              </div>
              <div id="cara-guna">
                <h1>Cara Guna</h1>
              </div>
              
            </section>
            <section className="side-right">
              <div id="sticky-bar" className="sticky-bar">
                <h1>Biaya Kirim</h1>
                <h1>Biaya Kirim</h1>
                <h1>Biaya Kirim</h1>
              </div>
            </section>
          </div>
        </section>
      </>
    );
  }
}


export default Content;