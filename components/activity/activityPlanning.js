
const ActivityPlanning = () => (
  <div className="panel panel-bordered panel-info sr-btm mt2">
    <div className="panel-body">
      <div className='row'>
        <div className='col-lg-12 col-md-12 col-sm-12 bottom-bordered mb2'>
            <div className="text-wrapper">
              <h4 className="text-title text-center">Activities &amp; Places in Jakarta</h4>
            </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-md-6 col-xs-12">
            <p className="text-center">Find Interesting<br/>Full-day and Multiple-days Activities </p>
            <button className="btn btn-block btn-o btn-info">Tour &amp; Event</button>
          </div>
          <div className="col-lg-6 col-md-6 col-xs-12">
            <p className="text-center">FREE and INTERESTING <br/>places in the area</p>
            <button className="btn btn-block btn-info">Places</button>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default ActivityPlanning;