import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { get, isEmpty, map, sum } from 'lodash'
import { Link, Router } from '../routes'
import React from 'react'
import ReactGravatar from 'react-gravatar'
// import userPicture from "/static/images/team1.jpg"
import { logout, setSuccessMessage } from '../stores/actions'
import { modalToggle } from '../stores/actions'
import numeral from '../utils/numeral';
import { slugify, urlImage } from '../utils/helpers';
const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: `100%`,
    backgroundRepeat: `no-repeat`,
  }
}
class Nav extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isMenu: false
    }
    this.changeMenu = this.changeMenu.bind(this)
    this.logout = this.logout.bind(this)
  }
  changeMenu() {
    this.setState({
      isMenu: !this.state.isMenu
    })
  }

  logout() {
    this.props.logout();
    setSuccessMessage('Berhasil Logout')
    if (this.props.isPrivate) {
      Router.pushRoute('/');
      // location.reload();

    }
  }

  modalToggle = (type) => {
    // e.preventDefault();

    this.props.modalToggle(true, type);
  }
  _handleGoToProduct = (item) => {
    switch (item.category_slug) {
      case "places":
        Router.push(`/place/${slugify(`${item.product_id}-${item.product_name}`)}`);
        break;
      case "activity":
        Router.push(`/p/${slugify(`${item.product_id}-${item.product_name}`)}`);
        break;
      case "local-experiences":
        Router.push(`/p/${slugify(`${item.product_id}-${item.product_name}`)}`);
        break;
      case "local-assistants":
        Router.push(`/p/${slugify(`${item.product_id}-${item.product_name}`)}`);
        break;
      case "rent-car":
        Router.push(`/rent-car`);
        break;
      case "accommodations":
        Router.push(`/accommodation`);
        break;
      case "homestay":
        Router.push(`/accommodation`);
        break;
      default:
        Router.push('/')
    }
  }
  render() {
    const {
      categories,
      logout,
      login,
      profile,
      cart,
      cartDetails,
      customClass,
    } = this.props;
    const _dpStyle = (url) => {
      return {
        backgroundImage: `url(${url})`,
        backgroundSize: 'cover',
        backgroundPosition: ' center center',
      }
    }
    return (

      <header className={this.state.isMenu ? `main-header active-menu ${customClass}` : `main-header  ${customClass}`}>
        <div className="container">
          <a href="/" className="logo-header">
            <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/pigijo.png" width="100" />
          </a>
          <div className="lang-header">
            {/* <a href="" className="lang-toggle" data-toggle="dropdown">En</a>
      <div className="dropdown-menu dropdown-menu-custom dropdown-menu-right">
        <ul>
          <li><a href="#!">English</a></li>
          <li><a href="#!">Indonesia</a></li>
        </ul>
      </div> */}
          </div>
          {/* <!-- lang-header --> */}


          <div className="search-mobile">
            <a href="#" className="btn-toggle" onClick={() => this.modalToggle('searchbox')}>
              <div className="box-svg">
                <i className="ti-search" />
              </div>
            </a>
          </div>


          <div className="shopping-cart">
            <div>
              {!isEmpty(cart) && <span className="badge">{cartDetails.length > 0 && cartDetails.length}</span>}
              <a href="#" className="btn-toggle" data-toggle="dropdown">
                <div className="box-svg">
                  <i className="ti-shopping-cart" />
                </div>
              </a>
              <div className="dropdown-menu dropdown-menu-custom dropdown-menu-right">
                <ul>
                  {map(cartDetails, (item, index) => (
                    <li key={`cart-${index}`}>
                      <a onClick={() => this._handleGoToProduct(item)}>
                        <div className="box-img">
                          <div className="box-img-icon" style={_imgStyle(urlImage(item.product_image, item.category_slug))}>

                          </div>
                        </div>
                        <div className="text-cart">
                          <h4 className="text-title">{item.product_name.substring(0, 40)}</h4>
                        </div>
                        {/* <span className="text-cart">
                    {item.product_name.substring(0, 40)}
                  </span> */}
                        <div className="price"><span className="qty">{item.qty} x {item.unit}</span>{item.total_paid > 0 ? `Rp ${numeral(parseInt(item.total_paid, 10)).format('0,0')}` : 'Free Access'}</div>
                      </a>
                    </li>

                  ))}
                  {!isEmpty(cartDetails) &&
                    <li>
                      <Link route="/cart-list">
                        <a className="goto-cart btn btn-blok btn-primary">
                          Your Plan
                  </a>
                      </Link>
                    </li>
                  }


                  {isEmpty(cartDetails) &&
                    <li>
                      <i className="cart-empty icon-cart mb2" />
                      <Link route="/">
                        <a className="btn btn-o btn-primary text-center">
                          Go to Homepage
                  </a>
                      </Link>
                    </li>
                  }
                </ul>
              </div>
            </div>
          </div>
          <div className="user-header">
            {
              !isEmpty(profile) ? (
                <div>
                  <a href="#" className="user-toggle" data-toggle="dropdown">
                    <div className="user-avatar">
                      <div className="box-img-icon">
                        <div className="thumb" style={_dpStyle('/static/images/team1.jpg')}>
                        </div>
                      </div>
                    </div>
                  </a>
                  <div className="dropdown-menu dropdown-menu-custom dropdown-menu-right">
                    <ul>
                      <li className="dropdown-header">
                        <div className="user-name">Hi, <strong style={{ 'textTransform': 'capitalize' }}>{`${(!isEmpty(profile.salutation) ? `${profile.salutation}.` : '')} ${profile.firstname}`}</strong></div>
                        <div className="user-email">{profile.email}</div>
                      </li>

                      <li><Link route="/profile"><a>My Account</a></Link></li>
                      <li><Link to="/my-booking"><a>My Booking<span className="amount">0</span></a></Link></li>
                      <li><a href="#" onClick={this.logout}>Log Out</a></li>
                    </ul>
                  </div>
                </div>
              ) : (
                  <div>
                    <ul className="menu"></ul>
                    <a href="#" className="user-toggle" onClick={() => this.modalToggle('auth')}>
                      <div className="user-avatar ">
                        <div className="box-svg user-log">
                          <i className="ti-user" />
                        </div>
                      </div>
                    </a>
                  </div>
                )
            }
          </div>




          <div className="toggle-menu-wrapper" onClick={this.changeMenu}>
            <span className="label-menu"></span>
            <div className="toggle-menu">
              <div className="toggle-menu-icon">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
          </div>


          <nav className="nav-header">
            <div className="container">
              <div className="menu-header">
                <ul className="menu">
                  {/* <li><Link to="/how-to-plan"><a>How to Plan</a></Link></li> */}
                  <li><Link to="/flight"><a>Flight</a></Link></li>
                  <li><a href="/become-partner" >Become a Partner</a></li>
                  <li><Link to="/community-list"><a >Community Corner</a></Link></li>
                  <li><a href="https://blog.pigijo.com" rel='noopener noreferrer' >Our Stories</a></li>
                  <li>
                    <a href="https://investor.pigijo.com">Investor Relation</a>
                  </li>
                </ul>
                <a href="https://elearning.pigijo.com"><img src={require('../static/icons/pigilearning-new.png')} width='131px'/></a>
                {/* <div className="footer"> */}
                {/* <div className="copyright">© Pigijo 2018. All rights reserved.</div>
            <div className="social-icon">
              <a href="https://www.facebook.com/pigijoid" rel='noopener noreferrer'><i className="socicon socicon-facebook" /></a>
              <a href="https://twitter.com/pigijotweet" rel='noopener noreferrer'><i className="socicon socicon-twitter" /></a>
              <a href="https://www.instagram.com/pigijoo/" rel='noopener noreferrer'><i className="socicon socicon-instagram" /></a> */}
                {/* <a href="#!"><i className="socicon socicon-youtube"></i></a> */}
                {/* </div> */}
                {/* </div> */}
              </div>
            </div>
          </nav>


        </div>
        <style jsx>{`
        .menu li a{
          color: #484848;
          font-weight: bold !important;
          letter-spacing: 0;
          font: 14px Helvetica;
        }
        
        .dropdown-menu{
          z-index: 999;
          
        }

        .dropdown-menu-sub-left{
          margin-left: 1em;
        }
        
        .nav-header{
          padding: 20px 0;
        }
        
        .toggle-menu-icon span{
          background: gray;
        }
        
        .active-menu .menu-header{
          padding-bottom: 15px;
        }
        
        .menu li{
          margin-top: 8px;
          position: relative;
        }
        
        .menu li a{
          margin-right: 20px;
        }
        
        // dropdown
        .dropdown-submenu {
            position: relative;
        }
        
        .dropdown-submenu>.dropdown-menu {
            top: 0;
            left: 100% !important;
            margin-top: -6px;
            margin-left: -1px;
            -webkit-border-radius: 0 6px 6px 6px;
            -moz-border-radius: 0 6px 6px;
            border-radius: 0 6px 6px 6px;
            background: white;
        }
        
        .dropdown-submenu:hover>.dropdown-menu {
            display: block;
        }
        
        .dropdown-submenu>a:after {
            display: block;
            content: " ";
            float: right;
            width: 0;
            height: 0;
            border-color: transparent;
            border-style: solid;
            border-width: 5px 0 5px 5px;
            border-left-color: #ccc;
            margin-top: 5px;
            margin-right: -10px;
            background: white;
        }
        
        .dropdown-submenu:hover>a:after {
            border-left-color: #fff;
        }
        
        .dropdown-submenu.pull-left {
            float: none;
        }
        
        .dropdown-submenu.pull-left>.dropdown-menu {
            left: -100%;
            margin-left: 10px;
            -webkit-border-radius: 6px 0 6px 6px;
            -moz-border-radius: 6px 0 6px 6px;
            border-radius: 6px 0 6px 6px;
        }
        
        @media (max-width: 767px) {
          .menu li a{
            color: #fff;
          }
          
          .dropdown-menu{
            background: #333;
          }
          .search-mobile, .shopping-cart, .user-header{
            float: left !important;
            
          }
          .itech-user-menu-wrapper{
            display: inline;
            justify-content: center;
            display: flex;
          }
        }
          
    `}</style>
      </header>
    )
  }
}

const mapStateToProps = state => ({
  profile: state.auth.profile,
  cart: state.cart.data,
  cartDetails: state.cart.details,
})

const mapDispatchToProps = dispatch => ({
  logout: bindActionCreators(logout, dispatch),
  setSuccessMessage: bindActionCreators(setSuccessMessage, dispatch),
  modalToggle: bindActionCreators(modalToggle, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav);
