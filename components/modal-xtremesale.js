import React,{ Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux"
import { isMobile } from 'lodash'
import { modalToggle } from '../stores/actions'
import ImageBanner from '../static/images/popup_website_xtreamesale.jpg'
import BannerMobile from '../static/images/banner_mobile_xtreamsale.jpg'
import Countdown from 'react-countdown-now';

const _imgStyle = (url) => {
    return {
        backgroundImage: `url(${url})`,
        backgroundSize: 'cover',
        backgroundPosition: ' center center',
    }
  }

const playButton = {
    opacity: 1, 
    color: '#fff', 
    bottom: '1em', 
    top: 'auto', 
    width: '250px', 
    right: '2em'
}

const playButtonMobile = {
    opacity: 1,
    color: '#fff',
    bottom: '4em',
    top: 'auto',
    width: '150px',
    left: '0em',
}

class XtremeSale extends Component {
    constructor(props) {
        super(props)
        this.state={
            isMobile: false
        }
        this.toggle = this.toggle.bind(this)
    }

    componentDidMount(){
        window.innerWidth < 480 ? this.setState({isMobile: true}) : null
    }

    toggle(e){
      e.preventDefault();
      this.props.dispatch(modalToggle(false));
    }
    render() {
        return (
            <div className="modal-panel main-login">
                <div className="row content-row">
                    <div className="col-lg-12 col-md-12 col-sm-12 login-form" style={{padding: 0, minHeight: '620px'}}>
                        <div className="thumb" style={_imgStyle(`../static/images/${this.state.isMobile ? 'banner_popup_mobile_app.jpg' : 'popup_website_xtreamesale.jpg'}`)}/>
                        <a className="close" style={{opacity: 1, color: this.state.isMobile ? "#000" : "#fff"}} href="#" onClick={
                            this.toggle
                        }><i className="ti-close" color={this.state.isMobile ? "#000" : "#fff"}/></a>
                        <a className="close" style={this.state.isMobile ? playButtonMobile : playButton} href="https://play.google.com/store/apps/details?id=app.pigijo.com&hl=en" target="_blank">
                            <img src="../static/images/google-play-badge.png" width="100%"/>
                        </a>                                                
                    </div>
                </div>
            </div>
        )
        
    }
}

const mapDispatchToProps = dispatch => ({
    dispatch: dispatch
  });
  
export default connect(null, mapDispatchToProps)(XtremeSale);

