import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field as ReduxField, reduxForm, change } from 'redux-form';
import { isEmpty, map } from "lodash"
import validate from './validate';
import { getCountries } from '../../stores/actions';

const renderField = ({
  input, placeholder, type, meta: { touched, error }, title
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <input className="form-control" {...input} placeholder={placeholder} type={type}/>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

const renderSelect = ({
  input, placeholder, meta: { touched, error }, title, option, defaultValue
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <select className='form-control' {...input} >
        {
          map(option, (item,idx) => (
            <option key={`ext-${idx}`}>{item.area_code}</option>
          ))
        }
        
      </select>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

const renderSelectSalutation = ({
  input, placeholder, meta: { touched, error }, title
}) => (
    <div className="form-group">
      {!isEmpty(title) && <span className="text-label">{title}</span>}
      <select className='form-control' {...input} >
        <option value='Mr'>Mr.</option>
        <option value='Mrs'>Mrs.</option>
        <option value='Ms'>Miss.</option>
      </select>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

class Content extends Component {
  constructor(props){
    super(props);
    this.state = {
			guest: false
    }
  }
  componentDidMount(){
    
    const {user, form, dispatch} = this.props;    
    if(user){
      
      dispatch(change(form, 'salutation', user.salutation))
      dispatch(change(form, 'firstname', user.firstname));
      dispatch(change(form, 'lastname', user.lastname));
      dispatch(change(form, 'email', user.email));
      if(user.phone){
        const phone = user.phone.substring(3,user.phone.length);
        dispatch(change(form, 'ext', user.phone.substring(0,3)));
        dispatch(change(form, 'phone', phone));
      }
    } else {
      this.setState({guest: true})
    }
  }

  asGuest = (value) => {
    this.setState({guest: !this.state.guest})
    const {form, dispatch, user} = this.props;

    if(value) {
      if(user){
        dispatch(change(form, 'salutation', user.salutation))
        dispatch(change(form, 'firstname', user.firstname));
        dispatch(change(form, 'lastname', user.lastname));
        dispatch(change(form, 'email', user.email));
        if(user.phone){
          const phone = user.phone.substring(3,user.phone.length);
          dispatch(change(form, 'ext', user.phone.substring(0,3)));
          dispatch(change(form, 'phone', phone));
        }
      }
    } else {
      dispatch(change(form, 'firstname', ''));
      dispatch(change(form, 'lastname', ''));
      dispatch(change(form, 'email', ''));
      dispatch(change(form, 'phone', ''));
    }

    
  }

  render() {
    const { handleSubmitForm, handleSubmit, country } = this.props;
    let { guest } = this.state
    return (
      <div className="row">
          <div className='col-lg-12 col-md-12 col-sm-12'>
            <form
              className='form-wrapper'
              onSubmit={handleSubmit(value => { handleSubmitForm(value, guest) })}
              >
              <div>
                <div className='main-title'>
                  <h3 className='text-title'>Contact Person</h3>
                  <span className='help-block'>
                      Fill in the contact person detail for this booking. This contact person will be in charge for this booking, so please make sure the contact person detail is reachable.
                    </span>
                </div>
                <div className='row'>
                
                  <div className="col-md-12">
                    <div className="row">
                      <div className='col-lg-2 col-md-2 col-sm-2'>
                        {/* <span className='text-label'>First Name</span> */}
                        <ReduxField
                        className="form-control"
                        title="Title"
                        component={renderSelectSalutation}
                        name="salutation"/>
                      </div>
                      <div className='col-lg-4 col-md-4 col-sm-4'>
                        {/* <span className='text-label'>First Name</span> */}
                        <ReduxField
                        className="form-control"
                        component={renderField}
                        name="firstname"
                        type="text"
                        title="Firstname"
                        placeholder="Firstname"/>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <ReduxField
                          className="form-control"
                          component={renderField}
                          name="lastname"
                          type="text"
                          title="Lastname"
                          placeholder="Lastname"/>
                      </div>

                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <ReduxField
                      className="form-control"
                      component={renderField}
                      name="email"
                      type="email"
                      title="Email Address"
                      placeholder="Email"/>
                      
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-label'>Phone Number</span>
                    <div className='row sm-gutter form-groups'>
                      <div className='col-lg-3 col-md-3 col-sm-4 col-xs-4'>
                        <ReduxField
                        className="form-control"
                        component={renderSelect}
                        option={country}
                        name="ext"/>
                      </div>
                      <div className='col-lg-9 col-md-9 col-sm-8 col-xs-8'>
                        <ReduxField
                          className="form-control"
                          component={renderField}
                          name="phone"
                          type="text"
                          placeholder="ex. 8123xxx"/>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              {/* { this.FormCus() } */}
              <div className='row mt1 mb2'>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  <div className='row'>
                    <div className='col-lg-5 col-lg-7 col-sm-12'>
                      {
                        this.props.user ? (
                          <label>
                            <input
                              name="guest"
                              type="checkbox"
                              className="styled-checkbox"
                              checked={guest}
                              onChange={e => this.asGuest(guest)}
                              style={{width: 'auto', marginRight: '5px'}}
                            />
                            I'm booking for someone else
                          </label>
                        ) : null
                      }
                    </div>
                    <div className='col-lg-5 col-lg-5 col-sm-12'>
                      <button
                        type='submit'
                        className='btn btn-block btn-primary btn-fix btn-blue'
                        >
                          Continue Payment
                        </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
      </div>  
    )
  }
}
const mapStateToProps = state => ({
  country: state.country.data,
})

Content = reduxForm({
  form: "contact_details",
  initialValues: {
    salutation: 'Mr',
    ext: '+62',
  },
  validate
})(Content);

export default connect(mapStateToProps)(Content);
