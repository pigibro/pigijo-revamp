const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Email field shouldn’t be empty';
  }
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if(!values.firstname){
    errors.firstname = 'Firstname field shouldn’t be empty';
  }
  if(!values.lastname){
    errors.lastname = 'Lastname field shouldn’t be empty';
  }
  if(!values.phone){
    errors.phone = 'Phone Number field shouldn’t be empty';
  }else if(isNaN(values.phone)){
    errors.phone = 'Phone Number field can only contain numbers';
  }else if(!/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(values.phone)) {
    errors.phone = "phone number is not valid"
  }
  return errors;
};

export default validate;
