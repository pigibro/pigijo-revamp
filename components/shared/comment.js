
import React, { Component } from 'react'

export default class Comment extends Component {
  render() {
    return (
      // <div>
<div>
        {/* <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" /> */}
        {/*---- Include the above in your HEAD tag --------*/}
        <div className="container">
          <div className="row">
            <div className="col-sm-8 " id="logout">
              
              <div className="comment-tabs">
                <ul className="nav nav-tabs" role="tablist">
                  <li className="active"><a href="#comments-logout" role="tab" data-toggle="tab"><h4 className="reviews text-capitalize">Comments</h4></a></li>
                  <li><a href="#add-comment" role="tab" data-toggle="tab"><h4 className="reviews text-capitalize">Add comment</h4></a></li>
                </ul>            
                <div className="tab-content">
                  <div className="tab-pane active" id="comments-logout">                
                    <ul className="media-list">
                      <li className="media">
                        
                      </li>          
                      <li className="media">
                        <a className="pull-left" href="#">
                          <img className="media-object img-circle img-circle-size" src="https://s3.amazonaws.com/uifaces/faces/twitter/kurafire/128.jpg" alt="profile" />
                        </a>
                        <div className="media-body">
                          <div className="well well-lg">
                            <h4 className="media-heading text-uppercase reviews">Nico</h4>
                            <ul className="media-date text-uppercase reviews list-inline">
                              <li className="dd">22</li>
                              <li className="mm">09</li>
                              <li className="aaaa">2014</li>
                            </ul>
                            <p className="media-comment">
                              I'm looking for that. Thanks!
                            </p>
                            <a className="btn btn-info btn-circle text-uppercase btn-comment" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                          </div>              
                        </div>
                      </li>
                      <li className="media">
                        <a className="pull-left" href="#">
                          <img className="media-object img-circle img-circle-size" src="https://s3.amazonaws.com/uifaces/faces/twitter/lady_katherine/128.jpg" alt="profile" />
                        </a>
                        <div className="media-body">
                          <div className="well well-lg">
                            <h4 className="media-heading text-uppercase reviews">Kriztine</h4>
                            <ul className="media-date text-uppercase reviews list-inline">
                              <li className="dd">22</li>
                              <li className="mm">09</li>
                              <li className="aaaa">2014</li>
                            </ul>
                            <p className="media-comment">
                              Yehhhh... Thanks for sharing.
                            </p>
                            <a className="btn btn-info btn-circle text-uppercase btn-comment" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                            <a className="btn btn-warning btn-circle text-uppercase btn-comment" data-toggle="collapse" href="#replyTwo"><span className="glyphicon glyphicon-comment" /> 1 comment</a>
                          </div>              
                        </div>
                        <div className="collapse" id="replyTwo">
                          <ul className="media-list">
                            <li className="media media-replied">
                              <a className="pull-left" href="#">
                                <img className="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/jackiesaik/128.jpg" alt="profile" />
                              </a>
                              <div className="media-body">
                                <div className="well well-lg">
                                  <h4 className="media-heading text-uppercase reviews"><span className="glyphicon glyphicon-share-alt" /> Lizz</h4>
                                  <ul className="media-date text-uppercase reviews list-inline">
                                    <li className="dd">22</li>
                                    <li className="mm">09</li>
                                    <li className="aaaa">2014</li>
                                  </ul>
                                  <p className="media-comment">
                                    Classy!
                                  </p>
                                  <a className="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                                </div>              
                              </div>
                            </li>
                          </ul>  
                        </div>
                      </li>
                    </ul> 
                  </div>
                  <div className="tab-pane" id="add-comment">
                    <form action="#" method="post" className="form-horizontal" id="commentForm" role="form"> 
                      <div className="form-group">
                        <label htmlFor="email" className="col-sm-2 control-label">Comment</label>
                        <div className="col-sm-10">
                          <textarea className="form-control" name="addComment" id="addComment" rows={5} defaultValue={""} />
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="uploadMedia" className="col-sm-2 control-label">Upload media</label>
                        <div className="col-sm-10">                    
                          <div className="input-group">
                            <div className="input-group-addon">http://</div>
                            <input type="text" className="form-control" name="uploadMedia" id="uploadMedia" />
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">                    
                          <button className="btn btn-success btn-circle text-uppercase" type="submit" id="submitComment"><span className="glyphicon glyphicon-send" /> Summit comment</button>
                        </div>
                      </div>            
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-10 col-sm-offset-1" id="login">
              <div className="page-header">
                <h3 className="reviews">Leave your comment</h3>
                <div className="logout">
                  <button className="btn btn-default btn-circle text-uppercase" type="button">
                    <span className="glyphicon glyphicon-off" /> Login                    
                  </button>                
                </div>
              </div>
              <div className="comment-tabs">
                <ul className="nav nav-tabs" role="tablist">
                  <li className="active"><a href="#comments-login" role="tab" data-toggle="tab"><h4 className="reviews text-capitalize">Comments</h4></a></li>
                  <li><a href="#add-comment-disabled" role="tab" data-toggle="tab"><h4 className="reviews text-capitalize">Add comment</h4></a></li>
                  <li><a href="#new-account" role="tab" data-toggle="tab"><h4 className="reviews text-capitalize">Create an account</h4></a></li>
                </ul>            
                <div className="tab-content">
                  <div className="tab-pane active" id="comments-login">                
                    <ul className="media-list">
                      <li className="media">
                        <a className="pull-left" href="#">
                          <img className="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/128.jpg" alt="profile" />
                        </a>
                        <div className="media-body">
                          <div className="well well-lg">
                            <h4 className="media-heading text-uppercase reviews">Marco</h4>
                            <ul className="media-date text-uppercase reviews list-inline">
                              <li className="dd">22</li>
                              <li className="mm">09</li>
                              <li className="aaaa">2014</li>
                            </ul>
                            <p className="media-comment">
                              Great snippet! Thanks for sharing.
                            </p>
                            <a className="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                            <a className="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#replyThree"><span className="glyphicon glyphicon-comment" /> 2 comments</a>
                          </div>              
                        </div>
                        <div className="collapse" id="replyThree">
                          <ul className="media-list">
                            <li className="media media-replied">
                              <a className="pull-left" href="#">
                                <img className="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/ManikRathee/128.jpg" alt="profile" />
                              </a>
                              <div className="media-body">
                                <div className="well well-lg">
                                  <h4 className="media-heading text-uppercase reviews"><span className="glyphicon glyphicon-share-alt" /> The Hipster</h4>
                                  <ul className="media-date text-uppercase reviews list-inline">
                                    <li className="dd">22</li>
                                    <li className="mm">09</li>
                                    <li className="aaaa">2014</li>
                                  </ul>
                                  <p className="media-comment">
                                    Nice job Maria.
                                  </p>
                                  <a className="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                                </div>              
                              </div>
                            </li>
                            <li className="media media-replied" id="replied">
                              <a className="pull-left" href="#">
                                <img className="media-object img-circle" src="https://pbs.twimg.com/profile_images/442656111636668417/Q_9oP8iZ.jpeg" alt="profile" />
                              </a>
                              <div className="media-body">
                                <div className="well well-lg">
                                  <h4 className="media-heading text-uppercase reviews"><span className="glyphicon glyphicon-share-alt" /> Mary</h4>
                                  <ul className="media-date text-uppercase reviews list-inline">
                                    <li className="dd">22</li>
                                    <li className="mm">09</li>
                                    <li className="aaaa">2014</li>
                                  </ul>
                                  <p className="media-comment">
                                    Thank you Guys!
                                  </p>
                                  <a className="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                                </div>              
                              </div>
                            </li>
                          </ul>  
                        </div>
                      </li>          
                      <li className="media">
                        <a className="pull-left" href="#">
                          <img className="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kurafire/128.jpg" alt="profile" />
                        </a>
                        <div className="media-body">
                          <div className="well well-lg">
                            <h4 className="media-heading text-uppercase reviews">Nico</h4>
                            <ul className="media-date text-uppercase reviews list-inline">
                              <li className="dd">22</li>
                              <li className="mm">09</li>
                              <li className="aaaa">2014</li>
                            </ul>
                            <p className="media-comment">
                              I'm looking for that. Thanks!
                            </p>
                            <div className="embed-responsive embed-responsive-16by9">
                              <iframe className="embed-responsive-item" src="//www.youtube.com/embed/80lNjkcp6gI" allowFullScreen />
                            </div>
                            <a className="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                          </div>              
                        </div>
                      </li>
                      <li className="media">
                        <a className="pull-left" href="#">
                          <img className="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/lady_katherine/128.jpg" alt="profile" />
                        </a>
                        <div className="media-body">
                          <div className="well well-lg">
                            <h4 className="media-heading text-uppercase reviews">Kriztine</h4>
                            <ul className="media-date text-uppercase reviews list-inline">
                              <li className="dd">22</li>
                              <li className="mm">09</li>
                              <li className="aaaa">2014</li>
                            </ul>
                            <p className="media-comment">
                              Yehhhh... Thanks for sharing.
                            </p>
                            <a className="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                            <a className="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#replyFour"><span className="glyphicon glyphicon-comment" /> 1 comment</a>
                          </div>              
                        </div>
                        <div className="collapse" id="replyFour">
                          <ul className="media-list">
                            <li className="media media-replied">
                              <a className="pull-left" href="#">
                                <img className="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/jackiesaik/128.jpg" alt="profile" />
                              </a>
                              <div className="media-body">
                                <div className="well well-lg">
                                  <h4 className="media-heading text-uppercase reviews"><span className="glyphicon glyphicon-share-alt" /> Lizz</h4>
                                  <ul className="media-date text-uppercase reviews list-inline">
                                    <li className="dd">22</li>
                                    <li className="mm">09</li>
                                    <li className="aaaa">2014</li>
                                  </ul>
                                  <p className="media-comment">
                                    Classy!
                                  </p>
                                  <a className="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span className="glyphicon glyphicon-share-alt" /> Reply</a>
                                </div>              
                              </div>
                            </li>
                          </ul>  
                        </div>
                      </li>
                    </ul> 
                  </div>
                  
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

