const Loading = () => (
  <div>
      <div id="preloader-lay">
      </div>
      <div id="preloader">
          <div id="status">
              <span></span>
              <span></span>
              <span></span>
          </div>
      </div>
  </div>
);
export default Loading;