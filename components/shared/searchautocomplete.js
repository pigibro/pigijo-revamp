import React, { Component } from 'react'
import {Typeahead, Highlighter, Token} from 'react-bootstrap-typeahead';

export default class SearchAutoComplete extends Component {

  onChange = (value) => {
    this.props.onChange(value);
  };

  renderToken = (option,props,index) => {;
    if(index < (this.props.maxLimit || 1)){
      return (
        <Token
          key={index}
          onRemove={props.onRemove}>
          {`${option.name}`}
        </Token>
      );
   }
   if(index > (this.props.maxLimit-1 || 0)){
      setTimeout(() => {
        props.onRemove();
      }, 500);
    }
  }
 _renderMenuItemChildren=(option, props, index) => {
    return  [
      <Highlighter key={index.toString()} search={props.text}>
        {option.name}
      </Highlighter>,
      <span key={"type-"+index} className={option.type !== "" ? "search-type" : ""}>
        <small>
         {option.type.toLocaleString()}
        </small>
      </span>,
      <span className="search-desc" key={"desc-"+index}>
      {option.count_tours !== undefined ? (
        <small>{option.count_tours} tours nearby</small>
      ):""}
      </span>
    ];
  }
  render() {
      return (
        <Typeahead
        id={this.props.id}
        multiple={this.props.multiple === undefined ? false : this.props.multiple === true ? true : false}
        key={"select-"+this.props.index}
        data-index={this.props.index}
        filterBy={['name', 'type']}
        labelKey={this.props.labelKey}
        inputValue={this.props.value}
        onChange={(e) => this.onChange(e)}
        renderToken={(option,props,index) => this.renderToken(option,props,index)}
        options={this.props.data}
        placeholder={this.props.placeholder}
        renderMenuItemChildren={this._renderMenuItemChildren}
        className={this.props.className !== undefined ?this.props.className || "search-box-adv single-search" : this.props.multiple ? "search-box-adv" : "search-box-adv single-search"}
        selectHintOnEnter
        bsSize={this.props.bsSize || 'small'}
        selected={this.props.selected || []}
        defaultInputValue={this.props.defaultInputValue || ''}
        isInvalid={this.props.isInvalid}
        // ref={this.props.reff}
        />
      )
  }

}
