import React, { Component } from 'react';
import ImageGallery from 'react-image-gallery';

import "react-image-gallery/styles/css/image-gallery.css";

export default class Slider extends Component {
  render() {
    return (
      <div>
        <ImageGallery 
          infinite
          showBullets={false}
          // showFullscreenButton
          useBrowserFullscreen
          showPlayButton={false}
          showThumbnails
          showIndex
          disableArrowKeys
          items={this.props.images} 
          style={{maxHeight:'564px'}}
          lazyLoad
          showNav 
        />
      
      </div>
    )
  }
};
