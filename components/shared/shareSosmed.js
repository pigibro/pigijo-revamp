import {
  FacebookShareButton,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon,
  TelegramShareButton,
  TelegramIcon,
  WhatsappShareButton,
  WhatsappIcon
  // PinterestShareButton, PinterestIcon,
  // EmailShareButton, EmailIcon,
} from 'react-share'
import React, { Component } from 'react'

export default class ShareSoc extends Component {
  render() {
    const {uri}=this.props
    return (
      <div>
        <div className='col-md-12 col-sm-12 ev-share'>
      {/* <p className='shareSocTitle'>Share</p>  */}
      <ul>  
        <li>
          <FacebookShareButton url={uri}>
            <FacebookIcon round size={30} />
          </FacebookShareButton>
        </li>
        <li>
          <TwitterShareButton url={uri}>
            <TwitterIcon round size={30} />
          </TwitterShareButton>
        </li> 
        <li>
          <LinkedinShareButton url={uri}>
            <LinkedinIcon round size={30} />
          </LinkedinShareButton>
        </li>
        <li>
          <TelegramShareButton url={uri}>
            <TelegramIcon round size={30} />
          </TelegramShareButton>
        </li>
        <li>
          <WhatsappShareButton url={uri}>
            <WhatsappIcon round size={30} />
          </WhatsappShareButton>
        </li>
      </ul>
    </div>
      </div>
    )
  }
}

