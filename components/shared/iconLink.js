import { carrent, accommodation, TA, event, place, activities, airplane } from "../../utils/icons";
import { isMobileOnly } from 'react-device-detect'
const icon = [
	{ category: "local-experiences", slug: "/l", name: "Tour", img: activities, onsearch: true },
	{ category: "local-assistants", slug: "/l", name: "Assistances", img: TA, onsearch: true },
	{ category: "places", slug: "/places", name: "Places", img: place, onsearch: true },
	// { category: "events", slug: "/c/events", name: "Events", img: event, onsearch: false },
	{ category: "accommodation", slug: "/accommodation/l", name: "Homestay", img: accommodation, onsearch: true },
	{ category: "rent-car", slug: "/rent-car", name: "Vehicle", img: carrent, onsearch: true },
	// { category: "flight", slug: "/flight", name: "Flights", img: airplane, onsearch: false },
];

const IconLink = ({ type, slug, category, route}) => {
	return (
		<div className="col-md-12">
			<div className="row">
				<div className="col-md-6 col-md-offset-3">
					<div className="content--category" style={{ overflowX: "unset" }}>
						{
							icon.map((item, key) => (
								<a href=
									{
										item.onsearch && type && slug ?
											`${item.slug}/${type}/${slug}${item.category === 'places' || item.category === "accommodation" || item.category === "rent-car" ? '' : '/' + item.category}` 
											: (item.category === 'local-experiences' || item.category === 'local-assistants') ? `/c/${item.category}` : (item.slug ==='/accommodation/l' ? '/accommodation' : item.slug)
									}
									className="icon--category" key={key}>
									<div className=
										{
											`${type}/${slug}/${item.category}` === `${type}/${slug}/${category}` ? 'how-to-icon icon-active':
											`${item.category}` === `${route}` ? 'how-to-icon icon-active'
											:'how-to-icon'
										}
									>
										<img src={item.img} alt={item.name} />
									</div>
									<h1 className=
										{
											`${type}/${slug}/${item.category}` === `${type}/${slug}/${category}` ? 'text-title title-active' :
											`${item.category}` === `${route}` ? 'text-title title-active'
											: 'text-title'}
									>
									{item.name}</h1>
								</a>
							))
						}
					</div>
				</div>
			</div>
		</div>
	);
};




export default IconLink;
