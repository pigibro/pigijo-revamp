import React, { Component } from 'react'
import StarRatingComponent from "react-star-rating-component";

export default class Ratting extends Component {
constructor(props){
  super(props);
  this.state={
    star:3
  }
}
  onStarClick = (nextValue, prevValue, name) => {
		this.setState({ rating: nextValue });
  };
  
  render() {
    const {rating} = this.state
    return (
      <div>
        <StarRatingComponent name="rate1" starCount={5} value={rating} onStarClick={this.onStarClick} />
      </div>
    )
  }
}
