import { isEmpty, map, get } from "lodash";
import numeral from "../../utils/numeral";
import { slugify, urlImage } from "../../utils/helpers";
import { Link } from '../../routes';
const SideBar = ({ title, person, startDate, endDate, destination, totalPrice, cartList, cartDetails, closeBtn, handleClose, voucher, onChangeVoucher, isVoucher, onSubmitVoucher, voucherErr, voucherSuc }) => (
	
	<div className={`col-lg-4 col-md-4 col-sm-4 sidebar`}>
		<aside>
			<div className="panel">
				<div className="panel-body">
				{title && <h4 className="text-title">{title}</h4>}
					{(totalPrice || 0) > 0 && (
						<span className="help-block mb1">
							This is the total price for items that need prior booking in your itinerary. 
						</span>
					)}
					<ul className="list-unstyled list-bordered">
						{!isEmpty(person) && (
							<li>
								<div className="text-label">Number of Person</div>
								<div className="text-dark">{person} person(s)</div>
							</li>
						)}
						{!isEmpty(startDate) && !isEmpty(endDate) && (
							<li>
								<div className="text-label">Trip Date</div>
								<div className="text-dark">{`${startDate} → ${endDate}`}</div>
							</li>
						)}
						{!isEmpty(destination) && (
							<li>
								<div className="text-label">Destination</div>
								<div className="text-dark">{destination}</div>
							</li>
						)}
						{!isEmpty(cartList) && 
							map(cartDetails, (item, index) => (
								<li key={`cart-${index}`} className="cart-list">
									<Link route="activity" params={{slug: `${item.product_id}-${slugify(item.product_name)}`}}>
									<a>
										<img className="box-img-icon" src={urlImage(item.product_image,item.category_slug)}/>
										<span>
											{item.product_name} {(item.qty_optional > 0) && 'x '+item.qty_optional+' '+item.unit_optional}
										</span>
										<div className="price"><span className="qty">{item.qty} x {item.unit}</span>{ item.unit_price > 0 ? `Rp ${numeral(parseInt(item.unit_price,10)).format('0,0')}` : 'FREE'}</div>
									</a>
									</Link>
								</li>
								))
						}
						{
							isVoucher && (
								<li>
									<form onSubmit={onSubmitVoucher}>
										<div className="row flex-row flex-center">
											<span className='text-label' style={{marginLeft: '1.5em'}}>Voucher Code</span>
											<div className="col-xs-9">
												{
													voucherSuc === false ? (
														
														<input
														type='text'
														placeholder='Insert voucher code'
														style={{padding: '5px'}}
														value={voucher}
														onChange={onChangeVoucher}/>
														
													) : (
														<span style={{fontWeight: 'bold'}}>{get(cartList, 'coupon_code')}</span>
													)
												}
											</div>
											<div className="col-xs-3" style={{paddingLeft: 0}}>
												{
													voucherSuc === false ? (
														
														<button
															type='submit'
															className='btn btn-block btn-primary btn-fix btn-blue'
															style={{lineHeight: '35px'}}
														>
															Use
														</button>
														
													) : (
														<div className="amount h5 text-primary" style={{fontWeight: 'bold'}}>{`- Rp ${numeral(parseInt(get(cartList, 'total_discount'), 10)).format("0,0")}`}</div>
													)
												}
											</div>
											<span style={{marginLeft: '1.2em', color: 'red'}}>{voucherErr}</span>
										</div>
									</form>
								</li>
							)
						}
						{(totalPrice || 0) > 0 && (
							<li>
								<div className="row flex-row flex-center">
									<div className="col-xs-5">
										Total
										{/* {discount.coupon === undefined?'':discount.coupon} */}
									</div>
									<div className="col-xs-7 text-right">
										<div className="price">
											<div className="amount h4 text-primary">{`Rp ${numeral(parseInt(totalPrice, 10)).format("0,0")}`}</div>
										</div>
									</div>
								</div>
							</li>
						)}
						{
							closeBtn &&
							<li>
								<button className="btn btn-primary btn-block" onClick={() => handleClose()}>Close</button>
							</li>
						}
					</ul>
				</div>
			</div>
		</aside>
	</div>
);

export default SideBar;
