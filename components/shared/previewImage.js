import React, { Component } from 'react';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css'; // This only needs to be imported once in your app
 
// const image_others = [
//   'https://s3-ap-southeast-1.amazonaws.com/pigijo/86024cad1e83101d97359d7351051156/2019/04/16/large/69f23eee896399063d08d9e508a9e976.jpg',
//   'https://s3-ap-southeast-1.amazonaws.com/pigijo/86024cad1e83101d97359d7351051156/2019/04/16/large/69f23eee896399063d08d9e508a9e976.jpg',
//   'https://s3-ap-southeast-1.amazonaws.com/pigijo/86024cad1e83101d97359d7351051156/2019/04/16/large/69f23eee896399063d08d9e508a9e976.jpg',
// ];
 
export default class PreviewImages extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      photoIndex: 0,
    };
  }

  render() {
    const { photoIndex } = this.state;
    const { image_others, isOpen, handleOpen } = this.props;
 
    return (
      <div>
        {isOpen && (
          <Lightbox
            mainSrc={image_others[photoIndex]}
            nextSrc={image_others[(photoIndex + 1) % image_others.length]}
            prevSrc={image_others[(photoIndex + image_others.length - 1) % image_others.length]}
            onCloseRequest={handleOpen}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + image_others.length - 1) % image_others.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % image_others.length,
              })
            }
          />
        )}
      </div>
    );
  }
}