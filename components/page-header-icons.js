import Swiper from "react-id-swiper/lib";
import { urlImage } from "../utils/helpers";
import { map, get, isEmpty } from "lodash";
import React, { Component } from "react";
import PreviewImages from "./shared/previewImage";
import {isMobileOnly} from 'react-device-detect'

const _divStyle = (slide) => {
	return {
		backgroundImage: `url(${slide})`,
		backgroundSize: `cover`,
		backgroundPosition: "center center",
		width: "100%",
		height: "100%",
	};
};

class PageHeaderIcons extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        const { background, title, caption, image_others, type, slug, category, section } = this.props
        return (
            <section className="page-header">
                <div className="thumb page-header-bg" style={_divStyle(background)}>
					{/* <img src={} alt="header"/> */}
				</div>
                <div className="page-header-content" style={{paddingBottom: 0}}>
                    <div className="container">
                        <div className="row flex-center" style={{display: 'flex', justifyContent: 'center'}}>
                            <div className="col-md-8 cat-header">
                                <h3 className="text-title text-first" >Find the best</h3>
                                <h3 className="text-title text-first" style={{fontWeight: 'bold'}}>{title}</h3>
                                <h3 className="text-title" style={{fontFamily: 'Poppins Medium'}}>on your next trip</h3>
                                
                                {
                                    section === 'transportation' && (
                                        <div className="icons-section trans-section">
                                            <a className="trans-menus" href={`/flight`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/flight.png')} style={{width: '45%'}}/>
                                                </div>
                                                <span className={`menu-text ${category === "flight" ? 'active-menu': ''}`}>FLIGHT</span>
                                            </a>
                                            {/* <a className="trans-menus" href={`/train/${type}/${slug}/local-experiences`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/train.png')} style={{width: '45%'}}/>
                                                </div>
                                                <span className={`menu-text ${ category === "train" ? 'active-menu': ''}`}>TRAIN</span>
                                            </a> */}
                                            <a className="trans-menus" href={`/rent-car/${type}/${slug}`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/rent-car.png')} style={{width: '60%'}}/>
                                                </div>
                                                <span className={`menu-text ${category === "rent-car" ? 'active-menu': ''}`}>RENT CAR</span>
                                            </a>
                                        </div>
                                    )
                                }
                                

                                {
                                    section === 'main' && (
                                        <div className="icons-section main">
                                            <a className="menus" href={`/l/${type}/${slug}/local-experiences`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/local-exp.png')} style={{width: '45%'}}/>
                                                </div>
                                                <span className={`menu-text ${ category === "local-experiences" ? 'active-menu': ''}`}>LOCAL EXP.</span>
                                            </a>
                                            <a className="menus" href={`/flight`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/flight.png')} style={{width: '45%'}}/>
                                                </div>
                                                <span className={`menu-text ${category === "flight" ? 'active-menu': ''}`}>FLIGHT</span>
                                            </a>
                                            <a className="menus" href={`/places/${type}/${slug}`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/places.png')} style={{width: '50%'}}/>
                                                </div>
                                                <span className={`menu-text ${category === "places" ? 'active-menu': ''}`}>PLACES</span>
                                            </a>
                                            <a className="menus" href={`/l/${type}/${slug}/local-assistants`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/travel-ast.png')} style={{width: '45%'}}/>
                                                </div>
                                                <span className={`menu-text ${category === "local-assistants" ? 'active-menu': ''}`}>TRAVEL ASSISTANCE</span>
                                            </a>
                                            <a className="menus" href={`/accommodation/l/${type}/${slug}`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/stay.png')} style={{width: '50%'}}/>
                                                </div>
                                                <span className={`menu-text ${category === "accommodation" ? 'active-menu': ''}`}>STAY</span>
                                            </a>
                                            <a className="menus" href={`/rent-car/${type}/${slug}`}>
                                                <div className="icon-box">
                                                    <img src={require('../static/icons/rent-car.png')} style={{width: '65%'}}/>
                                                </div>
                                                <span className={`menu-text ${category === "rent-car" ? 'active-menu': ''}`}>RENT CAR</span>
                                            </a>
                                        </div>
                                    )
                                }

                                
                            </div>
                        </div>
                    </div>
                </div>
                <style jsx>{`
                    .text-first {
                        margin-bottom: 0;
                        font-family: Poppins Medium
                    }

                    .icons-section {
                        display: flex;
                        flex-direction: row;
                        background-color: #F7F7F7;
                        padding: 1.5em;
                        border-top-left-radius: 15px;
                        border-top-right-radius: 15px;
                        font-family: Poppins Medium;
                        display: flex;
                        justify-content: center
                    }

                    .main-section {
                        width: 100%
                    }

                    .trans-section {
                        width: 50%;
                        margin-left: auto;
                        margin-right: auto;
                    }

                    .menus {
                        width: 16.5%;
                        text-align: center
                    }

                    .trans-menus {
                        width: 33%;
                        text-align: center
                    }

                    .icon-box {
                        width: 100%;
                        height: 6em
                    }

                    .menu-text {
                        font-family: Poppins Medium;
                        font-size: 0.8em;
                        font-weight: bold;
                        color: black
                    }

                    .active-menu {
                        color: #F8A20E !important
                    }
                `}</style>
            </section>
        );
    }
}

export default PageHeaderIcons;