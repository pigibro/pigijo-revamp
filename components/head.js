import React from 'react'
import NextHead from 'next/head'
import { string } from 'prop-types'
import { isEmpty } from 'lodash'

const defaultTitle = 'Pigijo - Indonesia Trip Planner & Travel Marketplace'
const defaultDescription = 'We are here to help you enjoy the beauty of Indonesia. Do you need Information about traveling in Indonesia? Tour packages? Homestay & travel assistant? You can find all of it in Pigijo! We are partnering with local businesses to make your vacation in Indonesia better!'
const defaultKeyword = 'Pigijo,Promo,Liburan Murah,Wisata Keluarga,Jakarta,Tour,Open-Trip,November,Pulau Harapan,Festival Nias,Festival Toba,Wakatobi, Bali, Snorkeling'
const defaultOGURL = 'https://www.pigijo.com'
const defaultOGImage = 'https://s3.ap-southeast-1.amazonaws.com/pigijo/assets/images/logo_pigijo_black.svg'

const Head = props => (
  <NextHead>
    <meta charSet="UTF-8" />
    <title>{(props.title) ? `${props.title}` : defaultTitle }</title>
    <meta
      name="description"
      content={props.description || defaultDescription}
    />
    <meta
      name="keywords"
      content={props.keywords || defaultKeyword}
    />
    <meta property="og:url" content={props.url || defaultOGURL} />
    <meta property="og:title" content={(props.title) ? `${props.title} - Pigijo` : defaultTitle } />
    <meta
      property="og:description"
      content={props.description || defaultDescription}
    />
    <meta name="twitter:site" content={props.url || defaultOGURL} />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content={props.ogImage || defaultOGImage} />
    <meta property="og:image" content={props.ogImage || defaultOGImage} />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="900" />
  </NextHead>
)

Head.propTypes = {
  title: string,
  description: string,
  url: string,
  ogImage: string
}

export default Head
