import React, { Component } from "react";
import SearchAutoComplete from "../../shared/searchautocomplete";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import TimePicker from "rc-time-picker/lib";
import "rc-time-picker/assets/index.css";
import moment from "moment";

const SearchSide = ({
	startDate,
	startDaterent,
	endDate,
	focusedInput,
	locations,
	handleDestination,
	onFocusChange,
	onDatesChange,
	handleTime,
	findRentCar,
	validation,
	defaultValue,
	query
}) => {
	return (
		<div className="panel-body">
			<div className="cnt-filter">
				<p className="text-title">Find Car Rental</p>
				<div className="cnt-body">
					{/* <form> */}
					<div className="form-group">
						{!query.type && !query.slug &&
							<>
								<span className="text-label">City</span>
								<SearchAutoComplete
									id="destination"
									key={"select-single"}
									index={0}
									data={locations || []}
									placeholder="Select City"
									selected={defaultValue}
									labelKey="name"
									onChange={handleDestination}
									bsSize="small"
								/>
							</>
						}
						
						<span className="text-label">Dates</span>
						<DateRangePicker
							small
							startDatePlaceholderText="Start"
							endDatePlaceholderText="End"
							startDate={startDaterent || startDate}
							startDateId="startDateId"
							endDate={endDate}
							endDateId="endDateId"
							onDatesChange={onDatesChange}
							focusedInput={focusedInput}
							onFocusChange={onFocusChange}
							displayFormat="DD MMM YYYY"
							hideKeyboardShortcutsPanel
							numberOfMonths={1}
							withPortal={false}
							minimumNights={0}
						/>
						{/* <span className="text-label">Pickup Time</span>
						<TimePicker
							defaultValue={moment("06:00:00", "HH:mm:ss")}
							style={{ width: "100%" }}
							minuteStep={15}
							showSecond={false}
							onChange={handleTime}
						/> */}
					</div>
					{/* <div align="right">
						<button href="" onClick={findRentCar} disabled={validation} className="btn btn-primary btn-sm btn-o">
							Search
						</button>
					</div> */}
					{/* </form> */}
				</div>
			</div>
		</div>
	);
};
export default SearchSide;
