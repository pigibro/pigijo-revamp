import React from "react";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";

const FilterBy = ({
	minMax,
	handlePrice,
	handlePriceRange,
	convertToRp,
	dataType,
	dataBrand,
	dataTrans,
	stateFilter,
	handleOnChange,
}) => {
	return (
		<div className="panel-body">
			<div className="cnt-filter">
				<p className="text-title">Filter by</p>
				<div className="cnt-body">
					<div className="form-group">
						<span className="text-label">Range Price</span>
						<InputRange
							formatLabel={(value) => convertToRp(value)}
							maxValue={2000000}
							minValue={0}
							value={minMax}
							onChange={(value) => handlePriceRange(value)}
							onChangeComplete={(e) => handlePrice(e)}
						/>
						<p align='center' style={{border:'1px dashed #e17306', padding:'0.8em'}}>{convertToRp(minMax.min) + " - " + convertToRp(minMax.max)}</p>
					</div>
					{/* <div className="form-group">
						<span className="text-label">Transportation Type</span>
						<select name="type" onChange={handleOnChange} value={stateFilter.type} className="form-control no-border">
							<option disabled>Transportation Type</option>
							<option key="99" value="">
								All Type
							</option>
							{dataType.map((item, key) => (
								<option key={key} value={item.key}>
									{item.value}
								</option>
								// <option key={key} value={item.key} selected={this.props.paramDetail.type === item.key? true:false }>{item.value}</option>
							))}
						</select>
					</div> */}
					{/* <div className="form-group">
						<span className="text-label">Tranmission</span>
						<select
							name="transmission"
							onChange={handleOnChange}
							value={stateFilter.transmission}
							className="form-control no-border"
						>
							<option disabled>Tranmission</option>
							<option key="99" value="">
								All Tranmission
							</option>
							{dataTrans.map((item, key) => (
								<option key={key} value={item.key}>
									{item.value}
								</option>
								// <option key={key} value={item.key} selected={this.props.paramDetail.type === item.key? true:false }>{item.value}</option>
							))}
						</select>
					</div> */}
					{/* <div className="form-group">
						<span className="text-label">Brand</span>
						<select name="brand" onChange={handleOnChange} value={stateFilter.brand} className="form-control no-border">
							<option disabled>Brand</option>
							<option key="99" value="">
								All Brand
							</option>
							{dataBrand.map((item, key) => (
								<option key={key} value={item.key}>
									{item.value}
								</option>
								// <option key={key} value={item.key} selected={this.props.paramDetail.brand === item.key? true:false }>{item.value}</option>
							))}
						</select>
					</div> */}
				</div>
			</div>
		</div>
	);
};

export default FilterBy;
