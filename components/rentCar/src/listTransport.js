import React, { Component } from "react";
import NumberFormat from "react-number-format";
// import { changeInt } from 'actions/_helper/general'
// import { sendCartCarRent } from "actions/cart/carRentCart.action"
import moment from "moment";

class List extends Component {
	handleOnClickCart = (e) => {
		e.preventDefault();
		// export const sendCartCarRent = (vehicle_id, start_date, end_date, time_from, time_to, day_at) => {

		// this.props.dispatch(sendCartCarRent(
		//     this.props.id,
		//     moment(this.props.cartDetail.startDate).format("YYYY-MM-DD"),
		//     moment(this.props.cartDetail.endDate).format("YYYY-MM-DD"),
		//     moment(this.props.cartDetail.time, "HH:mm").format("HH:mm"),
		//     moment(this.props.cartDetail.time, "HH:mm").add(12, 'hours').format("HH:mm"),
		//     1))
	};
	render() {
		const { type, name, vehicleName, address, thumb, price, service, id, plat, location, description, handleOnClickBook, handleOnClickAdd } = this.props;
		const _imgStyle = (slide) => {
			return {
				backgroundImage: `url(${slide})`,
				backgroundSize: `100%`,
				backgroundPosition: "center center",
				backgroundRepeat: `no-repeat`
			};
		};
		return (
			<div className="list-transport">
				<div className="panel m0">
					<div className="trip-destination flex-row flex-center" data-toggle="collapse" data-target={"#tp" + id}>
						<div className="box-img re-box">
							<div className="thumb" style={_imgStyle(thumb)}>
							</div>
						</div>
						<div className="panel-body rent-panel">
							<div className="flex-row flex-center">
								<div className="info">
									<div className="text-label">{type}</div>
									<h4 className="text-title rent-title">
										{vehicleName} {plat !== "" ? "(" + plat + ")" : ""}{" "}
									</h4>
									<div className="text-sm">{name}</div>
									<div className="text-sm">{location}</div>
								</div>
								<div className="price text-right mla mla-pricerent">
									<div className="text-label">Per Day Starts From</div>
									<div className="amount h4 price-rent">
										<NumberFormat
											value={parseInt(price, 10)}
											displayType={"text"}
											thousandSeparator={true}
											prefix={"Rp "}
											
										/>
									</div>
								</div>
							</div>
							<div className="footer footer-rentCar">
								<div className="row flex-row flex-end">
									<div className="col-lg-2 col-md-2 col-sm-3 col-xs-6">
										<div className="text-label">Brand</div>
										<div className="text-dark">{this.props.brand}</div>
									</div>
									<div className="col-lg-2 col-md-2 col-sm-3 col-xs-6">
										<div className="text-label">Max Hours</div>
										<div className="text-dark">{this.props.max_hours} hour(s)</div>
									</div>
									<div className="col-lg-2 col-md-2 col-sm-3 col-xs-6">
										<div className="text-label">Fuel Type</div>
										<div className="text-dark">{this.props.fuel_type}</div>
									</div>
									<div className="col-lg-2 col-md-2 col-sm-3 col-xs-6">
										<div className="text-label">Transmission</div>
										<div className="text-dark">{this.props.transmission}</div>
									</div>
									<div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 book-rent">
										<button className="btn btn-primary btn-sm mb1" href="" onClick={() => handleOnClickBook(id)}>
										Checkout
										</button>
										<button className="btn btn-check btn-sm" href="" onClick={() => handleOnClickAdd(id)}>
											Add to Plan
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="collapse collapse-wrapper transport-schedule" id={"tp" + id}>
					<div className="panel-body">
						<div className="row text-wrapper">
							<div className="col-lg-3 col-md-3 col-sm-3">
								<h5>Description</h5>
							</div>
							<div className="col-lg-9 col-md-9 col-sm-9">{description}</div>
						</div>
						<div className="space-1" />
						<div className="row text-wrapper">
							<div className="col-lg-3 col-md-3 col-sm-3">
								<h5>Agency Info</h5>
							</div>
							<div className="col-lg-9 col-md-9 col-sm-9">
								<p dangerouslySetInnerHTML={{ __html: service }} />
								<p>address: {address}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default List;
