import React from "react";
import ListTransport from "./src/listTransport";
import Loader from "../loader";
import Pagination from "react-js-pagination";
import { isEmpty } from "lodash";

const RentCarIndex = ({ listCar, pagination, paramDetail, isLoading, handlePageChange, cartDetail, handleOnClickBook, handleOnClickAdd }) => {
	return (
		<div className="">
			<div className="row">
				<div className="col-xs-12">
					{isLoading ? (
						<Loader />
					) : !isEmpty(listCar) ? (
						listCar.map((item) => (
							<ListTransport
								key={item.vehicle_id}
								id={item.vehicle_id}
								type={item.vehicle_type}
								name={item.agency_name}
								location={item.location}
								vehicleName={item.vehicle_name}
								brand={item.vehicle_brand}
								thumb={item.car_image}
								max_hours={item.max_hours}
								fuel_type={item.fuel_type}
								transmission={item.transmission}
								price={item.price_per_day}
								address={item.agency_address}
								service={item.agency_info}
								description={item.description}
								phone={item.agency_phone}
								plat={item.registration_number}
								paramDetail={paramDetail}
								cartDetail={cartDetail}
								handleOnClickBook={handleOnClickBook}
								handleOnClickAdd={handleOnClickAdd}
							/>
						))
					) : (
						<div className="alert alert-bordered" align="center">
							<div className="text-wrapper">
								<h3 style={{ color: "#c3c3c3" }}>&#9785;</h3>
								<h5 style={{ color: "#c3c3c3" }} className="text-title">
									No transportation found in {paramDetail.name}.
								</h5>
							</div>
						</div>
					)}
				</div>
				<div className="col-xs-12" style={{ height: "150px" }}>
					{!isEmpty(pagination) &&
						(!isEmpty(listCar) && (
							<nav className="pagination-nav">
								<span className="text-label">
									Displaying {isEmpty(pagination) ? 0 : pagination.displaying} of{" "}
									{isEmpty(pagination) ? 0 : pagination.total}
								</span>
								<Pagination
									activePage={paramDetail.page}
									itemsCountPerPage={pagination.per_page}
									pageCount={pagination.last_page}
									totalItemsCount={pagination.total}
									pageRangeDisplayed={3}
									onChange={handlePageChange}
								/>
							</nav>
						))}
				</div>
			</div>
		</div>
	);
};

export default RentCarIndex;
