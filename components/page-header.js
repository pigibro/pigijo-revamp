import Swiper from "react-id-swiper/lib";
import { urlImage } from "../utils/helpers";
import { map, get, isEmpty } from "lodash";
import React, { Component } from "react";
import PreviewImages from "./shared/previewImage";
import {isMobileOnly} from 'react-device-detect'

const _divStyle = (slide) => {
	return {
		backgroundImage: `url(${slide})`,
		backgroundSize: `cover`,
		backgroundPosition: "center center",
		width: "100%",
		height: "100%",
	};
};

export default class PageHeader extends Component {
  state={
    isPreview:false
  }
  handleOpen=()=>{
    this.setState({isPreview:!this.state.isPreview})
    // this.props.dispatch(previewPhotos(!this.props.isPreview))
  }
	render() {
    const { background, title, caption, type, image_others } = this.props
    
    return(
			<section className="page-header">
				<div className="thumb page-header-bg" style={_divStyle(background)}>
					{/* <img src={} alt="header"/> */}
				</div>
				<div className="page-header-content">
					<div className="container">
						<div className="row flex-row flex-center">
							<div className="col-lg-7 col-md-7 col-sm-8">
								<h3 className="text-title" dangerouslySetInnerHTML={{ __html: title }} />
								<p dangerouslySetInnerHTML={{ __html: caption }} />
							</div>
							{
                !isEmpty(image_others) && (
									<div className="col-lg-5 col-md-5 col-sm-4">
									<button className="btn btn-o btn-sm btn-primary btn-priview" onClick={this.handleOpen} style={{bottom: 0}}>
										<span className="ti-image" /> See more photos{" "}
									</button>
								</div>
							)}
						</div>
					</div>
				</div>
        <PreviewImages isOpen={this.state.isPreview} image_others={image_others} handleOpen={this.handleOpen} />
			</section>
		);
	}
}
