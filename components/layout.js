import PropTypes from "prop-types"
import Nav from './nav'
import Footer from './footer'
import ModalContainer from './modals/container'
import { isEmpty } from 'lodash'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { removeAlert, modalToggle } from "../stores/actions"

const Layout = ({ children, title = 'This is the default title', customClass, isPrivate, modalToggle }) => (
  <div>
    <Nav isPrivate={isPrivate} customClass={customClass} />
      <main className={`main-page`} style={{'marginTop': '20px'}}>
      {children}
      </main>
    <Footer modalToggle={modalToggle} />
    <ModalContainer/>
  </div>
)

Layout.propTypes = {
  alert: PropTypes.object
};

const mapStateToProps = state => ({
  alert: state.alert,
  modal: state.modal
});

const mapDispatchToProps = dispatch => {
  return {
    removeAlert: bindActionCreators(removeAlert, dispatch),
    modalToggle: bindActionCreators(modalToggle, dispatch)
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout);