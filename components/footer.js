import ScrollToTop from 'react-scroll-up';
import { Link, Router } from '../routes'
import Alert from 'react-s-alert'
import { modalToggle } from "../stores/actions"

const now = new Date();
const style = {
  position: 'unset',
  bottom: 50,
  right: 30,
  cursor: 'pointer',
  transitionDuration: '0.2s',
  transitionTimingFunction: 'linear',
  transitionDelay: '0s'
}
const Footer = (props) => (
  <footer className="main-footer">
    {/* <section className="content-wrap newsletter">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12">
                <div className="main-title text-center sr-btm">
                  <h3 className="text-title">Join Pigijo Community</h3>
                  <p>Be the first to receive updates from Pigijo.</p>
                </div>
                <form className="form-action sr-btm">
                  <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <div className="row">
                          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="form-group">
                              <input className="form-control" type="text" name="firstname" placeholder="First Name" required/>
                            </div>
                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="form-group">
                              <input className="form-control" type="text" name="lastname"  placeholder="Last Name" required/>
                            </div>
                          </div>
                      </div>
                      <div className="form-group form-group-md">
                        <input className="form-control" type="email" name="email" pattern='[^@]+@[^@]+\.[a-zA-Z]{2,6}' placeholder="Email Address" required/>
                        {/* <button className="btn btn-submit" type="submit"><i className="ti-arrow-right" /></button>
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <button className="btn btn-block btn-primary" type="submit" >SUBSCRIBE NOW</button>
                    </div>
                  </div>
                </form>
                <div className="social-icon social-icon-lg sr-btm">
                  <a href="https://www.facebook.com/pigijoid" rel='noopener noreferrer' target="_blank"><i className="socicon socicon-facebook" /></a>
                  <a href="https://twitter.com/pigijotweet" rel='noopener noreferrer' target="_blank"><i className="socicon socicon-twitter" /></a>
                  <a href="https://www.instagram.com/pigijoo/" rel='noopener noreferrer' target="_blank"><i className="socicon socicon-instagram" /></a>
                </div>
              </div>
            </div>
          </div>
        </section> */}

    <section className="main-footer content-wrap footer-info">
      <div className="container">
        <div className="row flex-row flex-center">
          <div className="col-xs-12">
            <div className="row">
              {/* <div className='col-sm-2'>
                <a href='https://play.google.com/store/apps/details?id=app.pigijo.com&hl=en' target='_blank'>
                  <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/google_playstore.png' width='180' />
                </a>
                <a href='https://apps.apple.com/us/app/pigijo-indonesia-trip-planner/id1493384258?ls=1' target='_blank'>
                  <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/app-store.png' width='180' style={{marginTop: '1em'}}/>
                </a>
              </div> */}
              <div className="col-sm-4 col-xs-6">
                <div className="row">
                      <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <p className='font-weight-bold mt-2 mb-4 footer-v2-title'>PRODUCT</p>
                    <ul className=" list-unstyled">
                      <li><Link to="/how-to-plan"><a>How to Plan?</a></Link></li>
                      <li><Link to="/how-to-use"><a>How to Use Pigijo?</a></Link></li>
                      <li><Link to="/term-and-condition"><a>Terms &amp; Conditions</a></Link></li>
                      <li><Link to="/privacy-policy"><a>Privacy Policy</a></Link></li>

                      {/* <li><a href="" onClick={e => {e.preventDefault(); props.modalToggle(true, 'term-and-condition')}}>Terms &amp; Conditions</a></li> */}
                      {/* <li><a href="" onClick={e => {e.preventDefault(); props.modalToggle(true, 'privacy-policy')}}>Privacy Policy</a></li> */}
                      {/* <li><a href="" rel='noopener noreferrer' >Tour Guide</a></li> */}
                      {/* <li><a href="https://tour-guide.pigijo.com/" rel='noopener noreferrer' target="_blank">Tour Guide</a></li> */}
                      {/* <li><ScrollToTop style={style} showUnder={0}><a  href="" onClick={(e) => e.preventDefault()}>Tour Packages</a></ScrollToTop></li> */}
                    </ul>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                    <p className='font-weight-bold mt-2 mb-4 footer-v2-title'>CORPORATE</p>
                    <ul className="list-unstyled">
                      <li><Link to="/about-us"><a>Who We Are?</a></Link></li>
                      <li><a href='https://pigijo.s3-ap-southeast-1.amazonaws.com/pdf/Prospektus+IPO+Pigijo_Final.pdf'>Prospektus Final</a></li>
                      {/*<li><a href="https://blog.pigijo.com" rel='noopener noreferrer' target="_blank">Blog</a></li>*/}
                      <li><Link to="/contact-us"><a>Contact Us</a></Link></li>
                      <li><a href="/press-release">Press Release</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-sm-5 col-xs-6">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <p className='font-weight-bold mt-2 mb-4 footer-v2-title'>PARTNER</p>
                    <ul className="list-unstyled">
                      {/*<li><a href="https://partner.pigijo.com/register#tour-packages" rel='noopener noreferrer'>Register Your Tour Packages</a></li>
                          <li><a href="https://partner.pigijo.com/register#accommodation" rel='noopener noreferrer'>Register Your Property</a></li>
                          <li><a href="https://partner.pigijo.com/register#rent-car" rel='noopener noreferrer'>Register Your Rent Car </a></li>*/}
                      <li><a href='/become-partner' rel='noopener noreferrer'>Become a Partner</a></li>
                      {/* <li><a href="/">Site Map</a></li> */}
                    </ul>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                    <div className="font-weight-bold mt-2 mb-4 footer-v2-title">
                      <p>CUSTOMERS SERVICE</p>
                    </div>
                    <div className='welcome-text'>
                      <p>Monday - Friday : 08.30 - 18.00</p>
                      <div className="social-icon">
                        <a href='mailto:cs@pigijo.com?subject='>
                          <i className="socicon socicon-mail" /> &nbsp; cs@pigijo.com
                              </a>
                      </div>
                      <div className="social-icon">
                        <a href="https://api.whatsapp.com/send?phone=6282246598802">
                          <i className="socicon socicon-whatsapp" /> &nbsp; (+62) 082246598802
                              </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="main-footer content-wrap bottom-footer">
      <div className="container">
        <div className="row flex-row flex-center">
          <div className="col-lg-4 col-md-4 col-sm-12 footer-wrap">
            <div className="logo-footer" />
          </div>
          <div className="col-lg-4 col-md-4 col-sm-12 footer-wrap">
            &copy; Pigijo 2018-2020. All rights reserved
              </div>
          <div className="col-lg-4 col-md-4 col-sm-12 footer-wrap">
            <ScrollToTop showUnder={160}>
              <a className="back-to-top" href="" onClick={(e) => e.preventDefault()}><span className="desktop-back">Back to top <i className="ti-angle-up"></i></span><span className="mobile-back"><i className="ti-angle-double-up" style={{ fontSize: 30, backgroundColor: 'rgba(128, 128, 128, 0.6)' }}></i></span></a>
            </ScrollToTop>
          </div>
        </div>
      </div>
    </section>
    <style jsx>{`
              .main-footer .footer-info{
                padding-top: 30px;
              }
          `}</style>
  </footer>
)

export default Footer;

