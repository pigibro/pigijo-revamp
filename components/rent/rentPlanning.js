const RentPlanning = () => (
  <div className='panel panel-bordered panel-success sr-btm mt2'>
    <div className='panel-body'>
      <div className='row flex-row flex-center'>
        <div className='col-lg-6 col-md-6 col-sm-6'>
          <div className='text-wrapper'>
            <h4 className='text-title'>
            Car Rental
              <br />
            in
            Jakarta
            </h4>
            Find Car Rental Ticket in Jakarta
          </div>
        </div>
        <div className='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-0'>
          <a
            className='btn btn-sm btn-block btn-success'
            href=''
          >
          Find Car Rental
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default RentPlanning;