import { map } from 'lodash';
import ContentLoader from "react-content-loader"
const loopTo = [1, 1];

const Loader = () => (
  <div>
    {map(loopTo, (item, index) => (
      <div key={index} className='panel overlayCard'>
        <ContentLoader
          height={134}
          width={400}
          speed={1}
          primaryColor="#dfdddd"
          secondaryColor="#f0f0f0"
        >
          <rect x="9" y="35" rx="3" ry="3" width="209" height="6" />
          <rect x="9" y="50" rx="3" ry="3" width="362" height="13" />
          <rect x="273" y="89" rx="3" ry="3" width="96" height="18" />
          <rect x="9" y="10" rx="0" ry="0" width="360" height="13" />
          <rect x="37" y="6" rx="0" ry="0" width="0" height="0" />
          <rect x="9" y="77" rx="0" ry="0" width="118" height="10" />
          <rect x="176" y="39" rx="0" ry="0" width="0" height="0" />
        </ContentLoader>

      </div>
    ))}

  </div>
)

export default Loader;