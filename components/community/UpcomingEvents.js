import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Swiper from "react-id-swiper/lib";
import numeral from "../../utils/numeral";
import LoadingEvent from "./loading-event";
import { Link, Router } from "../../routes";
import { map, isEmpty } from "lodash";
import { urlImage, dateGroup } from "../../utils/helpers";
import { Glyphicon } from "react-bootstrap";
import { getLocalEvents } from "../../stores/actions";

class Event extends PureComponent {
  componentDidMount() {
    this.props.dispatch(getLocalEvents(this.props.slug));
  }
  render() {
    const { loadingEvent, localEvents } = this.props;
    const _divStyle = slide => {
      return {
        backgroundImage: `url(${slide})`,
        backgroundSize: `cover`,
        backgroundPosition: "center center",
        width: "100%",
        height: "100%"
      };
    };
    const slider = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    const params = {
      loop: true,
      speed: 600,
      parallax: true,
      parallaxEl: {
        el: ".parallax-bg",
        value: "-23%"
      },
      // renderParallax: () => <div className="parallax-bg" style={parallaxBg} />,

      rebuildOnUpdate: true,
      simulateTouch: false,
      slidesPerView: 3,
      autoplay: true,
      spaceBetween: 30,
      slidesPerColumn: 1,
      paginationClickable: true,
      autoplayDisableOnInteraction: false,
      preloadImages: true,
      containerClass: "place-list swiper-container sr-btm",
      navigation: {
        nextEl: ".swiper-button-next"
        // prevEl: ".swiper-button-prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 3,
          spaceBetween: 40,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 20,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 16,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        320: {
          slidesPerView: 2,
          spaceBetween: 16,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        }
      }
    };

    const styles = {
      productName: {
        position: "absolute",
        marginBottom: "1px",
        margin: "1em",
        color: "white",
        fontWeight: "bold",
        textShadow: "-1px -1px 12px rgba(0,0,0,0.72)"
      }
    };

    return (
      <>
      {
        isEmpty(localEvents) ? null : (
          <div style={styles.subTitile} className="main-title sr-btm">
            <h1 className="text-title">Upcoming Events</h1>
          </div>
        )
      }
      
      {isEmpty(localEvents) ? (
          <>
            {/* <LoadingEvent /> */}
          </>
        ) : (
          <>
      <section className="event-list bg-content-home">
        <div className="container">
          <div className="row">
            {localEvents.length < 3 ? (
              <>
              {map(localEvents, (item, index) => (
                <div className="col-xs-6 col-sm-4">
                  <Link route="activity" params={{ slug: item.slug }}>
                    <a>
                      <div className="plan-item sr-btm">
                        <div className="box-img plan-img">
                          <div className="thumb">
                            <img
                              src={urlImage(
                                item.cover_path +
                                  "/small/" +
                                  item.cover_filename
                              )}
                              alt={item.product_name}
                            />
                            <h2
                              style={styles.productName}
                              className="plan-title"
                            >
                              {item.product_name}
                            </h2>
                          </div>
                        </div>
                      </div>
                    </a>
                  </Link>
                </div>
              ))}
            </>
            ) : (
              <Swiper {...params} style={{ width: "100%" }}>
                {map(localEvents, (item, index) => (
                  <div
                    data-swiper-slide-index={item.slug}
                    key={item.slug}
                    className="swiper-slide-active"
                    style={{ width: "294.25px", marginRight: "30px" }}
                  >
                    <Link href={item.id === 1322 ? "https://join.pigijo.com/pendaftaran-jamnas-2019" : "/p/"+item.slug} params={{ slug: item.slug }}>
                      <a>
                        <div className="plan-item sr-btm">
                          <div className="box-img plan-img">
                            <div className="thumb">
                              <img
                                src={urlImage(
                                  item.cover_path +
                                    "/small/" +
                                    item.cover_filename
                                )}
                                alt={item.product_name}
                              />
                              <h2
                                style={styles.productName}
                                className="plan-title"
                              >
                                {item.product_name}
                              </h2>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                ))}
              </Swiper>
            )}
          </div>
        </div>
        {/* container */}
      </section>
      </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  loadingEvent: state.event.loading,
  localEvents: state.localEvents.data
});

export default connect(mapStateToProps)(Event);
