import { compose } from 'redux';
import { connect } from 'react-redux';
import Error from '../../pages/_error';
import { map, get, isEmpty, takeRight } from 'lodash';
import Head from '../head';
import numeral from '../../utils/numeral';
import { Link } from '../../routes';
import PageHeader from '../../components/page-header';
import Loading from '../../components/home/loading-flashsale';
import InfiniteScroll from 'react-infinite-scroller';
import React from 'react';
import { Glyphicon } from 'react-bootstrap';
import withAuth from '../../_hoc/withAuth';
import { getActivities } from '../../stores/actions';
import { slugify, urlImage } from '../../utils/helpers';
import moment from 'moment';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      // backgroundRepeat: 'no-repeat'
      backgroundPosition: ' center center',
  }
}

class CommunityActivity extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          data: [],
          page: 1,
          isLoading: false,
          product_type:[],
          hasMore: true,
        }
    }

    static async getInitialProps({ query, store}) {
        const { slug, child } = query;
        return {slug: slug, child: child }
    }

    componentDidMount(){
      
    }
    
    handleTypeFilter = (e) => {
      const { product_type } = this.state;
      let newState = product_type;
      if(product_type.indexOf(e.target.value) < 0){
        newState.push(e.target.value);
      }else{
        newState.splice(product_type.indexOf(e.target.value), 1);
      }
      this.setState({
        product_type: newState,
      });
    }

    loadMore = async () => {
      if (this.state.isLoading) {
        return 'isLoading...';
      }
      const { dispatch } = this.props;
      const { page } = this.state;
      let dataTmp = this.state.data;
      this.setState({ isLoading: true })
      const params = {
        'filter': [
          {'product_category': 'Activity'}
        ],
        'per_page': 16,
        'page':page,
      };
      await dispatch(getActivities(params)).then(result => {
        if (get(result, 'meta.code') === 200){
          this.setState({data: dataTmp.concat(get(result,'data')),page: page+1});
          if(page === get(result,'pagination.last_page')){
            this.setState({hasMore: false});
          }
        }
        this.setState({ isLoading: false })
      });
    }
    

    
    
    render(){
      const {slug, child} = this.props;
      const { product_type, data, pagination, hasMore } = this.state;
      const loopTo = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
      return (
        <>
            <div className="container">
              <div className="row">
                <section className="filter-section">
                  filter
                </section>
                <div className="product-list">
                <InfiniteScroll pageStart={0} initialLoad loader={<div style={{padding:"0 .99em"}} key={'loading'}><Loading /></div>} useWindow={true} loadMore={this.loadMore} hasMore={true} threshold={1000}>
                {
                  map(data, (item, index) => (
                    <div className="col-md-3 col-sm-6 col-xs-6 p-item" key={`p-${index}`}>
                      <Link route="activity" params={{slug: item.slug}}>
                      <a>
                      <div className="plan-item sr-btm">
                        <div className="box-img plan-img">
                          <div className="thumb" style={_imgStyle(urlImage(item.image_path+'/'+item.image_filename))}/>
                          <div className="box-img-top">
                            {!isEmpty(item.city) && <span><i className="ti-pin"/> {item.city.name}</span> }
                          </div>
                          <div className="box-img-bottom">
                            {item.discount > 0 && <p className="p_discount">{Math.round(item.discount)}% OFF</p>}
                          </div>
                        </div>
                        <div className="plan-info">
                          <ul>
                            <li className="title"><h3 title={item.name}>{item.name}</h3></li>
                            <li className="min-person">
                              <p className="text-label">Min {item.min_person} person{item.min_person === 1? '' : 's'}</p>
                            </li>
                            <li className="price">
                              {item.discount > 0 && <p className="price-before"><del>Rp {numeral(parseInt(item.price_before,10)).format('0,0')}</del></p>}
                              <p>
                                <span  className="price-latest">
                                  <b className="text-primary">Rp {numeral(parseInt(item.price,10)).format('0,0')}</b><span>/person</span>
                                </span>
                                <span className={ item.upcomming_schedule === moment().format('YYYY-MM-DD') || item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') ? 'notice text-success' : 'notice text-info'}>
                                  { item.upcomming_schedule === moment().format('YYYY-MM-DD') &&
                                    'Available Today'
                                  }
                                  { item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') &&
                                    'Available Tommorow'
                                  }
                                  { moment().add(1,'days').valueOf() < moment(item.upcomming_schedule).valueOf() &&
                                    `Available from ${moment(item.upcomming_schedule).format("DD MMMM YYYY")}`
                                  }
                                </span>
                              </p>
                            </li>
                          </ul>
                        </div>
                      </div>{/*plan-item-end*/}
                      </a>
                      </Link>
                    </div>
                  ))
                }
                </InfiniteScroll>
                </div>
              </div>
            </div>
        </>
      )
    }
}

const mapStateToProps = state => ({

})

export default connect(mapStateToProps)(CommunityActivity);