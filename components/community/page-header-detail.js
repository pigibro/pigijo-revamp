import Swiper from "react-id-swiper/lib";
import { urlImage } from "../../utils/helpers";
import { map, get, isEmpty } from "lodash";

const _divStyle = slide => {
  return {
    backgroundImage: `url(${slide})`,
    backgroundSize: `cover`,
    backgroundPosition: "center center",
    width: "100%",
    height: "100%"
  };
};

const _avatarCommunity = img => {
  return {
    height: "100px",
    width: "100px",
    margin: "4px",
    backgroundImage: `url(${img})`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain"
  };
};

const styles = {
  joinButton: {
    textAlign: "right"
  },
  joinContainer: {
    textAlign: "right"
  },
  caption: {
    backgroundColor: "#4CAF50",
    display: "inline-block",
    paddingLeft: "8px",
    paddingRight: "8px",
    paddingBottom: "3px"
  }
};

const PageHeaderDetail = ({ background, title, caption, imageAvatar }) => (
  <section className="page-header">
    <div className="thumb page-header-bg" style={_divStyle(background)}>
      {/* <img src={} alt="header"/> */}
    </div>
    <div className="page-header-content">
      <div className="container">
        <div className="row flex-row flex-center">
          <div className="col-lg-2 col-md-2 col-sm-2">
            <center>
              <div style={_avatarCommunity(imageAvatar)} />
            </center>
          </div>
          <div className="col-lg-10 col-md-10 col-sm-10">
            <h3
              className="text-title"
              dangerouslySetInnerHTML={{ __html: title }}
            />
            <p
              style={styles.caption}
              dangerouslySetInnerHTML={{ __html: caption }}
            />

            <div style={styles.joinContainer}>
              <a
                href="#"
                className="btn btn-primary btn-sm"
                style={styles.joinButton}
              >
                JOIN
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default PageHeaderDetail;
