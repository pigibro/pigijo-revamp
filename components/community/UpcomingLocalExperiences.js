import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Swiper from "react-id-swiper/lib";
import numeral from "../../utils/numeral";
import LoadingEvent from "./loading-event";
import { Link, Router } from "../../routes";
import { map, isEmpty } from "lodash";
import { urlImage, dateGroup } from "../../utils/helpers";
import { Glyphicon } from "react-bootstrap";
import { getLocalExp } from "../../stores/actions";

class LocalExperiences extends PureComponent {
  componentDidMount() {
    this.props.dispatch(getLocalExp(this.props.slug, { per_page: 8 }));
  }
  render() {
    const { loadingEvent, localExp } = this.props;
    const _divStyle = slide => {
      return {
        backgroundImage: `url(${slide})`,
        backgroundSize: `cover`,
        backgroundPosition: "center center",
        width: "100%",
        height: "100%"
      };
    };
    const slider = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    const params = {
      loop: true,
      speed: 600,
      parallax: true,
      parallaxEl: {
        el: ".parallax-bg",
        value: "-23%"
      },
      // renderParallax: () => <div className="parallax-bg" style={parallaxBg} />,

      rebuildOnUpdate: true,
      simulateTouch: false,
      slidesPerView: 3,
      autoplay: true,
      spaceBetween: 30,
      slidesPerColumn: 1,
      paginationClickable: true,
      autoplayDisableOnInteraction: false,
      preloadImages: true,
      containerClass: "place-list swiper-container sr-btm",
      navigation: {
        nextEl: ".swiper-button-next"
        // prevEl: ".swiper-button-prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 3,
          spaceBetween: 40,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 20,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 16,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        320: {
          slidesPerView: 2,
          spaceBetween: 16,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        }
      }
    };

    const styles = {
      productName: {
        position: "absolute",
        marginBottom: "1px",
        margin: "1em",
        color: "white",
        fontWeight: "bold",
        fontSize: "25px",
        textShadow: "-1px -1px 12px rgba(0,0,0,0.72)"
      }
    };

    return (
      <>
        {
          isEmpty(localExp) ? null : (
            <div style={styles.subTitile} className="main-title sr-btm">
              <h1 className="text-title">Must Join</h1>
            </div>
          )
        }
        
        {isEmpty(localExp) ? (
          <>
            {/* <LoadingEvent /> */}
          </>
        ) : (
          <>
            <div className="event-list bg-content-home">
              <div className="container">
                <div className="row flex-row flex-center">
                  {loadingEvent || isEmpty(localExp) ? (
                    <LoadingEvent />
                  ) : (
                    <Swiper {...params} style={{ width: "100%" }}>
                      {map(localExp.community_tours, (item, index) => (
                        <div
                          data-swiper-slide-index={item.slug}
                          key={item.slug}
                          className="swiper-slide-active"
                          style={{ width: "294.25px", marginRight: "30px" }}
                        >
                          <Link route="activity" params={{ slug: item.slug }}>
                            <a>
                              <div className="plan-item sr-btm">
                                <div className="box-img plan-img">
                                  <div className="thumb">
                                    <img
                                      src={urlImage(
                                        item.image_path +
                                          "/small/" +
                                          item.image_filename
                                      )}
                                      alt={item.name}
                                    />
                                    <h2
                                      style={styles.productName}
                                      className="plan-title"
                                    >
                                      {item.name}
                                    </h2>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </div>
                      ))}
                    </Swiper>
                  )}
                </div>
              </div>
              {/* container */}
            </div>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  loadingEvent: state.localExperiences.loading,
  localExp: state.localExperiences.data
});

export default connect(mapStateToProps)(LocalExperiences);
