import Loading from "../shared/loading-activity";
import { Link } from "../../routes";
import { urlImage } from "../../utils/helpers";
import { map } from "lodash";
import { truncateString } from '../../utils/helpers';

const _imgStyle = url => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: "cover",
    backgroundPosition: " center center"
  };
};

const removeTag = str => {
  if ((str === null) || (str === '')) {
    return false
  } else {
    str = str.toString();
    return str.replace(/<[^>]*>|&nbsp;/g, '')
  }
}

const ActivityCategory = ({
  data,
  isLoading,
  title,
  link,
  model,
  classProps,
  loopTo,
  moreData,
  moreBtn = false,
  moreLoading = false,
  total = 6
}) => {
  return (
    <div>
      <div className="row">
        <div className="col-md-12 cat-header">
          <div className="main-title sr-btm">
            <h1 className="text-title">{title}</h1>
          </div>
        </div>
      </div>
      {isLoading && (
        <div className="product-list">
          <Loading loopTo={loopTo} classProps={classProps} />
        </div>
      )}
      {!isLoading && (
        <div className="row">
          <div className="product-list">
            {map(data, (item, index) => (
              <div
                className={`col-md-${classProps[0]} col-sm-${
                  classProps[1]
                  } col-xs-${classProps[2]} ${
                  model === 1 ? "p-item-o" : "p-item"
                  }`}
                key={`p-${index}`}
              >
                <Link route="community-home" params={{ slug: item.slug }}>
                  <a>
                    <div className="plan-item sr-btm">
                      <div className="box-img plan-img">
                        <div
                          className="thumb"
                          style={_imgStyle(
                            urlImage(
                              item.logo.path + "/" + item.logo.filename
                            )
                          )}
                        />
                      </div>
                      <div className="plan-info">
                        <ul>
                          <li className="title">
                            <h3 style={{ fontSize: '24px' }}>
                              {item.company.company_name}
                            </h3>
                          </li>
                          <li className="title">
                            <h3 style={{ color: '#e17306' }}>
                              {item.category_community.name}
                            </h3>
                          </li>
                          <li>
                            <span style={{ fontSize: '12px', color: 'black', }}>{removeTag(item.description).substr(0, 60)}...</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                    {/*plan-item-end*/}
                  </a>
                </Link>
              </div>
            ))}
            {data.length > 1 &&
              data.length < total &&
              moreBtn &&
              moreLoading && (
                <div className="col-md-12">
                  <Loading loopTo={loopTo} classProps={classProps} />
                </div>
              )}
          </div>
          {data.length > 1 && data.length < total && moreBtn && !moreLoading && (
            <div className="col-md-6 col-md-offset-3">
              <button
                id={link}
                className="btn-o btn btn-primary btn-block"
                onClick={moreData}
              >
                Load More
                </button>
            </div>
          )}
        </div>
      )}
    </div>
  );
};
export default ActivityCategory;
