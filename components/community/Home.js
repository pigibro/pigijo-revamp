import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import withAuth from "../../_hoc/withAuth";
import UpcomingEvents from "./UpcomingEvents";
import UpcomingLocalExperiences from "./UpcomingLocalExperiences";
import LoadingEvent from "./loading-event";

class CommunityHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const styles = {
      subTitile: {
        margin: "1em"
      }
    };

    return (
      <section>
        <UpcomingEvents slug={this.props.slug} />

        <UpcomingLocalExperiences slug={this.props.slug} />

        {/* <div style={styles.subTitile} className="main-title sr-btm">
          <h1 className="text-title">Must Join</h1>
        </div>
        <LoadingEvent /> */}
      </section>
    );
  }
}
const mapStateToProps = state => ({});

export default connect(mapStateToProps)(CommunityHome);
