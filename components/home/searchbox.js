import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
// import DateRangePicker from 'react-bootstrap-daterangepi cker';
import Preloader from 'Preloader-bg'
import { createBrowserHistory } from 'history'

// import { getDetailPlan } from 'actions/home/planningDetail.action'
import { ModalToggle, ModalType } from 'actions/modal.action'
import {
  getPlanning,
  isRedirectPage,
  ResetCountRoute
} from 'actions/home/planning.action'
import { getAllCity, getNewCity } from 'actions/home/city.action'
// import { getHolidays } from 'actions/holiday/holiday.action'
import { hitungSelisih } from 'actions/selisih'
// import { pushPixelCustom } from  "../../../actions/vendor/FbPixel"

import SearchAutoComplete from '../../_component/SearchAutoComplete'
import { DateRangePicker } from 'react-dates'
import 'react-dates/lib/css/_datepicker.css'
import 'react-dates/initialize'
import { Row, Col } from 'react-bootstrap'
import Surprise from '../../_additional/surprise/surprise'
import { Collapse } from 'reactstrap'
import { Glyphicon } from 'react-bootstrap'
import { isMobile } from 'react-device-detect'
import moment from 'moment'

class Planning extends Component {
  constructor (props) {
    super(props)
    this.wrapperRef = {}
    this.state = {
      focusedInput: null,
      isOpen: false,
      cityValue: [],
      destValue: [],
      errorDest: [],
      startDate: null,
      endDate: null,
      person: 2,
      isActive: false,
      error: {
        starting: null,
        destination: null,
        dateerr: null
      },
      dateList: [],
      dataCheckBox:[]
    }
    this.setWrapperRef = this.setWrapperRef.bind(this)
  }

  setWrapperRef (node) { this.wrapperRef = node }

  handleChangePerson = e => { this.setState({ person: e.target.value }) }

  onChange = value => {this.setState({ cityValue: value })}

  onFocusChange = focusedInput => {this.setState({ focusedInput })}

  handleToggle = e => {e.preventDefault(); this.setState({ isOpen: !this.state.isOpen })}

  onDatesChange = ({ startDate, endDate }) => {
    if (endDate !== null) {
      this.setState({ isOpen: !this.state.isOpen })
    }
    this.setState({ startDate, endDate })
  }

  componentWillMount () {
    if (!isMobile) {
      document.addEventListener('mousedown', this.handleClickOutside)
    }
    this.props.dispatch(getNewCity())
    this.setState({dataCheckBox:this.props.dataCheckBox})
    // this.props.dispatch(getHolidays())
  }

  componentWillReceiveProps(props){
    this.setState({dataCheckBox:props.dataCheckBox})
  }

  handleClickOutside = event => {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ isActive: false })
    } else {
      this.setState({ isActive: true })
    }
  }

  componentWillUnmount () {
    if (!isMobile) {
      document.removeEventListener('mousedown', this.handleClickOutside)
    }
  }

  toggleSuprise = e => {
    const bool = !this.props.isOpen
    this.props.dispatch(ModalToggle(bool))
    this.props.dispatch(ModalType('surprise'))
  }

  loadOptions = (input, callback) => {
    this.props.dispatch(getAllCity())
    setTimeout(() => {
      callback(this.props.listCity)
    }, 1000)
  }

  handleCityNameChange = i => evt => {
    let val = evt[0]
    const newCity = this.state.destValue.map((city, id) => {
      if (i !== id) return city
      return { ...city, val }
    })
    this.setState({ destValue: newCity })
  }

  handleAddCity = e => {
    e.preventDefault()
    let error = {
      starting: null,
      destination: null,
      dateerr: null
    }
    if (
      this.state.cityValue.length === 0 ||
      this.state.destValue.includes(null)
    ) {
      error.destination = 'Please input your destination city'
    } else {
      this.setState({ destValue: this.state.destValue.concat([null]) })
    }
    this.setState({ error: error })
  }

  handleRemoveCity = i => () => {
    this.setState({
      destValue: this.state.destValue.filter((s, id) => i !== id),
      error: { starting: null, destination: null, dateerr: null }
    })
  }

  HandleClickStart = e => {
    e.preventDefault()
    const { destValue, cityValue, startDate, endDate } = this.state
    let dest = this.state.destValue
    let bool = true
    let error = {
      starting: null,
      destination: null,
      dateerr: null
    }
    if (cityValue.length < 1 || destValue.includes(null)) {
      error.destination = 'Please input your destination city'
      bool = false
    } else if (Math.floor(hitungSelisih(startDate, endDate)) >= 15) {
      error.dateerr = 'Max 15 days.'
      bool = false
    } else if (startDate === null || endDate === null) {
      error.dateerr = 'Please choose your trip date'
      bool = false
    } else {
      let newDest = []
      if (dest.length > 0) {
        dest.map(i => newDest.push(i.val.id + '-' + i.val.type))
        if (newDest.includes(cityValue[0].id + '-' + cityValue[0].type)) {
          error.destination =
            'Your starting city must not be same as departure city'
          // error.destination = 'Your destination city not required to be input, because same as your starting-city'
          bool = false
        }
      }
    }

    this.setState({ error: error })

    if (bool) {
      const history = createBrowserHistory()

      let destination = []
      let starting = {}

      cityValue.map((dest, i) => {
        if (dest.type === 'Kabupaten' || dest.type === 'Kota') {
          // starting = {'id':dest.id,'key':'city'};
          destination[0] = { id: dest.id, key: 'city' }
        } else if (dest.type === 'Provinsi') {
          // starting = {'id':dest.id,'key':'province'};
          destination[0] = { id: dest.id, key: 'province' }
        } else if (dest.type === 'Area') {
          // starting = {'id':dest.id,'key':'area'};
          destination[0] = { id: dest.id, key: 'area' }
        }
        return 'test'
      })
      dest.map((dest, i) => {
        if (dest.val.type === 'Kabupaten' || dest.val.type === 'Kota') {
          destination[i + 1] = { id: dest.val.id, key: 'city' }
        } else if (dest.val.type === 'Provinsi') {
          destination[i + 1] = { id: dest.val.id, key: 'province' }
        } else if (dest.val.type === 'Area') {
          destination[i + 1] = { id: dest.val.id, key: 'area' }
        }
        return 'test'
      })
      let someState = { starting, destination }

      this.props.dispatch(
        getPlanning(
          starting,
          destination,
          this.state.startDate,
          this.state.endDate,
          this.props.dataCheckBox,
          this.state.person,
          'create_itinerary',
          false
        )
      )
      if (this.props.isLoading) {
        history.push({
          pathname: '/planning-itinerary/' + this.props.planId,
          state: someState
        })
      }
    }
  }
  holidayInfo = day => {
    // if (day !== undefined) {
    //   this.props.holidayNameDate.map((data, index)=>{
    //     let as = moment(day).format('YYYY-MM-DD')
    //     return(<div key={index}>
    //       <span>{data.date} ~ {data.moment(day).format('YYYY-MM-DD')}</span>
    //     </div>)
    //   })
    // }
  }

  render () {
    const { dateerr } = this.state.error
    const history = createBrowserHistory()

    if (
      this.props.isRedirect &&
      this.props.countRoute === 1 &&
      this.props.option === 'create_itinerary'
    ) {
      this.props.dispatch(isRedirectPage(false))
      this.props.dispatch(ResetCountRoute())
      // this.props.dispatch(getDetailPlan(this.props.planId))
      // return (
      //   <Redirect to={'/planning-itinerary/' + this.props.planId + '/step2'} />
      // )
      history.push('/planning-itinerary/' + this.props.planId)
      window.location.reload()
    } else if (
      this.props.isRedirect &&
      this.props.countRoute === 1 &&
      this.props.option === 'find_activities'
    ) {
      this.props.dispatch(isRedirectPage(false))
      this.props.dispatch(ResetCountRoute())
      // this.props.dispatch(getDetailPlan(this.props.planId))
      localStorage.setItem('isActivity', true)

      if (this.props.destination.key === 'city') {
        return (
          <Redirect
            to={{
              pathname: `/planning-activity/${this.props.destination.name}/1`,
              state: {
                person: this.props.person,
                start: this.props.startDate,
                finish: this.props.finishDate,
                type: 'city',
                dayAt: 1,
                category: this.props.categories
              }
            }}
          />
        )
      } else {
        return (
          <Redirect
            to={{
              pathname: `/planning-activity/${this.props.destination.name}/1`,
              state: {
                person: this.props.person,
                start: this.props.startDate,
                finish: this.props.finishDate,
                type: 'province',
                dayAt: 1,
                category: this.props.categories
              }
            }}
          />
        )
      }
    } else if (this.props.isRedirect && this.props.countRoute >= 1) {
      this.props.dispatch(isRedirectPage(false))
      this.props.dispatch(ResetCountRoute())
      // this.props.dispatch(getDetailPlan(this.props.planId))
      // return <Redirect to={'/planning-itinerary/' + this.props.planId} />;
      history.push('/planning-itinerary/' + this.props.planId)
      window.location.reload()
    }
    // const fixBt = isMobile ? { position: 'fixed' } : { position: 'unset' }
    const btnCustom ={backgroundColor:'#1ba0e2',width: '60%',left: '2.5em',bottom: '1.6em'}
    return (
      <div className='container'>
        <div className={ !this.state.isActive ? 'row greyout' : 'row greyout show-greyout1'}>
          <div className='col-lg-5 col-lg-offset-7 col-md-5 col-md-offset-7'>
            <div ref={node => this.setWrapperRef(node)} className='panel reservation-form'>
              <div className={isMobile? 'main_form_search_panel': 'panel-body main_form_search_panel'}>
                {/* <a className="close-planning" href="" onClick={this.props.onClick}><i className="ti-close" /></a> */}

                {/* <p className="help-block" style={{ marginBottom: "15px" }}>
                  Start creating your own itinerary or you can skip the itinerary planning and find activities in your destination right away.
                </p> */}

                {isMobile ? (
                  <section className='content-wrap-modal flight-search-header-wrap ' style={{backgroundColor:'rgb(51, 49, 44)'}}>
                    <div className='container top-content'>
                      <div className='flex-row flex-center' style={{ marginLeft: '1em' }}>
                        <a href='' onClick={this.props.onClick}>
                          <Glyphicon className='back-title' glyph='menu-left' />
                        </a>
                        <span>Plan Your Trip</span>
                        <div className='flex-row flex-center mla' />
                      </div>
                    </div>
                  </section>
                ) : (
                  <h4 className='text-title' align='center' style={{ marginBottom: '15px', fontWeight: 650 }}>Plan Your Trip</h4>
                )}
                <br />
                <form className={ isMobile? 'panel-body new shadow-modal' : 'form-action main_searching_form'} onSubmit={this.HandleClickStart}>
                  {/* <span className='text-label'>Destinations</span> */}
                  <div className='form-group'>
                    <Row>
                      <Col xs={12}>
                        <SearchAutoComplete
                          key={'select-single-1'}
                          index={0}
                          data={this.props.list}
                          placeholder='Enter Destination or City or Province'
                          labelKey='name'
                          onChange={this.onChange}
                        />
                      </Col>
                    </Row>
                    {this.state.destValue.map((item, idx) => (
                      <Row key={idx}>
                        <Col xs={10} className='select-front'>
                          <SearchAutoComplete
                            key={'select-single-1'}
                            index={0}
                            data={this.props.list}
                            placeholder='Enter Destination or City or Province'
                            labelKey='name'
                            onChange={this.handleCityNameChange(idx)}
                          />
                        </Col>
                        <Col xs={2}>
                          <abbr title='Remove destinations'>
                            <button type='button' className='btn btn-danger custom-btn-plus' onClick={this.handleRemoveCity(idx)} >
                              <Glyphicon glyph='trash' />
                            </button>
                          </abbr>
                        </Col>
                      </Row>
                    ))}
                    <div className='text-danger error-text'>
                      <i>{this.state.error.destination}</i>
                    </div>
                    {this.state.destValue.length >= 6 ? (
                      <div />
                    ) : (
                      <a href='' className='add-more mla' onClick={this.handleAddCity}>
                        <Glyphicon glyph='plus' /> add more destination
                      </a>
                    )}
                  </div>
                  <div className='form-group'>
                    <Row>
                      <Col xs={12}>
                          <div className='column-n'>
                            <div className='cell'>
                              <DateRangePicker
                                startDatePlaceholderText='Start'
                                endDatePlaceholderText='End'
                                startDate={this.state.startDate}
                                startDateId='startDateId'
                                endDate={this.state.endDate}
                                endDateId='endDateId'
                                onDatesChange={this.onDatesChange}
                                focusedInput={this.state.focusedInput}
                                onFocusChange={this.onFocusChange}
                                displayFormat='DD MMM'
                                hideKeyboardShortcutsPanel
                                numberOfMonths={1}
                                withPortal={isMobile}
                                minimumNights={0}
                                // renderCalendarInfo={this.holidayInfo}
                                // renderDayContents={(day) => {day.day() % 6 === 5 ? day.format('D') : day.format('D');this.holidayInfo(day)}}

                                renderCalendarInfo={this.holidayInfo}
                                isDayHighlighted={day1 => { 
                                  this.holidayInfo(day1)
                                  return this.props.holidayDate.some(day2 =>
                                    day1.isSame(moment(day2))
                                  )
                                }}
                              />
                              <div className='text-danger'>
                                <i>{dateerr}</i>
                              </div>
                            </div>
                            <div className='cell2'>
                              <div className='select-person clearfix'>
                                <select
                                  defaultValue={this.state.person}
                                  onChange={this.handleChangePerson}
                                  className={isMobile? 'form-control line-brd': 'form-control'}
                                >
                                  <option value='1'>1 Person</option>
                                  <option value='2'>2 Persons</option>
                                  <option value='3'>3 Persons</option>
                                  <option value='4'>4 Persons</option>
                                  <option value='5'>5 Persons</option>
                                  <option value='6'>6 Persons</option>
                                  <option value='7'>7 Persons</option>
                                </select>
                              </div>
                            </div>
                          </div>
                      </Col>
                      <Col md={12} xs={12} style={{ margin: '1em 0' }}>
                        <span className='text-title' style={{ color: 'black' }}>Interest</span>
                        <div className='form-group'>
                          {/* <div className="activity-toggle" onClick={this.toggleSuprise} data-toggle="collapse" data-target="#surprise"> */}
                          <a
                            href=''
                            className='activity-toggle'
                            onClick={this.handleToggle}
                            style={{textDecoration: 'underline',color: '#000'}}
                          >
                            {this.state.dataCheckBox.length === 0
                              ? 'Any kind. Surprise me!'
                              : this.state.dataCheckBox.length <= 3 ?
                                 this.state.dataCheckBox.map((item, id) => (
                                  <span key={id}>{this.state.dataCheckBox.length>1?id===0?"":", ":''}{item}</span>
                                )): this.state.dataCheckBox[0] +
                                ', ' +
                                this.state.dataCheckBox[1] +
                                ', ' +
                                this.state.dataCheckBox[2] +
                                '... +' +
                                (this.state.dataCheckBox.length - 3) +
                                ' others'}{' '}
                            &nbsp;&nbsp;
                            {!this.state.isOpen ? (
                              <Glyphicon glyph='menu-down' />
                            ) : (
                              <Glyphicon glyph='menu-up' />
                            )}
                            {/* <input className='form-control activity-result' value={this.props.dataCheckBox.length === 0 ? 'Any kind. Surprise me!': this.props.dataCheckBox.length <= 3? this.props.dataCheckBox: this.props.dataCheckBox[0] +', ' +this.props.dataCheckBox[1] +', ' +this.props.dataCheckBox[2] +'... +' +(this.props.dataCheckBox.length - 3) +' others'} readOnly /> */}
                          </a>
                        </div>
                        <Collapse isOpen={this.state.isOpen}>
                          <Surprise />
                        </Collapse>
                        {isMobile ? (
                          <div>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                          </div>
                        ) : (
                          ''
                        )}
                      </Col>
                      <br />
                    </Row>
                  </div>
                  {/* <div className="col-xs-12">
                          <div className="form-group">
                            <div className="checkbox">
                              <input type="checkbox" id="s18" name="back" onChange={this.handleReturnChange} checked={this.props.isReturnCity}/>
                              <label htmlFor="s18">I need transportation back to starting city.</label>
                            </div>
                          </div>
                        </div> */}
                  <div className='col-xs-12'>
                    <button className='btn btn-block btn-primary btn-submit btn-fix' style={isMobile?btnCustom:{}} type='submit'>Start Planning</button>
                    {/* <button
                      className='btn btn-block btn-primary btn-submit btn-fix'
                      type='submit'
                      style={fixBt}
                    >
                      Start Planning 
                    </button>*/}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {this.props.isLoading ? <Preloader /> : ''}
      </div>
    )
  }
}

const mapStateToProps = state => {
  const {
    isReturnCity,
    planId,
    isRedirect,
    isRoute,
    countRoute,
    option,
    startDate,
    finishDate,
    person,
    categories,
    isLoading
  } = state.planningReducer
  return {
    isOpen: state.modalReducer,
    type: state.modalTypeReducer,
    dataCheckBox: state.surpriseReducer,
    listCity: state.cityReducer.all,
    list: state.cityReducer.list,
    isReturnCity,
    planId,
    isRedirect,
    isRoute,
    countRoute,
    option,
    startDate,
    finishDate,
    person,
    categories,
    isLoading,
    destination: state.planningReducer.firstdestination,
    holidays: state.holidaysReducer.holidays,
    holidayName: state.holidaysReducer.holidayName,
    holidayDate: state.holidaysReducer.holidayDate,
    holidayNameDate: state.holidaysReducer.holidayNameDate
  }
}
export default connect(mapStateToProps)(Planning)
