import React, { PureComponent } from 'react';
import Swiper from 'react-id-swiper/lib';
// import Planning from './planning';
import LazyLoad from 'react-lazy-load';
import ImageLoader from '../../utils/ImageLoader';
// import Search from './search';
import AutoComplete from './autocomplete-search';
import { isMobileOnly } from "react-device-detect";
// import Planning from './planning';
import { Link, Router } from '../../routes'

export default class Banner extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false
    }
    this.togglePlan = this.togglePlan.bind(this)
  }

  togglePlan(e) {
    e.preventDefault()
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    const { dispatch } = this.props;
    const _divStyle = (slide) => {
      return {
        backgroundImage: `url(${slide})`,
        backgroundSize: `cover`,
        backgroundPosition: 'center center',
        width: '100%',
        // height:'100%'
      }
    }
    
    const params = {
      autoplay: {
        delay: 3000,
        disableOnInteraction: true
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    };

    return (
      <div className={this.state.isOpen ? "active-planning" : ""} style={{ 'marginTop': '20px' }}>
        <section className="banner-area" style={{height: 'auto'}}>
          <section>
            {/* <a href='https://iniindonesiaku.com'>
              <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/08/vid-comp-24-aug.jpg" style={{width: '100%'}}/>
            </a> */}
            
            <Swiper  {...params} style={{width: '120em'}}>

              <div>
                <div className="thumb">
                  {/* <div style={_divStyle('/static/images/img01.jpg')}/>
                  <LazyLoad>
                    <ImageLoader  alt="slider" />
                  </LazyLoad> */}
                  <Link to="/p/2164-pigijo-wellness-program-at-bali-cultivating-5-senses">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/11/1-banner-wellness-program.jpg" alt="slider"/>
                  </Link>
                </div>
                <section className="banner-caption">
                
                </section>
              </div>
              
              <div>
                <div className="thumb">
                  {/* <div style={_divStyle('/static/images/img01.jpg')}/>
                  <LazyLoad>
                    <ImageLoader  alt="slider" />
                  </LazyLoad> */}
                  <Link to="/p/2071-mt-prau-explore-dieng-plateu-trip">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/10/1-banner-prau.jpg" alt="slider"/>
                  </Link>
                </div>
                <section className="banner-caption">
                
                </section>
              </div>
              
              <div> 
                <div className="thumb">
                  <Link to="/p/2072-diving-in-nusa-penida-certified-divers">
                  <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/10/2-banner-certified-dive-nusa-penida.jpg" alt="slider"/>
                  </Link>
                </div>
                <section className="banner-caption"> 

                </section>
              </div>

              <div> 
                <div className="thumb">
                  <Link to="/p/2073-nusa-penida-try-to-dive-introductory-dive">
                  <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/10/3-banner-dive-nusa-penida.jpg" alt="slider"/>
                  </Link>
                </div>
                <section className="banner-caption"> 

                </section>
              </div>

              <div> 
                <div className="thumb">
                  <Link to="/p/1907-belitung-exotics-3d2n-include-flight-hotel">
                  <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/10/4-banner-belitung.jpg" alt="slider"/>
                  </Link>
                </div>
                <section className="banner-caption"> 

                </section>
              </div>

              <div> 
                <div className="thumb">
                  <Link to="/p/1861-newnormal-lombok-3d2n-montana-premier-3">
                  <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/10/5-banner-lombok.jpg" alt="slider"/>
                  </Link>
                </div>
                <section className="banner-caption"> 

                </section>
              </div>

              <div> 
                <div className="thumb">
                  <Link to="/p/1585-luxury-hiking-mt-papandayan-2d1n">
                  <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/10/6-banner-papandayan.jpg" alt="slider"/>
                  </Link>
                </div>
                <section className="banner-caption"> 

                </section>
              </div>

              <div> 
                <div className="thumb">
                  <Link to="/p/1885-open-trip-pulau-harapan-kepulauan-seribu-2d1n">
                  <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/banner/2020/10/7-banner-pulau-harapan.jpg" alt="slider"/>
                  </Link>
                </div>
                <section className="banner-caption"> 

                </section>
              </div>

            </Swiper>
            {/* <div className="mouse" /> */}
          </section>
          {/* banner */}

          {/* <section className="planning-area">
            <div className="container text-center">
              <div className="caption" style={{ paddingTop: '10%' }}>
                <h1 className="text-title">Your Perfect Next Adventure</h1>
                <h1 className="text-title">#SupportLocalExpert</h1>
                <p>Discover 3.000+ Local Experiences, 4.500+ Places, 1.200+ Car Rentals,  and 400+ Homestays in Indonesia</p>
                <p>Beyond experience with Great Insight</p>
              </div>
              <AutoComplete dispatch={dispatch} />
            </div>
          </section>planning-area */}
        </section>
        <style jsx>{`
            @media (max-width: 575px) {
              .text-title{
                font-size: 35px;
              }
            }
      
            `}
        </style>
      </div>
    )
  }
};
