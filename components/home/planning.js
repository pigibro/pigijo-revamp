import React, { Component } from 'react';
import { Row, Col, Glyphicon } from 'react-bootstrap';
import { isEmpty } from 'lodash';
import { connect } from "react-redux";
import SearchAutoComplete from '../shared/searchautocomplete';
import { getLocations } from '../../stores/actions';
import { removeEmpty, countDate } from '../../utils/helpers';
// import Router from 'next/router'
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import { withRouter } from 'next/router';

class Planning extends Component {
  constructor (props) {
    super(props)
    this.wrapperRef = {}
    this.state = {
      startDate: null,
      endDate:null,
      destination: [],
      isOpen:false,
      person: 2,
      error: {
        starting: null,
        destination: null,
        dateerr: null
      },
      focusedInput: null,
    }
  }

  async componentDidMount () {
    const { dispatch } = this.props;
    await dispatch(getLocations({type:'tours'}));
  }

  handleRemove = (e, i) => {
    e.preventDefault();
    const {destination} = this.state;
    this.setState({
      destination: destination.filter((s, id) => i !== id),
      error: { starting: null, destination: null, dateerr: null }
    })
  }
 
  handleSubmit = e => {
    e.preventDefault();
    const {
      startDate,
      endDate,
      destination,
      person,
    } = this.state;
    let error = this.state.error;
    if(destination.includes(null) || destination.length === 0){
      error.destination = 'Please input your destination city';
    }
    if(startDate === null || endDate === null){
      error.dateerr = 'Please select your trip date';
    }
    if(Math.floor(countDate(startDate, endDate)) >= 15){
      error.dateerr = 'Max 15 days';
    }
    if(error.destination === null && error.dateerr === null && error.starting === null){
      const {router} = this.props;
      router.push('/planning-itinerary/MTExNTcyLDIwMTktMDUtMDY=');
    }
    this.setState({ error })
  }

  handleAddCity = e => {
    e.preventDefault()
   
    const { destination, error } = this.state;
    let newError = error;
    if (
      destination.includes(null)
    ) {
      if(newError.destination === null){
        newError.destination = 'Please input your destination city';
      }
    } else {
      this.setState({ destination: destination.concat([null]) })
    }
    this.setState({ error: newError });
  }

  onFocusChange = focusedInput => {this.setState({ focusedInput })}

  handleDestination = i => evt => {
    let val = evt[0];
    const { destination, error } = this.state;
    let newCity = destination;
    let newError = {
      starting: error.starting,
      destination: null,
      dateerr: error.dateerr
    }
    if(destination.length < 1){
      if(!isEmpty(val)){
        newCity = [val];
      }else{
        newCity = [];
      }
    }else{
      const isExist = newCity.map(obj => obj === val);
      if(isExist.includes(true)){
        newError.destination = 'Destination can`t be same.';
      }else{
        newCity[i] = val;
      }
    }
    this.setState({ destination: newCity})
    this.setState({ error: newError })
  }

  render() {
    const {destination, error, startDate, endDate, focusedInput, person} = this.state;
    const {isLoading, locations} = this.props;
    return(
      <div className='col-lg-5 col-lg-offset-7 col-md-5 col-md-offset-7'>
        <div className='panel reservation-form'>
          <div className="panel-body panel-search-box">
            <h1 className='text-title' align='center'>Plan Your Trip</h1>
            <form className="form-action main_searching_form" onSubmit={this.handleSubmit}>
              <div className='form-group'>
                <Row>
                  <Col xs={12}>
                    <SearchAutoComplete
                      id="destination"
                      key={"select-single"}
                      index={0}
                      data={locations}
                      placeholder="Enter Destination or City or Province"
                      labelKey="name"
                      onChange={this.handleDestination(0)}
                    />
                    {destination.map((item, idx) => {
                      if(idx > 0)
                      return (
                        <div className="more-destination">
                          <div>
                            <SearchAutoComplete
                              id={`destination-${idx}`}
                              key={`select-single-${idx}`}
                              index={idx}
                              data={locations}
                              placeholder="Enter Destination or City or Province"
                              labelKey="name"
                              onChange={this.handleDestination(idx)}
                            />
                            
                          </div>
                          <a className="remove-destination" onClick={e => this.handleRemove(e, idx)}>
                            <span className="ti-close"></span>
                          </a>
                        </div>
                      )
                    })}
                    { error.destination && 
                      <div className='text-danger error-text'>
                        {error.destination}
                      </div>
                    }
                    { destination.length <= 6 &&
                      <a href='' className='add-more mla' onClick={this.handleAddCity}>
                        <Glyphicon glyph='plus' /> add more destination
                      </a>
                    }
                  </Col>
                  <Col xs={12} className="mt1">
                    <div className="form-planning-group">
                      <DateRangePicker
                        startDatePlaceholderText='Start'
                        endDatePlaceholderText='End'
                        startDate={startDate}
                        startDateId='startDateId'
                        endDate={endDate}
                        endDateId='endDateId'
                        onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })}
                        focusedInput={focusedInput}
                        onFocusChange={focusedInput => this.setState({ focusedInput })}
                        displayFormat='DD MMM YYYY'
                        hideKeyboardShortcutsPanel
                        numberOfMonths={1}
                        withPortal={false}
                        minimumNights={0}
                      />
                      <select
                        defaultValue={person}
                        onChange={e => this.setState({ person: e.target.value })}
                        className='form-control mb1'
                      >
                        <option value='1'>1 Person</option>
                        <option value='2'>2 Persons</option>
                        <option value='3'>3 Persons</option>
                        <option value='4'>4 Persons</option>
                        <option value='5'>5 Persons</option>
                        <option value='6'>6 Persons</option>
                        <option value='7'>7 Persons</option>
                      </select>
                    </div>
                      { error.dateerr && 
                        <div className='text-danger error-text'>
                          {error.dateerr}
                        </div>
                      }
                  </Col>
                  <Col xs={12}>
                    <div className='form-group'>
                      <div className='checkbox'>
                        <input type='checkbox' id='s0112' 
                          name='suprise' defaultValue='Any kind of activities'/>
                        <label htmlFor='s0112'>Any kind of activities. Surprise me!</label>
                      </div>
                    </div>
                  </Col>
                  <Col xs={12}>
                   <button className='btn btn-block btn-primary btn-submit btn-fix btn-blue' type='submit'>Start Planning</button>
                  </Col>
                </Row>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.city.loading,
  locations: state.city.data,
})

Planning.defaultProps = {
  locations: []
}

export default withRouter(connect(mapStateToProps)(Planning));