import React, { Component } from 'react';
import { airplane, hotel, vehicle, house, beach, restaurant, mountain } from '../../utils/icons';

export default class NextGetaway extends Component {
  render() {
    const icon =[
        {slug:'/c/flight', name:'Flight', img:airplane},
        {slug:'/c/hotel', name:'Hotel', img:hotel},
        {slug:'/rent-car', name:'Vehicle', img:vehicle},
        {slug:'/c/tour', name:'Tour', img:beach},
        {slug:'/accommodation', name:'Homestay', img:house},
        {slug:'/c/restaurant', name:'Restaurant', img:restaurant},
        {slug:'/c/destination', name:'Destination', img:mountain}
    ]
    return (
      <section className="content-wrap bg-content-home">
        <div className="container">
          <div className="row flex-row flex-center">
            <div className="col-lg-12 col-md-12">
              <div className="main-title">
                  <h1 className="text-title">Your Next Getaway</h1>
              </div>
            </div>
            <div className="col-md-8 col-md-offset-2">
              <div className="content--category">
                {
                  icon.map((item,key)=>
                    (
                      <a href={item.slug} className="icon--category" key={key}>
                        <div className="how-to-icon">
                          <img src={item.img} alt={item.name}/>
                        </div>
                        <h1 className="text-title">{item.name}</h1>
                      </a>                 
                    )
                  ) 
                }
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}