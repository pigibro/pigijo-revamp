import Loading from '../shared/loading-activity';
import { Link } from '../../routes';
import { slugify, urlImage } from '../../utils/helpers';
import { map, get, isEmpty, takeRight } from 'lodash';
import numeral from '../../utils/numeral';
import moment from 'moment'; 
import Countdown from 'react-countdown-now';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
  }
}

// const countdownBox = {
//     padding: window.innerWidth < 480 ? '1.5em' : '2em',
//     backgroundColor: '#e17306', 
//     color: '#fff', 
//     borderRadius: '2px', 
//     display: 'flex', 
//     flexDirection: 'column', 
//     textAlign: 'center', 
//     marginRight: '1.5em'
// }

const addLeadingZeros = (value) => {
    value = String(value);
    while (value.length < 2) {
      value = '0' + value;
    }
    return value;
  }

const Completionist = () => <span style={{color: '#e17306', fontSize: '2.5em'}}>Happy Shopping!</span>;
 
// Renderer callback with condition
const renderer = ({ days, hours, minutes, seconds, completed }) => {
  if (completed) {
    // Render a completed state
    return <Completionist />;
  } else {
    // Render a countdown
    return (
        <div style={{
            display: 'flex',
            flexWrap: 'wrap'
        }}>
            <div className='countdownBox'>
                <span style={{ fontSize: '2.5em', marginBottom: '0.4em'}}>{addLeadingZeros(days)}</span>
                <span>Days</span>
            </div>
            <div className='countdownBox'>
                <span style={{ fontSize: '2.5em', marginBottom: '0.4em'}}>{addLeadingZeros(hours)}</span>
                <span>Hours</span>
            </div>
            <div className='countdownBox'>
                <span style={{ fontSize: '2.5em', marginBottom: '0.4em'}}>{addLeadingZeros(minutes)}</span>
                <span>Minutes</span>
            </div>
            <div className='countdownBox'>
                <span style={{ fontSize: '2.5em', marginBottom: '0.4em'}}>{addLeadingZeros(seconds)}</span>
                <span>Seconds</span>
            </div>
            {/* <span>{days}:{hours}:{minutes}:{seconds}</span> */}
        </div>
        
    );
  }
};

const Xtremesale =  ({isMobile, data, isLoading, title, link, model, classProps, loopTo , moreData, moreBtn = false, moreLoading = false, total=6}) => {
    return (
        <section className="content-wrap bg-content-home">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 cat-header" style={{paddingBottom: '2.5em'}}>
                        {/* <img src={`../../static/images/${ isMobile ? 'banner_mobile_xtreamsale.jpg' : 'banner_website_xtreamsale.png'}`} width="100%"/> */}
                    </div>
                </div>
                {isLoading &&
                    <div className="product-list">
                        <Loading loopTo={loopTo} classProps={classProps}/>
                    </div>
                }
                {(!isLoading && !isEmpty(data)) &&
                    <div className="row">
                        <div className="product-list">
                            {
                                map(data, (item, index) => (
                                    <div className={`col-md-${classProps[0]} col-sm-${classProps[1]} col-xs-${classProps[2]} ${model === 1? 'p-item-o' : 'p-item'}`} key={`p-${index}`}>
                                        <Link route="activity" params={{slug: item.slug}}>
                                            <a>
                                                <div className="plan-item sr-btm">
                                                    <div className="box-img plan-img" style={ item.discount > 0 ? {} : {opacity: 0.3}}>
                                                    <div className="thumb" style={_imgStyle(urlImage(item.image_path+'/'+item.image_filename))}/>
                                                    <div className="box-img-top" style={{display: 'flex', justifyContent: 'space-between'}}>
                                                        {!isEmpty(item.city) && <span><i className="ti-pin"/> {item.city.name}</span> }
                                                        <img src="../../static/icons/XTREME_SALE-01.png" style={{width: '75px'}}/>
                                                    </div>
                                                    <div className="box-img-bottom" style={{position: 'absolute', bottom: 0, top: 'unset', zIndex: 10}}>
                                                        {item.discount > 0 && <p className="p_discount">{Math.round(item.discount)}% OFF</p>}
                                                    </div>
                                                    </div>
                                                    <div className="plan-info">
                                                    <ul>
                                                        <li className="title"><h3 title={item.name}>{item.name.substring(0,35)}...</h3></li>
                                                        <li className="min-person">
                                                        <p className="text-label">Min {item.min_person} person{item.min_person === 1? '' : 's'}</p>
                                                        </li>
                                                        <li className="price">
                                                        {item.discount > 0 && <p className="price-before"><del>Rp {numeral(parseInt(item.price_before,10)).format('0,0')}</del></p>}
                                                        <p>
                                                            <span  className="price-latest">
                                                            <b className="text-primary">Rp {numeral(parseInt(item.price,10)).format('0,0')}</b><span className="text-person">/person</span>
                                                            </span>
                                                            <span className={ item.upcomming_schedule === moment().format('YYYY-MM-DD') || item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') ? 'notice text-success' : 'notice text-info'}>
                                                            { item.upcomming_schedule === moment().format('YYYY-MM-DD') &&
                                                                'Available Today'
                                                            }
                                                            { item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') &&
                                                                'Available Tommorow'
                                                            }
                                                            { moment().add(1,'days').valueOf() < moment(item.upcomming_schedule).valueOf() &&
                                                                `Available ${moment(item.upcomming_schedule).format("DD MMMM YYYY")}`
                                                            }
                                                            </span>
                                                        </p>
                                                        </li>
                                                    </ul>
                                                    </div>
                                                </div>
                                            </a>
                                        </Link>
                                    </div>
                                ))
                                
                            }
                            
                        </div>
                    </div>
                }
                {
                    (!isLoading && isEmpty(data)) &&
                    <div className="row">
                        <div className="col-12" style={{display: 'flex', alignItems: 'center', flexDirection: 'column'}}>
                        <h1 className="text-title" style={{color: '#e17306', marginBottom: '0'}}>
                            Xtreme Sale
                        </h1>
                           <Countdown
                                date={new Date("October 09, 2019 09:00:00")}
                                renderer={renderer}
                            /> 
                        </div>
                        
                    </div>
                }
            </div>
            
            
        </section>
    )
}

export default Xtremesale