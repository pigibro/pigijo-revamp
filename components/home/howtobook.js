import React, { Component } from 'react';
import { Tab,Tabs } from 'react-bootstrap';
import { ResponsiveEmbed } from 'react-bootstrap';
import { vacation, pictures, diary, list } from '../../utils/icons';

export default class HowToBook extends Component {
  render() {
    return (
      <section className="content-wrap bg-content-home">
        <div className="container">
          <div className="row flex-row flex-center">
            <div className="col-lg-12 col-md-12">
              <div className="main-title">
                  <h1 className="text-title">How To Book</h1>
              </div>
            </div>
            <div className="col-md-12">
              <Tabs defaultActiveKey={1} id="tab-event">
                <Tab eventKey={1} title="Plan Your Trip">

                    <div className="row how-to-book">

                        <div className="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6">
                        <div className='video-wrap'>
                            <ResponsiveEmbed a16by9>
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/pyD8tsI9Fq4?rel=0&controls=1;autoplay=0" title="tour" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            </ResponsiveEmbed>
                        </div>
                        </div>
                        <div className="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6">
                            <h2>Create your own day by day desired trip</h2>
                            <div className="panel">
                                <img alt="Start Planning" src={list} title="Start Planning" />
                                <div className="panel-body">
                                    <h3>1. Start Planning</h3>
                                    <p>
                                        <b>Destination City:</b> (try typing) Bali or Yogyakarta<br/>
                                        <b>Start Trip:</b> (click) ‚pick your departure date‘<br/>
                                        <b> End Trip:</b> (click) ‚pick your return date‘<br/>
                                        <b>Number of Person:</b> (click & choose)
                                    </p>
                                </div>
                            </div>{/* panel */}
                            <div className="panel">
                                <img alt="Confirm Your Trip Route and Date" src={vacation} title="Confirm Your Trip Route and Date" />
                                <div className="panel-body">
                                    <h3>2. Confirm Your Trip Route and Date</h3>
                                    <p>You will see the destination cities and range date / duration of your stay.<br/><b>Click on the dates if you want to change your length of stay</b></p>
                                </div>
                            </div>{/* panel */}
                            <div className="panel">
                                <img alt="Find Activities" src={pictures} title="Find Activities" />
                                <div className="panel-body">
                                    <h3>3. Plan Your Day-to-Day Activities</h3>
                                    You will see 5 options on day by day planner:
                                    <ol>
                                        <li><b>Find Activities:</b> all tours in the area</li>
                                        <li><b>Other things to do:</b> all places of interest in the area</li>
                                        <li><b>Find Flight:</b> flight ticket from your city to your first destination</li>
                                        <li><b>Find Hotel:</b> find your place to stay in the city</li>
                                        <li><b>Car Rental:</b> Rent a car for the day / whole journey</li>
                                    </ol>
                                </div>
                            </div>{/* panel */}
                            <div className="panel">
                                <img alt="Book Your Trip" src={diary} title="Book Your Trip" />
                                <div className="panel-body">
                                    <h3>4. Book Your Trip</h3>
                                    <p>Click: Finish Planning<br/>Check your Itinerary AGAIN!<br/>Click: Continue to Payment</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </Tab>
                <Tab eventKey={2} title="Find Activities">
                    <div className="row how-to-book">
                        <div className="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6">
                            <ResponsiveEmbed a16by9>
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/MgiKM_cC23U?rel=0&amp;controls=1;autoplay=0" title="start" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            </ResponsiveEmbed>
                        </div>
                        <div className="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6">
                          <div className="panel">
                              {/* <img alt="" src={list} /> */}
                              <div className="panel-body">
                                  {/* <h3>1. Start Planning</h3> */}
                                  Looking for special trip packages? there are only 2 of you? 4 of you? planning a sweet escape to Labuan Bajo? Bromo? Belitung? We got you covered! Check out our one of a kind trip packages to your desired destination. Just click on the 'Find Tour Packages' Tab, take what you need and be on your way!
                              </div>
                          </div>
                          <div className="panel">
                              <img alt="Start Planning" src={list} title="Start Planning" />
                              <div className="panel-body">
                                  <h3>1. Start Planning</h3>
                                  <p>Start planning by setting up the destination city that you want to visit, your trip date, your group size, and you might want to set your interest specifically. You can add multiple destination into your plan.</p>
                              </div>
                          </div>{/* panel */}
                          <div className="panel">
                              <img alt="Confirm Your Trip Route and Date" src={vacation} title="Confirm Your Trip Route and Date" />
                              <div className="panel-body">
                                  <h3>2. Confirm Your Trip Route and Date</h3>
                                  <p>Check your trip route and adjust the date on each destination. Confirm it when you are ready.</p>
                              </div>
                          </div>
                        </div>

                    </div>
                </Tab>
                <Tab eventKey={3} title="Find Event" >
                <div className="row how-to-book">
                    <div className="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6">
                      <ResponsiveEmbed a16by9>
                          <iframe width="560" height="315" src="https://www.youtube.com/embed/50CxvQBhZdU?rel=0&amp;controls=1;autoplay=0" title="event" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                      </ResponsiveEmbed>
                    </div>
                    <div className="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6">
                        <div className="panel">
                            {/* <img alt="" src={list} /> */}
                            <div className="panel-body">
                                {/* <h3>1. Start Planning</h3> */}
                                <p>Got no ideas for your weekend or place to take your girl/boyfriend? or simply looking for any exhibition in town? Fret not! Check out our list of events.. search and find your go-to event(s), we have it all.. not to worry, we will rock you!</p>
                            </div>
                        </div>
                        <div className="panel">
                            <img alt="Pick Your Event" src={list} title="Pick Your Event" />
                            <div className="panel-body">
                                <h3>1.Pick Your Event</h3>
                                <p>Choose your interest Event in home page and see the interestes to do along the event and details things to do.</p>
                            </div>
                        </div>{/* panel */}
                        <div className="panel">
                            <img alt="Booking Your Event" src={vacation} title="Booking Your Event" />
                            <div className="panel-body">
                                <h3>2. Booking Your Event</h3>
                                <p>When you finish with choose event and you have item that needs prior booking listed in your itinerary. All booking will be proceeded in a single-checkout process. One bill and one transaction for all your bookings.</p>
                            </div>
                        </div>
                    </div>

                </div>
                </Tab>
              </Tabs>
            </div>
          </div>
        </div>
      </section>
    );
  }
}