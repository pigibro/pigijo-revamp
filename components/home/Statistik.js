import React, { Component } from 'react'
import Swiper from 'react-id-swiper';

export default class Statistik extends Component {
    render() {
        const params = {
            
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
            spaceBetween: 30
          }

        return (
            <React.Fragment>
                <section className="content-wrap bg-content-home">
                    <div className="container  hidden-xs">
                            <div className='row statistikWrapper' style={{'marginBottom': '60px', 'marginTop': '40px', 'display': 'block', 'padding': '0 1em'}}>
                                <div className='col-sm-12'>
                                    <h4 className='text-title' style={{'fontWeight': 'bold', 'marginBottom': '0'}}>Explore Indonesia with Pigijo</h4>
                                    <p style={{'marginBottom': '20px'}}>If you are seeking to get an unforgettable experience, Indonesia is the perfect country for you. We are here to show you that paradise exists on earth, and it is here in Indonesia.</p>
                                </div>
                                <div className='col-sm-3'>
                                    <a className='thumbnail-statistik'>
                                        <div className='pull-left image-thumbnail'>
                                        <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/thumbnails/tour.png' style={{'width': '100%'}}/>
                                        </div>
                                        <div className='pull-left text-statistik'>
                                            <span className='statistikTitle'>
                                                3.123 Tour Package
                                            </span>
                                        <div className='clearfix'></div>
                                        <span className='text-light-dark'>Discover thousands of local experiences in Indonesia</span>
                                        </div>
                                    </a>
                                </div>
                                <div className='col-sm-3'>
                                    <a className='thumbnail-statistik'>
                                        <div className='pull-left image-thumbnail'>
                                        <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/thumbnails/ta.png' style={{'width': '100%'}}/>
                                        </div>
                                        <div className='pull-left text-statistik'>
                                            <span className='statistikTitle'>
                                                66 Travel Assistants
                                            </span>
                                        <div className='clearfix'></div>
                                        <span className='text-light-dark'>We provide you the best local expert </span>
                                        </div>
                                    </a>
                                </div>
                                <div className='col-sm-3'>
                                    <a className='thumbnail-statistik'>
                                        <div className='pull-left image-thumbnail'>
                                        <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/thumbnails/places.png' style={{'width': '100%'}}/>
                                        </div>
                                        <div className='pull-left text-statistik'>
                                            <span className='statistikTitle'>
                                                4.820 Places
                                            </span>
                                        <div className='clearfix'></div>
                                        <span className='text-light-dark'>Discover thousands of places in Indonesia</span>
                                        </div>
                                    </a>
                                
                                </div>
                                <div className='col-sm-3'>
                                    <a className='thumbnail-statistik'>
                                        <div className='pull-left image-thumbnail'>
                                        <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/thumbnails/homestay.png' style={{'width': '100%'}}/>
                                        </div>
                                        <div className='pull-left text-statistik'>
                                            <span className='statistikTitle'>
                                                407 Homestays
                                            </span>
                                        <div className='clearfix'></div>
                                        <span className='text-light-dark'>Discover hundreds of local homestay in Indonesia</span>
                                        </div>
                                    </a>  
                                </div>            
                            </div>
                    </div>

                    <div className='visible-xs'>
                        <Swiper {...params}>
                            <div className='container'>
                                <a href='#'>
                                    <div className='pull-left' style={{'width': '35%'}}>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/thumbnails/tour.png' width='110'/>
                                    </div>
                                    <div className='pull-left' style={{'width': '65%'}}>
                                    <span style={{'fontWeight': 'bold', 'color': '#333', 'fontSize': '16px' }}>
                                        3.123 Tour Package
                                    </span>
                                    <div className='clearfix'></div>
                                    <span className='text-light-dark'>Discover thousands of local experiences in Indonesia</span>
                                    </div>
                                </a>
                            </div>

                            <div className='container'>
                                <a href='#'>
                                    <div className='pull-left' style={{'width': '35%'}}>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/thumbnails/ta.png' width='110'/>
                                    </div>
                                    <div className='pull-left' style={{'width': '65%'}}>
                                    <span className='statistikTitle'>
                                        66 Travel Assistants
                                    </span>
                                    <div className='clearfix'></div>
                                    <span className='text-light-dark'>We provide you the best local expert </span>
                                    </div>
                                </a>
                            </div>

                            <div className='container'>
                                <a href='#'>
                                    <div className='pull-left' style={{'width': '35%'}}>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/thumbnails/places.png' width='110'/>
                                    </div>
                                    <div className='pull-left' style={{'width': '65%'}}>
                                    <span className='statistikTitle'>
                                        4.820 Places
                                    </span>
                                    <div className='clearfix'></div>
                                    <span className='text-light-dark'>Discover thousands of local experiences in Indonesia</span>
                                    </div>
                                </a>
                            </div>

                            <div className='container'>
                                <a href='#'>
                                    <div className='pull-left' style={{'width': '35%'}}>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/thumbnails/homestay.png' width='110'/>
                                    </div>
                                    <div className='pull-left' style={{'width': '65%'}}>
                                    <span className='statistikTitle'>
                                        407 Homestays
                                    </span>
                                    <div className='clearfix'></div>
                                    <span className='text-light-dark'>Discover hundreds of local homestay in Indonesia</span>
                                    </div>
                                </a>  
                            </div>

                        </Swiper>
                    </div>

                    
                    <style jsx>{`
                    @media (max-width: 767px) {
                        .bg-content-home .container-fluid{
                            padding: 1em;
                        }
    
                        .statistikWrapper .col-sm-3{
                            display: block;
                            background: red;
                        }

                        .swiper-wrapper{
                            height: 100px;
                        }

                        .swiper-button-next, .swiper-button-prev{
                            background-color: #333 !important;
                        }
                    }

                    .swiper-button-next, .swiper-button-prev{
                        background-color: #333;
                    }

                    .statistikTitle{
                        font-weight: bold;
                        color: #333;
                        font-size: 16px;
                    }

                    .text-light-dark{
                        color: #797979;
                    }

                    .thumbnail-statistik .image-thumbnail{
                        width: 35%;
                    }

                    .image-thumbnnail img{
                        width: 100% !important;
                    }

                    .thumbnail-statistik .text-statistik{
                        width: 65%;
                        float: left;
                        padding-left: 10px;
                    }

                    
                    
                `}</style>
                </section>
            </React.Fragment>
        )
    }
}
