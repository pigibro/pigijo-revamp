import Loading from '../shared/loading-accommodation';
import { Link } from '../../routes';
import { slugify, urlImage } from '../../utils/helpers';
import { map, get, isEmpty, takeRight } from 'lodash';
import numeral from '../../utils/numeral';
import moment from 'moment';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
  }
}


const AccommodationCategory =  ({data, isLoading, title, link, model, classProps, loopTo , moreData, moreBtn = false, moreLoading = false, total=6}) => {
  return(
    <section className="content-wrap bg-content-home">
        <div className="container">
            <div className="row">
              <div className="col-md-12 cat-header">
                <div className="main-title sr-btm">
                  <h1 className="text-title">
                    {title}
                  </h1>
                </div>
                <Link route={`c/${link}`}>
                  <a className="see-all">
                    SEE ALL
                  </a>
                </Link>
              </div>
            </div>
            {isLoading &&
            <div className="product-list">
              <Loading loopTo={loopTo} classProps={classProps}/>
            </div>
            }
            {!isLoading &&
            <div className="row">
              <div className="product-list">
              {
                map(data, (item, index) => (
                  <div className={`col-md-${classProps[0]} col-sm-${classProps[1]} col-xs-${classProps[2]} ${model === 1? 'p-item-o' : 'p-item'}`} key={`p-${index}`}>
                    <Link route="rent-car" params={{slug: item.slug}}>
                    <a>
                    <div className="plan-item sr-btm">
                      <div className="box-img plan-img">
                        <div className="thumb" style={_imgStyle(urlImage(item.image_path+'/'+item.image_filename))}/>
                        <div className="box-img-top">
                          {!isEmpty(item.city) && <span><i className="ti-pin"/> {item.city.name}</span> }
                        </div>
                        <div className="box-img-bottom">
                          {item.discount > 0 && <p className="p_discount">{Math.round(item.discount)}% OFF</p>}
                        </div>
                      </div>
                      <div className="plan-info">
                        <ul>
                          <li className="title"><h3 title={item.name}>{item.name}</h3></li>
                          <li className="min-person">
                            <p className="text-label">Min {item.min_person} person{item.min_person === 1? '' : 's'}</p>
                          </li>
                          <li className="price">
                            {item.discount > 0 && <p className="price-before"><del>Rp {numeral(parseInt(item.price_before,10)).format('0,0')}</del></p>}
                            <p>
                              <span  className="price-latest">
                                <b className="text-primary">Rp {numeral(parseInt(item.price,10)).format('0,0')}</b>
                              </span>
                              <span className={ item.upcomming_schedule === moment().format('YYYY-MM-DD') || item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') ? 'notice text-success' : 'notice text-info'}>
                                { item.upcomming_schedule === moment().format('YYYY-MM-DD') &&
                                  'Available Today'
                                }
                                { item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') &&
                                  'Available Tommorow'
                                }
                                { moment().add(1,'days').valueOf() < moment(item.upcomming_schedule).valueOf() &&
                                  `Available ${moment(item.upcomming_schedule).format("DD MMMM YYYY")}`
                                }
                              </span>
                            </p>
                          </li>
                        </ul>
                      </div>
                    </div>{/*plan-item-end*/}
                    </a>
                    </Link>
                  </div>
                ))
              }
              {data.length > 1 && data.length < total && moreBtn && moreLoading && <div className="col-md-12"><Loading loopTo={loopTo} classProps={classProps}/></div>}
              </div>
              {
                data.length > 1 && data.length < total && moreBtn && !moreLoading &&
                <div className="col-md-6 col-md-offset-3">
                  <button id={link} className="btn-o btn btn-primary btn-block" onClick={moreData}>
                    More
                  </button>
                </div>
              }
            </div>
            }
        </div>
    </section>
  );
};
export default AccommodationCategory;