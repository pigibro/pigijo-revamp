import { map } from 'lodash';
const loopTo = [1,1,1,1];
const LoadingBlog = props => (
  <div className="loading">
  {
    map(loopTo, (item, index) => (
      <div className="col-lg-3 col-md-3 col-sm-6 col-xs-6" key={index}>
          <div className="single-article sr-btm blog-content">
            <div className="box-img article-img">
              <div className="thumb">
                {/* <img src={item.thumbnail_images.full.url} alt="" /> */}
              </div>
            </div>
            <div className="single-desc">
              <div className="text-label"></div>
              <p className="single-title"></p>
              <div className="readmore"></div>
            </div>
          </div>{/* single-article */}
        </div>
    )) 
  } 
  </div>
)

export default LoadingBlog;