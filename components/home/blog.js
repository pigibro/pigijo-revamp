import React, { PureComponent } from "react";
import { connect } from "react-redux";
import LoadingBLog from './loading-blog';
import { map, isEmpty } from 'lodash'
import { getLatestBlog } from '../../stores/actions';
import LazyLoad from 'react-lazy-load';
import ImageLoader from '../../utils/ImageLoader';

class Blog extends PureComponent {
  componentDidMount() {
    if (isEmpty(this.props.blogs))
      this.props.dispatch(getLatestBlog());
  }
  render() {
    const _divStyle = (slide) => {
      return {
        backgroundImage: `url(${slide})`,
        backgroundSize: `cover`,
        backgroundPosition: 'center center',
        width: '100%',
        height: '100%'
      }
    }
    const _imgStyle = (url) => {
      return {
        backgroundImage: `url(${url})`,
        backgroundSize: 'cover',
        backgroundPosition: ' center center',
      }
    }
    const { loadingBlog, blogs } = this.props;
    return (
      <section>
        <div className="home-travel-diary">
          <div className="thumb diary-img">
            <LazyLoad debounce={true}>
              <ImageLoader src={isEmpty(blogs) ? "/static/images/img05.jpg" : blogs[0].thumbnail_images.full.url} />
            </LazyLoad>
          </div>

          <div className="diary-desc">
            <div className="container sr-btm">
              <div className="row">
                <div className="col-lg-7 col-lg-offset-0 col-md-7 col-md-offset-0 col-sm-6 col-sm-offset-3">
                  <div className="text-label">Travel Diary</div>
                  <h2 className="text-title" dangerouslySetInnerHTML={{ __html: isEmpty(blogs) ? "" : blogs[0].title }}></h2>
                  <div className="diary-text">
                    <a className="link" href={isEmpty(blogs) ? "" : blogs[0].url} target="_blank">Read Article</a>
                  </div>
                </div>
              </div>{/* row */}
            </div>{/* container */}
          </div>
        </div>{/* home-travel-diary */}
        <div className="list-article">
          <div className="container">
            <div className="row">
              {
                loadingBlog || isEmpty(blogs) ? (<LoadingBLog />) :
                  (
                    map(blogs, (item, index) => {
                      if (index > 0) {
                        return (
                          <div className="col-lg-3 col-md-3 col-sm-6 col-xs-6" key={item.id}>
                            <div className="single-article sr-btm blog-content">
                              <a href={item.url} target="_blank">
                                <div className="box-img article-img">
                                  <div className="thumb">
                                    <LazyLoad debounce={true}>
                                      <ImageLoader src={item.thumbnail_images.full.url} alt={item.title} />
                                    </LazyLoad>
                                  </div>
                                </div>
                                <div className="single-desc">
                                  <span className="text-label">Travel Diary</span>
                                  <h2 className="single-title" dangerouslySetInnerHTML={{ __html: item.title }}></h2>
                                  <span className="readmore">Read Article</span>
                                </div>
                              </a>
                            </div>{/* single-article */}
                          </div>
                        )
                      }
                    }
                    )
                  )
              }
              <div className="col-md-4 col-md-offset-4 btn-register-blog mb1">
                <a href="https://blog.pigijo.com/register" rel='noopener noreferrer' target="_blank" className="btn btn-primary btn-block">Become an Author</a>
              </div>
            </div>{/* row */}
          </div>{/* container */}
        </div>{/* list-article */}
      </section>
    );
  }
}

const mapStateToProps = state => ({
  loadingBlog: state.blog.loading,
  blogs: state.blog.data
})

export default connect(mapStateToProps)(Blog);