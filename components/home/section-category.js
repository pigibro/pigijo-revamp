
import { Link } from '../../routes';
const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: 'top center',
  }
}


const SectionCategory =  ({title, link, description}) => {
  return(
    <section className="section-category">
       <div className="home-travel-diary">
            <div className="thumb diary-img" style={_imgStyle("/static/images/img07.jpg")}/>
            <div className="diary-desc">
              <div className="container sr-btm">
                <div className="row">
                    <div className="col-lg-7 col-lg-offset-0 col-md-7 col-md-offset-0 col-sm-6 col-sm-offset-3">
                      {/* <div className="text-label">Travel Diary</div> */}
                      <h2 className="text-title">{title}</h2>
                      <div className="diary-text">
                        {description}
                      </div>
                    </div>
                </div>{/* row */}
              </div>{/* container */}
            </div>
        </div>
    </section>
  );
};
export default SectionCategory;