import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Swiper from "react-id-swiper/lib";
import numeral from '../../utils/numeral';
import LoadingEvent from './loading-event';
import { Link, Router } from '../../routes';
import { map, isEmpty } from 'lodash'
import { urlImage, dateGroup } from '../../utils/helpers';
import { Glyphicon } from 'react-bootstrap';
import { getEvent } from '../../stores/actions';

class Event extends PureComponent {
  componentWillMount(){
    if (isEmpty(this.props.event))
       this.props.dispatch( getEvent({limit: 8}) );
  }
  render() {
    const { loadingEvent, event } = this.props;
    const _divStyle = slide => {
      return {
        backgroundImage: `url(${slide})`,
        backgroundSize: `cover`,
        backgroundPosition: "center center",
        width: "100%",
        height: "100%"
      };
    };
    const slider = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    const params = {
      loop: true,
      speed: 600,
      parallax: true,
      parallaxEl: {
        el: '.parallax-bg',
        value: '-23%'
      },
      // renderParallax: () => <div className="parallax-bg" style={parallaxBg} />,
      
      rebuildOnUpdate: true,
      simulateTouch: false,
      slidesPerView: 3,
      autoplay: true,
      spaceBetween: 30,
      slidesPerColumn: 1,
      paginationClickable: true,
      autoplayDisableOnInteraction: false,
      preloadImages: true,
      containerClass: "place-list swiper-container sr-btm col-md-9",
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 3,
          spaceBetween: 40,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 20,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 16,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        320: {
          slidesPerView: 2,
          spaceBetween: 16,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        }
      }
    };
    return (
      <section className="content-wrap event-list bg-content-home">
        <div className="container">
          <div className="row flex-row flex-center">
            <div className="col-md-3 col-lg-3 col-sm-12">
              <div className="main-title sr-btm">
                <h1 className="text-title">POPULAR EVENT</h1>
              </div>
              <div className="welcome-text">
                <p>Don't Miss another Event!</p>
                <p>
                  Whether you wanna see your favorite artists, or just
                  looking for a great night out with your friends. We have
                  it all. Discover the best live music, conference,
                  marathon, and many more from all over Indonesia.
                </p>
                {/*<p>Check out more from our events by clicking the link Below.</p>*/}
              </div>
              <br/><br/>
                <a href="/events" className="btn btn-primary btn-submit">More Events</a>
            </div>
              {
                loadingEvent || isEmpty(event)  ? (
                  <LoadingEvent/>
                ):(
                  <Swiper {...params}>
                    { map( event, (item, index) => (
                        <div
                          data-swiper-slide-index={item.product_slug}
                          key={item.product_slug}
                          className="swiper-slide-active"
                          style={{ width: "294.25px", marginRight: "30px" }}
                        >
                          <Link route="activity" params={{slug: item.product_slug}}>
                            <a>
                              <div className="plan-item sr-btm">
                                  <div className="box-img plan-img">
                                    <div className="thumb">
                                      <img src={urlImage(item.product_cover_path +"/small/"+item.product_cover_filename)} alt={item.product_name} />
                                    </div>
                                  </div>
                                  <div className="plan-info">
                                    <h2 className="plan-title">{item.product_name}</h2>
                                  </div>
                                  
                                  <div className="plan-footer flex-row flex-center">
                                    <div className="price">
                                      <div className="icon-event">
                                        <Glyphicon glyph="glyphicon glyphicon-map-marker" />
                                        <span> {item.city_name}, {item.province_name.length > 25 ?item.province_name.slice(0,13)+` ...`:item.province_name} </span>
                                      </div>
                                      <div className="icon-event">
                                        <Glyphicon glyph="glyphicon glyphicon-calendar" />
                                        <span className="text-primary">{item.always_available === 1 ? "Everyday" : dateGroup(item.schedules[0].start_date,item.schedules[0].end_date)}</span>
                                      </div>
                                      <br />
                                      <div className="text-label">Price starts from:</div>
                                        <div className="amount h5 text-primary">
                                          {numeral(parseInt(item.price,10)).format('0,0')}
                                        </div>
                                        {item.is_promo ? (
                                          <div className="text-muted" style={{ fontStyle: 'italic' }}>
                                            <s>
                                              {numeral(parseInt(item.price_before,10)).format('0,0')}
                                            </s>
                                          </div>
                                        ) : (
                                          ''
                                        )}
                                        <div className="text-label" style={{ fontSize: '.5em ' }}>
                                          per person
                                        </div>
                                    </div>
                                  </div>
                              </div>
                            </a>
                            </Link>
                        </div>  
                      )
                    )}
                  </Swiper>
                )
              }
          </div>
        </div>
      {/* container */}
    </section>
    );
  }
}

const mapStateToProps = state => ({
  loadingEvent: state.event.loading,
  event: state.event.data
})

export default connect(mapStateToProps)(Event);