import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import IsolatedScroll from 'react-isolated-scroll';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import {escapeRegexCharacters, urlImage} from '../../utils/helpers';

const data = [
     {
      title : 'Activity',
      data : [
        {
          name: 'Rasakan Jelajah Tiga Pulau Dalam 1 Day Open Trip Pulau Cipir, Kelor, Onrust',
          slug: '192-paket-tour-di-rasakan-jelajah-tiga-pulau-dalam-1-day-open-trip-pulau-cipir-kelor-onrust',
          type: 'tour',
          image_path: '86024cad1e83101d97359d7351051156/2018/09/07',
          image_filename: 'd41d8cd98f00b204e9800998ecf8427e.jpg',
        },
        {
          name: 'Jelajahi Destinasi Terbaik Jawa Timur Open Trip Bromo, Air Terjun Tumpak Sewu Goa Tetes Lumajang',
          slug: '1195-paket-tour-di-jelajahi-destinasi-terbaik-jawa-timur-open-trip-bromo-air-terjun-tumpak-sewu-goa-tetes-lumajang',
          type: 'tour',
          image_path: '86024cad1e83101d97359d7351051156/2019/03/13',
          image_filename: 'a7fdac73cc70003117e096d7f9dbe633.jpg',
        },
        {
          name: 'The Best Animal Tourism in Lampung 2D1N Way Kambas',
          slug: '1227-paket-tour-di-the-best-animal-tourism-in-lampung-2d1n-way-kambas',
          type: 'tour',
          image_path: '86024cad1e83101d97359d7351051156/2019/04/02',
          image_filename: '441a96efbd58eee48045ea3fff09011a.png',
        },
      ]
     },
     {
       title: 'Locations',
       data: [
        {
          name:'Jakarta Barat',
          slug:'jakarta-barat',
          type: 'city',
          image_path: '86024cad1e83101d97359d7351051156/2019/04/02',
          image_filename: '441a96efbd58eee48045ea3fff09011a.png',
        },
        {
           name:'Jakarta Selatan',
           slug:'jakarta-selatan',
           type: 'city',
           image_path: '86024cad1e83101d97359d7351051156/2019/04/02',
           image_filename: '441a96efbd58eee48045ea3fff09011a.png',
         }
       ]
     }
   ];
const getSuggestions = value => {
  const escapedValue = escapeRegexCharacters(value.trim());
  if (escapedValue === '') {
    return [];
  }
  const regex = new RegExp('\\b' + escapedValue, 'i');
  return data
    .map(section => {
      return {
        title: section.title,
        data: section.data.filter(item => regex.test(item.name))
      };
    })
    .filter(section => section.data.length > 0);
  // return data.filter(data => regex.test(getSuggestionValue(data)));
};
const getSuggestionValue = (suggestion) => {
  return suggestion.name;
}

function getSectionSuggestions(section) {
  return section.data;
}

function renderSectionTitle(section) {
  return (
    <strong>{section.title}</strong>
  );
}


// const getSuggestionValue = suggestion => suggestion.name;

const renderSuggestion = (suggestion, {query}) => {
  const suggestionText = suggestion.name;
  const matches = match(suggestionText, query);
  const parts = parse(suggestionText, matches);
  return (
    <span className='suggestion--content'>
      <a href={suggestion.type === 'city' ? '/'+suggestion.type+'/'+suggestion.slug : '/p/'+suggestion.slug}>
        <img className="suggestion--image" src={urlImage(suggestion.image_path+'/xsmall/'+suggestion.image_filename)} alt={suggestion.name}/>
        <span className="suggestion--highlight">
          {
            parts.map((part, index) => {
              const className = part.highlight ? 'highlight' : null;
              return (
                <span className={className} key={index}>{part.text}</span>
              );
            })
          }
        </span>
      </a>
    </span>
  );
};

const renderSuggestionsContainer = ({ containerProps, children }) => {
  const { ref, ...restContainerProps } = containerProps;
  const callRef = isolatedScroll => {
    if (isolatedScroll !== null) {
      ref(isolatedScroll.component);
    }
  };
  return (
    <IsolatedScroll ref={callRef} {...restContainerProps}>
      {children}
    </IsolatedScroll>
  );
}
 
export default class Search extends Component {
  constructor() {
    super();
    this.state = {
      value: '',
      suggestions: []
    };
  }
  
  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: getSuggestions(value)
    });
  };
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };
  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: 'Search whatever do you want...',
      value,
      onChange: this.onChange
    };
    return (
      <div className='advance--search'>
        <Autosuggest
          multiSection={true}
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          renderSectionTitle={renderSectionTitle}
          getSectionSuggestions={getSectionSuggestions}
          renderSuggestionsContainer={renderSuggestionsContainer}
          inputProps={inputProps}
        />
      </div>
    );
  }
}