import React, { Component } from 'react'

export default class Partners extends Component {
    render() {
        return (
            <React.Fragment>
                <div className='partners-wrapper'>
                    <div className='container'>
                        <div className='row'>
                            <h3 className='text-center text-title'>Affiliates</h3>
                            <div className='col-sm-2 col-xs-4 col-sm-offset-3'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/01+itx_partner_tr.png' width='90%' />
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/Wonderful_Indonesia_logo.png' width='80%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/PATA+logo+trans.png' width='50%'></img>
                                </p>
                            </div>
                        </div>

                        <div className='row'>
                            <h3 className='text-center text-title' style={{ 'paddingTop': '40px' }}>Our Partners</h3>
                            <div className='col-sm-2 col-xs-4 col-sm-offset-1'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/Youreka_Logo.png' width='75%' />
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/02+midtrans+logo+trans+side.png' width='80%' />
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/03+logo-loket-blue.png' width='70%' />
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/04+opsigo.png' width='70%' />
                                </p>
                            </div>
                            {/*<div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/05+MG+group+trans.png' width='70%'/>
                                </p>
                            </div>*/}
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/06+redbus+logo+trans.png' width='50%' />
                                </p>
                            </div>
                        </div>

                        <div className='row'>
                            {/* <div className='col-sm-2 col-xs-4 col-sm-offset-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/07+Airy+Rooms.png' width='40%'></img>
                                </p>
                            </div> */}
                            {/*<div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/08+ctrip-logo.png' width='50%'></img>
                                </p>
        </div>*/}
                            <div className='col-lg-12 col-md-12 col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/09+toorhop-logo-trans.png' width='15%'></img>
                                </p>
                            </div>
                        </div>

                        <div className='row'>
                            <h3 className='text-center text-title' style={{ 'paddingTop': '40px' }}>Communities</h3>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/01+logo-imi.png' width='40%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/02+PaSTI+logo_trans.png' width='80%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/03+Serikat+Pendaki.png' width='50%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/04+BPI+Jabodetabek.png' width='50%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/05+JARI+logo_small.png' width='50%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/06+LOGO+Indonesia+Mountains_small.png' width='50%'></img>
                                </p>
                            </div>
                        </div>

                        <div className='row'>
                            <div className='col-sm-2 col-xs-4 col-sm-offset-2'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/07+Logo+Carter+pendaki.png' width='40%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/08+APGI+logo+trans1.png' width='80%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/communities/09+Logo+PUWSI_trans.png' width='60%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/Samarauke+logo+512.png' width='40%'></img>
                                </p>
                            </div>
                        </div>

                        <div className='row'>
                            <h3 className='text-center text-title' style={{ 'paddingTop': '40px' }}>Payments</h3>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/paymentGateway/slack-imgs.com.png' width='70%'></img>
                                </p>
                            </div>
                            {/* <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/paymentGateway/01+GPN+logo_small.png' width='40%'></img>
                                </p>
                            </div> */}
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/paymentGateway/02+Visa+logo+trans.png' width='50%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/paymentGateway/03+mastercard-vector-logo.png' width='40%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/paymentGateway/04+JCB_logo+small.png' width='40%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/paymentGateway/05+Gopay+logo+trans+side.png' width='70%'></img>
                                </p>
                            </div>
                            <div className='col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/paymentGateway/06+Akulaku+logo+trans+side.png' width='80%'></img>
                                </p>
                            </div>
                        </div>
                        {/* <div className='row'>
                            <div className='col-lg-12 col-md-12 col-sm-2 col-xs-4'>
                                <p className='text-center'>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/partners/paymentGateway/06+Akulaku+logo+trans+side.png' width='13%'></img>
                                </p>
                            </div>
                        </div> */}

                    </div>
                </div>


                <style jsx>{`
                    .partners-wrapper{
                        margin: 50px 0;
                    }

                    .partners-wrapper img{
                        vertical-align: middle;
                        text-align: center;
                    }

                    .partners-wrapper p{
                        line-height: 55px;
                    }
                `}</style>
            </React.Fragment>
        )
    }
}
