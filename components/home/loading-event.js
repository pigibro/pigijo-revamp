import { map } from 'lodash';
const loopTo = [1,1,1];
const LoadingEvent = props => (
  <div className="col-md-9 col-xs-12">
  {(
      <div className="place-list">
      {
        map(loopTo, (item, index) => (
          <div className="col-md-4 col-sm-4 col-xs-6 item-list" key={index}>
            <div className="loading" key={`loading.${index}`}>
              <div className="plan-item sr-btm">
                <div className="box-img plan-img">
                  <div className="thumb"></div>
                  </div>
                </div>
                <div className="plan-info">
                  <p className="plan-title"></p>
                </div>
                <div className="plan-footer flex-row flex-center">
                  <div className="price">
                    <div className="icon-event">
                      <div className="travel-period"></div>
                    </div>
                    <br />
                    <div className="amount h4 text-primary">
                    </div>
                  </div>
                </div>
            </div>
          </div>
        ))
      }
      </div>
  )}
  </div>
)

export default LoadingEvent;