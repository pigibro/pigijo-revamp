import { map } from "lodash";

// ==================|| README || ================ 
// ==================|| how to use || ============
//--------------------------------------------------------------------------
// #loopTo                                                                ||
// you can [PROPS] total ternary or total item typeof must be an Array.   ||
// by default [1, 1, 1, 1]                                                ||
// -------------------------------------------------------------------------

//#classProps                                                             ||
// send PROPS must be typeof an number for fill number of column.         ||
// by default 3


const LoadingFlashale = ({ loopTo = [1, 1, 1, 1], classProps = 3 }) => (
	<div className="flex items warp">
		{
			<div className="row">
				{map(loopTo, (item, index) => (
					<div className={`col-md-${classProps} col-sm-6 col-xs-6 p-item`} key={index}>
						<div className="loading" key={`loading.${index}`}>
							<div className="plan-item sr-btm">
								<div className="box-img plan-img">
									<div className="thumb" />
								</div>
								<div className="plan-info">
									<ul>
										<li className="title" />
										<li className="min-person">
											<p className="text-label" />
										</li>
										<li className="price">
											<p className="price-before" />
											<p>
												<span className="price-latest" />
												<span className="notice" />
											</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				))}
			</div>
		}
	</div>
);

export default LoadingFlashale;
