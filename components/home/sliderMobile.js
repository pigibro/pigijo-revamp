import React, { Component } from 'react'
import Swiper from 'react-id-swiper';

export default class SliderMobile extends Component {
  render() {
    return (
      <React.Fragment>
        <div className='visible-xs banner-mobile'>
          <Swiper>
            <div>
              <a href='#'>
                  <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/banner/newBanner/banner_bromo.jpg' className='banner-mobile' />
              </a>
            </div>
            <div>
              <a href='/road-trip'>
                  <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/slider/road-trip.jpg' width="100%" className='banner-mobile' />
              </a>
            </div>
            <div>
              <a href='https://play.google.com/store/apps/details?id=app.pigijo.com&hl=en'>
                  <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/slider/download.jpg' width="100%" className='banner-mobile' />
              </a>
            </div>
            <div>
              <a href='/how-to-use'>
                  <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/slider/how-to-use.jpg' width="100%" className='banner-mobile' />
              </a>
            </div>
          </Swiper>

          <style jsx>{`
              .banner-mobile a img{
                width: 100%;
              }

              .banner-mobile{
                height: 240px;
                object-fit: cover;
              }
          `}</style>
        </div>
      </React.Fragment>
    )
  }
}
