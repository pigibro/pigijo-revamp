import Loading from '../shared/loading-activity';
import { Link } from '../../routes';
import { slugify, urlImage } from '../../utils/helpers';
import { map, get, isEmpty, takeRight } from 'lodash';
import numeral from '../../utils/numeral';
import moment from 'moment';

const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: 'cover',
    backgroundPosition: ' center center',
  }
}


const ActivityCategory = ({ data, isLoading, title, subtitle, link, model, classProps, loopTo, moreData, moreBtn = false, moreLoading = false, total = 6 }) => {
  return (
    <section className="content-wrap bg-content-home">
      <div className="container">
        <div className="row">
          <div className="col-md-12 cat-header">
            <div className="main-title sr-btm" style={{ 'zIndex': '-2' }}>
              <h1 className="text-title">
                {title}
              </h1>
              <p>{subtitle}</p>
            </div>
            <Link route={`c/${link}`}>
              <a className="see-all">
                See All
                  </a>
            </Link>
          </div>
        </div>
        {isLoading &&
          <div className="product-list">
            <Loading loopTo={loopTo} classProps={classProps} />
          </div>
        }
        {!isLoading &&
          <div className="row">
            <div className="product-list">
              {
                map(data, (item, index) => (
                  <div className={`col-md-${classProps[0]} col-sm-${classProps[1]} col-xs-${classProps[2]} ${model === 1 ? 'p-item-o' : 'p-item'}`} key={`p-${index}`}>
                    <Link route="activity" params={{ slug: item.slug }}>
                      <a>
                        <div className="plan-item sr-btm">
                          <div className="box-img plan-img">
                            <div className="thumb" style={_imgStyle(urlImage(item.image_path + '/' + item.image_filename))} />

                            <div className="box-img-bottom">
                              {item.discount > 0 && <p className="p_discount">{Math.round(item.discount)}% OFF</p>}
                            </div>
                          </div>
                          <div className="itech-card">
                            <div className="box-img-top">
                              {!isEmpty(item.city) && <span>{item.city.name}</span>}
                            </div>
                            <ul>
                              <li className="title"><h3 title={item.name}>{item.name}</h3></li>
                              <li className="price">
                                {item.discount > 0 && <p className="price-before"><del>Rp {numeral(parseInt(item.price_before, 10)).format('0,0')}</del></p>}

                                <span className="price-latest">
                                  {item.product_category !== 'Local Assistants' ?
                                    <span><span className="text-primary">Rp {numeral(parseInt(item.price, 10)).format('0,0')}</span><span className="text-person">/unit</span></span> :
                                    <span><span className="text-primary">Rp {numeral(parseInt(item.price, 10)).format('0,0')}</span><span className="text-person">/day</span></span>
                                  }
                                </span>
                              </li>

                              <li className="min-person">
                                <span className={item.upcomming_schedule === moment().format('YYYY-MM-DD') || item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') ? 'notice text-success' : 'notice text-info'}>
                                  {item.upcomming_schedule === moment().format('YYYY-MM-DD') &&
                                    'Available Today'
                                  }
                                  {item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') &&
                                    'Available Tommorow'
                                  }
                                  {moment().add(1, 'days').valueOf() < moment(item.upcomming_schedule).valueOf() &&
                                    `Available ${moment(item.upcomming_schedule).format("DD MMMM YYYY")}`
                                  }
                                </span>, &nbsp;
                            <span className="text-min-small">Min {item.min_person} unit{item.min_person === 1 ? '' : 's'}</span>
                              </li>
                            </ul>

                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                ))
              }
              {data.length > 1 && data.length < total && moreBtn && moreLoading && <div className="col-md-12"><Loading loopTo={loopTo} classProps={classProps} /></div>}
            </div>
            {
              data.length > 1 && data.length < total && moreBtn && !moreLoading &&
              <div className="col-md-4 col-md-offset-4">
                <button id={link} className="btn-o btn btn-primary btn-block" onClick={moreData}>
                  More
                  </button>
              </div>
            }
          </div>
        }

        <style jsx>{`
                .plan-item{
                  box-shadow: none;
                }
                
                .itech-card h3{
                  font: 18px Helvetica;
                  font-weight: bold;
                  margin: 4px 0;
                }

                .itech-card ul{
                  list-style: none;
                  padding-left: 0;
                }

                .text-city{
                  font: 12px Helvetica;
                  font-weight: bold;
                }

                .box-img-top{
                  background: none;
                  font: 12px helvetica;
                  font-weight: bold;
                  text-transform: uppercase;
                  color: #484848;
                  margin-top: 10px;
                  white-space: nowrap;
                  overflow: hidden;
                  text-overflow: ellipsis;
                }

                .plan-item{
                  margin-bottom: 40px;
                  min-height: 410px;
                }

                .price .text-primary, .price .text-person{
                  color: #4C4C4C;
                  font-size: 16px;
                }

                .text-dark-gray{
                  color: #484848;
                }

                .text-gray{
                  color: #7B7B7B;
                }

                .text-info{
                  color: #7B7B7B;
                  font: 13px Helvetica;
                }

                .text-min-small{
                  font: 13px Helvetica;
                  color: #7B7B7B;
                }

                .btn-o.btn-primary{
                  color: #787878;
                  background-color: transparent;
                  box-shadow: rgba(0, 0, 0, 0.12) 0px 2px 16px !important;
                  border: none;
                  border-radius: 2px;
                }

                .btn-o.btn-primary:hover{
                  border: none;
                }

                .see-all{
                  color: #008489;
                  font-weight: bold;
                }

                .cat-header p{
                  font-size: 15px;
                  color: #797979;
                }

                .main-title h1{
                  margin-bottom: 0px;
                }

                .text-label{
                  color: #787878;
                }

                @media (max-width: 767px) {
                  .itech-card h3{
                    font: 15px Helvetica;
                    font-weight: bold;
                    margin: 4px 0;
                  }

                  .plan-item{
                    margin-bottom: 20px;
                    min-height: 260px;
                  }

                  .content-wrap .container-fluid{
                    padding: 0 1em;
                  }
                }
            `}</style>
      </div>
    </section>
  );
};
export default ActivityCategory;