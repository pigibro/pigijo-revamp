import React, { Component } from 'react';
import { get, isEmpty, takeRight } from 'lodash';
import { MenuItem } from 'react-bootstrap';
import { AsyncTypeahead, Highlighter, Token, Menu, menuItemContainer } from 'react-bootstrap-typeahead';
import { advanceSearch, getAreas } from '../../stores/actions';
import { urlImage, ucFirst } from '../../utils/helpers';
import Router from 'next/router'
import {isMobile} from 'react-device-detect'

import { Link } from '../../routes'

const TypeaheadMenuItem = menuItemContainer(MenuItem);
const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: 'cover',
    backgroundPosition: ' center center',
  }
}

export default class Search extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      searchValue: '',
      data: [],
      defaultData: [],
      isOpen: false,
    }
  }
  componentDidMount(){
    const { dispatch } = this.props;
    // dispatch(getAreas()).then((result) =>{
    //   if (get(result, 'meta.code') === 200) {
    //     this.setState({
    //       defaultData: get(result, 'data'),
    //       data: get(result, 'data'),
    //       isLoading: false,
    //     });
    //   }else{
    //     this.setState({
    //       defaultData: [],
    //       isLoading: false,
    //     });
    //   }
    // });
  }

  onChange = (value) => {
    this.setState({searchValue: value});
    if(value[0].type === 'area' || value[0].type === 'province' || value[0].type === 'Kota' || value[0].type === 'Kabupaten' || value[0].type ==='suggestion' || value[0].type === 'Suggestion'){
      Router.push(`/${value[0].slug}`);
    }
    else if(value[0].type === 'Place'){
      Router.push(`/place/${value[0].slug}`);
    }
    else{
      Router.push(`/p/${value[0].slug}`);
    }
  };

  handleSearch = (query) => {
    const {dispatch} = this.props;
    this.setState({isLoading: true});
    dispatch(advanceSearch({q: query, per_page: 300})).then((result) =>{
      if (get(result, 'meta.code') === 200) {
        if(get(result, 'pagination')) {
          dispatch(advanceSearch({q: query, per_page: get(result, 'pagination.total')})).then(results => {
            this.setState({
              data: get(results, 'data'),
              isLoading: false
            });
          })
        }
        
      }else{
        this.setState({
          data: [],
          isLoading: false,
        });
      }
    });
  }

  onFocusSearch = () => {
    this.setState({isLoading: true, isOpen: true});
  }

  onBlurSearch = () => {
    this.setState({isLoading: false, isOpen: false});
  }

  renderToken = (option,props,index) => {
    if(index < (this.props.maxLimit || 1)){
      return (
        <Token
          key={index}
          onRemove={props.onRemove}>
          {`${option.name}`}
        </Token>
      );
   }
   if(index > (this.props.maxLimit-1 || 0)){
      setTimeout(() => {
        props.onRemove();
      }, 500);
    }
  }
  _renderMenu = (results, menuProps, props) =>
  {
    if(props.text.length > 0){
      return (
        <Menu {...menuProps}>
          {results.map((option, index) => (
            <TypeaheadMenuItem option={option} key={`menu-${index}`}>
              {this._renderMenuItemChildren(option, props, index)}
            </TypeaheadMenuItem>
          ))}
        </Menu>
      ); 
    }else{
      return (
        <Menu {...menuProps}>
          {results.map((option, index) => (
            <TypeaheadMenuItem option={option} key={`menu-${index}`}>
              {this._renderMenuItemChildren(option, props, index)}
            </TypeaheadMenuItem>
          ))}
        </Menu>
      ); 
    }
  }
 _renderMenuItemChildren=(option, props, index) => {
   if(get(option,'name')){
    return (
        <>
          {/* <img className="suggestion--image" key={`image-${index}`} src={urlImage(option.image_path+'/xsmall/'+option.image_filename)} alt={option.name}/> */}
          {
            (get(option, 'type') === 'Kabupaten' || get(option, 'type') === 'Kota' || get(option, 'type') === 'province' || get(option, 'type') === 'area' || get(option, 'type') === 'suggestion' || get(option, 'type') === 'Suggestion')
            ?
            <i className="ti-pin mr1"/> 
            :
            <img className="suggestion--image" key={`image-${index}`} src={urlImage(get(option,'image_path')+'/xsmall/'+get(option,'image_filename'))} alt={option.name}/>
          }
          <Highlighter key={`highlight-${index}`} search={props.text}>
            {option.name}
          </Highlighter>
          {
            (get(option, 'type') === 'Kabupaten' || get(option, 'type') === 'Kota' || get(option, 'type') === 'province' || get(option, 'type') === 'area')
            &&
            <span key={"type-"+index} className="search-type">
              <small>
              {ucFirst(get(option, 'type').toLowerCase()  === 'area'  ? 'all' : get(option, 'type'))}
              </small>
            </span>
          }
        </>
    );
  }
    // return  [
      
    //   ,
    //   ,
    //   ,
    // ];
  }

  render(){
    const { data, searchValue,isLoading, isOpenSuggestion, isOpen } = this.state;
    let styleDefault = {display:"none"};
    if(isOpenSuggestion){
      styleDefault = {dispaly:"block"};
    }
    return(
      <React.Fragment>
        <div className='container search-autocomplete'>
          <div className="row">
          <div className="col-lg-8 col-lg-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 form-search">
            <AsyncTypeahead
              open={isOpen}
              isLoading={isLoading}
              onSearch={query => this.handleSearch(query)}
              id={'desk-advance-search'}
              multiple={false}
              key={"select-1"}
              filterBy={['tag']}
              data-index={0}
              useCache
              promptText={'Bali, Jakarta, Lombok, Diving, Hiking, etc...'}
              searchText={'Searching...'}
              onBlur={this.onBlurSearch}
              onFocus={this.onFocusSearch}
              // onInputChange={this.onInputChangeSearch}
              labelKey={'name'}
              inputValue={searchValue}
              onChange={(e) => this.onChange(e)}
              renderToken={(option,props,index) => this.renderToken(option,props,index)}
              options={data}
              renderMenu={this._renderMenu}
              placeholder="Search: Where do You Want to go? /or What do you want to do?"
              // renderMenuItemChildren={this._renderMenuItemChildren}
              className={`search-box-adv desktop-search single-search home-search ${isMobile && 'modal-search'}`}
              selectHintOnEnter
              bsSize="large"
              // maxResults={300}
            />
          </div>
        </div>
        </div>
        <style jsx>{`
          .search-autocomplete{
            margin: 30px 0;
          }

          .search-box-adv .rbt-input{
            border: none !important;
            box-shadow: rgba(0,0,0,0.12) 0px 2px 16px !important;
          }

          .form-control{
            border: none !important;
          }
      `}</style>
      </React.Fragment>
    )
  }
}