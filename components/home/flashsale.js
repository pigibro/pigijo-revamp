import React, { PureComponent } from "react";
// import { connect } from "react-redux";
import Swiper from "react-id-swiper/lib";
import numeral from '../../utils/numeral';
import Loading from '../shared/loading-activity';
import { Link, Router } from '../../routes';
import { map, get, isEmpty, takeRight } from 'lodash';
// import ImageLoader from '../../utils/ImageLoader';
import { getActivityPromos } from '../../stores/actions';
import { slugify, urlImage } from '../../utils/helpers';
import { Glyphicon } from 'react-bootstrap';
import moment from 'moment';

const _imgStyle = (url) => {
  return {
      backgroundImage: `url(${url})`,
      backgroundSize: 'cover',
      backgroundPosition: ' center center',
  }
}

class FlashSale extends PureComponent {
  render() {
    const { isLoading, data, isReload } = this.props;
    const _divStyle = slide => {
      return {
        backgroundImage: `url(${slide})`,
        backgroundSize: `cover`,
        backgroundPosition: "center center",
        width: "100%",
        height: "100%"
      };
    };
    const params = {
      loop: true,
      speed: 600,
      parallax: true,
      parallaxEl: {
        el: '.parallax-bg',
        value: '-23%'
      },
      // renderParallax: () => <div className="parallax-bg" style={parallaxBg} />,
      
      rebuildOnUpdate: true,
      simulateTouch: false,
      slidesPerView: 3,
      autoplay: true,
      spaceBetween: 7.5,
      slidesPerColumn: 1,
      paginationClickable: true,
      autoplayDisableOnInteraction: false,
      preloadImages: true,
      containerClass: "place-list swiper-container sr-btm product-list mb0",
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 3,
          spaceBetween: 7.5,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 7.5,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true
        },
        640: {
          slidesPerView: 1,
          spaceBetween: 7.5,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true,
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 7.5,
          loop: false,
          autoplay: false,
          rebuildOnUpdate: true,
        }
      }
    };
    return (
      <section className="content-wrap bg-content-home">
        <div className="container">
          <div className="row">
            <div className="col-md-12 cat-header">
              <div className="main-title sr-btm">
                <h1 className="text-title">
                  Special Offer for You
                </h1>
                <p>Come & see our monthly promo and recommendations for your holiday</p>
              </div>
              <Link route={`/promo`}>
                <a className="see-all">
                  See All
                </a>
              </Link>
            </div>
          </div>
          {isLoading || isEmpty(data) ? (
            <div className="product-list">
              <Loading loopTo={[1,1,1]} classProps={[4,6,12]}/>
            </div>
          ) :
          (
            <Swiper {...params}>
              { map(data, (item, index) => (
                  <div
                    data-swiper-slide-index={index}
                    key={index}
                    className="swiper-slide-active"
                    style={{ width: "294.25px", marginRight: "30px" }}
                  >
                    <div className="p-item-o">
                      <Link route="activity" params={{slug: item.slug}}>
                      <a>
                      <div className="plan-item sr-btm ">
                        <div className="box-img plan-img">
                          <div className="thumb" style={_imgStyle(urlImage(item.image_path+'/'+item.image_filename))}/>
                          
                          <div className="box-img-bottom">
                            {item.discount > 0 && <p className="p_discount">{Math.round(item.discount)}% OFF</p>}
                          </div>
                        </div>
                        <div className="itech-card">
                          <div className="box-img-top">
                            {!isEmpty(item.city) && <span> {item.city.name}</span> }
                          </div>
                          <ul>
                            <li className="title"><h3 title={item.name}>{item.name.substring(0,35)}...</h3></li>
                            <li className="min-person">
                              <p className="text-label">Min {item.min_person} person{item.min_person === 1? '' : 's'}</p>
                            </li>
                            <li className="price">
                              {item.discount > 0 && <p className="price-before"><del>Rp {numeral(parseInt(item.price_before,10)).format('0,0')}</del></p>}
                              <p>
                                <span  className="price-latest">
                                  <b className="text-primary">Rp {numeral(parseInt(item.price,10)).format('0,0')}</b><span className="text-person">/person</span>
                                </span>&nbsp;&nbsp;
                                <span className={ item.upcomming_schedule === moment().format('YYYY-MM-DD') || item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') ? 'notice text-success' : 'notice text-info'}>
                                  { item.upcomming_schedule === moment().format('YYYY-MM-DD') &&
                                    'Available Today'
                                  }
                                  { item.upcomming_schedule === moment().add(1, 'days').format('YYYY-MM-DD') &&
                                    'Available Tommorow'
                                  }
                                  { moment().add(1,'days').valueOf() < moment(item.upcomming_schedule).valueOf() &&
                                    `Available ${moment(item.upcomming_schedule).format("DD MMMM YYYY")}`
                                  }
                                </span>
                              </p>
                            </li>
                          </ul>
                        </div>
                      </div>{/*plan-item-end*/}
                      </a>
                      </Link>
                    </div>
                  </div>
              )
              )}
            </Swiper>
          )
        }

              <style jsx>{`
                .plan-item{
                  box-shadow: none;
                }
                
                .itech-card h3{
                  font: 18px Helvetica;
                  font-weight: bold;
                  margin: 4px 0;
                }

                .itech-card ul{
                  list-style: none;
                  padding-left: 0;
                }

                .text-city{
                  font: 12px Helvetica;
                  font-weight: bold;
                }

                .box-img-top{
                  background: none;
                  font: 12px helvetica;
                  font-weight: bold;
                  text-transform: uppercase;
                  color: #484848;
                  margin-top: 10px;
                }

                .plan-item{
                  margin-bottom: 40px;
                  min-height: 100%;
                }

                .price .text-primary, .price .text-person{
                  color: #4C4C4C;
                  font-size: 16px;
                }

                .text-dark-gray{
                  color: #484848;
                }

                .main-title h1{
                  margin-bottom: 0px;
                }

                .text-gray{
                  color: #7B7B7B;
                }

                .cat-header p{
                  font-size: 15px;
                  color: #797979;
                }

                .text-info{
                  color: #7B7B7B;
                  font: 13px Helvetica;
                }

                .text-min-small{
                  font: 13px Helvetica;
                  color: #7B7B7B;
                }

                .btn-o.btn-primary{
                  color: #787878;
                  background-color: transparent;
                  box-shadow: rgba(0, 0, 0, 0.12) 0px 2px 16px !important;
                  border: none;
                  border-radius: 2px;
                }

                .btn-o.btn-primary:hover{
                  border: none;
                }

                .see-all{
                  color: #008489;
                  font-weight: bold;
                }

                .plan-info{
                  padding: 0;
                }

                .badge-success, .badge-error{
                  border-radius: 3px;
                  margin-right: 10px;
                }

                .text-label{
                  font-size: 11px;
                  letter-spacing: 0;
                  text-transform: capitalize;
                }

                .text-label, .price-before{
                  color: #787878;
                }

                @media (max-width: 767px) {
                  .itech-card h3{
                    font: 15px Helvetica;
                    font-weight: bold;
                    margin: 4px 0;
                  }

                  .plan-item{
                    margin-bottom: 20px;
                    min-height: 260px;
                  }
                }
            `}</style>
        </div>
      </section>
    );
  }
}

export default FlashSale;