import React, { Component } from 'react'

export default class IconWrapper extends Component {
    render() {
        return (
            <div>
                <div className='container-fluid icons-wrapper' style={{'display': 'block'}}>
                    <div className='row'>
                        <nav>
                            <div>
                                <ul className="navbar-nav mr-auto pigijo-middle-nav">
                                <li className="nav-item">
                                    <h4 className='title-nav'>Plan an Amazing Trip</h4>
                                    <p className='subtitle-nav'>Create Your Itinerary Based on Your Preferences</p>
                                </li>
                                <li>
                                    <a href='#' className='active btn-toggle'>
                                    
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/plan_icon.png' />
                                    <p>Planner</p>
                                    </a>
                                </li>

                                <li>
                                    <a>
                                    <div className="icon-badge">Soon</div>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/flight_icon.png' /><p>Flight</p></a>
                                </li>

                                <li>
                                    <a>
                                    <div className="icon-badge">Soon</div>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/ta_icon.png' /><p>Assistant</p></a>
                                </li>

                                <li>
                                    <a>
                                    <div className="icon-badge">Soon</div>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/places_icon.png' /><p>Places</p></a>
                                </li>

                                <li>
                                    <a>
                                    <div className="icon-badge">Soon</div>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/experience_icon.png' /><p>Xperience</p></a>
                                </li>

                                <li>
                                    <a>
                                    <div className="icon-badge">Soon</div>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/homestay.png' /><p>Homestay</p></a>
                                </li>

                                <li>
                                    <a>
                                    <div className="icon-badge">Soon</div>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/event_icon.png' /><p>Events</p></a>
                                </li>

                                <li>
                                    <a>
                                    <div className="icon-badge">Soon</div>
                                    <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/comunity.png' /><p>Community</p></a>
                                </li>

                                </ul>
                            </div>
                        </nav>
                    </div>
                    </div>

                    <style jsx>{`
                        .icons-wrapper{
                            margin-top: 35px;
                            margin-bottom: 25px;
                          }
                          .pigijo-middle-nav{
                            list-style: none;
                          }
                
                          .pigijo-middle-nav li a{
                            background: white;
                            padding: 10px 20px;
                            display: block;
                            border: 1px solid #f5f5f5;
                            border-radius: 3px;
                            margin: 0 10px;
                            -o-transition: all 0.3s linear;
                            -moz-transition: all 0.3s linear;
                            -khtml-transition: all 0.3s linear;
                            -webkit-transition: all 0.3s linear;
                            -ms-transition: all 0.3s linear;
                            transition: all 0.3s linear;
                            position: relative;
                          }
                
                          .icon-badge{
                            border: 1px solid #fff;
                            border-radius: 12px;
                            text-align: center;
                            font-size: 10px;
                            background: red;
                            position: absolute;
                            padding: 3px 5px;
                            color: #fff;
                            top: -5px; right: 0;
                          }
                
                          .pigijo-middle-nav li a:hover, .pigijo-middle-nav li a.active{
                            background: #D6ECEB;
                            border: 1px solid #2E4052;
                          }
                
                          .pigijo-middle-nav li a img{
                            width: 56px;
                          }
                
                          .pigijo-middle-nav li a p{
                            text-align: center;
                            margin-bottom: 0;
                            color: #484848;
                          }
                
                          .title-nav{
                            font: 25px Helvetica;
                            font-weight: bold;
                            margin-bottom: 0;
                            margin-top: 25px;
                          }
                
                          .subtitle-nav{
                            font: 15px helvetica;
                            line-height: 24px;
                          }

                          .pigijo-middle-nav li a img{
                                width: 90px;
                            }

                          @media (max-width: 767px) {
                            .pigijo-middle-nav li a{
                                width: 22%;
                                float: left;
                                margin: 5px 5px 5px 0;
                                min-height: 95px;
                                padding: 10px 12px;
                            }

                            .pigijo-middle-nav li a p{
                                font: 11px helvetica;
                                line-height: 18px;
                            }

                            .pigijo-middle-nav li a img{
                                width: 50px;
                            }
                          }
                    `}</style>
            </div>
        )
    }
}
