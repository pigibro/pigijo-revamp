import Loading from '../shared/loading-activity';
import { Link } from '../../routes';
import { slugify, urlImage } from '../../utils/helpers';
import { map, get, isEmpty, takeRight } from 'lodash';
import numeral from '../../utils/numeral';
import moment from 'moment';

const _imgStyle = (url) => {
  return {
    backgroundImage: `url(${url})`,
    backgroundSize: 'cover',
    backgroundPosition: ' center center',
  }
}


const TaCategory = ({ data, isLoading, title, subtitle, link, model, classProps, loopTo, moreData, moreBtn = false, moreLoading = false, total = 6 }) => {
  return (
    <section className="content-wrap bg-content-home">
      <div className="container">
        <div className="row">
          <div className="col-md-12 cat-header">
            <div className="main-title sr-btm" style={{ 'zIndex': '-2' }}>
              <h1 className="text-title">
                {title}
              </h1>
              <p>{subtitle}</p>
            </div>
            {/* <Link route={`c/${link}`}>
              <a className="see-all">
                See All
                  </a>
            </Link> */}
          </div>
        </div>
        {isLoading &&
          <div className="product-list">
            <Loading loopTo={loopTo} classProps={classProps} />
          </div>
        }
        {!isLoading &&
          <div className="row">
            <div className="row">

              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6" style={{ padding: '10px 20px' }}>
                <div style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: '10px',
                  position: 'relative'
                }}>
                  <a href="http://elearning.pigijo.com/travel-assistance/rahman-mukhlis"><img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/rahman-profile.png" style={{ width: '90%', borderRadius: '10px' }} /></a>
                  <div id="product-card">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/rahman-product.jpg" style={{ width: '170px', height: '200px', borderRadius: '10px', position: 'absolute', bottom: '20px', right: '7px', zIndex: 10 }} />
                    <a href="http://bit.ly/tourbuilderrahman">
                      <div style={{
                        width: '170px',
                        height: '200px',
                        position: 'absolute',
                        bottom: '20px',
                        right: '7px',
                        zIndex: 50,
                        color: 'white',
                        textAlign: 'center',
                        padding: '.8em 1em'
                      }}>
                        <p style={{ fontSize: '.7em' }}>FROM JAKARTA</p>
                        <span style={{ fontSize: '.9em', fontWeight: 'bold' }}>Mount Semeru Adventure</span>
                        <p style={{ fontSize: '.7em' }}>WITH RAHMAN MUKHLIS</p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6" style={{ padding: '10px 20px' }}>
                <div style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: '10px',
                  position: 'relative'
                }}>
                  <a href="http://elearning.pigijo.com/travel-assistance/medion-suryo-wibowo"><img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/medion-profile.png" style={{ width: '90%', borderRadius: '10px' }} /></a>
                  <div id="product-card">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/medion-product.jpg" style={{ width: '170px', height: '200px', borderRadius: '10px', position: 'absolute', bottom: '20px', right: '7px', zIndex: 10 }} />
                    <a href="http://bit.ly/tourbuildermedion">
                      <div style={{
                        width: '170px',
                        height: '200px',
                        position: 'absolute',
                        bottom: '20px',
                        right: '7px',
                        zIndex: 50,
                        color: 'white',
                        textAlign: 'center',
                        padding: '.8em 1em'
                      }}>
                        <p style={{ fontSize: '.7em' }}>FROM JAKARTA</p>
                        <span style={{ fontSize: '.9em', fontWeight: 'bold' }}>Seven Summit Campaign: Rinjani Mountain</span>
                        <p style={{ fontSize: '.7em' }}>WITH MEDION SURYO WIBOWO</p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6" style={{ padding: '10px 20px' }}>
                <div style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: '10px',
                  position: 'relative'
                }}>
                  <a href="http://elearning.pigijo.com/travel-assistance/i-putu-sudiartana"><img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/putu-profile.png" style={{ width: '90%', borderRadius: '10px' }} /></a>
                  <div id="product-card">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/putu-product.jpg" style={{ width: '170px', height: '200px', borderRadius: '10px', position: 'absolute', bottom: '20px', right: '7px', zIndex: 10 }} />
                    <a href="http://bit.ly/tourbuilderputu">
                      <div style={{
                        width: '170px',
                        height: '200px',
                        position: 'absolute',
                        bottom: '20px',
                        right: '7px',
                        zIndex: 50,
                        color: 'white',
                        textAlign: 'center',
                        padding: '.8em 1em'
                      }}>
                        <p style={{ fontSize: '.7em' }}>FROM BALI</p>
                        <span style={{ fontSize: '.9em', fontWeight: 'bold' }}>Diving in Tulamben</span>
                        <p style={{ fontSize: '.7em' }}>WITH I PUTU SUDIARTANA</p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6" style={{ padding: '10px 20px' }}>
                <div style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: '10px',
                  position: 'relative'
                }}>
                  <a href="http://elearning.pigijo.com/travel-assistance/vita-landra"><img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/vita-profile.png" style={{ width: '90%', borderRadius: '10px' }} /></a>
                  <div id="product-card">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/vita-product.jpg" style={{ width: '170px', height: '200px', borderRadius: '10px', position: 'absolute', bottom: '20px', right: '7px', zIndex: 10 }} />
                    <a href="http://bit.ly/tourbuilderkerincivita">
                      <div style={{
                        width: '170px',
                        height: '200px',
                        position: 'absolute',
                        bottom: '20px',
                        right: '7px',
                        zIndex: 50,
                        color: 'white',
                        textAlign: 'center',
                        padding: '.8em 1em'
                      }}>
                        <p style={{ fontSize: '.7em' }}>FROM JAKARTA</p>
                        <span style={{ fontSize: '.9em', fontWeight: 'bold' }}>MOUNT KERINCI ADVENTURE</span>
                        <p style={{ fontSize: '.7em' }}>WITH VITA LANDRA</p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6" style={{ padding: '10px 20px' }}>
                <div style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: '10px',
                  position: 'relative'
                }}>
                  <a href="http://elearning.pigijo.com/travel-assistance/muhammad-fajar-alamsyah"><img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/fajar-profile.jpg" style={{ width: '90%', borderRadius: '10px' }} /></a>
                  <div id="product-card">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/bayu-product.jpg" style={{ width: '170px', height: '200px', borderRadius: '10px', position: 'absolute', bottom: '20px', right: '7px', zIndex: 10 }} />
                    <a href="http://bit.ly/tourbuilderbayu">
                      <div style={{
                        width: '170px',
                        height: '200px',
                        position: 'absolute',
                        bottom: '20px',
                        right: '7px',
                        zIndex: 50,
                        color: 'white',
                        textAlign: 'center',
                        padding: '.8em 1em'
                      }}>
                        <p style={{ fontSize: '.7em' }}>FROM LOMBOK</p>
                        <span style={{ fontSize: '.9em', fontWeight: 'bold' }}>-</span>
                        <p style={{ fontSize: '.7em' }}>WITH MUHAMMAD FAJAR ALAMSYAH</p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6" style={{ padding: '10px 20px' }}>
                <div style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: '10px',
                  position: 'relative'
                }}>
                  <a href="http://elearning.pigijo.com/travel-assistance/reza-romi-yuzaryahya"><img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/reza-profile.png" style={{ width: '90%', borderRadius: '10px' }} /></a>
                  <div id="product-card">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/reza-product.jpg" style={{ width: '170px', height: '200px', borderRadius: '10px', position: 'absolute', bottom: '20px', right: '7px', zIndex: 10 }} />
                    <a href="http://bit.ly/tourbuilderreza">
                      <div style={{
                        width: '170px',
                        height: '200px',
                        position: 'absolute',
                        bottom: '20px',
                        right: '7px',
                        zIndex: 50,
                        color: 'white',
                        textAlign: 'center',
                        padding: '.8em 1em'
                      }}>
                        <p style={{ fontSize: '.7em' }}>FROM JAKARTA</p>
                        <span style={{ fontSize: '.9em', fontWeight: 'bold' }}>Mount Gede Pangrango Adventure</span>
                        <p style={{ fontSize: '.7em' }}>WITH REZA ROMI YUZARYAHYA</p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6" style={{ padding: '10px 20px' }}>
                <div style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: '10px',
                  position: 'relative'
                }}>
                  <a href="http://elearning.pigijo.com/travel-assistance/fadhila"><img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/fadhila-profile.png" style={{ width: '90%', borderRadius: '10px' }} /></a>
                  <div id="product-card">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/fadhila-product.jpg" style={{ width: '170px', height: '200px', borderRadius: '10px', position: 'absolute', bottom: '20px', right: '7px', zIndex: 10 }} />
                    <a href="http://bit.ly/tourbuilderfadhilabandung">
                      <div style={{
                        width: '170px',
                        height: '200px',
                        position: 'absolute',
                        bottom: '20px',
                        right: '7px',
                        zIndex: 50,
                        color: 'white',
                        textAlign: 'center',
                        padding: '.8em 1em'
                      }}>
                        <p style={{ fontSize: '.7em' }}>FROM LAMPUNG</p>
                        <span style={{ fontSize: '.9em', fontWeight: 'bold' }}>Trip around Bandung 3D2N</span>
                        <p style={{ fontSize: '.7em' }}>WITH FADHILA</p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6" style={{ padding: '10px 20px' }}>
                <div style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: '10px',
                  position: 'relative'
                }}>
                  <a href="http://elearning.pigijo.com/travel-assistance/bayu-adi-nugroho"><img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/bayu-profile.png" style={{ width: '90%', borderRadius: '10px' }} /></a>
                  <div id="product-card">
                    <img src="https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/travel-assistance/bayu-product.jpg" style={{ width: '170px', height: '200px', borderRadius: '10px', position: 'absolute', bottom: '20px', right: '7px', zIndex: 10 }} />
                    <a href="http://bit.ly/tourbuilderbayu">
                      <div style={{
                        width: '170px',
                        height: '200px',
                        position: 'absolute',
                        bottom: '20px',
                        right: '7px',
                        zIndex: 50,
                        color: 'white',
                        textAlign: 'center',
                        padding: '.8em 1em'
                      }}>
                        <p style={{ fontSize: '.7em' }}>FROM BENGKULU</p>
                        <span style={{ fontSize: '.9em', fontWeight: 'bold' }}>-</span>
                        <p style={{ fontSize: '.7em' }}>WITH BAYU ADI NUGROHO</p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

            </div>
          </div>
        }

        <style jsx>{`
                .plan-item{
                  box-shadow: none;
                }
                
                .itech-card h3{
                  font: 18px Helvetica;
                  font-weight: bold;
                  margin: 4px 0;
                }

                .itech-card ul{
                  list-style: none;
                  padding-left: 0;
                }

                .text-city{
                  font: 12px Helvetica;
                  font-weight: bold;
                }

                .box-img-top{
                  background: none;
                  font: 12px helvetica;
                  font-weight: bold;
                  text-transform: uppercase;
                  color: #484848;
                  margin-top: 10px;
                  white-space: nowrap;
                  overflow: hidden;
                  text-overflow: ellipsis;
                }

                .plan-item{
                  margin-bottom: 40px;
                  min-height: 410px;
                }

                .price .text-primary, .price .text-person{
                  color: #4C4C4C;
                  font-size: 16px;
                }

                .text-dark-gray{
                  color: #484848;
                }

                .text-gray{
                  color: #7B7B7B;
                }

                .text-info{
                  color: #7B7B7B;
                  font: 13px Helvetica;
                }

                .text-min-small{
                  font: 13px Helvetica;
                  color: #7B7B7B;
                }

                .btn-o.btn-primary{
                  color: #787878;
                  background-color: transparent;
                  box-shadow: rgba(0, 0, 0, 0.12) 0px 2px 16px !important;
                  border: none;
                  border-radius: 2px;
                }

                .btn-o.btn-primary:hover{
                  border: none;
                }

                .see-all{
                  color: #008489;
                  font-weight: bold;
                }

                .cat-header p{
                  font-size: 15px;
                  color: #797979;
                }

                .main-title h1{
                  margin-bottom: 0px;
                }

                .text-label{
                  color: #787878;
                }

                #product-card img{
                  filter: brightness(50%);
                }

                @media (max-width: 767px) {
                  .itech-card h3{
                    font: 15px Helvetica;
                    font-weight: bold;
                    margin: 4px 0;
                  }

                  .plan-item{
                    margin-bottom: 20px;
                    min-height: 260px;
                  }

                  .content-wrap .container-fluid{
                    padding: 0 1em;
                  }
                }

                @media (max-width: 1198px) {
                  #product-card{
                     display: none;
                  }
                }
            `}</style>
      </div>
    </section >
  );
};
export default TaCategory;