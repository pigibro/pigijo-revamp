import React, { Component } from 'react'

export default class BannersContainer extends Component {
    render() {
        return (
            <div>
                <div className='container-fluid hidden-xs' style={{'padding': '0 6em'}}>
                    <div className="banners-container">
                    <div className="banner1">
                        <a href='#'>
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/banner/newBanner/banner_bromo.jpg' className='img-banner1' />
                        </a>
                    </div>
                    <div className="banner2">
                        <div>
                        <a href='/road-trip'>
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/banner/newBanner/challange.jpg' width="100%" className='img-banner2' />
                        </a>
                        </div>

                        <div className='banner3'>
                        <a href='https://play.google.com/store/apps/details?id=app.pigijo.com&hl=en'>
                            <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/banner/newBanner/download.jpg' width="100%" className='img-banner3' />
                        </a>
                        </div>
                        <div className='banner4'>
                            <a href='/how-to-use'>
                                <img src='https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/banner/newBanner/howtouse.jpg' width="100%" className='img-banner4' />
                            </a>
                        </div>
                    </div>
                    </div>
                </div>
                <style jsx>{`
                    .banner-container{
                        margin-top: 10px;
                      }
                    
                      .banners-container{
                        z-index: -2;
                      }
                      
                      .banner1{
                        margin-right: 10px;
                        float: left;
                      }
                      
                      .img-banner1{
                        width: 900px;
                        height: 582px;
                        object-fit: cover;
                        
                      }
                      
                      .img-banner2{
                        width: 590px;
                        height: 282px;
                        object-fit: cover;
                      }
                      
                      .banner3, .banner4{
                        margin-top: 10px;
                        float: left;
                      }
                      
                      .banner3{
                        margin-right: 10px;
                      }
                      
                      .img-banner3, .img-banner4{
                        width: 290px;
                        height: 290px;
                        object-fit: cover;
                        
                      }

                      @media (min-width: 1200px){
                        .img-banner1{
                          width: 740px;
                          height: 500px;
                          object-fit: cover;
                          
                        }

                        .img-banner2{
                          width: 520px;
                          height: 240px;
                          object-fit: cover;
                        }

                        .img-banner3, .img-banner4{
                          width: 255px;
                          height: 250px;
                          object-fit: cover;
                          
                        }
                      }
                `}</style>

            </div>
        )
    }
}  