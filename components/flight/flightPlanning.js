const FlightPlanning = () => (
  <div className='panel panel-bordered panel-primary sr-btm mt2'>
    <div className='panel-body'>
      <div className='row flex-row flex-center'>
        <div className='col-lg-6 col-md-6 col-sm-6'>
          <div className='text-wrapper'>
            <h4 className='text-title'>
            Flight
              <br />
            in
            Jakarta
            </h4>
            Find Flight Ticket in Jakarta
          </div>
        </div>
        <div className='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-0'>
          <a
            className='btn btn-sm btn-block btn-primary'
            href=''
          >
          Find Flight
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default FlightPlanning;