import React from 'react';
import { Col, Panel, PanelGroup, DropdownButton, ButtonToolbar, MenuItem } from "react-bootstrap";
import numberWithComma from '../../utils/numberWithComma';
import moment from 'moment';

class FlightList extends React.Component{

  render () {
    const {index, cabinType, chooseFlight, detail_origin, detail_destination, dateDeparture, dateReturn, departure_flight, adult, child, infant, activeKey, handleSelect, sortFlight, sortBy} = this.props;
    return (
      <div key={index}>
        <section className="check-flight" >
          <div className="col-md-1 check-flight-1">
            <p className="check-flight-number">{index+1}</p>
          </div>
          <div className="col-md-11 check-flight-2">
            <div className='row'>
              <div className='col-md-6'>
                <p className="check-flight-title">{index === 0 ? "Departure Flight" : "Return Flight"}</p>
                <p className="check-flight-rute">{index === 0 ? detail_origin.CityName : detail_destination.CityName} ({index === 0 ? detail_origin.Code : detail_destination.Code}) - {index === 1 ? detail_origin.CityName : detail_destination.CityName} ({index === 1 ? detail_origin.Code : detail_destination.Code})</p>
                <p className="check-flight-date">{moment(index === 0 ? dateDeparture : dateReturn).format('LL')} - {adult} Adult, {child} Child, {infant} Infant</p>
              </div>
              <div className='col-md-6' style={{display: 'flex', justifyContent: 'flex-end'}}>
                <section className="sort-section">
                  <ButtonToolbar className="pull-right">
                    <DropdownButton
                      bsStyle="default"
                      title="Sort By"
                      noCaret
                      id="dropdown-no-caret sort-btn"
                      className="btn btn-primary btn-sm btn-flight-search"
                    >
                      {
                        sortBy.map((sort, idx) => (
                          <MenuItem className='try' onSelect={() => {sortFlight(sort, index)}} key={idx} eventKey={sort.key} active={sort.active}>{sort.label}</MenuItem>
                        ))
                      }
                    </DropdownButton>
                  </ButtonToolbar>
                  <div className="clearfix"></div>
                </section>
              </div>
            </div>

          </div>
        </section>
    
        
    
        <div className="panel no-margin-bottom list-flight">
          {
            departure_flight.length > 0 ? (
              departure_flight.map((flight, idx) => (
                <div className="panel-body margin-bottom-1 shadow" key={idx}>
                  <div className="col-md-12">
                    {
                      flight.IsConnecting ? (
                        flight.MultiAirline ? (
                          flight.ConnectingFlights.map((connectFlight, id) => (
                            <div className="col-md-1 col-xs-2 flight-pos" key={id}>
                              <img src={connectFlight.AirlineImageUrl} className="img-logo-flight" />
                            </div>
                          ))
                        ) :  (
                          <div className="col-md-1 col-xs-2 flight-pos">
                            <img src={flight.ConnectingFlights[0].AirlineImageUrl} className="img-logo-flight" />
                          </div>
                        )
                        
                      ) : (
                        <div className="col-md-1 col-xs-2 flight-pos">
                          <img src={flight.AirlineImageUrl} className="img-logo-flight" />
                        </div>
                      )
                    }
                    
                    <div className="col-md-9 col-xs-10 flight-pos">
                      <p className="plane-flight">{flight.IsConnecting ? ( flight.MultiAirline ? 'Multi Airline' :  flight.ConnectingFlights[0].AirlineName) : flight.AirlineName }</p>
                    </div>
                  </div>
                  <div className="col-md-9 col-xs-12 flight-pad">
                    <div className="col-md-4 col-xs-4">
                      <div className="col-md-12 flight-cen">
                        <span className="plane-time">{flight.DepartDate} {flight.DepartTime}</span>
                      </div>
                      <div className="col-md-12 flight-cen">
                        <span className="plane-lok">{index === 0 ? detail_origin.CityName : detail_destination.CityName} ({index === 0 ? detail_origin.Code : detail_destination.Code})</span>
                      </div>
                    </div>
                    <div className="col-md-4 col-xs-4">
                      <div className="col-md-12 flight-cen">
                        <span className="plane-time">{flight.ArriveDate} {flight.ArriveTime}</span>
                      </div>
                      <div className="col-md-12 flight-cen">
                        <span className="plane-lok">{index === 1 ? detail_origin.CityName : detail_destination.CityName} ({index === 1 ? detail_origin.Code : detail_destination.Code})</span>
                      </div>
                    </div>
                    <div className="col-md-4 col-xs-4">
                      <div className="col-md-12 flight-cen">
                        <span className="plane-time">{flight.Duration}</span>
                      </div>
                      <div className="col-md-12 flight-cen">
                        <span className="plane-lok">{flight.IsConnecting ? 'Transit' : 'Direct'}</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3 col-xs-12 flight-padd">
                    <div className="col-md-12 col-xs-6">
                      <span className="plane-price">Rp {numberWithComma(flight.TotalFare)}</span><span>/pax</span>
                    </div>
                    <div className="col-md-12 col-xs-6 flight-buy-cen" onClick={() => chooseFlight(flight, index)}>
                      <a className="btn btn-primary btn-sm btn-flight-buy">Choose</a>
                    </div>
                  </div>
                  <div className="col-md-12 col-xs-12">
                    <div className="col-md-12">
                      <PanelGroup
                        accordion
                        id="accordion-controlled-example"
                        activeKey={activeKey}
                        onSelect={() => handleSelect(flight.Id)}
                        className="flight-accord-panel"
                      >
                        <Panel eventKey={flight.Id}>
                          <Panel.Heading className="flight-accord" >
                            <Panel.Title toggle className="flight-accord-title" >
                              Flight Details
                                </Panel.Title>
                          </Panel.Heading>
                          <Panel.Body collapsible className="flight-accord-body">
                            {
                              flight.IsConnecting ? (
                                flight.ConnectingFlights.map((flights, idx) => (
                                  <div key={idx}>
                                    <Col sm={4} className="flight-body-padd">
                                      <p className="flight-plane-type">{flights.AirlineName} - {flights.Number}</p>
                                      <p className="flight-cabin-type">{cabinType}</p>
                                    </Col>
                                    <Col sm={8}>
                                      <ul className="list-unstyled list-activities">
                                        <li className="review-explore">
                                          <div className="row no-gutter">
                                            <div className="col-lg-4 col-md-4 col-sm-4">
                                              <div className="flight-time">{flights.DepartDate} {flights.DepartTime}</div>
                                              <div className="flight-date">{moment(flights.DepartDate).format('ll')}</div>
                                            </div>
                                            {
                                              flights.detail_origin || flights.detail_destination ? (
                                                <div className="col-lg-8 col-md-8 col-sm-8">
                                                  <div className="flight-loc">{index === 0 ? flights.detail_origin.CityName : flights.detail_destination.CityName} ({index === 0 ? flights.detail_origin.Code : flights.detail_destination.Code})</div>
                                                  <div className="flight-airport">{flights.detail_origin.AirportName}</div>
                                                </div>
                                              ) : null
                                            }
                                            
                                          </div>
                                        </li>
                                        <li className="review-explore">
                                          <div className="row no-gutter">
                                            <div className="col-lg-4 col-md-4 col-sm-4">
                                              <div className="flight-time">{flights.ArriveDate} {flights.ArriveTime}</div>
                                              <div className="flight-date">{moment(flights.ArriveDate).format('ll')}</div>
                                            </div>
                                            {
                                              flights.detail_origin || flights.detail_destination ? (
                                                <div className="col-lg-8 col-md-8 col-sm-8">
                                                  <div className="flight-loc">{index === 1 ? flights.detail_origin.CityName : flights.detail_destination.CityName} ({index === 1 ? flights.detail_origin.Code : flights.detail_destination.Code})</div>
                                                  <div className="flight-airport">{flights.detail_destination.AirportName} </div>
                                                </div>
                                              ) : null
                                            }
                                            
                                          </div>
                                        </li>
                                      </ul>
                                    </Col>
                                  </div>
                                ))
                              ) : (
                                <div>
                                  <Col sm={4} className="flight-body-padd">
                                    <p className="flight-plane-type">{flight.AirlineName} - {flight.Number}</p>
                                    <p className="flight-cabin-type">{cabinType}</p>
                                  </Col>
                                  <Col sm={8}>
                                    <ul className="list-unstyled list-activities">
                                      <li className="review-explore">
                                        <div className="row no-gutter">
                                          <div className="col-lg-4 col-md-4 col-sm-4">
                                            <div className="flight-time">{flight.DepartDate} {flight.DepartTime}</div>
                                            <div className="flight-date">{moment(index === 0 ? dateDeparture : dateReturn).format('ll')}</div>
                                          </div>
                                          <div className="col-lg-8 col-md-8 col-sm-8">
                                            <div className="flight-loc">{index === 0 ? detail_origin.CityName : detail_destination.CityName} ({index === 0 ? detail_origin.Code : detail_destination.Code})</div>
                                            <div className="flight-airport">{detail_origin.AirportName}</div>
                                          </div>
                                        </div>
                                      </li>
                                      <li className="review-explore">
                                        <div className="row no-gutter">
                                          <div className="col-lg-4 col-md-4 col-sm-4">
                                            <div className="flight-time">{flight.ArriveDate} {flight.ArriveTime}</div>
                                            <div className="flight-date">{moment(index === 0 ? dateDeparture : dateReturn).format('ll')}</div>
                                          </div>
                                          <div className="col-lg-8 col-md-8 col-sm-8">
                                            <div className="flight-loc">{index === 1 ? detail_origin.CityName : detail_destination.CityName} ({index === 1 ? detail_origin.Code : detail_destination.Code})</div>
                                            <div className="flight-airport">{detail_destination.AirportName} </div>
                                          </div>
                                        </div>
                                      </li>
                                    </ul>
                                  </Col>
                                </div>
                              )
                            }
                            
                            
                          </Panel.Body>
                        </Panel>
                      </PanelGroup>
                    </div>
                  </div>
                </div>
              ))
            ) : null
          }
          
        </div>
      </div>
    )
  }
}

export default FlightList