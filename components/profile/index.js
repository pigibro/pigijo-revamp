import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import Sidebar from "./_component/sidebar"
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';
import { urlImage } from '../../utils/helpers'
import { get, isEmpty } from 'lodash'
import { sendEditProfile, sendNewPicture } from '../../stores/actions';
import Changepass from './src/changePass';
class Test extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isUpdate: false,
            firstname: '',
            birth: null,
            focusedInput: false,
            lastname: '',
            phone: '',
            gender: '',
            salutation: '',
            isPassOpen: false,
            image: null,
            errorImage: '',
        }
    }

    componentDidMount() {        
        this.setState({
            firstname: this.props.profile.firstname,
            lastname: this.props.profile.lastname,
            email: this.props.profile.email,
            phone: this.props.profile.phone,
            gender: this.props.profile.gender,
            salutation: this.props.profile.salutation,
            birth: this.props.profile.bod !== null ? moment(this.props.profile.bod) : null
        })
    }

    togglePassword=(e)=>{
        e.preventDefault();
        this.setState({ isPassOpen: !this.state.isPassOpen })
    }
    toggleProfile = (e) => {
        e.preventDefault()
        this.setState({ isUpdate: !this.state.isUpdate })
    }
    handleOnChange = (e) => {
        let name = e.target.name
        this.setState({ [name]: e.target.value })
    }

    handleOnSubmit=(e)=>{
        e.preventDefault();
        const {firstname, lastname, phone, salutation, birth, image} = this.state
        const {token} = this.props
        let param={
            data: {
              'gender': salutation,
              'firstname': firstname,
              'lastname': lastname,
              'phone':phone,
              'bod':birth !== null?moment(birth).format('YYYY-MM-DD'):moment().format('YYYY-MM-DD')
            },
            headers: {
              'Guest-Token':token
            }
          }
        this.props.dispatch(sendEditProfile(param))
        if(this.state.image !== null){
            this.props.dispatch(sendNewPicture(image))
        }
    }

    handleChangeImage =(evt) =>{
        const self = this;
        const reader = new FileReader();
        const file = evt.target.files[0];
        const fileTypes = ['jpg', 'jpeg', 'png']; 

        if (evt.target.files && file) {
            const extension = evt.target.files[0].name.split('.').pop().toLowerCase();
            const isSuccess = fileTypes.indexOf(extension) > -1;  
            if (isSuccess) { 
                if (file.size <= 5000000) {
                    reader.onload = function (upload) {
                        self.setState({
                            image: upload.target.result,
                            errorImage: null
                        });
                    };
                    reader.readAsDataURL(file);
                }else{
                    this.setState({
                        errorImage: "File size must not exceed 5 mb"
                    })
                }
            }
            else { 
                this.setState({
                    errorImage: "File type not allowed. File type to be must *.jpg, *.jpeg, *.png"
                })
            }
        }
    }

    onDateChange = (birth) => { this.setState(() => ({ birth: birth })) };

    onFocusChange = ({ focused }) => { this.setState(() => ({ focusedInput: focused })) };

    dropDown = ({ month, onMonthSelect, onYearSelect }) => {
        const styles = {
            border: '1px solid #e3e3e3',
            background: 'none',
            borderRadius: '3px'
        }

        let defYear = []
        if (defYear.length === 0) {
            for (let i = 0; i < 90; i++) {
                defYear.push(i)
            }
        }
        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div>
                    <select style={styles} value={month.month()} onChange={(e) => onMonthSelect(month, e.target.value)}>
                        {moment.months().map((label, value) => (
                            <option key={value} value={value}>{label}</option>
                        ))}
                    </select>
                </div>
                <div>
                    <select style={styles} value={month.year()} onChange={(e) => onYearSelect(month, e.target.value)}>
                        {
                            defYear.map((val, id) => (
                                <option key={id} value={moment().year() - val}>{moment().year() - val}</option>
                            ))
                        }
                    </select>
                </div>
            </div>
        )
    }

    render() {
        const { birth, isUpdate, salutation, firstname, focusedInput, lastname, email, phone , isPassOpen} = this.state
        const { profile, token, path } = this.props
        return (
            <section className="content-wrap">
                <div className="container">
                    <div className="row main-dashboard">
                        <Sidebar path={path} />
                        <div className="col-lg-9 col-md-9 col-sm-12 profile-content">
                            <div className='row'>

                                <div className={isUpdate ? "panel hide" : "panel "}>
                                    <div className="panel-heading">
                                        <h4 className="text-title">Edit Profile</h4>
                                    </div>
                                    <div className="panel-body">
                                        <div className="row">
                                            <div className="col-lg-7 col-md-7 col-sm-8">
                                                <div className="text-wrapper">
                                                    Edit your profile when you feel like to.
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4">
                                                {/* <button className="btn btn-block btn-o btn-primary">Change Profile</button> */}
                                                <a className="btn btn-block btn-o btn-primary" href="" onClick={this.toggleProfile}>Change Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>{/* panel */}
                                <div className={isUpdate ? "panel panel-profile" : "panel panel-profile hide"}>
                                    <div className="panel-body">
                                        <div className="row">
                                            <div className="col-lg-4 col-md-4 ">
                                                <div className="user-name" style={{backgroundColor: '#f5f7fa',border:'1px solid lightgrey', borderRadius:'5px', padding:'15px'}}>
                                                    <div className="change-picture" align="center">
                                                        <input type="file" id="upload-pic" ref="file" name="file"
                                                            className="upload-file"
                                                            onChange={this.handleChangeImage}
                                                            encType="multipart/form-data" />
                                                        {
                                                            !isEmpty(profile) ?
                                                                profile.profile_picture === null ? (
                                                                    <img src="/static/images/img01.jpg" alt="profile-image" width='100%' className="img-rounded" />
                                                                ) : (
                                                                        <img src={urlImage(profile_picture_path + "/" + profile_picture)} alt="profile-image" style={{ maxHeight: '250px', maxWidth: '250px' }} className="img-rounded" />
                                                                    ) : (
                                                                    <img src="/static/images/team1.jpg" alt="profile-image" width='100%' className="img-rounded" />
                                                                )

                                                        }
                                                        <div className='panel-body'>
                                                            <label htmlFor="upload-pic" className="btn btn-block btn-o btn-primary btn-sm">Changes Photo</label>
                                                        </div>
                                                        
                                                        <div className='panel-body' style={{fontSize:'11px'}}>
                                                            <p className='text-wrapper' align='left'>File size must not exceed 5 mb</p>
                                                            <p className='text-wrapper' align='left'>File type to be must *.jpg, *.jpeg, *.png</p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-8">

                                                <form className='form-horizontal'>
                                                    <div className="form-group">
                                                        <label htmlFor="salutation" className="col-sm-2 text-label control-label ">Salutation</label>
                                                        <div className="col-sm-10">
                                                            <select onChange={this.handleOnChange} className="form-control" value={salutation} name="salutation" >
                                                                <option value="Mr" >Mr</option>
                                                                <option value="Mrs" >Mrs</option>
                                                                <option value="Ms" >Miss</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="firstname" className="col-sm-2 text-label control-label">Firstname</label>
                                                        <div className="col-sm-10">
                                                            <input onChange={this.handleOnChange} className="form-control" id='firstname' type="text" name="firstname" required value={firstname || ''} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="inputPassword" className="col-sm-2 text-label control-label">Lastname</label>
                                                        <div className="col-sm-10">
                                                            <input onChange={this.handleOnChange} className="form-control" type="text" name="lastname" required value={lastname || ''} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="email" className="col-sm-2 text-label control-label">Email</label>
                                                        <div className="col-sm-10">
                                                            <input onChange={this.handleOnChange} className="form-control" type="email" pattern='[^@]+@[^@]+\.[a-zA-Z]{2,6}' value={email || ''} disabled />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="dob" className="col-sm-2 text-label control-label">Birthdate</label>
                                                        <div className="col-sm-10" align='left'>
                                                            <SingleDatePicker
                                                                renderMonthElement={this.dropDown}
                                                                small block
                                                                date={birth}
                                                                onDateChange={this.onDateChange}
                                                                focused={focusedInput}
                                                                onFocusChange={this.onFocusChange}
                                                                numberOfMonths={1}
                                                                isOutsideRange={() => false}
                                                                displayFormat="DD-MM-YYYY"
                                                                placeholder='dd-mm-yyyy'
                                                                id="BOD"
                                                                withPortal
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="phone" className="col-sm-2 text-label control-label">Phone</label>
                                                        <div className="col-sm-10">
                                                            <input onChange={this.handleOnChange} className="form-control" type="number" name="phone" required value={phone || ''} />
                                                        </div>
                                                    </div>

                                                    <div className="col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6">
                                                        <div className="row mt2">
                                                            <div className="col-lg-6 col-md-6 col-sm-6">
                                                                <button className="btn btn-block btn-o btn-primary" onClick={this.toggleProfile} >Cancel</button>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-6">
                                                                <button className="btn btn-block btn-primary" onClick={this.handleOnSubmit}>Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="row">
                                <div className="panel ">
                                    <div className="panel-heading">
                                        <h4 className="text-title">Change Password</h4>
                                    </div>
                                    <div className="panel-body">
                                        <div className="row">
                                            <div className="col-lg-7 col-md-7 col-sm-8">
                                                <div className="text-wrapper">
                                                    Change your password wisely.
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4">
                                                <button className="btn btn-block btn-o btn-primary" onClick={this.togglePassword}>Change Password</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>{/* panel */}
                                {
                                    isPassOpen ? (
                                        <Changepass token={token} cancel={this.togglePassword} />
                                    ):""
                                }

                            </div>{/* container */}
                        </div>{/* profile-content */}
                    </div>
                </div>{/* container */}
            </section>

        )
    }
}
// const mapStateToProps = (state) =>{
//     return {
//         isChange: state.editReducer.isChange,
//         data: state.editReducer.data
//     }
// }

// export default connect(mapStateToProps)(Content)

const mapStateToProps = state => ({
    profile: state.auth.profile,
    token:state.auth.token
})

export default connect(mapStateToProps)(Test);