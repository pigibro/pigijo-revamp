import React, { Component } from "react";
import { connect } from "react-redux";
import Sidebar from "../_component/sidebar";
import { compose } from "redux";
import TicketCard from "./ticketCard";
import TicketCardRentCar from "./ticketCardRentCar";
import TicketCardFlight from "./ticketCardFlight";
import TicketCartHotel from "./ticketCartHotel";
import {
	getBookTourList,
	getBookAccList,
	getBookRentCarList,
	getBookFlight,
} from "../../../stores/actions/eticketActions";
import Loader from "../../loader";
import Pagination from "react-js-pagination";
import { FormControl, FormGroup, InputGroup, Glyphicon } from "react-bootstrap";
import { isEmpty } from "lodash";
import { IMAGE_URL } from "../../../stores/actionTypes";

class Content extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isUpdate: false,
			page: 1,
			name_tab: "",
			numb: "",
			sort: "",
			qsort: "order_by=date&order_type=desc",
		};
	}

	handleToggle = (e) => {
		e.preventDefault();
		this.setState({
			isUpdate: !this.state.isUpdate,
		});
	};
	getSomeData = (query) => {
		const { name_tab } = this.state;
		const { token } = this.props;

		let qDefault = `?q=${this.state.numb}&${this.state.qsort}&page=${this.state.page}&per_page=10`;
		if (name_tab === "car") {
			return this.props.dispatch(getBookRentCarList(query, token));
			// }
		} else if (name_tab === "hotel") {
			return this.props.dispatch(getBookAccList(query, token));
			// }
		} else if (name_tab === "tour") {
			return this.props.dispatch(getBookTourList(query, token));
			// }
		} else if (name_tab === "flight") {
			return this.props.dispatch(getBookFlight(query, token));
			// }
		} else if (this.state.numb === "" && this.state.sort === "") {
			return this.props.dispatch(getBookTourList(qDefault, token));
			// }
		} else {
			return this.props.dispatch(getBookTourList(qDefault, token));
			// }
		}
	};

	handleSearch = (e) => {
		e.preventDefault();
		this.setState({ numb: e.target.value });
	};

	searchBooking = (e) => {
		e.preventDefault();

		let querySearch = `?q=${this.state.numb}`;
		let query = `?page=${this.state.page}&per_page=10`;
		if (this.state.name_tab === "") {
			this.setState({ name_tab: "tour" });
		}
		if (this.state.numb !== "") {
			setTimeout(() => {
				this.getSomeData(querySearch);
			}, 1000);
		} else {
			this.getSomeData(query);
		}
		this.setState({ sort: "" });
	};

	handlePageChange = (pageNumber) => {
		let queryStr = `page=${this.state.page}&per_page=10`;
		this.setState({ page: pageNumber }, () => {
			this.getSomeData(`?${this.state.qSort}&${queryStr}`);
		});
	};

	resetPage = (e, name_tab) => {
		e.preventDefault();
		let query = `?page=1&per_page=10`;
		this.setState({ page: 1, name_tab: name_tab, numb: "", sort: "", qSort: "order_by=date&order_type=desc" }, () => {
			return this.getSomeData(query);
		});
	};

	onChangeSort = (e) => {
		// e.preventDefault();
		this.setState({ sort: e.target.value });
		let desc = "?q=&order_by=date&order_type=desc";
		let asc = "?q=&order_by=date&order_type=asc";
		setTimeout(() => {
			if (this.state.sort === "latest") {
				this.getSomeData(desc + `&page=${this.state.page}&per_page=10`);
				this.setState({ qSort: desc });
			} else if (this.state.sort === "oldest") {
				this.getSomeData(asc + `&page=${this.state.page}&per_page=10`);
				this.setState({ qSort: asc });
			} else {
				return null;
			}
		}, 1000);
	};

	paging = (pagination) => {
		if (pagination !== null) {
			return (
				<nav className="pagination-nav">
					<span className="text-label">
						Displaying {pagination.displaying} of {pagination.total}
					</span>
					<Pagination
						activePage={this.state.page}
						pageCount={pagination.last_page}
						itemsCountPerPage={pagination.per_page}
						totalItemsCount={pagination.total}
						pageRangeDisplayed={10}
						onChange={this.handlePageChange}
					/>
				</nav>
			);
		} else {
			return <div />;
		}
	};

	sortAndSearch = () => {
		return (
			<div className="row sort_search">
				<div className="col-lg-3 col-md-3 col-sm-12 col-xs-6">
					<div className="select-person ">
						<select onChange={this.onChangeSort} value={this.state.sort} className="form-control">
							<option value="">Sort by</option>
							<option value="latest">Latest</option>
							<option value="oldest">Oldest</option>
						</select>
					</div>
				</div>
				<div className="col-lg-5 col-md-5  col-xs-6" />
				<div className="col-lg-4 col-md-4 col-sm-12 col-xs-6" align="right">
					<form onSubmit={this.searchBooking}>
						<FormGroup>
							<InputGroup>
								<FormControl
									bsSize="small"
									type="text"
									onChange={this.handleSearch}
									value={this.state.numb}
									placeholder="Search ..."
								/>
								<InputGroup.Addon>
									<a style={{ cursor: "pointer" }} onClick={this.searchBooking}>
										<Glyphicon glyph="search" />
									</a>
								</InputGroup.Addon>
							</InputGroup>
						</FormGroup>
					</form>
				</div>
			</div>
		);
	};
	alertEticket = () => {
		return (
			<div className="alert alert-bordered">
				<div className="text-wrapper">
					<h5 className="text-title"> No e-ticket found here.</h5>
					Start planning you itinerary and generate the booking via homepage. Your e-ticket for each booking will be
					found here.
				</div>
			</div>
		);
	};

	render() {
		return (
			<section className="content-wrap">
				<div className="container">
					<div className="row main-dashboard">
						<Sidebar path={this.props.path} />
						<div className="col-lg-9 col-md-9 col-sm-12 profile-content">
							<div className="container-fluid ">
								<nav style={{ fontSize: "0.9em" }} align="center" id="tab-event">
									<ul className="nav nav-tabs">
										<li className="active">
											<a
												href="#activity"
												onClick={(e) => this.resetPage(e, "tour")}
												data-toggle="tab"
												aria-expanded="true"
											>
												<img
													src="/static/icons/ic_ticket_24.png"
													className="icon"
													alt=""
													style={{ paddingRight: "1em" }}
												/>
												Activity
											</a>
										</li>
										{/* <li className><a href="#travel_bus" data-toggle="tab" aria-expanded="false"><img src="assets/images/ic_bus_24.png" className="icon" alt="" />Travel Bus</a></li> */}
										{/* <li className><a href="#tour_guide" data-toggle="tab" aria-expanded="false"><img src={ic_pin_24} className="icon" alt="" />Tour Guide</a></li> */}
										<li>
											<a
												href="#local_transport"
												onClick={(e) => this.resetPage(e, "car")}
												data-toggle="tab"
												aria-expanded="false"
											>
												<img src="/static/icons/ic_car.png" className="icon" alt="" style={{ paddingRight: "1em" }} />
												Car Rental
											</a>
										</li>
										<li>
											<a
												href="#accommodation"
												onClick={(e) => this.resetPage(e, "hotel")}
												data-toggle="tab"
												aria-expanded="false"
											>
												<img
													src="/static/icons/ic_bed_24.png"
													className="icon"
													alt=""
													style={{ paddingRight: "1em" }}
												/>
												Hotel
											</a>
										</li>
										<li>
											<a
												href="#flight"
												onClick={(e) => this.resetPage(e, "flight")}
												data-toggle="tab"
												aria-expanded="false"
											>
												<img src="/static/icons/ic_plane.png" className="icon" alt="" style={{ paddingRight: "1em" }} />
												Flight
											</a>
										</li>
									</ul>
								</nav>
								<div className="tab-content">
									<div className="tab-pane fade active in" id="activity">
										{this.props.isLoading ? (
											<div align="center">
												<Loader />
											</div>
										) : !isEmpty(this.props.tour) ? (
											<div>
												{this.sortAndSearch()}
												{this.props.tour.map((item, key) => (
													<div key={key}>
														<TicketCard
															tourName={item.tourName}
															start={item.start}
															end={item.end}
															startHours={item.startHours}
															endHours={item.endHours}
															person={item.numberOfPerson}
															location={item.cityName + ", " + item.provinceName}
															providerContactName={item.providerContactName}
															providerContactPhone={item.providerContactPhone}
                                                            bookNumb={item.booking_number}
                                                            url={IMAGE_URL}
														/>
													</div>
												))}
												<div>{this.paging(this.props.paging_tour)}</div>
											</div>
										) : (
											this.alertEticket()
										)}
									</div>
									<div className="tab-pane fade" id="local_transport">
										{this.props.isLoading ? (
											<div align="center">
												<Loader />
											</div>
										) : !isEmpty(this.props.rent_car) ? (
											<div>
												{this.sortAndSearch()}
												{this.props.rent_car.map((item, key) => (
													<div key={key}>
														<TicketCardRentCar
															tourName={item.vehicle_name}
															start={item.start_date}
															end={item.end_date}
															startHours={item.time_from}
															endHours={item.time_to}
															day={item.number_of_day}
															reservation_description={item.reservation_description}
                                                            bookNumb={item.booking_number}
                                                            url={IMAGE_URL}
														/>
													</div>
												))}
												<div>{this.paging(this.props.paging_car)}</div>
											</div>
										) : (
											this.alertEticket()
										)}
									</div>
									<div className="tab-pane fade" id="accommodation">
										{this.props.isLoading ? (
											<div align="center">
												<Loader />
											</div>
										) : !isEmpty(this.props.hotel) ? (
											<div>
												{this.sortAndSearch()}
												{this.props.hotel.map((item, key) => (
													<div key={key}>
														<TicketCartHotel
															hotelName={item.order_name}
															detailName={item.order_name_detail}
															start={item.detail.startdate}
															end={item.detail.enddate}
															person={item.detail.adult}
															night={item.detail.nights}
															room={item.detail.rooms}
                                                            bookNumb={item.booking_number}
                                                            url={IMAGE_URL}
														/>
													</div>
												))}
												<div>{this.paging(this.props.paging_hotel)}</div>
											</div>
										) : (
											this.alertEticket()
										)}
									</div>
									<div className="tab-pane fade" id="flight">
										{this.props.isLoading ? (
											<div align="center">
												<Loader />
											</div>
										) : !isEmpty(this.props.flight) ? (
											<div>
												{this.sortAndSearch()}
												{this.props.flight.map((item, key) => (
													<div key={key}>
														<TicketCardFlight
															flightName={item.provider_name}
															arrival_time={item.arrival_time}
															departure_time={item.departure_time}
															destination={item.destination}
															origin={item.origin}
															person={item.number_of_person}
															product_class={item.product_class}
															img={item.img}
															bookNumb={item.booking_number}
                                                            imgDefault={airplane}
                                                            url={IMAGE_URL}
														/>
													</div>
												))}
												<div>{this.paging(this.props.paging_flight)}</div>
											</div>
										) : (
											this.alertEticket()
										)}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}
const mapStateToProps = (state) => ({
	flight: state.eticket.flight,
	hotel: state.eticket.hotel,
	rent_car: state.eticket.rent_car,
	tour: state.eticket.tour,
	paging_flight: state.eticket.paging_flight,
	paging_hotel: state.eticket.paging_hotel,
	paging_tour: state.eticket.paging_tour,
	paging_car: state.eticket.paging_car,
	isLoading: state.eticket.isLoading,
});
export default compose(connect(mapStateToProps))(Content);
