import React from "react";
import moment from "moment";

const Flight = ({
	flightName,
	arrival_time,
	departure_time,
	destination,
	origin,
	person,
	product_class,
	img,
	imgDefault,
	bookNumb,
	url
}) => {
	return (
		<div className="panel overlayCard">
			<div className="panel-body">
				<div className="trip-destination">
					<div className="col-lg-12 col-md-12 col-sm-12">
						<div className="row">
							<div className="col-lg-3 col-md-3 col-sm-3">
								<img src={img !== undefined ? img : imgDefault} style={{ width: "70%", height: "70%" }} alt="img" />
							</div>
							<div className="col-lg-9 col-md-9 col-sm-9">
								<div className="panel-body panel-pl">
									<div className="flex-row flex-center">
										<div className="info">
											<div className="text-label" />
											<h4 className="text-title">{flightName} </h4>
											<h4 className="text-title">{product_class} </h4>
											<div className="text-label" />
										</div>
									</div>
									<div className="footer">
										<div className="row flex-row flex-end">
											<div className="col-lg-4 col-md-4 col-sm-4 col-xs-6">
												<div className="text-label">Departure</div>
												<div className="text-dark">{origin} </div>
												<div className="text-dark">{moment(departure_time).format("ddd, DD MMM YYYY")} &rarr;</div>
												<div className="text-dark">
													{moment(departure_time, "YYYY-MM-DD HH:mm:ss").format("HH:mm")}{" "}
												</div>
											</div>
											<div className="col-lg-4 col-md-4 col-sm-4 col-xs-6">
												<div className="text-label">Arrival</div>
												<div className="text-dark">{destination}</div>
												<div className="text-dark">{moment(arrival_time).format("ddd, DD MMM YYYY")}</div>
												<div className="text-dark">{moment(arrival_time, "YYYY-MM-DD HH:mm:ss").format("HH:mm")}</div>
											</div>
											<div className="col-lg-4 col-md-4 col-sm-4 col-xs-6">
												<div className="text-label">Total Person :{person}</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div className="row text-wrapper">
							<div className="col-lg-6 col-md-6 col-sm-6">
								<p>Booking Reference:</p>{" "}
								<p>
									<b>{bookNumb}</b>
								</p>
							</div>
							<div className="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2">
								<a
									className="btn btn-block btn-sm btn-primary"
									target="_blank"
									href={url + "pdf/issued-ticket/flight/" + bookNumb + ".pdf"}
									download
								>
									Download Ticket
								</a>
							</div>
						</div>
						{/* row */}
					</div>
				</div>
			</div>
		</div>
	);
};
export default Flight