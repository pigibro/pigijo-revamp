import React, { Component } from 'react'
import moment from 'moment'

export default class componentName extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isUpdate: false
        }
    }

    handleToggle=(e)=> {
        e.preventDefault()
        this.setState({
            isUpdate: !this.state.isUpdate
        })
    }
    render() {
        const { hotelName, detailName, start, end, person, bookNumb, night, url } = this.props
        return (
            <div className={moment(this.props.end).format("YYYY-MM-DD") < moment().format("YYYY-MM-DD") ? "panel overlayCard" : "panel "}>
                <div className="panel-heading">
                    <h4 className="text-title">{hotelName} ({detailName})</h4>
                </div>
                <div className="panel-body">
                    <ul className="list-unstyled list-bordered">
                        <li>
                            <div className="row">
                                <div className="col-lg-6 col-md-6 col-sm-6">
                                    <div className="text-label">Schedule</div>
                                    <div className="text-dark">{moment(start).format("ddd, DD MMM YYYY")} - {moment(end).format("ddd, DD MMM YYYY")}</div>
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-6">
                                    <div className="text-label">Total Person</div>
                                    <div className="text-dark">{person} person(s)</div>
                                </div>
                            </div>{/* row */}
                        </li>
                        <li>
                            <div className="row">
                                <div className="col-lg-6 col-md-6 col-sm-6">
                                    <div className="text-label">Night</div>
                                    <div className="text-dark">{night} night(s)</div>
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-6">
                                    <div className="text-label">Room</div>
                                    <div className="text-dark">{room} room(s)</div>
                                </div>
                            </div>{/* row */}
                        </li>
                        <li>
                            <div className="row flex-row flex-center">
                                <div className="col-lg-6 col-md-6 col-sm-6">
                                    Booking Reference: {bookNumb}
                                </div>
                                <div className="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2">
                                    <a className="btn btn-block btn-sm btn-primary" target='_blank' href={url + 'pdf/issued-ticket/accommodation/' + bookNumb + '.pdf'} download>Download Ticket</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}
