import React, { Component } from 'react';
import moment from 'moment';

export default class componentName extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isUpdate: false
        }
        this.handleToggle = this.handleToggle.bind(this)
    }
    handleToggle(e) {
        e.preventDefault()
        this.setState({
            isUpdate: !this.state.isUpdate
        })
    }
    render() {
        const {
            start,
            startHours,
            endHours,
            person,
            location,
            providerContactName,
            providerContactPhone,
            tourName,
            bookNumb,
            url
        } = this.props
        return (
            <div className={moment(this.props.end).format("YYYY-MM-DD") < moment().format("YYYY-MM-DD") ? "panel overlayCard" : "panel "}>
                <div className="panel-heading">
                    <h4 className="text-title">{tourName}</h4>
                </div>
                <div className="panel-body">
                    <ul className="list-unstyled list-bordered">
                        <li>
                            <div className="row">
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="text-label">Schedule</div>
                                    <div className="text-dark">{moment(start).format("ddd, DD MMM YYYY")}, {moment(startHours, "HH:mm:ss").format("HH:ss")} - {moment(endHours, "HH:mm:ss").format("HH:ss")}</div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="text-label">Total Person</div>
                                    <div className="text-dark">{person} person(s)</div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="text-label">Location</div>
                                    <div className="text-dark">{location}</div>
                                </div>
                            </div>{/* row */}
                        </li>
                        <li>
                            <p>Provider Contact</p>
                            <p className="text-sm">This is the activity provider contact whom you'll meet at the meeting point. The person in charge will reach you within 24 hours prior the schedule.</p>
                            <div className="row">
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="text-label">Full Name</div>
                                    <div className="text-dark">{providerContactName}</div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="text-label">Phone Number</div>
                                    <div className="text-dark">{providerContactPhone}</div>
                                </div>
                            </div>{/* row */}
                        </li>
                        <li>
                            <div className="row flex-row flex-center">
                                <div className="col-lg-6 col-md-6 col-sm-6">
                                    Booking Reference: {bookNumb}
                                </div>
                                <div className="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2">
                                    <a className="btn btn-block btn-sm btn-primary" target='_blank' href={url + 'pdf/issued-ticket/activity/' + bookNumb + '.pdf'} download>Download Ticket</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}
