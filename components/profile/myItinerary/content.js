import React, { Component } from "react";
import Itinerary from "./itinerary";
import Sidebar from "../_component/sidebar";
import Pagination from "react-js-pagination";
import { FormControl, FormGroup, InputGroup, Glyphicon } from "react-bootstrap";
import Loader from '../../loader'
import {getItinerary} from '../../../stores/actions/listItineraryAction'
import {isEmpty} from 'lodash'

class Content extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isUpdate: false,
			page: 1,
			search: "",
			sort: "latest",
			qsort: "order_by=date&order_type=desc",
		};
	}

	handlePageChange = (pageNumber) => {
		this.setState({ page: pageNumber }, () => {
			let query = "?q=" + this.state.search + "&" + this.state.qsort + "&page=" + this.state.page;
			this.props.dispatch(getItinerary(query));
		});
	};

	handleSearch = (e) => {
		e.preventDefault();
		this.setState({ search: e.target.value });
	};

	searchBooking = (e) => {
		e.preventDefault();
		let query = "?q=" + this.state.search + "&" + this.state.qsort;
		this.setState({ page: 1 }, () => {
			return this.props.dispatch(getItinerary(query));
		});
	};

	handleToggle = (e) => {
		e.preventDefault();
		this.setState({ isUpdate: !this.state.isUpdate });
	};

	onChangeSort = (e) => {
		e.preventDefault();
		this.setState({ sort: e.target.value });
		this.setState({ page: 1 });
		if (e.target.value === "latest") {
			this.setState({ qsort: "order_by=date&order_type=desc" });
		} else {
			this.setState({ qsort: "order_by=date&order_type=asc" });
		}
		setTimeout(() => {
			let query = "?q=" + this.state.search + "&" + this.state.qsort + "&page=" + this.state.page;
			this.props.dispatch(getItinerary(query));
		}, 1000);
	};

	sortAndSearch = () => {
		return (
			<div className="row sort_search">
				<div className="col-lg-3 col-md-3 col-sm-12 col-xs-6">
					<div className="select-person ">
						<select onChange={this.onChangeSort} defaultValue={this.state.sort} className="form-control">
							<option value="">Sort by</option>
							<option value="latest">Latest</option>
							<option value="oldest">Oldest</option>
						</select>
					</div>
				</div>
				<div className="col-lg-5 col-md-5  col-xs-6" />
				<div className="col-lg-4 col-md-4 col-sm-12 col-xs-6" align="right">
					<form onSubmit={this.searchBooking}>
						<FormGroup>
							<InputGroup>
								<FormControl
									bsSize="small"
									type="text"
									onChange={this.handleSearch}
									value={this.state.numb}
									placeholder="Search ..."
								/>
								<InputGroup.Addon>
									<a style={{ cursor: "pointer" }} onClick={this.searchBooking}>
										<Glyphicon glyph="search" />
									</a>
								</InputGroup.Addon>
							</InputGroup>
						</FormGroup>
					</form>
				</div>
			</div>
		);
	};

	render() {
		const { token, path, list, timeline, hotel, tour, rentcar, details, paging, isLoading } = this.props;

		return (
			<section className="content-wrap">
				<div className="container">
					<div className="row main-dashboard">
						<Sidebar handleToggle={this.handleToggle} isUpdate={this.state.isUpdate} path={path} />

						<div className="col-lg-9 col-md-9 col-sm-12 profile-content">
							<div className="container-fluid">
								<div className="alert alert-bordered">
									<img src="/static/images/ic_info_24.png" className="icon" alt="" />
									<div className="text-wrapper">
										You can find your activity bookings in <a href="#!">My Booking</a>. You can view all itinerary you
										have ever created here and continue planning the itinerary that you have not finished.
									</div>
								</div>
								{isLoading ? (
									<div align="center">
										<Loader />
									</div>
								) : (
									<div>
										{this.sortAndSearch()}
										<div className="list-itinerary">
											{list.map((item) => (
												<Itinerary
													key={item.planning_id}
													start={item.start_date}
													end={item.end_date}
													name={item.planning_name}
													status={item.status}
													id={item.planning_id}
													lastUpdate={item.lastUpdate}
													itineraryNumber
													person={item.person}
												/>
											))}
										</div>

										{!isEmpty(paging) && (
											<div>
												<nav className="pagination-nav">
													<span className="text-label">
														Displaying {isEmpty(paging) ? 0 : paging.displaying} of {paging === null ? 0 : paging.total}
													</span>
													<Pagination
														activePage={this.state.page}
														pageCount={paging.last_page}
														itemsCountPerPage={paging.per_page}
														totalItemsCount={paging.total}
														pageRangeDisplayed={10}
														onChange={this.handlePageChange}
													/>
												</nav>
											</div>
										)}
									</div>
								)}
							</div>
						</div>
						{/* profile-content */}
					</div>
				</div>
				{/* container */}
			</section>
		);
	}
}

export default Content;
