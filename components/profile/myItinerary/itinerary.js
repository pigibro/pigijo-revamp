import React, { Component } from 'react'
import moment from 'moment'
// import {Link} from 'react-router-dom'
import { hitungSelisih} from "../../../utils/helpers"
import { getDetailPlanItinerary } from "../../../stores/actions/listItineraryAction"
// import { Redirect } from 'react-router-dom'
// import history from '../../../../history';
import {connect} from 'react-redux'

class componentName extends Component {
    constructor(props){
        super(props)
        this.state = {
            isRedirect:false,
            id:null,
        }
        this.nextPlan = this.nextPlan.bind(this)
    }
    componentDidMount(){
        this.setState({continuePlan:false})
    }
    handleClickDetail=(e)=>(id)=>{
        e.preventDefault()
        this.setState({
            isRedirect: true,
            id
        })
    }
    async nextPlan (){
        let success = await this.props.dispatch(getDetailPlanItinerary(this.props.id));
        if(success !== false){
          history.push(`/planning-itinerary/${this.props.id}/step2`);
          window.location.reload()
        }

    }

    render() {
        if(this.state.isRedirect && this.state.id !== null){
            // return <Redirect to={'/profile/itinerary/' + this.state.id + '/detail/'} />
        }

        return (
            <div className="panel">
                <div className="panel-heading flex-row flex-center">
                    <p>{this.props.name} </p>
                    <span className="text-label mla">Last Updated: {moment(this.props.lastUpdate).format("DD MMM YYYY")}</span>
                </div>
                <div className="panel-body">
                    <div className="row flex-row flex-center">
                        {/* <div className="col-lg-3 col-md-3 col-sm-3">
                            <div className="text-label">Itinerary Number</div>
                            <div className="text-dark">{this.props.itineraryNumber}</div>
                        </div> */}
                        <div className="col-lg-6 col-md-6 col-sm-6">
                            <div className="text-label">Trip Date</div>
                            <div className="text-dark">{moment(this.props.start).format("DD MMM YYYY")} - {moment(this.props.end).format("DD MMM YYYY")} ({Math.floor(hitungSelisih(this.props.start,this.props.end))} days)</div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-4">
                            <div className="text-label">Number of Person</div>
                            <div className="text-dark">{this.props.person} person(s)</div>
                        </div>
                        <div className="col-lg-2 col-md-2 col-sm-2">
                        {
                            this.props.status !== "done" && this.props.status !=='expired'?(
                                <button className="btn btn-sm btn-block btn-primary" onClick={this.nextPlan} >Continue<br />Planning</button>
                            ):(
                                <a className="link" href="" onClick={(e)=>this.handleClickDetail(e)(this.props.id)}>View Details</a>
                            )
                        }
                        </div>
                    </div>
                </div>{/* panel-body */}
            </div>
        )
    }
}
export default connect()(componentName)
