import React, { Component } from 'react'
import moment from 'moment'
import NumberFormat from 'react-number-format';

// import { parseInt } from 'actions,10/_helper/general'

export default class componentName extends Component {
    render() {
        return (
            <div>
                <div className="panel">
                    <div className="panel-heading">
                        <h4 className="text-title">Booking Items</h4>
                    </div>
                    <div className="panel-body">
                        <ul className="list-unstyled list-bordered">
                            {
                                this.props.item.transaction_details.map((item,key)=>(
                                    <li key={key}>
                                        <div className="row flex-row flex-center">
                                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                                <div className="text-dark">{item.product_name}</div>
                                                <div className="text-sm">{moment(item.start_date).format("LLLL")} - {moment(item.end_date).format("LLLL")}</div>
                                            </div>
                                            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                <div className="text-dark">x {item.qty} {item.unit}</div>
                                            </div>
                                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                                <div className="price">
                                                    <div className="amount h5 total-priceBookitemsa" style={{textAlign: 'right'}}>
                                                    {
                                                        item.total_paid === "0.00" ? 'Free' : (
                                                            <NumberFormat value={parseInt(item.total_paid,10)} displayType={'text'} thousandSeparator={true} prefix={'Rp '} />
                                                        )
                                                    }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ))
                            }
                            
                            <li>
                                <div className="row flex-row flex-center">
                                    <div className="col-lg-5 col-md-5 col-sm-5 col-xs-6">
                                        Total Price
                                     </div>{/* col */}
                                    <div className="col-lg-4 col-lg-offset-3 col-md-4 col-md-offset-3 col-sm-4 col-sm-offset-3 col-xs-6">
                                        <div className="price">
                                            <div className="amount h5 total-priceBookitems" style={{textAlign: 'right'}}>
                                            {
                                                this.props.item.total_price === "0.00" ? 'Free' : (
                                                    <NumberFormat value={parseInt(this.props.item.total_price ,10)} displayType={'text'} thousandSeparator={true} prefix={'Rp '} />
                                                )
                                            }
                                            </div>

                                        </div>
                                    </div>{/* col */}
                                </div>{/* row */}
                            </li>
                            {
                                this.props.item.coupon_code !== null ? (
                                    <li style={{border: 0}}>
                                        <div className="row flex-row flex-center">
                                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-6">
                                                Discount ({this.props.item.coupon_code})
                                            </div>
                                            <div className="col-lg-4 col-lg-offset-3 col-md-4 col-md-offset-3 col-sm-4 col-sm-offset-3 col-xs-6">
                                                <div className="price">
                                                    <div className="amount h5 total-priceBothis.props.okitems" style={{textAlign: 'right'}}>
                                                        ( <NumberFormat value={parseInt(this.props.item.total_discount,10)} displayType={'text'} thousandSeparator={true} prefix={'Rp '} /> 
                                                        )
                                                    </div>
        
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ) : null
                            }
                        </ul>
                    </div>{/* panel-body */}
                    <div className="panel-footer">
                        <div className="row flex-row flex-center">
                            <div className="col-lg-5 col-md-5 col-sm-5">
                                <b>Total Amount to Pay</b>
                            </div>{/* col */}
                            <div className="col-lg-4 col-lg-offset-3 col-md-4 col-md-offset-3 col-sm-4 col-sm-offset-3">
                                <div className="price">
                                    <div className="amount h4 total-priceBookitems" style={{textAlign: 'right'}}>
                                    {
                                        this.props.allpaymenttotal === "0.00" ? 'Free' : (
                                            <NumberFormat value={parseInt(this.props.allpaymenttotal,10)} displayType={'text'} thousandSeparator={true} prefix={'Rp '} />
                                        )
                                    }
                                    </div>
                                </div>
                            </div>{/* col */}
                        </div>{/* row */}
                    </div>
                </div>{/* panel */}
            </div>

        )
    }
}
