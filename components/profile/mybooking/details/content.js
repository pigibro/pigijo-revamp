import React, { Component } from 'react'
// import {connect} from 'react-redux'
// import { withRouter } from 'react-router'
// import { Link } from 'react-router-dom'
import BookingDetail from './bookingDetails'
import BookingItem from './bookingItem'
import Sidebar from "../../_component/sidebar"
// import { getBookingDetail } from "actions/myBooking/listBooking.action"
// import back from "assets/images/ic_back_24.png"

export default class Content extends Component {
    
    render() {
        return (
            <section className="content-wrap">
                <div className="container">
                    <div className="row main-dashboard">

                        <Sidebar
                            path="/my-booking"

                        />

                        <div className="col-lg-9 col-md-9 col-sm-12 profile-content">
                            <div className="container-fluid">
                                <div className="heading-toolbar">
                                    <div className="toolbar-left">
                                        {/* <Link className="back" to="/profile/booking/"><img src={'back'} className="icon" alt="" />Back</Link> */}
                                    </div>
                                </div>
                                {
                                    this.props.details.id ? (
                                        <>
                                          <BookingDetail
                                                date={this.props.details.paid_at}
                                                bookNumb={this.props.details.order_number}
                                                fullname={this.props.details.contact_details.firstname + " "+this.props.details.contact_details.lastname}
                                                email={this.props.details.contact_details.email}
                                                phone={this.props.details.contact_details.phone}

                                                status={this.props.details.status_name}
                                                statusCode={this.props.details.status}
                                                price={this.props.details.total_paid}
                                                method={this.props.details.payment_method}
                                                timeLimit={this.props.details.time_limit}
                                                token={this.props.details.cart_token}
                                            />
                                            <BookingItem
                                                item = {this.props.details}
                                                allpaymenttotal={this.props.details.total_paid}
                                            />  
                                        </>
                                    ) : null
                                }
                                

                            </div>
                        </div>
                    </div>
                </div>{/* container */}
            </section>


        )
    }
}
