import React from "react";
import NumberFormat from "react-number-format";
import moment from "moment";
import getConfig from "next/config"
const { publicRuntimeConfig } = getConfig()

const { PAYMENT_URL } = publicRuntimeConfig

const BookingDetails = ({ date, bookNumb, fullname, email, phone, method, price, timeLimit,statusCode, status, token }) => {
	return (
		<div>
			<div className="booking-detail">
				<div className="panel">
					<div className="panel-heading Bokdetail-head">
						<h4 className="text-title">Booking Details</h4>
					</div>
					<div className="panel-body">
						<ul className="list-unstyled list-bordered">
							<li>
								<div className="row flex-row flex-center">
									<div className="col-lg-5 col-md-5 col-sm-5">
										<div className="text-label">Booking Date</div>
										<div className="text-dark">{moment(date).format("ddd, DD MMM YYYY, HH:mm")}</div>
									</div>
									{/* col */}
									<div className="col-lg-3 col-md-3 col-sm-3">
										<div className="text-label">Booking Number</div>
										<div className="text-dark">{bookNumb}</div>
									</div>
									{/* col */}
									<div className="col-lg-4 col-md-4 col-sm-4">
										<div className="text-label">Booked By</div>
										<div className="text-dark">
											{fullname} ({email})
										</div>
									</div>
									{/* col */}
								</div>
							</li>
							<li>
								<p>Contact Person</p>
								<div className="row flex-row flex-center">
									<div className="col-lg-5 col-md-5 col-sm-5">
										<div className="text-label">Email Address</div>
										<div className="text-dark">{email}</div>
									</div>
									{/* col */}
									<div className="col-lg-3 col-md-3 col-sm-3">
										<div className="text-label">Phone Number</div>
										<div className="text-dark">{phone}</div>
									</div>
									{/* col */}
									<div className="col-lg-4 col-md-4 col-sm-4">
										<div className="text-label">Full Name</div>
										<div className="text-dark">{fullname}</div>
									</div>
									{/* col */}
								</div>
							</li>
							<li>
								<p>Payment</p>
								<div className="row flex-row flex-center">
									<div className="col-lg-5 col-md-5 col-sm-5 col-xs-7">
										<div className="text-label">Payment Method</div>
										<div className="text-dark">{method === "" ? "-" : method}</div>
									</div>
									{/* col */}
									<div className="col-lg-3 col-md-3 col-sm-3 col-xs-5">
										<div className="text-label">Amount to Pay</div>
										<div className="text-dark">
											{
												price === "0.00" ? 'Free' : (
													<NumberFormat
														value={parseInt(price, 10)}
														displayType={"text"}
														thousandSeparator={true}
														prefix={"Rp "}
													/>
												)
											}
											
										</div>
									</div>
									{/* col */}
									<div className="col-lg-3 col-md-3 col-sm-3">
										<div className="text-label">Complete Payment Before</div>
										<div className="text-dark">{moment(timeLimit).format("DD MMMM YYYY, HH:mm")}</div>
									</div>
									{/* col */}
								</div>
							</li>
						</ul>
					</div>
					{/* panel-body */}
					<div className="panel-footer">
						<div className="row flex-row flex-center">
							<div className="col-lg-6 col-md-6 col-sm-6 ">Booking Status</div>
							<div className="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2">
								<div className="booking-status">
									{
										statusCode === 1 || statusCode === 4 ? (
											<>
											<img src="/static/images/ic_clock_24.png" className="icon" alt="" />
											{statusCode === 1 ? 'Awaiting Payment' : 'Payment Pending'}
											{statusCode === 1 ? (<a href={`${PAYMENT_URL}${token}`} className="link" style={{marginLeft: '12px'}}>
												Pay your bill
											</a>) : null}
											</>
										) : statusCode === 2 ? (
											<>
											<img src="/static/images/ic_success_24.png" className="icon" alt="" />
											{'Payment Accepted'}
											</>
										) : (
											<>
												<img src="/static/images/ic_cancel_24.png" className="icon" alt="" />
												{'Cancelled'}
											</>
										)
									}
								</div>
							</div>
						</div>
					</div>
					{status === "paid" ? (
						<div className="panel-body">
							<div className="row flex-row flex-center">
								<div className="col-lg-6 col-md-6 col-sm-6">
									<a href="#!" className="link">
										Download Receipt
									</a>
								</div>
								{/* <div className="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2">
                                            <a className="btn btn-block btn-sm btn-primary" href="#!">Refund</a>
                                        </div> */}
							</div>
						</div>
					) : (
						""
					)}
				</div>
			</div>
		</div>
	);
};
export default BookingDetails;
