import React, { Component } from "react";
import { connect } from "react-redux";
import Booking from "./booking";
import Sidebar from "../_component/sidebar";
import { getBooking } from "../../../stores/actions/myBookingAction";
import Pagination from "react-js-pagination";
import Loader from "../../loader";
import { FormControl, FormGroup, InputGroup, Glyphicon } from "react-bootstrap";
import { isEmpty } from "lodash";

class Content extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isUpdate: false,
			page: 1,
			search: "",
			sort: "latest",
		};
	}

	getDefault = (query, data) => {
		this.props.dispatch(getBooking( query, this.props.joToken, data));
	};

	handlePageChange = (pageNumber) => {
		let query = { page: pageNumber}
		this.setState({ page: pageNumber }, () => {
			return this.getDefault(query);
		});
	};

	handleSearch = (e) => {
		e.preventDefault();
		this.setState({ search: e.target.value });
	};

	searchBooking = (e) => {
		e.preventDefault();
		this.setState({ page: 1 }, () => {
			return this.getDefault({page: 1}, {filter: [{order_number: this.state.search}], sort: [{created_at: 'DESC'}]});
		});
	};

	handleToggle = (e) => {
		e.preventDefault();
		this.setState({
			isUpdate: !this.state.isUpdate,
		});
	};
	onChangeSort = (e) => {
		e.preventDefault();
		this.setState({ sort: e.target.value, page: 1 });
		let body = {
			sort: {created_at: e.target.value === "latest" ? "DESC" : "ASC"}
		}
		if(this.state.search !== "") {
			body.filter = [{order_number: this.state.filter}]
		}
		setTimeout(() => {
			this.getDefault({page: 1}, body);
		}, 1000);
	};

	sortAndSearch = () => {
		return (
			<div className="row sort_search">
				<div className="col-lg-3 col-md-3 col-sm-12 col-xs-6">
					<div className="select-person ">
						<select onChange={this.onChangeSort} defaultValue={this.state.sort} className="form-control">
							<option value="">Sort by</option>
							<option value="latest">Latest</option>
							<option value="oldest">Oldest</option>
						</select>
					</div>
				</div>
				<div className="col-lg-5 col-md-5  col-xs-6" />
				<div className="col-lg-4 col-md-4 col-sm-12 col-xs-6" align="right">
					<form onSubmit={this.searchBooking}>
						<FormGroup>
							<InputGroup>
								<FormControl
									bsSize="small"
									type="text"
									onChange={this.handleSearch}
									value={this.state.search}
									placeholder="Search ..."
								/>
								<InputGroup.Addon>
									<a style={{ cursor: "pointer" }} onClick={this.searchBooking}>
										<Glyphicon glyph="search" />
									</a>
								</InputGroup.Addon>
							</InputGroup>
						</FormGroup>
					</form>
				</div>
			</div>
		);
	};
	render() {		
		const { list, url, pagination, path, isLoading } = this.props;
		let history = 0,
			active = 0;
		list.map((item, key) => {
			return item.payment_status_code === 1 || item.payment_status_code === 4 ? (active += 1) : active;
		});
		list.map((item, key) => {
			return item.payment_status_code !== 1 && item.payment_status_code !== 4 ? (history += 1) : history;
		});

		return (
			<section className="content-wrap">
				<div className="container">
					<div className="row main-dashboard">
						<Sidebar handleToggle={this.handleToggle} isUpdate={this.state.isUpdate} path={path} />

						<div className="col-lg-9 col-md-9 col-sm-12 profile-content">
							<div className="container-fluid">
								<div className="alert alert-bordered">
									<img src="/static/images/ic_info_24.png" className="icon" alt="" />
									<div className="text-wrapper">
										Listed below is your booking history. Click on the each booking to find the details, related
										e-tickets can be found attached in each booking or you can simply go to <a href="#!">E-Tickets</a>{" "}
										to find your tickets.
									</div>
								</div>
								{this.sortAndSearch()}
								{isLoading ? (
									<div align="center">
										<Loader />
									</div>
								) : (
									<div>
										<div className="list-booking panel-group">
											<h4 className="text-title">Booking History</h4>
											{list.map((item, key) => (
													<Booking
														key={key}
														id={item.order_number}
														date={item.created_at}
														status={item.status_name}
														statusCode={item.status}
														paymentUrl={item.payment_url}
														bookNumb={item.order_number}
														price={item.total_paid}
													/>
											))}

											{history === 0 ? <p className="text-muted">No booking history.</p> : ""}
											{/* <Booking /> */}

											{!isEmpty(pagination) && (
												<div>
													<nav className="pagination-nav">
														<span className="text-label">
															Displaying {isEmpty(pagination) ? 0 : pagination.to} of{" "}
															{pagination === null ? 0 : pagination.total}
														</span>
														<Pagination
															activePage={this.state.page}
															pageCount={pagination.last_page}
															itemsCountPerPage={pagination.per_page}
															totalItemsCount={pagination.total}
															pageRangeDisplayed={3}
															onChange={this.handlePageChange}
														/>
													</nav>
												</div>
											)}
										</div>
									</div>
								)}
							</div>
						</div>
						{/* profile-content */}
					</div>
				</div>
				{/* container */}
			</section>
		);
	}
}
export default connect()(Content);
