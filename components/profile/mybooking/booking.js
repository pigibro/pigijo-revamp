import React from "react";
import NumberFormat from "react-number-format";
import moment from "moment";
import { Link } from "../../../routes/index";

class Booking extends React.Component {
	// handleAllowToConttinue = (e) => {
	// 	this.props.dispatch(setAllowToPay(1));
	// };
	render() {
		return (
			<div className="panel">
				<div className="panel-body">
					<div className="row flex-row flex-center">
						<div className="col-lg-5 col-md-5 col-sm-12 col-xs-12 booking-padd">
							<div className="text-label">Booking Date</div>
							<div className="text-dark">{moment(this.props.date).format("ddd, DD MMM YYYY, HH:mm")}</div>
						</div>
						<div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div className="text-label">Booking Number</div>
							<div className="text-dark">{this.props.bookNumb}</div>
						</div>
						<div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div className="price ">
								<div className="amount h5 price-booking">
									<NumberFormat
										value={parseInt(this.props.price, 10)}
										displayType={"text"}
										thousandSeparator={true}
										prefix={"Rp "}
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="panel-footer flex-row flex-center">
					<div className="booking-status">
						{this.props.statusCode === 1 || this.props.statusCode === 4 ? (
							<img src="/static/images/ic_clock_24.png" className="icon" alt="" />
						) : this.props.statusCode === 2 ? (
							<img src="/static/images/ic_success_24.png" className="icon" alt="" />
						) : (
							<img src="/static/images/ic_cancel_24.png" className="icon" alt="" />
						)}
						{this.props.status}
					</div>
					<div className="booking-nav">
						{/* {this.props.statusCode === 1 && (
							<Link route="/finalize-itinerary/payment" onClick={this.handleAllowToConttinue()}>
								Continue Payment
							</Link>
						)} */}
						<Link route="my-booking-detail" params={{ slug: this.props.id }}>
							<a>View Details</a>
						</Link>
					</div>
				</div>
			</div>
		);
	}
}
export default Booking;
