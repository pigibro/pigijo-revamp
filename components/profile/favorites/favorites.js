import React, { Component } from 'react'
import { connect } from 'react-redux'
// import { Glyphicon } from 'react-bootstrap'
import { addWishList, removeWishList } from 'actions/wishList/wishList.action'
import { ModalToggle, ModalType } from 'actions/modal.action'
class Favorites extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isWishList: false,
      isDetail: false
    }
    this.postWishList = this.postWishList.bind(this)
  }

  componentWillMount () {
    this.setState({
      isWishList: this.props.is,
      isDetail:
        this.props.isDetail !== undefined
          ? this.props.isDetail
          : this.state.isDetail
    })
  }

  async postWishList (e,is_active) {
    e.preventDefault()
    const { id, type, userData } = this.props
    let cekObj = Object.keys(userData)
    if (cekObj.length > 0) {
      if (!is_active) {
        let res = await this.props.dispatch(addWishList(id, type , this.props.temporary_wishlist_list ,this.props.temporary_wishlist_remove))
        if (res) {
          this.setState({ isWishList: true })
        }
      } else {
        let res = await this.props.dispatch(removeWishList(id, type , this.props.temporary_wishlist_list ,this.props.temporary_wishlist_remove))
        if (res) {
          this.setState({ isWishList: false })
        }
      }
    } else {
      this.props.dispatch(ModalToggle(true))
      this.props.dispatch(ModalType('login'))
    }
  }

  render () {
    const { id, type } = this.props
    let temporary_list = this.props.temporary_wishlist_list
    let temporary_remove = this.props.temporary_wishlist_remove
    let string_id = id+"_"+type
    let is_active = this.state.isWishList === true || temporary_list.includes(string_id) === true
    let is_remove = temporary_remove.includes(string_id)

    if(is_remove === true)
      {
        is_active = false
      }

    return (
      <div>
        {!this.state.isDetail ? (
          
          <div className='whistlist'>
            <a href='' onClick={(e)=>this.postWishList(e,is_active)}>
              { is_active === true ? (
                <abbr title="Remove from favorites">
                  <div className='fill'>
                    <div className='isi' />
                    {/* <Glyphicon glyph='heart' /> */}
                  </div>
                </abbr>
              ) : (
                <abbr title="Add to favorites">
                  <div className='empty'>
                    <div className='kosong' />
                    {/* <Glyphicon glyph='heart' /> */}
                  </div>
                </abbr>
              )}
            </a>
          </div>
        ) : (
          <div className='whistlist-detail'>
            <a href='' onClick={(e)=>this.postWishList(e,is_active)}>
              { is_active ? (
                <abbr title="Remove from favorites">
                  <div className='fill'>
                    {/* <Glyphicon glyph='heart' /> */}
                    <div className='isi-detail' />
                  </div>
                </abbr>
              ) : (
                <abbr title="Add to favorites">
                  <div className='empty'>
                    {/* <Glyphicon glyph='heart' /> */}
                    <div className='kosong-detail' />
                  </div>
                </abbr>
              )}
            </a>
          </div>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    userData: state.sessionReducer.user,
    temporary_wishlist_list : state.wishListReducer.temporary_list,
    temporary_wishlist_remove : state.wishListReducer.temporary_remove,
  }
}
export default connect(mapStateToProps)(Favorites)
