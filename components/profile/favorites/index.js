import React, { Component } from 'react'
import { connect } from 'react-redux'
import { wishlistList, removeWishList } from '../../../stores/actions/favoritesActions'
import Sidebar from '../_component/sidebar'
import { Badge, Glyphicon, Popover, ButtonToolbar,OverlayTrigger } from 'react-bootstrap'
import { isMobileOnly } from "react-device-detect";
import Pagination from 'react-js-pagination'
import {isEmpty} from 'lodash'
import Loader from '../../loader'

class FavoritesItem extends Component {
  constructor (props) {
    super(props)
    this.state = {
      show: false,
      isSelected: false,
      page:1
    }
    this.delItem = this.delItem.bind(this)
    this.handlePageChange = this.handlePageChange.bind(this)
    this.getList = this.getList.bind(this)
  }
  
async getList(query){
   await this.props.dispatch(wishlistList(this.props.token,query))
}

componentWillReceiveProps(props){
    this.setState({nextUrl:props.paging})
}

handlePageChange (pageNumber) {
    let query = `?page=${this.state.page}&per_page=8`;

    this.setState({ page: pageNumber },()=>(
        this.getList(query)
    ))
}

async delItem (e,id, type){
    e.preventDefault();
    
    let res = await this.props.dispatch(removeWishList(id, type, null,null,this.props.token))
    if (res) {
        this.getList(`?page=1&per_page=8`)
    }
 }

  render () {
    const {list, url, paging, path, isLoading} = this.props
    const _imgStyle = slide => {
      return {
        backgroundImage: `url(${slide}),url('https://s3.ap-southeast-1.amazonaws.com/pigijo/assets/team1.jpg')`,
        backgroundSize: `cover`,
        backgroundPosition: 'center center',
        width: '100%',
        height: '100%'
      }
    }
    
    return (
      <section className='content-wrap'>
        <div className='container'>
          <div className='row main-dashboard'>
            <Sidebar path={path} />
            <div className='col-lg-9 col-md-9 col-sm-12 profile-content'>
              <div className='container-fluid'>
                <h4 className='text-title'>Favorites</h4>
                { isLoading?<Loader/>:
                    !isEmpty(list)? list.map((item, i) => (
                        <div className='col-lg-3 col-md-4 col-sm-6 col-xs-12' key={i}>
                            <div className='plan-item sr-btm'>
                                <div className='box-img plan-img'>
                                    {item.product_type === 'event_loket'?(
                                        <div className='thumb' style={_imgStyle( item.cover_path + item.cover_filename)}/>
                                    ):(
                                        <div className='thumb' style={_imgStyle(url + item.cover_path + '/small/' +item.cover_filename)}/>
                                    )}
                                    {isMobileOnly?'':(
                                        <div className='overlay'>
                                            <button className='btn btn-sm btn-o btn-select-activity' onClick={e=>this.delItem(e,item.product_id, item.product_type)} type='button'>Remove</button>
                                        </div>
                                    )}
                                </div>
                                {isMobileOnly?(
                                    <div className='option-v' align='right'>
                                        <ButtonToolbar>
                                            <OverlayTrigger
                                            style={{left:'70%'}}
                                                trigger="click"
                                                key="bottom"
                                                placement="bottom"
                                                overlay={
                                                    <Popover id={i} align='right'  >
                                                        <a href='' onClick={e=>this.delItem(e,item.product_id, item.product_type)}>
                                                            <Glyphicon glyph='trash'/> Remove
                                                        </a>
                                                    </Popover>
                                                }
                                            >
                                                <div as='a'>
                                                    <Glyphicon glyph='option-vertical'/>
                                                </div>
                                            </OverlayTrigger>
                                        </ButtonToolbar>
                                    </div>):''}
                                <a href={item.detail_url}>
                                <div className='plan-info'>
                                    <h1 className='plan-title' style={{height:'auto'}}>{item.product_name?item.product_name.length > 25?item.product_name.slice(0,20)+` ...`:item.product_name:'' }</h1>
                                    <p style={{fontSize:'0.9em'}}><small className="text-muted">{item.city}, {item.province?item.province.length > 25?item.province.slice(0,15)+` ...`:item.province:''} </small></p>
                                    <hr/>
                                    <div className='row'>
                                        <div className='col-md-6 col-xs-6'>
                                            <Badge variant="info">{item.type_alias || 'Tour'}</Badge>
                                        </div>
                                        <div className='col-md-6 col-xs-6' align='right'>
                                            <span className='link' style={{fontSize:'1em'}}>Detail</span>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    ))
                  : (
                    <div className='alert alert-bordered' align='center'>
                        <div className='text-wrapper' >
                            <h3 style={{color:'#c3c3c3'}} >&#9785;</h3>
                            <h5 style={{color:'#c3c3c3'}} className='text-title'>No wishlist found here </h5>
                        </div>
                    </div>
                  )}
              </div>
                {!isEmpty(paging) && (
                    <div>
                    <nav className='pagination-nav'>
                    <span className='text-label'>
                        Displaying {isEmpty(paging) ?  0 : paging.displaying} of {paging === null ? 0 : paging.total}
                    </span>
                    <Pagination
                        activePage={this.state.page}
                        pageCount={paging.last_page}
                        itemsCountPerPage={paging.per_page}
                        totalItemsCount={paging.total}
                        pageRangeDisplayed={10}
                        onChange={this.handlePageChange}
                        />
                    </nav>
                    </div>
                )}
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default connect()(FavoritesItem)
