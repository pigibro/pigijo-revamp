import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { sendPasswordEdit } from '../../../stores/actions';


class ChangePass extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            current_password: null,
            new_password: null,
            confirm_password: null,
        }
    }

    handleOnChange = (e) => {
        let name = e.target.name
        this.setState({
            [name]: e.target.value
        })
    }

    handleOnSubmit = (e) => {
        const { current_password, new_password, confirm_password } = this.state
        e.preventDefault()
        let param = { data: { 
            'current_password': current_password, 
            'new_password': new_password, 
            'confirm_password': confirm_password
         }, headers: { 'Guest-Token': this.props.token } }
        this.props.dispatch(sendPasswordEdit(param))
    }
    render() {
        return (
            <div className="panel">
                <div className="panel-heading">
                    <h4 className="text-title">Change Password</h4>
                </div>
                <div className="panel-body">
                    <form onSubmit={this.handleOnSubmit}>
                        <div className="row">
                            <div className="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-lg-6 col-md-6 col-sm-6">
                                <span className="text-label">Current Password</span>
                                <div className="form-group">
                                    <input className="form-control" type="password" onChange={this.handleOnChange} required name="current_password" />
                                </div>
                            </div>
                            <div className="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-lg-6 col-md-6 col-sm-6">
                                <span className="text-label">New Password</span>
                                <div className="form-group">
                                    <input className="form-control" type="password" onChange={this.handleOnChange} required name="new_password" />
                                </div>
                            </div>
                            <div className="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-lg-6 col-md-6 col-sm-6">
                                <span className="text-label">Confirm New Password</span>
                                <div className="form-group">
                                    <input className="form-control" type="password" onChange={this.handleOnChange} required name="confirm_password" />
                                </div>
                            </div>
                            <div className="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-lg-6 col-md-6 col-sm-6">
                                <div className="row">
                                    <div className="col-lg-6 col-md-6 col-sm-6">
                                        <button className="btn btn-block btn-primary" type="submit">Submit</button>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-6">
                                        <button className="btn btn-block btn-o btn-primary" onClick={this.props.cancel} type="submit">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}
export default connect()(ChangePass)