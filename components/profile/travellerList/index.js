import React, { Component } from "react";
import { connect } from "react-redux";
import Traveller from "./content";
import Sidebar from "../_component/sidebar";
import { getTraveller, getSingleTraveller, removeTraveller } from "../../../stores/actions/travellerActions";
import AddTraveller from "./addTraveller";
import EditTraveller from "./editTraveller";
import Loader from "../../loader";
import { isEmpty } from "lodash";

class Index extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isUpdate: false,
			inAction: null,
		};
	}

	onClickRemove = (id) => {
		this.props
			.dispatch(removeTraveller(this.props.token, id))
			.then(() => this.props.dispatch(getTraveller(this.props.token)));
	};

	handleToggle = (e, name, id) => {
		e.preventDefault();
		this.setState({ inAction: name });
		if (name === "edit") {
			this.props.dispatch(getSingleTraveller(this.props.token, id));
		}
	};

	render() {
		const { list, isLoading, detail, token, id } = this.props;
		const { inAction } = this.state;

		return (
			<section className="content-wrap">
				<div className="container">
					<div className="row main-dashboard">
						<Sidebar path={this.props.path} />
						<div className="col-lg-9 col-md-9 col-sm-12 profile-content">
							<div className="container-fluid">
								{isLoading ? (
									<Loader />
								) : inAction === "add" ? (
									<AddTraveller token={token} action={this.handleToggle} />
								) : inAction === "edit" ? (
									<EditTraveller token={token} action={this.handleToggle} id={id} detail={detail} />
								) : (
									<div>
										<h4 className="text-title">Traveller List</h4>
										<p>
											Add your travel companions to the address book for easier booking process. You can keep up to 20
											travellers data.
										</p>
										<div className="row sm-gutter list-traveller">
											{!isEmpty(list) &&
												list.map((item, key) => (
													<div key={key}>
														<Traveller
															salutation={item.salutation}
															firstname={item.firstname}
															lastname={item.lastname}
															id={item.id}
															onClick={this.onClickRemove}
															toggleEdit={this.handleToggle}
															phone={item.phone}
														/>
													</div>
												))}
											<div className="col-lg-4 col-md-4 col-sm-4">
												<a
													className="btn btn-block btn-o btn-primary add-traveller"
													href=""
													onClick={(e) => this.handleToggle(e, "add")}
												>
													<i className="ti-plus" />
													Add Traveller
												</a>
											</div>
										</div>
									</div>
								)}
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		list: state.traveller.list,
		detail: state.traveller.details,
		isLoading: state.traveller.isLoading,
		id: state.traveller.id,
	};
};

export default connect(mapStateToProps)(Index);
