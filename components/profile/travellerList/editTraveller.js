import React, { Component } from "react";
import { connect } from "react-redux";
import { editTraveller, getTraveller } from "../../../stores/actions/travellerActions";
import { isEmpty } from "lodash";

class EditTraveller extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstname: "",
			lastname: "",
			phone: "",
			email: "",
			salutation: "",
		};
	}

	handleOnChange = (e) => {
		const { name, value } = e.target;
		this.setState({
			[name]: value,
		});
	};

	componentDidMount() {
        const {detail} = this.props
        if(!isEmpty(detail)){
            this.setState({
                firstname: detail.firstname,
                lastname: detail.lastname,
                phone: detail.phone,
                email: detail.email,
                salutation: detail.salutation,
            });
        }
	}

	save = (e) => {
		e.preventDefault();
		const { salutation, firstname, lastname, email, phone } = this.state;
		const { id, token } = this.props;
        this.props.dispatch(editTraveller(firstname, lastname, salutation, phone, email, id, token))
        .then(()=>{
            this.props.dispatch(getTraveller(token))
            this.props.action(e,null)
        }

        )
	};

	render() {
		const { salutation, firstname, lastname, email, phone } = this.state;
		return (
			<div className="panel panel-profile">
				<div className="panel-body">
					<div className="row">
						<div className="col-lg-12">
							<form className="form-horizontal">
								<div className="form-group">
									<label htmlFor="salutation" className="col-sm-2 text-label control-label ">
										Salutation
									</label>
									<div className="col-sm-10">
										<select
											onChange={this.handleOnChange}
											className="form-control"
											value={salutation}
											name="salutation"
										>
											<option value="Mr">Mr</option>
											<option value="Mrs">Mrs</option>
											<option value="Ms">Miss</option>
										</select>
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="firstname" className="col-sm-2 text-label control-label">
										Firstname
									</label>
									<div className="col-sm-10">
										<input
											onChange={this.handleOnChange}
											className="form-control"
											id="firstname"
											type="text"
											name="firstname"
											required
											value={firstname}
										/>
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="inputPassword" className="col-sm-2 text-label control-label">
										Lastname
									</label>
									<div className="col-sm-10">
										<input
											onChange={this.handleOnChange}
											className="form-control"
											type="text"
											name="lastname"
											required
											value={lastname}
										/>
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="email" className="col-sm-2 text-label control-label">
										Email
									</label>
									<div className="col-sm-10">
										<input
											className="form-control"
											type="email"
											pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"
											name="email"
											onChange={this.handleOnChange}
											required
											value={email}
										/>
									</div>
								</div>

								<div className="form-group">
									<label htmlFor="phone" className="col-sm-2 text-label control-label">
										Phone
									</label>
									<div className="col-sm-10">
										<input
											onChange={this.handleOnChange}
											className="form-control"
											type="number"
											name="phone"
											required
											value={phone}
										/>
									</div>
								</div>

								<div className="col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6">
									<div className="row mt2">
										<div className="col-lg-6 col-md-6 col-sm-6">
											<button className="btn btn-block btn-o btn-primary" onClick={(e) => this.props.action(e,null)}>
												Cancel
											</button>
										</div>
										<div className="col-lg-6 col-md-6 col-sm-6">
											<a className="btn btn-block btn-primary" href="" onClick={this.save}>
												Save
											</a>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default connect()(EditTraveller);
