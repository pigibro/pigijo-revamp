import React from 'react'

const Content = (props) => {
    return (
        <div className="col-lg-4 col-md-4 col-sm-4">
            <div className="traveller">
                <div className="icon">
                    <i className="ti-user" />
                </div>
                <div className="traveller-info">
                    <p className="traveller-name">{props.salutation} {props.firstname + " " + props.lastname}</p>
                    <div className="traveller-action">
                        <a href="" data-toggle="dropdown"><i className="ti-more-alt" /></a>
                        <div className="dropdown-menu dropdown-menu-custom dropdown-menu-right">
                            <ul>
                                <li><a href="" onClick={(e)=>props.toggleEdit(e,'edit',props.id)}>Edit</a></li>
                                <li><a href="" onClick={()=>props.onClick(props.id)}>Remove</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Content