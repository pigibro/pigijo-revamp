import React, { Component } from 'react'
import {
  getBookTourList,
  getBookAccList,
  getBookRentCarList
} from 'actions/eTicket/bookList.action'
import {getBooking} from "actions/myBooking/listBooking.action"
import Pagination from 'react-js-pagination';
import { connect } from 'react-redux'
// import { dispatch } from 'redux'

class Paging extends Component{
  constructor(props){
    super(props);
      this.state = {
      page: 1,
      name_tab: 'tour'
    }
  }

  getSomeData () {
  // return (props) => {
  const { name_tab, page } = this.state
  let query = `?page=${page}&per_page=10`
    if (name_tab === 'car') {
        return this.props.dispatch(getBookRentCarList(query))
    } else if (this.state.name_tab === 'hotel'){
        return this.props.dispatch(getBookAccList(query))
    } else if (this.state.name_tab === 'tour'){
        return this.props.dispatch(getBookTourList(query))
    } else {
        return this.props.dispatch(getBooking(query))
    // }
    }
  }
  
  handlePageChange (pageNumber) {
  this.setState({ page: pageNumber })
    // state.page = pageNumber
    setTimeout(() => {
      return this.getSomeData()
    }, 1000)
  }
  
  resetPage (e, name_tab) {
    e.preventDefault()
    
    this.setState({ page: 1, name_tab: name_tab })
    // state.page = 1,
    // state.name_tab = name_tab
    setTimeout(() => {
      return this.getSomeData()
    }, 1000)
  }
  
  paging (pagination) {
  if (pagination !== null) {
    return (
      <nav className='pagination-nav'>
        <span className='text-label'>
          Displaying {pagination.displaying} of {pagination.total}
        </span>
        <Pagination
          activePage={this.state.page}
          pageCount={pagination.last_page}
          itemsCountPerPage={pagination.per_page}
          totalItemsCount={pagination.total}
          pageRangeDisplayed={10}
          onChange={this.handlePageChange}
        />
      </nav>
    )
  } else {
    return <div />
  }
  }
    
}
export const getSomeData = Paging.prototype.getSomeData;
export const resetPage = Paging.prototype.resetPage;
export const handlePageChange = Paging.prototype.handlePageChange;
export const paging = Paging.prototype.paging;
export default connect(getSomeData,resetPage,handlePageChange,paging)(Paging)
