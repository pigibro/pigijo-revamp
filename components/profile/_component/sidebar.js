import React from 'react'
// import { Link } from 'react-router-dom'

const Sidebar = ({ path }) => {
    return (
        <aside className="col-lg-3 col-sm-12 col-xs-12 sidebar">
            <nav className="sidebar-nav">
                <ul className="list-unstyled sidebar-menu">
                    <li className={path === "/profile" ? "active" : "unactive"}><a href="/profile"><i className="ti-user" /><span>Edit Profile</span></a></li>
                    {/* <li className={path === "/itinerary" ? "active" : "unactive"}><a href="/itinerary" ><i className="ti-location-pin" /><span>My Itinerary</span></a> </li> */}
                    <li className={path === "/my-booking" ? "active" : "unactive"}><a href="/my-booking" ><i className="ti-star" /><span>My Booking</span></a></li>
                    {/* <li className={path === "/eticket" ? "active" : "unactive"}><a href="/eticket" ><i className="ti-ticket" /><span>Issued E-Ticket</span></a> </li> */}
                    {/* <li className={path === "/traveller" ? "active" : "unactive"}><a href="/traveller" ><i className="ti-map-alt" /><span>Traveller Quick Pick</span></a> </li> */}
                    {/* <li className={path === "/favorites" ? "active" : "unactive"}><a href="/favorites" ><i className="ti-heart" /><span>Favorites</span></a> </li> */}
                </ul>
            </nav>
        </aside>
    )
}

export default Sidebar
