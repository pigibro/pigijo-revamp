import React, { Component } from 'react'
import { connect } from 'react-redux'
// import headPicture from "assets/images/img01.jpg"
// import { isChangeProfile } from "actions/profileEdit/edit.action"
// import userPicture from "assets/images/team1.jpg"
// import { HELPER } from "actions/_constants"
// import { Redirect } from 'react-router-dom'
import { isEmpty } from 'lodash'
class Header extends Component {
    constructor(props){
        super(props)
        this.handleToggle = this.handleToggle.bind(this)
    }
    // componentWillReceiveProps(props){
    //     const userData_val = props.userData
    //     // localStorage.setItem("logData",JSON.stringify(userData_val))
    // }
   async componentWillMount(){
         await this.props.userData
         await this.props.authenticated
        // const userData_val = await this.props.userData
        // let getUser = JSON.parse(localStorage.getItem("logData"));  
        //  if(getUser !== null || getUser !== {}){
        //    if(userData_val.login !== undefined && userData_val !== null){
        //     localStorage.setItem("logData",JSON.stringify(userData_val))
        // }
        //     localStorage.setItem("logData",JSON.stringify(userData_val))
        //  }else{
        //     localStorage.setItem("logData",JSON.stringify(userData_val))
        // }
    }
    handleToggle(e){
        e.preventDefault()
        const bool = !this.props.isChange
        // this.props.dispatch(isChangeProfile(bool))
    }
    render() {
        const _divStyle = (slide) => {
        return {
                backgroundImage: `url(${slide})`,
                backgroundSize: `cover`,
                backgroundPosition: 'center center',
                width: '100%',
                height: '100%'
            }
        }

            return (
                <section className="page-header">
                    <div className="thumb page-header-bg" style={_divStyle('/static/images/img03.jpg')}>
                    {/* <img src={} alt="header"/> */}
                    </div>
                    <div className="page-header-content">
                            <div className="profile-header">
                                <div className="container">
                                    <div className="row flex-row flex-center">
                                        <div className="col-lg-9 col-md-9 col-sm-8">
                                            <div className="user-avatar">
                                                <div className="">
                                                {/* {
                                                    getUser.profile_picture === null ? (
                                                    <div className="thumb" style={_divStyle(userPicture)} />
                                                    ): (
                                                    <div className="thumb" style={_divStyle(HELPER.IMAGE_URL+getUser.profile_picture_path+"/"+getUser.profile_picture)}/>
                                                    )
                                                } */}
                                                </div>
                                                {
                                                    !isEmpty(this.props.profile) && (
                                                        <div className="user-info">
                                                            <div className="user-name">{this.props.profile.firstname} {this.props.profile.lastname}</div>
                                                            <div className="user-email">{this.props.profile.email} </div>
                                                        </div>

                                                    )
                                                }
                                            </div>{/* user-avatar */}
                                        </div>{/* col */}
                                        <div className="col-lg-3 col-md-3 col-sm-4">
                                        {/* {
                                            this.props.isProfile ?
                                                    <a className="btn btn-block btn-primary update-toggle" onClick={this.handleToggle} href="">Change Profile</a>
                                                : ''
                                        } */}

                                        </div>
                                    </div>
                                </div>{/* container */}
                            </div>
                    </div>{/* page-header-content */}
                </section>
            )
        } 
};

const mapStateToProps = (state) => {
    return{
        profile: state.auth.profile
    }
}
// export default Header
export default connect(mapStateToProps)(Header)
