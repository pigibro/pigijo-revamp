import React,{ Component } from 'react';
import { connect } from "react-redux";
import { modalToggle } from '../../../stores/actions'

class Privacy extends Component {
  constructor(props) {
      super(props)
      this.toggle = this.toggle.bind(this)
  }
  toggle(e){
    e.preventDefault();
    this.props.dispatch(modalToggle(false));
  }
  render() {
      return (
        <>
        <div className="modal-header">
          <span className="h4 text-title">Privacy &amp; Policy</span>
        </div>
        <div className="modal-panel ">
          <div className="panel-body">
          <div>
            <b>PENDAHULUAN</b><br />
            <br />
            <div style={{ textAlign: 'justify' }}>
                Selamat datang di Website pigijo.com. Website ini dimiliki, dioperasikan, diselenggarakan oleh PT. Tourindo Guide Indonesia (“Kami” atau “pigijo.com”), suatu perseroan terbatas yang didirikan berdasarkan hukum Republik Indonesia dengan Ijin Tetap Usaha Pariwisata Nomor AHU-10.AH.02.02 tertanggal 09 Februari 2010; Kami menyediakan website dan layanan yang tersedia secara online melalui website: www.pigijo.com atau situs web mobile : m.pigijo.com atau aplikasi mobile : pigijo untuk iOS dan pigijo untuk Android, dan berbagai akses, media, perangkat dan platform lainnya (“Layanan Online”), baik yang sudah atau akan tersedia di kemudian hari.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <br />
            <b>APA ITU ATURAN PRIVASI PIGIJO.COM</b><br />
            <br />
            <div style={{ textAlign: 'justify' }}>
                Kebijakan Privasi ini mengatur dan atau menjelaskan seluruh layanan yang sudah Kami sediakan untuk Anda (“Pengguna”) gunakan, baik layanan yang Kami operasikan sendiri maupun yang dioperasikan melalui afiliasi dan/atau rekanan Kami. Untuk menjaga kepercayaan Anda kepada Kami, maka Kami senantiasa akan menjaga segala kerahasiaan yang terkandung dalam data pribadi Anda, karena Kami menganggap privasi Anda sangat penting bagi Kami. Dalam aturan privasi ini Kami sertakan penjelasan mengenai tata cara Kami mengumpulkan, menggunakan, mengungkapkan, memproses dan melindungi informasi dan data pribadi Anda yang Kami identifikasi (“Data Pribadi”). Pada saat Anda membuat pemesanan dan atau akun pribadi pada website pigijo.com, maka Kami akan mencatat dan menyimpan informasi dan data pribadi Anda. Dimana pada prinsipnya, setiap data yang Anda berikan dari waktu ke waktu akan Kami simpan dan Kami gunakan untuk kepentingan penyediaan produk dan layanan Kami kepada Anda, yaitu antara lain untuk keperluan tokenisasi, akuntansi, tagihan, audit, verifikasi kredit atau pembayaran, newsletter, serta keperluan keamanan, administrasi dan hukum, bonus poin atau bentuk sejenisnya, pengujian, pemeliharaan dan pengembangan sistem, hubungan pelanggan, promosi dan membantu Kami di kemudian hari dalam memberikan pelayanan kepada Anda. Sehubungan dengan itu, Kami dapat mengungkapkan data Anda kepada grup perusahaan dimana pigijo.com bergabung didalamnya, mitra penyedia produk, perusahaan lain yang tercatat sebagai rekanan dari pigijo.com, perusahaan yang ditunjuk untuk melakukan proses data yang terikat kontrak dengan Kami, agen perjalanan, badan pemerintah dan badan peradilan yang berwenang di jurisdiksi manapun. Oleh karena pentingnya aturan privasi ini dan ketentuan lainnya dalam penggunaan website Kami, maka untuk menjaga keamanan data pribadi Anda, maka mohon untuk dibaca secara seksama seluruh ketentuan dalam aturan privasi ini dan ketentuan lainnya dalam website Kami.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <br />
            <b>INFORMASI APA SAJA YANG KAMI KUMPULKAN DAN GUNAKAN ?</b><br />
            <br />
            <div style={{ textAlign: 'justify' }}>
                Pada saat Anda mengakses website dan atau layanan online yang Kami sediakan, maka semua informasi dan data pribadi Anda akan Kami kumpulkan dengan ketentuan sebagai berikut :</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <ul>
                <li style={{ textAlign: 'justify' }}>Kami akan mengumpulkan informasi mengenai komputer atau pun media apapun yang Anda gunakan, termasuk IP address, sistem operasi, browser yang digunakan, URL, halaman, lokasi geografis dan waktu akses serta data lainnya terkait dengan penggunaan komputer Anda (“Detail IP”).</li>
                <li style={{ textAlign: 'justify' }}>Kami akan meminta Anda untuk mengisi data-data pribadi Anda secara benar, jelas, lengkap, akurat dan tidak menyesatkan, seperti nama, alamat email, nomor telepon, alamat lengkap, informasi yang digunakan untuk pembayaran, informasi kartu kredit (nomor kartu kredit dan masa berlaku kartu kredit) dan data-data lain yang Kami perlukan guna melakukan transaksi melalui website dan layanan online lainnya yang Kami sediakan agar Anda dapat memanfaatkan layanan yang Anda butuhkan. Kami tidak bertanggung jawab atas segala kerugian yang mungkin terjadi karena informasi dan atau data yang tidak benar, jelas, lengkap, akurat dan menyesatkan yang Anda berikan;</li>
                <li style={{ textAlign: 'justify' }}>c. Kami dapat menggunakan data pribadi Anda dan informasi lainnya yang dikumpulkan dengan tujuan pemasaran Media Sosial menggunakan tehnik grafik langsung dan terbuka dan untuk tujuan pemasaran digital konvensional, seperti mengirimkan Anda newsletter secara otomatis melalui surat elektronik untuk memberitahukan informasi produk baru, penawaran khusus atau informasi lainnya yang menurut Kami akan menarik bagi Anda.</li>
                <li style={{ textAlign: 'justify' }}>Dalam menggunakan layanan Kami, informasi-informasi yang Anda berikan dapat Kami gunakan dan berikan kepada pihak ketiga yang bekerjasama dengan Kami, sejauh untuk kepentingan transaksi dan penggunaan layanan Kami</li>
                <li style={{ textAlign: 'justify' }}>Segala informasi yang Kami terima dapat Kami gunakan untuk melindungi diri Kami terhadap segala tuntutan dan hukum yang berlaku terkait dengan penggunaan layanan dan pelanggaran yang Anda lakukan pada website Kami atas segala ketentuan sebagaimana diatur dalam persyaratan layanan pigijo.com dan pedoman penggunaan produk dan layanan Kami, termasuk dan tidak terbatas apabila dibutuhkan atas perintah Pengadilan dalam proses hukum;</li>
                <li style={{ textAlign: 'justify' }}>Anda bertanggung jawab atas kerahasiaan informasi dan data pribadi Anda, termasuk bertanggung jawab atas semua akses dan penggunaan website yang menggunakan kata sandi dan akun serta token yang Anda miliki yang digunakan oleh siapa saja, baik atas seijin maupun tidak seijin Anda. Demi keamanan data <span style={{ whiteSpace: 'pre' }}>	</span>rahasia Anda, Kami sangat menyarankan agar Anda menyimpan akun dan kata sandi yang Anda miliki dengan sebaik-baiknya dan atau melakukan perubahan kata sandi secara berkala. Setiap penggunaan yang tidak sah dan tanpa sepengetahuan dan izin Anda menjadi tanggung jawab Anda sendiri dan Kami tidak bertanggung-jawab atas segala kerugian yang ditimbulkan sebagai akibat dari kelalaian yang Anda lakukan.</li>
                <li style={{ textAlign: 'justify' }}>Anda harus segera memberitahukan kepada Kami mengenai adanya penggunaan sandi atau akun tanpa izin Anda atau semua bentuk pelanggaran atau ancaman pelanggaran keamanan dalam website ini.</li>
                <li style={{ textAlign: 'justify' }}>Anda berhak untuk merubah dan atau menghapus data alamat yang telah Anda berikan dan telah tersimpan dalam sistem Kami dengan cara menghubungi&nbsp; Customer Service kami.</li>
            </ul>
            <br />
            <br />
            <b>PENGGUNAAN WEBSITE</b><br />
            <br />
            <div style={{ textAlign: 'justify' }}>
                Dengan menerima Ketentuan Penggunaan website ini, maka Anda telah diberikan keleluasaan dan atau otoritas akses yang terbatas dan dapat dibatalkan, tidak dapat dipindah-tangankan dan lisensi non-eksklusif untuk mengakses dan menggunakan website dengan menampilkan pada tampilan browser Anda untuk tujuan transaksi jual beli produk-produk Kami. Dalam penggunaan website, Anda akan diberi kata sandi dan akun yang unik untuk memungkinkan Anda mengakses bagian-bagian tertentu dari website ini. Setiap kali Anda menggunakan kata sandi atau akun tersebut, Anda akan dianggap memiliki otorisasi untuk mengakses dan menggunakan website ini dengan cara yang sesuai dengan syarat dan ketentuan Syarat Penggunaan ini dan Kami tidak memiliki kewajiban untuk menyelidiki otorisasi atau sumber dari akses tersebut atau penggunaan website. Dengan demikian, Anda setuju bahwa Anda senantiasa memperhatikan ketentuan larangan yang berlaku dalam penggunaan website ini, yaitu sebagai berikut :</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <div style={{ textAlign: 'justify' }}>
                Anda dilarang untuk melakukan serta tidak akan membantu orang lain untuk mereproduksi, mendistribusikan, menampilkan, menjual, menyewakan, mengirimkan, membuat karya turunan dari, menerjemahkan, memodifikasi, merekayasa balik, membongkar, menguraikan atau mengeksploitasi website ini atau sebagian dari itu.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <div style={{ textAlign: 'justify' }}>
                Anda dilarang untuk menjual dan memindah-tangankan akun Anda, profil, atau item yang terkait dengan Layanan kecuali secara tegas diizinkan secara tertulis oleh Kami.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <div style={{ textAlign: 'justify' }}>
                Anda dilarang untuk menggunakan setiap informasi yang diberikan pada website atau membuat penggunaan website untuk kepentingan komersial dan atau bisnis lain, kecuali secara tegas diizinkan secara tertulis oleh Kami.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <div style={{ textAlign: 'justify' }}>
                Anda dilarang untuk meniru atau menggambarkan diri Anda dengan orang lain atau badan usaha lain pada saat Anda memberikan data dan informasi Anda kepada Kami dalam penggunaan website dan layanan Kami.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <div style={{ textAlign: 'justify' }}>
                Anda dilarang untuk mengirimkan iklan, materi promosi, email spam, serta merusak operasi layanan dengan cara mengupload virus, worm atau kode berbahaya lainnya.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <div style={{ textAlign: 'justify' }}>
                Anda dilarang untuk melakukan serta tidak akan membantu orang lain untuk mengasosiasikan, mencitrakan, mengaitkan, menghubungkan, mengafiliasikan dengan suatu website yang berisi konten-konten sebagai berikut :</div>
            <ol>
                <li style={{ textAlign: 'justify' }}>Konten yang melanggar hak kekayaan intelektual dan hak kepemilikan lainnya.</li>
                <li style={{ textAlign: 'justify' }}>Konten yang mengandung unsur pronografi atau cabul atau kesusilaan, perjudian, penghinaan, fitnah dan atau pencemaran nama baik, pemerasan</li>
                <li style={{ textAlign: 'justify' }}>dan atau pengancaman, penipuan, pemalsuan.</li>
                <li style={{ textAlign: 'justify' }}>Konten yang menimbulkan dan atau mendorong timbulnya atau memfasilitasi rasa kebencian, permusuhan, pengancaman, penghasutan, pelecehan dan mengobarkan suatu ketegangan antar perorangan, pihak manapun atau kelompok tertentu, baik berdasarkan suku, agama, ras dan antar golongan (SARA) ataupun gender atau orientasi seksual, asal negara, etnis, kebangsaan, keterbatasan fisik dan keyakinan.</li>
                <li style={{ textAlign: 'justify' }}>Konten-konten terlarang lainnya berdasarkan peraturan perundang-undangan yang berlaku.Atas dasar kebijaksanaan Kami dalam hal setiap perilaku Anda selaku pelanggan termasuk dan atau dianggap melanggar hukum dan ketentuan yang berlaku serta merupakan pelanggaran Persyaratan Penggunaan, atau berbahaya bagi kepentingan Kami, maka atas setiap pelanggaran yang terjadi tersebut mengakibatkan pencabutan secara serta merta ijin akses yang diberikan dalam ketentuan ini tanpa pemberitahuan terlebih dahulu kepada Anda dan Kami berhak untuk menolak layanan, menghentikan akun, dan atau membatalkan pesanan yang Anda lakukan, termasuk dan tidak terbatas untuk mengungkapkan identitas dalam database Anda kepada pihak yang berwenang atas dasar suatu surat perintah yang sah. Anda harus bertanggung-jawab terhadap semua kerugian yang Kami maupun afiliasi dan mitra atau rekanan Kami alami, termasuk dan tidak terbatas terhadap klaim pihak ketiga, beserta biaya-biaya lainnya yang mungkin muncul sebagai akibat yang ditimbulkan dari perilaku Anda. Konten yang disediakan di website ini adalah semata-mata untuk tujuan informasi. Dengan Anda menyetujui ketentuan ini, maka sekaligus membebaskan Kami dari tanggung jawab atas setiap klaim, sanggahan, kerusakan, atau kerugian yang diakibatkan dari penggunaan website atau layanan yang terkandung di dalamnya.</li>
            </ol>
            <br />
            <b>PERUBAHAN ATURAN PRIVASI</b><br />
            <br />
            <div style={{ textAlign: 'justify' }}>
                Kami berhak untuk melakukan perubahan, penambahan dan atau pengurangan seluruh ataupun sebagian ketentuan dari aturan privasi ini dari waktu ke waktu tanpa pemberitahuan terlebih dahulu dan segala perubahan yang Kami lakukan akan Kami beritahukan melalui website www.pigijo.com. Kami mengharapkan agar Anda dapat secara berkala memeriksa mengenai kebijakan ini. Dengan mengakses dan menggunakan layanan Kami, maka secara langsung Kami menganggap Anda sudah mengerti dan menyetujui segala kebijakan privasi yang tertera di halaman website Kami saat itu juga.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <br />
            <b>HUKUM YANG BERLAKU</b><br />
            <br />
            <div style={{ textAlign: 'justify' }}>
                Apabila Anda mengakses website ini di luar Indonesia, maka atas penggunaan website dan layanan yang Kami berikan akan tunduk dan diatur oleh dan atau ditafsirkan sesuai dengan hukum yang berlaku di Indonesia, tanpa mempengaruhi adanya prinsip-prinsip konflik hukum yang berlaku.</div>
            <div style={{ textAlign: 'justify' }}>
                <br /></div>
            <br />
            <b>NEWSLETTER</b><br />
            <br />
            <div style={{ textAlign: 'justify' }}>
                Newsletter adalah suatu sarana yang kami sediakan untuk mempermudah proses komunikasi dan pemberian informasi penyediaan produk dan layanan kami serta informasi lainnya yang menurut kami akan menarik bagi Anda. Newsletter yang kami kirimkan akan Anda terima secara otomatis dan berkala melalui email yang Anda daftarkan, setelah Anda selesai membuat pemesanan dan atau akun pribadi pada website pigijo.com.</div>
            <div style={{ textAlign: 'justify' }}>
                Anda dapat memberhentikan fitur ini dengan cara yang mudah dan bebas biaya, melalui manage subcription di dalam menu profil atau dengan menggunakan link "Unsubcribe" di email yang anda terima.</div>
            </div>
          </div>
        </div>
        </>
      );
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

export default connect(null, mapDispatchToProps)(Privacy);