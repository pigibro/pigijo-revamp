import React,{ Component } from 'react';
import { connect } from "react-redux";
import { modalToggle } from '../../../stores/actions'

class TermsAndConditions extends Component {
  constructor(props) {
      super(props)
      this.toggle = this.toggle.bind(this)
  }
  toggle(e){
    e.preventDefault();
    this.props.dispatch(modalToggle(false));
  }
  render() {
      return (
        <>
        <div className="modal-header">
          <span className="h4 text-title">Terms and Conditions</span>
        </div>
        <div className="modal-panel ">
          <div className="panel-body">
            <div>
              <div style={{ textAlign: 'left' }}>
              Syarat dan ketentuan ini, dapat berubah dari waktu ke waktu, berlaku untuk semua layanan, langsung atau tidak langsung (melalui distributor) yang tersedia online, melalui perangkat seluler apa pun, melalui email, atau telepon. Dengan mengakses, menelusuri, dan menggunakan website maupun mobile apps kami atau salah satu aplikasi kami melalui platform apa pun (setelahnya disebut “Platform”) dan/atau dengan menyelesaikan reservasi, Anda mengetahui dan menyetujui untuk membaca, memahami, dan menyetujui syarat dan ketentuan yang tercantum di bawah ini (termasuk kebijakan privasi).<br />
              <br />
              <blockquote style={{ border: 'none', margin: '0 0 0 40px', padding: 0 }}>
                  <li>Layanan</li>
              </blockquote>
              <br />
              <ul><ul>
                  <ul>
                      <li>Layanan yang disediakan Pigijo adalah rencana perjalanan online dan aktivitas. Anda dapat menelusuri aktivitas (banyak hari, satu hari, beberapa jam), akomodasi, rental mobil, dan tour guide.</li>
                      <li>Pigijo semaksimal mungkin mengkurasi informasi yang tampil dan sangat berhati-hati dalam memberikan layanan. Kami tidak menjamin semua informasi yang diberikan akurat, lengkap, benar dan terbaru. Pigijo tidak bertanggung jawab atas setiap kesalahan, gangguan, informasi yang tidak sesuai, palsu, dan tidak tersampaikan.</li>
                  </ul>
              </ul>
              </ul>
              <br />
              <blockquote style={{ border: 'none', margin: '0 0 0 40px', padding: 0 }}>
                  <li>Pembatalan</li>
              </blockquote>
              <br />
              <ul><ul>
                  <ul>
                      <li>Dengan melakukan pemesanan dan pembayaran dari situs maupun aplikasi Pigijo, Anda menerima dan setuju dengan syarat dan ketentuan dari Supplier terkait, terutama berkaitan dengan kebijakan pembatalan. Pigijo tidak bertanggung jawab atas setiap pelanggaran syarat dan ketentuan yang disepakati atau perubahan akibat adanya permintaan khusus Anda, sehingga harap syarat dan ketentuan dibaca dengan baik.</li>
                  </ul>
              </ul>
              </ul>
              <br />
              <blockquote style={{ border: 'none', margin: '0 0 0 40px', padding: 0 }}>
                  <li>Harga Produk</li>
              </blockquote>
              <br />
              <ul><ul>
                  <ul>
                      <li>Harga yang ditampilkan kepada Anda sudah terperinci dan termasuk biaya dari metode pembayaran yang dipilih.&nbsp;</li>
                      <li>Pigijo berusaha memberikan harga terbaik atau lebih rendah dan atau promosi dan waktu ke waktu. Setiap harga dan promosi memiliki syarat dan ketentuan masing-masing yang dapat menyebabkan perubahan di kebijakan pembatalan, pemesanan dan pengembalian uang. Sehingga harap syarat dan ketentuan dibaca dengan baik.</li>
                      <li>Pigijo dapat merubah harga sewaktu waktu namun perubahan harga terjadi jika setelah Anda mendapatkan booking confirmation.</li>
                  </ul>
              </ul>
              </ul>
              <br />
              <blockquote style={{ border: 'none', margin: '0 0 0 40px', padding: 0 }}>
                  <li>Pengembalian Dana</li>
              </blockquote>
              <br />
              <ul><ul>
                  <ul>
                      <li>Setiap harga yang tercantum di Pigijo hanya tersedia dengan ketentuan tertentu dan harga tersebut dapat berubah tergantung pada ketersediaan pemesanan, lamanya pemesanan dan/atau jumlah inventaris. Harga yang tersedia dapat mencakup pajak tambahan dan biaya lainnya lagi tapi dalam keadaan tertentu mungkin tidak termasuk pajak dan biaya jasa lainnya (seperti biaya tips untuk pemandu wisata, biaya fasilitas&nbsp;</li>
                      <li>lainnya (jika ada), dan biaya-biaya lainnya yang timbul diluar dari informasi layanan produk kami); Anda setuju bahwa mereka bertanggung jawab untuk memverifikasi total biaya yang harus dibayar dan persyaratan lainnya dan rincian ketika email konfirmasi dikirim ke Anda. Anda harus memverifikasi pemesanan dalam lembar pemesanan; Anda dapat membatalkan pemesanan setiap saat sebelum konfirmasi akhir dilakukan. Harga yang ditampilkan terperinci sehingga Anda dapat melihat jumlah yang harus dibayar, biaya tambahan apa pun yang disebabkan penggunaan kartu kredit atau biaya antar bank yang dikenakan untuk biaya pengiriman akan dibebankan kepada Anda dan jika terdapat kekurangan dalam jumlah yang dibayarkan, maka Pigijo dapat memberikan notifikasi email mengenai kekurangan dalam jumlah yang harus dibayarkan Anda. Pengembalian dana mungkin tidak segera setelah pembayaran dari customer terjadi, tergantung pada metode awal pembayaran. Anda dapat menghubungi layanan pelanggan Pigijo untuk rincian lebih lanjut mengenai perkiraaan durasi untuk menerima pengembalian dana Anda dan kami akan membantu Anda sebaik yang kami mampu.</li>
                      <li>Sehubungan dengan pembatalan, jika termasuk, ada biaya yang ditahan oleh Pigijo sebagai biaya admin atau mengganti biaya akibat pembatalan (misal biaya transfer antar bank). Keterangan lebih lanjut mengenai biaya yang ditahan maupun biaya yang harus dibayarkan ke Supplier dapat menghubungi layanan pelanggan Pigijo (selama waktu operasional layanan pelanggan yang ditentukan oleh Pigijo).</li>
                  </ul>
              </ul>
              </ul>
              <br />
              <blockquote style={{ border: 'none', margin: '0 0 0 40px', padding: 0 }}>
                  <li>Data Anda (Tamu)</li>
              </blockquote>
              <br />
              <ul><ul>
                  <ul>
                      <li>Pada saat Anda membuat pemesanan atau mengakses informasi Akun Anda, Anda akan menggunakan akses Secure Server Layer (SSL) akan mengenkripsi informasi yang Anda kirimkan melalui Website ini.</li>
                      <li>Walaupun Pigijo akan menggunakan upaya terbaik untuk memastikan keamanannya, Pigijo tidak bisa menjamin seberapa kuat atau efektifnya enkripsi ini dan Pigijo tidak dan tidak akan bertanggung jawab atas masalah yang terjadi akibat pengaksesan tanpa ijin dari informasi yang Anda sediakan.</li>
                      <li>Kami menganggap privasi Anda sebagai hal yang penting.</li>
                      <li>Pada saat Anda membuat pemesanan di Pigijo, Kami akan mencatat dan menyimpan informasi dan data pribadi Anda. Pada prinsipnya, data Anda akan Kami gunakan untuk menyediakan Produk dan memberi Layanan kepada Anda. Kami akan menyimpan setiap data yang Anda berikan, dari waktu ke waktu, atau yang Kami kumpulkan dari penggunaan Produk dan Layanan Kami. Data pribadi Anda yang ada pada Kami, dapat Kami gunakan untuk keperluan internal kami untuk membantu Kami dikemudian hari dalam memberi pelayanan kepada Anda. Sehubungan dengan itu, Kami dapat mengungkapkan data Anda kepada group perusahaan di mana Pigijo tergabung didalamnya, Mitra penyedia Produk, perusahaan lain yang merupakan rekanan dari Pigijo, agen perjalanan, badan pemerintah dan badan peradilan yang berwenang, di jurisdiksi manapun.</li>
                  </ul>
              </ul>
              </ul>
          </div>
          </div>
          </div>
        </div>
        </>
      );
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

export default connect(null, mapDispatchToProps)(TermsAndConditions);