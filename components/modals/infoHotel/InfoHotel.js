import React, { Component } from 'react'
import {Modal, Row, Col} from 'react-bootstrap'

export default class InfoHotel extends Component {
  render() {
    return (
         <div className="modal-panel main-login">
            <div className="row content-row">
              <div className="col-lg-6 col-md-6 col-sm-6 login-form">
                <div className="content">
                  <a className="close" href="#"><i className="ti-close" /></a>
                  <h4 className="text-title">Log in to<br />your account</h4>
                  <p>Login to plan and book more for your next gateway!</p>
                  <div className="footer">Dont have an Account? <a href="">Register Now</a><br/>or need to <a href="/register/verification/email/RESENDVERIF" >resend verification email?</a></div>
                </div>{/* content */}
              </div>{/* login-form */}

              <div className="col-lg-6 col-md-6 col-sm-6 login-bg">
                <div className="thumb">
                  {/* <img src={sideImage} alt=""/> */}
                </div>
                <div className="bg-title">
                  <h1 className="text-title">In Pigijo, everyday is weekend. Trip on a whim.</h1>
                </div>
              </div>
            </div>
          </div>
 
    )
  }
}
