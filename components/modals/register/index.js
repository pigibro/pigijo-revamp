import React,{ Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux"
import { modalToggle } from '../../../stores/actions'
import LoginForm from './form';

class RegisterModal extends Component {
  static async getInitialProps (ctx) {
      return {pathname:ctx.pathname}
  }
  constructor(props) {
      super(props)
      this.toggle = this.toggle.bind(this)
  }
  toggle(e){
    e.preventDefault();
    this.props.dispatch(modalToggle(false));
  }
  render() {
      return (
        <div className="modal-panel main-login">
            <div className="row content-row">
              <div className="col-lg-6 col-md-6 col-sm-6 login-form">
                <div className="content">
                  <a className="close" href="#" onClick={
                    this.toggle
                  }><i className="ti-close" /></a>
                  <h4 className="text-title">Log in to<br />your account</h4>
                  <p>Login to plan and book more for your next gateway!</p>
                  <LoginForm/>
                  <div className="footer">Dont have an Account? <a href="">Register Now</a><br/>or need to <a href="/register/verification/email/RESENDVERIF" >resend verification email?</a></div>
                </div>{/* content */}
              </div>{/* login-form */}

              <div className="col-lg-6 col-md-6 col-sm-6 login-bg">
                <div className="thumb">
                  {/* <img src={sideImage} alt=""/> */}
                </div>
                <div className="bg-title">
                  <h1 className="text-title">In Pigijo, everyday is weekend. Trip on a whim.</h1>
                </div>
              </div>
            </div>
          </div>
      );
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch,
  pathname: null,
});

export default connect(null, mapDispatchToProps)(LoginModal);