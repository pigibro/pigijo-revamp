import React, { PureComponent } from 'react';
import validate from './validate';
import { Field as ReduxField, reduxForm } from 'redux-form';
import { get, isEmpty } from "lodash"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { register, setSuccessMessage, modalToggle } from '../../../stores/actions';

const renderField = ({
  input, placeholder, type, meta: { touched, error }, title
}) => (
    <div className="form-group">
      <span className="text-label">{title}</span>
      <input className="form-control" {...input} placeholder={placeholder} type={type}/>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);
class RegisterForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      globalError: "",
      isDisabled: false,
    }
  }
 
  render(){
    const { handleSubmit, register, modalToggle, setSuccessMessage } = this.props;
    const { globalError, isDisabled } = this.state;
    return(
     
      <form className="form-action" onSubmit={ handleSubmit(value => {
        this.setState({isDisabled: true});
        register(value).then(result => {
            if (get(result, 'meta.code') === 200) {
              setSuccessMessage('Registration successfully, please check your email.')
              modalToggle(false);
            }else{
              this.setState({globalError: get(result, 'meta.message') })
            }
            this.setState({isDisabled: false});
          });
        })
      }
      >
        <ReduxField
         className="form-control"
         component={renderField}
         name="email"
         type="email"
         title="Email"
         placeholder="Your Email"/>
         <div className="row">
          <div className="col-md-4 col-lg-4 col-xs-12">
            <div className="form-group">
              <span className="text-label">Salutation</span>
              <ReduxField name="gender" className="form-control" component="select">
                <option value="mr">Mr</option>
                <option value="mrs" >Mrs</option>
                <option value="ms" >Miss</option>
              </ReduxField>
            </div>
          </div>
          <div className="col-md-8 col-lg-8 col-xs-12">
            <ReduxField
            className="form-control"
            component={renderField}
            name="firstname"
            type="input"
            title="Firstname"
            placeholder="First Name"/>
          </div>
        </div>
        <ReduxField
        className="form-control"
        component={renderField}
        name="lastname"
        type="input"
        title="Lastname"
        placeholder="Last Name"/>
        <ReduxField
        className="form-control"
        component={renderField}
        name="phone"
        type="input"
        title="Phone Number"
        placeholder="Phone"/>
        <ReduxField
        className="form-control"
        component={renderField}
        name="password"
        type="password"
        title="Password"
        placeholder="Password"/>
        <ReduxField
        className="form-control"
        component={renderField}
        name="password_confirmation"
        type="password"
        title="Confirm Password"
        placeholder="Confirm Password"/>
        {!isEmpty(globalError) &&
          <div className="global-error mb1">
            <span className="text-danger">{globalError}</span>
          </div>
        }
        <button className="btn btn-block btn-primary btn-submit" type="submit" disabled={isDisabled}>Register</button>
        
      </form>
    )
  }
}


function mapStateToProps(state, ownProps) {
  return {
    initialValues: {
      gender: 'mr'
    }
  }
}


const mapDispatchToProps = dispatch => ({
  register: bindActionCreators(register, dispatch),
  setSuccessMessage: bindActionCreators(setSuccessMessage, dispatch),
  modalToggle: bindActionCreators(modalToggle, dispatch)
});

RegisterForm = reduxForm({
  form: "register_form",
  enableReinitialize : true,
  validate,
})(RegisterForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterForm);