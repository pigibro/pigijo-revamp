import React,{ Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux"
import { modalToggle } from '../../../stores/actions'
import LoginForm from './login-form';
import ForgotPasswordForm from './forgot-form';
import RegisterForm from './register-form';
import ResendForm from './resend-form';

class LoginModal extends Component {
  constructor(props) {
      super(props)
      this.state={
        step: 'login'
      }
      this.toggle = this.toggle.bind(this)
      this.changeStep = this.changeStep.bind(this)
  }
  toggle(e){
    e.preventDefault();
    this.props.dispatch(modalToggle(false));
  }
  changeStep(e,step = 'login'){
    e.preventDefault();
    this.setState({
      step:step
    });
  }
  render() {
    const {step} =this.state;
      return (
        <div className="modal-panel main-login">
            <div className="row content-row">
              <div className="col-lg-6 col-md-6 col-sm-6 login-form">
                <div className="content">
                  <a className="close" href="#" onClick={
                    this.toggle
                  }><i className="ti-close" /></a>
                  {step === 'login' && 
                  <>
                    <h4 className="text-title">Log in to<br />your account</h4> 
                    <p>Login to plan and book more for your next gateway!</p>
                    <LoginForm changeStep={this.changeStep}/>
                    <div className="footer">Dont have an Account? <a href="" onClick={(e) => this.changeStep(e,'register')}>Register Now</a><br/>or need to <a href="" onClick={(e) => this.changeStep(e,'resend')}>Resend verification email?</a></div>
                  </>
                  }
                  {step === 'forgot' && 
                  <>
                    <h4 className = "text-title" > Forgot Your Password ?</h4>
                    <p>Enter your email below and we will send you a password reset link to the email address.</p> 
                    <ForgotPasswordForm/>
                    <div className="footer">Already have an Account? <a href="" onClick={(e) => this.changeStep(e,'login')}>Log in here</a></div>
                  </>
                  }
                  {step === 'register' && 
                  <>
                    <h4 className="text-title">Register</h4>
                    <p>Register with your email address to join us as Pigijo Member. It takes less then a minute.</p>
                    <RegisterForm/>
                    <div className="footer">Already have an Account? <a href="" onClick={(e) => this.changeStep(e,'login')}>Log in here</a></div>
                  </>
                  }
                  {step === 'resend' &&
                  <>
                    <h4 className="text-title">Resend Verification Email</h4>
                    <p>You can resend into your Email if you have failed to verify your email.</p>
                    <ResendForm/>
                    <div className="footer">Already have an Account? <a href="" onClick={(e) => this.changeStep(e,'login')}>Log in here</a></div>
                  </>
                  }
                  
                </div>{/* content */}
              </div>{/* login-form */}

              <div className="col-lg-6 col-md-6 col-sm-6 login-bg">
                <div className="thumb">
                  {/* <img src={sideImage} alt=""/> */}
                </div>
                <div className="bg-title">
                  <h1 className="text-title">In Pigijo, everyday is weekend. Trip on a whim.</h1>
                </div>
              </div>
            </div>
          </div>
      );
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

export default connect(null, mapDispatchToProps)(LoginModal);