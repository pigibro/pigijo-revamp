import React, { PureComponent } from 'react';
import validate from './validate';
import { Field as ReduxField, reduxForm } from 'redux-form';
import { get, isEmpty } from "lodash"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { forgot, modalToggle } from '../../../stores/actions';
import Loading from '../../shared/loading';

const renderField = ({
  input, placeholder, type, meta: { touched, error }, title
}) => (
    <div className="form-group">
      <span className="text-label">{title}</span>
      <input className="form-control" {...input} placeholder={placeholder} type={type}/>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

class ForgotPasswordForm extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      globalSuccess: "",
      globalError: "", 
      isLoading: false,
    }
  }
  render(){
    const { handleSubmit, forgot, modalToggle } = this.props;
    const { globalError, globalSuccess, isLoading } = this.state;
    return(
      <form className="form-action" onSubmit={ handleSubmit(value => {
        this.setState({isLoading: true })
        forgot(value).then(result => {
            if (get(result, 'meta.code') === 200) {
              // this.setState({globalError: result.meta.message })
              // modalToggle(true,'message', get(result, 'meta.message'));
              this.setState({globalSuccess: get(result, 'meta.message') })
            }else{
              this.setState({globalError: get(result, 'meta.message') })
            }
            this.setState({isLoading: false })
          });
        })
      }
      >
        <ReduxField
         className="form-control"
         component={renderField}
         name="email"
         type="email"
         title="Email"
         placeholder="Your Email"/>
         
        <button className="btn btn-block btn-primary btn-submit" type="submit" disabled={isLoading}>Forgot Password</button>
        {!isEmpty(globalError) &&
          <div className="global-error">
            <span className="text-danger">{globalError}</span>
          </div>
        }
        {!isEmpty(globalSuccess) &&
          <div className="global-error mt1">
            <span className="text-success">{globalSuccess}</span>
          </div>
        }
      </form>
    )
  }
}

const mapStateToProps = state => {
  return {}
};

const mapDispatchToProps = dispatch => ({
  forgot: bindActionCreators(forgot, dispatch),
  modalToggle: bindActionCreators(modalToggle, dispatch)
});

ForgotPasswordForm = reduxForm({
  form: "forgot_password_form",
  validate
})(ForgotPasswordForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPasswordForm);