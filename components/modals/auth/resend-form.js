import React, { PureComponent } from 'react';
import validate from './validate';
import { Field as ReduxField, reduxForm } from 'redux-form';
import { get, isEmpty } from "lodash"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { resendVerification, setSuccessMessage, modalToggle } from '../../../stores/actions';

const renderField = ({
  input, placeholder, type, meta: { touched, error }, title
}) => (
    <div className="form-group">
      <span className="text-label">{title}</span>
      <input className="form-control" {...input} placeholder={placeholder} type={type}/>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

class ResendForm extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      globalError: "",
      globalSuccess: "",
      isDisabled: false,
    }
  }

  render(){
    const { handleSubmit, resendVerification } = this.props;
    const { globalError,globalSuccess,isDisabled } = this.state;
    return(
     
      <form className="form-action" onSubmit={ handleSubmit(value => {
        this.setState({isDisabled: true });
        resendVerification(value).then(result => {
            if (get(result, 'meta.code') === 200) {
              this.setState({globalSuccess: get(result, 'meta.message'),globalError: ''});
            }else{
              this.setState({globalError: get(result, 'meta.message') ,globalSuccess: ''});
            }
            this.setState({isDisabled: false });
          });
        })
      }
      >
        <ReduxField
         className="form-control"
         component={renderField}
         name="email"
         type="email"
         title="Email"
         placeholder="Your Email"/>
        {!isEmpty(globalError) &&
          <div className="global-error mb1">
            <span className="text-danger">{globalError}</span>
          </div>
        }
        {!isEmpty(globalSuccess) &&
          <div className="global-error mb1">
            <span className="text-success">{globalSuccess}</span>
          </div>
        }
        <button className="btn btn-block btn-primary btn-submit" type="submit" disabled={isDisabled}>Resend Email</button>
        
      </form>
    )
  }
}

const mapStateToProps = state => {
  return {}
};

const mapDispatchToProps = dispatch => ({
  resendVerification: bindActionCreators(resendVerification, dispatch)
});

ResendForm = reduxForm({
  form: "resend_form",
  validate
})(ResendForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResendForm);