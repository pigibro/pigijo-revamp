const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Email field shouldn’t be empty';
  }
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.firstname) {
    errors.firstname = 'First Name field shouldn’t be empty';
  }
  if (!values.lastname) {
    errors.lastname = 'Last Name field shouldn’t be empty';
  }
  if (!values.phone) {
    errors.phone = 'Phone Number field shouldn’t be empty';
  }else if(isNaN(Number(values.phone))){
    errors.phone = 'Phone Number must be a number';
  }
  if (!values.password) {
    errors.password = 'Password field shouldn’t be empty';
  }
  if (!values.password_confirmation) {
    errors.password_confirmation = 'Confirm Password field shouldn’t be empty';
  }else if (values.password_confirmation !== values.password) {
    errors.password_confirmation = 'Confirm Password Didn`t match with password';
  }

  return errors;
};

export default validate;
