import React, { PureComponent } from 'react';
import validate from './validate';
import { Field as ReduxField, reduxForm } from 'redux-form';
import { flatten, get, isEmpty, map, values } from "lodash"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { Router } from '../../../routes'
import { login, setSuccessMessage, getProfile, modalToggle } from '../../../stores/actions';
import { setCookie } from '../../../utils/cookies';

const renderField = ({
  input, placeholder, type, meta: { touched, error }, title
}) => (
    <div className="form-group">
      <span className="text-label">{title}</span>
      <input className="form-control" {...input} placeholder={placeholder} type={type}/>
      {touched && <span className="text-danger">{error}</span>}
    </div>
);

class LogInForm extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      globalError: ""
    }
  }

  render(){
    const { handleSubmit, login, changeStep, setSuccessMessage, getProfile, modalToggle } = this.props;
    const { globalError } = this.state;
    return(
     
      <form className="form-action" onSubmit={ handleSubmit(value => {
        login(value).then(result => {
            if (get(result, 'meta.code') === 200) {              
              setCookie('__jotkn', get(result,'data.token'));
              setCookie('__jogtkn', get(result,'data.guestToken'));
              getProfile(get(result,'data.guestToken'));
              modalToggle(false);
              setSuccessMessage('Berhasil Login');
            }else{
              this.setState({globalError: result.meta.message })
            }
          });
        })
      }
      >
        <ReduxField
         className="form-control"
         component={renderField}
         name="email"
         type="email"
         title="Email"
         placeholder="Your Email"/>

        <ReduxField
         className="form-control"
         component={renderField}
         name="password"
         type="password"
         title="Password"
         placeholder="Your Password"/>
        {!isEmpty(globalError) &&
          <div className="global-error">
            <span className="text-danger">{globalError}</span>
          </div>
        }
        <div className="checkbox-wrapper">
          <a className="link" href="#!" onClick={(e) => changeStep(e,'forgot')}>Forgot password?</a><br/>
        </div>
        <button className="btn btn-block btn-primary btn-submit" type="submit">Log In</button>
        
      </form>
    )
  }
}

const mapStateToProps = state => {
  return {}
};

const mapDispatchToProps = dispatch => ({
  login: bindActionCreators(login, dispatch),
  getProfile: bindActionCreators(getProfile, dispatch),
  setSuccessMessage: bindActionCreators(setSuccessMessage, dispatch),
  modalToggle: bindActionCreators(modalToggle, dispatch),
});

LogInForm = reduxForm({
  form: "login_form",
  validate
})(LogInForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogInForm);