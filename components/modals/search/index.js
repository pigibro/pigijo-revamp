import React from "react";
import Search from '../../home/autocomplete-search';

const  SearchModal = ({handleClose, dispatch})=> {
		return (
			<div className="modal-panel main-login" style={{ paddingBottom: "100%" }}>
				<div className="row">
					<div className="col-lg-12 col-md-12 col-sm-12 login-form " >
						<div className="content">
							<a className="close" href="#" onClick={handleClose}>
								<i className="ti-close" />
							</a>
              <br/>
              <br/>
							<Search dispatch={dispatch} />
						</div>
					</div>
				</div>
			</div>
		);
	}
export default SearchModal