import React,{ Component } from 'react';
import {  Modal  } from 'react-bootstrap';
import { connect, bindActionCreators } from "react-redux";
import { modalToggle } from '../../stores/actions'
import AuthModal from './auth';
import MessageModal from './message';
import InfoHotel from './infoHotel/InfoHotel';
import SearchModal from './search';
import PaymentModal from './payment';
import TermCondition from './static/term-n-condition';
import PrivacyPolicy from './static/privacy';
import Xtremesale from '../modal-xtremesale';


class ModalContainer extends Component {
  constructor( props ) {
    super(props);
  }
  handleClose=()=> {
    this.props.dispatch(modalToggle(false));
  }

  render() {
    let className = 'modal-dialog-centered modal-lg';
    const {type, message ,isOpen} = this.props;
    if(type === 'message'){
      className = 'modal-dialog-centered modal-sm'
    }else if (type === 'searchbox') {
      className = 'modal-dialog-full modal-lg'
    }else if(type === 'checkout-payment'){
      className = 'modal-dialog-centered modal-md'
    }else if(type === 'term-and-condition' || type === 'privacy-policy'){
      className = 'modal-dialog-centered modal-lg'
    }
      return (
      <Modal show={isOpen} onHide={this.handleClose} backdrop dialogClassName={className}>
        {type === 'xtremesale' && <Xtremesale/>}
        {type === 'auth' && <AuthModal/> }
        {type === 'message' && <MessageModal message={message} /> }
        {type === 'info-hotel' && <InfoHotel /> }
        {type === 'checkout-payment' &&
          <>
            <a className="close" onClick={e => {e.preventDefault(); this.handleClose()}}><i className="ti-close"/></a>
            <PaymentModal />
          </>
        }
        {type === 'term-and-condition' &&
          <>
            <a className="close" onClick={e => {e.preventDefault(); this.handleClose()}}><i className="ti-close"/></a>
            <TermCondition />
          </>
        }
        {type === 'privacy-policy' &&
          <>
            <a className="close" onClick={e => {e.preventDefault(); this.handleClose()}}><i className="ti-close"/></a>
            <PrivacyPolicy />
          </>
        }
        {type === 'searchbox' && <SearchModal handleClose={this.handleClose} dispatch={this.props.dispatch} /> }
      </Modal>
      );
  }
}
const mapStateToProps = state => ({
  type: state.modal.type,
  isOpen: state.modal.isOpen,
  message: state.modal.message
})

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer);