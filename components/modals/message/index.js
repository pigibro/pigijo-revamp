import React,{ Component } from 'react';
import { connect } from "react-redux";
import { modalToggle } from '../../../stores/actions'

class MessageModal extends Component {
  constructor(props) {
      super(props)
      this.toggle = this.toggle.bind(this)
  }
  toggle(e){
    e.preventDefault();
    this.props.dispatch(modalToggle(false));
  }
  render() {
      const {message} = this.props;
      return (
        <div className="modal-panel ">
          <div className="panel-body">
            <p style={{textAlign:"center"}}>{message}</p>
            <div className="row mt2">
              <div className="col-xs-8 col-xs-offset-2">
                <a className="btn btn-block btn-primary btn-sm btn-cus" href="" onClick={this.toggle}>OK</a>
              </div>
            </div>
          </div>
        </div>
      );
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

export default connect(null, mapDispatchToProps)(MessageModal);