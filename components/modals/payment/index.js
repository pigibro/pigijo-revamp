import React,{ Component } from 'react';
import { connect } from "react-redux";
import { get, map } from 'lodash';
import { modalToggle, getPaymentList, checkoutPayment } from '../../../stores/actions'
import { getCookie } from '../../../utils/cookies'

class PaymentModal extends Component {
  constructor(props) {
      super(props)
      this.state = {
        data: [],
        isLoading: false,
        paymentUrl: '',
        redirect: false,
      }
      this.toggle = this.toggle.bind(this)
  }
  componentDidMount(){
    const {dispatch} = this.props;
    this.setState({isLoading: true});
    dispatch(getPaymentList()).then(result => {
      if (get(result, 'meta.code') === 200) {
        this.setState({
          data: get(result,'data'), 
          isLoading:false,
        });
      }
      else{
        this.setState({
          data: [],
          isLoading:false,
        })
      }
    })
  }
  onChange = (id) =>{
    const ctoken = getCookie('__jocrtkn');
    const {dispatch} = this.props;
    dispatch(checkoutPayment({token:ctoken, payment_method_id: id})).then(result => {
      if (get(result, 'meta.code') === 200) {
        const data = get(result, 'data.original.data');
        window.location.assign(data.url);
      }
    });
  }
  toggle(e){
    e.preventDefault();
    this.props.dispatch(modalToggle(false));
  }
  render() {
      const {data, isLoading, redirect, paymentUrl} = this.state;
      return (
        <>
        <div className="modal-header">
          <span className="h4 text-title">Choose Payment</span>
        </div>
        <div className="modal-panel ">
          <div className="panel-body">
            {isLoading && 'Loading...'}
            {!isLoading && map(data, (item, index) => (
                <div className="col-lg-12 col-md-12 col-sm-12" key={index}>
                  <div className="radio list-payment">
                  <input type="radio" id={item.id} value={item.link_rewrite} name="payment" onChange={() => this.onChange(item.id)}/>
                    <label htmlFor={item.id} style={{ width: "100%" }}>
                      <img src={item.image} alt="" />
                      <div className="text-wrapper">
                        <h5 className="text-title">{item.name}</h5>
                        <span className="text-sm">{item.description}</span>
                      </div>
                    </label>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
        </>
      );
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

export default connect(null, mapDispatchToProps)(PaymentModal);