const express = require("express");
const next = require("next");
const compression = require("compression");
const session = require("express-session");
const { parse } = require('url');
const { join } = require("path");
const routes = require("./routes");
const dev = process.env.NODE_ENV !== "production";
const axios = require("axios");

require("dotenv").config();

const port = process.env.PORT || 5000;

const app = next({ dev });
const handler = routes.getRequestHandler(app, ({ req, res, route, query }) => {
  app.render(req, res, route.page, query)
})

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(compression());
    server.use(
      session({
        secret: "pigibro team",
        resave: true,
        saveUninitialized: true,
        cookie: {}
      })
    );
    server.get('*', (req, res) => {
      const parsedUrl = parse(req.url, true)
      const rootStaticFiles = ['/robots.txt', '/sitemap.xml', '/favicon.ico']
      if (rootStaticFiles.indexOf(parsedUrl.pathname) > -1) {
        const path = join(__dirname, 'static', parsedUrl.pathname)
        return app.serveStatic(req, res, path)
      }
      return handler(req, res)
    })

    server.use(handler);


    server.listen(port, err => {
      if (err) throw err;
      console.log("> Ready on http://localhost:" + port);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
