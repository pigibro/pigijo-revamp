require('dotenv').config()     
const css = require("@zeit/next-css");
const sass = require("@zeit/next-sass");
const optimizedImages = require('next-optimized-images');
const withPlugins = require('next-compose-plugins');
const path = require('path')
const Dotenv = require('dotenv-webpack')

const { PHASE_PRODUCTION_BUILD } = require('next/constants');


module.exports = withPlugins([
    [optimizedImages],
    [css, {
      cssModules: true,
      distDir: "build"
    }],
    [sass, {
      cssModules: false,
      cssLoaderOptions: {
        localIdentName: '[path]___[local]___[hash:base64:5]',
      },
      [PHASE_PRODUCTION_BUILD]: {
        cssLoaderOptions: {
          localIdentName: '[hash:base64:8]',
        },
      }
    }]
  ],{
  webpack: config => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    }
    config.plugins = [
      ...config.plugins,
      new Dotenv({
        path: path.join(__dirname, '.env')
      })
    ]

    return config
  },
  publicRuntimeConfig: {
    API_URL: process.env.API_URL,
    APP_KEY: process.env.APP_KEY,
    APP_SECRET: process.env.APP_SECRET,
    PORT: process.env.PORT,
    PREFERRED_AIRLINE: process.env.PREFERRED_AIRLINE,
    GA_ID: process.env.GA_ID,
    SOCKET_URL: process.env.SOCKET_URL,
    SEASON_TAG: process.env.SEASON_TAG,
    PAYMENT_URL: process.env.PAYMENT_URL
  }
})
