const routes = require("next-routes")();
routes

  .add("profile", "/profile", "profile")
  .add("favorites", "/favorites", "favorites")
  .add("eticket", "/eticket", "eticket")
  .add("traveller", "/traveller", "traveller")
  .add("event", "/event/:slug", "event")
  .add("activity", "/p/:slug", "activity")
  .add("my-booking-detail", "/my-booking-detail/:slug", "my-bookingDetail")
  .add("itinerary", "/itinerary", "itinerary")
  .add("rent-car", "/rent-car", "rent-car")
  .add("rent-car-location", "/rent-car/:type/:slug", "rent-car")
  .add("accommodation", "/accommodation", "accommodation")
  .add("accommodation-location", "/accommodation/l/:type/:slug", "accommodation")
  .add({
    name: "confirmationDetail",
    pattern: "/confirmation-detail",
    page: "confirmation-detail"
  })
  .add("checkout", "/checkout", "contact-details")
  .add("cart-list", "/cart-list", "cart")
  // .add({name: 'confirmation-detail', pattern: '/confirmation-detail', page: 'confirmation-detail'})
  .add({
    name: "acc-detail",
    pattern: "/accommodation/detail/:slug",
    page: "acc-detail"
  })
  .add({ name: "my-booking", pattern: "/my-booking", page: "my-booking" })
  // .add({name: 'category-child', pattern: '/c/:slug/:child', page: 'category'})
  // .add({name: 'flight', pattern: '/c/flight', page: 'category'})
  // .add({name: 'accommodation', pattern: '/c/accommodation', page: 'category'})
  // .add({name: 'vehicle', pattern: '/c/vehicle', page: 'category'})
  // .add({name: 'homestay', pattern: '/c/homestay', page: 'category'})
  // .add({name: 'restaurant', pattern: '/c/restaurant', page: 'category'})
  // .add({name: 'destination', pattern: '/c/destination', page: 'category'})
  // .add({ name: "tour-package", pattern: "/c/tour", page: "tour" })
  .add({ name: "how-to-book", pattern: "/how-to-book", page: "howtobook" })
  .add({ name: "how-to-plan", pattern: "/how-to-plan", page: "howtoplan" })
  .add({ name: "term-and-condition", pattern: "/term-and-condition", page: "termcondition" })
  .add({ name: "privacy-policy", pattern: "/privacy-policy", page: "privacypolicy" })
  .add({ name: "thankyou", pattern: "/thankyou/:no_transactions", page: "thankyou" })
  .add({ name: "thank-you", pattern: "/thankyou", page: "thankyou" })
  .add({ name: "about-us", pattern: "/about-us", page: "aboutus" })
  .add({ name: "contact-us", pattern: "/contact-us", page: "contactus" })
  .add({ name: "local-experience", pattern: "/c/:slug", page: "tour" })
  .add({ name: "place", pattern: "/place/:slug", page: "place" })
  .add({ name: "places", pattern: "/places", page: "places" })
  .add({ name: "places-by-location", pattern: "/places/:type/:slug", page: "places" })
  // .add({ name: "our-assistance", pattern: "/c/:slug", page: "tour" })
  .add({ name: 'promo', pattern: '/promo/:type/:slug', page: 'promo' })
  .add({ name: "location", pattern: "/l/:type/:slug", page: "location" })
  .add({ name: "location-by-category", pattern: "/l/:type/:slug/:category", page: "location" })
  .add({ name: "awaiting-payment", pattern: "/awaiting-payment", page: "awaiting-payment" })
  .add({ name: "payment-failed", pattern: "/payment-failed", page: "payment-failed" })
  .add({ name: "payment-success", pattern: "/payment-success", page: "payment-success" })
  .add({
    name: "planning-itineraray",
    pattern: "/planning-itinerary/:planning_id",
    page: "planning"
  })
  .add({
    name: "planning-days",
    pattern: "/planning-days/:planning_id",
    page: "planning-days"
  })
  .add({
    name: "register-verification",
    pattern: "/register/verification/email/:verifyToken",
    page: "register-verification"
  })
  .add({
    name: "forgot-password",
    pattern: "/user/password/forgot/new_password/:verifyToken",
    page: "forgot-password"
  })
  .add("index", "/")
  .add({ name: "community-list", pattern: "/community-list", page: "community-list" })
  .add({ name: "community", pattern: "/community/:slug", page: "community" })
  .add({ name: "community-home", pattern: "/community/home/:slug", page: "community-home" })
  .add({ name: "community-local-experience", pattern: "/community/local-experiences/:slug", page: "community-local-experience" })
  .add({ name: "community-events", pattern: "/community/events/:slug", page: "community-events" })
  .add({ name: "community-local-transports", pattern: "/community/local-transports/:slug", page: "community-local-transports" })
  .add({ name: "community-imi", pattern: "/communities/imi", page: "community-imi" })
  .add({ name: "city-search", pattern: "/s/city", page: "city-search" })
  // .add({ name: "imi-registration", pattern: "/imi-registration"})
  // .add({ name: 'imi-pusat-form', pattern: "/imi-registration/imi/:type"})
  // .add({ name: 'imi-atlet-form', pattern: "/imi-registration/atlet-nasional"})
  .add({ name: 'imi-tokoh-otomotif', pattern: "/ipo-registration" })
  .add({ name: 'imi-detail-profile', pattern: "/ipo-registration/profile/:uid" })
  .add({ name: 'imi-thankyou', pattern: "/ipo-registration/thankyou" })
  .add({ name: 'imi-verification', pattern: "/ipo-registration/activation/:uid" })
  .add({ name: "road-trip", pattern: "/campaign/promo-valentine" })
  .add({ name: "press-release", pattern: "/press-release" })
  .add({ name: "become-partner", pattern: "/become-partner" })
  .add({ name: "how-to-use", pattern: "/how-to-use" })
  .add({ name: "local-experiences", pattern: "/local-experiences" })
  .add({ name: 'flight', pattern: "/flight", page: "flight" })
  .add({ name: 'register', pattern: "/registration", page: "register" })
  // .add({ name: 'wisata-sehat-pulau-harapan', pattern: "/wisata-sehat-pulau-harapan", page: "wisata-sehat-pulau-harapan" })
  .add({ name: 'explore-belitung-3d2n', pattern: "/explore-belitung-3d2n", page: "new-normal-lombok-3d2n" });

module.exports = routes;
