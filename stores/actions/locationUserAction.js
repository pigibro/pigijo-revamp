import { SET_LOCATION } from "../actionTypes"
import axios from 'axios'

export const getLocationUser = () => async (dispatch) => {
	  await axios.get('https://ipapi.co/json/')
	  .then((response) => {
      let data = response.data;
      dispatch({
        type: SET_LOCATION,
        data: data
      });
      return true

	}).catch((error) => {
    // dispatch(setErrorMessage(error));
    return false
	});
};