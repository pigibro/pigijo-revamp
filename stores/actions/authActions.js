import { get } from 'lodash'
import {
  AUTH_GET_PROFILE,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT,
  AUTH_FORGOT_SUCCESS,
  AUTH_REGISTER_SUCCESS,
  AUTH_SET_TOKEN,
  AUTH_VERIFIED,
  CART_CLEAR,
  AUTH_RESEND_SUCCESS,
} from "../actionTypes";

import { apiCall, apiUrl } from "../../services/request";
import { removeCookie } from "../../utils/cookies";


const getGuid = () => {
    var nav = window.navigator;
    var screen = window.screen;
    var guid = nav.mimeTypes.length;
    guid += nav.userAgent.replace(/\D+/g, '');
    guid += nav.plugins.length;
    guid += screen.height || '';
    guid += screen.width || '';
    guid += screen.pixelDepth || '';

    return guid;
}

export const getProfile = (token) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + "/user/profile",
    data: {
      headers: {
        'Guest-Token': token
      }
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: AUTH_GET_PROFILE,
      payload: {
        data: res.data
      }
    });
    return res;
  }
  return res;
};

export const login = value => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + "/user/login",
    data: {
      data: {
        email: value.email,
        password: value.password,
        device_id: getGuid()
      }
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: AUTH_LOGIN_SUCCESS,
      payload: res.data
    });
    return res;
  }
  return res;
};

export const logout = () => dispatch => {
  removeCookie('__jotkn');
  removeCookie('__jocrtkn');
  dispatch({type: AUTH_LOGOUT});
  dispatch({type: CART_CLEAR});
};

export const register = value => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + "/user/register",
    data: {
      data: value
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: AUTH_REGISTER_SUCCESS,
      payload: res.data
    });
    return res;
  }
  return res;
};

export const forgot = value => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + "/user/password/forgot",
    data: {
      data: value,
      headers: {
        'Guest-Token': null
      }
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: AUTH_FORGOT_SUCCESS,
      payload: res.data
    });
    return res;
  }
  return res;
};

export const resendVerification = value => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + "/user/register/verifikasi/email/resend",
    data: {
      data: value
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: AUTH_RESEND_SUCCESS,
      payload: res.data
    });
    return res;
  }
  return res;
};

export const setToken = (token,gtoken) => dispatch => {
  dispatch({
    type: AUTH_SET_TOKEN,
    payload: {
      token: token,
      gtoken: gtoken
    }
  });
};




export const verify = token => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + "/user/register/verifikasi/email",
    data: {
      data: {
        verifikasi_token: token
      }
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: AUTH_VERIFIED,
      payload: res.data
    });
    return res;
  }
  return res;
};

export const submitNewPass = params => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/user/password/forgot/new_password/${params.token}`,
    data: {
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};


