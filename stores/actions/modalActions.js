import {
  MODAL_SHOW
} from "../actionTypes";

export const modalToggle = (bool = false, type = '', message= '') => async dispatch => {
  dispatch({
    type: MODAL_SHOW,
    payload: {bool,type,message}
  });
};
