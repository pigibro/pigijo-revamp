import { get } from 'lodash'
import { apiCall, apiUrl } from "../../services/request";

export const getTagList = () => async dispatch => {
    const dataReq = {
      method: "GET",
      url: apiUrl + `/v2/activity/list/tag`,
    };
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
      return res;
    }
    return res;
}; 

