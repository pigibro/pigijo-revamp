import { get } from 'lodash'
import { apiCall, apiUrl } from "../../services/request";

export const getCommunityList = (params) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/communities/list`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};
