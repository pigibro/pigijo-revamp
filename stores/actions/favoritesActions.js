import { FAVORITES } from '../actionTypes'
import { apiCall, apiUrl } from "../../services/request";
import { get } from 'lodash'
import { setSuccessMessage, setErrorMessage } from '../actions/alertActions'

export function dataWishList (data, paging, bool) {
  return {
    type: FAVORITES.LIST,
    data:data,
    paging:paging,
    isLoading:bool
  }
}

function arrayRemove(arr, value) {
   return arr.filter(function(ele){
       return ele !== value;
   });
}

export function addTemporaryList (data_old,data_new,action_type) {
     if(action_type === 'add'){
      return{
        type: FAVORITES.TEMPORARY_LIST,
        data : data_old !== null && data_old.lenght > 0 ? data_old.concat(data_new) : data_old
      }
    }else {
      return{
        type: FAVORITES.TEMPORARY_LIST,
        data : data_old !== null && data_old.lenght > 0 ? arrayRemove(data_old,data_new) : data_old
      }
    }
}

export function addTemporaryRemove(data_old,data_new,action_type) {
    if(action_type === 'add'){
      return{
        type: FAVORITES.TEMPORARY_REMOVE,
        data :  data_old !== null && data_old.lenght > 0 ? data_old.concat(data_new) : data_old
      }
    }else {
      return{
        type: FAVORITES.TEMPORARY_REMOVE,
        data : data_old !== null && data_old.lenght > 0 ? arrayRemove(data_old,data_new) : data_old
      }
    }
}

export const addWishList = (id, type , temporary_list = null , temporary_remove_list = null, token) => async dispatch =>  {
  const dataReq = {
      method: "POST",
      url: apiUrl + `/wishlist/add`,
      data: {
        headers: { 'Guest-Token': token },
        data: {
          product_id: id,
          product_type: type
        }
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
      dispatch(addTemporaryList(temporary_list, id+'_'+type,'add'));
      dispatch(addTemporaryRemove(temporary_remove_list, id+'_'+type,'remove'));
      dispatch(setSuccessMessage(get(res,'meta.message')));
      return res;
  }else{
      dispatch(setErrorMessage(get(res,'meta.message')));
  }
  return res;
};


export const  removeWishList = (id, type , temporary_list = null , temporary_remove_list = null, token) => async dispatch =>  {
  const dataReq = {
      method: "POST",
      url: apiUrl + `/wishlist/remove`,
      data: {
        headers: { 'Guest-Token': token },
        data: {
          product_id: id,
          product_type: type
        }
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
      dispatch(addTemporaryList(temporary_list, id+'_'+type,'remove'));
      dispatch(addTemporaryRemove(temporary_remove_list, id+'_'+type,'add'));
      dispatch(setSuccessMessage(get(res,'meta.message')));
      return res;
  }else{
      dispatch(setErrorMessage(get(res,'meta.message')));
  }
  return res;
};

export const wishlistList = (token,query = '') => async dispatch => {
  const dataReq = {
      method: "GET",
      url: apiUrl + `/wishlist/list`+query,
      data: { headers: { 'Guest-Token': token } }
  };
  dispatch({
    type: FAVORITES.LIST,
    isLoading:true
  })
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
        dispatch(dataWishList(res.data, res.paging, false ))
      return res;
  }
  return res;
};
