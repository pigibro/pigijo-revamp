import { get } from 'lodash'
import { apiCall, apiUrl } from "../../services/request";

export const sendContact = (params) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/contact`,
    data:{
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};
