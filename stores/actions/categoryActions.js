import { get } from 'lodash'

import { apiCall, apiUrl } from "../../services/request";

export const getDetailCategory = (slug) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/categories/${slug}`,
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const getCategories = (params = {}) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/categories`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

