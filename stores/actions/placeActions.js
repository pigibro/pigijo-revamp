import { get } from 'lodash'

import { apiCall, apiUrl } from "../../services/request";

export const getPlaces = (params = {}) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/place/list`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const getPlace = (params = {},slug) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/place/d/${slug}`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};