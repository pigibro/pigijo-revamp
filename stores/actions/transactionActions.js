import { get } from 'lodash'
import {
  CART_LIST_GET,
  CART_LIST_GET_SUCCESS,
} from "../actionTypes";

import { setCookie, removeCookie } from "../../utils/cookies"
import { apiCall, apiUrl } from "../../services/request";

export const praCheckout = (token) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/booking/pra-checkout`,
    data:{
      data: {token}
    }
  };
  // dispatch({type: CART_LIST_GET});
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    // dispatch({
    //   type: CART_LIST_GET_SUCCESS,
    //   payload: res
    // });
    return res;
  }
  return res;
};

export const checkoutDetails = (params) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/booking/checkout`,
    data:{
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const checkVoucher = (body) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + "/v2/coupon/check",
    data: {
      data: body
    }
  }
  const res = await dispatch(apiCall(dataReq))
  if(get(res, 'meta.code') === 200) {
    return res
  }
  return res
}

export const checkoutPayment = (params) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/booking/payment`,
    data:{
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    removeCookie('__jocrtkn');
    return res;
  }
  return res;
};
