
import { DRAFT, HEADERS } from "../_constants"
import axios from 'axios'
// import moment from 'moment'
import { ModalToggle, ModalType } from '../modal.action'

export function isRedirectDone(bool) {
  return {
    type: DRAFT.IS_REDIRECT,
    bool
  }
}
export const sendPlanningDone = (planning_id, planning_name , isPublic = 0) =>{
  return (dispatch) => {
    dispatch(isRedirectDone(true))

    const headers = {
      headers: {
        'App-Key': HEADERS.AppKey,
        'App-Secret': HEADERS.AppSecret,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Guest-Token': sessionStorage.getItem('guestToken')
      }
    }
    const url = HEADERS.URL+`planning/done`

    let data = new FormData();
    data.append('planning_id', planning_id)
    data.append('planning_name', planning_name)
    data.append('is_public', isPublic)

    axios.post(url,data, headers)
      .then(function (response) {
          localStorage.setItem("itinerary_name", planning_name)
        dispatch(isRedirectDone(false))
        // const data = response.data.data
        //  ("save success")
      })
      .catch(function (error) {
        // handle error
        dispatch(isRedirectDone(false))
        //  (error);
        if (error.response) {
          const meta = error.response.data.meta
          if (meta.code) {
            sessionStorage.setItem("msg", meta.message)
            dispatch(ModalType("message"))
            dispatch(ModalToggle(true))
          }
        }
      })

  }
}
