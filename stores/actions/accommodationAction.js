import { ACCOMMODATION } from "../actionTypes"
import { apiCall, apiUrl } from "../../services/request";
import { get } from "lodash";
import { setErrorMessage } from "../actions/alertActions";


export function accLoading(bool){
  return{
      type: ACCOMMODATION.IS_LOADING,
      bool
  }
}

export const getGuestHouse = (token,param) => async (dispatch) => {
    dispatch(accLoading(true))
    const dataReq = {
      method: "GET",
      url: apiUrl + `/guesthouse/search`,
      data: {
        headers: { "Guest-Token": token },
        params: param,
      },
    };
    const res = await dispatch(apiCall(dataReq));
    if (get(res, "meta.code") === 200) {
      dispatch({
        type: ACCOMMODATION.SET_GUEST,
        data: res.data,
      });
      dispatch(accLoading(false))
      return res;
    }
    return res;
  };

export const getHotelFirst = (param) => async (dispatch) => {
  dispatch(accLoading(true))
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/hotel/availability/first`,
    data: {
      data: param,
    },
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    dispatch(accLoading(false))
    return res;
  }
  return res;
}

export const getHotelPage = (param) => async (dispatch) => {
  dispatch(accLoading(true))
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/hotel/availability/page`,
    data: {
      data: param,
    },
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    dispatch(accLoading(false))
    return res;
  }
  return res;
}

export const getCancelPolicyHotel = (param) => async (dispatch) => {
  dispatch(accLoading(true))
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/hotel/cancelation/policy`,
    data: {
      data: param,
    },
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    dispatch(accLoading(false))
    return res;
  }
  return res;
}

export const getDetailGuestHouse = (token, hotel_id, param) => async (dispatch) => {
    dispatch(accLoading(true))
    const dataReq = {
      method: "GET",
      url: apiUrl + `/guesthouse/detail/${hotel_id}?`,
      data: {
        headers: { "Guest-Token": token },
        params: param,
      },
    };
    const res = await dispatch(apiCall(dataReq));
    if (get(res, "meta.code") === 200) {
      dispatch({
        type: ACCOMMODATION.SET_DETAIL_GUEST,
        data: res.data,
      });
      dispatch(accLoading(false))
      return res;
    } 
    return res;
  };
