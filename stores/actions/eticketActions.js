import { ETICKET } from "../actionTypes"
import { apiCall, apiUrl } from "../../services/request";
import { get } from 'lodash'
// import { setSuccessMessage, setErrorMessage } from '../actions/alertActions'

export function isLoading(bool){
    return{ type: ETICKET.IS_LOADING, isLoading:bool }
}

export const getBookTourList = (params = {}, token) => async dispatch => {
    let param = { headers: { 'Guest-Token': token } }
    const dataReq = {
        method: "GET",
        url: apiUrl + `/booking/list/tour` + params,
        data: param
    };
    dispatch({ type: ETICKET.SET_LIST_TOUR, isLoading:true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: ETICKET.SET_LIST_TOUR,
            data: res.data.result,
            paging:res.data.paging,
            isLoading:false
        });
        return res;
    } else{
        dispatch(isLoading(false));
    }
    return res;
};

export const getBookAccList = (params = {}, token) => async dispatch => {
    let param = { headers: { 'Guest-Token': token } }
    const dataReq = {
        method: "GET",
        url: apiUrl + `/booking/list/accommodation` + params,
        data: param
    };
    dispatch({ type: ETICKET.SET_LIST_ACC, isLoading:true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: ETICKET.SET_LIST_ACC,
            data: res.data.result,
            paging:res.data.paging,
            isLoading:false
        });
        return res;
    } else {
        dispatch(isLoading(false));
    }
    return res;
};

export const getBookRentCarList = (params = {}, token) => async dispatch => {
    let param = { headers: { 'Guest-Token': token } }
    const dataReq = {
        method: "GET",
        url: apiUrl + `/booking/list/rentcar` + params,
        data: param
    };
    dispatch({ type: ETICKET.SET_LIST_RENTCAR, isLoading:true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: ETICKET.SET_LIST_RENTCAR,
            data: res.data.result,
            paging:res.data.paging,
            isLoading:false
        });
        return res;
    } else {
        dispatch(isLoading(false));
    }
    return res;
};

export const getBookFlight = (params = {}, token) => async dispatch => {
    let param = { headers: { 'Guest-Token': token } }
    const dataReq = {
        method: "GET",
        url: apiUrl + `/booking/list/flight` + params,
        data: param
    };
    dispatch({ type: ETICKET.SET_LIST_FLIGHT, isLoading:true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: ETICKET.SET_LIST_FLIGHT,
            data: res.data.result,
            paging:res.data.paging,
            isLoading:false
        });
        return res;
    } else {
        dispatch(isLoading(false));
    }
    return res;
};