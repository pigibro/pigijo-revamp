import { get } from "lodash";
import QueryString from "query-string";
import {
  LOCAL_EXPERIENCES_GET,
  LOCAL_EXPERIENCES_GET_SUCCESS
} from "../actionTypes";

import { apiCall, apiUrl } from "../../services/request";
import { slugify } from '../../utils/helpers'

export const getLocalExp = (
  slug,
  params = { per_page: 25 }
) => async dispatch => {
  // const queryString = Object.keys(params)
  //   .map(key => key + "=" + params[key])
  //   .join("&");
  
  const dataReq = {
    method: "GET",
    url:
      apiUrl +
      `/v2/communities/${slug}/tours`
  };
  dispatch({ type: LOCAL_EXPERIENCES_GET });
  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    let tours = res.data.community_tours
    for (let i = 0; i < tours.length; i++) {
      tours[i].meta_keyword = tours[i].meta_keyword.split(',')
      tours[i].slug = slugify(`${tours[i].id} ${tours[i].product_name}`)
    }
    res.data.community_tours = tours
    dispatch({
      type: LOCAL_EXPERIENCES_GET_SUCCESS,
      payload: res
    });
    return res;
  }
  return res;
};
