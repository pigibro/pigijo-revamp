import { get } from 'lodash'
import {
  CITY_GET,
  CITY_GET_SUCCESS,
  COUNTRY_GET,
  COUNTRY_GET_SUCCESS
} from "../actionTypes";

import { apiCall, apiUrl } from "../../services/request";

export const getCountries = (params = {}) => async dispatch => {
  const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
  const dataReq = {
    method: "GET",
    url: apiUrl + `/search/list-country?`+queryString,
  };
  dispatch({type: COUNTRY_GET});
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: COUNTRY_GET_SUCCESS,
      payload: res
    });
    return res;
  }
  return res;
}

export const getLocations= (params = {}) => async dispatch => {
  const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
  const dataReq = {
    method: "GET",
    url: apiUrl + `/city/get?`+queryString,
  };
  dispatch({type: CITY_GET});
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: CITY_GET_SUCCESS,
      payload: res
    });
    return res;
  }
  return res;
};

export const getLocationDetail = (type, id) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/location/d/${type}/${id}`,
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
}

export const getAreas = (params = {}) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/location/area`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const getCities = (params = {}) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/location/city`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const getProvinces = (params = {}) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/location/province`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const getaCitiesHotel = (params = {}) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/hotel/cities/ID`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};