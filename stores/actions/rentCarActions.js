import { RENTCAR } from "../actionTypes";
import { apiCall, apiUrl } from "../../services/request";
import { get } from "lodash";
import { setErrorMessage } from "../actions/alertActions";
import axios from "axios";
import moment from "moment";

export function setRenCarLoading(bool) {
	return {
		type: RENTCAR.IS_LOADING,
		bool,
	};
}

export const getTypeCarDefault = (token) => async (dispatch) => {
	dispatch(getCarBrand(token));
	dispatch(getCarPrice(token));
	dispatch(getCarTransmission(token));
	dispatch(getCarType(token));
};

export const getCarListDefault = (token) => async (dispatch) => {
	dispatch(getCarList(token, "Bali", "", "", "", "", "", 1, "", "115.0919509", "-8.3405389", "", "province"));
	dispatch(getTypeCarDefault(token));
	//   axios.get('https://ipapi.co/json/')
	//   .then((response) => {
	//     let data = response.data;
	//     dispatch(getCarList(token, 'Bali','','','','','',1,'','115.0919509', '-8.3405389','','province'))
	//     // dispatch(getCarList(token, data.city,'','','','','',1,'',data.longitude, data.latitude,'','area'))

	// }).catch((error) => {
	//   dispatch(setErrorMessage(error));
	// });
};

export const getCarList = (
	token,
	location,
	transmission,
	brand,
	type,
	agency,
	price,
	page,
	startDate,
	dayFilter,
	location_type,
) => async (dispatch) => {
	const dataReq = {
		method: "GET",
		url: apiUrl + `/vehicle/search`,
		data: {
			headers: { "Guest-Token": token },
			params: {
				location: location,
				transmission: transmission,
				brand: brand,
				type: type,
				agency_name: agency,
				price: price,
				page: page,
				location_type: location_type,
			},
		},
	};
	dispatch(setRenCarLoading(true));
	const res = await dispatch(apiCall(dataReq));
	if (get(res, "meta.code") === 200) {
		dispatch({
			type: RENTCAR.SET_LIST,
			data: res.data,
			paging: res.paging,
			paramDetail: {
				name:location,
				transmission:transmission,
				brand:brand,
				type:type,
				agency:agency,
				price:price,
				page:page,
				startDate:startDate,
				dayFilter:dayFilter,
        		location_type:location_type,
			},
		});
		dispatch(setRenCarLoading(false));
		return res;
	} else {
		dispatch(setErrorMessage(get(res, "meta.message")));
	}
	return res;
};

export const getCarType = (token) => async (dispatch) => {
	const dataReq = {
		method: "GET",
		url: apiUrl + `/vehicle/type`,
		data: { headers: { "Guest-Token": token } },
	};
	const res = await dispatch(apiCall(dataReq));
	if (get(res, "meta.code") === 200) {
		dispatch({
			type: RENTCAR.SET_LIST_TYPE,
			dataType: res.data,
		});
		return res;
	}
	return res;
};

export const getCarBrand = (token) => async (dispatch) => {
	const dataReq = {
		method: "GET",
		url: apiUrl + `/vehicle/brand`,
		data: { headers: { "Guest-Token": token } },
	};
	const res = await dispatch(apiCall(dataReq));
	if (get(res, "meta.code") === 200) {
		dispatch({
			type: RENTCAR.SET_LIST_BRAND,
			dataBrand: res.data,
		});
		return res;
	}
	return res;
};

export const getCarTransmission = (token) => async (dispatch) => {
	const dataReq = {
		method: "GET",
		url: apiUrl + `/vehicle/tranmission`,
		data: { headers: { "Guest-Token": token } },
	};
	const res = await dispatch(apiCall(dataReq));
	if (get(res, "meta.code") === 200) {
		dispatch({
			type: RENTCAR.SET_LIST_TRANSMISSION,
			dataTrans: res.data,
		});
		return res;
	}
	return res;
};

export const getCarPrice = (token) => async (dispatch) => {
	const dataReq = {
		method: "GET",
		url: apiUrl + `/vehicle/price`,
		data: { headers: { "Guest-Token": token } },
	};
	const res = await dispatch(apiCall(dataReq));
	if (get(res, "meta.code") === 200) {
		dispatch({
			type: RENTCAR.SET_LIST_PRICE,
			dataPrice: res.data,
		});
		return res;
	}
	return res;
};
