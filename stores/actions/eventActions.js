import { get } from "lodash";
import { EVENT_GET, EVENT_GET_SUCCESS, HEADERS } from "../actionTypes";

import { apiCall, apiUrl } from "../../services/request";

export const getEvent = (params = {}) => async dispatch => {
  const queryString = Object.keys(params)
    .map(key => key + "=" + params[key])
    .join("&");
  const dataReq = {
    method: "GET",
    url: apiUrl + `/product/event?` + queryString
  };
  dispatch({ type: EVENT_GET });
  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    dispatch({
      type: EVENT_GET_SUCCESS,
      payload: res
    });
    return res;
  }
  return res;
};

export const getEventList = (params = {}) => async dispatch => {
  const queryString = Object.keys(params)
    .map(key => key + "=" + params[key])
    .join("&");
  const dataReq = {
    method: "GET",
    url: apiUrl + `/event/list?` + queryString
  };
  dispatch({ type: EVENT_GET });
  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    dispatch({
      type: EVENT_GET_SUCCESS,
      payload: res,
      paging: res.paging
    });
    return res;
  }
  return res;
};

export const getEventLoketCategories = (params = {}) => async dispatch =>{
    dispatch({ type: EVENT_GET });
    const url = HEADERS.URL+'event/categories'
    const dataReq = {
      method: "GET",
      url: url,
      data: {
        headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'App-Key': HEADERS.AppKey,
                    'App-Secret': HEADERS.AppSecret
                  }
      }
      
    };
    let res = await dispatch(apiCall(dataReq))
    if (get(res, "meta.code") === 200) {
      dispatch({
        type: EVENT_GET_SUCCESS,
        payload: res
      })
      return res
    }
    return res
    // axios.get(url,headers)
    // .then(function (response){
    //     dispatch(setLoketEventCategories(oldData.concat(response.data.data)))
    //     dispatch(isEventLoketLoading(false))
    //   })
    //   .catch(function (error) {
    //     dispatch(isEventLoketLoading(false))
    //     // handle error
    //     //  (error);
      // })
}

export const getEventDetail = (slug) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/event/detail/` + slug
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    return res;
  }
  return res;
};
