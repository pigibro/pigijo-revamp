import { get } from 'lodash'
import {
  CART_LIST_GET,
  CART_LIST_GET_SUCCESS,
} from "../actionTypes";

import { setCookie, getCookie, removeCookie } from "../../utils/cookies"
import { apiCall, apiUrl } from "../../services/request";

export const getCartList = (token, sort) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/cart/list?token=${token}&sort_by=${sort}`,
  };
  dispatch({type: CART_LIST_GET});
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: CART_LIST_GET_SUCCESS,
      payload: res
    });
    return res;
  }
  if (get(res, 'meta.code') === 401) {
    removeCookie('__jocrtkn');
    removeCookie('__jocrtlimit');
  }
  return res;
};

export const createToken = () => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/cart/token`,
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    setCookie('__jocrtkn', get(res,'data.token'));
    setCookie('__jocrtlimit', get(res,'data.timelimit'));
    return res;
  }
  return res;
};

export const storeCart = (params) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/cart/${params.category || 'activity'}/store`,
    data:{
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const storeCartRentCar = (params) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/cart/rent-car/store`,
    data:{
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const storeCartHotel = (params) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/cart/hotel/store`,
    data:{
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const storeCartHomestay = (params) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/cart/homestay/store`,
    data:{
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const storeCartPlaces = (params) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/cart/places/store`,
    data:{
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const deleteCart = (data) => async dispatch => {
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/cart/delete`,
    data:{
      data: data
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    if(data.delete ) {
      removeCookie('__jocrtkn');
      removeCookie('__jocrtlimit');
    }
    return res;
    
  }
  return res;
};

export const clearCart = () => async dispatch => {
  removeCookie('__jocrtkn');
  removeCookie('__jocrtlimit');
};
