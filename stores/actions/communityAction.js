import { COMMUNITY } from "../actionTypes"
import { apiCall, apiUrl } from "../../services/request"
import { get } from 'lodash'




export const httpReqGetDetailCommunity = (slug) => async dispatch => {
    const dataReq = {
      method: "GET",
      url: apiUrl + `/v2/communities/${slug}`
    };

    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
       dispatch({
        type: COMMUNITY.DETAIL,
        payload : res
      });
      return res;
    }
    return res;
  };

export const getEvents = (slug, queryString) => async dispatch => {
    const newQuery = queryString === undefined ? '' : queryString;
    const dataReq = {
      method: "GET",
      url: apiUrl + `/v2/community/${slug}${newQuery}/events`
    };

    try {
      const res = await dispatch(apiCall(dataReq));
      dispatch({
        type: COMMUNITY.GET_EVENT_SUCCESS,
        payload: res,
      })
    } catch (error) {
      
    }
    // if (get(res, 'meta.code') === 200) {
    //    dispatch({
    //     type: COMMUNITY.DETAIL,
    //     payload : res
    //   });
    //   return res;
    // }
    // return res;
  };