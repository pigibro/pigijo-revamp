import { get } from 'lodash'
import { FLIGHT_SET, FLIGHT_SET_DEPARTURE, FLIGHT_SET_RETURN } from '../actionTypes'

import { apiCall, apiUrl } from "../../services/request";

export const getAirportList = (params) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/flight/airport/ID`,
    data: { params }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const getAirlineList = () => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/flight/filter`,
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const postAirportList = (params = {}) => async dispatch => {
  
  const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6NTM0MywibG9naW4iOmZhbHNlfQ.U68EAZtQHg9hE1LXZm_HdS52K8nHCxfQBF9-njbLfYo';
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/flight/check/availbility`,
    data: {
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const sortFlightList = (flight_list, params, idx) => async dispatch => {
  let flights = flight_list[idx]
  switch(params) {
    case "highest_price" : 
      flights.sort((a, b) => (b.TotalFare - a.TotalFare))
      flight_list[idx] = flights
      return flight_list;
    case "lowest_price" : 
      flights.sort((a, b) => (a.TotalFare - b.TotalFare))
      flight_list[idx] = flights
      return flight_list;
    case "earliest_d_time": 
      flight_list[idx] = flights.slice().sort((a, b) => new Date(`${a.DepartDate} ${a.DepartTime}`) - new Date(`${b.DepartDate} ${b.DepartTime}`))
      return flight_list
    case "latest_d_time": 
      flight_list[idx] = flights.slice().sort((a, b) => new Date(`${b.DepartDate} ${b.DepartTime}`) - new Date(`${a.DepartDate} ${a.DepartTime}`))
        return flight_list
    case "earliest_a_time": 
      flight_list[idx] = flights.slice().sort((a, b) => new Date(`${a.ArriveDate} ${a.ArriveTime}`) - new Date(`${b.ArriveDate} ${b.ArriveTime}`))
      return flight_list
    case "latest_a_time":
      flight_list[idx] = flights.slice().sort((a, b) => new Date(`${b.ArriveDate} ${b.ArriveTime}`) - new Date(`${a.ArriveDate} ${a.ArriveTime}`))
      return flight_list
    default : return flight_list
  }
}

export const flighAddtoCart = (params = {}) => async dispatch => {
  const dataReq = {
    method: 'POST',
    url: apiUrl + '/v2/cart/flight/store',
    data: {
      data: params
    }
  }
  const res = await dispatch(apiCall(dataReq));
  if(get(res, 'meta.code') === 200) {
    return res
  }
  return res
};

export const flightCart = (token) => async dispatch => {
  const dataReq = {
    method: 'GET',
    url: apiUrl + '/v2/cart/list?token=' + token
  }

  const res = await dispatch(apiCall(dataReq));
  if(get(res, 'meta.code') === 200) {
    return res
  }
  return res
};

export const setDepart = (data) => async dispatch => {
  dispatch({
    type: FLIGHT_SET_DEPARTURE,
    payload: data
  })
  return data
};

export const setReturn = (data) => async dispatch => {
  dispatch({
    type: FLIGHT_SET_RETURN,
    payload: data
  })
  return data
}

export const getFareDetail = (params = {}) => async dispatch => {
  const dataReq = {
    method: 'POST',
    url: apiUrl + "/v2/flight/fare/detail",
    data: {
      data: params
    }
  }

  const res = await dispatch(apiCall(dataReq));
  if(get(res, 'meta.code') === 200) {
    return res
  }
  return res
}