import { get } from "lodash";
import { LOCAL_EVENTS_GET, LOCAL_EVENTS_GET_SUCCESS } from "../actionTypes";

import { apiCall, apiUrl } from "../../services/request";

export const getLocalEvents = (slug, params = {}) => async dispatch => {
  const queryString = Object.keys(params)
    .map(key => key + "=" + params[key])
    .join("&");
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/communities/` + slug  + '/events'
  };
  dispatch({ type: LOCAL_EVENTS_GET });

  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    dispatch({
      type: LOCAL_EVENTS_GET_SUCCESS,
      payload: res
    });

    return res;
  }
  return res;
};
