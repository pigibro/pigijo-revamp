import { get } from 'lodash'
import {
  BLOG_GET,
  BLOG_GET_SUCCESS
} from "../actionTypes";

import { apiBlogCall, apiBlogUrl } from "../../services/request";

export const getLatestBlog= (slug) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiBlogUrl + `/api/get_recent_posts?count=5`
  };
  dispatch({type: BLOG_GET});
  const res = await dispatch(apiBlogCall(dataReq));
  if(res){
    if (res.status === 'ok') {
      dispatch({
        type: BLOG_GET_SUCCESS,
        payload: res
      });
      return res;
    }
  }
  return res;
};