import { USER_BOOKING } from "../actionTypes";
import { apiCall, apiUrl } from "../../services/request";
import { get } from 'lodash';
import { setErrorMessage } from '../actions/alertActions'

export function listBookingLoading(bool) {
  return {
    type: USER_BOOKING.IS_LOADING,
    bool
  }
}

export const getBooking = (params = {}, token, data = {sort: {created_at: "DESC"}}) => async dispatch => {  
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/my-booking`,
    data: { 
      headers: { 
        'Authorization': `Bearer ${token}`      
      },
      params: params,
      data: data
     }
  };
  dispatch(listBookingLoading(true))
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: USER_BOOKING.SET,
      data: res.data,
      pagination: res.pagination
    })
    dispatch(listBookingLoading(false))
    return res;
  }else{
    dispatch(setErrorMessage(get(res,'meta.message')));
  }
  return res;
};



export const getBookingDetail = (id, token) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/my-booking/${id}`,
    data: { 
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
  };
  dispatch(listBookingLoading(true))
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: USER_BOOKING.SET_DETAIL,
      item: res.data
    })
    dispatch(listBookingLoading(false))
    return res;
  }else{
    dispatch(setErrorMessage(get(res,'meta.message')));
  }
  return res;
};
