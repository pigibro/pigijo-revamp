
import { ITINERARY } from "../actionTypes";
import { apiCall, apiUrl } from "../../services/request";
import { get } from 'lodash';
import { setErrorMessage } from '../actions/alertActions'

export function itieraryLoading(bool) {
  return {
      type: ITINERARY.IS_LOADING,
      bool
  }
}
export function itierarySetDetail(item, tour, hotel, rentCar) {
  return {
      type: ITINERARY.SET_DETAIL,
      item,
      tour, hotel, rentCar
  }
}
export const getItinerary = (params='', token) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/planning/list`+params,
    data: { headers: { 'Guest-Token': token } }
  };
  dispatch(itieraryLoading(true))
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type:ITINERARY.SET,
      data:res.data,
      paging:res.paging
  })
    dispatch(itieraryLoading(false))
    return res;
  }else{
    dispatch(setErrorMessage(get(res,'meta.message')));
  }
  return res;
};


export const getDetailPlanItinerary = (id, token) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/planning/${id}/detail`,
    data: { headers: { 'Guest-Token': token } }
  };
  dispatch(itieraryLoading(true))
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    const data = res.data
    sessionStorage.setItem('planData', JSON.stringify(data))
    localStorage.setItem('planId',data.planning_id)
    localStorage.setItem('planning_name',data.planning_name)
    if (localStorage.getItem('day') === null) localStorage.setItem('day', 0);
    let cart = data.cart
    let tourList = []
    let hotelList = []
    let rentCarList = []
    cart.map(item=>{
        return(
            hotelList.push(item.hotel.list),
            tourList.push(item.tour.list),
            rentCarList.push(item.rent_car.list)
        )
    })
    dispatch({
      type:ITINERARY.SET,
      data:res.data,
      tour:tourList[0],
      hotel:hotelList[0],
      rentCar:rentCarList[0]
  })
    dispatch(itieraryLoading(false))
    return res;
  }else{
    dispatch(setErrorMessage(get(res,'meta.message')));
  }
  return res;
};