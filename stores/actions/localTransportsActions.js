import { get } from "lodash";
import QueryString from "query-string";
import {
  LOCAL_TRANSPORTS_GET,
  LOCAL_TRANSPORTS_SUCCESS,
} from "../actionTypes";

import { apiCall, apiUrl } from "../../services/request";

export const getLocalTrans = (
  slug,
  params = { per_page: 25 }
) => async dispatch => {
  const dataReq = {
    method: "GET",
    url:
      apiUrl +
      `/v2/community/local-transports/${slug}?${QueryString.stringify(params)}`
  };
  dispatch({ type: LOCAL_TRANSPORTS_GET });
  const res = await dispatch(apiCall(dataReq));
  if (get(res, "meta.code") === 200) {
    dispatch({
      type: LOCAL_TRANSPORTS_SUCCESS,
      payload: res
    });
    return res;
  }
  return res;
};

