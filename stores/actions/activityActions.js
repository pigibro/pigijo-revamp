import { get, takeRight } from 'lodash'
import {
  ACTIVITY_GET,
  ACTIVITY_GET_SINGLE,
  ACTIVITY_GET_SUCCESS,
  ACTIVITY_PROMO_GET,
  ACTIVITY_PROMO_GET_SUCCESS,
  ACTIVITY_GET_SCHEDULES,
  ACTIVITY_GET_SCHEDULES_SUCCESS,
  IS_PREVIEWS
} from "../actionTypes";

import { apiCall, apiUrl } from "../../services/request";
import { urlImage } from '../../utils/helpers';

export const previewPhotos = (bool) =>  dispatch => {
  dispatch({
    type: IS_PREVIEWS,
    bool: bool
  });
};

export const advanceSearch = (params) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/search`,
    data:{
      params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const getActivity = (slug) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/activity/d/${slug}`,
  };
  dispatch({type: ACTIVITY_GET});
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    let imgTmp =[]
    let imgOthers = res.data.image_others
    imgOthers.map(item=>(
      imgTmp.push(urlImage(item.path+'/'+item.filename))
    ))
    dispatch({
      type: ACTIVITY_GET_SINGLE,
      payload: res,
      image_others:imgTmp
    });
    return res;
  }
  return res;
};

export const getActivities = (params) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/activity/list`,
    data:{
      params
    }
  };
  dispatch({type: ACTIVITY_GET});
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: ACTIVITY_GET_SUCCESS,
      payload: res
    });
    return res;
  }
  return res;
};

export const getSchedules = (params) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/v2/activity/schedule/list`,
    data:{
      params
    }
  };
  dispatch({type: ACTIVITY_GET_SCHEDULES});
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: ACTIVITY_GET_SCHEDULES_SUCCESS,
      payload: res
    });
    return res;
  }
  return res;
};

export const getActivityPromos = () => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/promo/list`
  };
  dispatch({type: ACTIVITY_PROMO_GET});
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    dispatch({
      type: ACTIVITY_PROMO_GET_SUCCESS,
      payload: res
    });
    return res;
  }
  return res;
};
