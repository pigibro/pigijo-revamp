
import { TRAVELLER } from "../actionTypes"
import { setSuccessMessage, setErrorMessage } from '../actions/alertActions'
import { apiCall, apiUrl } from "../../services/request";
import { get } from 'lodash'

export function isLoading(bool) {
    return {
        type: TRAVELLER.IS_LOADING,
        bool:bool
    }
}

export const getTraveller = (token) => async dispatch => {
    let param = { headers: { 'Guest-Token': token } }
    const dataReq = {
        method: "GET",
        url: apiUrl + `/user/contact/list`,
        data: param
    };
    dispatch({ type: TRAVELLER.SET, bool: true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: TRAVELLER.SET,
            data: res.data.result,
            bool: false
        });
        return res;
    } else {
        dispatch(isLoading(false));
    }
    return res;
};

export const getSingleTraveller = (token, id) => async dispatch => {
    let param = { headers: { 'Guest-Token': token } }
    const dataReq = {
        method: "GET",
        url: apiUrl + `/user/contact/${id}/detail`,
        data: param
    };
    dispatch({ type: TRAVELLER.SET_DETAIL, bool: true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: TRAVELLER.SET_DETAIL,
            data: res.data,
            bool: false,
            id
        });
        return res;
    } else {
        dispatch(isLoading(false));
    }
    return res;
};

export const removeTraveller = (token, id) => async dispatch => {
    let param = { data:{contact_id:id},headers: { 'Guest-Token': token } }
    const dataReq = {
        method: "POST",
        url: apiUrl + `/user/contact/remove`,
        data: param
    };
    dispatch({ type: TRAVELLER.SET_DETAIL, bool: true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: TRAVELLER.SET_DETAIL,
            bool: false
        });
        dispatch(setSuccessMessage(get(res, 'meta.message')));
        return res;
    } else {
        dispatch(isLoading(false));
        dispatch(setErrorMessage(get(res, 'meta.message')));
    }
    return res;
};

export const addTraveller = (firstname, lastname, salutation, phone, email, token) => async dispatch => {
    const dataReq = {
        method: "POST",
        url: apiUrl + `/user/contact/add`,
        data: {
            headers: { 'Guest-Token': token },
            data: {
                firstname: firstname,
                lastname: lastname,
                salutation: salutation,
                phone: phone,
                email: email
            }
        }
    };
    dispatch({ type: TRAVELLER.SET_DETAIL, bool: true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: TRAVELLER.SET_DETAIL,
            bool: false
        });
        dispatch(setSuccessMessage(get(res, 'meta.message')));
        return res;
    } else {
        dispatch(setErrorMessage(get(res, 'meta.message')));
        dispatch(isLoading(false));
    }
    return res;
};

export const editTraveller = (firstname, lastname, salutation, phone, email, id, token) => async dispatch => {
    const dataReq = {
        method: "POST",
        url: apiUrl + `/user/contact/edit`,
        data: {
            headers: { 'Guest-Token': token },
            data: {
                firstname: firstname,
                lastname: lastname,
                salutation: salutation,
                phone: phone,
                email: email,
                contact_id:id
            }
        }
    };
    dispatch({ type: TRAVELLER.SET_DETAIL, bool: true });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: TRAVELLER.SET_DETAIL,
            bool: false
        });
        dispatch(setSuccessMessage(get(res, 'meta.message')));
        return res;
    } else {
        dispatch(isLoading(false));
        dispatch(setErrorMessage(get(res, 'meta.message')));
    }
    return res;
};
