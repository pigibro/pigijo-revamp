import { get } from 'lodash'

import { apiCall, apiUrl } from "../../services/request";

export const getPaymentList = () => async dispatch => {
  const dataReq = {
    method: "GET",
    url: apiUrl + `/payment/list`,
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};