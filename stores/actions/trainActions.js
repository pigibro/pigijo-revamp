import { get } from 'lodash'
import { apiCall, apiUrl } from "../../services/request";

export const getStationList = (params) => async dispatch => {
    const dataReq = {
      method: "GET",
      url: apiUrl + `/v2/train/stations`,
      data: { params }
    };
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
      return res;
    }
    return res;
};

export const getTrainList = (params = {}) => async dispatch => {
  
  const dataReq = {
    method: "POST",
    url: apiUrl + `/v2/train/availability`,
    data: {
      data: params
    }
  };
  const res = await dispatch(apiCall(dataReq));
  if (get(res, 'meta.code') === 200) {
    return res;
  }
  return res;
};

export const trainAddtoCart = (params = {}) => async dispatch => {
  const dataReq = {
    method: 'POST',
    url: apiUrl + '/v2/cart/train/store',
    data: {
      data: params
    }
  }
  const res = await dispatch(apiCall(dataReq));
  if(get(res, 'meta.code') === 200) {
    return res
  }
  return res
};

export const trainCart = (token) => async dispatch => {
  const dataReq = {
    method: 'GET',
    url: apiUrl + '/v2/cart/list?token=' + token
  }

  const res = await dispatch(apiCall(dataReq));
  if(get(res, 'meta.code') === 200) {
    return res
  }
  return res
};