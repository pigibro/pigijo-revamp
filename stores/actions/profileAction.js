import { PROFILE_EDIT, SET_PROFILE } from "../actionTypes";
import { apiCall, apiUrl } from "../../services/request";
import { get } from 'lodash'
import { setSuccessMessage, setErrorMessage } from '../actions/alertActions'

export const sendEditProfile = (param) => async dispatch => {
    const dataReq = {
        method: "POST",
        url: apiUrl + `/user/edit`,
        data: param
    };
    dispatch({ type: SET_PROFILE });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: SET_PROFILE,
            payload: res,
        });
        dispatch(setSuccessMessage(get(res,'meta.message')));
        return res;
    }else{
        dispatch(setErrorMessage(get(res,'meta.message')));
    }
    return res;
};

export const sendNewPicture = (img) => async dispatch => {
    let param = { data: { 'image': img }, headers: { 'Guest-Token': token } }
    const dataReq = {
        method: "POST",
        url: apiUrl + `/user/profile_picture/edit`,
        data: param
    };
    dispatch({ type: PROFILE_EDIT });
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch({
            type: PROFILE_EDIT,
            payload: res,
        });
        dispatch(setSuccessMessage(get(res,'meta.message')));
        return res;
    }else{
        dispatch(setErrorMessage(get(res,'meta.message')));
    }
    return res;
};

export const sendPasswordEdit = (param) => async dispatch => {
    const dataReq = {
        method: "POST",
        url: apiUrl + `/user/password/edit`,
        data: param
    };
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
        dispatch(setSuccessMessage(get(res,'meta.message')));
        return res;
    }else{
        dispatch(setErrorMessage(get(res,'meta.message')));
    }
    return res;
};

export const getBookingList = (params) => async dispatch => {
    const dataReq = {
      method: "GET",
      url: apiUrl + `/v2/order/list`,
      data: { params },
      headers: {
        'Authorization': `Bearer ${token}`
      }
    };
    const res = await dispatch(apiCall(dataReq));
    if (get(res, 'meta.code') === 200) {
      return res;
    }
    return res;
  };