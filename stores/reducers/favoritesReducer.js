import { FAVORITES } from "../actionTypes";

const initialState = {
	list: [],
	temporary_list: [],
	temporary_remove: [],
  paging: null,
  isLoading:false
};

export default (state = initialState, action) => {

	switch (action.type) {
    case FAVORITES.LIST:    
      return { 
        ...state, 
        list: action.data, 
        paging: action.paging ,
        isLoading:action.isLoading
      };
      
    case FAVORITES.TEMPORARY_LIST:
      return { 
        ...state, 
        temporary_list: action.data 
      };
      
    case FAVORITES.TEMPORARY_REMOVE:
      return { 
        ...state, 
        temporary_remove: action.data 
      };
      
		default:
			return state;
	}
};
