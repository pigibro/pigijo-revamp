import * as actionTypes from "../actionTypes";

const initialState = {
  loading: false,
  loaded: false,
  data: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOCAL_EXPERIENCES_GET:
      return {
        ...state,
        loading: true
      };
    case actionTypes.LOCAL_EXPERIENCES_GET_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.data,
        pagination: action.payload.pagination
      };
    default:
      return state;
  }
};
