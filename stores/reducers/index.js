import { reducer as reduxFormReducer } from "redux-form";
import { combineReducers } from "redux";

import alertReducer from "./alertReducer";
import authReducer from "./authReducer";
import activityReducer from "./activityReducer";
import blogReducer from "./blogReducer";
import eventReducer from "./eventReducer";
import modalReducer from "./modalReducer";
import cityReducer from "./cityReducer";
import countryReducer from "./countryReducer";
import eTicketReducer from "./eTicketReducer";
import travellerReducers from "./travellerReducers";
import scheduleReducer from "./scheduleReducer";
import favoritesReducer from "./favoritesReducer";
import flightReducer from './flightReducer';
import myBookingReducer from "./myBookingReducer";
import listItineraryReducers from "./listItineraryReducers";
import rentCarReducers from "./rentCarReducer";
import communityReducer from "./communityReducer";
import locationReducer from "./locationReducer";
import accReducer from "./accommodationReducer";
import cartReducer from "./cartReducer";
import localExperiencesReducer from "./localExperiencesReducer";
import localEventsReducer from "./localEventsReducer";
import localTransportsReducer from "./localTransportsReducer";

const reducers = combineReducers({
  alert: alertReducer,
  form: reduxFormReducer,
  auth: authReducer,
  activity: activityReducer,
  schedules: scheduleReducer,
  blog: blogReducer,
  event: eventReducer,
  modal: modalReducer,
  city: cityReducer,
  country: countryReducer,
  eticket: eTicketReducer,
  traveller: travellerReducers,
  favorites: favoritesReducer,
  flights: flightReducer,
  myBooking: myBookingReducer,
  listItinerary: listItineraryReducers,
  rentCar: rentCarReducers,
  community: communityReducer,
  locationUser: locationReducer,
  guestHoust: accReducer,
  cart: cartReducer,
  localExperiences: localExperiencesReducer,
  localEvents: localEventsReducer,
  localTransports: localTransportsReducer
});

export default reducers;
