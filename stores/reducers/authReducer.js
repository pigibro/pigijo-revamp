import * as actionTypes from "../actionTypes";

const initialState = {
  token : null,
  gtoken: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_GET_PROFILE:
      return {
        ...state,
        profile: action.payload.data
      };
    case actionTypes.AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload.token
      };
    case actionTypes.AUTH_LOGOUT:
      return {
        ...state,
        token: null,
        profile: null
      };
    case actionTypes.AUTH_REGISTER_SUCCESS:
      return {
        ...state,
        registered: true
      };
    case actionTypes.AUTH_FORGOT_SUCCESS:
      return {
        ...state,
        forgot: true
      };
    case actionTypes.AUTH_SET_TOKEN:
      return {
        ...state,
        token: action.payload.token,
        gtoken: action.payload.gtoken
      };
    case actionTypes.AUTH_VERIFIED:
      return {
        ...state,
        verified: true
      };
    default:
      return state;
  }
};