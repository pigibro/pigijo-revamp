import * as actionTypes from "../actionTypes";

const initialState = {
  isOpen: false,
  type: '',
  message: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.MODAL_SHOW:
      return {
        ...state,
        isOpen: action.payload.bool,
        type: action.payload.type,
        message: action.payload.message
      };
    default:
      return state;
  }
};