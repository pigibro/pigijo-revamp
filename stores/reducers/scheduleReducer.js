import * as actionTypes from "../actionTypes";

const initialState = {
  loading: false,
  loaded: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ACTIVITY_GET_SCHEDULES:
      return {
        ...state,
        loading: true
      };
    case actionTypes.ACTIVITY_GET_SCHEDULES_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.data,
      };
    default:
      return state;
  }
};