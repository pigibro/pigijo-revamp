import * as actionTypes from "../actionTypes";

const initialState = {
  loading: false,
  loaded: false,
  isPreview:false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ACTIVITY_GET:
      return {
        ...state,
        loading: true
      };
    case actionTypes.ACTIVITY_GET_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.data,
        pagination: action.payload.pagination
      };
    case actionTypes.ACTIVITY_GET_SINGLE:
      return {
        ...state,
        loading: false,
        loaded: true,
        single: action.payload.data,
        image_others:action.image_others
      };
    case actionTypes.ACTIVITY_PROMO_GET:
      return {
        ...state,
        loading: true
      };
    case actionTypes.IS_PREVIEWS:
      return {
        ...state,
        isPreview: action.bool
      };
    case actionTypes.ACTIVITY_PROMO_GET_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.data,
        pagination: action.payload.pagination
      };
    default:
      return state;
  }
};