import { ITINERARY } from "../actionTypes"

const initialState = {
  list  : [],
  paging  : [],
  isLoading: false,
  hotel:[],
  tour:[],  
  rentcar:[],
  timeline:[],
  details:[]
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ITINERARY.SET:
      return Object.assign({}, state, {
        list: action.data,
        paging: action.paging,
      });
    case ITINERARY.SET_DETAIL:
      return Object.assign({}, state, {
        timeline: action.item.time_line,
        details: action.item,
        tour: action.tour,
        hotel:action.hotel,
        rentcar:action.rentcar,
      });
    case ITINERARY.IS_LOADING:
      return Object.assign({}, state, {
        isLoading: action.bool,
      })
    default:
      return state
  }
}
