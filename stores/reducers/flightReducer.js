import { 
    FLIGHT_SET, 
    FLIGHT_SET_DEPARTURE, 
    FLIGHT_SET_RETURN
} from "../actionTypes";

const initialState = {
	departure_flight: {},
    return_flight: {},
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FLIGHT_SET_DEPARTURE:    
            return { 
                ...state, 
                departure_flight: action.payload
            };
        case FLIGHT_SET_RETURN:    
            return { 
                ...state, 
                return_flight: action.payload
            };
        default:
            return state;
    }
}