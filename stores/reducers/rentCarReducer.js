import { RENTCAR } from "../actionTypes"

const initialState = {
  isLoading: false,
  list_car: [],
  dataType: [],
  dataBrand: [],
  dataTrans: [],
  dataPrice: [],
  paramDetail: [],
  pagination: [],
  cartDetail:[]
}

export default (state = initialState, action) => {
  switch (action.type) {
    case RENTCAR.SET_LIST:
      return Object.assign({}, state,{
        list_car: action.data,
        paramDetail: action.paramDetail,
        pagination: action.paging
      })
    case RENTCAR.IS_LOADING:
      return Object.assign({}, state,{
        isLoading: action.bool
      })
    case RENTCAR.SET_LIST_TYPE:
      return Object.assign({}, state,{
       dataType:  action.dataType
      })
    case RENTCAR.SET_LIST_BRAND:
      return Object.assign({}, state,{
        dataBrand: action.dataBrand
      })
    case RENTCAR.SET_LIST_TRANSMISSION:
      return Object.assign({}, state,{
        dataTrans: action.dataTrans
      })
    case RENTCAR.SET_LIST_PRICE:
      return Object.assign({}, state,{
        dataPrice: action.dataPrice
      })
    case RENTCAR.SET_CART_DETAIL:
      return Object.assign({}, state, {
        cartDetail: action.dataDetail
      })
    default:
      return state
  }
}