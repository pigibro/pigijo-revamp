import { USER_BOOKING } from "../actionTypes"

const initialState = {
  list  : [],
  pagination:[],
  isLoading: false,
  details:[],
  item:[]
}

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_BOOKING.SET:
      return {...state, 
        list: action.data,
        pagination:action.pagination
      };
    case USER_BOOKING.SET_DETAIL:
      return {...state, 
        details: action.item,
        item: action.item.booking_items
      };
    case USER_BOOKING.IS_LOADING:
      return {...state, 
        isLoading: action.bool
      }
    default:
      return state
  }
}