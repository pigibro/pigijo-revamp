import * as actionTypes from "../actionTypes";

const initialState = {
  loading: false,
  loaded: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.BLOG_GET:
      return {
        ...state,
        loading: true
      };
    case actionTypes.BLOG_GET_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.posts
      };
    default:
      return state;
  }
};