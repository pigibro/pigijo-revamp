import { TRAVELLER } from "../actionTypes"

const initialState = {
  list  : [],
  isLoading: false,
  details:null,
  id:null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TRAVELLER.SET:
      return {
        ...state, 
        list: action.data,
        isLoading:action.bool
      };
    case TRAVELLER.SET_DETAIL:
      return {
        ...state, 
        details: action.data,
        id: action.id,
        isLoading:action.bool
      };
    case TRAVELLER.IS_LOADING:
      return {
        ...state, 
        isLoading:action.bool
      };
    default:
      return state
  }
}