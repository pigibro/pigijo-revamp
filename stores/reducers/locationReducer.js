import { SET_LOCATION } from "../actionTypes"

export default (state = {userLocation  : null}, action) => {
  switch (action.type) {
    case SET_LOCATION:
      return {...state,userLocation: action.data};
    default:
      return state
  }
}
