import * as actionTypes from "../actionTypes";

const initialState = {
  loading: false,
  loaded: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CITY_GET:
      return {
        ...state,
        loading: true
      };
    case actionTypes.CITY_GET_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.data,
      };
    default:
      return state;
  }
};