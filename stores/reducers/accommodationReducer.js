import { ACCOMMODATION } from "../actionTypes"

const initialState = {
  list_GHouse: [],
  rooms  : [],
  all_photo: [],
  city:[],
  pagination: [],
  facilities:[],
  result:[],
  breadcrumb:null,
  primaryPhotos:'',
  policy:[],
  search_queries:null,
  isLoading: false,
}

export default (state = initialState, action) => {
  
  switch (action.type) {
    case ACCOMMODATION.SET_GUEST:
      return Object.assign({}, state, {
        list_GHouse: action.data.result,
        search_query: action.data.search_queries,
        pagination: action.data.pagination,
      });
    case ACCOMMODATION.SET_DETAIL_GUEST:
      return Object.assign({}, state, {
        breadcrumb: action.data.breadcrumb,
        rooms: action.data.results,
        primaryPhotos: action.data.primaryPhotos,
        all_photo: action.data.all_photo.photo,
        policy: action.data.policy,
        facilities: action.data.avail_facilities.avail_facilities,
      });
    case ACCOMMODATION.IS_LOADING:
      return Object.assign({}, state, {
        isLoading: action.bool
      });
    default:
      return state
  }
}
