import * as actionTypes from "../actionTypes";

const initialState = {
  loading: false,
  loaded: false,
  data: [],
  details:[],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CART_LIST_GET:
      return {
        ...state,
        loading: true
      };
    case actionTypes.CART_LIST_GET_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.data,
        details: action.payload.data.details,
      };
    default:
      return state;
  }
};