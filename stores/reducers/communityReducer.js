import { COMMUNITY } from "../actionTypes";

const initialState = {
  detail : null,
  eventList: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case COMMUNITY.DETAIL:
      return {
        ...state,
        detail : action.payload.data,
      };
    case COMMUNITY.GET_EVENT_SUCCESS:
      return {
        ...state,
        eventList : action.payload.data,
      }; 
    default:
      return state;
  }
};