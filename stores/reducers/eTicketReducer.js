import {ETICKET} from "../actionTypes";


const initialState = {
  hotel  : [],
  paging_hotel:null,

  tour  : [],
  paging_tour:null,

  rent_car: [],
  paging_car:null,

  flight: [],
  paging_flight:null,
  
  isLoading: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ETICKET.SET_LIST_TOUR:
    return {
      ...state,
      isLoading: action.isLoading,
      tour: action.data,
      paging_tour: action.paging
    };
      case ETICKET.SET_LIST_ACC:
      return {
        ...state,
        isLoading: action.isLoading,
        hotel: action.data,
        paging_hotel: action.paging
      };
      case ETICKET.SET_LIST_RENTCAR:
      return {
        ...state,
        isLoading: action.isLoading,
        data: action.data,
        pagination: action.paging
      };
      case ETICKET.SET_LIST_FLIGHT:
      return {
        ...state,
        isLoading: action.isLoading,
        data: action.data,
        pagination: action.paging
      };
    case ETICKET.IS_LOADING:
      return {...state, 
        isLoading: action.isLoading,
      };
    default:
      return state
  }
}