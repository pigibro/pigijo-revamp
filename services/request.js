import axios from "axios"
import { setErrorMessage } from "../stores/actions"
import { get, merge } from 'lodash'

import getConfig from "next/config"
const { publicRuntimeConfig } = getConfig()
const { API_URL, APP_KEY, APP_SECRET } = publicRuntimeConfig

export const apiBlogUrl = "https://blog.pigijo.com";
export const apiUrl = API_URL;

const appHeaderProperties = {
  "App-Key" : APP_KEY,
  "App-Secret" : APP_SECRET,
}

export const apiCall = ({ method, url, data = "" }) => async dispatch => {
  try {
    const response = await axios({
      method: method,
      url: url,
      data: data.data || "",
      headers: merge(data.headers, appHeaderProperties) || "",
      params: data.params || "",
      timeout: data.timeout || 0
    });
    return response.data;
  } catch (error) {
    if (error.response) {
      const { data } = error.response;
     if(error.response.status !== 400){
        if (get(data,'meta.message')){
          dispatch(setErrorMessage(get(data,'meta.message')));
        }else{
          dispatch(
            setErrorMessage(
              "Maaf sedang terjadi masalah dengan server kami. Mohon tunggu beberapa menit lagi 🙏"
            )
          );
        }
        return data;
     }
     return data;
    } else {
      dispatch(
        setErrorMessage(
          "Maaf sedang terjadi masalah dengan server kami. Mohon tunggu beberapa menit lagi 🙏"
        )
      );
    }
    return null;
  }
};

export const apiBlogCall = ({method, url, data = ""}) => async dispatch =>{
  try {
    const response = await axios({
      method: method,
      url: url,
      data: data.data || "",
      headers: merge(data.headers) || "",
      params: data.params || "",
      timeout: data.timeout || 0
    });
    return response.data;
  } catch (error) {
    if (error.response) {
      const { data } = error.response;
      if (get(data,'meta.message')){
        dispatch(setErrorMessage(get(data,'meta.message')));
      }else{
        dispatch(
          setErrorMessage(
            "Maaf sedang terjadi masalah dengan server kami. Mohon tunggu beberapa menit lagi 🙏"
          )
        );
      }
      return data;
    } else {
      dispatch(
        setErrorMessage(
          "Maaf sedang terjadi masalah dengan server kami. Mohon tunggu beberapa menit lagi 🙏"
        )
      );
    }
    return null;
  }
}