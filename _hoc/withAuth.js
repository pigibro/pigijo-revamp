import React from "react"
import { compose } from "redux"
import { connect } from "react-redux"

import Layout from "../components/layout"
import Router from 'next/router'
import { setCookie, getCookie } from "../utils/cookies"
import { initGA, logPageView } from "../utils/analytics"
import { setToken, getProfile, getCartList } from "../stores/actions";
import { getLocationUser } from "../stores/actions";

import { get, isEmpty } from 'lodash'

const withAuthentication = (permissions = [], customClass = "") => AuthComponent =>
  class Auth extends React.Component {
    static async getInitialProps(context) {
      const { req, store, isServer, asPath, locationUser } = context;
      let initialProps = {};
      const currentState = store.getState();
      const token = getCookie("__jotkn", req) || null;
      const gtoken = getCookie("__jogtkn", req) || null;
      const ctoken = getCookie("__jocrtkn", req) || null;
      
      if (token && gtoken) {
        await store.dispatch(setToken( token,gtoken ));
        if (isEmpty(get(currentState, 'auth.profile')))
          await store.dispatch(getProfile( gtoken ));
      }

      if (ctoken){
        await store.dispatch(getCartList(ctoken, 'schedule|ASC'));
      }
      // if (isEmpty(get(currentState, 'locationUser'))){
        // await store.dispatch(getLocationUser());
        
      // }
        
      // if (cartToken){
      //   await store.dispatch(getCart(cartToken));
      // }

      if (AuthComponent.getInitialProps) {
        initialProps = await AuthComponent.getInitialProps(context);
      }

      return { ...initialProps, asPath, locationUser, gtoken };
    }
    constructor(props) {
      super(props);
      this.state = {
        isLoading: true,
        isLogin: false,
        joToken: null,
        gToken: null,
        isPrivate: permissions.indexOf("PRIVATE") !== -1,
        customClass: customClass
      };
    }
    handleSuccessLogin = () => {
      this.setState({ isLoading: false, isLogin: true });
    };
    componentWillReceiveProps(props){
      this.setState({joToken:props.token})
    }
    async componentDidMount() {
      if (!window.GA_INITIALIZED) {
        initGA();
        window.GA_INITIALIZED = true
      }
      logPageView();
      await this.props.dispatch(getLocationUser())

      if (this.state.isPrivate) {
        if (this.props.token === null) {
          Router.pushRoute('/login');
        }
      }
    }

    render() {
      const { isLoading, isLogin, isPrivate, joToken, customClass, isCheckout, isPayment, locationUser } = this.state;
      return (
          <Layout customClass={customClass} isPrivate={isPrivate} isCheckout={isCheckout} locationUser={locationUser} isPayment={isPayment}>
            <AuthComponent {...this.props} joToken={joToken} />
          </Layout>
        )
    }
  };

const withAuth = (component, permissions) =>
  compose(
    connect(state => ({
      token: state.auth.token,
      locationUser:state.locationUser.userLocation
    })),
    withAuthentication(component, permissions)
  );

export default withAuth;
