import numeral from 'numeral'
import { get, isEmpty } from 'lodash'

if (isEmpty(get(numeral, 'locales.id'))) {
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'rb',
            million: 'jt',
            billion: 'm',
            trillion: 'tr'
        },
        ordinal : function (number) {
            return 'ke-';
        },
        currency: {
            symbol: 'Rp '
        }
    });
    numeral.locale('id');
}

export default numeral;