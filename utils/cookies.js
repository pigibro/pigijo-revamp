import Cookies from "js-cookie";
import uCookies from 'universal-cookie';

export const setCookie = (key, value) => {
  if (process.browser) {
    Cookies.set(key, value, {
      expires: 7
    });
  }
};

export const removeCookie = key => {
  if (process.browser) {
    Cookies.remove(key);
  }
};

export const getCookie = (key, req = null) => {
  return !process.browser && req
    ? getCookieFromServer(key, req)
    : getCookieFromBrowser(key);
};

export const getCookieFromBrowser = key => {
  return Cookies.get(key);
};

export const getCookieFromServer = (key, req) => {
  if (req) {
    const cookies = new uCookies(req.headers.cookie);
    return cookies.get(key)
  }
  return null;
};