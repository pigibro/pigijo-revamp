export const GA_TRACKING_ID = 'AW-783099727'

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = url => {
  window.gtag('config', GA_TRACKING_ID, {
    page_location: url
  })
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, buttonId, category, label, value }) => {
  window.gtag('event', action, {
    send_to: `${GA_TRACKING_ID}/${buttonId}`,
    event_category: category,
    event_label: label,
    value: value
  })
}