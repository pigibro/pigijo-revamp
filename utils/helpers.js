import moment from "moment";
import { useState, useEffect, useRef } from 'react';

export const replaceEnter = (text) => {
	if (text !== null && text.length > 0) {
		return text.replace(/(?:\r\n|\r|\n)/g, "<br />");
	}
	return text;
};

export const escapeRegexCharacters = (str) => {
	return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
};

export const slugify = (string) => {
	const a = "àáäâãåèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;";
	const b = "aaaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------";
	const p = new RegExp(a.split("").join("|"), "g");
	return string
		.toString()
		.toLowerCase()
		.replace(/\s+/g, "-") // Replace spaces with
		.replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
		.replace(/&/g, "-and-") // Replace & with ‘and’
		.replace(/[^\w-]+/g, "") // Remove all non-word characters
		.replace(/--+/g, "-") // Replace multiple — with single -
		.replace(/^-+/, ""); // Trim — from start of text .replace(/-+$/, '') // Trim — from end of text
};

export const urlImage = (string, category = 'activity') => {
	let url = 'https://s3-ap-southeast-1.amazonaws.com/pigijo/';
	if(category === 'homestay'){
		url = 'http://hotel.pigijo.id/images/hotels/';
	}
	if(category === 'rent-car'){
		url = 'http://hotel.pigijo.id/images/vehicles/';
	}
	return `${url}${string}`;
};

export const ucFirst = (string) => {
	return string.substr(0, 1).toUpperCase() + string.substr(1);
}

export const dateGroup = (start, end) => {
	let month1 = moment(start).format("MMMM");
	let month2 = moment(end).format("MMMM");
	let day1 = moment(start).format("DD");
	let day2 = moment(end).format("DD");
	let year1 = moment(start).format("YYYY");
	let year2 = moment(end).format("YYYY");
	if (start === end) {
		return day1 + " " + month1 + " " + year1;
	} else {
		if (day1 !== day2 && (month1 === month2 && year1 === year2)) {
			return day1 + " - " + day2 + " " + month1 + " " + year1;
		}
		if (month1 !== month2) {
			return day1 + " " + month1 + " " + year1 + " - " + day2 + " " + month2 + " " + year2;
		}
	}
};

export const removeEmpty = (obj) => {
	const o = JSON.parse(JSON.stringify(obj)); // Clone source oect.
	Object.keys(o).forEach((key) => {
		if (o[key] && typeof o[key] === "object") {
			o[key] = removeEmpty(o[key]);
		} else if (o[key] === undefined || o[key] === null) {
			delete o[key]; // Delete undefined and null.
		} else {
			o[key] = o[key];
		}
	});
	return o; // Return new object.
};

export const countDate = (tgl1, tgl2) => {
	const miliday = 24 * 60 * 60 * 1000;
	const tanggal1 = new Date(tgl1);
	const tanggal2 = new Date(tgl2);
	const tglPertama = Date.parse(tanggal1);
	const tglKedua = Date.parse(tanggal2);
	const selisih = (tglKedua - tglPertama) / miliday;
	return selisih;
};

export const hitungSelisih = (tgl1, tgl2) => {
	const miliday = 24 * 60 * 60 * 1000;
	const tanggal1 = new Date(tgl1);
	const tanggal2 = new Date(tgl2);
	const tglPertama = Date.parse(tanggal1);
	const tglKedua = Date.parse(tanggal2);
	const selisih = (tglKedua - tglPertama) / miliday;
	return selisih;
};

export const convertToRp = (angka) => {
	let rupiah = "";
	let angkarev = angka
		.toString()
		.split("")
		.reverse()
		.join("");
	for (let i = 0; i < angkarev.length; i++) if (i % 3 === 0) rupiah += angkarev.substr(i, 3) + ".";
	return (
		"Rp. " +
		rupiah
			.split("", rupiah.length - 1)
			.reverse()
			.join("")
	);
};

export const truncateString = (str, maxLength=145) => {
	if (str.length > maxLength) {
		return str.substring(0, maxLength - 2) + '..';
	}
	return str;
}

export function useComponentVisible(initialIsVisible) {
    const [isComponentVisible, setIsComponentVisible] = useState(initialIsVisible);
    const ref = useRef(null);

    const handleClickOutside = (event) => {
        if (ref.current && !ref.current.contains(event.target)) {
            setIsComponentVisible(false);
        }
    };

    useEffect(() => {
        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
    });

    return { ref, isComponentVisible, setIsComponentVisible };
}
