import airplane from '../static/icons/airplane.svg';
import event from '../static/icons/event.svg';
import hotel from '../static/icons/hotel.svg';
import accommodation from '../static/icons/accommodation.svg';
import restaurant from '../static/icons/restaurant.svg';
import carrent from '../static/icons/carrent.svg';
import beach from '../static/icons/beach.svg';
import place from '../static/icons/place.svg';
import ic_car from '../static/icons/ic_car.png';
import list from '../static/icons/ic_list.png';
import ic_ticket_24 from '../static/icons/ic_ticket_24.png';
import ic_bed_24 from '../static/icons/ic_bed_24.png';
import ic_cutlery_24 from '../static/icons/ic_cutlery_24.png';
import ic_plane from '../static/icons/ic_plane.png';
import activities from '../static/icons/activities.svg';
import pictures from '../static/icons/ic_pictures.png';
import TA from '../static/icons/TA.svg';
import step1 from "../static/steps/step1.png";
import step2 from "../static/steps/step2.png";
import step3 from "../static/steps/step3.png";
import step4 from "../static/steps/step4.png";
import step5 from "../static/steps/step5.png";
import diary from "../static/icons/ic_diary.png";
import vacation from "../static/icons/ic_vacation.png";
import mountain from "../static/icons/mountains.svg";
import house from "../static/icons/house.svg";
import calendar from "../static/icons/calendar.svg";
import vehicle from "../static/icons/vehicle.svg";








export {
  vehicle,
  calendar,
  house,
  mountain,
  vacation,
  diary,
  airplane,
  ic_bed_24,
  ic_car,
  ic_ticket_24,
  ic_plane,
  ic_cutlery_24,
  list,
  event,
  hotel,
  accommodation,
  restaurant,
  carrent,
  place,
  beach,
  step1,
  step2,
  step3,
  step4,
  step5,
  activities,
  pictures,
  TA,
}